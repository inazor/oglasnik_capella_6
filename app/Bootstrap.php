<?php

namespace Baseapp;

require_once APP_DIR . '/common/traits/CommonServiceLoaders.php';

use Baseapp\Library\Email;
use Baseapp\Models\Categories;
use Baseapp\Models\CmsCategories;
use Baseapp\Traits\CommonServiceLoaders as BaseServices;
use Phalcon\Di;
use Phalcon\DiInterface;
use Phalcon\Mvc\Application;

/**
 * Bootstrap (ran on every request!)
 */
class Bootstrap extends Application
{
    use BaseServices;

    protected $renderProphiler = true;

    /**
     * Bootstrap constructor - set the dependency Injector
     *
     * @param DiInterface $di
     */
    public function __construct(DiInterface $di)
    {
        mb_internal_encoding('utf-8');

        $this->_di = $di;

        $service_loaders = array(
            'loader',
            'config',
            'timezone',
            'logger',
            'shutdown_manager',
            'messages_collector',
            'error_handling',
            'db',
            'dispatcher',
            'events_manager',
            'cache',
            'cached_global_objects',
            'crypt',
            'security',
            'cookies',
            'session',
            'auth',
            'router',
            'flash',
            'url',
            'filter',
            'models_metadata',
            'assets',
            'tag',
            'signer',
            'hybridauth',
            'view_counts_manager',
            'uploader',
            'beanstalk'
        );

        // Register services
        foreach ($service_loaders as $service) {
            $this->$service();
        }

        // Hook up prophiler listeners if config says so
        $config = $this->_di->getShared('config');
        if ($config->app->debug) {
            $this->prophiler();
        }

        // Register modules
        $this->registerModules(array(
            'frontend' => array(
                'className' => 'Baseapp\Frontend\Module',
                'path'      => ROOT_PATH . '/app/frontend/Module.php'
            ),
            'backend'  => array(
                'className' => 'Baseapp\Backend\Module',
                'path'      => ROOT_PATH . '/app/backend/Module.php'
            ),
			'suva'  => array(
                'className' => 'Baseapp\Suva\Module',
                'path'      => ROOT_PATH . '/app/suva/Module.php'
            )
        ));

        // Register the app itself as a service
        $this->_di->set('app', $this);

        // Set the dependency Injector
        parent::__construct($this->_di);
    }

    /**
     * Hook up prophiler listeners
     */
    protected function prophiler()
    {
        // Bail early if 'prophiler' service ain't present
        if (!$this->getDI()->has('prophiler')) {
            return;
        }

        $em = $this->getEventsManager();

        $prophiler = $this->getDI()->getShared('prophiler');
        $em->attach('dispatch', \Fabfuel\Prophiler\Plugin\Phalcon\Mvc\DispatcherPlugin::getInstance($prophiler));
        $em->attach('db', \Fabfuel\Prophiler\Plugin\Phalcon\Db\AdapterPlugin::getInstance($prophiler));

        // If the shared 'db' service has no EventsManager yet, we have to set it
        // (or the prophiler hooks don't get invoked)
        // $db_em = $this->getDI()->getShared('db')->getEventsManager();
        $di = $this->getDI();
        if ($di->has('db')) {
            $db_em = $di->getShared('db')->getEventsManager();
            if (null === $db_em) {
                $di->getShared('db')->setEventsManager($em);
            }
        }
    }

    protected function cached_global_objects()
    {
        $this->_di->set(
            Categories::MEMCACHED_KEY,
            function() {
                return Categories::getMemcachedData();
            },
            true // shared service
        );

        $this->_di->set(
            CmsCategories::MEMCACHED_KEY,
            function() {
                return CmsCategories::getMemcachedData();
            },
            true // shared service
        );
    }

    /**
     * Set the static router service
     *
     * @return void
     */
    protected function router()
    {
        $this->_di->set(
            'router',
            function() {
                $router = new \Phalcon\Mvc\Router(false);

                $router->removeExtraSlashes(true);

                $router->setDefaults(array(
                    'module'     => 'frontend',
                    'controller' => 'index',
                    'action'     => 'index'
                ));

                /*
                 * All defined routes are traversed in reverse order until Phalcon\Mvc\Router
                 * finds the one that matches the given URI and processes it, while ignoring the rest.
                 * This means routes added last get checked first I guess.
                 */
                $frontend = new \Phalcon\Mvc\Router\Group(array(
                    'module' => 'frontend',
                ));

                $frontend->add('/:controller/:action/:params', array(
                    'controller' => 1,
                    'action'     => 2,
                    'params'     => 3,
                ))->convert('action', function($action) {
                    return lcfirst(\Phalcon\Text::camelize($action));
                });
                $frontend->add('/:controller/:int', array(
                    'controller' => 1,
                    'id'         => 2,
                ));
                $frontend->add('/:controller[/]?', array(
                    'controller' => 1,
                ));

                // Building a "static" list of routes for all the categories for SEO purposes
                $tree = $this->getDI()->get(Categories::MEMCACHED_KEY);
                unset($tree[0]);
                unset($tree[1]);
                foreach ($tree as $category) {
                    if (!empty($category->url)) {
                        $frontend->add('/' . $category->url, array(
                            'controller' => 'categories',
                            'action'     => 'slug'
                        ));
                        $frontend->add('/' . $category->url . '/:params', array(
                            'controller' => 'categories',
                            'action'     => 'slug',
                            'params'     => 1
                        ));
                    }
                }
                unset($tree);

                // Some quasi-static routes
                $frontend->add('/popularni-oglasi', array(
                    'controller' => 'ads',
                    'action'     => 'popular'
                ));
                $frontend->add('/novi-oglasi', array(
                    'controller' => 'ads',
                    'action'     => 'latest'
                ));

                // trgovina controller route
                $frontend->add('/trgovina/([a-zA-Z0-9_-]+)[/]?', array(
                    'controller' => 'trgovina',
                    'action' => 'view',
                    'id' => 1
                ));

                // Ads (and potentially other stuff) belonging to a certain user
                $frontend->add('/korisnik/:params[/]?', array(
                    'controller' => 'user',
                    'action'     => 'ads',
                    'params'     => 1,
                ));

                // section view route
                $frontend->add('/oglasi/:params', array(
                    'controller' => 'sections',
                    'action'     => 'view',
                    'params'     => 1
                ));
                // category view route
                $frontend->add('/category/:int[/]?', array(
                    'controller' => 'categories',
                    'action'     => 'view',
                    'id'         => 1
                ));
                // category view route
                $frontend->add('/category/confirm-age/:int', array(
                    'controller' => 'categories',
                    'action'     => 'confirmAge',
                    'id'         => 1
                ));
                // ad view route
                $frontend->add('/oglas/:int[/]?', array(
                    'controller' => 'ads',
                    'action'     => 'view',
                    'id'         => 1
                ));
                $frontend->add('/(.*)-oglas-([0-9]+)', array(
                    'controller'    => 'ads',
                    'action'        => 'view',
                    'entity_id'     => 2,
                    'seo_slug'      => 1
                ));

                $frontend->add('/oglas/:action/:int[/]?', array(
                    'controller' => 'ads',
                    'action'     => 1,
                    'id'         => 2
                ));

                // cms magazine category view route
                $frontend->add('/novosti/clanak/([a-zA-Z0-9_-]+)[/]?', array(
                    'controller' => 'magazine',
                    'action'     => 'article',
                    'id'         => 1
                ));
                $frontend->add('/novosti/([a-zA-Z0-9_-]+)[/]?', array(
                    'controller' => 'magazine',
                    'action'     => 'category',
                    'id'         => 1
                ));
                $frontend->add('/novosti[/]?', array(
                    'controller' => 'magazine',
                    'action'     => 'index'
                ));

                // cms info view route
                $frontend->add('/info/:params[/]?', array(
                    'controller' => 'static',
                    'action'     => 'view',
                    'params'     => 1
                ));

                $cmsTree = $this->getDI()->get(CmsCategories::MEMCACHED_KEY);
                unset($cmsTree[0]);
                unset($cmsTree[1]);
                foreach ($cmsTree as $cmsCategory) {
                    if ($cmsCategory->active === 1 && !empty($cmsCategory->url) && isset($cmsCategory->type) && $cmsCategory->type == 'info') {
                        $frontend->add('/' . $cmsCategory->url . '/:params', array(
                            'controller'   => 'static',
                            'action'       => 'articleSlug',
                            'categorySlug' => $cmsCategory->url,
                            'params'       => 1
                        ));
                        $frontend->add('/' . $cmsCategory->url, array(
                            'controller' => 'static',
                            'action'     => 'categorySlug'
                        ));
                    }
                }
                unset($cmsTree);

                // tag view route
                $frontend->add('/tag/:params[/]?', array(
                    'controller' => 'tags',
                    'action'     => 'view',
                    'params'     => 1
                ));

                // moj oglasnik/trgovina controller route
                $frontend->add('/moj-kutak/trgovina[/]?', array(
                    'controller' => 'moj-kutak',
                    'action' => 'trgovina'
                ));
                // moj oglasnik/trgovina controller route
                $frontend->add('/moj-kutak/trgovina/izlog/:params[/]?', array(
                    'controller' => 'moj-kutak',
                    'action' => 'trgovinaIzlog',
                    'params' => 1
                ));

                $frontend->add('/');

                // Mount a group of routes for frontend
                $router->mount($frontend);

                /**
                 * Define routes for each module
                 */
                // foreach ($this->getModules() as $module => $options) {
                // foreach (array('backend' => array('alias' => 'admin'), 'documentation' => array('alias' => 'doc')) as $module => $options) {
                foreach (array('backend' => array('alias' => 'admin')) as $module => $options) {
                    $group = new \Phalcon\Mvc\Router\Group(array(
                        'module' => $module,
                    ));
                    $group->setPrefix('/' . (isset($options['alias']) ? $options['alias'] : $module));

                    $group->add('/:controller/:action/:params', array(
                        'controller' => 1,
                        'action'     => 2,
                        'params'     => 3,
                    ))->convert('action', function($action) {
                        return lcfirst(\Phalcon\Text::camelize($action));
                    });
                    $group->add('/:controller/:int', array(
                        'controller' => 1,
                        'id'         => 2,
                    ));
                    $group->add('/:controller[/]?', array(
                        'controller' => 1,
                    ));
                    $group->add('/shops/:int/:action/:int', array(
                        'controller'       => 'shops',
                        'shop_id'          => 1,
                        'action'           => 2,
                        'featured_shop_id' => 3
                    ));
                    $group->add('/shops/:int/:action', array(
                        'controller'       => 'shops',
                        'shop_id'          => 1,
                        'action'           => 2
                    ));
                    $group->add('[/]?', array());

                    // Mount a group of routes for the module
                    $router->mount($group);
                }

                $router->notFound(array(
                    'controller' => 'error',
                    'action'     => 'show404'
                ));
				
				foreach (array(
					'suva' => array('alias' => 'suva')
					
					
					) as $module => $options) {
                    $group = new \Phalcon\Mvc\Router\Group(array(
                        'module' => $module,
                    ));
                    $group->setPrefix('/' . (isset($options['alias']) ? $options['alias'] : $module));

                    $group->add('/:controller/:action/:params', array(
                        'controller' => 1,
                        'action'     => 2,
                        'params'     => 3,
                    ))->convert('action', function($action) {
                        return lcfirst(\Phalcon\Text::camelize($action));
                    });
                    $group->add('/:controller/:int', array(
                        'controller' => 1,
                        'id'         => 2,
                    ));
                    $group->add('/:controller[/]?', array(
                        'controller' => 1,
                    ));
                    $group->add('/shops/:int/:action/:int', array(
                        'controller'       => 'shops',
                        'shop_id'          => 1,
                        'action'           => 2,
                        'featured_shop_id' => 3
                    ));
                    $group->add('/shops/:int/:action', array(
                        'controller'       => 'shops',
                        'shop_id'          => 1,
                        'action'           => 2
                    ));
                    $group->add('/(.*)-oglas-([0-9]+)', array(
						'controller'    => 'ads',
						'action'        => 'view',
						'entity_id'     => 2,
						'seo_slug'      => 1
					));
					
					$group->add('[/]?', array());

                    // Mount a group of routes for the module
                    $router->mount($group);
                }

                $router->notFound(array(
                    'controller' => 'error',
                    'action'     => 'show404'
                ));

                return $router;
            }
        );
    }

    /**
     * Adds additional response headers (X-Powered-By and potentially others we might need)
     *
     * @param \Phalcon\Http\ResponseInterface $response
     *
     * @return mixed
     */
    public function add_extra_headers($response)
    {
        $response->setHeader('X-Powered-By', "['blood', 'sweat', 'tears']");
        $response->setHeader('X-Frame-Options', 'SAMEORIGIN');
        $response->setHeader('X-UA-Compatible', 'IE=edge');

        $version = self::get_version();
        if ($version) {
            $response->setHeader('X-Version', $version);
        }

        if ('production' !== $this->config->app->env) {
            $response->setHeader('X-MemUsage', number_format(memory_get_usage()/1024, 2) . ' KB');
            $response->setHeader('X-MemUsagePeak', number_format(memory_get_peak_usage()/1024, 2) . ' KB');
        }

        return $response;
    }

    public static function get_version()
    {
        $ver = $ph_version = 'ext-' . \Phalcon\Version::get();

        $config = Di::getDefault()->getShared('config');
        if (isset($config->versions->app)) {
            $ver = $config->versions->app . ' (' . $ph_version . ')';
        }

        return $ver;
    }

    /**
     * "The destructor method will be called as soon as there are no other references to
     * a particular object, or in any order during the shutdown sequence."
     *
     * Note:
     * Destructors called during the script shutdown have HTTP headers already sent.
     *
     * Note:
     * Attempting to throw an exception from a destructor (called in the time of script termination)
     * causes a fatal error.
     *
     * We're currently using it to check the 'messagesCollector' DI service for any
     * messages stored in the 'email-log' bucket and sending the error report
     * email if there's something to send.
     *
     * There is potential for exceptions to be thrown here in various edge cases,
     * but it seems to work for the general case so far.
     *
     * @throws \Exception
     * @throws \phpmailerException
     */
    public function __destruct()
    {
        $di = Di::getDefault();
        $collector = null;
        if ($di->has('messagesCollector')) {
            $collector = $di->get('messagesCollector');
        }
        if (null === $collector) {
            return;
        }

        $email_log = $collector->getMessages('email-log');
        if (!empty($email_log)) {
            $email_log = implode("\n<br>", $email_log);
            if (!empty($email_log)) {
                $config = $di->getShared('config');
                // Send an email if not in dev or test environments (where errors happen often)
                // if ($config->app->env !== 'testing') {
                if ('testing' !== $config->app->env && 'development' !== $config->app->env) {
                    $email       = new Email();
                    $email_title = 'Oglasnik.hr - error report (env: ' . $config->app->env . ')';
                    $email->prepare($email_title,
                        $config->app->admin,
                        'error',
                        array(
                            'log'   => $email_log,
                            'title' => $email_title
                        )
                    );
                    $email->send();
                }
            }
        }
    }

    /**
     * Logs exception (and other types of) messages into a file and into the 'email-log' bucket of the
     * 'messagesCollector' DI service if it's available. On production/staging environments messages from
     * that bucket are supposed to be emailed to the admin email from config.ini -- see `Bootstrap::__destruct()`
     *
     * @param string|bool|array|\Exception $data Data
     */
    public static function log($data)
    {
        static $call_cnt = 0;
        $call_cnt++;

        // Log exceptions by transforming them into an array
        if ($data instanceof \Exception) {
            $data = array(
                'error' => get_class($data) . '[' . $data->getCode() . ']: ' . $data->getMessage(),
                'info' => $data->getFile() . '[' . $data->getLine() . ']',
                'debug' => "Trace: \n" . $data->getTraceAsString() . "\n",
            );
        }

        // Enables quick&dirty debugging in various situations by simply calling Bootstrap::log($stuff);
        if (is_string($data) || is_numeric($data) || is_bool($data)) {
            $data = (array) $data;
        }

        $di = Di::getDefault();

        // Collects stuff for the duration of the request and then
        // only send 1 email with all the messages, not for every call to log()
        $collector = null;
        if ($di->has('messagesCollector')) {
            $collector = $di->getShared('messagesCollector');
        }

        /**
         * @var $logger \Phalcon\Logger\AdapterInterface
         */
        $logger = null;
        if ($di->has('logger')) {
            $logger = $di->getShared('logger');
        }

        // Logging the request URI additionally (but only for the first call during a request)
        if (1 === $call_cnt && $logger) {
            if ('cli' === PHP_SAPI) {
                $extra_msg = 'CLI: ' . implode(' ', $_SERVER['argv']);
            } else {
                $request = Di::getDefault()->getShared('request');
                $extra_msg = 'Request URI: ' . $request->getMethod() . ' ' . $request->getURI();
            }
            $logger->log('debug', $extra_msg);
            if ($collector) {
                $collector->addMessage($extra_msg, 'email-log');
            }
        }

        // Collect and log actual messages with severity levels (if specified)
        if ($logger) {
            foreach ($data as $key => $message) {
                if (!is_string($message)) {
                    $message = var_export($message, true);
                }
                if (in_array($key, array('alert', 'debug', 'error', 'info', 'notice', 'warning'), true)) {
                    $logger->$key($message);
                } else {
                    $logger->log('debug', $message);
                }
                if ($collector) {
                    $collector->addMessage($message, 'email-log');
                }
            }
        }
    }

    public function setRenderProphiler($value)
    {
        $this->renderProphiler = $value;
    }

    public function shouldRenderProphiler()
    {
        return (true === (bool) $this->renderProphiler);
    }
}
