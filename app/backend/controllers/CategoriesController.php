<?php

namespace Baseapp\Backend\Controllers;

use Baseapp\Extension\Tag;
use Baseapp\Models\Categories;
use Baseapp\Models\TransactionTypes;
use Baseapp\Traits\CrudActions;

/**
 * Backend Categories Controller
 */
class CategoriesController extends IndexController
{
    use CrudActions;

    public $crud_model_class = 'Baseapp\Models\Categories';

    protected $allowed_roles = array('admin');

    /**
     * Index Action
     */
    public function indexAction()
    {
        $this->tag->setTitle('Categories');

        $this->assets->addJs('assets/vendor/jquery-ui-1.11.2-custom/jquery-ui.min.js');
        $this->assets->addJs('assets/vendor/jquery.nestedSortable.js');
        $this->assets->addJs('assets/backend/js/nested-items-sortable.js');
        $this->assets->addJs('assets/backend/js/categories-index.js');

        $tree = $this->getDI()->get(Categories::MEMCACHED_KEY);
        // TODO: since categories are using a single root tree, everything
        // is wrapped within a master root node, which has id=1 currently,
        // so this grabs those children. But this feels very hackish.
        // Need to try to come up with a better api for this...
        
		//zakomentirano borna
		// $tree_root_id = 1;
        // Tag::setDefault('tree_root_id', $tree_root_id);
        // $categories = $tree[$tree_root_id]->children;
        // $this->view->setVar('categories', $categories);
		
		//dodano borna
		$tree_root_id = 1;
        Tag::setDefault('tree_root_id', $tree_root_id);
		$xCat = new \Baseapp\Models\Categories();
		$tree = $xCat->getTree();
        $categories = $tree[$tree_root_id]->children;
        $this->view->setVar('categories', $categories);


        $this->view->setVar('available_item_actions', array(
            array(
                'path' => 'admin/categories/edit/',
                'name'   => '<i class="fa fa-edit fa-fw"></i>Edit'
            ),
            array(
                'path' => 'admin/categories/create/',
                'name'   => '<i class="fa fa-code-fork fa-fw"></i>New sub'
            ),
            array(
                'path' => 'admin/categories/delete/',
                'name'   => '<i class="fa fa-trash-o text-danger fa-fw"></i>'
            )/*,
            array(
                'path' => 'admin/categories/active-toggle/',
                'name'   => '<i class="fa fa-trash-o text-danger fa-fw"></i>'
            )*/
        ));
    }

    /**
     * Create Action
     *
     * @param int|string|null $parent_id
     *
     * @return void|\Phalcon\Http\ResponseInterface
     */
    public function createAction($parent_id = null)
    {
        if ($this->request->isPost() && $this->request->hasPost('parent_id')) {
            $parent_id = (int) $this->request->getPost('parent_id');
        } elseif (is_numeric($parent_id)) {
            $parent_id = (int) $parent_id;
        } else {
            $parent_id = 1;
        }

        if ($this->request->isPost() && $this->request->hasPost('category_icon')) {
            $category_icon = $this->request->getPost('category_icon');
        } else {
            $category_icon = null;
        }

        $category_fallback_image = null;
        if ($this->request->isPost() && $this->request->hasPost('category_fallback_image')) {
            $category_fallback_image = $this->request->getPost('category_fallback_image');
        }

        /* @var $model \Baseapp\Models\Categories */
        $model = $this->crud_model_class;
        $parent = $model::findFirst($parent_id);

        if (!$parent) {
            $this->flashSession->error(sprintf('Entity not found [Model: %s, ID: %s]', $this->crud_model_class, $parent_id));
            return $this->redirect_back();
        }

        $this->tag->setTitle('Add new category');
        $this->view->setVar('form_title_long', 'Add category');

        $this->add_common_crud_assets();

        // Use the same form/view as editAction
        $this->view->pick('categories/edit');

        $entity = new Categories();
        $entity->json = json_decode('{}');  // initialize empty json
        if ($this->request->isPost()) {
            $save_action = $this->get_save_action();
            $next_url = $this->get_next_url();
            $created = $entity->add_new($this->request, $parent);
            if ($created instanceof Categories) {
                $this->flashSession->success('<strong>Successfully created new category!</strong> ');
                if ('save' === $save_action) {
                    $next_url = 'admin/categories/edit/' . $entity->id;
                }
                return $this->redirect_to($next_url);
            } else {
                $this->flashSession->error('<strong>Error!</strong> Fix all fields that failed to validate.');
                $this->view->setVar('errors', $created);
            }
        } else {
            $entity->transaction_type_id = 0;
            $entity->active = 1;
        }

        $this->parent_id_dropdown($parent_id);
        $this->transaction_type_dropdown($entity);
        $this->moderation_type_dropdown();
        $this->category_icon_dropdown($category_icon);
        $this->category_fallback_image_dropdown($category_fallback_image);

        $this->view->setVar('category', $entity);
    }

    /**
     * Edit Action
     *
     * @param $entity_id
     *
     * @return void|\Phalcon\Http\ResponseInterface
     */
    public function editAction($entity_id)
    {
        $this->tag->setTitle('Edit category');
        $this->view->setVar('form_title_long', 'Edit category');

        $this->add_common_crud_assets();

        $entity_id = (int) $entity_id;
        if (!$entity_id) {
            $this->flashSession->error('Missing required ID parameter for edit action!');
            return $this->redirect_back();
        }

        /* @var $model \Baseapp\Models\Categories */
        $model = $this->crud_model_class;
        $entity = $model::findFirst($entity_id);

        if (!$entity) {
            $this->flashSession->error(sprintf('Entity not found [Model: %s, ID: %s]', $this->crud_model_class, $entity_id));
            return $this->redirect_back();
        }
        $entity_settings = $entity->getSettings();

        $parent_id = $this->request->isPost() ? $this->request->getPost('parent_id') : $entity->parent()->id;

        if ($this->request->isPost()) {
            $save_action = $this->get_save_action();
            $next_url = $this->get_next_url();
            $saved = $entity->save_changes($this->request, Categories::findFirst($parent_id));
            if ($saved instanceof Categories) {
                $this->flashSession->success('<strong>Category saved successfully!</strong>');
                if ('save' === $save_action) {
                    return $this->redirect_self();
                } else {
                    return $this->redirect_to($next_url);
                }
            } else {
                $this->view->setVar('errors', $saved);
                $this->flashSession->error('<strong>Error!</strong> Fix all fields that failed to validate.');
            }
        }

        $disabled_categories = $entity->getChildrenIDs(true);
        $this->parent_id_dropdown($parent_id, $disabled_categories);
        $this->transaction_type_dropdown($entity);
        $this->moderation_type_dropdown($entity_settings);

        $category_icon = null;
        if (isset($entity->json->icon)) {
            $category_icon = $entity->json->icon;
        }
        $this->category_icon_dropdown($category_icon);

        $category_fallback_image = null;
        if (isset($entity->json->fallback_image)) {
            $category_fallback_image = $entity->json->fallback_image;
        }
        $this->category_fallback_image_dropdown($category_fallback_image);

        $this->view->setVar('category', $entity);
    }

    /**
     * Save categories order Action
     */
    public function saveOrderAction()
    {
        if ($this->request->isPost()) {

            $model = new Categories();
            $root = $model->getRoot();
            $tree = $root->getTree();

            if (!$root) {
                $this->flashSession->error('Tree root not found');
                return $this->redirect_back();
            }

            $items = $this->request->getPost('categories_order');
            $items = json_decode($items, true);

            if ($root->updateTreeOrder($items, $tree)) {
                $root->deleteMemcachedData();
                $this->flashSession->success('<strong>Categories reordered successfully!</strong>');
            } else {
                $this->flashSession->error('<strong>Error!</strong> Categories were not reordered successfully!');
            }
            return $this->redirect_to('admin/categories');
        } else {
            $this->flashSession->error('<strong>Error!</strong> Method not allowed!');
            return $this->redirect_back();
        }
    }

    /**
     * activeToggle Action
     */
    public function activeToggleAction($category_id = null)
    {
        if ($category = Categories::findFirst(array('id=:category_id:', 'bind' => array('category_id' => $category_id)))) {
            if ($category->activeToggle()) {
                $this->flashSession->success('Active status for category <strong>' . $category->name . '</strong> changed successfully.');
            } else {
                $this->flashSession->error('Failed changing active status for category <strong>'.$category->name.'</strong>.');
            }
        } else {
            $this->flashSession->error('Category with ID: <strong>'.$category_id.'</strong> not found.');
        }

        return $this->redirect_back();
    }

    /**
     * Helper method to generate parent_id dropdown
     *
     * @param integer $preselected_category_value   Value to preselect
     * @param array                                 Array of values to disable in dropdown
     */
    public function parent_id_dropdown($preselected_category_value = 1, $disabled = array())
    {
        Tag::setDefault('parent_id', $preselected_category_value);

        $parent_id_dropdown = Tag::select(array(
            'parent_id',
            Categories::findFirst(1)->getSelectValues(),
            'disabled' => $disabled,
            'class' => 'form-control show-tick'
        ));

        $this->view->setVar('parent_id_dropdown', $parent_id_dropdown);
    }

    /**
     * @param \Phalcon\Mvc\ModelInterface $category
     */
    public function transaction_type_dropdown($category)
    {
        if (isset($category->transaction_type_id)) {
            Tag::setDefault('transaction_type_id', $category->transaction_type_id);
        }

        $transaction_type_dropdown = \Phalcon\Tag::select(array(
            'transaction_type_id',
            TransactionTypes::find(),
            'using' => array('id', 'name'),
            'useEmpty' => true,
            'emptyText' => 'No transaction type',
            'emptyValue' => '0',
            'class' => 'form-control show-tick'
        ));

        $this->view->setVar('transaction_type_id_dropdown', $transaction_type_dropdown);
    }

    /**
     * @param \Baseapp\Models\CategoriesSettings|null $category_settings
     */
    public function moderation_type_dropdown($category_settings = null)
    {
        if (null !== $category_settings) {
            if (isset($category_settings->moderation_type)) {
                Tag::setDefault('moderation_type', $category_settings->moderation_type);
            }
        }

        $moderation_types = array(
            'pre'  => 'Pre-moderation',
            'post' => 'Post-moderation'
        );

        $moderation_type_dropdown = \Phalcon\Tag::select(array(
            'moderation_type',
            $moderation_types,
            'class' => 'form-control show-tick'
        ));

        $this->view->setVar('moderation_type_dropdown', $moderation_type_dropdown);
    }

    /**
     * Helper method to generate category_icon dropdown
     *
     * @param integer $preselected_category_icon    Icon to preselect
     */
    public function category_icon_dropdown($preselected_category_icon = null)
    {
        Tag::setDefault('category_icon', $preselected_category_icon);

        $category_icon_dropdown = Tag::select(array(
            'category_icon',
            Categories::getAvailableIcons(),
            'useEmpty' => true,
            'emptyText' => 'No icon',
            'emptyValue' => '0',
            'class' => 'form-control show-tick'
        ));

        $this->view->setVar('category_icon_dropdown', $category_icon_dropdown);
    }

    /**
     * Helper method to generate category_fallback_image dropdown
     *
     * @param integer $preselected Preselected image
     */
    public function category_fallback_image_dropdown($preselected = null)
    {
        Tag::setDefault('category_fallback_image', $preselected);

        $fallback_image_dropdown = Tag::select(array(
            'category_fallback_image',
            Categories::getPossibleFallbackImagesList(),
            'useEmpty' => true,
            'emptyText' => 'Default',
            'emptyValue' => '0',
            'class' => 'form-control show-tick'
        ));

        $this->view->setVar('category_fallback_image_dropdown', $fallback_image_dropdown);
    }

    public function add_common_crud_assets()
    {
        $this->assets->addJs('assets/vendor/ckeditor/ckeditor.js');
        $this->assets->addJs('assets/vendor/ckeditor/adapters/jquery.js');
        $this->assets->addJs('assets/backend/js/categories-edit.js');
    }

    public function get_chosen_transaction_type_ids()
    {
        $transaction_type_ids = array();
        if ($this->request->isPost()) {
            $transaction_type_ids = $this->request->getPost('transaction_type_ids', null, array());
        }
        return $transaction_type_ids;
    }
}
