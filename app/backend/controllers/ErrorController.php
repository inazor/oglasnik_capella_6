<?php

namespace Baseapp\Backend\Controllers;

use Baseapp\Traits\ErrorControllerTrait;

class ErrorController extends IndexController
{
    use ErrorControllerTrait;
}
