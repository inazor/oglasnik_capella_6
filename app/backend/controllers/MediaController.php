<?php

namespace Baseapp\Backend\Controllers;

use Baseapp\Controllers\BaseBackendController;
use Baseapp\Extension\Tag;
use Baseapp\Library\Tool;
use Baseapp\Library\Utils;
use Baseapp\Models\Media;
use Baseapp\Models\MediaCrops;
use Baseapp\Traits\CrudActions;
use Baseapp\Traits\CrudHelpers;
use Phalcon\Http\Response;
use Phalcon\Paginator\Adapter\QueryBuilder;

class MediaController extends BaseBackendController
{
    use CrudActions;
    use CrudHelpers;

    public $crud_model_class = 'Baseapp\Models\Media';

    protected $allowed_roles = array('admin', 'content');

    public static function get_types_dropdown_data()
    {
        return array(
            Media::TYPE_IMAGE => array(
                'slug'      => 'image',
                'title'     => 'Images only',
                'glyphicon' => 'glyphicon-picture',
            ),
            Media::TYPE_VIDEO => array(
                'slug'      => 'video',
                'title'     => 'Videos only',
                'glyphicon' => 'glyphicon-facetime-video',
            ),
            Media::TYPE_AUDIO => array(
                'slug'      => 'audio',
                'title'     => 'Audio only',
                'glyphicon' => 'glyphicon-music'
            ),
            Media::TYPE_FILE  => array(
                'slug'      => 'file',
                'title'     => 'Files only',
                'glyphicon' => 'glyphicon-file'
            )
        );
    }

    // Override default order_by options from CrudHelpers since we want different options here
    protected function getPossibleOrderByOptions()
    {
        return array(
            'created_at'    => 'Date created',
            'modified_at'   => 'Date modified',
            'filename_orig' => 'Original filename',
            'filename'      => 'Filename',
            'title'         => 'Title',
            'type'          => 'Type'
        );
    }

    /**
     * Index Action
     */
    public function indexAction()
    {
        $title = 'Media Library';

        $this->tag->setTitle($title);
        $this->view->setVar('page_title', $title);

        $this->assets->addCss('assets/vendor/dropzone/css/dropzone.css');
        $this->assets->addJs('assets/vendor/dropzone/dropzone.js');
        $this->assets->addJs('assets/backend/js/search-media.js');

        $is_popup = $this->request->getQuery('is_popup', 'int', 0);
        $this->view->setVar('is_popup', $is_popup);
        if ($is_popup) {
            $this->set_popup_layout();
        }

        $page       = $this->getPageParam();
        $search_for = $this->processSearchParam();
        $mode       = $this->processSearchMode();
        $dir        = $this->processAscDesc('desc');
        $order_by   = $this->processOrderBy('created_at');

        // Type filtering
        $all_type = array(0 => array(
                'slug' => 'all',
                'title' => 'All types',
                'icon' => '',
            )
        );
        $types = self::get_types_dropdown_data();
        $types = $all_type + $types;
        $type = $this->request->getQuery('type', 'string', 0);
        if (!isset($types[$type])) {
            $type = 0;
        }
        $this->view->setVar('available_types', $types);
        $this->view->setVar('type', $type);

        $available_fields = array(
            'filename',
            'filename_orig',
            'mimetype',
            'mimetype_orig',
            'author',
            'title',
            'description',
            'copyright',
            'keywords',
            'created_at',
            'modified_at',
            'created_by_user_id',
            'modified_by_user_id'
        );
        $this->view->setVar('available_fields', $available_fields);

        $field = null;
        if ($this->request->hasQuery('field')) {
            $field = $this->request->getQuery('field');
            if (!in_array($field, $available_fields)) {
                $field = null;
            }
        }
        if (null === $field) {
            $field = 'all';
        }
        $this->view->setVar('field', $field);

        $builder = $this->modelsManager->createBuilder()
            ->columns(array(
                '*'
            ))
            ->from('Baseapp\Models\Media');

        if ($type > 0) {
            $builder->where('type = :type:', array('type' => $type));
        }

        if (!empty($order_by)) {
            $builder->orderBy(sprintf('%s %s', $order_by, strtoupper($dir)));
        }

        // If a specific field is searched/requested, use only that one, otherwise search all the fields
        $fields_to_search = (null === $field || 'all' === $field) ? $available_fields : array($field);
        $search_params = Utils::build_phalcon_search_sql_params($fields_to_search, $search_for, $mode);
        if (null !== $search_params && is_array($search_params)) {
            $conditions = array();
            $binds = array();
            foreach ($search_params as $fld => $val) {
                $field_bind_name = sprintf(':%s:', $fld);
                $conditions[] = $fld . ' LIKE ' . $field_bind_name;
                $binds[$fld] = $val;
                // $builder->orWhere($fld . ' LIKE ' . $field_bind_name, array($fld => $val));
            }
            $conditions = $this->changeSearchParamsLikeIntoEqualsWhenExactMode($conditions, $mode);
            $builder->andWhere(implode(' OR ', $conditions), $binds);
        }

        $paginator = new QueryBuilder(array(
            'builder' => $builder,
            'limit' => $this->config->settingsBackend->pagination_items_per_page,
            'page' => $page
        ));
        $current_page = $paginator->getPaginate();

        $pagination_links = Tool::paginationAdmin(
            $current_page,
            'admin/media',
            'pagination',
            $this->config->settingsBackend->pagination_count_out,
            $this->config->settingsBackend->pagination_count_in
        );

        Tag::setDefaults(array(
            'mode' => $mode,
            'q' => $search_for,
            'field' => $field,
            'is_popup' => (int) $is_popup
        ), true);
        $this->view->setVar('page', $current_page);
        $this->view->setVar('pagination_links', $pagination_links);
    }

    protected function set_common_view_vars()
    {
        $types = self::get_types_dropdown_data();
        $this->view->setVar('media_types', $types);
    }

    public function downloadAction($entity_id = null)
    {
        /**
         * @var $model \Baseapp\Models\Media
         * @var $entity \Baseapp\Models\Media
         */
        $model = $this->crud_model_class;
        $entity = $model::findFirst($entity_id);

        if (!$entity) {
            $this->flashSession->error(sprintf('Resource not found [Model: %s, ID: %s]', $this->crud_model_class, $entity_id));
            return $this->redirect_back();
        } else {
            // $this->view->setRenderLevel(\Phalcon\Mvc\View::LEVEL_NO_RENDER);
            $this->disable_view();

            $file = $entity->get_original_file();

            $this->response->setFileToSend($file, rawurlencode($entity->filename_orig));
            $this->response->setContentType('application/octet-stream');
            $this->response->setContentType($entity->mimetype);
            $this->app->add_extra_headers($this->response);

            // Using X-Sendfile, so we need a relative path from the uploads_dir, which
            // needs to be set properly in the server config (XSendFilePath)
            $file_relative = str_replace($this->config->app->uploads_dir . '/', '', $file);
            $this->response->setHeader('X-Sendfile', $file_relative);

            $this->response->sendHeaders();
            // $this->readfile_chunked($file);
            exit;
        }
    }

    public function cropAction($entity_id, $style)
    {
        $this->disable_view();

        $error         = false;
        $status_code   = 200;
        $status_text   = 'OK';
        $response_data = array();

        if (!$this->request->isPost()) {
            $error       = true;
            $status_code = 405;
            $status_text = 'Method Not Allowed';
        }

        if (!$error && !$this->request->isAjax()) {
            $error       = true;
            $status_code = 400;
            $status_text = 'Bad Request';
        }

        if (!$error) {
            /**
             * @var $model \Baseapp\Models\Media
             * @var $entity \Baseapp\Models\Media
             */
            $model  = $this->crud_model_class;
            $entity = $model::findFirst($entity_id);
            $style  = Media::static_styles_adapter($style);
            if (!$entity || !$style) {
                $error                = true;
                $status_code          = 404;
                $status_text          = 'Not Found';
                $response_data['msg'] = 'Media not found';
                if (!$style) {
                    $response_data['msg'] = 'Style not found';
                }
            }
        }

        // Do the actual thing this action is supposed to be doing...
        if (!$error && $style && (isset($entity) && $entity)) {
            $data = array(
                'x'      => $this->request->getPost('x', 'int', 0),
                'y'      => $this->request->getPost('y', 'int', 0),
                'width'  => $this->request->getPost('width', 'int', 0),
                'height' => $this->request->getPost('height', 'int', 0)
            );

            // Create a new or update an existing crop definition (coords)
            $crop = MediaCrops::findFirst(array(
                'conditions' => 'media_id = :media_id: AND style_id = :style_id:',
                'bind' => array(
                    'media_id' => $entity->id,
                    'style_id' => $style->id
                )
            ));
            if (!$crop) {
                $crop = new MediaCrops();
                $crop->media_id = $entity->id;
                $crop->style_id = $style->id;
            }
            $crop->assign($data);
            $crop->save();

            // Forced creation should create the newly cropped thumb immediately
            $preview = $entity->get_thumb($style, $force_create = true);
            $response_data['preview'] = $preview->toArray();
        }

        $response_data['ok']     = !$error;
        $response_data['status'] = $status_code;
        $response_data['text']   = $status_text;

        $this->response->setContentType('application/json', 'UTF-8');
        $this->response->setStatusCode($status_code, $status_text);
        $this->response->setJsonContent($response_data);

        return $this->response;
    }

    public function editAction($entity_id = null)
    {
        $title = 'Edit media';
        $this->tag->setTitle($title);

        $entity_id = (int) $entity_id;
        if (!$entity_id) {
            $this->flashSession->error('Missing required ID parameter for edit action!');
            return $this->redirect_back();
        }

        /**
         * @var $model \Baseapp\Models\Media
         * @var $entity \Baseapp\Models\Media
         */
        $model = $this->crud_model_class;
        $entity = $model::findFirst($entity_id);

        if (!$entity) {
            $this->flashSession->error(sprintf('Entity not found [Model: %s, ID: %s]', $this->crud_model_class, $entity_id));
            return $this->redirect_back();
        }

        if ($this->request->isPost()) {
            $saved = $entity->backend_save($this->request);
            if ($saved instanceof Media) {
                $this->flashSession->success('<strong>Changes saved successfully!</strong>');
                $save_action = $this->get_save_action();
                if ('save' === $save_action) {
                    return $this->redirect_self();
                } else {
                    return $this->redirect_to($this->get_next_url());
                }
            } else {
                $this->view->setVar('errors', $saved);
                $this->flashSession->error('<strong>Error!</strong> Fix fields that failed to validate.');
            }
        }

        $original_file_details = $entity->get_original_file_details();

        // Get a list of all the image styles for this entity
        if ($entity->is_styleable()) {
            $this->assets->addCss('assets/vendor/cropper/cropper.min.css');
            $this->assets->addJs('assets/vendor/cropper/cropper.min.js');
            $this->assets->addJs('assets/backend/js/media-cropper.js');

            $previews = $entity->get_all_thumbs();
            $this->view->setVar('previews', $previews);

            // Holds crop definitions for all the image styles of this media entity
            $crops = $entity->get_matching_crops();

            // Setup JSON for the image cropper
            $cropper_configs = array();
            foreach ($previews as $cnt => $preview) {
                $style = $preview->getStyle();
                $slug = $style['slug'];
                $cfg = new \stdClass();
                $cfg->multiple = false;
                $cfg->zoomable = false;
                $cfg->rotatable = false;
                $cfg->autoCropArea = 1;
                if ($style['crop']) {
                    $cfg->aspectRatio = $style['width'] / $style['height'];
                    $cfg->minWidth = $style['width'];
                    $cfg->minHeight = $style['height'];
                }
                if ($crops && isset($crops[$style['id']])) {
                    $cfg->data = new \stdClass();
                    $crop_definition = $crops[$style['id']];
                    $cfg->data->x = $crop_definition['x'];
                    $cfg->data->y = $crop_definition['y'];
                    $cfg->data->width = $crop_definition['width'];
                    $cfg->data->height = $crop_definition['height'];
                    // If we have a specific crop, unset autoCropArea since
                    // that would select the entire frame in some cases
                    unset($cfg->autoCropArea);
                }
                $cropper_configs[$slug] = $cfg;
            }
            $this->scripts['cropper-configs'] = 'var cropper_configs = ' . json_encode($cropper_configs);
        }

        $this->set_common_view_vars();

        $this->view->setVar('linked_content', $this->get_linked_content($entity));
        $this->view->setVar('form_title', $title);
        $this->view->setVar('entity', $entity);
        $this->view->setVar('entity_details', $original_file_details);
        $this->view->setVar('user_created', $entity->CreatedBy);
        $this->view->setVar('user_modified', $entity->ModifiedBy);
    }

    public function createAction()
    {
        $title = 'New media';
        $this->tag->setTitle($title);

        $this->assets->addCss('assets/vendor/jasny-bootstrap-3.1.3-dist/css/jasny-bootstrap.min.css');
        $this->assets->addJs('assets/vendor/jasny-bootstrap-3.1.3-dist/js/jasny-bootstrap.min.js');

        $this->view->pick('media/edit');  // Use the same form/view as editAction
        $entity = new Media();

        if ($this->request->isPost()) {
            $created = $entity->backend_create($this->request);
            if ($created instanceof Media) {
                $this->flashSession->success('<strong>New Media created successfully!</strong>');
                $save_action = $this->get_save_action();
                $next_url = $this->get_next_url();
                // Override next url to go to entity edit in save button case
                if ('save' === $save_action) {
                    $next_url = 'admin/media/edit/' . $created->id;
                }
                return $this->redirect_to($next_url);
            } else {
                $this->view->setVar('errors', $created);
                $this->flashSession->error('<strong>Error!</strong> Please fix any fields that failed to validate.');
            }
        }

        $this->view->setVar('form_title', $title);
        $this->view->setVar('entity', $entity);
    }

    /**
     * Builds an array representing grouped "linked" objects of the
     * specified Media record.
     *
     * It's an assoc array in the form of array('Ads' => array(), 'Articles' => array(), ... )
     *
     * Each group is assigned one or more arrays and each of those arrays represents
     * a specific linked object/content-item containing the keys: 'id', 'title', 'link', 'edit_link'...
     *
     * This way the view template code doesn't have to care about the specific
     * properties/attributes of each related model, it just loops over the data
     * returned from this method (which has identical keys regardless of the underlying model)
     *
     * @param Media $entity
     *
     * @return array
     */
    public function get_linked_content(Media $entity)
    {
        $linked = array();

        $current_uri = $this->request->getURI();

        // Populate any Ads belonging to this entity
        if (isset($entity->Ads) && $entity->Ads->valid()) {
            $linked['Ads'] = array();
            foreach ($entity->Ads as $related) {
                $ad = array();
                $ad['id'] = $related->Ad->id;
                $ad['ident'] = $related->Ad->ident();
                // TODO: properly implement a generic CRUD "link handler" thingy to a base model,
                // so that we can call something like $model->get_link('admin'); and $model->get_link('frontend');
                $ad['link'] = $this->url->get('ads/' . $related->Ad->id);
                $ad['edit_link'] = $this->url->get('admin/ads/edit/' . $related->Ad->id . '?next=' . rawurlencode($current_uri));
                $linked['Ads'][] = $ad;
            }
        }

        // Populate any Articles belonging to this entity
        if (isset($entity->Articles) && $entity->Articles->valid()) {
            $linked['Articles'] = array();
            foreach ($entity->Articles as $related) {
                $article = array();
                $article['id'] = $related->Article->id;
                $article['ident'] = $related->Article->ident();
                $article['link'] = $related->Article->get_frontend_view_link();
                $article['edit_link'] = $this->url->get('admin/cms-articles/edit/' . $article['id'] . '?next=' . rawurlencode($current_uri));
                $linked['Articles'][] = $article;
            }
        }

        return $linked;
    }

    public function browseAction($specific_ids = null)
    {
        $this->initJsonResponse();
/*
        if (!$this->request->isAjax()) {
            $this->response->setStatusCode(400, 'Bad Request');
            return $this->response;
        }
//*/
        $media_records = null;
        $media         = array();

        if (!empty($specific_ids)) {

            $media_model = new Media();
            $media_records = $media_model->get_multiple_ids($specific_ids);

        } else {

            // Do a query for whatever was specified
            $page = 1;
            if ($this->request->hasQuery('page')) {
                $page = $this->request->getQuery('page', 'int', 1);
                if ($page <= 0) {
                    $page = 1;
                }
            }

            $types = self::get_types_dropdown_data();
            $type = $this->request->getQuery('type', 'string', 'image');
            $type_valid = false;
            foreach ($types as $type_id => $type_data) {
                if ($type_data['slug'] === $type) {
                    $type = $type_id;
                    $type_valid = true;
                    break;
                }
            }
            if (!$type_valid) {
                $type = null;
            }

            $builder = $this->modelsManager->createBuilder()
                ->columns(array(
                    '*'
                ))
                ->from('Baseapp\Models\Media');
            if (null !== $type) {
                $builder->where('Baseapp\Models\Media.type = :type:', array('type' => $type));
            }

            $paginator = new QueryBuilder(array(
                'builder' => $builder,
                'limit' => $this->config->settingsBackend->pagination_items_per_page,
                'page' => $page
            ));
            $current_page = $paginator->getPaginate();
            if ($current_page && $current_page->items) {
                $media_records = $current_page->items;
                $pagination_links = Tool::paginationAdmin(
                    $current_page,
                    'admin/media',
                    'pagination',
                    $this->config->settingsBackend->pagination_count_out,
                    $this->config->settingsBackend->pagination_count_in
                );
            }

        }

        // Prepare almost the same json output (browse has pagination, specific ids don't)
        if (!empty($media_records)) {
            foreach ($media_records as $media_record) {
                $media_json = array(
                    'info' => $media_record->toArray()
                );
                $backend_thumb = $media_record->get_backend_thumb()->src;
                $media_json['info']['src'] = $backend_thumb;
                if ($item_thumbs = $media_record->get_all_thumbs()) {
                    $crops = array();
                    foreach ($item_thumbs as $item_thumb) {
                        $crops[] = array(
                            'dimensions' => $item_thumb->width . ' x ' . $item_thumb->height,
                            'src'        => $item_thumb->src
                        );
                    }
                    $media_json['crops'] = $crops;
                }

                $media[] = array(
                    'image'  => $media_record->get_url(),
                    'thumb'  => $backend_thumb,
                    'json'   => $media_json,
                );
            }
        }

        $json = array(
            'media' => $media,
        );

        if (isset($pagination_links)) {
            $json['pagination'] = $pagination_links;
        }

        $this->response->setJsonContent($json);

        return $this->response;
    }

    public function stylesAction($filename = null)
    {
        $this->initJsonResponse();
//*
        if (!$this->request->isAjax()) {
            $this->response->setStatusCode(400, 'Bad Request');
            return $this->response;
        }
//*/

        $file_url = trim($this->request->getQuery('url', 'string'));

        if (!empty($file_url)) {
            $media = Media::getEntityByThumb($file_url);
            if ($media) {
                $this->response->setJsonContent($media->get_all_thumbs());
            }
        }

        return $this->response;
    }

    protected function initJsonResponse()
    {
        $this->view->disable();
        $this->response = new Response();
        $this->response->setContentType('application/json', 'UTF-8');
    }

    /**
     * Read and output a file (using chunked reads, so large files should work too).
     * Remember to turn off output buffering before using this function (ob_end_flush()) if
     * you encounter memory errors or corrupted downloads.
     *
     * @param $filename
     * @param bool $return_bytes
     *
     * @return bool|int
     */
    private function readfile_chunked($filename, $return_bytes = true) {
        $chunk_size = 1 * (1024 * 1024); // how many bytes per chunk
        $cnt = 0;
        $handle = fopen($filename, 'rb');
        if ($handle === false) {
            return false;
        }
        while (!feof($handle)) {
            $buffer = fread($handle, $chunk_size);
            print $buffer;
            //ob_flush();
            //flush();
            $this->response->appendContent($buffer);
            if ($return_bytes) {
                $cnt += strlen($buffer);
            }
        }
        $status = fclose($handle);
        if ($return_bytes && $status) {
            // return num of bytes printed like readfile() does.
            return $cnt;
        }
        return $status;
    }
}
