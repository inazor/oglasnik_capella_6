<?php

namespace Baseapp\Backend\Controllers;

use Baseapp\Library\Utils;
use Baseapp\Extension\Tag;
use Baseapp\Library\Tool;
use Baseapp\Models\Categories;
use Baseapp\Models\Users;
use Baseapp\Models\UsersShops;
use Baseapp\Models\UsersShopsFeatured;
use Baseapp\Models\UsersShopsFeaturedHomepage;
use Baseapp\Traits\CrudActions;
use Baseapp\Traits\CrudHelpers;
use Baseapp\Traits\CommonAdActions;
use Phalcon\Dispatcher;
use Phalcon\Paginator\Adapter\QueryBuilder;

/**
 * Backend Shops Controller
 */
class ShopsController extends IndexController
{
    use CrudHelpers;
    use CrudActions;
    use CommonAdActions;

    /* @var \Baseapp\Models\UsersShops */
    public $crud_model_class = 'Baseapp\Models\UsersShops';

    protected $allowed_roles = array('admin', 'supersupport', 'support', 'sales', 'moderator');
    static $can_delete_roles = array('admin', 'sales');

    /**
     * Overridden default baseBackendController's beforeExecuteRoute so we can allow
     * specific action (deleteAction) to be executed by a reduced set of roles (different than
     * those that normally have access to this controller)
     */
    public function beforeExecuteRoute(Dispatcher $dispatcher)
    {
        // Overriding the list of allowed roles in case of deleteAction
        if ('delete' === $dispatcher->getActionName()) {
            $this->allowed_roles = self::$can_delete_roles;
        }

        parent::beforeExecuteRoute($dispatcher);
    }

    /**
     * Index Action
     */
    public function indexAction()
    {
        $this->tag->setTitle('Shops');

        $page = $this->getPageParam();
        $search_for = $this->processSearchParam();
        $mode = $this->processSearchMode();
        $dir = $this->processAscDesc('desc');
        $order_by = $this->processOrderBy('id');
        $limit = $this->processLimit();

        Tag::setDefaults(array(
            'q'        => $search_for,
            'mode'     => $mode,
            'dir'      => $dir,
            'order_by' => $order_by,
            'limit'    => $limit
        ), true);

        // Available search fields
        $available_fields = array(
            'id',
            'title',
            'subtitle',
            'user_id',
            'username',
            'slug',
            'about',
            'email',
            'web',
            'published_at',
            'expires_at',
            'created_at',
            'modified_at'
        );
        $date_fields = array(
            'created_at',
            'modified_at',
            'published_at',
            'expires_at'
        );

        $field = $this->processOptions('field', $available_fields, null);
        Tag::setDefault('field', $field);
        if (null === $field || empty($field)) {
            $field = 'all';
        }

        // Build the query
        $builder = $this->modelsManager->createBuilder()
            ->columns(array(
                's.id',
                's.title',
                's.user_id',
                's.published_at',
                's.expires_at',
                's.created_at',
                's.modified_at',
                'u.username AS username',
                'u.company_name',
                'u.banned',
                'u.remark',
                'u.active',
                '(SELECT COUNT(*) FROM Baseapp\Models\InfractionReports WHERE model_name LIKE \'%UsersShops\' AND model_pk_val = s.id AND resolved_at IS NULL) AS infraction_reports_count'
            ))
            ->addFrom('Baseapp\Models\UsersShops', 's')
            ->innerJoin('Baseapp\Models\Users', 's.user_id = u.id', 'u')
            ->where('s.id > 0');   // for query optimization because of GROUP BY usage

        // If a specific field is specified, search only that one, otherwise search across all the fields
        // (but only if there's an actual search term entered)
        $fields_to_search = (null === $field || 'all' === $field) ? $available_fields : array($field);
        $search_params = Utils::build_phalcon_search_sql_params($fields_to_search, $search_for, $mode);
        if (null !== $search_params && is_array($search_params)) {
            $conditions = array();
            $binds = array();
            foreach ($search_params as $fld => $val) {
                $field_bind_name = sprintf(':%s:', $fld);
                if (in_array($fld, $date_fields)) {
                    $conditions[] = 'DATE(FROM_UNIXTIME(s.' . $fld . ')) LIKE ' . $field_bind_name;
                } else {
                    if ('username' === $fld) {
                        $conditions[] = 'u.' . $fld . ' LIKE ' . $field_bind_name;
                    } else {
                        $conditions[] = 's.' . $fld . ' LIKE ' . $field_bind_name;
                    }
                }
                $binds[$fld] = $val;
            }
            $conditions = $this->changeSearchParamsLikeIntoEqualsWhenExactMode($conditions, $mode);
            $builder->andWhere(implode(' OR ', $conditions), $binds);
        }

        // $builder->groupBy('s.id');
        $builder->orderBy('s.' . $order_by . ' ' . strtoupper($dir));

        $paginator = new QueryBuilder(array('builder' => $builder, 'limit' => $limit, 'page' => $page));
        $current_page = $paginator->getPaginate();

        $current_page->items = $current_page->items->toArray();

        //echo Debug::dump($current_page->items);exit;

        // Augmenting $current_page->items with extra data to simplify view code
        $model = new UsersShops();
        $logged_in_user = $this->auth->get_user();
        $callback = function($record) use ($model, $logged_in_user) {
            $info = $model->buildBackendInfoLinksMarkupFromModelRow($record, $logged_in_user);
            if (is_array($record)) {
                $record['info'] = $info;
            } else {
                $record->info = $info;
            }
            return $record;
        };
        UsersShops::paginatorItemsCallableDecorator($current_page->items, $callback);

        $pagination_links = Tool::paginationAdmin(
            $current_page,
            'admin/shops',
            'pagination',
            $this->config->settingsBackend->pagination_count_out,
            $this->config->settingsBackend->pagination_count_in
        );

        $this->view->setVar('page', $current_page);
        $this->view->setVar('pagination_links', $pagination_links);
    }

    private function add_common_crud_assets()
    {
        $this->assets->addCss('assets/vendor/jquery-ui-1.11.2-custom/jquery-ui.min.css');
        $this->assets->addJs('assets/vendor/jquery-ui-1.11.2-custom/jquery-ui.min.js');

        $this->assets->addJs('assets/vendor/ckeditor/ckeditor.js');
        $this->assets->addJs('assets/vendor/ckeditor/adapters/jquery.js');

        $this->assets->addCss('assets/vendor/magicsuggest/magicsuggest-min.css');
        $this->assets->addJs('assets/vendor/magicsuggest/magicsuggest-min.js');

        $this->assets->addJs('assets/vendor/moment/moment.js');
        $this->assets->addJs('assets/vendor/moment/locale/hr.js');
        $this->assets->addCss('assets/vendor/bootstrap-datetimepicker/bootstrap-datetimepicker.css');
        $this->assets->addJs('assets/vendor/bootstrap-datetimepicker/bootstrap-datetimepicker.min.js');

        $this->assets->addCss('assets/vendor/jquery-file-upload/jquery.fileupload.css');
        $this->assets->addJs('assets/vendor/jquery-file-upload/jquery.iframe-transport.js');
        $this->assets->addJs('assets/vendor/jquery-file-upload/jquery.fileupload.js');

        $this->assets->addJs('assets/backend/js/shops-edit.js');
    }

    private function default_sorting_order_dropdown($shop)
    {
        if (isset($shop->default_sorting_order)) {
            Tag::setDefault('default_sorting_order', $shop->default_sorting_order);
        } else {
            Tag::setDefault('default_sorting_order', $this->config->adSorting->default_value);
        }

        $sorting_order_array = array();
        foreach ($this->config->adSorting->toArray() as $key => $value) {
            if ('default_value' !== $key) {
                $sorting_order_array[$key] = $value['name'];
            }
        }

        $default_sorting_order = Tag::select(array(
            'default_sorting_order',
            $sorting_order_array,
            'useEmpty' => false,
            'class' => 'form-control'
        ));

        $this->view->setVar('default_sorting_order_dropdown', $default_sorting_order);
    }

    /**
     * Create new shop action
     */
    public function createAction()
    {
        $this->tag->setTitle('New shop');
        $this->add_common_crud_assets();

        // Use the same form/view as editAction
        $this->view->pick('shops/edit');

        $form_title_long = 'New shop';
        $form_action = 'add';

        $shop = new UsersShops();
        if ($this->request->isPost()) {
            $created = $shop->backend_add_new($this->request);
            if ($created instanceof UsersShops) {
                $this->flashSession->success('<strong>Successful shop creation!</strong> ' . $created->getUrl('html'));
                $save_action = $this->get_save_action();
                $next_url = $this->get_next_url();
                // Override next url to go to entity edit in save button case
                if ('save' === $save_action) {
                    $next_url = 'admin/shops/edit/' . $created->id;
                }
                return $this->redirect_to($next_url);
            } else {
                $this->flashSession->error('<strong>Error!</strong> Fix all fields that failed to validate.');
                $this->view->setVar('errors', $created);
            }
        }

        $this->default_sorting_order_dropdown($shop);
        $this->view->setVar('user', $shop->getOwner());
        $this->view->setVar('shop', $shop);
        $this->view->setVar('form_title_long', $form_title_long);
        $this->view->setVar('form_action', $form_action);
    }

    /**
     * Edit existing shop action
     */
    public function editAction($entity_id = null)
    {
        $entity_id = (int) $entity_id;

        $model = $this->crud_model_class;
        /* @var $entity \Baseapp\Models\UsersShops */
        $entity = $model::findFirst($entity_id);

        if (!$entity) {
            $this->flashSession->error(sprintf('Entity not found [Model: %s, ID: %s]', $this->crud_model_class, $entity_id));
            return $this->redirect_back();
        }

        $this->tag->setTitle('Edit shop');
        $this->add_common_crud_assets();

        $form_title_long = 'Edit shop';
        $form_action = 'edit';

        if ($this->request->isPost()) {
            // Determine which save button was used
            $save_action = $this->get_save_action();
            $next_url = $this->get_next_url();
            $saved = $entity->backend_save_changes($this->request);
            if ($saved instanceof UsersShops) {
                $this->flashSession->success('<strong>Shop saved successfully!</strong> ' . $saved->getUrl('html'));
                if ('save' === $save_action) {
                    return $this->redirect_self();
                } else {
                    return $this->redirect_to($next_url);
                }
            } else {
                $this->view->setVar('errors', $saved);
                $this->flashSession->error('<strong>Error!</strong> Fix all fields that failed to validate.');
            }
        }

        $featured_shops = $entity->FeaturedShops;
        // $featured_shops = $entity->getShoppingWindows();

        $this->default_sorting_order_dropdown($entity);
        $this->view->setVar('user', $entity->getOwner());
        $this->view->setVar('shop', $entity);
        $this->view->setVar('infraction_reports', $entity->getInfractionReports($groupStatuses = true));
        $this->view->setVar('featured_shops', $featured_shops);
        $this->view->setVar('form_title_long', $form_title_long);
        $this->view->setVar('form_action', $form_action);
    }

    protected function buildCategoriesDropdown($dropdown_name, $selectables = array(), $selected = null, $options = array())
    {
        // Prevent invalid choices from being set as default
        if ($selected) {
            if (is_array($selected)) {
                $selected_array = array();
                foreach ($selected as $sel) {
                    if (array_key_exists($sel, $selectables)) {
                        $selected_array[] = $sel;
                    }
                }
                if (count($selected_array) == 0) {
                    $selected = null;
                } else {
                    $selected = $selected_array;
                }
            } else {
                if (!array_key_exists($selected, $selectables)) {
                    $selected = null;
                }
            }
        }

        $dropdown_id = $dropdown_name;

        if (isset($options['multiple'])) {
            $dropdown_name = $dropdown_name . '[]';
        }

        // Preselected value, if there's a valid one
        if ($selected) {
            if (!is_array($selected)) {
                Tag::setDefault($dropdown_name, $selected);
            }
        }

        // Default options
        $defaults = array(
            $dropdown_name,
            $selectables,
            'class' => 'form-control',
            'id'    => $dropdown_id
        );

        if ($selected) {
            if (is_array($selected)) {
                $selected = implode(',', $selected);
            }
            $defaults['data-preselect'] = $selected;
        }

        // Merge passed in $options
        $options = array_merge($defaults, $options);

        // Build the dropdown
        $category_dropdown = Tag::select($options);
        $this->view->setVar($dropdown_id . '_dropdown', $category_dropdown);
    }

    /**
     * Ajax action for getting users active ads
     *
     * @param int $user_id User's ID we want ads for
     *
     * @return \Phalcon\Http\Response JSON encoded response
     */
    public function usersActiveAdsAction($user_id = null, $term = null)
    {
        $this->disable_view();
        $this->response->setContentType('application/json', 'UTF-8');
        $response_array = array('status' => false);

        if (!$this->request->isAjax()) {
            $this->response->setStatusCode(400, 'Bad Request');
            $response_array['error'] = 'Bad request';
            $this->response->setJsonContent($response_array);
            return $this->response;
        }

        $user_id = (int) $user_id;
        if (!$user_id) {
            $response_array['msg'] = 'Unknown user id';
            $this->response->setJsonContent($response_array);
            return $this->response;
        }

        $user = Users::findFirst($user_id);
        if (!$user) {
            $response_array['msg'] = 'Unknown user';
            $this->response->setJsonContent($response_array);
            return $this->response;
        }

        $paginated_ads = $this->getPaginatedAds($user, 'active', 'admin/shops/usersActiveAds/' . $user->id, 10, $term);
        if ($paginated_ads && isset($paginated_ads['ads'])) {
            $ads_markup = array();
            foreach ($paginated_ads['ads'] as $ad) {
                $ads_markup[] = $this->simpleView->render(
                    'shops/ad-chunk',
                    array(
                        'ad'   => $ad,
                    )
                );
            }
            if (count($ads_markup)) {
                if ($paginated_ads['pagination']) {
                    $ads_markup[] = $paginated_ads['pagination'];
                }

                $response_array['status'] = true;
                $response_array['data'] = implode("\n", $ads_markup);
            }
        }

        $this->response->setJsonContent($response_array);
        return $this->response;
    }

    protected function handleViewDropdowns(UsersShopsFeatured $featured_shop)
    {
        $model       = new Categories();
        $tree        = $this->getDI()->get(Categories::MEMCACHED_KEY);
        $selectables = $model->getSelectablesFromTreeArray(
            $tree,
            '-',
            $with_root = false,
            $depth = null,
            $breadcrumbs = true
        );

        $main_category_id        = $featured_shop->getMainCategory();
        $additional_category_ids = $featured_shop->getAdditionalCategories();

        // Allowing posted categories to override fetched stat (otherwise they're not present/correct when errors happen etc.)
        $posted_categories = $featured_shop->get_categories_from_request($this->request);
        if (!empty($posted_categories)) {
            // First element is the main cat, any others are additional category ids
            $posted_categories_array = explode(',', $posted_categories);
            if (!empty($posted_categories_array)) {
                if (isset($posted_categories_array[0]) && !empty($posted_categories_array[0])) {
                    $main_category_id = array_shift($posted_categories_array);
                    // If there's still stuff in exploded array after shifting, set that as additional category ids
                    if (count($posted_categories_array)) {
                        $additional_category_ids = $posted_categories_array;
                    }
                }
            }
        }

        $this->buildCategoriesDropdown('main_category_id', $selectables, $main_category_id, array(
            'useEmpty'         => true,
            'emptyText'        => 'Choose category',
            'emptyValue'       => '',
            'data-live-search' => true
        ));

        // Filtering $selectables down to only those initially required (can be empty too)
        $children = array();
        if (!empty($additional_category_ids)) {
            foreach ($additional_category_ids as $child_cat_id) {
                if (isset($selectables[$child_cat_id])) {
                    $children[$child_cat_id] = $selectables[$child_cat_id];
                }
            }
        }
        $this->buildCategoriesDropdown('additional_category_ids', $children, $additional_category_ids, array(
            'multiple'         => 'multiple',
            'data-max-options' => 2
        ));

        $this->view->setVar('main_category_id', $main_category_id);
        $this->view->setVar('additional_category_ids', $additional_category_ids);

        // FIXME: $tree appears to be used in featured-edit.volt (and only in the .js part) for building the dropdowns in JS
        $this->view->setVar('tree', $tree);
    }

    /**
     * Create new featured shop box action
     */
    public function createFeaturedAction()
    {
        $params = $this->router->getParams();

        $shop_id = null;
        if (isset($params['shop_id'])) {
            $shop_id = (int) $params['shop_id'];
        }

        if (!$shop_id) {
            $this->flashSession->error('Missing entity id');
            return $this->redirect_back();
        }

        $model = $this->crud_model_class;
        /* @var $shop \Baseapp\Models\UsersShops */
        $shop = $model::findFirst($shop_id);

        if (!$shop) {
            $this->flashSession->error(sprintf('Entity not found [Model: %s, ID: %s]', $this->crud_model_class, $shop_id));
            return $this->redirect_back();
        }

        $title = 'New shopping window';
        $this->tag->setTitle($title);
        //$this->add_common_crud_assets();

        // Use the same form/view as editFeaturedAction
        $this->view->pick('shops/featured-edit');

        $form_title_long = $title;
        $form_action = 'add';

        $featured_shop = new UsersShopsFeatured();
        if ($this->request->isPost()) {
            $created = $featured_shop->backend_add_new($this->request);
            if ($created instanceof UsersShopsFeatured) {
                $this->flashSession->success('<strong>New shopping window created successfully!</strong>');
                $save_action = $this->get_save_action();
                $next_url = $this->get_next_url();
                // Override next url to go to entity edit in save button case
                if ('save' === $save_action) {
                    $next_url = 'admin/shops/' . $shop->id . '/editFeatured/' . $created->id;
                }
                return $this->redirect_to($next_url);
            } else {
                $this->flashSession->error('<strong>Error!</strong> Fix all fields that failed to validate.');
                $this->view->setVar('errors', $created);
            }
        }

        $user = $shop->getOwner();
        $resulting_array = $this->getPaginatedAds($user, 'active', 'admin/shops/usersActiveAds/' . $user->id, 10);

        if ($resulting_array && !$resulting_array['error'] && isset($resulting_array['ads']) && count($resulting_array['ads'])) {
            $this->view->setVar('user_has_ads', true);
            $this->view->setVar('users_active_ads', $resulting_array['ads']);
            $this->view->setVar('pagination_links', $resulting_array['pagination']);
        } else {
            $this->view->setVar('user_has_ads', false);
        }

        $this->handleViewDropdowns($featured_shop);

        $this->view->setVar('shop', $shop);
        $this->view->setVar('featured_shop', $featured_shop);
        $this->view->setVar('featured_shop_ads', $featured_shop->getAds());
        $this->view->setVar('form_title_long', $form_title_long);
        $this->view->setVar('form_action', $form_action);

        $this->assets->addJs('assets/vendor/moment/moment.js');
        $this->assets->addJs('assets/vendor/moment/locale/hr.js');
        $this->assets->addCss('assets/vendor/bootstrap-datetimepicker/bootstrap-datetimepicker.css');
        $this->assets->addJs('assets/vendor/bootstrap-datetimepicker/bootstrap-datetimepicker.min.js');
        $this->assets->addCss('assets/backend/css/featured-shops.css');
        $this->assets->addJs('assets/backend/js/featured-shop-edit.js');
    }

    /**
     * Edit existing featured shop's box action
     */
    public function editFeaturedAction()
    {
        $params = $this->router->getParams();

        $shop_id = null;
        if (isset($params['shop_id'])) {
            $shop_id = (int) $params['shop_id'];
        }

        if (!$shop_id) {
            $this->flashSession->error('Missing entity id');
            return $this->redirect_back();
        }

        $model = $this->crud_model_class;
        /* @var $shop \Baseapp\Models\UsersShops */
        $shop = $model::findFirst($shop_id);

        if (!$shop) {
            $this->flashSession->error(sprintf('Entity not found [Model: %s, ID: %s]', $this->crud_model_class, $shop_id));
            return $this->redirect_back();
        }

        $featured_shop_id = null;
        if (isset($params['featured_shop_id'])) {
            $featured_shop_id = (int) $params['featured_shop_id'];
        }

        if (!$featured_shop_id) {
            $this->flashSession->error('Missing entity id');
            return $this->redirect_back();
        }

        $featured_shop = UsersShopsFeatured::findFirst($featured_shop_id);

        if (!$featured_shop) {
            $this->flashSession->error(sprintf('Entity not found [Model: %s, ID: %s]', 'Baseapp\Models\UsersShops', $featured_shop_id));
            return $this->redirect_back();
        }

        $title = 'Edit shopping window';
        $this->tag->setTitle($title);
        //$this->add_common_crud_assets();

        // Use the same form/view as editFeaturedAction
        $this->view->pick('shops/featured-edit');

        $form_title_long = $title;
        $form_action = 'edit';

        if ($this->request->isPost()) {
            // Determine which save button was used
            $save_action = $this->get_save_action();
            $next_url = $this->get_next_url();
            $saved = $featured_shop->backend_save_changes($this->request);
            if ($saved instanceof UsersShopsFeatured) {
                $this->flashSession->success('<strong>Shopping window saved successfully!</strong>');
                if ('save' === $save_action) {
                    return $this->redirect_self();
                } else {
                    return $this->redirect_to($next_url);
                }
            } else {
                $this->view->setVar('errors', $saved);
                $this->flashSession->error('<strong>Error!</strong> Fix all fields that failed to validate.');
            }
        }

        $user = $shop->getOwner();
        $resulting_array = $this->getPaginatedAds($user, 'active', 'admin/shops/usersActiveAds/' . $user->id, 10);
        if ($resulting_array && !$resulting_array['error'] && isset($resulting_array['ads']) && count($resulting_array['ads'])) {
            $this->view->setVar('user_has_ads', true);
            $this->view->setVar('users_active_ads', $resulting_array['ads']);
            $this->view->setVar('pagination_links', $resulting_array['pagination']);
        } else {
            $this->view->setVar('user_has_ads', false);
        }

        $this->handleViewDropdowns($featured_shop);

        $this->view->setVar('shop', $shop);
        $this->view->setVar('featured_shop', $featured_shop);
        $this->view->setVar('featured_shop_ads', $featured_shop->getAds());
        $this->view->setVar('form_title_long', $form_title_long);
        $this->view->setVar('form_action', $form_action);

        $this->assets->addJs('assets/vendor/moment/moment.js');
        $this->assets->addJs('assets/vendor/moment/locale/hr.js');
        $this->assets->addCss('assets/vendor/bootstrap-datetimepicker/bootstrap-datetimepicker.css');
        $this->assets->addJs('assets/vendor/bootstrap-datetimepicker/bootstrap-datetimepicker.min.js');
        $this->assets->addCss('assets/backend/css/featured-shops.css');
        $this->assets->addJs('assets/backend/js/featured-shop-edit.js');
    }

    public function setHomepageFeaturedAction()
    {
        $params = $this->router->getParams();

        $shop_id = null;
        if (isset($params['shop_id'])) {
            $shop_id = (int) $params['shop_id'];
        }

        if (!$shop_id) {
            $this->flashSession->error('Missing entity id');
            return $this->redirect_back();
        }

        $model = $this->crud_model_class;
        /* @var $shop \Baseapp\Models\UsersShops */
        $shop = $model::findFirst($shop_id);

        if (!$shop) {
            $this->flashSession->error(sprintf('Entity not found [Model: %s, ID: %s]', $this->crud_model_class, $shop_id));
            return $this->redirect_back();
        }

        $featured_shop_id = null;
        if (isset($params['featured_shop_id'])) {
            $featured_shop_id = (int) $params['featured_shop_id'];
        }

        if (!$featured_shop_id) {
            $this->flashSession->error('Missing entity id');
            return $this->redirect_back();
        }

        $featured_shop = UsersShopsFeatured::findFirst($featured_shop_id);

        if (!$featured_shop) {
            $this->flashSession->error(sprintf('Entity not found [Model: %s, ID: %s]', 'Baseapp\Models\UsersShops', $featured_shop_id));
            return $this->redirect_back();
        }

        $result = UsersShopsFeaturedHomepage::insertOrUpdate($featured_shop);

        if ($result) {
            $this->flashSession->success('Shopping window successfully featured on homepage');
        } else {
            $this->flashSession->error(sprintf('Shopping window could not be featured on homepage [Model: %s, ID: %s]', 'Baseapp\Models\UsersShops', $featured_shop_id));
        }
        return $this->redirect_back();
    }

    public function removeHomepageFeaturedAction()
    {
        $params = $this->router->getParams();

        $shop_id = null;
        if (isset($params['shop_id'])) {
            $shop_id = (int) $params['shop_id'];
        }

        if (!$shop_id) {
            $this->flashSession->error('Missing entity id');
            return $this->redirect_back();
        }

        $model = $this->crud_model_class;
        /* @var $shop \Baseapp\Models\UsersShops */
        $shop = $model::findFirst($shop_id);

        if (!$shop) {
            $this->flashSession->error(sprintf('Entity not found [Model: %s, ID: %s]', $this->crud_model_class, $shop_id));
            return $this->redirect_back();
        }

        $featured_shop_id = null;
        if (isset($params['featured_shop_id'])) {
            $featured_shop_id = (int) $params['featured_shop_id'];
        }

        if (!$featured_shop_id) {
            $this->flashSession->error('Missing entity id');
            return $this->redirect_back();
        }

        $featured_shop = UsersShopsFeatured::findFirst($featured_shop_id);

        if (!$featured_shop) {
            $this->flashSession->error(sprintf('Entity not found [Model: %s, ID: %s]', 'Baseapp\Models\UsersShops', $featured_shop_id));
            return $this->redirect_back();
        }

        $result = UsersShopsFeaturedHomepage::deleteByUsersShopsFeaturedId($featured_shop_id);

        if ($result) {
            $this->flashSession->success('Shopping window successfully <b class="text-danger">deleted</b> from homepage featured list');
        } else {
            $this->flashSession->error(sprintf('Shopping window could not be deleted from homepage featured list [Model: %s, ID: %s]', 'Baseapp\Models\UsersShops', $featured_shop_id));
        }
        return $this->redirect_back();
    }

}
