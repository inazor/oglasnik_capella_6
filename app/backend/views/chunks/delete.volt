{# backend delete entity confirmation #}
<div class="row">
    <div class="col-lg-12 col-md-12">
        <h1 class="page-header">{{ is_soft_delete|default(false) ? 'Soft' : '' }}Delete resource</h1>
        {{ flashSession.output() }}
        {{ form(null, 'id' : 'frm_delete', 'method', 'post') }}
            {{ hiddenField('next') }}
            {{ hiddenField('entity_id') }}
            {{ hiddenField('_csrftoken') }}
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">You are about to <strong class="text-danger">{{ is_soft_delete ? 'SoftDelete' : 'irreversibly delete' }}</strong> the following resources:</h3>
                </div>
                <div class="panel-body">
                    <h4>{{ model }}</h4>
                    <ul class="list-group">
                    {% for entity in entities %}
                        <li class="list-group-item list-group-item-warning"><strong>{{ entity.ident() }}</strong></li>
                    {% endfor %}
                    </ul>
                    {% if fk_warning is defined %}
                        {{ partial('chunks/delete-warning-relationships') }}
                    {% endif %}
                    <p class="text-danger">Are you sure you want to continue?</p>
                </div>
            </div>
            <div>
                <button class="btn btn-primary btn-danger" type="submit" name="delete">Yes, I'm sure, {{ is_soft_delete|default(false) ? 'SoftDelete' : 'delete' }}!</button>
                <button class="btn btn-default" type="submit" name="cancel">No, I changed my mind...</button>
            </div>
        {{ endForm() }}
    </div>
</div>
