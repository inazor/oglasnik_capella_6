{% for item in items %}
<li class="nested-item-li{% if item.active == 0 %} disabled{% endif %} no-nest" data-id="{{ item.id }}" id="item_{{ item.id }}">
    {% set children = item.children %}
    {% set hasChildren = (count(children) > 0) %}
    <div class="nested-item-div{% if hasChildren %} with-children{% endif %}">
        <div class="nested-item-row">
            <div class="drag-me ui-sortable-handle"><span class="fa fa-arrows"></span></div>
            <div class="children{% if hasChildren %} clickable{% endif %}">
                <span class="toggle fa {% if hasChildren %}fa-plus-square-o{% else %}fa-angle-right{% endif %}"></span>
            </div>
            <div class="name-cat">
                <span class="name">{{ item.name }} [ID: {{ item.id }}]</span>
            </div>
            {% if available_item_actions is iterable %}
            <div class="actions-cat">
            {% for available_action in available_item_actions %}
                {{ linkTo([available_action['path'] ~ item.id, available_action['name']]) }}
                {% if not loop.last %} · {% endif %}
            {% endfor %}
            </div>
            {% endif %}
        </div>
    </div>
    {% if hasChildren %}
    {{ partial("chunks/nested-list", ['items': children, 'class': 'sublist', 'available_item_actions': available_item_actions]) }}
    {% endif %}
</li>
{% endfor %}
