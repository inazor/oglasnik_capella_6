<ul class="{{ class }}">
{{ partial("chunks/nested-list-items", ['items': items, 'available_item_actions': available_item_actions]) }}
</ul>
