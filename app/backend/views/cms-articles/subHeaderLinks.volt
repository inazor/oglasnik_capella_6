{# Aktualno Editor #}
<div class="row">
    <div class="col-lg-12 col-md-12">
        <h1 class="page-header">SubHeader Links</h1>
        {{ flashSession.output() }}
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title"><strong>Links</strong></h3>
            </div>
            <div class="panel-body">
                <div class="row">
                    <div class="col-md-12 col-lg-12">
                        {{ form(NULL, 'id' : 'frm-tpleditor', 'method' : 'post', 'class' : 'form-horizontal') }}
                        {{ hiddenField('_csrftoken') }}
                        {{ hiddenField(['code', 'value': textarea_contents])}}
                        {{ textArea(['textarea_contents', 'class': 'form-control', 'rows': '5', 'value': textarea_contents]) }}
                        <p class="help-block">
                            Keep in mind that the contents above is split with new lines and inserted in an &lt;UL&gt; list:<br>
                        </p>
                        <div class="margin-top-10">
                            <button class="btn btn-primary" type="submit" name="save">Save</button>
                        </div>
                        {{ endForm() }}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
