{# Admin Dictionary Form View #}
<div class="row">
    <div class="col-lg-12 col-md-12">
        <h1 class="page-header">Dictionaries <small>{{ form_title_long }}</small></h1>
        {{ flashSession.output() }}

        {{ form(NULL, 'id' : 'frm_dictionaries', 'method' : 'post', 'autocomplete' : 'off') }}
            {{ hiddenField('next') }}
            {{ hiddenField('_csrftoken') }}
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">Dictionary details</h3>
                </div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-4 col-lg-4">
                            <div class="form-group">
                                <label class="control-label" for="parent_id">Parent dictionary</label>
                                {{ parent_id_dropdown }}
                            </div>
                        </div>
                        <div class="col-md-4 col-lg-4">
                            {% set field = 'slug' %}
                            <div class="form-group{{ errors is defined and errors.filter(field) ? ' has-error' : '' }}">
                                <label class="control-label" for="{{ field }}">Match slug</label>
                                <input class="form-control" name="{{ field }}" id="{{ field }}" value="{{dictionary.slug | default('')}}" maxlength="64" />
                            {% if errors is defined and errors.filter(field) %}
                                <p class="help-block">{{ current(errors.filter(field)).getMessage() }}</p>
                            {% endif %}
                            </div>
                        </div>
                        <div class="col-md-4 col-lg-4">
                            {% set field = 'name' %}
                            <div class="form-group{{ errors is defined and errors.filter(field) ? ' has-error' : '' }}">
                                <label class="control-label" for="{{ field }}">Name</label>
                                <input class="form-control" name="{{ field }}" id="{{ field }}" value="{{dictionary.name | default('')}}" maxlength="64" />
                            {% if errors is defined and errors.filter(field) %}
                                <p class="help-block">{{ current(errors.filter(field)).getMessage() }}</p>
                            {% endif %}
                            </div>
                        </div>
                        <div class="col-md-12 col-lg-12">
                            <div class="checkbox">
                                <label><input type="checkbox" name="active"{% if dictionary.active %} checked="checked"{% endif %} value="1"> Active</label>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div>
                <button class="btn btn-primary" type="submit" name="save">{{ save_text }}</button>
                <button class="btn btn-primary" type="submit" name="saveexit">{{ save_text }} and exit</button>
                <button class="btn btn-default" type="submit" name="cancel">Cancel</button>
            </div>
        {{ endForm() }}
    </div>
</div>
