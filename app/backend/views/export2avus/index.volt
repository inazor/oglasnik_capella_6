{# Admin Category Listing/View #}
<div class="row">
    <div class="col-lg-12 col-md-12">
        <h1 class="page-header">Category mapping for Avus export</h1>
        {{ flashSession.output() }}
        <div>
            <button id="toggle-all" disabled="disabled" class="btn btn-default btn-xs">Toggle All</button>
        </div>
        {% if categories is defined %}
        <div class="nested-list-sortable">
            {% if available_item_actions is not defined %}
            {% set available_item_actions = [] %}
            {% endif %}
            {{ partial("chunks/nested-list-simple", ['items': categories, 'class': 'sortable ui-sortable', 'available_item_actions': available_item_actions]) }}
        </div>
        {% endif %}
    </div>
</div>
