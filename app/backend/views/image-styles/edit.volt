<div class="row">
    <div class="col-lg-12 col-md-12">
        <h1 class="page-header">{{ form_title }}</h1>
        {{ flashSession.output() }}
        {{ form(NULL, 'id' : 'frm_section', 'method' : 'post', 'autocomplete' : 'off') }}
            {{ hiddenField('next') }}
            {{ hiddenField('_csrftoken') }}
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h3 class="panel-title">Image Style details</h3>
                        </div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-md-6 col-lg-6">
                                    {% set field = 'slug' %}
                                    <div class="form-group{{ errors is defined and errors.filter(field) ? ' has-error' : '' }}">
                                        <label class="control-label" for="{{ field }}">Slug</label>
                                        <input class="form-control" name="{{ field }}" id="{{ field }}" value="{{ entity.slug|default('') }}" maxlength="50">
                                        {% if errors is defined and errors.filter(field) %}
                                            <p class="help-block">{{ current(errors.filter(field)).getMessage() }}</p>
                                        {% endif %}
                                    </div>
                                </div>
                                <div class="col-md-3 col-lg-3">
                                    {% set field = 'width' %}
                                    <div class="form-group{{ errors is defined and errors.filter(field) ? ' has-error' : '' }}">
                                        <label class="control-label" for="{{ field }}">Width (px)</label>
                                        <input type="number" min="1" max="9999" step="1" class="form-control" name="{{ field }}" id="{{ field }}" value="{{ entity.width|default('') }}">
                                        {% if errors is defined and errors.filter(field) %}
                                            <p class="help-block">{{ current(errors.filter(field)).getMessage() }}</p>
                                        {% endif %}
                                    </div>
                                </div>
                                <div class="col-md-3 col-lg-3">
                                    {% set field = 'height' %}
                                    <div class="form-group{{ errors is defined and errors.filter(field) ? ' has-error' : '' }}">
                                        <label class="control-label" for="{{ field }}">Height (px)</label>
                                        <input type="number" min="1" max="9999" step="1" class="form-control" name="{{ field }}" id="{{ field }}" value="{{ entity.height|default('') }}">
                                        {% if errors is defined and errors.filter(field) %}
                                            <p class="help-block">{{ current(errors.filter(field)).getMessage() }}</p>
                                        {% endif %}
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6 col-lg-6">
                                    <div class="row">
                                        <div class="col-md-6 col-lg-6">
                                            <div class="form-group">
                                                <div class="checkbox">
                                                    <label><input type="checkbox" name="crop"{% if entity.crop %} checked="checked"{% endif %} value="1"> Crop</label>
                                                    <p class="help-block">Hard-crop mode (resulting image will always have the specified dimensions)</p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6 col-lg-6">
                                            <div class="form-group">
                                                <div class="checkbox">
                                                    <label><input type="checkbox" name="watermark"{% if entity.watermark %} checked="checked"{% endif %} value="1"> Watermark</label>
                                                    <p class="help-block">Whether to apply default watermarking strategy to the resulting image</p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row entity-actions">
                <div class="col-lg-12">
                    {% if (not(entity.id is empty)) %}
                    <a href="{{ this.url.get('admin/image-styles/delete/' ~ entity.id ~ '?next=admin/image-styles') }}" class="btn btn-danger pull-right">Delete</a>
                    {% endif %}
                    <button class="btn btn-primary" type="submit" name="saveexit">Save and exit</button>
                    <button class="btn btn-primary" type="submit" name="save">Save</button>
                    <button class="btn btn-default" type="submit" name="cancel">Cancel</button>
                </div>
            </div>
        {{ endForm() }}
    </div>
</div>
