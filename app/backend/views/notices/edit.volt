{# Admin Company notices Form View #}
<div class="row">
    <div class="col-lg-12 col-md-12">
        <h1 class="page-header">Company notices <small>{{ form_title_long }}</small></h1>
        {{ flashSession.output() }}

        {{ form(NULL, 'id' : 'frm_notice', 'method' : 'post', 'autocomplete' : 'off') }}
            {{ hiddenField('next') }}
            {{ hiddenField('_csrftoken') }}
            <div class="row">
                <div class="col-lg-9 col-md-8">
                    <div class="row">
                        <div class="col-lg-12">
                            {% set field = 'title' %}
                            <div class="form-group{{ errors is defined and errors.filter(field) ? ' has-error' : '' }}">
                                <label class="control-label" for="{{ field }}">Title</label>
                                <input class="form-control" name="{{ field }}" id="{{ field }}" value="{{ notice.title|default('') }}" maxlength="256" />
                            {% if errors is defined and errors.filter(field) %}
                                <p class="help-block">{{ current(errors.filter(field)).getMessage() }}</p>
                            {% endif %}
                            </div>

                            {% set field = 'message' %}
                            <div class="form-group{{ errors is defined and errors.filter(field) ? ' has-error' : '' }}">
                                <label class="control-label" for="{{ field }}">Content</label>
                                <textarea class="form-control" name="{{ field }}" id="{{ field }}" rows="5" cols="40">{{ notice.message|default('') }}</textarea>
                            {% if errors is defined and errors.filter(field) %}
                                <p class="help-block">{{ current(errors.filter(field)).getMessage() }}</p>
                            {% endif %}
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-4">
                    {% set field = 'received_at' %}
                    <div class="form-group{{ errors is defined and errors.filter(field) ? ' has-error' : '' }}">
                        <label class="control-label" for="{{ field }}">Received at</label>
                        <input type='text' class="form-control" name="{{ field }}" id="{{ field }}" value="{{ notice.received_at|default(date('Y-m-d H:i')) }}" />
                    {% if errors is defined and errors.filter(field) %}
                        <p class="help-block">{{ current(errors.filter(field)).getMessage() }}</p>
                    {% else %}
                        <p class="help-block">Date when message should be received by users</p>
                    {% endif %}
                    </div>
                    <div class="checkbox">
                        <label><input type="checkbox" name="active"{% if notice.active %} checked="checked"{% endif %} value="1"> Active</label>
                    </div>
                    {% if notice.id is defined %}
                    <hr/>
                    <table class="table-condensed table-striped table-bordered media-details">
                        <tbody>
                            <tr>
                                <td>Created</td>
                                <td><span class="glyphicon glyphicon-time"></span> {{ notice.created_at }}</td>
                                <td class="text-nowrap">
                                    {%- if not user_created is null -%}
                                        {{ linkTo(['admin/user/edit/' ~ user_created.id, '<span class="glyphicon glyphicon-user"></span>' ~ user_created.username, 'class': 'text-nowrap']) }}
                                    {%- else -%}
                                        <span class="glyphicon glyphicon-user"></span> anonymous
                                    {%- endif -%}
                                </td>
                            </tr>
                            {% if notice.modified_at %}
                            <tr>
                                <td>Modified</td>
                                <td><span class="glyphicon glyphicon-time"></span> {{ notice.modified_at }}</td>
                                <td class="text-nowrap">
                                    {%- if not user_modified is null -%}
                                        {{ linkTo(['admin/user/edit/' ~ user_modified.id, '<span class="glyphicon glyphicon-user"></span>' ~ user_modified.username, 'class': 'text-nowrap']) }}
                                    {%- else -%}
                                        <span class="glyphicon glyphicon-user"></span> anonymous
                                    {%- endif -%}
                                </td>
                            </tr>
                            {% endif %}
                        </tbody>
                    </table>
                    {% endif %}
                </div>
            </div>
            <hr/>
            <div>
                <button class="btn btn-primary" type="submit" name="save">Save</button>
                <button class="btn btn-primary" type="submit" name="saveexit">Save and exit</button>
                <button class="btn btn-default" type="submit" name="cancel">Cancel</button>
            </div>
        {{ endForm() }}
    </div>
</div>
