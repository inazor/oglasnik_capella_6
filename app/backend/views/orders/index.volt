{# Orders Viewer #}
<div class="row">
    <div class="col-lg-12 col-md-12">
        <div>
            {% if auth.logged_in(['admin', 'finance']) %}
            <div class="pull-right text-right col-md-9 search-upload-wrapper">
                {% if import_warnings_download_href %}
                <div class="pull-right" style="margin-left:2em">
                    <a href="{{ import_warnings_download_href }}" class="btn btn-danger"><span class="fa fa-download"></span> Download import-warnings.csv</a>
                </div>
                {% endif %}
                {%- if errors is defined -%}
                    {%- set upload_errors = errors.filter('upload') -%}
                {%- else -%}
                    {% set upload_errors = [] %}
                {%- endif -%}
                {{ form(NULL, 'id' : 'orders-upload', 'action' : '/admin/orders/bank-xml-upload', 'method' : 'post', 'enctype' : 'multipart/form-data', 'autocomplete' : 'off', 'class' : 'form-inline orders-xml-upload') }}
                {{ hiddenField('_csrftoken') }}
                <div class="form-group{{ not(upload_errors is empty) ? ' has-error' : '' }}">
                    <div class="fileinput fileinput-new" data-provides="fileinput">
                        <span class="btn btn-info btn-file"><span class="fileinput-new"><span class="glyphicon glyphicon-upload"></span> Select file</span><span class="fileinput-exists"><span class="glyphicon glyphicon-edit"></span> Change file</span><input type="file" name="upload" id="upload"></span>
                        <span class="fileinput-filename-wrap">
                            <span class="fileinput-filename"></span>
                            <a href="#" class="close fileinput-exists" data-dismiss="fileinput"><span class="glyphicon glyphicon-remove"></span></a>
                        </span>
                    </div>
                    {%- for err in upload_errors -%}
                        <p class="help-block">{{ err.getMessage() }}</p>
                    {%- endfor -%}
                </div>
                <div class="form-group text-right">
                    <button class="btn btn-primary">Upload</button>
                </div>
                {{ endForm() }}
            </div>
            {% endif %}
            <h1 class="page-header">Orders</h1>
        </div>
        {{ flashSession.output() }}
        <form class="form-inline" role="form">
            {{ hiddenField('field') }}
            {{ hiddenField('mode') }}
            <div class="row">
                <div class="col-lg-4 col-sm-6">
                    <div class="input-group search-panel">
                        <div class="input-group-btn">
                            <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                                <span class="dropdown-label" id="search_field">All fields</span> <span class="caret"></span>
                            </button>
                            <ul data-param="#field" class="dropdown-menu" role="menu">
                                <li role="presentation" class="dropdown-header">Narrow down your search</li>
                                {%- for f in field_options -%}
                                    <li{% if f == field %} class="active"{% endif %}><a href="#{{ f }}">{{ f }}</a></li>
                                {%- endfor -%}
                                <li class="divider"></li>
                                <li><a href="#all">All fields</a></li>
                            </ul>
                        </div>
                        <input type="text" class="form-control" name="q" value="{{ q|escape_attr }}" placeholder="Search term(s)...">
                        <div class="input-group-btn">
                            <button type="submit" class="btn btn-primary"><span class="glyphicon glyphicon-search"></span></button>
                            <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                                <span class="caret"></span>
                                <span class="sr-only">Toggle Dropdown</span>
                            </button>
                            <ul data-param="#mode" class="dropdown-menu dropdown-menu-right" role="menu">
                                <li role="presentation" class="dropdown-header">Search mode</li>
                                {% for m, m_desc in mode_options %}
                                    <li{% if m == mode %} class="active"{% endif %}><a href="#{{ m }}">{{ m_desc }}</a></li>
                                {% endfor %}
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-lg-8 col-sm-6 text-right">
                    <select id="status" name="status" class="form-control" title="Order status">
                        {%- for value, title in status_options -%}
                            <option {% if value == status %} selected="selected" {% endif %}value="{{ value }}">{{ title }}</option>
                        {%- endfor -%}
                    </select>
                    <select id="order-by" name="order_by" class="form-control" title="Order by">
                        {%- for value, title in order_by_options -%}
                            <option {% if value == order_by %} selected="selected" {% endif %}value="{{ value }}">{{ title }}</option>
                        {%- endfor -%}
                    </select>
                    <select id="dir" name="dir" class="form-control" title="ASC/DESC">
                        {%- for value, title in dir_options -%}
                            <option {% if value == dir %} selected="selected" {% endif %}value="{{ value }}">{{ title }}</option>
                        {%- endfor -%}
                    </select>
                    <select id="limit" name="limit" class="form-control" title="Records per page">
                        {%- for value in limit_options -%}
                            <option {% if value == limit %} selected="selected" {% endif %}value="{{ value }}">{{ value }}</option>
                        {%- endfor -%}
                    </select>
                </div>
            </div>
        </form>
        <br>
        {% if (not(page.items is empty)) %}
        {% if auth.logged_in(['admin', 'finance']) %}
        <div class="pull-right"><p><a href="{{ json_download_href|escape }}"><span class="fa fa-fw fa-download"></span>Export current page as JSON</a></p></div>
        {% endif %}

        <table class="table table-striped table-hover master-detail">
            <tr>
                <th>Order <small class="text-muted">Details: <a id="toggle-all" href="javascript:void(0);">toggle all</a> / <a id="show-all" href="javascript:void(0);">show all</a> / <a id="hide-all" href="javascript:void(0);">hide all</a> / <span class="text-muted">(or click a certain row)</span></small></th>
                <th class="text-right">Total</th>
                <th>Status</th>
                <th class="text-right">Valid until</th>
                <th class="text-right">Created at</th>
                <th class="text-right">Modified at</th>
                <th>User</th>
            </tr>
            {% for order in page.items %}
                {% set row_class = styling_data[order['status']]['classname'] %}
                <tr id="order-{{ order['id'] }}" class="x{{ row_class }} master-row">
                    <td class="col-md-5">
                        ID: {{ order['id'] }} <small class="text-muted">PBO: {{ order['pbo'] }}</small> <small>{{ order['payment_method'] }}</small>
                    </td>
                    <td class="col-md-1 text-right">{{ order['total']|money(2) }}</td>
                    <td class="col-md-2">
                        <span class="label label-{{ row_class }}">{{ status_options[order['status']] }}</span>
                        {% if order['status'] == constant("Baseapp\Models\Orders::STATUS_NEW") %}
                            <div class="btn-group">
                                <button class="btn btn-default btn-xs dropdown-toggle" type="button" data-toggle="dropdown"><span class="glyphicon glyphicon-edit"><!--IE--></span> <span class="caret"></span></button>
                                <ul class="dropdown-menu" role="menu">
                                    <li class="bg-success">{{ linkTo(['admin/orders/complete/' ~ order['id'], '<span class="label label-success">Completed</span>', 'title': 'Mark as Completed', 'onclick':'return confirm("Are you sure? There is no undo!");']) }}</li>
                                </ul>
                            </div>
                        {% endif %}
                    </td>
                    <td class="col-md-1 text-right nowrap">{{ order['valid_until'] }}</td>
                    <td class="col-md-1 text-right nowrap">{{ order['created_at'] }}</td>
                    <td class="col-md-1 text-right nowrap">{{ order['modified_at'] }}</td>
                    <td class="col-md-1">
                        {% if order['user_id'] %}
                            {{ linkTo(['admin/users/edit/' ~ order['user_id'], order['username']]) }}
                        {% endif %}
                    </td>
                </tr>
                {% if (not(order['table_markup'] is empty)) %}
                <tr data-master="order-{{ order['id'] }}" class="x{{ row_class }} detail-row">
                    <td colspan="7">
                        {{ order['table_markup'] }}
                    </td>
                </tr>
                {% endif %}
            {% endfor %}
        </table>
        {% else %}
            <div class="no-results alert alert-success">
                <p class="text-center">No results found</p>
            </div>
        {% endif %}
        {% if pagination_links %}{{ pagination_links }}{% endif %}
    </div>
</div>
