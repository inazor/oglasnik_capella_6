{# Admin Shop Edit View #}
<div class="row">
    <div class="col-lg-12 col-md-12">
        <div>
            {% if not (shop.id is empty) %}
            <div class="pull-right">
                <a href="{{ url('trgovina/' ~ shop.slug) }}" class="btn btn-default" target="_blank"><span class="fa fa-eye fa-fw"></span> View</a>
            </div>
            {% endif %}
            <h1 class="page-header">Shops <small>{{ form_title_long }}</small></h1>
        </div>

        {{ flashSession.output() }}

        {% if auth.logged_in(['admin', 'supersupport', 'support', 'sales']) %}
        {{ form(NULL, 'id':'frm_shop', 'method':'post', 'autocomplete':'off') }}
            {{ hiddenField('_csrftoken') }}
            {{ hiddenField('next') }}
            <input type="hidden" name="action" value="{{ form_action }}">
        {% endif %}
            <div class="row">
                <div class="col-lg-9 col-md-8">

                    {% include('chunks/infraction-reports') %}

                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h3 class="panel-title">Shop details</h3>
                        </div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-md-12">
                                    {% set field = 'user_id' %}
                                    <div class="form-group{{ errors is defined and errors.filter(field) ? ' has-error' : '' }}">
                                        <label class="control-label" for="{{ field }}_input">User</label>
                                        {# User selector #}
                                        {%- if (not(user is empty)) -%}
                                            <input type="hidden" name="{{ field }}" id="{{ field }}" value="{{ user.id }}" data-username="{{ user.username }}" />
                                        {%- else -%}
                                            <input type="hidden" name="{{ field }}" id="{{ field }}" value="" data-username="" />
                                        {%- endif -%}
                                        <input type="text" class="form-control" id="{{ field }}_input" value="" />
                                        {%- if errors is defined and errors.filter(field) -%}
                                            <p class="help-block">{{- current(errors.filter(field)).getMessage() -}}</p>
                                        {%- endif -%}
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-8">
                                    {% set field = 'title' %}
                                    <div class="form-group{{ errors is defined and errors.filter(field) ? ' has-error' : '' }}">
                                        <label class="control-label" for="{{ field }}">Title</label>
                                        <input type="text" class="form-control" name="{{ field }}" id="{{ field }}" placeholder="Shop name" value="{{ (_POST[field] is defined ? _POST[field] : shop.readAttribute(field)) }}" />
                                        {% if errors is defined and errors.filter(field) %}
                                            <p class="help-block">{{ current(errors.filter(field)).getMessage() }}</p>
                                        {% endif %}
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    {% set field = 'subtitle' %}
                                    <div class="form-group{{ errors is defined and errors.filter(field) ? ' has-error' : '' }}">
                                        <label class="control-label" for="{{ field }}">Subtitle</label>
                                        <input type="text" class="form-control" name="{{ field }}" id="{{ field }}" placeholder="Shop's subtitle" value="{{ (_POST[field] is defined ? _POST[field] : shop.readAttribute(field)) }}" />
                                        {% if errors is defined and errors.filter(field) %}
                                            <p class="help-block">{{ current(errors.filter(field)).getMessage() }}</p>
                                        {% endif %}
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-8">
                                    {% set field = 'slug' %}
                                    <div class="form-group{{ errors is defined and errors.filter(field) ? ' has-error' : '' }}">
                                        <label class="control-label" for="{{ field }}">Slug</label>
                                        <div class="input-group">
                                            <span class="input-group-addon">{{ url('trgovina/') }}</span>
                                            <input type="text" class="form-control" name="{{ field }}" id="{{ field }}" placeholder="shop-url-slug" value="{{ (_POST[field] is defined ? _POST[field] : shop.readAttribute(field)) }}" />
                                        </div>
                                        {% if errors is defined and errors.filter(field) %}
                                            <p class="help-block">{{ current(errors.filter(field)).getMessage() }}</p>
                                        {% endif %}
                                    </div>
                                </div>
                                <div class="col-lg-4 col-md-4">
                                    {% set field = 'default_sorting_order' %}
                                    <div class="form-group{{ errors is defined and errors.filter(field) ? ' has-error' : '' }}">
                                        <label class="control-label">Default sort</label>
                                        {{ default_sorting_order_dropdown }}
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-4 col-md-4">
                                    {% set field = 'email' %}
                                    <div class="form-group{{ errors is defined and errors.filter(field) ? ' has-error' : '' }}">
                                        <label class="control-label" for="{{ field }}">Email</label>
                                        <input type="text" class="form-control" name="{{ field }}" id="{{ field }}" placeholder="someone@example.com" value="{{ (_POST[field] is defined ? _POST[field] : shop.readAttribute(field)) }}" />
                                    {% if errors is defined and errors.filter(field) %}
                                        <p class="help-block">{{ current(errors.filter(field)).getMessage() }}</p>
                                    {% endif %}
                                    </div>
                                </div>
                                <div class="col-lg-4 col-md-4">
                                    {% set field = 'web' %}
                                    <div class="form-group{{ errors is defined and errors.filter(field) ? ' has-error' : '' }}">
                                        <label class="control-label">Web</label>
                                        <input type="text" class="form-control" name="{{ field }}" id="{{ field }}" placeholder="http://www.example.com" value="{{ (_POST[field] is defined ? _POST[field] : shop.readAttribute(field)) }}" />
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-4 col-md-4">
                                    {% set field = 'published_at' %}
                                    <div class="form-group{{ errors is defined and errors.filter(field) ? ' has-error' : '' }}">
                                        <label class="control-label" for="{{ field }}">Published at</label>
                                        <div class='input-group date' id='published_at_datetimepicker'>
                                            <input type="text" class="form-control" name="{{ field }}" id="{{ field }}" placeholder="" value="{{ (_POST[field] is defined ? _POST[field] : (shop.readAttribute(field) ? date('d.m.Y H:i', shop.readAttribute(field)) : '')) }}" />
                                            <span class="input-group-addon">
                                                <span class="glyphicon glyphicon-calendar"></span>
                                            </span>
                                        </div>
                                    {% if errors is defined and errors.filter(field) %}
                                        <p class="help-block">{{ current(errors.filter(field)).getMessage() }}</p>
                                    {% endif %}
                                    </div>
                                </div>
                                <div class="col-lg-4 col-md-4">
                                    {% set field = 'expires_at' %}
                                    <div class="form-group{{ errors is defined and errors.filter(field) ? ' has-error' : '' }}">
                                        <label class="control-label" for="{{ field }}">Expires at</label>
                                        <div class='input-group date' id='expires_at_datetimepicker'>
                                            <input type="text" class="form-control" name="{{ field }}" id="{{ field }}" placeholder="" value="{{ (_POST[field] is defined ? _POST[field] : (shop.readAttribute(field) ? date('d.m.Y H:i', shop.readAttribute(field)) : '')) }}" />
                                            <span class="input-group-addon">
                                                <span class="glyphicon glyphicon-calendar"></span>
                                            </span>
                                        </div>
                                    {% if errors is defined and errors.filter(field) %}
                                        <p class="help-block">{{ current(errors.filter(field)).getMessage() }}</p>
                                    {% endif %}
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-12">
                                    {% set field = 'about' %}
                                    <div class="form-group{{ errors is defined and errors.filter(field) ? ' has-error' : '' }}">
                                        <label class="control-label" for="{{ field }}">About the shop</label>
                                        <textarea class="form-control" name="{{ field }}" id="{{ field }}" rows="5" cols="40">{{ (_POST[field] is defined ? _POST[field] : shop.readAttribute(field)) }}</textarea>
                                        {% if errors is defined and errors.filter(field) %}
                                            <p class="help-block">{{ current(errors.filter(field)).getMessage() }}</p>
                                        {% endif %}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-4">
                    {% if shop.id is defined and shop.id %}
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <div class="pull-right">
                                <a href="{{ url('admin/shops/' ~ shop.id ~ '/createFeatured') }}" class="btn btn-primary btn-xs"><span class="fa fa-plus fa-fw"></span> Add new</a>
                            </div>
                            <h3 class="panel-title">Shopping windows</h3>
                        </div>
                        <div class="panel-body">
                        {% if featured_shops is defined and featured_shops is iterable %}
                            {% for featured in featured_shops %}
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="panel panel-default">
                                        <div class="panel-body">
                                            <div class="pull-right action-btn">
                                                <a href="{{ url('admin/shops/' ~ shop.id ~ '/editFeatured/' ~ featured.id) }}" class="btn btn-info btn-xs"><span class="fa fa-edit"></span> Edit</a>
                                            </div>
                                            {% set status_array = featured.getStatusArray() %}
                                            <div class="status">
                                                Status: <span class="{{ status_array['class'] }}">{{ status_array['text'] }}</span>
                                            </div>
                                            <hr/>
                                            {% set timeframe = featured.getTimeframe() %}
                                            {% if timeframe %}
                                            <div class="timeframe">
                                                <small>Valid: <span>{{ timeframe }}</span></small>
                                            </div>
                                            {% endif %}
                                            {% set cat_data = featured.getCategoriesDisplayData() %}
                                            {% if not(cat_data is empty) %}
                                                {{ cat_data['markup'] }}
                                            {% endif %}
                                            {% if featured.active %}
                                            <hr/>
                                            {% set homepageFeatured = featured.is_featured_on_homepage() %}
                                            <div class="status">
                                                {% if homepageFeatured %}
                                                <b class="text-success">Homepage featured</b><br/><br/>
                                                <a href="{{ url('admin/shops/' ~ shop.id ~ '/removeHomepageFeatured/' ~ featured.id) }}" class="btn btn-danger btn-xs">Remove as homepage featured</a>
                                                {% else %}
                                                <span class="text-muted">Not featured on homepage</span><br/><br/>
                                                <a href="{{ url('admin/shops/' ~ shop.id ~ '/setHomepageFeatured/' ~ featured.id) }}" class="btn btn-success btn-xs">Set as homepage featured</a>
                                                {% endif %}
                                            </div>
                                            {% endif %}
                                        </div>
                                    </div>
                                </div>
                            </div>
                            {% else %}
                            <p class="text-danger">Shop has no windows yet!</p>
                            {% endfor %}
                        {% endif %}
                        </div>
                    </div>
                    {% endif %}
                </div>
            </div>
        {% if auth.logged_in(['admin', 'supersupport', 'support', 'sales']) %}
            <div>
                <button class="btn btn-primary" type="submit" name="save">Save</button>
                <button class="btn btn-primary" type="submit" name="saveexit">Save and exit</button>
                <button class="btn btn-default" type="submit" name="cancel">Cancel</button>
            </div>
        {{ endForm() }}
        {% else %}
            <div>
                <button class="btn btn-default" onclick="history.go(-1);">Back</button>
            </div>
        {% endif %}
    </div>
</div>
