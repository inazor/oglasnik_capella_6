{# Admin User Edit View #}
<div class="row">
    <div class="col-lg-12 col-md-12">
        <div>
            {% if show_impersonate_link %}
            <div class="pull-right">
                <a href="{{ url('admin/users/impersonate/' ~ user.id) }}" class="btn btn-default"><span class="fa fa-user-secret fa-fw"></span> Impersonate</a>
            </div>
            {% endif %}
            <h1 class="page-header">Users <small>{{ form_title_long }}</small></h1>
        </div>

        {{ flashSession.output() }}

        {% if auth.logged_in(['admin', 'supersupport', 'support', 'sales']) %}
        {{ form(NULL, 'id' : 'frm_register', 'method' : 'post', 'autocomplete' : 'off') }}
            {{ hiddenField('_csrftoken') }}
            {{ hiddenField('next') }}
            <input type="hidden" name="action" value="{{form_action}}">
        {% endif %}
        <div class="row">
            <div class="col-lg-9 col-md-9">

                {% include('chunks/infraction-reports') %}

                <div class="panel panel-default">
                    <div class="panel-heading">
                        {% if user.id is defined %}
                        <div class="pull-right">[ID: <b>{{ user.id ~ (user.import_id|default(false) ? '</b> / ImportID: <b>' ~ user.import_id : '') }}</b> ]</div>
                        {% endif %}
                        <h3 class="panel-title">User details</h3>
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-lg-3 col-md-3">
                                {% set field = 'username' %}
                                <div class="form-group{{ errors is defined and errors.filter(field) ? ' has-error' : '' }}">
                                    <label class="control-label" for="{{ field }}">Username</label>
                                    <input class="form-control" name="{{ field }}" id="{{ field }}" value="{{user.username | default('')}}" maxlength="24" />
                                {% if errors is defined and errors.filter(field) %}
                                    <p class="help-block">{{ current(errors.filter(field)).getMessage() }}</p>
                                {% endif %}
                                </div>
                            </div>
                            <div class="col-lg-3 col-md-4">
                                {% set field = 'email' %}
                                <div class="form-group{{ errors is defined and errors.filter(field) ? ' has-error' : '' }}">
                                    <label class="control-label" for="{{ field }}">Email address</label>
                                    <input class="form-control" name="{{ field }}" id="{{ field }}" value="{{user.email | default('')}}" maxlength="256">
                                {% if errors is defined and errors.filter(field) %}
                                    <p class="help-block">{{ current(errors.filter(field)).getMessage() }}</p>
                                {% else %}
                                    <p class="help-block">Users email address</p>
                                {% endif %}
                                </div>
                            </div>
                            <div class="col-lg-3 col-md-3">
                                {% set field = 'password' %}
                                <div class="form-group{{ errors is defined and errors.filter(field) ? ' has-error' : '' }}">
                                    <label class="control-label">Password</label>
                                    <input type="password" placeholder="Enter password" class="form-control" name="{{ field }}" id="{{ field }}" value="">
                                {% if errors is defined and errors.filter(field) %}
                                    <p class="help-block">{{ current(errors.filter(field)).getMessage() }}</p>
                                {% else %}
                                    <p class="help-block">Fill this field to change/set password</p>
                                {% endif %}
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-lg-12">
                                <div class="form-group">
                                    <label class="control-label">User type</label>
                                    <div>
                                        <label class="radio-inline"><input type="radio"{% if user.type == constant("Baseapp\Models\Users::TYPE_PERSONAL") %} checked="checked"{% endif %} value="{{ constant("Baseapp\Models\Users::TYPE_PERSONAL") }}" id="userType1" name="type"> Person</label>
                                        <label class="radio-inline"><input type="radio"{% if user.type == constant("Baseapp\Models\Users::TYPE_COMPANY") %} checked="checked"{% endif %} value="{{ constant("Baseapp\Models\Users::TYPE_COMPANY") }}" id="userType2" name="type"> Company</label>
                                    </div>
                                </div>
                            </div>

                            <div class="col-lg-12{% if user.type == constant("Baseapp\Models\Users::TYPE_COMPANY") %} companyData{% endif %}" id="userTypeDetails">
                                <div class="row">
                                    <div class="col-lg-3 col-md-3">
                                        <div class="form-group" data-company-label="Responsible person's first name" data-personal-label="First name">
                                            <label for="first_name" class="control-label">First name</label>
                                            <input placeholder="Persons first name" class="form-control" name="first_name" id="first_name" value="{{user.first_name | default('')}}" maxlength="64">
                                        </div>
                                    </div>
                                    <div class="col-lg-3 col-md-3">
                                        <div class="form-group" data-company-label="Responsible person's last name" data-personal-label="Last name">
                                            <label for="last_name" class="control-label">Last name</label>
                                            <input placeholder="Persons last name" class="form-control" name="last_name" id="last_name" value="{{user.last_name | default('')}}" maxlength="64">
                                        </div>
                                    </div>
                                    {% set field = 'company_name' %}
                                    <div class="col-lg-6 col-md-6 companyField{{ errors is defined and errors.filter(field) ? ' has-error' : '' }}">
                                        <div class="form-group">
                                            <label class="control-label">Company name</label>
                                            <input placeholder="Company name" class="form-control" name="{{ field }}" id="{{ field }}" value="{{user.company_name | default('')}}" maxlength="128">
                                        {% if errors is defined and errors.filter(field) %}
                                            <p class="help-block">{{ current(errors.filter(field)).getMessage() }}</p>
                                        {% endif %}
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-3 col-md-3">
                                        <div class="form-group">
                                            <label for="oib" class="control-label">OIB</label>
                                            <input placeholder="OIB (11 digits)" data-mask="00000000000" class="form-control" name="oib" id="oib" value="{{user.oib | default('')}}" maxlength="11">
                                        </div>
                                    </div>
                                    <div class="col-lg-3 col-md-3">
                                        <div class="form-group" data-company-label="Company registration date" data-personal-label="Birth date">
                                            <label for="birth_date" class="control-label">Birth date</label>
                                            <input placeholder="Choose date" class="form-control" name="birth_date" id="birth_date" value="{{ (user.birth_date ? date('d.m.Y', strtotime(user.birth_date)) : '') }}" maxlength="10">
                                        </div>
                                    </div>
                                    <div class="col-lg-3 col-md-3">
                                        <div class="form-group">
                                            <label for="phone1" class="control-label">Phone #1</label>
                                            <div class="input-group">
                                                <input placeholder="Enter phone number" class="form-control" name="phone1" id="phone1" value="{{user.phone1 | default('')}}" maxlength="32">
                                                <span class="input-group-addon">
                                                    <input type="checkbox" name="phone1_public"{% if user.phone1_public %} checked="checked"{% endif %} value="1">
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-3 col-md-3">
                                        <div class="form-group">
                                            <label for="phone2" class="control-label">Phone #2</label>
                                            <div class="input-group">
                                                <input placeholder="Enter phone number" class="form-control" name="phone2" id="phone2" value="{{user.phone2 | default('')}}" maxlength="32">
                                                <span class="input-group-addon">
                                                    <input type="checkbox" name="phone2_public"{% if user.phone2_public %} checked="checked"{% endif %} value="1">
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-6 col-lg-3">
                                <div class="form-group">
                                    <label class="control-label" for="country_id">Country</label>
                                    {{ country_dropdown }}
                                </div>
                            </div>
                            <div class="col-md-6 col-lg-3" id="county_div"{{ county_dropdown_hidden ? ' style="display:none"' : '' }}>
                                <div class="form-group">
                                    <label class="control-label">County</label>
                                    {{ county_dropdown }}
                                </div>
                            </div>
                            <div class="col-md-6 col-lg-3">
                                <div class="row">
                                    <div class="col-md-8 col-lg-8">
                                        <div class="form-group">
                                            <label class="control-label">City</label>
                                            <input placeholder="Enter text" class="form-control" name="city" id="city" value="{{user.city | default('')}}" maxlength="32">
                                        </div>
                                    </div>
                                    <div class="col-md-4 col-lg-4">
                                        <div class="form-group">
                                            <label class="control-label">ZIP</label>
                                            <input placeholder="Enter text" class="form-control" name="zip_code" id="zip_code" value="{{user.zip_code | default('')}}" maxlength="6">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6 col-lg-3">
                                <div class="form-group">
                                    <label class="control-label">Address</label>
                                    <input placeholder="Enter text" class="form-control" name="address" id="address" value="{{user.address | default('')}}" maxlength="150">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                {% if auth.logged_in(['admin']) %}
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">User Roles <small>(check all that should apply)</small></h3>
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-md-12 col-lg-12">
                                {% for role_name, role_description in roles %}
                                <div class="col-md-6 col-lg-6">
                                    <div class="checkbox">
                                        <label><input type="checkbox" name="roles[]"{% if role_name in selected_roles %} checked="checked"{% endif %} value="{{ role_name }}"> {{ role_name }} <small class="help-block">({{ role_description }})</small></label>
                                    </div>
                                </div>
                                {% endfor %}
                            </div>
                        </div>
                    </div>
                </div>
                {% endif %}
            </div>
            <div class="col-lg-3 col-md-3">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">Various settings</h3>
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-lg-6 col-md-6">
                                <div class="checkbox">
                                    <label><input type="checkbox" name="newsletter"{% if user.newsletter %} checked="checked"{% endif %} value="1"> Subscribed to newsletter</label>
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-6">
                                <div class="checkbox">
                                    <label><input type="checkbox" name="active"{% if user.active %} checked="checked"{% endif %} value="1"> {#Active (email is confirmed)#}Email is confirmed</label>
                                </div>
                            </div>
                            {% if form_action == 'add' and auth.logged_in(['admin', 'supersupport', 'support']) %}
                            <div class="col-lg-12">
                                <div class="checkbox">
                                    <label><input type="checkbox" name="send_email"{% if send_email %} checked="checked"{% endif %} value="1"> Send activation email</label>
                                </div>
                            </div>
                            {% endif %}
                            {% if show_activate_btn %}
                            <div class="col-lg-12">
                                <div class="checkbox">
                                    <label><input type="checkbox" name="activate_user" value="1"> Activate user (give 'login' role)</label>
                                </div>
                            </div>
                            {% endif %}
                            <div class="col-lg-12">
                                <div class="checkbox">
                                    <label><input data-toggle-target="#ban-reason-block" class="target-toggler" type="checkbox" name="banned"{% if user.banned %} checked="checked"{% endif %} value="1"> Banned</label>
                                </div>
                                {% set field = 'ban_reason' %}
                                <div id="ban-reason-block" class="form-group{{ errors is defined and errors.filter(field) ? ' has-error' : '' }}">
                                    <label class="control-label" for="{{ field }}">Reason (internal)</label>
                                    <textarea class="form-control" name="{{ field }}" id="{{ field }}" rows="4" cols="40">{{user.ban_reason | default('')}}</textarea>
                                    {% if errors is defined and errors.filter(field) %}
                                        <p class="help-block">{{ current(errors.filter(field)).getMessage() }}</p>
                                    {% endif %}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title"><span class="fa fa-warning fa-fw"></span> Remark (internal)</h3>
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-lg-12">
                                {% set field = 'remark' %}
                                <div class="form-group">
                                    <textarea class="form-control" name="{{ field }}" id="{{ field }}" rows="4" cols="40">{{ user.remark|default('') }}</textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        {% if auth.logged_in(['admin', 'supersupport', 'support', 'sales']) %}
        <div>
            <button class="btn btn-primary" type="submit" name="save">{{ form_action == 'add' ? 'Create' : 'Save' }}</button>
            {% if form_action == 'edit' %}<button class="btn btn-primary" type="submit" name="saveexit">Save and exit</button>{% endif %}
            <button class="btn btn-default" type="submit" name="cancel">Cancel</button>
        </div>
        {{ endForm() }}
        {% else %}
        <div>
            <button class="btn btn-default" onclick="history.go(-1);">Back</button>
        </div>
        {% endif %}
    </div>
</div>
