<?php


namespace Baseapp\Cli\Tasks;


use Baseapp\Console;
use Baseapp\Models\Users;

class AdminTask extends MainTask
{
    /**
     * Creates a new admin user with provided username and password
     */
    public function createAdminAction()
    {
        $params = $this->router->getParams();

        if (empty($params)) {
            Console::error('No required parameters provided');
        }

        if (!isset($params[0]) || empty($params[0])) {
            Console::error('Admin username not specified');
        }

        $username = $params[0];
        $pwd = isset($params[1]) ? $params[1] : \Phalcon\Text::random(\Phalcon\Text::RANDOM_ALNUM, 22);
        $email = isset($params[2]) ? $params[2] : $username . '@local.admin';

        // TODO: we really need to start using setPassword() at least...
        $new_user = new Users();
        $new_user->assignDefaults(array(
            'username' => $username,
            'password' => $this->getDI()->get('auth')->hash_password($pwd),
            'email' => $email
        ));

        try {
            $created = $new_user->create();
            if (true === $created) {
                // Add admin role
                $inserted = $new_user->insert_roles(array('admin', 'login'));
                if ($inserted) {
                    $msg = sprintf("\nAccount '%s' with password '%s' created, 'login' and 'admin' roles granted.\n", $username, $pwd);
                    print $msg;
                    return true;
                } else {
                    throw new \Exception('Failed granting roles for the new user account!');
                }
            } else {
                $messages = $new_user->getMessages();
                $msgs = array();
                foreach ($messages as $message) {
                    $msgs[] = $message->getMessage();
                }
                throw new \Exception(implode(', ', $msgs));
            }
        } catch (\Exception $e) {
            Console::error('Admin create failed: ' . $e->getMessage());
        }
    }

} 
