<?php

namespace Baseapp\Cli\Tasks;

use Baseapp\Console;
use Baseapp\Traits\AuditLog;

/**
 * Main Cli Task
 */
class MainTask extends \Phalcon\Cli\Task
{
    use AuditLog;

    /**
     * Initialize
     */
    public function initialize()
    {

    }

    /**
     * Main Action - shows a list of available tasks
     */
    public function mainAction()
    {
        echo "+---------------------+\n";
        echo "| Available CLI tasks |\n";
        echo "+---------------------+\n";
        foreach (new \DirectoryIterator(ROOT_PATH . '/app/cli/tasks') as $file) {
            if ($file->isDot() || 'MainTask' === $file->getBasename('.php')) {
                continue;
            }
            $task = $file->getBasename('.php');
            echo strtolower(strstr($task, 'Task', true)) . "\n";

            // Lists "executable" methods of files within the 'cli/tasks' folder
            $f = new \ReflectionClass(__NAMESPACE__ . '\\' . $task);
            foreach ($f->getMethods(\ReflectionMethod::IS_PUBLIC) as $m) {
                if ($m->class == __NAMESPACE__ . '\\' . $task && $m->name != 'initialize') {
                    echo "\t" . strstr($m->name, 'Action', true) . "\n";
                }
            }
        }
    }

    /**
     * Not found Action
     *
     * @param string|null $message
     */
    public function notFoundAction($message = null)
    {
        $msg = 'Task not found';

        if (null !== $message) {
            $msg .= ': ' . $message;
        }

        Console::error($msg);
    }

    /**
     * Handles exceptions forwarded from ErrorHandler component (which hooks into dispatch:beforeException)
     *
     * @param \Exception|null $exception
     */
    public function exceptionAction($exception = null)
    {
        if ($exception instanceof \Phalcon\Cli\Dispatcher\Exception) {
            $this->notFoundAction($exception->getMessage());
        } else {
            Console::exception($exception);
        }
    }
}
