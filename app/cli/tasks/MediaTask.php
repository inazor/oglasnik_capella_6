<?php

namespace Baseapp\Cli\Tasks;

use Baseapp\Models\ImageStyles;
use Baseapp\Models\Media;

class MediaTask extends MainTask
{
    public function fixGalleryBigImagesAction()
    {
        $slug = 'GalleryBig';

        $entity = ImageStyles::findFirstBySlug($slug);
        $entity->skipCheckingChangesBeforeUpdate();

        if ($entity) {
            echo "\nImageStyle `" . $slug . "` found, updating dimensions without deleting files...\n";
            $saved = $entity->update(array('width' => '570', 'height' => '520'));
            if ($saved) {
                echo "\nImageStyle `" . $slug . "` dimensions updated, now deleting files (this WILL TAKE A LONG TIME)...\n";
                $delete_result = (new Media())->delete_files_with_slug($slug);
                if ($delete_result) {
                    echo "\nDeleted files.\n";
                } else {
                    echo "\nNothing to delete.\n";
                }
            }
        }
    }

    public function updateListingMediumImagesAction()
    {
        $slug = 'ListingMedium';

        $entity = ImageStyles::findFirstBySlug($slug);
        $entity->skipCheckingChangesBeforeUpdate();

        if ($entity) {
            echo "\nImageStyle `" . $slug . "` found, updating dimensions without deleting files...\n";
            $saved = $entity->update(array('width' => '500', 'height' => '500', 'crop' => 1));
            if ($saved) {
                echo "\nImageStyle `" . $slug . "` dimensions updated, now deleting files (this WILL TAKE A LONG TIME)...\n";
                $delete_result = (new Media())->delete_files_with_slug($slug);
                if ($delete_result) {
                    echo "\nDeleted files.\n";
                } else {
                    echo "\nNothing to delete.\n";
                }
            }
        }
    }
}
