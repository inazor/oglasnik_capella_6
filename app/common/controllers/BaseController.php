<?php

namespace Baseapp\Controllers;

use Baseapp\Extension\Tag;
use Baseapp\Library\Debug;
use Baseapp\Library\Utils;
use Baseapp\Library\Uploader\Uploader;
use Baseapp\Traits\AuditLog;
use Phalcon\Validation\Message\Group;

class BaseController extends \Phalcon\Mvc\Controller
{
    use AuditLog;

    public $scripts = array();

    /**
     * Initializes the controller (executed first, before any action is executed on a controller)
     *
     * Method ‘initialize’ is only called if the event ‘beforeExecuteRoute’ is executed with success.
     */
    public function initialize()
    {
        // Prepare a _csrftoken variable for all the views that might want to use it (csrf protection)
        $this->setupCsrfTokenViewData();

        // Create a default empty 'head' collection
        $this->assets->collection('head');
    }

    protected function setupCsrfTokenViewData()
    {
        $_csrftoken = '';

        if ($this->csrfTokenGenerationWanted()) {
            $signer     = $this->getDI()->getShared('signer');
            $_csrftoken = $signer->getSignature();
        }

        // Setting the vars always (still, might be removed, but don't want to risk breaking something yet)
        Tag::setDefault('_csrftoken', $_csrftoken);
        $this->view->setVar('_csrftoken', $_csrftoken);
    }

    /**
     * @return bool
     */
    protected function csrfTokenGenerationWanted()
    {
        $wanted = true;

        // Resetting back to false only for user/signout for now
        $controller = $this->dispatcher->getControllerName();
        if ('user' === $controller) {
            $action = $this->dispatcher->getActionName();
            if ('signout' === $action) {
                $wanted = false;
            }
        }

        return $wanted;
    }

    /**
     * Forwards execution to ErrorController::show404() basically.
     * Remember to use `return $this->trigger_404()` inside a controller action (to prevent
     * any further controller (and possibly view) execution).
     *
     * @return mixed
     */
    public function trigger_404()
    {
        $params = array(
            'controller' => 'error',
            'action' => 'show404',
        );
        return $this->dispatcher->forward($params);
    }

    public function trigger_403()
    {
        $params = array(
            'controller' => 'error',
            'action' => 'show403',
        );
        return $this->dispatcher->forward($params);
    }

    public function do_204()
    {
        $this->disable_view();
        $this->response->setStatusCode(204, 'No Content');
        $this->response->setContent('');
    }

    public function do_401()
    {
        $this->disable_view();
        $this->response->setStatusCode(401, 'Unauthorized');
    }

    public function do_403()
    {
        $this->disable_view();
        $this->response->setStatusCode(403, 'Forbidden');
    }

    /**
     * Note that a redirection doesn’t disable the view component, so if there
     * is a view associated with the current action it will be executed anyway.
     * @link http://docs.phalconphp.com/en/latest/reference/response.html#making-redirections
     * You can disable the view from a controller by executing `$this->view->disable();` or
     * even better, calling `$this->disable_view();` which does it if the view is not
     * already disabled
     */
    public function disable_view()
    {
        if (!$this->view->isDisabled()) {
            $this->view->disable();
        }
    }

    /**
     * Redirects to site home/root.
     *
     * @return \Phalcon\Http\ResponseInterface
     */
    public function redirect_home()
    {
        $this->disable_view();
        return $this->response->redirect(null);
    }

    /**
     * Redirect to signin URL
     *
     * @param string $next Url where to redirect (relative, no starting slash) after successful signin
     *
     * @return \Phalcon\Http\ResponseInterface
     */
    public function redirect_signin($next = null)
    {
        $this->disable_view();
        $route = 'user/signin';

        // if no explicit "next" parameter is specified automatically set it to the current uri
        if (null === $next) {
            // $next = ltrim($this->router->getRewriteUri(), '/');
            $next = ltrim($this->request->getURI(), '/');
        }

        $route .= '?next=' . rawurlencode($next);
        return $this->response->redirect($route);
    }

    /**
     * Redirects back to where the user came from (or where he was specified to go
     * via "next" query param). If parsing those fails, we return him to the homepage.
     *
     * @return \Phalcon\Http\ResponseInterface
     */
    public function redirect_back()
    {
        $this->disable_view();
        $redirect_path = $this->get_redir_path_from_current_request();
        if ($redirect_path) {
            return $this->response->redirect($redirect_path);
        } else {
            return $this->redirect_home();
        }
    }

    /**
     * Helps build and return redirect responses from controller actions
     *
     * @param string $location Where to redirect
     * @param null|int $status_code Optional. If not specified, uses Phalcon's default (302)
     *
     * @return \Phalcon\Http\ResponseInterface
     */
    public function redirect_to($location, $status_code = null)
    {
        $this->disable_view();

        $location  = ltrim($location, '/');

        if (null !== $status_code) {
            $status_code = (int) $status_code;
        }

        if ($status_code) {
            $response = $this->response->redirect($location, null, $status_code);
        } else {
            $response = $this->response->redirect($location);
        }

        return $response;
    }

    public function redirect_external($url)
    {
        $this->disable_view();

        return $this->response->redirect($url, true);
    }

    public function redirect_self()
    {
        $this->disable_view();

        /**
         * Check for presence of 'next' POST parameter and maintain it
         * through the redirection (also overwrites any existing ?next get parameter
         * with the one from POST if it exists)
         */
        $uri = $this->request->getURI();
        if ($this->request->hasPost('next')) {
            $uri = Utils::add_query_arg(array('next' => $this->request->getPost('next')), $uri);
        }

        $path = $this->get_safe_redir_path($uri);

        return $this->response->redirect($path);
    }

    /**
     * Checks POST requests for presence and validity of the CSRF token.
     * You have to call it explicitly when required (for now at least)
     *
     * @throws \Baseapp\Extension\CsrfException
     */
    public function check_csrf()
    {
        // check CSRF token validity
        if ($this->request->isPost()) {
            $token = $this->request->getPost('_csrftoken');
            if (empty($token)) {
                throw new \Baseapp\Extension\CsrfException('Nedostaje CSRF token');
            }
            if (!$this->signer->validateSignature($token)) {
                throw new \Baseapp\Extension\CsrfException('Neispravan CSRF token');
            }
        }
    }

    /**
     * Returns a path component of a URL, if that URL's host matches
     * the host we're running on. Returns null if parsing the url fails,
     * which is treated as '/' in Phalcon. (at least when used in
     * a redirect() call)
     *
     * @param string $url URL from which to extract the path component
     *
     * @return string|null
     */
    public function get_safe_redir_path($url)
    {
        $url = trim($url);
        if (null === $url || empty($url)) {
            return null;
        }

        // suppressing warnings/notices since we've had some crap referrers from bots/malware extensions
        $parsed = @parse_url($url);

        // return null if parse failed
        if (!is_array($parsed) || empty($parsed)) {
            // return null if parse_url() failed
            $parsed = null;
        }

        // make sure the parsed host matches our own (if it's set)
        if ($parsed) {
            if (isset($parsed['host']) && !empty($parsed['host'])) {
                if ($parsed['host'] !== $this->request->getHttpHost()) {
                    $parsed = null;
                }
            }
        }

        // make sure to return the path component only for now,
        // taking into account that Phalcon's site root should be specified as 'null'
        if ($parsed) {
            if (isset($parsed['path'])) {
                if (empty($parsed['path'])) {
                    $parsed = null;
                } else {
                    // TODO: see if we should allow all query parameters or filter them
                    // and only allow some of them. For now, all are allowed.
                    $query = false;
                    if (isset($parsed['query'])) {
                        $query = $parsed['query'];
                    }
                    // If the path ends up being empty after removing a forward slash from the left,
                    // transform the result into a null, since that's how we say 'go to site root'.
                    $parsed = ltrim($parsed['path'], '/');
                    if (empty($parsed)) {
                        $parsed = null;
                    }
                    // If we had query params, use them
                    if ($parsed && $query) {
                        $parsed = $parsed . '?' . $query;
                    }
                }
            }
        }

        return $parsed;
    }

    /**
     * Inspects the current request and returns an appropriate
     * path/route that the client should be redirected to.
     *
     * Currently the order of checks is:
     * 1. If current request specifies a '?next' parameter, we use the 'next' parameter's value (if it's ok)
     * 2. If a referer header is present, use that (if it's ok)
     * 3. Use the fallback, if it's safe
     * 4. Return null if nothing else succeeded
     *
     * @param string|null $fallback The fallback route (in case when '?next' or
     *                              referer parsing fails, defaults to null, which means site root)
     *
     * @return mixed|null|string
     */
    public function get_redir_path_from_current_request($fallback = null) {
        $location = null;

        // If we have a ?next parameter in the $_REQUEST, use it, parse it,
        // and redirect to that
        if ($this->request->has('next')) {
            $location = rawurldecode($this->request->get('next'));
            $location = $this->get_safe_redir_path($location);
        }

        // If nothing found, try referer but skip it if it's
        // the same as the current request because it will loop
        if (!$location) {
            $referer = $this->request->getHTTPReferer();
            $location = $this->get_safe_redir_path($referer);
            $current = ltrim($this->request->getURI(), '/');
            if ($location === $current) {
                $location = null;
            }
        }

        // If still nothing found, set location to fallback
        if (!$location) {
            $location = $fallback;
        }

        // TODO: might need to check $fallback once more, but it's internal
        // so it should be fine, and we might want an escape hatch for some
        // special case when we need to redirect externally for some reason...
        return $location;
    }

    /**
     * Creates markup (<ul> with <li> for each error message) given a \Phalcon\Validation\Message\Group
     * Only includes the first error encountered per field (similar to the way we show only the first
     * error in volt forms) in order to not show all the validations that failed.
     * Used when more detailed error message lists are wanted in flashSession error messages.
     *
     * @param Group $messages
     *
     * @return string
     */
    public function buildErrorsListMarkupFromValidationMessages(Group $messages)
    {
        $errors_markup = 'Molimo ispravite uočene greške:';
        if (1 === count($messages)) {
            $errors_markup = 'Molimo ispravite uočenu grešku:';
        }

        // Filtering messages down to only one (first) for each encountered field, since
        // that's the way we display them in forms usually. Otherwise, since we're
        // add()-ing a ton of validators regardless of any previous validators on
        // the same field (and if they're valid or not), generated markup would contain
        // errors that really don't have to be there (like, '"field" is required, "field" is not valid format xy --
        // if the field is required, and it isn't present, don't even bother showing any other errors for it)
        $messages_by_field = array();
        foreach ($messages as $message) {
            $field = $message->getField();
            if (!isset($messages_by_field[$field])) {
                $messages_by_field[$field] = $message->getMessage();
            }
        }

        $errors = '';
        foreach ($messages_by_field as $field => $message) {
            $errors .= '<li>' . $message . '</li>';
        }
        $errors_markup .= '<ul>' . $errors . '</ul>';

        return $errors_markup;
    }

    /**
     * @param array $options
     *
     * @return Uploader
     */
    protected function createUploader($options = array(), $formFields = null)
    {
        $defaults = array(
            'upload_base' => ROOT_PATH . '/public/repository',
            'minsize' => 1000,
            'maxsize' => 204800
        );
        $options = array_merge($defaults, $options);

        /* @var Uploader $uploader */
        $uploader = $this->getDI()->get('uploader');

        if ('backend' === $this->router->getModuleName()) {
            $validator = new \Baseapp\Library\Uploader\Validator();
        } else {
            $validator = new \Baseapp\Library\Uploader\ValidatorCroatianMessages();
        }
        $uploader->setValidator($validator);
        $uploader->setRules([
            'directory'  => $options['upload_base'],
            'minsize'    => $options['minsize'], // bytes
            'maxsize'    => $options['maxsize'], // bytes
            'mimes'      => [
                // allowed mime types
                'image/gif',
                'image/jpeg',
                'image/pjpeg',
                'image/png',
            ],
            'extensions' => [
                // allowed extensions
                'gif',
                'jpeg',
                'jpg',
                'png',
            ]
        ]);
        $uploader->setFormFields($formFields);

        return $uploader;
    }

    protected function do204ifEmptyEmptyPostWithoutFiles()
    {
        $empty = false;
        if ($this->request->isPost() && !$this->request->hasFiles()) {
            $empty = true;
            $this->do_204();
        }

        return $empty;
    }
}
