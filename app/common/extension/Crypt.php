<?php

namespace Baseapp\Extension;

use Baseapp\Library\Utils;

/**
 * Overriding Phalcon's default Crypt class in order to be able to support
 * encrypting and decrypting arrays instead of just plain strings.
 *
 * @package Baseapp\Extension
 */
class Crypt extends \Phalcon\Crypt
{
    /*
    public function encrypt($text, $key = null)
    {
        $text = $this->maybe_serialize($text);

        return parent::encrypt($text, $key);
    }

    public function decrypt($text, $key = null)
    {
        $result = parent::decrypt($text, $key);
        $result = $this->maybe_unserialize($result);

        return $result;
    }
    */

    /**
     * @param array $encrypted_array
     * @param null $key
     * @param bool $safe
     *
     * @return array
     */
    public function decryptBase64Array($encrypted_array = array(), $key = null, $safe = false)
    {
        $decrypted = array();

        foreach ($encrypted_array as $idx => $value) {
            if (is_array($value)) {
                $decrypted[$idx] = $this->decryptBase64Array($value, $key, $safe);
            } else {
                $decrypted[$idx] = parent::decryptBase64($value, $key, $safe);
                // TODO: what if this ends up being decrypted into a serialized string?
                // It doesn't happen now, but it might in the future, if Phalcon changes something...
            }
        }

        return $decrypted;
    }

    /**
     * Overridden version of Phalcon\Crypt::decryptBase64 which takes into account our use-case
     * of using cookie encryption along with setting/getting cookies using cookie names with array notation.
     *
     * @param string $text
     * @param null $key
     * @param bool $safe
     *
     * @return array|string
     */
    public function decryptBase64($text, $key = null, $safe = false)
    {
        // https://github.com/phalcon/cphalcon/blob/master/phalcon/http/cookie.zep#L144
        // Values read directly from the $_COOKIE superglobal end up being sent
        // to Crypt::decryptBase64 -- in that case $text is really an array (instead of a string),
        // and we have to call decrypt on all the values of the array
        if (is_array($text)) {
            // Incoming data is an array of encrypted strings
            $value = $this->decryptBase64Array($text, $key, $safe);
        } else {
            // Incoming data is an encrypted strings
            $value = parent::decryptBase64($text, $key, $safe);

            // However, the decrypted data could've been decrypted into a serialized
            // string, which we need to unserialize in order to support encryptBase64() when
            // it's being used with arrays instead of strings (which is done inside Phalcon's
            // internals when doing Cookie::set())
            $value = $this->maybe_unserialize($value);
        }

        return $value;
    }

    public function encryptBase64($text, $key = null, $safe = false)
    {
        $text = $this->maybe_serialize($text);

        return parent::encryptBase64($text, $key, $safe);
    }

    public function maybe_serialize($data)
    {
        if (is_array($data)) {
            $data = serialize($data);
        }

        return $data;
    }

    public function maybe_unserialize($data)
    {
        // TODO: what if $data is an array of serialized data strings?

        if (Utils::is_serialized($data)) {
            $data = unserialize($data);
        }

        return $data;
    }

    /*
    public function maybe_encode($data)
    {
        if (is_array($data)) {
            $data = json_encode($data);
        }
    }

    public function maybe_decode($data)
    {
        if (Utils::is_json($data)) {
            $data = json_decode($data, true);
        }

        return $data;
    }
    */
}
