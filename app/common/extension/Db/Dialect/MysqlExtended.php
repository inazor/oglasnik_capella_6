<?php

namespace Baseapp\Extension\Db\Dialect;

/**
 * Phalcon\Db\Adapter\Dialect\MysqlExtended
 *
 * This is an extended MySQL dialect that introduces workarounds for some common MySQL-only functions like
 * searches based on FULLTEXT indexes and operations with date intervals. Since PHQL does not support these syntax
 * you can use it like this:
 * Setup:
 * ```
 * $di->set('db', function() use ($config) {
 *     return new \Phalcon\Db\Adapter\Pdo\Mysql(array(
 *         "host"         => $config->database->host,
 *         "username"     => $config->database->username,
 *         "password"     => $config->database->password,
 *         "dbname"       => $config->database->name,
 *         "dialectClass" => '\Phalcon\Db\Dialect\MysqlExtended'
 *     ));
 * });
 * ```
 * Usage:
 * ```
 * // SELECT `customers`.`created_at` - INTERVAL 7 DAY FROM `customers`
 * $data = $this->modelsManager->executeQuery(
 *     'SELECT created_at - DATE_INTERVAL(7, "DAY") FROM App\Models\Customers'
 * );
 *
 * // SELECT `customers`.`id`, `customers`.`name` FROM `customers` WHERE MATCH(`customers`.`name`, `customers`.`description`) AGAINST ("+CEO")
 * $data = $this->modelsManager->executeQuery(
 *     'SELECT id, name FROM App\Models\Customers WHERE FULLTEXT_MATCH(name, description, "+CEO")'
 * );
 *
 * // SELECT `customers`.`id`, `customers`.`name` FROM `customers` WHERE MATCH(`customers`.`name`, `customers`.`description`) AGAINST ("+CEO" IN BOOLEAN MODE)
 * $data = $this->modelsManager->executeQuery(
 *     'SELECT id, name FROM App\Models\Customers WHERE FULLTEXT_MATCH_BMODE(name, description, "+CEO")'
 * );
 * ```
 */
class MysqlExtended extends \Phalcon\Db\Dialect\Mysql
{
    /**
     * Transforms an intermediate representation of an expression into a MySQL valid expression
     *
     * @param array $expression
     * @param string $escapeChar
     * @param null $bindCounts
     *
     * @return string
     * @throws \Exception
     */
    // TODO: might have to be array $expression for Phalcon 2.x
    // TODO: why is $escapeChar not propagated to further $this->getSqlExpression() calls?
    public function getSqlExpression(array $expression, $escapeChar = null, $bindCounts = null)
    {
        if ('functionCall' === $expression['type']) {
            switch ($expression['name']) {
                case 'DATE_INTERVAL':
                    if (count($expression['arguments']) != 2) {
                        throw new \Exception('DATE_INTERVAL requires 2 parameters');
                    }
                    switch ($expression['arguments'][1]['value']) {
                        case "'DAY'":
                            return 'INTERVAL ' . $this->getSqlExpression($expression['arguments'][0]) . ' DAY';
                        case "'MONTH'":
                            return 'INTERVAL ' . $this->getSqlExpression($expression['arguments'][0]) . ' MONTH';
                        case "'YEAR'":
                            return 'INTERVAL ' . $this->getSqlExpression($expression['arguments'][0]) . ' YEAR';
                    }
                    break;
                case 'FULLTEXT_MATCH':
                    if (count($expression['arguments']) < 2) {
                        throw new \Exception('FULLTEXT_MATCH requires 2 parameters');
                    }
                    $arguments = array();
                    $length    = count($expression['arguments']) - 1;
                    for ($i = 0; $i < $length; $i++) {
                        $arguments[] = $this->getSqlExpression($expression['arguments'][$i]);
                    }

                    return 'MATCH(' . join(', ', $arguments) . ') AGAINST (' .
                    $this->getSqlExpression($expression['arguments'][$length]) . ')';
                case 'FULLTEXT_MATCH_BMODE':
                    if (count($expression['arguments']) < 2) {
                        throw new \Exception('FULLTEXT_MATCH requires 2 parameters');
                    }
                    $arguments = array();
                    $length    = count($expression['arguments']) - 1;
                    for ($i = 0; $i < $length; $i++) {
                        // $arguments[] = $this->getSqlExpression($expression['arguments'][$i], $escapeChar);
                        $arguments[] = $this->getSqlExpression($expression['arguments'][$i]);
                    }

                    return 'MATCH(' . join(', ', $arguments) . ') AGAINST (' .
                        $this->getSqlExpression($expression['arguments'][$length], $escapeChar) . ' IN BOOLEAN MODE)';
            }
        }

        return parent::getSqlExpression($expression, $escapeChar);
    }
}
