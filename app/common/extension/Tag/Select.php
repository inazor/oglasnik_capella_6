<?php

namespace Baseapp\Extension\Tag;

use Baseapp\Bootstrap;
use Baseapp\Extension\Tag;
use \Phalcon\Tag\Exception as PhTagException;
use \Phalcon\Tag\Select as PhTagSelect;

/**
 * Extends \Phalcon\Tag\Select to support additional stuff we'd like to have when generating
 * <option> and <select> tags (such as custom attributes support etc.)
 */
class Select extends PhTagSelect
{

    public static function selectField($parameters, $data = null)
    {
        if (!is_array($parameters)) {
            $params = array($parameters, $data);
        } else {
            $params = $parameters;
        }

        if (!isset($params[0]) && isset($params['id'])) {
            $params[0] = $params['id'];
        }
        $id = $params[0];

        // Automatically assign the id if the name is not an array
        if ($id{0} !== '[' && !isset($params['id'])) {
            $params['id'] = $id;
        }

        // name
        if (!isset($params['name'])) {
            $params['name'] = $id;
        }

        // value
        if (!isset($params['value'])) {
            $value = \Phalcon\Tag::getValue($id, $params);
        } else {
            $value = $params['value'];
            unset($params['value']);
        }

        /* check empty */
        $use_empty = false;
        $empty_value = '';
        $empty_text = '';
        if (isset($params['useEmpty'])) {
            // empty value
            if (!isset($params['emptyValue'])) {
                $empty_value = '';
            } else {
                $empty_value = $params['emptyValue'];
                unset($params['emptyValue']);
            }

            // empty text
            if (!isset($params['emptyText'])) {
                $empty_text = 'Choose...';
            } else {
                $empty_text = $params['emptyText'];
                unset($params['emptyText']);
            }

            $use_empty = $params['useEmpty'];
            unset($params['useEmpty']);
        }

        // check disabled list/callback
        $disabled = null;
        if (isset($params['disabled'])) {
            $disabled = $params['disabled'];
            unset($params['disabled']);
        }

        // generate code
        $code = '<select';
        if (is_array($params)) {
            foreach ($params as $key => $val) {
                if (!is_int($key) && !is_array($val)) {
                    $code .= ' ' . $key . '="' . $val . '"';
                }
            }
        }

        $code .= '>' . PHP_EOL;

        if ($use_empty) {
            // Create an empty value
            $code .= "\t" . '<option value="' . $empty_value . '">' . $empty_text . '</option>' . PHP_EOL;
        }

        if (isset($params[1])) {
            $options = $params[1];
        } else {
            $options = $data;
        }

        if (is_object($options)) {
            // Options is a Resultset
            if (!isset($params['using'])) {
                throw new PhTagException('The "using" parameter is required');
            } else {
                $using = $params['using'];
                if (!is_array($using) && !is_object($using)) {
                    throw new PhTagException('The "using" parameter should be an Array');
                }
            }

            // Create the SELECT's options from a resultset
            $code .= static::_optionsFromResultset($options, $using, $value, $disabled);
        } else {
            if (is_array($options)) {
                // Create the SELECT's options from an array
                $code .= static::_optionsFromArray($options, $value, $disabled);
            } else {
                throw new PhTagException('Invalid data provided to SELECT helper');
            }
        }

        $code .= '</select>';

        return $code;
    }

    /**
     * Builds HTML option elements from a Resultset.
     *
     * Note that since the Resultset is almost completely read-only (the Traversable interface does not maintain
     * state in order to achieve lower memory usage), we cannot easily implement additional attributes handling here.
     * (unless we _maybe_ extend the Resultset class itself, force Models to use our own Resultset implementation
     * and then also attach special model data in model's afterFetch hooks -- then we could maybe look for that
     * data here, in a similar way to what we do for arrays)
     * Since we should be switching any NestedTree-related stuff to the $tree_array approach anyway, we're
     * probably not going to miss not having attributes support for Resultset. Time will tell.
     *
     * @param \Phalcon\Mvc\Model\Resultset $resultset
     * @param array|object $using
     * @param mixed $value
     * @param null|array|\Closure $disabled List of disabled select items or a \Closure that should return a boolean if an element is to be disabled or not
     *
     * @return string|void
     * @throws \Phalcon\Tag\Exception
     */
    protected static function _optionsFromResultset($resultset, $using, $value, $disabled = null)
    {
        /* Type check */
        if (!is_object($resultset) ||
            $resultset instanceof \Phalcon\Mvc\Model\Resultset === false) {
            throw new PhTagException('Invalid parameter type ($resultset).');
        }

        if (!is_array($using) && !is_object($using)) {
            throw new PhTagException('Invalid parameter type ($using).');
        }

        $code = '';
        $params = null;

        /* Loop through resultset */
        $resultset->rewind();
        while ($resultset->valid()) {
            $option = $resultset->current();

            if (is_array($using)) {

                $using_zero = $using[0];
                $using_one = $using[1];
/*
                if (isset($using[2])) {
                    $using_two = $using[2];
                }

                $option_attrs = array();
*/

                if (is_object($option)) {
                    /* Object */
                    if (method_exists($option, 'readAttribute')) {
                        // Read the value attribute from the model
                        $option_value = $option->readAttribute($using_zero);

                        // Read the text attribute from the model
                        $option_text = $option->readAttribute($using_one);
/*
                        // Read additional option attributes from the model
                        if (isset($using_two)) {
                            $option_attrs = $option->readAttribute($using_two);
                        }
*/
                    } else {
                        // Read the value directly from the model/object
                        $option_value = $option->$using_zero;

                        // Read the text directly from the model/object
                        $option_text = $option->$using_one;
/*
                        // Read additional option attributes from the model
                        if (isset($using_two) && isset($option->$using_two)) {
                            $option_attrs = $option->$using_two;
                        }
*/
                    }
                } elseif (is_array($option)) {
                    /* Array */

                    // Read the value directly from the array
                    $option_value = $option[$using_zero];

                    // Read the text directly from the array
                    $option_text = $option[$using_one];
/*
                    // Read the extra option attributes from the array
                    if (isset($using_two) && isset($option[$using_two])) {
                        $option_attrs = $option[$using_two];
                    }
*/
                } else {
                    throw new PhTagException('Resultset returned an invalid value');
                }

                $option_value = htmlspecialchars($option_value);

                $tag_name = 'option';
                $tag_params = array();
/*
                // populate additional attributes if present
                if (!empty($option_attrs)) {
                    foreach ($option_attrs as $attr_name => $attr_value) {
                        // $tag_params[$attr->getName()] = $attr->getValue();
                        $tag_params[$attr_name] = $attr_value;
                    }
                }
*/
                $tag_params['value'] = $option_value;

                // If the current value is equal to the option's value we mark it as selected
                $selected = self::should_select($option_value, $value);
                if ($selected) {
                    $tag_params['selected'] = 'selected';
                }

                // check if current option should be disabled or have any additional parameters set
                $is_disabled = self::should_disable($disabled, $option_value);
                if ($is_disabled) {
                    $tag_params['disabled'] = 'disabled';
                }

                $code .= "\t" . Tag::tagHtml($tag_name, $tag_params, false, true, false);
                $code .= $option_text;
                $code .= Tag::tagHtmlClose($tag_name, true);

            } elseif (is_object($using) && $using instanceof \Closure) {
                // Check if using a closure
                if (null === $params) {
                    $params = array();
                }
                $params[0] = $option;
                $code .= call_user_func_array($using, $params);
            }

            $resultset->next();
        }

        return $code;
    }

    /**
     * Generate the OPTION tags based on an array
     *
     * @param array $data
     * @param mixed $value
     * @param null|array|\Closure $disabled
     *
     * @return string|void
     * @throws \Phalcon\Tag\Exception
     */
    protected static function _optionsFromArray($data, $value, $disabled = null)
    {
        if (!is_array($data)) {
            throw new PhTagException('Invalid parameter type.');
        }

        /* Loop through data */
        $code = '';

        if (strpos($value, ',')) {
            $value = explode(',', $value);
        }

        foreach ($data as $option_value => $option_data) {
            $option_value = htmlspecialchars($option_value);

            $tag_params = array();

            // back-compat
            $option_text = $option_data;

            /**
             * Populates any additional attributes that were specified in case
             * $option_data is our specially formatted array in which
             * the actual data is stored in the 'text' key and any attributes
             * in the 'attributes' key
             */
            if (is_array($option_data)) {
                // Go through attributes if present
                if (isset($option_data['attributes']) && !empty($option_data['attributes'])) {
                    foreach ($option_data['attributes'] as $attr_name => $attr_value) {
                        // $tag_params[$attr_name] = Utils::esc_html($attr_value);
                        $tag_params[$attr_name] = $attr_value;
                    }
                }
                // Set $option_text in a back-compat way (if present in our new array format)
                if (isset($option_data['text'])) {
                    $option_text = $option_data['text'];
                }
            }

            // Checking if disabled attribute should be set

            // TODO/FIXME: in optgroup's case there's no real need to
            // further check any optgroup options since the entire group is disabled by the browser?
            $is_disabled = self::should_disable($disabled, $option_value);
            if ($is_disabled) {
                $tag_params['disabled'] = 'disabled';
            }

            /**
             * $option_text is overridden above if we detected our special 'attributes' usage
             * so this keeps on working for 'plain' arrays which should generate an <optgroup>
             * which can in turn contain data wih 'attributes' etc. See tests for examples of usage.
             */
            if (is_array($option_text)) {
                $tag_name = 'optgroup';
                $tag_params['label'] = $option_value;

                $code .= "\t" . Tag::tagHtml($tag_name, $tag_params, false, false, true);
                $code .= self::_optionsFromArray($option_text, $value, $disabled);
                $code .= "\t" . Tag::tagHtmlClose($tag_name, true);
            } else {
                $tag_name = 'option';

                $tag_params['value'] = $option_value;

                // If the current value is equal to the option's value we mark it as selected
                $selected = self::should_select($option_value, $value);
                if ($selected) {
                    $tag_params['selected'] = 'selected';
                }

                $code .= "\t" . Tag::tagHtml($tag_name, $tag_params, false, true, false);
                $code .= $option_text;
                $code .= Tag::tagHtmlClose($tag_name, true);
            }
        }

        return $code;
    }

    /**
     * Determines if the current <option> value should be disabled or not.
     *
     * @param array|object|\Closure $disabled List of disabled values, or a closure that's passed the current value and
     *                                        and which should return a boolean if the option should be disabled or not
     * @param mixed $value The current <option> value to check
     *
     * @return bool
     */
    protected static function should_disable($disabled, $value)
    {
        $is_disabled = false;
        if (!empty($disabled)) {
            if (is_array($disabled)) {
                if (in_array($value, $disabled)) {
                    $is_disabled = true;
                }
            }
            // Closure support
            if (is_object($disabled) && $disabled instanceof \Closure) {
                $is_disabled = (bool) call_user_func($disabled, $value);
            }
        }

        return $is_disabled;
    }

    /**
     * Returns true if the current <option> value should be set as selected (it matches $value).
     *
     * @param mixed $option_value
     * @param mixed|array $value
     *
     * @return bool
     */
    protected static function should_select($option_value, $value)
    {
        $selected = false;

        if (is_array($value)) {
            if (in_array($option_value, $value)) {
                $selected = true;
            }
        } else {
            if ($option_value == $value) {
                $selected = true;
            }
        }

        return $selected;
    }

}
