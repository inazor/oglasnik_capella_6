<?php

namespace Baseapp\Library;

use Baseapp\Models\AdsTotalsCache;
use Phalcon\Cache\Exception as PhCacheException;
use Phalcon\Mvc\Model;
use Phalcon\Mvc\Model\Query\Builder;
use Phalcon\Mvc\User\Component;
use Baseapp\Bootstrap;
use Baseapp\Extension\Cache\Backend\MyMemcache;
use Baseapp\Extension\Tag;
use Baseapp\Models\Ads;
use Baseapp\Models\Categories;
use Baseapp\Models\Users;
use Baseapp\Models\SearchQueries;
use Baseapp\Traits\CategoryTreeHelpers;
use Baseapp\Traits\RandomSpecialProductsHelpers;

class AdsFiltering extends Component
{
    use CategoryTreeHelpers;
    use RandomSpecialProductsHelpers;

    protected $memcached = null;

    /* @var $adCategory \Baseapp\Models\Categories */
    protected $categoryTree = null;
    protected $adCategory = null;
    protected $adCategoriesIDs = array();
    protected $getChildCategoriesAsArray = false;
    protected $alwaysShowCompleteCategoriesTree = false;
    protected $sectionFiltering = false;

    /* @var $adUser \Baseapp\Models\Users */
    protected $adUser = null;

    /* @var $loggedInUser \Baseapp\Models\Users */
    protected $loggedInUser = null;

    protected $searchTerm           = null;
    protected $searchTermRaw        = null;
    protected $searchTermUnaccented = null;

    protected $page = 1;
    protected $itemsPerPage = null;
    protected $includeChildCategories = true;
    protected $updateSearchTermDetails = false;
    protected $sortParamData = null;
    protected $maintain_sort_param = false;
    protected $showOnlyRootCategoriesCounts = false;
    protected $baseURL = null;
    protected $allowSpecialProducts = false;

    protected $filters = array();
    protected $presetFiltersData = null;
    protected $autodetectFiltersFromCategory = false;
    protected $categoryFiltersData = null;

    protected $flashSessionType = null;
    protected $flashSessionMsg  = null;

    protected $pageSpecificJS = null;
    protected $pageAssetsJS = null;
    protected $pageAssetsCSS = null;
    protected $adSubmitCategory = null;
    protected $showEmailAgentButton = false;

    protected $gpsBounds = null;

    protected $error           = false;
    protected $trigger404      = false;
    protected $specialProducts = null;
    protected $ads             = null;
    protected $totalAdsCount   = 0;
    protected $pagination      = null;

    protected $currentPage = null;
    protected $generatedViewVars = array();

    protected $sqlBuilder = null;
    protected $sqlBuilderNoProducts = null;
    protected $sqlBuilderAllCategoriesCount = null;
    protected $queryData = null;
    protected $cacheResults = true;
    protected $cacheLifeTime = 300;
    protected $cachePrefix = '';

    protected $dbHit = false;

    /**
     * Set to true if the query/request resulted with "filters" being called/used as a result of
     * a visitor action.
     *
     * Category listings have the need to get that info back without relying
     * on just checking the request query string. That way only "raw/default" category
     * listing counts (and potentially custom landing pages) can be properly cached using AdsTotalsCache,
     * while still supporting requests with various ?utm parameters or similar... Also, when someone
     * just sorts the default listing results differently (and that request also touches
     * the database), there's no reason we can't also cache that count in AdsTotalsCache too.
     *
     * @var bool
     */
    protected $filteredBecauseOfUserAction = false;

    public function __construct($page = null)
    {
        $this->memcached    = Ads::getMemcache();
        $this->categoryTree = $this->getDI()->get(Categories::MEMCACHED_KEY);

        if ($page) {
            $this->setPage($page);
        } else {
            if ($this->request->hasQuery('page')) {
                $this->setPage($this->request->getQuery('page', 'int', 1));
            }
        }
        $this->itemsPerPage = $this->config->settingsFrontend->pagination_items_per_page;
    }

    /**
     * @return bool
     */
    public function wasFilteredBecauseOfUserAction()
    {
        return $this->filteredBecauseOfUserAction;
    }

    /**
     * Checks and sets filteredBecauseOfUserAction flag when needed. Must be called from
     * prepareQueryData() :/
     */
    protected function performFilteredBecauseOfUserActionCheck()
    {
        $this->filteredBecauseOfUserAction = false;
        if ($this->filters) {
            $this->filteredBecauseOfUserAction = true;
            // Reset the flag back in case there were preset filters data
            if (null !== $this->presetFiltersData) {
                $this->filteredBecauseOfUserAction = false;
            }
        }
    }

    /**
     * @param null|Users $user
     */
    public function setLoggedInUser($user = null)
    {
        $this->loggedInUser = $user;
    }

    /**
     * @param Categories $adCategory
     */
    public function setAdCategory(Categories $adCategory, $setChildCategoryIds = true)
    {
        $this->adCategory            = $adCategory;
        if ($setChildCategoryIds) {
            $this->adCategoriesIDs = $this->getDescendantIdsWithTransactionTypeFromTreeArray(
                $this->categoryTree[$this->adCategory->id]
            );
        }
    }

    /**
     * @param int $adCategoryID
     */
    public function setAdCategoryByID($adCategoryID, $setChildCategoryIds = true)
    {
        $this->adCategory = null;

        if ($adCategory = Categories::findFirst($adCategoryID)) {
            $this->setAdCategory($adCategory, $setChildCategoryIds);
        }
    }

    /**
     * @param array $categoryIDs
     */
    public function setAdCategoriesIDs($categoryIDs)
    {
        $this->adCategoriesIDs = $categoryIDs;
    }

    public function getChildCategoriesAsArray($asArray = null)
    {
        if (null !== $asArray) {
            $this->getChildCategoriesAsArray = (bool) $asArray;
        }

        return $this->getChildCategoriesAsArray;
    }

    public function alwaysShowCompleteCategoriesTree($completeTree = null)
    {
        if (null !== $completeTree) {
            $this->alwaysShowCompleteCategoriesTree = (bool) $completeTree;
        }

        return $this->alwaysShowCompleteCategoriesTree;
    }

    public function sectionFiltering($val = null)
    {
        if (null !== $val) {
            $this->sectionFiltering = (bool) $val;
        }

        return $this->sectionFiltering;
    }

    /**
     * @param Users $adUser
     */
    public function setAdUser(Users $adUser)
    {
        $this->adUser = $adUser;
    }

    /**
     * @param int $adUserID
     */
    public function setAdUserByID($adUserID)
    {
        $this->adUser = null;

        if ($adUser = Users::findFirst($adUserID)) {
            $this->adUser = $adUser;
        }
    }

    /**
     * @param string $searchTerm
     */
    public function setSearchTerm($searchTerm)
    {
        $this->searchTermRaw        = $searchTerm;
        $this->searchTerm           = Utils::strip_boolean_operator_characters($this->searchTermRaw);
        $this->searchTermUnaccented = Utils::remove_accents($this->searchTerm);
    }

    public function setSortParamData($data = null)
    {
        $this->sortParamData = $data;
    }

    public function setBaseURL($url = null)
    {
        $this->baseURL = $url;
    }

    public function setPage($page = 1)
    {
        $page = (int) $page;
        if ($page < 1) {
            $page = 1;
        }

        $this->page = $page;
    }

    public function setItemsPerPage($itemsPerPage = null)
    {
        if (!$itemsPerPage || intval($itemsPerPage) == 0) {
            $itemsPerPage = $this->config->settingsFrontend->pagination_items_per_page;
        }
        $this->itemsPerPage = $itemsPerPage;
    }

    public function setFilters($filters = array())
    {
        $this->filters = $filters;
    }

    public function setPresetFiltersData($presetFiltersData = null)
    {
        $this->presetFiltersData = $presetFiltersData;
    }

    public function gpsBounds($gpsBounds = null)
    {
        if (null !== $gpsBounds) {
            $this->gpsBounds = $gpsBounds;
        }

        return $this->gpsBounds;
    }

    public function includeChildCategories($included = null)
    {
        if (null !== $included) {
            $this->includeChildCategories = (bool) $included;
        }

        return $this->includeChildCategories;
    }

    public function updateSearchTermDetails($update = null)
    {
        if (null !== $update) {
            $this->updateSearchTermDetails = (bool) $update;
        }

        return $this->updateSearchTermDetails;
    }

    public function showOnlyRootCategoriesCounts($onlyRoot = null)
    {
        if (null !== $onlyRoot) {
            $this->showOnlyRootCategoriesCounts = (bool) $onlyRoot;
        }

        return $this->showOnlyRootCategoriesCounts;
    }

    public function allowSpecialProducts($allow = null)
    {
        if (null !== $allow) {
            $this->allowSpecialProducts = (bool) $allow;
        }

        return $this->allowSpecialProducts;
    }

    public function autodetectFiltersFromCategory($autodetect = null)
    {
        if (null !== $autodetect) {
            $this->autodetectFiltersFromCategory = (bool) $autodetect;
        }

        return $this->autodetectFiltersFromCategory;
    }

    public function cacheResults($cache = null)
    {
        if (null !== $cache) {
            $this->cacheResults = (bool) $cache;
        }

        return $this->cacheResults;
    }

    public function cacheLifeTime($lifeTime = null)
    {
        if (null !== $lifeTime) {
            $this->cacheLifeTime = (int) $lifeTime;
        }

        return $this->cacheLifeTime;
    }

    public function cachePrefix($prefix = null)
    {
        if (null !== $prefix && trim($prefix)) {
            $this->cachePrefix = trim($prefix);
        }

        return $this->cachePrefix;
    }

    private function getFilters()
    {
        if (0 === count($this->filters) && $this->autodetectFiltersFromCategory()) {
            if ($this->categoryFiltersData = $this->getFiltersResponse()) {
                $this->filters = $this->categoryFiltersData['filters'];
            }
        }

        return $this->filters;
    }

    public function getPageSpecificJS()
    {
        return $this->pageSpecificJS;
    }

    public function getPageAssetsJS()
    {
        return $this->pageAssetsJS;
    }

    public function getPageAssetsCSS()
    {
        return $this->pageAssetsCSS;
    }

    public function getAdSubmitCategory()
    {
        return $this->adSubmitCategory;
    }

    public function getGeneratedViewVars()
    {
        return count($this->generatedViewVars) ? $this->generatedViewVars : null;
    }

    public function hasError()
    {
        return $this->error;
    }

    public function shouldTrigger404()
    {
        return $this->trigger404;
    }

    public function getFlashSession()
    {
        if ($this->flashSessionType && $this->flashSessionMsg) {
            return array(
                'type' => $this->flashSessionType,
                'text' => $this->flashSessionMsg
            );
        }

        return null;
    }

    public function getPagination()
    {
        return $this->pagination;
    }

    public function getTotalAdsCount()
    {
        return $this->totalAdsCount;
    }

    private function getURLQuery()
    {
        $urlQuery = $this->request->get() ? $this->request->get() : array();
        if (isset($urlQuery['_url'])) {
            $urlQuery['_url'] = null;
            unset($urlQuery['_url']);
        }
        if (isset($urlQuery['page'])) {
            $urlQuery['page'] = null;
            unset($urlQuery['page']);
        }
        if (isset($urlQuery['all'])) {
            $urlQuery['all'] = null;
            unset($urlQuery['all']);
        }

        return $urlQuery;
    }

    private function prepareQueryData()
    {
        $columns     = array('ad.*');
        $whereArray  = array();
        $whereParams = array();
        $whereTypes  = array();
        $havingArray = array();

        $this->sqlBuilder = new Builder();
        $this->sqlBuilder->addFrom('Baseapp\Models\Ads', 'ad');

        if ($this->searchTerm) {
            $searchTermParsed = Utils::build_boolean_mode_operators($this->searchTermUnaccented, '+');
            $searchTermParsed = Utils::quote_unquoted_words_with_dots($searchTermParsed);

            $this->sqlBuilder->innerJoin('Baseapp\Models\AdsSearchTerms', 'ad.id = ast.ad_id', 'ast');
            $whereArray[] = 'FULLTEXT_MATCH_BMODE(ast.search_data_unaccented, :search_term_parsed:)';
            $whereParams['search_term_parsed'] = trim(mb_strtolower($searchTermParsed, 'UTF-8'));
            $whereTypes['search_term_parsed']  = \PDO::PARAM_STR;
        }

        // Only join the currency table if needed
        $joinCurrencyTable = false;
        if ($this->sortParamData && ('cheap' === $this->sortParamData['current'] || 'expensive' === $this->sortParamData['current'])) {
            $joinCurrencyTable = true;
        }

        // Check if we're supposed to exclude age restricted categories (and which)
        $restrictionData       = $this->getSearchAgeRestrictionData($this->loggedInUser);
        $excludeRestricted     = $restrictionData['exclude'];
        $restrictedCategoryIDs = $restrictionData['category_ids'];

        // Exclude restricted categories
        if ($excludeRestricted && !empty($restrictedCategoryIDs)) {
            $whereArray[] = 'ad.category_id NOT IN (' . implode(',', $restrictedCategoryIDs) . ')';
        }

        if ($this->adUser) {
            //$this->sqlBuilder->innerJoin('Baseapp\Models\Users', 'ad.user_id = user.id', 'user');
            $whereArray[] = 'ad.user_id = :user_id:';
            $whereParams['user_id'] = $this->adUser->id;
            $whereTypes['user_id']  = \PDO::PARAM_INT;
        }

        if ($this->gpsBounds()) {
            $whereArray[] = 'ad.lng IS NOT NULL AND ad.lat IS NOT NULL AND ad.lng <= :gps_east: AND ad.lng >= :gps_west: AND ad.lat <= :gps_north: AND ad.lat >= :gps_south:';
            $whereParams['gps_east']  = $this->gpsBounds->east;
            $whereParams['gps_west']  = $this->gpsBounds->west;
            $whereParams['gps_north'] = $this->gpsBounds->north;
            $whereParams['gps_south'] = $this->gpsBounds->south;
        }

        $additional_innerJoins = array();

        $filters = $this->getFilters();
        // Must be called after $this->getFilters() :/
        $this->performFilteredBecauseOfUserActionCheck();
        if (count($filters)) {
            foreach ($filters as $filter) {
                if (isset($filter['model'])) {
                    $this->sqlBuilder->innerJoin($filter['model'], $filter['alias'].'.ad_id = ad.id', $filter['alias']);
                    if (isset($filter['filter'])) {
                        if (isset($filter['filter']['columns'])) {
                            $columns = array_merge($columns, $filter['filter']['columns']);
                        }
                        if (isset($filter['filter']['where_conditions'])) {
                            $whereArray[] = $filter['filter']['where_conditions'];
                            if (isset($filter['filter']['where_params'])) {
                                $whereParams = array_merge($whereParams, $filter['filter']['where_params']);
                            }
                            if (isset($filter['filter']['where_types'])) {
                                $whereTypes = array_merge($whereTypes, $filter['filter']['where_types']);
                            }
                        }
                        if (isset($filter['filter']['having_conditions'])) {
                            $havingArray[] = $filter['filter']['having_conditions'];
                        }
                    }
                } else {
                    if (isset($filter['columns'])) {
                        $columns = array_merge($columns, $filter['columns']);
                    }
                    if (isset($filter['where_conditions'])) {
                        $whereArray[] = $filter['where_conditions'];
                        if (isset($filter['where_params'])) {
                            $whereParams = array_merge($whereParams, $filter['where_params']);
                        }
                        if (isset($filter['where_types'])) {
                            $whereTypes = array_merge($whereTypes, $filter['where_types']);
                        }
                    }
                    if (isset($filter['having_conditions'])) {
                        $havingArray[] = $filter['having_conditions'];
                    }
                }
                if (isset($filter['innerJoin']) && count($filter['innerJoin'])) {
                    foreach ($filter['innerJoin'] as $name => $data) {
                        if ($name === 'currency') {
                            $joinCurrencyTable = true;
                        } else {
                            $additional_innerJoins[] = $data;
                        }
                    }
                }
            }
        }

        $whereArray[] = 'ad.active = 1';
        $whereArray[] = 'ad.is_spam = 0';

        $this->sqlBuilder->columns($columns);
        if ($joinCurrencyTable) {
            $this->sqlBuilder->innerJoin('Baseapp\Models\Currency', 'ad.currency_id = currency.id', 'currency');
        }
        if (count($additional_innerJoins)) {
            foreach ($additional_innerJoins as $innerJoin) {
                $this->sqlBuilder->innerJoin($innerJoin['model'], $innerJoin['condition'], $innerJoin['alias']);
            }
        }
        $orderBy = $this->buildOrderByParam();
        $this->sqlBuilder->orderBy($orderBy);
        // Have to leave this in for now... default filters show only ads with images and the way
        // that is handled now (inner join on ads_media) causes us to have to set group by always
        // because Filters currently do not have the ability to specify if they require a group by clause or not
        $this->sqlBuilder->groupBy(array('ad.id'));


        // exclude special products if we don't want them here
        $specialProductsWhere = 'NOT(ad.online_product_id IN ("pinky", "platinum"))';
        if (!$this->allowSpecialProducts()) {
            $whereArray[] = $specialProductsWhere;
        }

/*
        if (!$this->sectionFiltering()) {
            if (count($whereTypes)) {
                $this->sqlBuilderAllCategoriesCount->where(implode(' AND ', $whereArray), $whereParams, $whereTypes);
            } else {
                $this->sqlBuilderAllCategoriesCount->where(implode(' AND ', $whereArray), $whereParams);
            }
            if (count($havingArray)) {
                $this->sqlBuilderAllCategoriesCount->having(implode(' AND ', $havingArray));
            }
        }
*/
        $categoryWhere = null;
        // continue with main sqlBuilder and add information about chosen category we want ads from
        if ($categoriesCount = count($this->adCategoriesIDs)) {
            if ($this->adCategory) {
                if (!$this->includeChildCategories()) {
                    $categoryWhere = 'ad.category_id = ' . $this->adCategory->id;
                } elseif (count($this->adCategoriesIDs) == 1) {
                    $categoryWhere = 'ad.category_id = ' . $this->adCategoriesIDs[0];
                } else {
                    $categoryWhere = 'ad.category_id IN (' . implode(',', $this->adCategoriesIDs) . ')';
                }
            } else {
                if ($categoriesCount == 1) {
                    $categoryWhere = 'ad.category_id = ' . $this->adCategoriesIDs[0];
                } else {
                    $categoryWhere = 'ad.category_id IN (' . implode(',', $this->adCategoriesIDs) . ')';
                }
            }
            $whereArray[] = $categoryWhere;
        }

        // create a copy of current sqlBuilder as we have to have a slightly different sqlBuilder for special products

        $this->sqlBuilderAllCategoriesCount = clone $this->sqlBuilder;
        $noCategoriesWhereArray = $whereArray;
        if ($categoryWhere && ($key = array_search($categoryWhere, $noCategoriesWhereArray)) !== false) {
            unset($noCategoriesWhereArray[$key]);
        }

        $this->sqlBuilderNoProducts = clone $this->sqlBuilder;
        $noProductsWhereArray = $whereArray;
        if (($key = array_search($specialProductsWhere, $noProductsWhereArray)) !== false) {
            unset($noProductsWhereArray[$key]);
        }

        if (count($whereTypes)) {
            $this->sqlBuilder->where(implode(' AND ', $whereArray), $whereParams, $whereTypes);
            $this->sqlBuilderNoProducts->where(implode(' AND ', $noProductsWhereArray), $whereParams, $whereTypes);
            if ($this->sectionFiltering()) {
                $this->sqlBuilderAllCategoriesCount->where(implode(' AND ', $whereArray), $whereParams, $whereTypes);
            } else {
                $this->sqlBuilderAllCategoriesCount->where(implode(' AND ', $noCategoriesWhereArray), $whereParams, $whereTypes);
            }
        } else {
            $this->sqlBuilder->where(implode(' AND ', $whereArray), $whereParams);
            $this->sqlBuilderNoProducts->where(implode(' AND ', $noProductsWhereArray), $whereParams);
            if ($this->sectionFiltering()) {
                $this->sqlBuilderAllCategoriesCount->where(implode(' AND ', $whereArray), $whereParams);
            } else {
                $this->sqlBuilderAllCategoriesCount->where(implode(' AND ', $noCategoriesWhereArray), $whereParams);
            }
        }
        if (count($havingArray)) {
            $this->sqlBuilder->having(implode(' AND ', $havingArray));
            $this->sqlBuilderNoProducts->having(implode(' AND ', $havingArray));
            $this->sqlBuilderAllCategoriesCount->having(implode(' AND ', $havingArray));
        }

        $parsed = $this->sqlBuilder->getQuery()->parse();
        $dialect = $this->getDI()->get('db')->getDialect();
        $sql = $dialect->select($parsed);

        // spotted in the wiled after removing favorites join...
        $sql = str_replace('SELECT `ad` FROM', 'SELECT `ad`.* FROM', $sql);

        /**
         * $sql can and does contain our MysqlExtended full text expressions (functionCall), and other
         * un-parsed/unresolved named bind parameters, due to the piece of shit that is Db\Dialect in 1.3.x currently.
         * That's why we do it ourselves, by searching the parsed query for potential occurrences of not-yet-parsed
         * placeholders (and replacing them with their values, and having a bunch of security issues potentially)
         */
        $paginatorBindParams = array();
        foreach ($whereParams as $k => $v) {
            if (false !== strpos($sql, ':' . $k)) {
                $paginatorBindParams[':' . $k] = $v;
            }
        }

        $this->queryData = array(
            'sql'    => $sql,
            'params' => $paginatorBindParams
        );

        return $this->queryData;
    }

    private function getQueryFromCache($sql, $params = null, $page = null, $callbackMethod = null)
    {
        $resultsArray = null;
        $getFromDB    = false;

        // Try getting from memcached first
        $hash            = md5($sql) . ($params ? '-' . md5(json_encode($params)) : '');
        $cacheKey        = $this->cachePrefix() . '-' . $hash . ($page ? '-' . $page : '');
        $memcachedResult = null;
        // Determine when it's safe to actually go get the data (taking care of preventing cache stampedes
        // when the cache is actually being used)
        if ($this->memcached && $this->cacheResults()) {
            try {
                // When caching is used, things get somewhat more complex...

                $memcachedResponse = $this->memcached->getWithLock($cacheKey, $this->cacheLifeTime());
                $memcachedResult = $this->memcached->getResultCode();
                if ($memcachedResponse && !empty($memcachedResponse)) {
                    $resultsArray = $memcachedResponse;
                }

                /**
                 * N.B:
                 * When a lock is not acquired, we get back a NOT_FOUND result in $memcached_result,
                 * which means someone else should be building the data currently (no guarantees though)...
                 * And so we're kind of stuck here with having to choose one of:
                 * - show nothing since we got nothing (bad UX for visitors maybe, maybe not)
                 * - get non-cached data from db and pray it's not that slow and not many other process end up in the same sate
                 * - retry from the cache several times and then decide which of the above two options to choose again
                 */

                if (!$resultsArray) {
                    // Need to check we're the ones that should be doing the expensive query/data
                    $getFromDB = (MyMemcache::GENERATE_DATA == $memcachedResult);

                    // If we're not, let's try fetching from the cache again with a few retries until someone
                    // else finishes caching the data hopefully...
                    if (!$getFromDB && $this->memcached->lockExists($cacheKey)) {
                        // TODO/FIXME: abstract this away
                        $maxBackoffSeconds = 5;
                        $maxRetries        = 3;
                        $attemptCnt        = 0;
                        while ($attemptCnt < $maxRetries) {
                            $attemptCnt++;
                            $resultsArray = $this->memcached->get($cacheKey, $this->cacheLifeTime());
                            if ($resultsArray) {
                                $getFromDB = false;
                                break;
                            }
                            // Randomize the delay somewhat so not all clients wait for the exact same amount of time,
                            // while still utilizing this ghetto version of the truncated exponential backoff algorithm
                            $randMS          = mt_rand(1, 1000);
                            $randS           = $randMS / 1000;
                            $waitSeconds     = pow(2, $attemptCnt - 1) + $randS;
                            $waitTimeSeconds = min($waitSeconds, $maxBackoffSeconds);
                            $waitTimeUSec    = $waitTimeSeconds * 1000000;
                            // Sleep for the calculated amount of time (never longer than $maxBackoffSeconds)
                            // Bootstrap::log('Attempt ' . $attemptCnt . ' failed, sleeping for ' . $waitTimeSeconds . ' seconds...');
                            usleep($waitTimeUSec);
                        }

                        // TODO/FIXME:
                        // If after several attempts above we still have nothing, and the lock still
                        // exists, what do we do?
                        if (!$resultsArray && $this->memcached->lockExists($cacheKey)) {
                            $getFromDB = true;
                            Bootstrap::log('Reached Memcached max_retries without results and lock still exists, cache key: ' . $cacheKey);
                        }
                    }
                }
            } catch (PhCacheException $e) {
                $getFromDB = true;
                Bootstrap::log($e);
            }
        } else {
            // When no cache is used, we have no choice really
            $getFromDB = true;
        }

        // If we didn't get stuff back from cache and we've been cleared to get
        // the data from DB (lock was set, build and store for later)
        // error_log('in qetQueryFromCache, cacheKey = ' . $cacheKey . ', getFromDb = ' . var_export($getFromDB, true));
        if ($getFromDB) {
            if ($callbackMethod) {
                $resultsArray = call_user_func_array(
                    array($this, $callbackMethod),
                    array($sql, $params)
                );
            } else {
                $resultsArray = $this->getPaginatedResultsFromDB($sql, $params, $page);
            }
        }

        // If we've touched the db, we probably need to cache whatever we got back. Or else we'll go get it from
        // the database again. Which defeats the purpose of caching kinda?
        // Another VERY important thing is we need to delete the lock if we touched the db, otherwise others will
        // keep waiting for it until it expires, and it's slowing down things for no reason when special products
        // are fetched and re-fetched n times and they still don't return anything (and their lock was never released).
        if ($getFromDB && $this->memcached && $this->cacheResults()) {
            try {
                // Whatever results we've gotten from the db should probably be cached too.
                // But they weren't before. They were only cached if they were trueish.
                // So there's a BC break here kind of.
                // TODO/FIXME: investigate impact of saving $resultsArray only if it has something...
                //if ($resultsArray) {
                $this->memcached->save($cacheKey, $resultsArray, $this->cacheLifeTime());
                //}
                // Delete the lock that was acquired since we've touched the db
                $this->memcached->deleteLock($cacheKey);
            } catch (PhCacheException $e) {
                Bootstrap::log($e);
            }
        }

        return $resultsArray;
    }

    private function getResultsFromDB($sql, $params = null)
    {
        $this->dbHit = true;
        $db          = $this->getDI()->getShared('db');
        $result      = $db->query($sql, $params);
        $result->setFetchMode(\Phalcon\Db::FETCH_OBJ);
        $results = $result->fetchAll();

        return $results && count($results) ? $results : null;
    }

    private function getPaginatedResultsFromDB($sql, $params = null, $page = null)
    {
        $paginator = new \Baseapp\Extension\Paginator\Adapter\SimpleMySQL(
            array(
                'sql'   => $sql,
                'limit' => $this->itemsPerPage,
                'page'  => ($page ? $page : 1)
            )
        );
        if ($params && !empty($params)) {
            $paginator->setBindParams($params);
        }

        $this->dbHit = true;

        return $paginator->getPaginate();
    }

    /** Get filtered ads based on given modifiers from DB */
    protected function filterAds()
    {
        $resultsArray = array();

        $this->currentPage = $this->getPaginatedResultsFromDB($this->queryData['sql'], $this->queryData['params'], $this->page);

        // update details for this search term in db for autocomplete purpose but only on first page (and never for user's ads)
        if ($this->updateSearchTermDetails() && $this->searchTerm && 1 == $this->page) {
            $searchQuery = new SearchQueries();
            $searchQuery->setSearchTermDetails($this->searchTerm, $this->currentPage->total_items);
        }

        // Trigger a 404 page for a non-existent page number (but only when there are actual results to show)
        if ($this->currentPage && $this->currentPage->total_pages > 0 && $this->page > $this->currentPage->total_pages) {
            $resultsArray['error']      = true;
            $resultsArray['trigger404'] = true;
        } else {
            if (0 === count($this->currentPage->items)) {
                //$resultsArray['error']            = true;
                $resultsArray['flashSessionType'] = 'warning';
                $resultsArray['flashSessionMsg']  = 'Nije pronađen nijedan oglas koji zadovoljava postavljene kriterije';
            }
        }

        // Process the resulting page and extend with some additional data (ad image if it exists)
        $hydratedArray            = Ads::hydrateAdsResultsArray($this->currentPage->items);
        $this->currentPage->items = $hydratedArray;

        if (count($this->currentPage->items)) {
            $resultsArray['currentPage'] = $this->currentPage;
        }

        return $resultsArray;
    }

    private function setupResultsArray($resultsArray)
    {
        $this->ads               = null;
        $this->error             = false;
        $this->trigger404        = false;
        $this->totalAdsCount     = 0;
        $this->pagination        = null;
        $this->generatedViewVars = array();
        $this->flashSessionType  = null;
        $this->flashSessionMsg   = null;

        if ($resultsArray && is_array($resultsArray)) {
            $this->error             = isset($resultsArray['error']) ? $resultsArray['error'] : false;
            $this->trigger404        = isset($resultsArray['trigger404']) ? $resultsArray['trigger404'] : false;
            $this->flashSessionType  = isset($resultsArray['flashSessionType']) ? $resultsArray['flashSessionType'] : null;
            $this->flashSessionMsg   = isset($resultsArray['flashSessionMsg']) ? $resultsArray['flashSessionMsg'] : null;
            $this->currentPage       = isset($resultsArray['currentPage']) ? $resultsArray['currentPage'] : null;
            $this->ads               = isset($this->currentPage->items) ? $this->currentPage->items : null;
            $this->totalAdsCount     = isset($this->currentPage->total_items) ? $this->currentPage->total_items : 0;

            // this is something that should be generated every time and should not be caches as links for same ad listing
            // may differ based on chosen viewType (grid/list)...
            $this->generatePagination();
            $this->generateViewVars();
        }
    }

    public function getAds()
    {
        $this->prepareQueryData();

        if ($this->cacheResults()) {
            $resultsArray = $this->getQueryFromCache(
                $this->queryData['sql'],
                $this->queryData['params'],
                $this->page,
                'filterAds'
            );
        } else {
            $resultsArray = $this->filterAds();
        }

        $this->setupResultsArray($resultsArray);

        return $this->ads;
    }

    protected function generatePagination()
    {
        $this->pagination = null;

        if ($this->currentPage) {
            $paginationLinks = Tool::pagination(
                $this->currentPage,
                null, //$this->baseURL,
                'pagination',
                $this->config->settingsFrontend->pagination_count_out,
                $this->config->settingsFrontend->pagination_count_in
            );
            $paginationLinksTop = Utils::get_top_pagination_markup($paginationLinks);
            $paginationLinksBottom = Utils::get_bottom_pagination_markup($paginationLinks);
            $paginationLinksBottom = Utils::anchorify_links($paginationLinksBottom, null);
            $paginationLinksTop = Utils::anchorify_links($paginationLinksTop, null);

            $this->pagination = array(
                'top'    => $paginationLinksTop,
                'bottom' => $paginationLinksBottom
            );
        }
    }

    protected function generateViewVars()
    {
        if ($this->categoryFiltersData) {
            // this applies to category listings only
            if (!$this->adCategory->transaction_type_id) {
                if ($this->getChildCategoriesAsArray()) {
                    // this generates child categories that look like 'landing-links'
                    $this->generatedViewVars['childCategoriesMarkup'] = $this->buildChildCategoriesMarkupFromTreeArray(array($this->adCategory->id));
                } else {
                    $this->generatedViewVars['categoryTreeCountsMarkup'] = $this->buildCategoriesHierarchyTreeFromTreeArray(array($this->adCategory->id));
                }
            } else {
                $this->generatedViewVars['categoryFormFilters'] = (count($this->categoryFiltersData['categoryFiltersArray']['shown']) > 0);
                $this->generatedViewVars['showEmailAgentButton'] = $this->showEmailAgentButton;
                if (isset($this->categoryFiltersData['categoryFiltersArray'])) {
                    $this->generatedViewVars['categoryFiltersArray'] = $this->categoryFiltersData['categoryFiltersArray'];
                }
            }
        } else {
            // this applies to sections, search and user's/shop's ads listings
            $this->generatedViewVars = $this->getCategoriesLinksFromQuery();
        }

        return $this->generatedViewVars;
    }

    /**
     * Modifies the QueryBuilder in order to create and execute a query to return all the matched
     * categories and their respective ad counts so that we can build the sidebar tree
     *
     * @return array
     */
    protected function getCategoriesLinksFromQuery()
    {
        $countsByCategory = array();

        // Clone the original builder and reset a bunch of stuff we don't need and
        // add some stuff that we do, and then grab the results from that query
        if ($this->alwaysShowCompleteCategoriesTree()) {
            $countBuilder = clone $this->sqlBuilderAllCategoriesCount;
        } else {
            $countBuilder = clone $this->sqlBuilder;
        }
        $countBuilder->columns(array('category_id', 'count(category_id) AS cat_cnt'));
        $countBuilder->groupBy('category_id');
        $countBuilder->orderBy('cat_cnt DESC');

        $parsed            = $countBuilder->getQuery()->parse();
        $parsedBindParams  = $countBuilder->getQuery()->getBindParams();
        $dialect           = $this->getDI()->get('db')->getDialect();
        $parsedSQL         = $dialect->select($parsed);

        // spotted in the wiled after removing favorites join...
        $parsedSQL = str_replace('SELECT `ad` FROM', 'SELECT `ad`.* FROM', $parsedSQL);

        $parsedParams = array();
        if ($parsedBindParams && is_array($parsedBindParams)) {
            foreach ($parsedBindParams as $k => $v) {
                // check if we have current param in prepared sql query
                if (false !== strpos($parsedSQL, ':' . $k)) {
                    $parsedParams[':' . $k] = $v;
                }
            }
        }

        if ($results = $this->getQueryFromCache($parsedSQL, $parsedParams, null, 'getResultsFromDB')) {
            if ($this->showOnlyRootCategoriesCounts()) {
                foreach ($results as $result) {
                    $categoryID          = $result->category_id;
                    $categoryCount       = $result->cat_cnt;
                    $categoryTopParentID = $this->categoryTree[$categoryID]->top_parent_id;

                    if (!isset($countsByCategory[$categoryTopParentID])) {
                        $countsByCategory[$categoryTopParentID] = 0;
                    }
                    $countsByCategory[$categoryTopParentID] += $categoryCount;
                    unset($categoryID, $categoryCount, $categoryTopParentID);
                }
            } else {
                foreach ($results as $result) {
                    $countsByCategory[$result->category_id] = $result->cat_cnt;
                }
            }
        }
        unset($countBuilder, $results, $result);

        // Process param that handles if all categories should be shown
        $showAll = (int) $this->request->getQuery('all', 'int', 0);
        if (!$showAll || $showAll < 0) {
            $showAll = 0;
        }

        // No need to show toggleable links when there aren't enough categories (handles the case
        // even when someone directly types in the '?all=1' url param)
        $foundCategoriesCount = count($countsByCategory);
        if ($showAll && ($foundCategoriesCount <= 8)) {
            $showAll = 0;
        }

        $urlQuery = $this->getURLQuery();
        $hrefParams = array();
        if (!$showAll) {
            $hrefParams['all'] = 1;
        }
/*
        // Building 'show all/top 8' toggleable link
        $hrefParams = array(
            'q' => $this->searchTerm
        );

        if ($this->sortParamData) {
            $hrefParams['sort'] = $this->sortParamData['current'];
            $additionalQueryParams['sort'] = $this->sortParamData['current'];
        }
*/
        $showAllToggleHref = $this->url->get($this->baseURL, array_merge($hrefParams, $urlQuery));

        $categoryLinksFromQuery = array(
            'showAllToggleHref'     => $showAllToggleHref . '#filters-anchor',
            'showAllToggleText'     => !$showAll ? 'Prikaži sve kategorije<span class="fa fa-chevron-down fa-fw"></span>' : 'Prikaži top kategorije<span class="fa fa-chevron-up fa-fw"></span>',
            'searchHasCategories'   => (!empty($countsByCategory) ? true : false)
        );

        Tag::setDefault('all', $showAll);
        // Iterate over the list and build the tree with parent categories
        if (!empty($countsByCategory)) {
            $categoriesHierarchyTreeByCounts = $this->buildCategoriesHierarchyTreeByCounts(
                $countsByCategory,
                $showAll,
                $urlQuery,
                $leafNodesLinksToThemselves = false
            );
            $categoryLinksFromQuery = array_merge($categoryLinksFromQuery, $categoriesHierarchyTreeByCounts);
        }

        return $categoryLinksFromQuery;
    }

    /**
     * Returns the ORDER BY clause to be used when constructing various listing queries that
     * support choosing different sort options.
     * If a Model instance is provided, it's used as a provider for the default sort/order by value (if it has a
     * non-empty `default_sorting_order` property).
     * Also checks for the presence of a valid named sort value in the request parameters and
     * return the corresponding columns (based on config.ini values) if they exist, null otherwise.
     *
     * @param null|Model $entity
     *
     * @return null|Model\Resultset|string
     */
    protected function buildOrderByParam(Model $entity = null)
    {
        $order_by   = null;
        $named_sort = null;

        // If an entity is specified, use it as the provider of default value
        if (null !== $entity) {
            if (isset($entity->default_sorting_order) && !empty($entity->default_sorting_order)) {
                $named_sort = $entity->default_sorting_order;
            }
        }

        // Check if a specific sort was specified via calls to setSortParamDAta()
        if ($this->sortParamData) {
            $named_sort = $this->sortParamData['current'];
        }

        /**
         * We have to find out what is the preferred sorting order.
         *
         * First, we check if user specifically selected a sorting option. If this is the case, then we validate if we
         * know what this option means (if it exists in our config array). If we have a match, everything's ok. In case
         * user didn't select anything, we look for the default preferred sorting order, or, in case there isn't one
         * (I don't know why there wouldn't be one) we fall back to some basic ordering...
         */
        $sorts = isset($this->config->adSorting) ? $this->config->adSorting->toArray() : array();

        // If a request specifies a named sort option, use it (if it's valid)
        if ($this->request->has('sort')) {
            $named_sort = trim($this->request->get('sort', 'string'));
        }

        // Check if the named sort is valid
        if (isset($sorts[$named_sort]['column'])) {
            $order_by = trim($sorts[$named_sort]['column']);
        }

        // If we haven't found anything (or a non-valid named sort was specified), try to use some sane defaults
        if (null === $order_by) {
            if (isset($sorts['default_value']) && isset($sorts[$sorts['default_value']]['column'])) {
                $order_by = trim($sorts[$sorts['default_value']]['column']);
            } else {
                $order_by = 'ad.id';
            }
        }

        return $order_by;
    }

    /**
     * Helper method to get category filters and if they exists, set everything needed
     * to the view..
     *
     * @return null|array
     */
    protected function getFiltersResponse()
    {
        $categoryFiltersData = null;

        // TODO: try to cache this shit as much as possible
        if ($this->adCategory) {
            $filterData = null;
            if ($this->request->isPost()) {
                $filterData = $this->request->getPost();
            } elseif ($this->request->getQuery()) {
                $filterData = $this->request->getQuery();
            }

            // Allow merge/override via $this->presetFiltersData param
            if (null !== $this->presetFiltersData) {
                $filterData = array_merge($filterData, $this->presetFiltersData);
            }
            $categoryFiltersData = $this->adCategory->getFilters($filterData);
            if (is_array($categoryFiltersData['html']) && count($categoryFiltersData['html'])) {
                $categoryFiltersArray = array(
                    'shown'  => array(),
                    'hidden' => array()
                );
                foreach ($categoryFiltersData['html'] as $index => $row) {
                    if ($index <= $categoryFiltersData['show_more_after']) {
                        $categoryFiltersArray['shown'][] = $row;
                    } else {
                        $categoryFiltersArray['hidden'][] = $row;
                    }
                }
                $categoryFiltersData['categoryFiltersArray'] = $categoryFiltersArray;

                if (count($categoryFiltersData['js'])) {
                    $this->pageSpecificJS = implode("\n", $categoryFiltersData['js']);
                }

                if (isset($categoryFiltersData['assets'])) {
                    if (isset($categoryFiltersData['assets']['js']) && count($categoryFiltersData['assets']['js'])) {
                        $this->pageAssetsJS = $categoryFiltersData['assets']['js'];
                    }
                    if (isset($categoryFiltersData['assets']['css']) && count($categoryFiltersData['assets']['css'])) {
                        $this->pageAssetsCSS = $categoryFiltersData['assets']['css'];
                    }
                }
            }

            $this->showEmailAgentButton = $categoryFiltersData['email_agent'];

            if ($categoryFiltersData['email_agent']) {
                if (!$this->pageAssetsJS) {
                    $this->pageAssetsJS = array();
                }
                $this->pageAssetsJS[] = 'assets/js/categories-email-agent.js';
            }

            if (isset($categoryFiltersData['form']) && $categoryFiltersData['form']) {
                $this->adSubmitCategory = $this->adCategory->id;
            }

//            $this->view->setVar('category_form_filters', $category_filters_data['form']);
        }

//        $this->view->setVar('category_has_filters', $category_has_filters);

        return $categoryFiltersData;
    }

    public function getSpecialProducts($hydrateResults = true, $limit = null, $randomOrdered = false)
    {
        $countBuilder = clone $this->sqlBuilderNoProducts;
        $countBuilder->andWhere('ad.online_product_id IN ("pinky", "platinum")');

        $parsed            = $countBuilder->getQuery()->parse();
        $parsedBindParams  = $countBuilder->getQuery()->getBindParams();
        $dialect           = $this->getDI()->get('db')->getDialect();
        $parsedSQL         = $dialect->select($parsed);

        // spotted in the wild after removing favorites join...
        $parsedSQL = str_replace('SELECT `ad` FROM', 'SELECT `ad`.* FROM', $parsedSQL);

        $parsedParams = array();
        if ($parsedBindParams && is_array($parsedBindParams)) {
            foreach ($parsedBindParams as $k => $v) {
                // check if we have current param in prepared sql query
                if (false !== strpos($parsedSQL, ':' . $k)) {
                    $parsedParams[':' . $k] = $v;
                }
            }
        }

        $resultsArray = $this->getQueryFromCache($parsedSQL, $parsedParams, null, 'getResultsFromDB');
        if ($resultsArray && count($resultsArray)) {
            $workingResultsArray = $resultsArray;
            $limit = (int) $limit;
            if ($randomOrdered) {
                shuffle($workingResultsArray);
            }
            if ($limit > 0) {
                $workingResultsArray = array_slice($workingResultsArray, 0, $limit, $preserve_keys = false);
            }

            if ($hydrateResults) {
                return Ads::hydrateAdsResultsArray($workingResultsArray);
            } else {
                return $workingResultsArray;
            }
        }

        return null;
    }

    private function getCustomFilterData($filterTypeID, $filterData)
    {
        switch ($filterTypeID) {
            case 'TITLE':
                $customFilter = new \Baseapp\Library\Parameters\Renderer\Filter\Title();
                break;
            case 'CURRENCY':
                $customFilter = new \Baseapp\Library\Parameters\Renderer\Filter\Currency();
                break;
            case 'UPLOADABLE':
                $customFilter = new \Baseapp\Library\Parameters\Renderer\Filter\Uploadable();
                break;
        }

        if (isset($customFilter)) {
            $customFilter->get_only_sql_filter_data = true;
            $customFilter->setModule('frontend');
            $customFilter->setData($filterData);
            return $customFilter->getMarkup();
        }

        return null;
    }

    public function setCustomFilter($customFilters)
    {
        $filters  = array();
        $customJS = array();
        $assets   = array(
            'css' => array(),
            'js'  => array()
        );
        foreach ($customFilters as $filterTypeID => $filterData) {
            if ($customFilterData = $this->getCustomFilterData($filterTypeID, $filterData)) {
                if (isset($customFilterData['js']) && trim($customFilterData['js'])) {
                    $customJS[] = trim($customFilterData['js']);
                }

                if (isset($customFilterData['assets'])) {
                    if (isset($customFilterData['assets']['js']) && count($customFilterData['assets']['js'])) {
                        $assets['js'] = array_merge($assets['js'], $customFilterData['assets']['js']);
                    }
                    if (isset($customFilterData['assets']['css']) && count($customFilterData['assets']['css'])) {
                        $assets['css'] = array_merge($assets['css'], $customFilterData['assets']['css']);
                    }
                }

                if (isset($customFilterData['filter'])) {
                    $filters[] = $customFilterData['filter'];
                }
            }
        }

        if (!empty($customJS)) {
            $this->pageSpecificJS = implode("\n", $customJS);
        }
        if (!empty($assets['js'])) {
            if (null === $this->pageAssetsJS) {
                $this->pageAssetsJS = array();
            }
            $this->pageAssetsJS = array_merge($this->pageAssetsJS, $assets['js']);
        }
        if (!empty($assets['css'])) {
            if (null === $this->pageAssetsCSS) {
                $this->pageAssetsCSS = array();
            }
            $this->pageAssetsCSS = array_merge($this->pageAssetsCSS, $assets['css']);
        }

        $this->setFilters($filters);
    }

    public function dbHit()
    {
        return $this->dbHit;
    }

/*----------------------------------------------------------------------------------------------------------------------
Methods needed to build hierarchy category tree
----------------------------------------------------------------------------------------------------------------------*/

    /**
     * @return null|string
     */
    private function buildCategoriesHierarchyTreeByCounts($totals, $showAll, $additionalQueryParams = array(), $leafNodesLinksToThemselves = true)
    {
        $markup = null;

        // Sort totals so that the highest number of results is first/top and
        // that the index association is maintained
        arsort($totals);

        $parent_cnts = array();

        // Collecting and building hierarchy data using $this->categoryTree and $totals
        $category_paths_data = array();
        foreach ($totals as $category_id => $cnt) {
            $category_node = $this->categoryTree[$category_id];
            $active_category_node = $this->adCategory && $this->adCategory->id == $category_id;

            // Rebuilding the tree and links for all the node's parents up to the root
            $parents = array();
            $parent_ids = array();
            $parent_id = $category_node->parent_id;
            while ($parent_id > 1) {
                if (isset($this->categoryTree[$parent_id])) {
                    $category_node_parent = $this->categoryTree[$parent_id];
                    $parent_id = $category_node_parent->parent_id;
                    $additionalQueryParams['q'] = $this->searchTerm;
                    $additionalQueryParams['category_id'] = $category_node_parent->id;
                    if ($this->maintain_sort_param) {
                        $additionalQueryParams['sort'] = $this->sort_data['current'];
                    }
                    $href = $this->url->get($this->baseURL, $additionalQueryParams);
                    $parent_data = array(
                        'id'     => $category_node_parent->id,
                        'href'   => $href,
                        'name'   => $category_node_parent->name,
                        'active' => $category_node_parent->active,
                    );
                    $parents[] = $parent_data;
                    $parent_ids[] = $category_node_parent->id;
                    if (!isset($parent_cnts[$category_node_parent->id])) {
                        $parent_cnts[$category_node_parent->id] = 0;
                    }
                    $parent_cnts[$category_node_parent->id] += $cnt;
                } else {
                    // Bail if no node with such a parent exists?
                    $parent_id = 0;
                }
            }

            // We only get/collect counts from leaf nodes
            if ($category_node->transaction_type_id && $leafNodesLinksToThemselves) {
                // If the cat is a leaf node, show a link to it (but only if leafNodesLinksToThemselves == true)
                $query_params = array(
                    'ad_params_title' => $this->searchTerm
                );
                if (isset($additionalQueryParams['viewType'])) {
                    $query_params['viewType'] = $additionalQueryParams['viewType'];
                }
                if ($this->maintain_sort_param) {
                    $query_params['sort'] = $this->sort_data['current'];
                }
                $href = $this->url->get($category_node->url, $query_params);
            } else {
                // Not a leaf node, create a link to drill down into that specific subtree
                $additionalQueryParams['q'] = $this->searchTerm;
                $additionalQueryParams['category_id'] = $category_node->id;
                if ($this->maintain_sort_param) {
                    $additionalQueryParams['sort'] = $this->sort_data['current'];
                }
                $href = $this->url->get($this->baseURL, $additionalQueryParams);
            }

            $data                   = new \stdClass();
            $data->id               = $category_id;
            $data->name             = $category_node->name;
            $data->count            = $cnt;
            $data->href             = $href;
            $data->current          = $active_category_node;
            $data->parents_reversed = array_reverse($parents);
            $data->active           = $category_node->active;

            $category_paths_data[$category_id] = $data;
        }

        foreach ($category_paths_data as $category_id => $category_data) {
            if (isset($category_data->parents_reversed)) {
                foreach ($category_data->parents_reversed as $i => $parent_data) {
                    if (isset($parent_cnts[$parent_data['id']])) {
                        $category_data->parents_reversed[$i]['count'] = $parent_cnts[$parent_data['id']];
                    }
                }
            }
        }

        $hierarchy = $this->buildChildrenHierarchyFromCategoryPathArray($category_paths_data);

        // Filter down $hierarchy to only the TOP 8 unless specified otherwise
        $root_cats_found = count($hierarchy);
        $expanded_class  = $root_cats_found > 8 && $showAll ? true : false;
        if ($root_cats_found > 8 && !$showAll) {
            $hierarchy = array_slice($hierarchy, 0, 8, $preserve_keys = true); // $preserving keys is important!
        }

        // the class controls whether the list will be initially opened up to it's leaf-nodes or should drill-down be applied
        $class = $expanded_class ? 'expanded' : '';
        $markup = $this->buildNestedList($hierarchy, $class);

        $finalResponse = array(
            'searchHasCategories' => (trim(strip_tags($markup)) ? true : false)
        );

        if ($finalResponse['searchHasCategories']) {
            $finalResponse['needsShowAll']             = $root_cats_found > 8;
            $finalResponse['categoryTreeCountsMarkup'] = $markup;

            if ($this->adCategory) {
                // If the cat is a leaf node, show a link to it (but only if leafNodesLinksToThemselves == true)
                $query_params = array(
                    'ad_params_title' => $this->searchTerm
                );
                if (isset($additionalQueryParams['viewType'])) {
                    $query_params['viewType'] = $additionalQueryParams['viewType'];
                }
                if ($this->maintain_sort_param) {
                    $query_params['sort'] = $this->sort_data['current'];
                }

                $finalResponse['linkToLeafCategory']     = true;
                $finalResponse['linkToLeafCategoryHref'] = $this->url->get($this->adCategory->url, $query_params);
            } else {
                $finalResponse['linkToLeafCategory'] = false;
            }
        }

        return $finalResponse;
    }

    private function buildChildrenHierarchyFromCategoryPathArray($categoryPathsData)
    {
        // Build the hierarchy array from path data created above
        $return   = array();
        $lastNode = &$return;

        $key = null;
        foreach ($categoryPathsData as $categoryID => $categoryData) {
            $parents = $categoryData->parents_reversed;

            foreach ($parents as $level => $parent) {
                $key = $parent['id'];

                if (!isset($lastNode[$key])) {
                    $lastNode[$key] = array(
                        'id'       => $key,
                        'name'     => $parent['name'],
                        'href'     => $parent['href'],
                        'count'    => $parent['count'] ? $parent['count'] : 0,
                        'active'   => $parent['active'],
                        'children' => array()
                    );
                }

                $lastNode = &$lastNode[$key]['children'];
            }

            $lastNode[(string) $categoryData->id] = array(
                'id'           => $categoryData->id,
                'name'         => $categoryData->name,
                'href'         => $categoryData->href,
                'current'      => $categoryData->current,
                'count'        => $categoryData->count,
                'active'       => $categoryData->active,
                'children'     => array()
            );

            $lastNode = &$return;
        }

        return $return;
    }

    private function buildNestedList(&$array, $class = null)
    {
        static $depth = 0;

        $markup = null;

        if (0 === $depth) {
            $markup .= '<ul data-depth="0" class="sidebar-lists depth-0 clickable-drill-down' . ($class ? ' ' . $class : '') .'">';
        }

        foreach ($array as $k => $v) {
            // Skip over inactive nodes completely
            if (is_array($v) && !$v['active']) {
                continue;
            }

            $markup .= '<li' . (count($v['children']) == 0 ? ' class="leaf-node"' : '') . '>';

            if (is_array($v)) {

                $depth++;

                $linkCount = isset($v['count']) && $v['count'] > 0 ? $v['count'] : '';
                $currentlySelectedNode = isset($v['current']) && $v['current'];

                $linkMarkup  = '<a data-id="' . $v['id'] . '" href="' . $v['href'] . '"' . ($linkCount ? ' data-cnt="' . $linkCount . '"' : '') . ($currentlySelectedNode ? ' class="current"' : '') . '>';
                if ($linkCount) {
                    $linkMarkup .= '<span class="fr margin-left-5px">(' . number_format($linkCount, 0, ',', '.') . ')</span>';
                }
                $linkMarkup .= $v['name'];
                $linkMarkup .= '</a>';

                if (1 === $depth) {
                    $markup .= '<strong data-id="' . $v['id'] . '">' . $linkMarkup . '</strong>';
                } else {
                    $markup .= $linkMarkup;
                }

                $children = $v['children'];

                // Nodes with counts that have children vs those that don't
                if (isset($v['count'])) {
                    if (!empty($children)) {
                        $markup .= '<ul data-depth="' . $depth . '" class="sub leaf depth-' . $depth . '">';
                        $markup .= $this->buildNestedList($children);
                        $markup .= '</ul>';
                    }
                } else {
                    $markup .= '<ul data-depth="' . $depth . '" class="sub depth-' . $depth . '">' . $this->buildNestedList($children) . '</ul>';
                }

            }

            $markup .= '</li>';
            $depth--;
        }

        if (0 === $depth) {
            $markup .= '</ul>';
        }

        return $markup;
    }

    private function buildCategoriesHierarchyTreeFromTreeArray(array $startNodes, $depthLimit = 1)
    {
        $markup = '';

        foreach ($startNodes as $startNode) {
            $subtree = $this->categoryTree[$startNode];
            $markup .= $this->buildNestedListFromTreeArrayNode($subtree, $depthLimit);
        }

        return $markup;
    }

    private function buildNestedListFromTreeArrayNode(&$node, $depthLimit = 2)
    {
        static $depth = 0;

        $markup = null;

        if (0 === $depth) {
            //$markup .= '<ul data-depth="0" class="sidebar-lists depth-0">';
        }

        if (!$node->active) {
            return null;
        }

        // TODO/FIXME: HACK! move this out of here or generate somewhere else, or whatever...
        if (!isset($node->href)) {
            $linkParams = $this->request->getQuery();
            $linkParams['_url'] = null;
            unset($linkParams['_url']);
            if ($this->maintain_sort_param) {
                $linkParams['sort'] = $this->sort_data['current'];
            }
            $node->href = $this->url->get($node->url, $linkParams);
        }

        $depth++;
        if (1 === $depth) {
            // First level nodes displayed differently
        //    $markup .= '<strong data-id="' . $node->id . '" class="text-muted"><a href="' . $node->href . '">' . $node->name . '</a></strong>';
        } else {
            $markup .= '<li>';
            if (isset($node->count)) {
                $markup .= '<a data-cnt="' . $node->count . '" data-id="' . $node->count . '" href="' . $node->href . '">';
                if ($node->count > 0) {
                    $markup .= '<span class="fr margin-left-5px">(' . number_format($node->count, 0, ',', '.') . ')</span>';
                }
                $markup .= $node->name;
                $markup .= '</a>';
            } else {
                $markup .= '<a data-id="' . $node->id . '" href="' . $node->href . '">' . $node->name . '</a>';
            }
            $markup .= '</li>';
        }

        $children = $node->children;

        // Nodes with counts that have children vs those that don't
        if (!empty($children) && $depth <= $depthLimit) {
            $markup .= '<ul data-depth="' . $depth . '" class="sub leaf depth-' . $depth . '">';
            foreach ($children as $k => $v) {
                $markup .= $this->buildNestedListFromTreeArrayNode($v, $depthLimit);
            }
            $markup .= '</ul>';
        }

        $depth--;

        if (0 === $depth) {
            //$markup .= '</ul>';
        }

        return $markup;
    }

    private function buildChildCategoriesMarkupFromTreeArray(array $startNodes, $depthLimit = 1)
    {
        $markup = '';

        foreach ($startNodes as $startNode) {
            $subtree = $this->categoryTree[$startNode];

            $urls = array();
            if (!empty($subtree->children)) {
                foreach ($subtree->children as $node) {
                    $urls[] = '/' . $node->url;
                }
            }

            $counts = array();
            if (!empty($urls)) {
                $counts = AdsTotalsCache::getMulti($urls);
            }

            $markup .= $this->buildChildCategoriesMarkupFromTreeArrayNode($subtree, $depthLimit, $counts);
        }

        if ($markup !== '') {
            $markup = '<div class="row no-gutters-xs xs-margin-offset-minus-10px black bold margin-top-md margin-bottom-md landing-links">' . $markup . '</div>';
        }

        return $markup;
    }

    protected function buildChildCategoriesMarkupFromTreeArrayNode(&$node, $depthLimit = 2, $counts = array())
    {
        static $depth = 0;

        $markup = null;

        if (!$node->active) {
            return null;
        }

        // TODO/FIXME: HACK! move this out of here or generate somewhere else, or whatever...
        if (!isset($node->href)) {
            $linkParams = array();
            $linkParams = $this->request->getQuery();
            $linkParams['_url'] = null;
            unset($linkParams['_url']);
            unset($linkParams['page']);
            if ($this->maintain_sort_param) {
                $linkParams['sort'] = $this->sort_data['current'];
            }
            $node->href = $this->url->get($node->url, $linkParams);
        }

        $count_attr = '';
        $count_markup = null;

        $depth++;
        if (1 !== $depth) {
            // Counts are using root-relative links, and $node->href contains the hostname too,
            // so we strip it for comparison purposes
            $link = parse_url($node->href, PHP_URL_PATH);
            $count = isset($counts[$link]) ? $counts[$link] : false;
            if ($count) {
                $count_attr = ' data-cnt=" (' . $count . ')"';
            }

            $markup .= '<div class="col-md-3 col-sm-4 col-xs-12">';
            $markup .= '    <a data-id="' . $node->id . '" href="' . $node->href . '"' . $count_attr . '> ' . $node->name . '</a>';
            $markup .= '</div>';
        }

        $children = $node->children;

        // Nodes with counts that have children vs those that don't
        if (!empty($children) && $depth <= $depthLimit) {
            foreach ($children as $k => $v) {
                $markup .= $this->buildChildCategoriesMarkupFromTreeArrayNode($v, $depthLimit, $counts);
            }
        }

        $depth--;

        return $markup;
    }

}
