<?php

namespace Baseapp\Library;

use Baseapp\Bootstrap;
use Phalcon\Mvc\User\Component;
use Baseapp\Models\Users;
use Baseapp\Models\Tokens;
use Baseapp\Models\FailedLogins;

/**
 * Auth Library
 */
class Auth extends Component
{

    private static $_instance;

    /**
     * Singleton pattern
     *
     * @return Auth instance
     */
    public static function instance()
    {
        if (empty(self::$_instance)) {
            self::$_instance = new Auth();
        }

        return self::$_instance;
    }

    /**
     * Private constructor - disallows creating a new object
     */
    private function __construct()
    {
        // pull the services via Di
        /*
        $this->request = \Phalcon\Di::getDefault()->getShared('request');
        $this->cookies  = \Phalcon\Di::getDefault()->getShared('cookies');
        $this->session  = \Phalcon\Di::getDefault()->getShared('session');
        */
    }

    /**
     * Private clone - disallows object cloning
     */
    private function __clone()
    {
    }

    /**
     * Checks if a session is active (and optionally if it has the required role[s])
     * If an array of roles is sent as parameter, then logged_in will return true if
     * at least one of the required roles is found (OR condition)
     *
     * @param mixed $role role name
     *
     * @return boolean
     */
    public function logged_in($role = null)
    {
        // Get the user from the session
        $user = $this->get_user();
        if (!$user) {
            return false;
        }

        // If user exists in session
        if ($user) {
            // If we don't have a role no further checking is needed
            if (!$role) {
                return true;
            }

            // get all user's roles in an array
            $users_roles = array();

            // try to get roles from session if they exists
            if ($this->config->auth->session_roles && $this->session->has($this->config->auth->session_roles)) {
                $users_roles_raw = $this->session->get($this->config->auth->session_roles);
                // build an array of user's roles
                foreach ($users_roles_raw as $raw_role_name => $raw_role_id) {
                    $users_roles[] = $raw_role_name;
                }
            } else {
                // get user's roles from db
                $users_roles_raw = $user->get_roles_joined();
                // build an array of user's roles
                foreach ($users_roles_raw as $raw_role) {
                    $users_roles[] = $raw_role['name'];
                }
            }

            if (!is_array($role)) {
                $role = array($role);
            }

            $has_access = count(array_intersect($users_roles, $role)) > 0;

            return $has_access;
        }
    }

    /**
     * Gets the roles belonging to a user.
     *
     * @param Users $user Users model
     *
     * @return array
     */
    public function get_user_roles($user)
    {
        $roles = array();
        $roles_raw = $user->get_roles_joined();

        // Key the roles by role name and set the value as id
        foreach ($roles_raw as $role) {
            $roles[$role['name']] = $role['id'];
        }

        return $roles;
    }

    /**
     * Gets the currently logged in user from the session.
     * Returns false if no user is currently logged in.
     *
     * @return null|Users Currently logged in Users object or false if there isn't any
     */
    public function get_user()
    {
        $user = $this->session->get($this->config->auth->session_key);

        // Check "remembered" login
        if (!$user) {
            $user = $this->check_auto_login();

            // Reset to null in case we got a boolean false back
            if (is_bool($user) && false === $user) {
                $user = null;
            }
        }

        /**
         * This simulates the stuff that occurs when the model record is
         * fetched from the DB (and we're getting the User object from the session above (most of the times)).
         *
         * Since we're later using this object in other places -- and treating it as though it's coming directly
         * from the db -- we have to explicitly set snapshot data. Otherwise we get those "The record doesn't have
         * a valid data snapshot" exceptions.
         *
         * Read this: https://github.com/phalcon/cphalcon/issues/1867#issuecomment-71155278
         */
        if ($user instanceof Users) {
            if (!$user->hasSnapshotData()) {
                $user->setSnapshotData($user->toArray());
            }
        }

        return $user;
    }

    /**
     * Refresh user data stored in the session from the database.
     * Returns null if no user is currently logged in.
     *
     * @return null|Users Null if no data in session, Users object otherwise
     */
    public function refresh_user()
    {
        $user = $this->session->get($this->config->auth->session_key);

        if (!$user) {
            return null;
        }

        // Get user's data from db
        $user = Users::findFirst($user->id);
        $roles = $this->get_user_roles($user);

        // Regenerate session_id
        $this->session_regenerate_id();

        $this->session_store_user_and_roles($user, $roles);

        return $user;
    }

    public function session_store_user_and_roles($user, $roles)
    {
        // Store user in session
        $this->session->set($this->config->auth->session_key, $user);

        // Store user's roles in session
        if ($this->config->auth->session_roles) {
            $this->session->set($this->config->auth->session_roles, $roles);
        }
    }

    public function session_remove_user_and_roles()
    {
        // Remove the user from the session
        $this->session->remove($this->config->auth->session_key);

        // Remove user's roles from the session
        if ($this->config->auth->session_roles) {
            $this->session->remove($this->config->auth->session_roles);
        }
    }

    /**
     * Complete the login for a user by incrementing the logins and saving login timestamp
     *
     * @param object $user user from the model
     *
     * @return void
     */
    private function complete_login($user)
    {
        // Update the number of logins
        /**
         * TODO: do we REALLY need this?
         * IF we do, could we store it in a separate table? pros/cons?
         */
        $user->logins = $user->logins + 1;

        // Set the last login date
        $user->last_login = time();

        // Save the user
        $user->update();
    }

    public function session_destroy($remove_cookie = true)
    {
        // Bootstrap::log(__METHOD__);
        $this->session->destroy($remove_cookie);
    }

    public function session_regenerate_id()
    {
        // Bootstrap::log(__METHOD__);
        $this->session->regenerateId();
    }

    /**
     * Checks if the user is banned/inactive/suspended
     *
     * @param Users $user
     *
     * @throws \Exception
     */
    public function check_user_states(Users $user)
    {
        /*if (!$user->is_active_by_having_login_role()) {
            throw new Exception('Korisnički račun nije aktivan');
        }*/

        if (!$user->active) {
            throw new \Exception('Korisnički račun nije aktivan');
        }

        if ($user->banned) {
            throw new \Exception('Korisnički račun je banan!');
        }

        if ($user->suspended) {
            throw new \Exception('Korisnički račun je suspendiran');
        }
    }

    /**
     * Implements login throttling, hopefully reducing the effectiveness of "brute force" attacks
     *
     * @param int $user_id
     * @return void
     */
    public function register_login_throttle($user_id)
    {
        $ip = $this->request->getClientAddress(true);

        $failed_login = new FailedLogins();
        $failed_login->user_id = $user_id;
        $failed_login->ip = $ip;
        $result = $failed_login->create();

        // TODO: either remove this or make it throw an exception if we really care that much about it
        if (true !== $result) {
            Bootstrap::log($result);
        }

        // get number of attempts from the same IP
        // within the last 6 hours
        $attempts = FailedLogins::count(array(
            'ip = ?0 AND attempted >= ?1',
            'bind' => array(
                $ip,
                // last 6 hours?
                time() - 3600 * 6
            )
        ));

        switch ($attempts) {
            case 1:
            case 2:
                // no delay
                break;
            case 3:
            case 4:
                // delay for 2 secs
                sleep(2);
                break;
            default:
                // delay for 4 seconds
                sleep(4);
                break;
        }
    }

    /**
     * Login a user based on the "authautologin" cookie.
     *
     * @return bool|Users False on failure, Users object matching the autologin token if successful
     */
    private function check_auto_login()
    {
        $cookie_token = null;
        $has_cookie = $this->cookies->has('authautologin');

        if ($has_cookie) {
            try {
                $cookie_token = $this->cookies->get('authautologin')->getValue();
            } catch (\Phalcon\Crypt\Exception $e) {
                Bootstrap::log($e);
            }
        }

        /**
         * zyt: 17.07.2014.
         * Need to check both presence and non-null value of the cookie
         * since has() returns true if we've set a cookie by that name during
         * the current request, but does not get the value since the actual cookie
         * is only yet to be sent as part of the current request's response.
         *
         * This sounds like a bug in Phalcon if you ask me. has() should maybe return
         * false in that case, because the cookie has not yet been fully set actually?
         */
        if ($has_cookie && null !== $cookie_token) {
            // Load the token
            $token = Tokens::findFirst(array('token = :token:', 'bind' => array('token' => $cookie_token)));

            // If the token exists
            if ($token) {
                // Load the user and his roles
                $user = $token->getUser();
                $roles = $this->get_user_roles($user);

                // If user has login role, is not banned and tokens match, perform a login
                if (isset($roles['login']) && (int)$user->banned === 0 && $token->user_agent === sha1($this->request->getUserAgent())) {
                    // Regenerate session_id
                    $this->session_regenerate_id();

                    // Store user in session
                    $this->session_store_user_and_roles($user, $roles);

                    // Finish the login
                    $this->complete_login($user);

                    // Save the token to create a new unique token
                    $token->token = $this->create_autologin_token();
                    $token->save();

                    // Set the new token
                    $this->cookies->set('authautologin', $token->token, $token->expires);

                    // Automatic login was successful
                    return $user;
                }

                // Token is invalid
                $token->delete();
            } else {
                // delete the cookie since it has an invalid value
                $this->cookies->set('authautologin', '', time() - 3600);
                $this->cookies->delete('authautologin');
            }
        }

        return false;
    }

    /**
     * Attempt to login a user (by using either an ORM object or a username) and that
     * user's plain-text password (which is matched against our stored password hash).
     *
     * @param string|Users $user User's username|email or a full blown Users object to log in
     * @param string $password Password (plain text) to check against
     * @param boolean $remember Enable autologin
     *
     *@return boolean|null True if successful, false if password doesn't match or the user doesn't have the 'login' role, null if user not found
     */
    public function login($user, $password, $remember = false)
    {
        $user = $this->check_login_user_object($user);

        // If we've got nothing to work with, bail
        if (!$user) {
            return null;
        }

        // Empty passwords can't match
        if (empty($password)) {
            return false;
        }

        // Get user's roles
        $roles = $this->get_user_roles($user);

        // User has no login role (might just need to activate first?)
        if (!isset($roles['login'])) {
            return false;
        }

        // Banned users cannot be logged in
        if ($user->banned) {
            return false;
        }

        $valid = $this->check_hashed_password($password, $user->password);

        if (!$valid && isset($user->old_password) && !empty($user->old_password)) {
            $valid = $this->login_with_old_password($user, $password);
        }

        if (!$valid) {
            // invalid pass attempt
            $this->register_login_throttle($user->id);
            return false;
        }

        // Regenerate session_id
        $this->session_regenerate_id();

        // Store user in session
        $this->session_store_user_and_roles($user, $roles);

        // Finish the login
        $this->complete_login($user);

        // If autologin was specified, create the autologin cookie
        if (true === $remember) {
            // Create a new autologin token
            $token = new Tokens();
            $token->user_id = $user->id;
            $token->user_agent = sha1($this->request->getUserAgent());
            $token->token = $this->create_autologin_token();
            $token->created = time();
            $token->expires = $token->created + $this->config->auth->lifetime;

            if (true === $token->create()) {
                // Set the autologin cookie if token creation succeeded
                $this->cookies->set('authautologin', $token->token, ($token->created + $this->config->auth->lifetime));
            }
        }

        return true;
    }

    /**
     * Attempt to login a user (by using their old password if it's available).
     * If we manage to match old passwords, we hash and write hashed password in
     * a 'password' field for future uses
     *
     * @param Users $user Full blown Users object to log in
     * @param string $old_password Password (plain text) to check against
     *
     *@return boolean True if successful and password written in 'password' field, false if old_password doesn't match
     */
    public function login_with_old_password($user, $old_password)
    {
        $valid = (md5($old_password) == $user->old_password);

        if (!$valid) {
            return false;
        }
        // we have a valid username/password combination - store old_password in password
        $user->password = $this->auth->hash_password($old_password);
        // clear the old password!
        $user->old_password = null;

        return $user->update();
    }

    /**
     * Checks if the provided $user argument is not a Users object and returns one if
     * we manage to find a user with the specified username/email
     *
     * @param string|Users $user User's username or email (strings) or a Users object (in which case we do nothing)
     *
     * @return null|Users Matching Users record (if found, when searched by username/email, null if not found)
     */
    public function check_login_user_object($user)
    {
        if (!$user instanceof Users) {
            $username = $user;

            // Username not specified
            if (!$username) {
                return null;
            }

            // Try loading the user record by username
            $user = Users::findFirst(array('username = :username: OR email = :email:', 'bind' => array('username' => $username, 'email' => $username)));
        }

        return $user;
    }

    /**
     * Login a user without a password check. He must have the 'login' role.
     * Used for social/hybridauth login flows.
     *
     * @param string|Users $user User's username or a full blown Users object to log in
     *
     * @return bool|null True if successful, false if no login role exists, null if user not found
     */
    public function login_without_password($user)
    {
        $user = $this->check_login_user_object($user);

        // If we've got nothing to work with, bail
        if (!$user) {
            return null;
        }

        // Get user's roles
        $roles = $this->get_user_roles($user);

        // User has no login role (might just need to activate first?)
        if (!isset($roles['login'])) {
            return false;
        }

        // Banned users cannot be logged in
        if ($user->banned) {
            return false;
        }

        // Regenerate session_id
        $this->session_regenerate_id();

        // Store user in session
        $this->session_store_user_and_roles($user, $roles);

        // Finish the login
        $this->complete_login($user);

        return true;
    }

    /**
     * Login a specified user without any checks whatsoever. Used for admin user switching/login-as feature.
     *
     * @param string|Users $user User's username or a full blown Users object to log in
     *
     * @return bool|null True if successful, null if User isn't found
     */
    public function login_impersonate(Users $user)
    {
        // If we've got nothing to work with, bail
        if (!$user) {
            return null;
        }

        // Get user's roles
        $roles = $this->get_user_roles($user);

        // Regenerate session_id
        $this->session_regenerate_id();

        // Store user in session
        $this->session_store_user_and_roles($user, $roles);

        return true;
    }

    /**
     * Checks if the user has a legacy hmac hash stored as a password hash in
     * the database, and if so, updates it to a bcrypt hash.
     *
     * @param $password string User's plain text password (provided on signin)
     * @param Users $user The User object representing the model/db record
     *
     * @return Users $user
     */
    protected function update_legacy_hash_if_matches($password, $user)
    {
        $legacy_hash = $this->hash($password);

        // hashed plain text matches the hashed value from the db
        // (meaning it's an old password, so let's update
        if ($legacy_hash === $user->password) {
            $user->password = $this->hash_password($password);
            $user->update();
        }

        return $user;
    }

    /**
     * Log out a user by removing the related session variables and
     * removing any autologin tokens.
     *
     * @param boolean $destroy completely destroy the session
     * @param boolean $logoutAll remove all tokens for user
     * @return boolean
     */
    public function logout($destroy = false, $logoutAll = false)
    {
        if ($this->cookies->has('authautologin')) {
            $cookie_token = $this->cookies->get('authautologin')->getValue();

            // Delete the autologin cookie to prevent re-login
            $this->cookies->set('authautologin', '', time() - 3600);
            $this->cookies->delete('authautologin');

            // Clear the autologin token from the database
            $token = Tokens::findFirst(array('token = :token:', 'bind' => array('token' => $cookie_token)));

            if ($token) {
                $token->delete();
            }

            if ($logoutAll) {
                // Delete all tokens for this user
                foreach (Tokens::find(array('user_id = :user_id:', 'bind' => array('user_id' => $token->user_id))) as $_token) {
                    $_token->delete();
                }
            }
        }

        // Destroy the session completely
        if (true === $destroy) {
            $this->session_destroy();
        } else {
            // Remove the user and user's roles from the session
            $this->session_remove_user_and_roles();

            // Regenerate session_id
            $this->session_regenerate_id();
        }

        // Double check
        return !$this->logged_in();
    }

    /**
     * Perform a hmac hash, using the configured method.
     *
     * @param string $str string to hash
     * @param string $secret Optional secret/salt, if not specified a default from config is used
     * @throws \Phalcon\Exception
     * @return string
     */
    public function hash($str, $secret = null)
    {
        if (null === $secret) {
            if (!$this->config->auth->hash_key) {
                throw new \Phalcon\Exception('A valid "hash_key" must be set in your [auth] config if no $secret parameter is present.');
            }
            $secret = $this->config->auth->hash_key;
        }

        return hash_hmac($this->config->auth->hash_method, $str, $secret);
    }

    /**
     * Generates a random string which is to be used as an auto login token.
     *
     * @return string
     */
    public function create_autologin_token()
    {
        do {
            $token = sha1(uniqid(\Phalcon\Text::random(\Phalcon\Text::RANDOM_ALNUM, 32), true));
        } while (Tokens::findFirst(array('token = :token:', 'bind' => array('token' => $token))));

        return $token;
    }


    public function get_csrf_session_token($token_key)
    {
        if ($this->session->has('$Token-' . $token_key)) {
            $token = $this->session->get('$Token-' . $token_key, null);
            $lifetime = (int) $this->session->get('$TokenTime-' . $token_key, 0);
            if (0 === $lifetime) {
                return $token;
            }

            $time = \time();
            if ($time < $lifetime) {
                return $token;
            } else {
                $this->session->remove('$Token-' . $token_key);
                $this->session->remove('$TokenTime-' . $token_key);
            }
        }
        return null;
    }

    public function check_csrf_token($token_key, $token_value)
    {
        $token = $this->get_csrf_session_token($token_key);
        if (null !== $token) {
            if ($token_value === $token) {
                return true;
            }
        }
        return false;
    }

    public function get_csrf_token($token_key, $lifetime = 0, $num_bytes = null)
    {
        if (null === $num_bytes) {
            $num_bytes = 12;
        }

        if (0 !== $lifetime) {
            $lifetime = \time() + $lifetime;
        }

        if (false === function_exists('openssl_random_pseudo_bytes')) {
            throw new \Exception('Openssl extension must be loaded');
        }
        $token = \md5(openssl_random_pseudo_bytes($num_bytes));

        $this->session->set('$Token-' . $token_key, $token);
        $this->session->set('$TokenTime-' . $token_key, $lifetime);

        return $token;
    }

    /**
     * Creates a password hash using \password_hash() function
     *
     * @param $pwd
     * @param array $options
     *
     * @return bool|false|string
     */
    public function hash_password($pwd, $options = array()) {
        $defaults = array(
            'cost' => 12
        );
        $options = array_merge($defaults, $options);
        return \password_hash($pwd, PASSWORD_DEFAULT, $options);
    }

    /**
     * Checks a plain text password and its hashed version for a match
     *
     * @param $pwd
     * @param $hashed_pwd
     *
     * @return bool
     */
    public function check_hashed_password($pwd, $hashed_pwd) {
        return \password_verify($pwd, $hashed_pwd);
    }

}

/**
 * https://github.com/ircmaxell/password_compat/blob/master/lib/password.php
 */

if (!defined('PASSWORD_DEFAULT')) {
    require(__DIR__ . '/password.php');
}
