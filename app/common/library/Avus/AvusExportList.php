<?php

namespace Baseapp\Library\Avus;

class AvusExportList extends BaseAvus
{

    public function getExportedIds(array $export_ids)
    {
        if (count($export_ids)) {
            $exported_ids_string = "'" . implode("','", $export_ids) . "'";

            return $exported_rows = $this->avus_db->find(
                "SELECT * FROM {$this->export_table_name} WHERE id IN ($exported_ids_string)"
            );
        }

        return null;
    }
}
