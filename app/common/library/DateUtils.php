<?php

namespace Baseapp\Library;

/**
 * DateUtils Library
 */
class DateUtils
{
    /**
     * Validates the date specified as a string in $in_format.
     *
     * @param string|int $date
     * @param string $in_format Optional, defaults to 'd.m.Y'
     * @param string $out_format Optional, defaults to 'Y-m-d'
     *
     * @return false|string Valid date in $out_format
     */
    public static function validateDate($date, $in_format = 'd.m.Y', $out_format = 'Y-m-d')
    {
        $d = \DateTime::createFromFormat($in_format, $date);

        if ($d && $d->format($in_format) == $date) {
            return $d->format($out_format);
        } else {
            return false;
        }
    }

    /**
     * Returns a MySQL-formatted date string.
     * If $datetime is not specified it's set to the current date and time.
     * If it is, it should be in a format accepted by \DateTime::__construct()
     *
     * @param string|int|null $datetime
     *
     * @return null|string
     */
    public static function toDB($datetime = null)
    {
        if ($datetime) {
            $d = new \DateTime($datetime);
        } else {
            $d = new \DateTime();
        }
        if ($d) {
            return $d->format('Y-m-d H:i:s');
        }
        return null;
    }

    /**
     * Returns a custom date string, formatted as "WeekDayName, dd.mm.yyyy." using Croatian weekday names.
     *
     * @param string|int|null $datetime Optional, defaults to the current date and time
     *
     * @return null|string
     */
    public static function day_date($datetime = null)
    {
        $days = array('Nedjelja', 'Ponedjeljak', 'Utorak', 'Srijeda', 'Četvrtak', 'Petak', 'Subota');

        if ($datetime) {
            $d = strtotime($datetime);
        } else {
            $d = time();
        }

        if ($d) {
            return sprintf('%s, %s', $days[(int) date('w', $d)], date('d.m.Y.', $d));
        }

        return null;
    }

    /**
     * Computes and returns the decimal difference between two dates in the specified time unit.
     * Default time unit is seconds ('s').
     *
     * @param string $unit Unit to return the difference in. Values: 'y', 'm', 'd', 'h', 'i', 's' (default)
     * @param $dt_minor
     * @param $dt_major
     * @param bool $relative Optional, defaults to false
     *
     * @return int|float
     */
    public static function diff($unit, $dt_minor, $dt_major, $relative = false){
        if (is_string($dt_minor)) {
            $dt_minor = date_create($dt_minor);
        }

        if (is_string($dt_major)) {
            $dt_major = date_create($dt_major);
        }

        $diff = date_diff($dt_minor, $dt_major, !$relative);

        $unit = strtolower($unit);
        switch ($unit) {
            case 'y':
                $total = $diff->y + $diff->m / 12 + $diff->d / 365.25;
                break;
            case 'm':
                $total= $diff->y * 12 + $diff->m + ($diff->d / 30) + ($diff->h / 24);
                break;
            case 'd':
                $total = $diff->y * 365.25 + $diff->m * 30 + $diff->d + ($diff->h / 24) + ($diff->i / 60);
                break;
            case 'h':
                $total = ($diff->y * 365.25 + $diff->m * 30 + $diff->d) * 24 + $diff->h + ($diff->i / 60);
                break;
            case 'i':
                $total = (($diff->y * 365.25 + $diff->m * 30 + $diff->d) * 24 + $diff->h) * 60 + $diff->i + ($diff->s / 60);
                break;
            case 's':
            default:
                $total = ((($diff->y * 365.25 + $diff->m * 30 + $diff->d) * 24 + $diff->h) * 60 + $diff->i) * 60 + $diff->s;
                break;
        }

        if ($diff->invert) {
            return -1 * $total;
        } else {
            return $total;
        }
    }
}
