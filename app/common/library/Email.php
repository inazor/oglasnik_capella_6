<?php

namespace Baseapp\Library;

use Phalcon\Mvc\View;

require_once __DIR__ . '/Email/class.phpmailer.php';

/**
 * Simple PHPMailer wrapper (templating/layout provided by Phalcon's View component)
 */
class Email extends \PHPMailer
{
    public function __construct()
    {
        $email = new \PHPMailer();

        // Load email config from config.ini
        if ($config = \Phalcon\Di::getDefault()->getShared('config')->email) {
            foreach ($config as $key => $value) {
                $this->$key = $value;
            }
        }

        return $email;
    }

    /**
     * Get email template and load view with params
     *
     * @param string $name View name to load
     * @param array $params Parameters to send to the view
     *
     * @return string
     */
    public function getTemplate($name, $params = array())
    {
        // we can add some default email template vars here if needed
        /*
        $config = \Phalcon\Di::getDefault()->getShared('config');
        $params = array_merge(array(
            'base_uri' => $config->app->base_uri,
            'static_uri' => $config->app->static_uri
            // ...
        ), $params);
        */
        /**
         * @var $view View
         */
        $view = \Phalcon\Di::getDefault()->getShared('view');

        $view->getRender('email', $name, $params, function($callback) {
            $callback->setRenderLevel(View::LEVEL_LAYOUT);
        });
        $contents = $view->getContent();
        return $contents;
    }

    /**
     * Rewrites email markup in order to transform/style links, or
     * any other kind of thing that will need to be done.
     *
     * @param string $html HTML markup
     *
     * @return mixed|string Rewritten or original HTML (original in case no changes have been done)
     */
    public function rewrite_markup($html)
    {
        if (!empty($html)) {
            $dom = Utils::html2dom($html);

            $links = $dom->getElementsByTagName('a');
            $modified = false;

            if ($links) {
                foreach ($links as $link) {
                    // do not mess with links that have the style attribute
                    if (!$link->hasAttribute('style')) {
                        $link->setAttribute('style', 'color:#009fe3;font-weight:bold;');
                        $modified = true;
                    }
                }
            }

            if ($modified) {
                $html = Utils::dom2html($dom);
            }
        }
        return $html;
    }

    /**
     * Prepare email - set title, recipient and body
     *
     * @param string $subject Email subject
     * @param string $to Email recipient
     * @param string $view View name to load
     * @param array $params Parameters to send to the view
     *
     * @return string
     */
    public function prepare($subject, $to, $view, $params = array())
    {
        $this->Subject = $subject;
        $this->AddAddress($to);

        // Load email content from template/view which wraps it in a layout
        $body = $this->getTemplate($view, $params);

        $body = $this->rewrite_markup($body);

        // Images are referenced via 'cid:' in layouts/email.volt
        $basedir = ROOT_PATH . '/public/assets/img/';
        $this->addEmbeddedImage($basedir . 'email-logo-header.jpg', 'email-logo-header', 'email-logo-header.jpg');
        $this->addEmbeddedImage($basedir . 'email-logo-footer.jpg', 'email-logo-footer', 'email-logo-footer.jpg');

        $this->MsgHTML($body);

        return $body;
    }

    /**
     * Overriding PHPMailer's MsgHTML in order to set our defaults
     *
     * @param string $message
     * @param string|null $basedir
     * @param bool $advanced
     *
     * @return string
     */
    public function MsgHTML($message, $basedir = null, $advanced = true) {
        if (null === $basedir) {
            $basedir = ROOT_PATH . '/public/assets/img/';
        }

        return parent::MsgHTML($message, $basedir, $advanced);
    }

    // For debugging purposes we can override send() to print/dump/log/whatever
    /*public function send()
    {
        return true;
    }*/
}
