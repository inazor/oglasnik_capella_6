<?php

namespace Baseapp\Library;

use Baseapp\Models\AdsTotalsCache;
use Baseapp\Models\Dictionaries;
use Baseapp\Models\Parameters;
use Phalcon\Di;
use Phalcon\Mvc\User\Plugin;

class LandingPagesHelpers extends Plugin
{
    public static function buildTitle($parts = array())
    {
        $page_title = 'Oglasnik.hr';

        // Remove empty elements
        $parts = array_filter($parts);

        if (!empty($parts)) {
            $page_title = implode(' - ', $parts);
            if (!empty($page_title)) {
                $page_title .= ' - Oglasnik.hr';
            }
        }

        return $page_title;
    }

    /**
     * @param string|null $category_url
     * @param string|null $requested_url
     *
     * @return array|null
     */
    public static function getLinksCollection($category_url = null, $requested_url = null)
    {
        $collection = array(
            '/osobni-kontakti' => array(
                '/osobni-kontakti/partneri' => 'Partneri',
                '/osobni-kontakti/masaze' => 'Masaže',
                '/osobni-kontakti/sexy-pribor-i-igracke' => 'Sexy pribor i igračke',
                '/osobni-kontakti/erotski-ples' => 'Erotski ples',
                '/osobni-kontakti/ostalo' => 'Ostalo',
            )
        );

        $automobili_links = self::generateAutomobiliLinksCollection();
        $links_collection = array_merge($collection, $automobili_links);

        // Return only those belonging to the specific url if requested that way
        if (null !== $category_url) {
            // Remove baseUri from $url so that we're working with host-relative links
            $di = Di::getDefault();
            if ($di->has('url')) {
                $base_uri = $di->get('url')->getBaseUri();
                // Remove the trailing slash from $base_uri so it stays in $url
                $base_uri     = rtrim($base_uri, '/');
                $category_url = str_replace($base_uri, '', $category_url);
            }

            // When given a link such as /auti-prodaja/bmw the $category_url passed to this
            // method is still just /auti-prodaja (because that's the way it used to work for
            // simple sets of landing pages links that didn't need to have a second level of links
            // TODO/FIXME: this can be done a lot smarter/better :/
            if ($category_url !== $requested_url) {
                // If we don't find an explicit match, we return $null
                $collection = null;

                // Check if we have links for $requested_url
                foreach ($links_collection as $key => $data) {
                    // If $requested_url contains/starts-with the entire $key, that's probably what we need
                    if (preg_match('/^' . preg_quote($key, '/') . '/', $requested_url)) {
                        if (isset($links_collection[$key][$requested_url])) {
                            if (isset($links_collection[$key][$requested_url]['children'])) {
                                $collection = $links_collection[$key][$requested_url]['children'];
                            }
                        }
                    }
                }
            } else {
                // Check if we have links for the $category_url
                $collection = isset($links_collection[$category_url]) ? $links_collection[$category_url] : null;
            }
        }

        return $collection;
    }

    /**
     * @param array $collection
     *
     * @return array
     */
    public static function getLinksCollectionCounts(array $collection)
    {
        $urls   = array_keys($collection);
        $counts = AdsTotalsCache::getMulti($urls);

        return $counts;
    }

    /**
     * @param $slug_or_id
     *
     * @return null|Dictionaries
     */
    public static function getParametersDictionary($slug_or_id)
    {
        $dict = null;
        if (is_int($slug_or_id)) {
            $condition = 'id = ?0';
        } else {
            $condition = 'slug = ?0';
        }

        $param = Parameters::findFirst(array($condition, 'bind' => array($slug_or_id)));
        if ($param) {
            $dict = $param->getDictionary();
            if (!$dict) {
                $dict = null;
            }
        }

        return $dict;
    }

    public static function generateAutomobiliLinksCollection()
    {
        $links = array();

        $landing_prefix = '/prodaja-automobila';
        $match_prefix = '/prodaja-automobila';

        $dict = self::getParametersDictionary('vehicle_make_model');
        if ($dict) {
            $tree = $dict->getTree();
            $hierarchy = $tree[0]->children;

            if (!empty($hierarchy)) {
                $links[$landing_prefix] = array();
            }
            foreach ($hierarchy as $node) {
                // Skip over inactive first-level nodes
                if (!$node->active) {
                    continue;
                }
                $node_slug = !empty($node->slug) ? $node->slug : Utils::slugify($node->name);
                $node_href = $landing_prefix . '/' . $node_slug;
                $links[$landing_prefix][$node_href] = array(
                    'name' => $node->name,
                    'children' => array()
                );
                // Going only 1 level deep for now
                if (!empty($node->children)) {
                    foreach ($node->children as $child_node) {
                        if (1 === $child_node->active) {
                            $child_node_slug = !empty($child_node->slug) ? $child_node->slug : Utils::slugify($child_node->name);
                            $child_node_href = $node_href . '/' . $child_node_slug;
                            $links[$landing_prefix][$node_href]['children'][$child_node_href] = $child_node->name;
                        }
                    }
                }
            }
        }

        // Create a copy for easier matching on /automobili
        if (!empty($links)) {
            $links[$match_prefix] = $links[$landing_prefix];
        }

        return $links;
    }

    public static function buildLinksListMarkup(array $links = array(), array $counts = array())
    {
        $markup = null;
        if (!empty($links)) {
            $markup .= '<ul>';
            foreach ($links as $link => $data) {
                if (is_array($data)) {
                    $title = $data['name'];
                    $anchor = '#landing';
                } else {
                    $title = $data;
                    $anchor = '';
                }
                $count = false;
                $count_markup = '';
                $count_attr = '';
                if (!empty($counts)) {
                    $count = isset($counts[$link]) ? $counts[$link] : false;
                    if ($count) {
                        $count_markup = ' <span class="text-small color-light text-count">(' . $count . ')</span>';
                        $count_attr = ' data-cnt=" (' . $count . ')"';
                    }
                }
                $markup .= '<li>';
                $markup .= '<a href="' . $link . $anchor . '"' . $count_attr . '>' . $title . '</a>';
                $markup .= '</li>';
            }
            $markup .= '</ul>';
        }

        return $markup;
    }

    public static function buildVerticalLinksMarkup($links = array(), array $counts = array(), $chunk_size = 4)
    {
        $markup = null;
        $chunked_links = array();

        if (!empty($links)) {
            $chunked_links = LandingPagesHelpers::array_partition($links, $chunk_size);
        }

        if (!empty($chunked_links)) {
            // $markup = '<div class="row no-gutters-xs black bold margin-top-sm landing-links landing-pages hidden-xs">';
            $markup = '<div class="row no-gutters-xs xs-margin-offset-minus-10px black bold margin-top-sm landing-links landing-pages">';
            foreach ($chunked_links as $chunk) {
                $markup .= '<div class="col-md-3 col-sm-3 col-xs-12">';
                $markup .= self::buildLinksListMarkup($chunk, $counts);
                $markup .= '</div>';
            }
            $markup .= '</div>';
        }

        return $markup;
    }

    public static function array_partition($list, $p)
    {
        $listlen = count( $list );
        $partlen = floor( $listlen / $p );
        $partrem = $listlen % $p;
        $partition = array();
        $mark = 0;

/*
        for ($px = 0; $px < $p; $px++) {
            $incr = ($px < $partrem) ? $partlen + 1 : $partlen;
            $partition[$px] = array_slice( $list, $mark, $incr );
            $mark += $incr;
        }
*/

        if ($partrem > 0) {
            $partlen += 1;
        }
        for ($px = 0; $px < $p; $px++) {
            $partition[$px] = array_slice( $list, $mark, $partlen );
            $mark += $partlen;
        }

        return $partition;
    }
}
