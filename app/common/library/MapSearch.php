<?php

namespace Baseapp\Library;

use Phalcon\Di;
use Baseapp\Models\CategoriesFieldsetsParameters;
use Baseapp\Models\Parameters as Parameter;
use Baseapp\Models\Categories;
use Baseapp\Traits\ParametrizatorHelpers;

class MapSearch
{
    use ParametrizatorHelpers;

    const APPLICABLE_DEFAULT_FILTERS = array(
        'ad_price' => array(),
        'ad_media' => array()
    );

    /**
     * For now, we define available filters per every category for 'Nekretnine' root, in this way
     */
    const APPLICABLE_MORE_FILTERS = array(
        // stanovi - prodaja
        '231' => array(
            //'ad_location' => array(),
            're_flat_type' => array(
                'is_searchable_type' => 'DROPDOWN'
            ),
            're_total_area' => array(
                'is_searchable_type' => 'INTERVAL_TEXT'
            ),
            're_floor_number' => array(
                'is_searchable_type' => 'DROPDOWN'
            ),
            're_condition' => array(
                'is_searchable_type' => 'DROPDOWN'
            ),
            're_construction_year' => array(),
            're_age_of_building' => array()
        ),
        // stanovi - najam
        '234' => array(
            're_flat_type' => array(
                'is_searchable_type' => 'DROPDOWN'
            ),
            're_total_area' => array(
                'is_searchable_type' => 'INTERVAL_TEXT'
            ),
            're_floor_number' => array(
                'is_searchable_type' => 'DROPDOWN'
            ),
            're_condition' => array(
                'is_searchable_type' => 'DROPDOWN'
            ),
            're_construction_year' => array(),
            're_age_of_building' => array()
        ),
        // kuće - prodaja
        '238' => array(
            're_house_type' => array(
                'is_searchable_type' => 'DROPDOWN'
            ),
            're_living_area' => array(
                'is_searchable_type' => 'INTERVAL_TEXT'
            ),
            're_house_level_numer' => array(
                'is_searchable_type' => 'DROPDOWN'
            ),
            're_condition' => array(
                'is_searchable_type' => 'DROPDOWN'
            ),
            're_construction_year' => array(),
            're_age_of_building' => array()
        ),
        // kuće - najam
        '240' => array(
            're_house_type' => array(
                'is_searchable_type' => 'DROPDOWN'
            ),
            're_living_area' => array(
                'is_searchable_type' => 'INTERVAL_TEXT'
            ),
            're_house_level_numer' => array(
                'is_searchable_type' => 'DROPDOWN'
            ),
            're_condition' => array(
                'is_searchable_type' => 'DROPDOWN'
            ),
            're_construction_year' => array(),
            're_age_of_building' => array()
        ),
        // poslovni prostor - prodaja
        '242' => array(
            're_commercial_object_type' => array(
                'is_searchable_type' => 'DROPDOWN'
            ),
            're_total_area' => array(
                'is_searchable_type' => 'INTERVAL_TEXT'
            ),
            're_floor_number' => array(
                'is_searchable_type' => 'DROPDOWN'
            ),
            're_condition' => array(
                'is_searchable_type' => 'DROPDOWN'
            ),
            're_construction_year' => array(),
            're_age_of_building' => array()
        ),
        // poslovni prostor - najam
        '244' => array(
            're_commercial_object_type' => array(
                'is_searchable_type' => 'DROPDOWN'
            ),
            're_total_area' => array(
                'is_searchable_type' => 'INTERVAL_TEXT'
            ),
            're_floor_number' => array(
                'is_searchable_type' => 'DROPDOWN'
            ),
            're_condition' => array(
                'is_searchable_type' => 'DROPDOWN'
            ),
            're_construction_year' => array(),
            're_age_of_building' => array()
        ),
        // zemljišta - prodaja
        '246' => array(
            're_land_type' => array(
                'is_searchable_type' => 'DROPDOWN'
            ),
            're_total_area' => array(
                'is_searchable_type' => 'INTERVAL_TEXT'
            ),
            're_permits' => array(
                'is_searchable_type' => 'DROPDOWN_MULTIPLE'
            )
        ),
        // zemljišta - najam
        '248' => array(
            're_land_type' => array(
                'is_searchable_type' => 'DROPDOWN'
            ),
            're_total_area' => array(
                'is_searchable_type' => 'INTERVAL_TEXT'
            ),
            're_permits' => array(
                'is_searchable_type' => 'DROPDOWN_MULTIPLE'
            )
        ),
        // garaže - prodaja
        '254' => array(
            're_parking_type' => array(
                'is_searchable_type' => 'DROPDOWN'
            ),
            're_total_area' => array(
                'is_searchable_type' => 'INTERVAL_TEXT'
            )
        ),
        // garaže - najam
        '256' => array(
            're_parking_type' => array(
                'is_searchable_type' => 'DROPDOWN'
            ),
            're_total_area' => array(
                'is_searchable_type' => 'INTERVAL_TEXT'
            )
        ),
        // nekretnine za odmor - prodaja
        '250' => array(
            're_vacation_type' => array(
                'is_searchable_type' => 'DROPDOWN'
            ),
            're_living_area' => array(
                'is_searchable_type' => 'INTERVAL_TEXT'
            ),
            're_condition' => array(
                'is_searchable_type' => 'DROPDOWN'
            ),
            're_construction_year' => array(),
            're_energy_certificate' => array()
        ),
        // nekretnine za odmor - najam
        '251' => array(
            're_vacation_type' => array(
                'is_searchable_type' => 'DROPDOWN'
            )
        ),
        // ostalo - prodaja
        '260' => array(
            're_total_area' => array(
                'is_searchable_type' => 'INTERVAL_TEXT'
            ),
            're_energy_certificate' => array()
        ),
        // ostalo - najam
        '262' => array(
            're_total_area' => array(
                'is_searchable_type' => 'INTERVAL_TEXT'
            ),
            're_energy_certificate' => array()
        )
    );

    const FILTERS_INFO = array(
        // stanovi - prodaja
        '231' => array(
            'right' => array(
                'ad_params_44' => array(   //re_total_area
                    'suffix' => ' m²'
                )
            )
        ),
        // stanovi - najam
        '234' => array(
            'right' => array(
                'ad_params_44' => array(   //re_total_area
                    'suffix' => ' m²'
                )
            )
        ),
        // kuće - prodaja
        '238' => array(
            'right' => array(
                'ad_params_45' => array(   //re_total_area
                    'suffix' => ' m²'
                )
            )
        ),
        // kuće - najam
        '240' => array(
            'right' => array(
                'ad_params_45' => array(   //re_total_area
                    'suffix' => ' m²'
                )
            )
        ),
        // poslovni prostor - prodaja
        '242' => array(
            'right' => array(
                'ad_params_44' => array(   //re_total_area
                    'suffix' => ' m²'
                )
            )
        ),
        // poslovni prostor - najam
        '244' => array(
            'right' => array(
                'ad_params_44' => array(   //re_total_area
                    'suffix' => ' m²'
                )
            )
        ),
        // zemljišta - prodaja
        '246' => array(
            'right' => array(
                'ad_params_44' => array(   //re_total_area
                    'suffix' => ' m²'
                )
            )
        ),
        // zemljišta - najam
        '248' => array(
            'right' => array(
                'ad_params_44' => array(   //re_total_area
                    'suffix' => ' m²'
                )
            )
        ),
        // garaže - prodaja
        '254' => array(
            'right' => array(
                'ad_params_44' => array(   //re_total_area
                    'suffix' => ' m²'
                )
            )
        ),
        // garaže - najam
        '256' => array(
            'right' => array(
                'ad_params_44' => array(   //re_total_area
                    'suffix' => ' m²'
                )
            )
        ),
        // nekretnine za odmor - prodaja
        '250' => array(
            'right' => array(
                'ad_params_45' => array(   //re_total_area
                    'suffix' => ' m²'
                )
            )
        ),
        // nekretnine za odmor - najam
        '251' => array(),
        // ostalo - prodaja
        '260' => array(
            'right' => array(
                'ad_params_44' => array(   //re_total_area
                    'suffix' => ' m²'
                )
            )
        ),
        // ostalo - najam
        '262' => array(
            'right' => array(
                'ad_params_44' => array(   //re_total_area
                    'suffix' => ' m²'
                )
            )
        )
    );

    const SKIP_CATEGORIES = array(
        27,     // Sobe i cimeri
        32,     // Grobna mjesta
        839,    // Montažni objekti
        838     // Novogradnja (currently inactive)
    );

    private $data = null;
    private $loggedInUser = null;
    private $sortParamData = null;
    private $categoryID = null;

    public static function getInfoFilters($categoryID)
    {
        $infoFilters = null;

        $filtersInfo = self::FILTERS_INFO;

        if (isset($filtersInfo[$categoryID]) && count($filtersInfo[$categoryID])) {
            $infoFilters = array();
            if (isset($filtersInfo[$categoryID]['left']) && count($filtersInfo[$categoryID]['left'])) {
                $infoFilters['left'] = $filtersInfo[$categoryID]['left'];
            }
            if (isset($filtersInfo[$categoryID]['right']) && count($filtersInfo[$categoryID]['right'])) {
                $infoFilters['right'] = $filtersInfo[$categoryID]['right'];
            }
        }

        return $infoFilters;
    }

    public function __construct($data = null)
    {
        $this->setData($data);
    }

    public function setData($data = null)
    {
        $this->data = $data;
    }

    public function setLoggedInUser($loggedInUser = null)
    {
        $this->loggedInUser = $loggedInUser;
    }

    public function setSortParamData($sortParamData = null)
    {
        $this->sortParamData = $sortParamData;
    }

    public function setCategoryID($categoryID = null)
    {
        $this->categoryID = $categoryID;
    }

    /**
     * Helper method to get all default and more filters that were given for selected categories prefilled with $this->data
     * 
     * @return array
     */
    public function getSpecificFilters()
    {
        $defaultFiltersArray = self::APPLICABLE_DEFAULT_FILTERS;
        $moreFiltersArray    = self::APPLICABLE_MORE_FILTERS;

        $output = array(
            'html'    => array(),
            'js'      => array(),
            'filters' => array(),
            'assets'  => array(
                'css' => array(),
                'js'  => array()
            )
        );
        $inRow      = false;
        $isLastRow  = false;
        $conditions = array();

        foreach ($moreFiltersArray as $categoryID => $parameter_slugs) {
            $parametersArray = array();
            if (count($defaultFiltersArray)) {
                $parametersArray = array_keys($defaultFiltersArray);
            }
            if (count($parameter_slugs)) {
                $parametersArray = array_merge($parametersArray, array_keys($parameter_slugs));
            }
            if (count($parametersArray)) {
                $conditions[] = "(category_id = $categoryID AND parameter_slug IN ('" . implode("','", $parametersArray) . "'))";
            }
        }

        $fieldsetParameters = null;

        if (count($conditions)) {
            $fieldsetParameters = CategoriesFieldsetsParameters::find(array(
                'conditions' => implode(' OR ', $conditions)
            ));
        }

        $currentlyFilteredCategoryID = isset($this->data['category_id']) && (int) $this->data['category_id'] ? (int) $this->data['category_id'] : null;
        $includedDefaultFilters      = array();
        foreach(array_keys($defaultFiltersArray) as $defaultFilterName) {
            $includedDefaultFilters[$defaultFilterName] = false;
        }
 
        if (null !== $fieldsetParameters && count($fieldsetParameters)) {
            $append_to_output = function ($categoryID, $render_data, $lastRow) use (&$output, &$inRow, &$lastCategoryID, &$includedDefaultFilters, $currentlyFilteredCategoryID) {

                if ($render_data && isset($render_data['html']) && '' !== trim($render_data['html'])) {
                    $paramHTML    = '';
                    $openRow      = false;
                    $closeRow     = false;
                    $lastInRow    = $lastRow;
                    $fullRowParam = $render_data['full_row'];

                    if ($lastCategoryID !== $categoryID) {
                        if ($lastCategoryID || $fullRowParam) {
                            $closeRow = !$lastCategoryID ? false : true;
                            $lastInRow = true;
                        }
                        $openRow = true;
                        $lastCategoryID = $categoryID;
                    } elseif (!$inRow || $fullRowParam) {
                        $openRow = true;
                        if ($fullRowParam) {
                            $closeRow = true;
                        }
                    }

                    if ($closeRow) {
                        if ($fullRowParam) {
                            $paramHTML .= '    <!-- closed by closeRow in fullRowParam -->' . PHP_EOL;
                        }
                        $paramHTML .= '</div>';
                        $inRow = false;
                    }
                    if ($openRow) {
                        $paramHTML .= '<div class="row">' . PHP_EOL;
                        $inRow = true;
                    }
                    $paramHTML .= $render_data['html'];

                    if ($fullRowParam || $lastInRow) {
                        $inRow      = false;
                        if ($fullRowParam) {
                            $paramHTML .= '    <!-- closed by fullRowParam -->' . PHP_EOL;
                        }
                        if ($lastInRow) {
                            $paramHTML .= '    <!-- closed by lastInRow -->' . PHP_EOL;
                        }
                        $paramHTML .= '</div>';
                    }

                    $output['html'][$categoryID][] = $paramHTML;
                }

                if ($currentlyFilteredCategoryID == $categoryID) {
                    if (isset($render_data['filter']) && is_array($render_data['filter'])) {
                        $output['filters'][] = $render_data['filter'];
                    }
                }

                // try to filter multiple default filters out... add them only once
                $addAssets = true;
                if (isset($render_data['name']) && isset($includedDefaultFilters[$render_data['name']]) && true === $includedDefaultFilters[$render_data['name']]) {
                    $addAssets = false;
                }

                if ($addAssets) {
                    if (isset($render_data['js']) && trim($render_data['js'])) {
                        $output['js'][] = $render_data['js'];
                    }
                    if (isset($render_data['assets']) && is_array($render_data['assets'])) {
                        if (isset($render_data['assets']['css']) && is_array($render_data['assets']['css'])) {
                            $output['assets']['css'] = array_merge($output['assets']['css'], $render_data['assets']['css']);
                        }

                        if (isset($render_data['assets']['js']) && is_array($render_data['assets']['js'])) {
                            $output['assets']['js'] = array_merge($output['assets']['js'], $render_data['assets']['js']);
                        }
                    }
                    if (isset($render_data['name']) && isset($includedDefaultFilters[$render_data['name']])) {
                        $includedDefaultFilters[$render_data['name']] = true;
                    }
                }
            };

            $lastFilterIndex = 0;

            $groupedRenderedParameters = array();

            foreach ($fieldsetParameters as $fieldsetParameter) {
                $dbParameter = Parameter::getCached($fieldsetParameter->parameter_id);

                $module = 'frontend';
                $parameter = $this->getParameterSubclass($dbParameter->type_id, $module);
                if ($parameter) {
                    $parameter->setModule($module);
                    $parameter->setFieldsetParameter($fieldsetParameter);

                    if (isset($moreFiltersArray[$fieldsetParameter->category_id][$fieldsetParameter->parameter_slug])) {
                        $moreFiltersArrayParameter = $moreFiltersArray[$fieldsetParameter->category_id][$fieldsetParameter->parameter_slug];
                        if (count($moreFiltersArrayParameter)) {
                            if (isset($moreFiltersArrayParameter['is_searchable_type'])) {
                                $isSearchableType = $moreFiltersArrayParameter['is_searchable_type'];

                                if (!isset($parameter->is_searchable) || !$parameter->is_searchable) {
                                    $parameter->is_searchable = 1;
                                }

                                $parameter->is_searchable_type = $isSearchableType;
                            }
                        }
                    }
                    $parameter->setData($this->data);
                    $render_data = $parameter->getMapSearchFilterMarkup();
                    $groupedRenderedParameters[$fieldsetParameter->category_id][] = $render_data;
                }
            }

            $lastCategoryID = null;
            foreach ($groupedRenderedParameters as $categoryID => $renderedParameters) {
                $totalRenderedParameters = count($renderedParameters);
                foreach ($renderedParameters as $i => $renderedParameter) {
                    $lastRow = ($i == ($totalRenderedParameters - 1));
                    $append_to_output($categoryID, $renderedParameter, $lastRow);
                }
                // in case we're in row, and we're done with all the parameters, we have to close last opened row...
                if ($inRow) {
                    $paramHTML  = '    <!-- closed by manualRowClose -->' . PHP_EOL;
                    $paramHTML .= '</div>';

                    $output['html'][$categoryID][] = $paramHTML;
                }
                $inRow = $lastCategoryID = null;
            }
        }

        if (count($output['html'])) {
            $html = array();
            foreach($output['html'] as $categoryID => $filters) {
                $rowHtml = '<div class="category-specific-filters" data-category-id="' . $categoryID . '" style="display:none">' . PHP_EOL . PHP_EOL . implode(PHP_EOL, $filters) . PHP_EOL . '</div>';
                $html[] = $rowHtml;
            }
            $output['html'] = $html;

        }

        return $output;
    }

    /**
     * Helper method to filter ads based on given bounds and custom filter's generated SQL
     * @param  \stdClass  $bounds      Bounds where we'll search ads in
     * @param  array      $filtersSQL  Prepared custom SQL filters
     * @return array
     */
    public function searchAds($bounds, $filtersSQL = null, $itemsPerPage = null)
    {
        $md5Array = array('bounds' => $bounds);
        if ($filtersSQL) {
            $md5Array['filtersSQL'] = $filtersSQL;
        }

        // Get users ads listing
        $adsFiltering = new AdsFiltering();
        $adsFiltering->setItemsPerPage($itemsPerPage);
        $adsFiltering->gpsBounds($bounds);
        $adsFiltering->cacheResults(true);
        $mapMD5 = md5(json_encode($md5Array));
        $adsFiltering->cachePrefix('map-' . $mapMD5 . '-ads');
        $adsFiltering->cacheLifeTime(120);
        if ($filtersSQL) {
            $adsFiltering->setFilters($filtersSQL);
        }
        $adsFiltering->setLoggedInUser($this->loggedInUser);
        $adsFiltering->setSortParamData($this->sortParamData);
        $adsFiltering->allowSpecialProducts(true);
        $adsFiltering->setBaseURL('search/map');
        $adsFiltering->showOnlyRootCategoriesCounts(false);
        $adsFiltering->alwaysShowCompleteCategoriesTree(false);
        if ($this->categoryID) {
            $adsFiltering->setAdCategoryByID($this->categoryID);
        }
        $ads = $adsFiltering->getAds();

        $result = array(
            'md5'        => $mapMD5,
            'hasError'   => $adsFiltering->hasError(),
            'trigger404' => $adsFiltering->shouldTrigger404()
        );
        if ($flashSession = $adsFiltering->getFlashSession()) {
            $result['flashSession'] = $flashSession;
        }
        if (!$result['hasError'] && $ads) {
            $result['ads'] = $ads;
            if ($pagination = $adsFiltering->getPagination()) {
                $result['pagination'] = $pagination;
            }
        }

        return $result;
    }

    public static function getRootMapSearchableCategory($categoryID = null)
    {
        $rootMapSearchableCategory = null;

        if ($categoryID) {
            $di           = Di::getDefault();
            $categoryTree = $di->get(Categories::MEMCACHED_KEY);

            if ($categoryTree && isset($categoryTree[$categoryID])) {
                $currentCategory = $categoryTree[$categoryID];

                if (isset($currentCategory->top_parent_id) && $currentCategory->top_parent_id && isset($categoryTree[$currentCategory->top_parent_id])) {
                    $topParent = $categoryTree[$currentCategory->top_parent_id];
                    if (isset($topParent->json) && isset($topParent->json->search_by_location) && $topParent->json->search_by_location) {
                        $rootMapSearchableCategory = $topParent;
                    }
                }
            }
        }

        return $rootMapSearchableCategory;
    }
}
