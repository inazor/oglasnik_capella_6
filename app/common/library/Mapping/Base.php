<?php

namespace Baseapp\Library\Mapping;

use Baseapp\Models\Categories;
use Baseapp\Library\Utils;

class Base
{
    public $offline_category_id = null;
    public $online_category_id = null;
}
