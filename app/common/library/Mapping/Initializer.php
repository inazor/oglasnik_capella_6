<?php

namespace Baseapp\Library\Mapping;

use Baseapp\Library\RawDB;
use Baseapp\Models\Parameters as Parameter;
use Baseapp\Models\Dictionaries as Dictionary;

class Initializer
{
    private $di;

    public function __construct($options = null)
    {
        $this->di = \Phalcon\Di::getDefault();
    }

    public function deleteAllDictionaries()
    {
        $dictionaries = Dictionary::find();
        if ($dictionaries) {
            echo 'Deleting old dictionaries...' . PHP_EOL;
            foreach ($dictionaries as $dictionary) {
                $dictionary->delete();
            }
            $db = new RawDB();
            $db->query('ALTER TABLE dictionaries AUTO_INCREMENT =1;');
        }
    }

    public function cars()
    {
        $data = array(
            "alfa_romeo" => array(
                "name" => "Alfa Romeo",
                "items" => array(
                    "145",
                    "146",
                    "147",
                    "149",
                    "155",
                    "156",
                    "159",
                    "164",
                    "166",
                    "33",
                    "75",
                    "8C",
                    "90",
                    "Alfasud",
                    "Alfetta",
                    "Arna",
                    "Brera",
                    "GT",
                    "GTV",
                    "Giulia",
                    "Giulietta",
                    "MiTo",
                    "RZ/SZ",
                    "Spider",
                    "Sprint",
                    "Ostalo"
                )
            ) ,
            "aston_martin" => array(
                "name" => "Aston Martin",
                "items" => array(
                    "AR1 ",
                    "DB",
                    "DB7 ",
                    "DB9 ",
                    "DBS ",
                    "Lagonda ",
                    "Rapide",
                    "Vanquish  ",
                    "V8",
                    "Vantage ",
                    "Virage",
                    "Volante ",
                    "Ostalo",
                )
            ) ,
            "audi" => array(
                "name" => "Audi",
                "items" => array(
                    "100 ",
                    "200 ",
                    "80",
                    "90",
                    "A1",
                    "A2",
                    "A3",
                    "A4",
                    "A4 Allroad",
                    "A5",
                    "A6",
                    "A6 Allroad",
                    "A8",
                    "Q5",
                    "Q7",
                    "QUATTRO ",
                    "R8",
                    "R82 ",
                    "RS2 ",
                    "RS3 ",
                    "RS4 ",
                    "RS5 ",
                    "RS6 ",
                    "S2",
                    "S3",
                    "S4",
                    "S5",
                    "S6",
                    "S8",
                    "TT",
                    "V8",
                    "Ostalo",
                )
            ) ,
            "bentley" => array(
                "name" => "Bentley",
                "items" => array(
                    "Arnage",
                    "Azure ",
                    "Brooklands",
                    "Continental ",
                    "Continental GT",
                    "Eight ",
                    "Mulsanne  ",
                    "Turbo R ",
                    "Turbo RT  ",
                    "Turbo S ",
                    "Ostalo",
                )
            ) ,
            "bmw" => array(
                "name" => "BMW",
                "items" => array(
                    "116 ",
                    "118 ",
                    "120 ",
                    "123 ",
                    "125 ",
                    "130 ",
                    "135 ",
                    "315 ",
                    "316 ",
                    "318 ",
                    "320 ",
                    "323 ",
                    "324 ",
                    "325 ",
                    "328 ",
                    "330 ",
                    "335 ",
                    "518 ",
                    "520 ",
                    "523 ",
                    "524 ",
                    "525 ",
                    "528 ",
                    "530 ",
                    "535 ",
                    "540 ",
                    "545 ",
                    "550 ",
                    "628 ",
                    "630 ",
                    "633 ",
                    "635 ",
                    "645 ",
                    "650 ",
                    "725 ",
                    "728 ",
                    "730 ",
                    "732 ",
                    "735 ",
                    "740 ",
                    "745 ",
                    "750 ",
                    "760 ",
                    "840 ",
                    "850 ",
                    "M1",
                    "M3",
                    "M5",
                    "M6",
                    "X1",
                    "X3",
                    "X5",
                    "X6",
                    "Z1",
                    "Z3",
                    "Z3 M  ",
                    "Z4",
                    "Z4 M  ",
                    "Z5",
                    "Z8",
                    "Ostalo",
                )
            ) ,
            "buick" => array(
                "name" => "Buick",
                "items" => array(
                    "Century ",
                    "Electra ",
                    "Enclave ",
                    "La Crosse ",
                    "Le Sabre  ",
                    "Park Avenue ",
                    "Regal ",
                    "Riviera ",
                    "Roadmaster",
                    "Skylark ",
                    "Ostalo",
                )
            ) ,
            "cadillac" => array(
                "name" => "Cadillac",
                "items" => array(
                    "Allante ",
                    "BLS ",
                    "Coupe de Ville",
                    "CTS ",
                    "Eldorado  ",
                    "Escalade  ",
                    "Fleetwood ",
                    "Seville ",
                    "SRX ",
                    "STS ",
                    "XLR ",
                    "Ostalo",
                )
            ) ,
            "chevrolet" => array(
                "name" => "Chevrolet",
                "items" => array(
                    "Avalanche ",
                    "Aveo  ",
                    "Beretta ",
                    "Blazer",
                    "Camaro",
                    "Caprice ",
                    "Captiva ",
                    "Cavalier  ",
                    "Chevy Van ",
                    "Citation  ",
                    "Corsica ",
                    "Cruze ",
                    "El Camino ",
                    "Epica ",
                    "Evanda",
                    "HHR ",
                    "Impala",
                    "Kalos ",
                    "Korando ",
                    "Lacetti ",
                    "Lumina",
                    "Malibu",
                    "Matiz ",
                    "Musso ",
                    "Nexia ",
                    "Nubira",
                    "S-11  ",
                    "SSR ",
                    "Sequel",
                    "Silverado ",
                    "Spark ",
                    "Suburban  ",
                    "Tacuma",
                    "Tahoe ",
                    "Trailblazer ",
                    "Traverse  ",
                    "Venture ",
                )
            ) ,
            "chrysler" => array(
                "name" => "Chrysler",
                "items" => array(
                    "300C  ",
                    "300M  ",
                    "Concorde  ",
                    "Crossfire ",
                    "Daytona ",
                    "ES",
                    "GS",
                    "GTS ",
                    "Grand Voyager ",
                    "Le Baron  ",
                    "Neon  ",
                    "New Yorker",
                    "PT Cruiser",
                    "Saratoga  ",
                    "Sebring ",
                    "Stratus ",
                    "Town and Country  ",
                    "Vision",
                    "Voyager ",
                    "Ostalo",
                )
            ) ,
            "citroen" => array(
                "name" => "Citroen",
                "items" => array(
                    "2 CV  ",
                    "AX",
                    "Berlingo  ",
                    "BX",
                    "C-Crosser ",
                    "C1",
                    "C2",
                    "C3",
                    "C3 Pluriel",
                    "C3 Picasso",
                    "C4",
                    "C4 Picasso",
                    "C5",
                    "C6",
                    "C8",
                    "CX",
                    "DS",
                    "DS3 ",
                    "DS4 ",
                    "DS5 ",
                    "Evasion ",
                    "Grand C4 Picasso  ",
                    "GSA ",
                    "Jumpy ",
                    "SAXO  ",
                    "Visa  ",
                    "Xantia",
                    "XM",
                    "Xsara ",
                    "Xsara Picasso ",
                    "ZX",
                    "Ostalo",
                )
            ) ,
            "dacia" => array(
                "name" => "Dacia",
                "items" => array(
                    "Duster",
                    "Logan ",
                    "Logan MCV ",
                    "Logan VAN ",
                    "Nova  ",
                    "Pick-up ",
                    "Sandero ",
                    "Solenza ",
                    "Supernova ",
                    "Ostalo",
                )
            ) ,
            "daewoo" => array(
                "name" => "Daewoo",
                "items" => array(
                    "Espero",
                    "Evanda",
                    "Kalos ",
                    "Korando ",
                    "Lacetti ",
                    "Lanos ",
                    "Leganza ",
                    "Matiz ",
                    "Musso ",
                    "Nexia ",
                    "Nubira",
                    "Tacuma",
                    "Tico  ",
                    "Ostalo",
                )
            ) ,
            "daihatsu" => array(
                "name" => "Daihatsu",
                "items" => array(
                    "Applause  ",
                    "Charade ",
                    "Charmant  ",
                    "Copen ",
                    "Cuore ",
                    "Feroza/Sportrak ",
                    "Freeclimber ",
                    "Gran Move ",
                    "Hijet ",
                    "Materia ",
                    "Move  ",
                    "Rocky/Fourtrak",
                    "Sirion",
                    "Terios",
                    "Trevis",
                    "YRV ",
                    "Ostalo",
                )
            ) ,
            "dodge" => array(
                "name" => "Dodge",
                "items" => array(
                    "Avenger ",
                    "Caliber ",
                    "Challenger",
                    "Charger ",
                    "Dakota",
                    "Durango ",
                    "Grand Caravan ",
                    "Journey ",
                    "Magnum",
                    "Neon  ",
                    "Nitro ",
                    "RAM ",
                    "Stealth ",
                    "Viper ",
                    "Ostalo",
                )
            ) ,
            "ferrari" => array(
                "name" => "Ferrari",
                "items" => array(
                    "208 ",
                    "288 ",
                    "328 ",
                    "348 ",
                    "355 ",
                    "360 ",
                    "400 ",
                    "456 ",
                    "458 Italia",
                    "550 ",
                    "575 ",
                    "599 ",
                    "612 ",
                    "California",
                    "DINO GT4  ",
                    "Enzo Ferrari  ",
                    "F355  ",
                    "F40 ",
                    "F430  ",
                    "F50 ",
                    "F512  ",
                    "FF",
                    "Mondial ",
                    "Testarossa",
                    "Ostalo",
                )
            ) ,
            "fiat" => array(
                "name" => "Fiat",
                "items" => array(
                    "124 ",
                    "126 ",
                    "127 ",
                    "130 ",
                    "131 ",
                    "132 ",
                    "500 ",
                    "Argenta ",
                    "Barchetta ",
                    "Brava ",
                    "Bravo ",
                    "Cinquecento ",
                    "Coupe ",
                    "Croma ",
                    "Doblo ",
                    "Fiorino ",
                    "Grande Punto  ",
                    "Idea  ",
                    "Linea ",
                    "Marea ",
                    "Marea Weekend ",
                    "Marengo ",
                    "Multipla  ",
                    "Palio ",
                    "Palio Weekend ",
                    "Panda ",
                    "Punto ",
                    "Punto Cabrio  ",
                    "Regata",
                    "Ritmo ",
                    "Scudo ",
                    "Sedici",
                    "Seicento  ",
                    "Spider",
                    "Stilo ",
                    "Stilo SW  ",
                    "Strada",
                    "Tempra",
                    "Tempra SW ",
                    "Tipo  ",
                    "Ulysse",
                    "Uno ",
                    "X 1/9 ",
                    "Ostalo",
                )
            ) ,
            "ford" => array(
                "name" => "Ford",
                "items" => array(
                    "Bronco",
                    "C-Max ",
                    "Capri ",
                    "Cougar",
                    "Econoline ",
                    "Edge  ",
                    "Escape",
                    "Escort",
                    "Excursion ",
                    "Expedition",
                    "Explorer  ",
                    "F 150 ",
                    "F 250 ",
                    "F 350 ",
                    "Falcon",
                    "Fiesta",
                    "Focus ",
                    "Focus C-Max ",
                    "Fusion",
                    "Galaxy",
                    "Granada ",
                    "GT",
                    "Ka",
                    "Kuga  ",
                    "Maverick  ",
                    "Mondeo",
                    "Mustang ",
                    "Orion ",
                    "Probe ",
                    "Puma  ",
                    "Ranger",
                    "S-Max ",
                    "Scorpio ",
                    "Sierra",
                    "StreetKa  ",
                    "Taunus",
                    "Taurus",
                    "Thunderbird ",
                    "Tourneo ",
                    "Windstar  ",
                    "Ostalo",
                )
            ) ,
            "gmc" => array(
                "name" => "GMC",
                "items" => array(
                    "Acadia",
                    "Envoy ",
                    "Safari",
                    "Savana",
                    "Sierra",
                    "Sonoma",
                    "Syclone ",
                    "Typhoon ",
                    "Vandura ",
                    "Yukon ",
                    "Ostalo",
                )
            ) ,
            "honda" => array(
                "name" => "Honda",
                "items" => array(
                    "Accord",
                    "Accord Aerodeck ",
                    "Civic ",
                    "Civic Aerodeck",
                    "Concerto  ",
                    "CR-V  ",
                    "CR-Z  ",
                    "CRX ",
                    "Element ",
                    "FR-V  ",
                    "HR-V  ",
                    "Insight ",
                    "Integra ",
                    "Jazz  ",
                    "Legend",
                    "Logo  ",
                    "NSX ",
                    "Odyssey ",
                    "Prelude ",
                    "S2000 ",
                    "Shuttle ",
                    "Stream",
                    "Ostalo",
                )
            ) ,
            "hummer" => array(
                "name" => "Hummer",
                "items" => array(
                    "H1",
                    "H2",
                    "H3",
                    "Ostalo",
                )
            ) ,
            "hyundai" => array(
                "name" => "Hyundai",
                "items" => array(
                    "Accent",
                    "Atos  ",
                    "Coupe ",
                    "Elantra ",
                    "Excel ",
                    "Galloper  ",
                    "Getz  ",
                    "Grandeur  ",
                    "H-1 ",
                    "i10 ",
                    "i20 ",
                    "i30 ",
                    "i30 CW",
                    "i40 ",
                    "i40 CW",
                    "i50 ",
                    "ix55  ",
                    "Lantra",
                    "Matrix",
                    "Pony  ",
                    "S-Coupe ",
                    "Santa Fe  ",
                    "Santamo ",
                    "Sonata",
                    "Terracan  ",
                    "Trajet",
                    "Tucson",
                    "XG 30 ",
                    "XG 350",
                    "Ostalo",
                )
            ) ,
            "infinity" => array(
                "name" => "Infinity",
                "items" => array(
                    "EX35  ",
                    "EX37  ",
                    "FX",
                    "G35 ",
                    "G37 ",
                    "I35 ",
                    "M45 ",
                    "Q45 ",
                    "QX56  ",
                    "Ostalo",
                )
            ) ,
            "isuzu" => array(
                "name" => "Isuzu",
                "items" => array(
                    "Amigo ",
                    "Bighorn ",
                    "Campo ",
                    "D-Max ",
                    "Gemini",
                    "Midi  ",
                    "Rodeo ",
                    "Trooper ",
                    "Ostalo",
                )
            ) ,
            "jaguar" => array(
                "name" => "Jaguar",
                "items" => array(
                    "Daimler ",
                    "E-Type",
                    "S-Type",
                    "Sovereign ",
                    "X-Type",
                    "XF",
                    "XJ",
                    "XJ12  ",
                    "XJ40  ",
                    "XJ6 ",
                    "XJ8 ",
                    "XJR ",
                    "XJS ",
                    "XJSC  ",
                    "XK",
                    "XK8 ",
                    "XKR ",
                    "Ostalo",
                )
            ) ,
            "jeep" => array(
                "name" => "Jeep",
                "items" => array(
                    "Cherokee  ",
                    "CJ",
                    "Comanche  ",
                    "Commander ",
                    "Compass ",
                    "Grand Cherokee",
                    "Patriot ",
                    "Renegade  ",
                    "Wagoneer  ",
                    "Wilys ",
                    "Wrangler  ",
                    "Ostalo",
                )
            ) ,
            "kia" => array(
                "name" => "Kia",
                "items" => array(
                    "Besta ",
                    "Borrego ",
                    "Carens",
                    "Carnival  ",
                    "cee'd ",
                    "Cerato",
                    "Clarus",
                    "Elan  ",
                    "Joice ",
                    "Leo ",
                    "Magentis  ",
                    "Mentor",
                    "Opirus",
                    "Optima",
                    "Picanto ",
                    "Pregio",
                    "Pride ",
                    "pro_cee'd ",
                    "Retona",
                    "Rio ",
                    "Roadster  ",
                    "Rocsta",
                    "Sedona",
                    "Sephia",
                    "Shuma ",
                    "Sorento ",
                    "Soul  ",
                    "Sportage  ",
                    "Venga ",
                    "Ostalo",
                )
            ) ,
            "lada" => array(
                "name" => "Lada",
                "items" => array(
                    "110 ",
                    "111 ",
                    "112 ",
                    "1200  ",
                    "1500  ",
                    "2101  ",
                    "2107  ",
                    "2110  ",
                    "2111  ",
                    "2112  ",
                    "Aleko ",
                    "Kalina",
                    "Niva  ",
                    "Nova  ",
                    "Priora",
                    "Samara",
                    "Ostalo",
                )
            ) ,
            "lamborghini" => array(
                "name" => "Lamborghini",
                "items" => array(
                    "Aventador ",
                    "Countach  ",
                    "Diablo",
                    "Gallardo  ",
                    "Jalpa ",
                    "LM002 ",
                    "Murcielago",
                    "Reventon  ",
                    "Ostalo",
                )
            ) ,
            "lancia" => array(
                "name" => "Lancia",
                "items" => array(
                    "Beta  ",
                    "Dedra ",
                    "Delta ",
                    "Fulvia",
                    "Gamma ",
                    "Kappa ",
                    "Lybra ",
                    "Musa  ",
                    "Phedra",
                    "Prisma",
                    "Stratos ",
                    "Thema ",
                    "Thesis",
                    "Trevi ",
                    "Y 10  ",
                    "Y/Ypsilon ",
                    "Zeta  ",
                    "Ostalo",
                )
            ) ,
            "land_rover" => array(
                "name" => "Land Rover",
                "items" => array(
                    "Defender  ",
                    "Discovery ",
                    "Freelander",
                    "Range Rover ",
                    "Range Rover Evoque",
                    "Range Rover Sport ",
                    "Ostalo",
                )
            ) ,
            "lexus" => array(
                "name" => "Lexus",
                "items" => array(
                    "ES",
                    "GS",
                    "GX",
                    "IS",
                    "LS",
                    "LX",
                    "RX",
                    "SC",
                    "Ostalo",
                )
            ) ,
            "lincoln" => array(
                "name" => "Lincoln",
                "items" => array(
                    "Aviator ",
                    "Continental ",
                    "LS",
                    "Mark  ",
                    "Navigator ",
                    "Town car  ",
                    "Ostalo",
                )
            ) ,
            "lotus" => array(
                "name" => "Lotus",
                "items" => array(
                    "340r  ",
                    "Elan  ",
                    "Elise ",
                    "Esprit",
                    "Evora ",
                    "Excel ",
                    "Exige ",
                    "Super seven ",
                    "Ostalo",
                )
            ) ,
            "mahindra" => array(
                "name" => "Mahindra",
                "items" => array(
                    "Bolero",
                    "Goa ",
                    "Ostalo",
                )
            ) ,
            "maserati" => array(
                "name" => "Maserati",
                "items" => array(
                    "222 ",
                    "224 ",
                    "228 ",
                    "3200  ",
                    "418 ",
                    "420 ",
                    "4200  ",
                    "422 ",
                    "424 ",
                    "430 ",
                    "Biturbo ",
                    "Coupe ",
                    "Ghilbi",
                    "Grancabrio",
                    "Gransport ",
                    "Granturismo ",
                    "Quattroporte  ",
                    "Racing",
                    "Shamal",
                    "Spyder",
                    "Ostalo",
                )
            ) ,
            "mazda" => array(
                "name" => "Mazda",
                "items" => array(
                    "121 ",
                    "2 ",
                    "3 ",
                    "323 ",
                    "323 F ",
                    "323 P ",
                    "323 HB",
                    "5 ",
                    "6 ",
                    "626 ",
                    "929 ",
                    "BT-50 ",
                    "CX-5  ",
                    "CX-7  ",
                    "CX-9  ",
                    "Demio ",
                    "Millenia  ",
                    "MPV ",
                    "MX-3  ",
                    "MX-5  ",
                    "MX-6  ",
                    "Premacy ",
                    "Protege ",
                    "RX-7  ",
                    "RX-8  ",
                    "Tribute ",
                    "Xedos 6 ",
                    "Xedos 9 ",
                    "Ostalo",
                )
            ) ,
            "mercedes" => array(
                "name" => "Mercedes-Benz",
                "items" => array(
                    "190 ",
                    "A ",
                    "B ",
                    "C ",
                    "C 30 AMG  ",
                    "C 32 AMG  ",
                    "C 36 AMG  ",
                    "C 43 AMG  ",
                    "C 55 AMG  ",
                    "C 63 AMG  ",
                    "C Sport Coupe ",
                    "C T-model ",
                    "CE",
                    "CL",
                    "CL 55 AMG ",
                    "CL 63 AMG ",
                    "CL 65 AMG ",
                    "CLC ",
                    "CLK ",
                    "CLK 55 AMG",
                    "CLK 63 AMG",
                    "CLS ",
                    "CLS 55 AMG",
                    "CLS 63 AMG",
                    "E ",
                    "E 36 AMG  ",
                    "E 50 AMG  ",
                    "E 500 ",
                    "E 55 AMG  ",
                    "E 60 AMG  ",
                    "E 63 AMG  ",
                    "E T-model ",
                    "G ",
                    "G 55 AMG  ",
                    "GL",
                    "GL 55 AMG ",
                    "GL 63 AMG ",
                    "GLK ",
                    "ML",
                    "ML 55 AMG ",
                    "ML 63 AMG ",
                    "R ",
                    "R 63 AMG  ",
                    "S ",
                    "S 63 AMG  ",
                    "S 65 AMG  ",
                    "SEC ",
                    "SEL ",
                    "SL",
                    "SL 55 AMG ",
                    "SL 60 AMG ",
                    "SL 63 AMG ",
                    "SL 65 AMG ",
                    "SL 70 AMG ",
                    "SL 73 AMG ",
                    "SLK ",
                    "SLK 32 AMG",
                    "SLK 55 AMG",
                    "SLR ",
                    "Vaneo ",
                    "W 116 ",
                    "W 123 ",
                    "W 124 ",
                    "W 126 ",
                    "Ostalo",
                )
            ) ,
            "mini" => array(
                "name" => "Mini",
                "items" => array(
                    "Cooper",
                    "Cooper clubman",
                    "Countryman",
                    "One ",
                    "Ostalo",
                )
            ) ,
            "mitsubishi" => array(
                "name" => "Mitsubishi",
                "items" => array(
                    "3000 GT ",
                    "ASX ",
                    "Carisma ",
                    "Colt  ",
                    "Cordia",
                    "Cosmos",
                    "Diamante  ",
                    "Eclipse ",
                    "Galant",
                    "Galloper  ",
                    "Grandis ",
                    "L200  ",
                    "Lancer",
                    "Outlander ",
                    "Pajero",
                    "Sapporo ",
                    "Sigma ",
                    "Space Gear",
                    "Space Runner  ",
                    "Space Star",
                    "Space Wagon ",
                    "Starion ",
                    "Tredia",
                    "Ostalo",
                )
            ) ,
            "nissan" => array(
                "name" => "Nissan",
                "items" => array(
                    "100 NX",
                    "200 SX",
                    "240 SX",
                    "280 ZX",
                    "300 ZX",
                    "350Z  ",
                    "370Z  ",
                    "Almera",
                    "Almera Tino ",
                    "Altima",
                    "Armada",
                    "Bluebird  ",
                    "Cherry",
                    "Frontier  ",
                    "Juke  ",
                    "King Cab  ",
                    "Kubistar  ",
                    "Laurel",
                    "Maxima",
                    "Micra ",
                    "Murano",
                    "Navara",
                    "Note  ",
                    "Pathfinder",
                    "Patrol",
                    "Pickup",
                    "Prairie ",
                    "Primera ",
                    "Pulsar",
                    "Qashqai ",
                    "Quest ",
                    "Sentra",
                    "Serena",
                    "Silvia",
                    "Stanza",
                    "Skyline ",
                    "Sunny ",
                    "Terrano ",
                    "Tiida ",
                    "Titan ",
                    "Trade ",
                    "X-Trail ",
                    "Ostalo",
                )
            ) ,
            "opel" => array(
                "name" => "Opel",
                "items" => array(
                    "Agila ",
                    "Antara",
                    "Ascona",
                    "Astra ",
                    "Astra GTC ",
                    "Calibra ",
                    "Campo ",
                    "Cavalier  ",
                    "Combo ",
                    "Commodore ",
                    "Corsa ",
                    "Frontera  ",
                    "GT",
                    "Insignia  ",
                    "Kadett",
                    "Manta ",
                    "Meriva",
                    "Monterey  ",
                    "Monza ",
                    "Omega ",
                    "Rekord",
                    "Senator ",
                    "Signum",
                    "Sintra",
                    "Speedster ",
                    "Tigra ",
                    "Vectra",
                    "Vivaro",
                    "Zafira",
                    "Ostalo",
                )
            ) ,
            "peugeot" => array(
                "name" => "Peugeot",
                "items" => array(
                    "104 ",
                    "106 ",
                    "107 ",
                    "205 ",
                    "206 ",
                    "207 ",
                    "305 ",
                    "306 ",
                    "307 ",
                    "308 ",
                    "309 ",
                    "405 ",
                    "406 ",
                    "407 ",
                    "504 ",
                    "505 ",
                    "604 ",
                    "605 ",
                    "607 ",
                    "806 ",
                    "807 ",
                    "1007  ",
                    "3008  ",
                    "4007  ",
                    "5008  ",
                    "Bipper",
                    "Partner ",
                    "RCZ ",
                    "Ostalo",
                )
            ) ,
            "pontiac" => array(
                "name" => "Pontiac",
                "items" => array(
                    "6000  ",
                    "Aztek ",
                    "Bonnevile ",
                    "Fiero ",
                    "Firebird  ",
                    "G6",
                    "Grand Am  ",
                    "Grand Prix",
                    "GTO ",
                    "Montana ",
                    "Solstice  ",
                    "Sunbird ",
                    "Sunfire ",
                    "Targa ",
                    "Trans Am  ",
                    "Trans Sport ",
                    "Vibe  ",
                    "Ostalo",
                )
            ) ,
            "porsche" => array(
                "name" => "Porsche",
                "items" => array(
                    "911 ",
                    "924 ",
                    "928 ",
                    "944 ",
                    "959 ",
                    "Boxster ",
                    "Carrera ",
                    "Cayenne ",
                    "Cayman",
                    "Ostalo",
                )
            ) ,
            "proton" => array(
                "name" => "Proton",
                "items" => array(
                    "Serija 300",
                    "Serija 400",
                    "Ostalo",
                )
            ) ,
            "renault" => array(
                "name" => "Renault",
                "items" => array(
                    "4 ",
                    "5 ",
                    "9 ",
                    "11",
                    "14",
                    "18",
                    "19",
                    "20 Chamade",
                    "20",
                    "21",
                    "22 Nevada ",
                    "25",
                    "30",
                    "Alpine",
                    "Avantime  ",
                    "Clio  ",
                    "Espace",
                    "Fluence ",
                    "Fuego ",
                    "Grand Espace  ",
                    "Grand Modus ",
                    "Grand Scenic  ",
                    "Kangoo",
                    "Koleos",
                    "Laguna",
                    "Megane",
                    "Megane Break  ",
                    "Megane Cabriolet  ",
                    "Megane Classic",
                    "Megane Coupe  ",
                    "Megane Grandtour  ",
                    "Megane Scenic ",
                    "Megane Scenic RX5 ",
                    "Megane Sedan  ",
                    "Modus ",
                    "Safrane ",
                    "Scenic",
                    "Spider",
                    "Thalia",
                    "Twingo",
                    "Vel Satis ",
                    "Wind  ",
                    "Ostalo",
                )
            ) ,
            "rolls" => array(
                "name" => "Rolls Royce",
                "items" => array(
                    "Cloud ",
                    "Corniche  ",
                    "Flying Spur ",
                    "Ghost ",
                    "Park Ward ",
                    "Phantom ",
                    "Silver Cloud  ",
                    "Silver Dawn ",
                    "Silver Seraph ",
                    "Silver Shadow ",
                    "Silver Spirit ",
                    "Ostalo",
                )
            ) ,
            "rover" => array(
                "name" => "Rover",
                "items" => array(
                    "25",
                    "45",
                    "75",
                    "100 ",
                    "111 ",
                    "114 ",
                    "115 ",
                    "200 ",
                    "213 ",
                    "214 ",
                    "216 ",
                    "218 ",
                    "220 ",
                    "400 ",
                    "414 ",
                    "416 ",
                    "418 ",
                    "420 ",
                    "600 ",
                    "618 ",
                    "620 ",
                    "623 ",
                    "625 ",
                    "800 ",
                    "820 ",
                    "825 ",
                    "827 ",
                    "City Rover",
                    "Metro ",
                    "MG ZR ",
                    "MGF ",
                    "Montego ",
                    "SD1 ",
                    "Streetwise",
                    "Tourer",
                    "Ostalo",
                )
            ) ,
            "saab" => array(
                "name" => "Saab",
                "items" => array(
                    "9-3 ",
                    "9-5 ",
                    "9-7X  ",
                    "90",
                    "900 ",
                    "9000  ",
                    "96",
                    "99",
                    "Ostalo",
                )
            ) ,
            "seat" => array(
                "name" => "Seat",
                "items" => array(
                    "Alhambra  ",
                    "Altea ",
                    "Arosa ",
                    "Cordoba ",
                    "Exeo  ",
                    "Fura  ",
                    "Ibiza ",
                    "Inca  ",
                    "Leon  ",
                    "Malaga",
                    "Marbella  ",
                    "Ronda ",
                    "Terra ",
                    "Toledo",
                    "Ostalo",
                )
            ) ,
            "smart" => array(
                "name" => "Smart",
                "items" => array(
                    "City  ",
                    "Crossblade",
                    "ForFour ",
                    "ForTwo",
                    "Roadster  ",
                    "Ostalo",
                )
            ) ,
            "ssang_yong" => array(
                "name" => "SsangYong",
                "items" => array(
                    "Actyon",
                    "Family",
                    "Kornado ",
                    "Kyron ",
                    "Musso ",
                    "Rexton",
                    "Rodius",
                    "Ostalo",
                )
            ) ,
            "subaru" => array(
                "name" => "Subaru",
                "items" => array(
                    "1801  ",
                    "Tribeca ",
                    "Baja  ",
                    "Forester  ",
                    "Impreza ",
                    "Justy ",
                    "Legacy",
                    "Libero",
                    "Outback ",
                    "SVX ",
                    "Vivio ",
                    "XT",
                    "Ostalo",
                )
            ) ,
            "suzuki" => array(
                "name" => "Suzuki",
                "items" => array(
                    "Alto  ",
                    "Baleno",
                    "Cappuccino",
                    "Carry ",
                    "Grand Vitara  ",
                    "Ignis ",
                    "Jimny ",
                    "Kizashi ",
                    "Liana ",
                    "LJ",
                    "Maruti",
                    "Samurai ",
                    "Splash",
                    "Swift ",
                    "SX4 ",
                    "Vitara",
                    "Verona",
                    "Wagon R ",
                    "X-90  ",
                    "XL7 ",
                    "Ostalo",
                )
            ) ,
            "skoda" => array(
                "name" => "Škoda",
                "items" => array(
                    "105 ",
                    "120 ",
                    "130 ",
                    "135 ",
                    "Fabia ",
                    "Favorit ",
                    "Felicia ",
                    "Forman",
                    "Octavia ",
                    "Pick-up ",
                    "Roomster  ",
                    "Superb",
                    "Yeti  ",
                    "Ostalo",
                )
            ) ,
            "tata" => array(
                "name" => "Tata",
                "items" => array(
                    "Indica",
                    "Indigo",
                    "Nano  ",
                    "Safari",
                    "Sumo  ",
                    "Telcoline ",
                    "Telcosport",
                    "Xenon ",
                    "Ostalo",
                )
            ) ,
            "toyota" => array(
                "name" => "Toyota",
                "items" => array(
                    "4-Runner  ",
                    "Auris ",
                    "Avalon",
                    "Avensis ",
                    "Avensis Verso ",
                    "Aygo  ",
                    "Camry ",
                    "Carina",
                    "Celica",
                    "Celsior ",
                    "Corolla ",
                    "Corolla Verso ",
                    "Cressida  ",
                    "Crown ",
                    "FJ Cruiser",
                    "Hiace ",
                    "Hilux ",
                    "iQ",
                    "Land Cruiser  ",
                    "LiteAce ",
                    "MR 2  ",
                    "Paseo ",
                    "Picnic",
                    "Previa",
                    "Prius ",
                    "RAV 4 ",
                    "Sequoia ",
                    "Sienna",
                    "Starlet ",
                    "Supra ",
                    "Tacoma",
                    "Tercel",
                    "Tundra",
                    "Urban Cruiser ",
                    "Yaris ",
                    "Yaris Verso ",
                    "Ostalo",
                )
            ) ,
            "vw" => array(
                "name" => "Volkswagen (VW)",
                "items" => array(
                    "181 ",
                    "Beetle",
                    "Bora  ",
                    "Buggy ",
                    "Caddy ",
                    "California",
                    "Caravelle ",
                    "Corrado ",
                    "CrossGolf ",
                    "Derby ",
                    "Eos ",
                    "Fox ",
                    "Golf I",
                    "Golf II ",
                    "Golf III  ",
                    "Golf IV ",
                    "Golf V",
                    "Golf VI ",
                    "Golf Cabrio ",
                    "GolfPlus  ",
                    "Golf Variant  ",
                    "Iltis ",
                    "Jetta ",
                    "Lupo  ",
                    "Multivan  ",
                    "Munga ",
                    "New Beetle",
                    "Passat",
                    "Phaeton ",
                    "Polo  ",
                    "Santana ",
                    "Scirocco  ",
                    "Sharan",
                    "Taro  ",
                    "Tiguan",
                    "Touareg ",
                    "Touran",
                    "Vento ",
                    "Ostalo",
                )
            ) ,
            "volvo" => array(
                "name" => "Volvo",
                "items" => array(
                    "240 ",
                    "242 ",
                    "244 ",
                    "245 ",
                    "262 ",
                    "264 ",
                    "265 ",
                    "340 ",
                    "360 ",
                    "400 ",
                    "440 ",
                    "460 ",
                    "480 ",
                    "740 ",
                    "744 ",
                    "745 ",
                    "760 ",
                    "765 ",
                    "780 ",
                    "850 ",
                    "855 ",
                    "940 ",
                    "944 ",
                    "945 ",
                    "960 ",
                    "965 ",
                    "C30 ",
                    "C70 ",
                    "S40 ",
                    "S60 ",
                    "S70 ",
                    "S80 ",
                    "S90 ",
                    "V40 ",
                    "V50 ",
                    "V60 ",
                    "V70 ",
                    "V90 ",
                    "XC 60 ",
                    "XC 70 ",
                    "XC 90 ",
                    "Ostalo",
                )
            ) ,
            "zastava" => array(
                "name" => "Zastava",
                "items" => array(
                    "10",
                    "101 ",
                    "128 ",
                    "750 ",
                    "850 ",
                    "Poly  ",
                    "Tempo ",
                    "Yugo Koral 45 ",
                    "Yugo Koral 55 ",
                    "Yugo Koral 60 ",
                    "Yugo Skala 55 ",
                    "Yugo Florida  ",
                    "Ostalo"
                )
            ) ,
            "other" => array(
                "name" => "Ostalo"
            )
        );

        echo 'Importing car brands...' . PHP_EOL;

        $root_dict = new Dictionary();
        $root_dict->name = 'AMN - Auti - Marka i model';
        $root_dict->slug = 'vehicle_make_model';
        $root_dict->active = 1;
        $root_dict->saveNode();

        if ($root_dict->id) {
            foreach ($data as $make) {
                $root = Dictionary::findFirst($root_dict->id);
                $car_make = trim($make['name']);

                $parent_val = new Dictionary();
                $parent_val->name = $car_make;
                $parent_val->active = 1;
                $parent_val->appendTo($root);

                if ($parent_val->id) {
                    if (isset($make['items'])) {
                        foreach ($make['items'] as $model) {
                            $parent = Dictionary::findFirst($parent_val->id);
                            $car_model = trim($model);

                            $child_val = new Dictionary();
                            $child_val->name = $car_model;
                            $child_val->active = 1;
                            $child_val->appendTo($parent);
                        }
                    }
                }
            }
        }
    }

    public function fuelType()
    {
        $data = array("benzin", "plin", "dizel", "elektro", "hybrid", "bio gorivo");

        echo 'Importing fuel type...' . PHP_EOL;

        $root_dict = new Dictionary();
        $root_dict->name = 'AMN - Auti - Vrsta goriva';
        $root_dict->slug = 'vehicle_fuel_type';
        $root_dict->active = 1;
        $root_dict->saveNode();

        if ($root_dict->id) {
            foreach ($data as $row) {
                $root = Dictionary::findFirst($root_dict->id);
                $name = trim($row);

                $parent_val = new Dictionary();
                $parent_val->name = $name;
                $parent_val->active = 1;
                $parent_val->appendTo($root);
            }
        }
    }

    public function carDrivetrainType()
    {
        $data = array("prednji", "zadnji", "4x4");

        echo 'Importing drivetrain type...' . PHP_EOL;

        $root_dict = new Dictionary();
        $root_dict->name = 'AMN - Auti - Pogon';
        $root_dict->slug = 'vehicle_drivetrain';
        $root_dict->active = 1;
        $root_dict->saveNode();

        if ($root_dict->id) {
            foreach ($data as $row) {
                $root = Dictionary::findFirst($root_dict->id);
                $name = trim($row);

                $parent_val = new Dictionary();
                $parent_val->name = $name;
                $parent_val->active = 1;
                $parent_val->appendTo($root);
            }
        }
    }

    public function carSeatNumber()
    {
        $data = array("2", "3", "4", "5", "6", "7 i više");

        echo 'Importing car seat numbers...' . PHP_EOL;

        $root_dict = new Dictionary();
        $root_dict->name = 'AMN - Auti - Broj sjedala';
        $root_dict->slug = 'vehicle_seat_number';
        $root_dict->active = 1;
        $root_dict->saveNode();

        if ($root_dict->id) {
            foreach ($data as $row) {
                $root = Dictionary::findFirst($root_dict->id);
                $name = trim($row);

                $parent_val = new Dictionary();
                $parent_val->name = $name;
                $parent_val->active = 1;
                $parent_val->appendTo($root);
            }
        }
    }

    public function ownerType()
    {
        $data = array("prvi", "drugi", "treći i više");

        echo 'Importing owner type...' . PHP_EOL;

        $root_dict = new Dictionary();
        $root_dict->name = 'AMN - Vlasnik';
        $root_dict->slug = 'amn_owner';
        $root_dict->active = 1;
        $root_dict->saveNode();

        if ($root_dict->id) {
            foreach ($data as $row) {
                $root = Dictionary::findFirst($root_dict->id);
                $name = trim($row);

                $parent_val = new Dictionary();
                $parent_val->name = $name;
                $parent_val->active = 1;
                $parent_val->appendTo($root);
            }
        }
    }

    public function vehicleService()
    {
        $data = array("da, svi servisi u ovlaštenom servisu", "da, poneki servis u neovlaštenom servisu", "ne");

        echo 'Importing vehicle service...' . PHP_EOL;

        $root_dict = new Dictionary();
        $root_dict->name = 'AMN - Redoviti servis';
        $root_dict->slug = 'amn_vehicle_service';
        $root_dict->active = 1;
        $root_dict->saveNode();

        if ($root_dict->id) {
            foreach ($data as $row) {
                $root = Dictionary::findFirst($root_dict->id);
                $name = trim($row);

                $parent_val = new Dictionary();
                $parent_val->name = $name;
                $parent_val->active = 1;
                $parent_val->appendTo($root);
            }
        }
    }

    public function carGearType()
    {
        $data = array(
            "ručni mjenjač",
            "automatski mjenjač",
            "poluautomatski (dsg, smg) mjenjač",
            "sekvencijalni mjenjač",
            "cvt mjenjač"
        );

        echo 'Importing gear types...' . PHP_EOL;

        $root_dict = new Dictionary();
        $root_dict->name = 'AMN - Auti - Vrsta mjenjača';
        $root_dict->slug = 'vehicle_gear_type';
        $root_dict->active = 1;
        $root_dict->saveNode();

        if ($root_dict->id) {
            foreach ($data as $row) {
                $root = Dictionary::findFirst($root_dict->id);
                $name = trim($row);

                $parent_val = new Dictionary();
                $parent_val->name = $name;
                $parent_val->active = 1;
                $parent_val->appendTo($root);
            }
        }
    }

    public function carGears()
    {
        $data = array(
            "3 brzine",
            "4 brzine",
            "5 brzina",
            "6 brzina",
            "7 i više brzina"
        );

        echo 'Importing gear number...' . PHP_EOL;

        $root_dict = new Dictionary();
        $root_dict->name = 'AMN - Auti - Broj brzina';
        $root_dict->slug = 'vehicle_gear_number';
        $root_dict->active = 1;
        $root_dict->saveNode();

        if ($root_dict->id) {
            foreach ($data as $row) {
                $root = Dictionary::findFirst($root_dict->id);
                $name = trim($row);

                $parent_val = new Dictionary();
                $parent_val->name = $name;
                $parent_val->active = 1;
                $parent_val->appendTo($root);
            }
        }
    }

    public function carDoorNumber()
    {
        $data = array(
            "2",
            "3",
            "4",
            "5",
            "6"
        );

        echo 'Importing door number...' . PHP_EOL;

        $root_dict = new Dictionary();
        $root_dict->name = 'AMN - Auti - Broj vrata';
        $root_dict->slug = 'vehicle_door_number';
        $root_dict->active = 1;
        $root_dict->saveNode();

        if ($root_dict->id) {
            foreach ($data as $row) {
                $root = Dictionary::findFirst($root_dict->id);
                $name = trim($row);

                $parent_val = new Dictionary();
                $parent_val->name = $name;
                $parent_val->active = 1;
                $parent_val->appendTo($root);
            }
        }
    }

    public function carBodyStyle()
    {
        $data = array(
            "limuzina",
            "hatchback",
            "karavan",
            "monovolumen",
            "kupe",
            "kabriolet",
            "terenac/suv",
            "van/minivan",
            "pick-up"
        );

        echo 'Importing car body style...' . PHP_EOL;

        $root_dict = new Dictionary();
        $root_dict->name = 'AMN - Auti - Vrsta karoserije';
        $root_dict->slug = 'vehicle_body_type';
        $root_dict->active = 1;
        $root_dict->saveNode();

        if ($root_dict->id) {
            foreach ($data as $row) {
                $root = Dictionary::findFirst($root_dict->id);
                $name = trim($row);

                $parent_val = new Dictionary();
                $parent_val->name = $name;
                $parent_val->active = 1;
                $parent_val->appendTo($root);
            }
        }
    }

    public function carAirbag()
    {
        $data = array(
            "nema",
            "vozački",
            "vozački i suvozački",
            "prednji i bočni",
            "prednji, bočni i zavjesa",
            "7 zr. jastuka",
            "8 i više zr. jastuka"
        );

        echo 'Importing car airbag...' . PHP_EOL;

        $root_dict = new Dictionary();
        $root_dict->name = 'AMN - Auti - Airbag';
        $root_dict->slug = 'car_airbag';
        $root_dict->active = 1;
        $root_dict->saveNode();

        if ($root_dict->id) {
            foreach ($data as $row) {
                $root = Dictionary::findFirst($root_dict->id);
                $name = trim($row);

                $parent_val = new Dictionary();
                $parent_val->name = $name;
                $parent_val->active = 1;
                $parent_val->appendTo($root);
            }
        }
    }

    public function carExtraEquipment()
    {
        $data = array(
            "3. stop svjetlo",
            "4x4",
            "aluminijski naplatci",
            "bi-xenonska svjetla",
            "krovni nosači",
            "krovni prozor",
            "nadzor pritiska u pneumaticima",
            "navigacija",
            "navigacija + tv",
            "putno računalo",
            "sportsko podvozje",
            "svjetla za maglu",
            "upravljač presvučen kožom",
            "xenonska svjetla",
            "zatamljena stakla"
        );

        echo 'Importing car extra quipment...' . PHP_EOL;

        $root_dict = new Dictionary();
        $root_dict->name = 'AMN - Auti - Dodatna oprema';
        $root_dict->slug = 'vehicle_extra_equipment';
        $root_dict->active = 1;
        $root_dict->saveNode();

        if ($root_dict->id) {
            foreach ($data as $row) {
                $root = Dictionary::findFirst($root_dict->id);
                $name = trim($row);

                $parent_val = new Dictionary();
                $parent_val->name = $name;
                $parent_val->active = 1;
                $parent_val->appendTo($root);
            }
        }
    }

    public function carSafety()
    {
        $data = array(
            "ABS",
            "ASD",
            "ASR",
            "EDC",
            "ESP",
            "ETS",
            "isofix (sustav vezanja sjedalice za dijete)",
            "samozatezajući sigurnosni pojasevi"
        );

        echo 'Importing car safety...' . PHP_EOL;

        $root_dict = new Dictionary();
        $root_dict->name = 'AMN - Auti - Sigurnost';
        $root_dict->slug = 'vehicle_safety';
        $root_dict->active = 1;
        $root_dict->saveNode();

        if ($root_dict->id) {
            foreach ($data as $row) {
                $root = Dictionary::findFirst($root_dict->id);
                $name = trim($row);

                $parent_val = new Dictionary();
                $parent_val->name = $name;
                $parent_val->active = 1;
                $parent_val->appendTo($root);
            }
        }
    }

    public function carTheftSafety()
    {
        $data = array(
            "alarm",
            "blokada motora",
            "centralno zaključavanje",
            "centralno zaključavanje sa daljinskim",
            "multilock"
        );

        echo 'Importing car theft safety...' . PHP_EOL;

        $root_dict = new Dictionary();
        $root_dict->name = 'AMN - Auti - Sigurnost od krađe';
        $root_dict->slug = 'vehicle_theft_safety';
        $root_dict->active = 1;
        $root_dict->saveNode();

        if ($root_dict->id) {
            foreach ($data as $row) {
                $root = Dictionary::findFirst($root_dict->id);
                $name = trim($row);

                $parent_val = new Dictionary();
                $parent_val->name = $name;
                $parent_val->active = 1;
                $parent_val->appendTo($root);
            }
        }
    }

    public function carComfortEquipment()
    {
        $data = array(
            "cd izmjenjivač",
            "djeljiva stražnja klupa",
            "el. podešavanje ogledala",
            "el. podešavanje sjedala",
            "el. podizanje prednjih stakala",
            "el. podizanje stražnjih stakala",
            "el. sklapanje ogledala",
            "grijanje ogledala",
            "grijanje sjedala",
            "handsfree",
            "komande na upravljaču",
            "kožna sjedala",
            "parkirni senzori",
            "priprema za mobilni aparat",
            "servo upravljač",
            "sportska sjedala",
            "središnji naslon za ruku",
            "tempomat",
            "upravljač podesiv po visini"
        );

        echo 'Importing car comfort equipment...' . PHP_EOL;

        $root_dict = new Dictionary();
        $root_dict->name = 'AMN - Auti - Udobnost';
        $root_dict->slug = 'vehicle_comfort_equipment';
        $root_dict->active = 1;
        $root_dict->saveNode();

        if ($root_dict->id) {
            foreach ($data as $row) {
                $root = Dictionary::findFirst($root_dict->id);
                $name = trim($row);

                $parent_val = new Dictionary();
                $parent_val->name = $name;
                $parent_val->active = 1;
                $parent_val->appendTo($root);
            }
        }
    }

    public function carNewUsed()
    {
        $data = array(
            "Novo",
            "Rabljeno"
        );

        echo 'Importing car new/used...' . PHP_EOL;

        $root_dict = new Dictionary();
        $root_dict->name = 'AMN - Novo/Rabljeno';
        $root_dict->slug = 'amn_new_used';
        $root_dict->active = 1;
        $root_dict->saveNode();

        if ($root_dict->id) {
            foreach ($data as $row) {
                $root = Dictionary::findFirst($root_dict->id);
                $name = trim($row);

                $parent_val = new Dictionary();
                $parent_val->name = $name;
                $parent_val->active = 1;
                $parent_val->appendTo($root);
            }
        }
    }

    public function carCondition()
    {
        $data = array(
            "odlično",
            "dobro",
            "osrednje",
            "slabo",
            "karambol/za dijelove"
        );

        echo 'Importing car conditions...' . PHP_EOL;

        $root_dict = new Dictionary();
        $root_dict->name = 'AMN - Stanje';
        $root_dict->slug = 'vehicle_condition';
        $root_dict->active = 1;
        $root_dict->saveNode();

        if ($root_dict->id) {
            foreach ($data as $row) {
                $root = Dictionary::findFirst($root_dict->id);
                $name = trim($row);

                $parent_val = new Dictionary();
                $parent_val->name = $name;
                $parent_val->active = 1;
                $parent_val->appendTo($root);
            }
        }
    }

    public function carColor()
    {
        $data = array(
            "bež",
            "bijela",
            "crna",
            "crvena",
            "bordo crvena",
            "ljubičasta",
            "narančasta",
            "plava",
            "svijetlo plava",
            "tamno plava",
            "siva",
            "svijetlo siva",
            "tamno siva",
            "smeđa",
            "srebrna",
            "zelena",
            "svijetlo zelena",
            "tamno zelena",
            "zlatna",
            "žuta"
        );

        echo 'Importing car color...' . PHP_EOL;

        $root_dict = new Dictionary();
        $root_dict->name = 'AMN - Auti - Boja';
        $root_dict->slug = 'vehicle_color';
        $root_dict->active = 1;
        $root_dict->saveNode();

        if ($root_dict->id) {
            foreach ($data as $row) {
                $root = Dictionary::findFirst($root_dict->id);
                $name = trim($row);

                $parent_val = new Dictionary();
                $parent_val->name = $name;
                $parent_val->active = 1;
                $parent_val->appendTo($root);
            }
        }
    }

    public function carMultimedia()
    {
        $data = array(
            "radio",
            "radio-kazetofon",
            "radio-cd",
            "radio-cd-mp3",
            "radio-cd-changer",
            "radio-cd-mp3-changer",
            "radio-dvd"
        );

        echo 'Importing car multimedia...' . PHP_EOL;

        $root_dict = new Dictionary();
        $root_dict->name = 'AMN - Auti - Multimedija';
        $root_dict->slug = 'car_multimedia';
        $root_dict->active = 1;
        $root_dict->saveNode();

        if ($root_dict->id) {
            foreach ($data as $row) {
                $root = Dictionary::findFirst($root_dict->id);
                $name = trim($row);

                $parent_val = new Dictionary();
                $parent_val->name = $name;
                $parent_val->active = 1;
                $parent_val->appendTo($root);
            }
        }
    }

    public function carAirCondition()
    {
        $data = array(
            "nema",
            "ručna",
            "automatska",
            "automatska dvozonska"
        );

        echo 'Importing car aircondition...' . PHP_EOL;

        $root_dict = new Dictionary();
        $root_dict->name = 'AMN - Auti - Klima uređaj';
        $root_dict->slug = 'car_aircondition';
        $root_dict->active = 1;
        $root_dict->saveNode();

        if ($root_dict->id) {
            foreach ($data as $row) {
                $root = Dictionary::findFirst($root_dict->id);
                $name = trim($row);

                $parent_val = new Dictionary();
                $parent_val->name = $name;
                $parent_val->active = 1;
                $parent_val->appendTo($root);
            }
        }
    }

    public function carPartsType()
    {
        $data = array(
            "amortizeri",
            "dijelovi ovjesa",
            "elektronika",
            "kočioni sustav",
            "limarija",
            "mehanika",
            "svjetla",
            "tuning i styling",
            "unutrašnjost",
            "ostalo"
        );

        echo 'Importing car part types...' . PHP_EOL;

        $root_dict = new Dictionary();
        $root_dict->name = 'AMN - Vrsta autodijelova';
        $root_dict->slug = 'amn_parts_type';
        $root_dict->active = 1;
        $root_dict->saveNode();

        if ($root_dict->id) {
            foreach ($data as $row) {
                $root = Dictionary::findFirst($root_dict->id);
                $name = trim($row);

                $parent_val = new Dictionary();
                $parent_val->name = $name;
                $parent_val->active = 1;
                $parent_val->appendTo($root);
            }
        }
    }

    public function carWheelDimension()
    {
        $data = array(
            '10"',
            '12"',
            '13"',
            '14"',
            '15"',
            '16"',
            '16.5"',
            '17"',
            '17.5"',
            '18"',
            '19"',
            '19.5"',
            '20"',
            '21"',
            '22"',
            '23"',
            '24"',
            '25"',
            '26"',
            '28"',
            '30"',
            'Ostalo'
        );

        echo 'Importing wheel diameters...' . PHP_EOL;

        $root_dict = new Dictionary();
        $root_dict->name = 'AMN - Felge - Dimenzija';
        $root_dict->slug = 'amn_wheel_dimension';
        $root_dict->active = 1;
        $root_dict->saveNode();

        if ($root_dict->id) {
            foreach ($data as $row) {
                $root = Dictionary::findFirst($root_dict->id);
                $name = trim($row);

                $parent_val = new Dictionary();
                $parent_val->name = $name;
                $parent_val->active = 1;
                $parent_val->appendTo($root);
            }
        }
    }

    public function carWheelBrand()
    {
        $data = array(
            "AEZ",
            "Alessio",
            "ATS",
            "BBS",
            "Dotz",
            "Enzo",
            "Lowenhart",
            "MAK",
            "Momo",
            "OZ",
            "Rial",
            "Sparco",
            "TSW",
            "Ostalo"
        );

        echo 'Importing wheel brands...' . PHP_EOL;

        $root_dict = new Dictionary();
        $root_dict->name = 'AMN - Felge - Proizvođač';
        $root_dict->slug = 'amn_wheel_brand';
        $root_dict->active = 1;
        $root_dict->saveNode();

        if ($root_dict->id) {
            foreach ($data as $row) {
                $root = Dictionary::findFirst($root_dict->id);
                $name = trim($row);

                $parent_val = new Dictionary();
                $parent_val->name = $name;
                $parent_val->active = 1;
                $parent_val->appendTo($root);
            }
        }
    }

    public function carTireType()
    {
        $data = array(
            "ljetna",
            "zimska",
            "cijelogodišnja"
        );

        echo 'Importing tire types...' . PHP_EOL;

        $root_dict = new Dictionary();
        $root_dict->name = 'AMN - Gume - Vrsta';
        $root_dict->slug = 'amn_tire_type';
        $root_dict->active = 1;
        $root_dict->saveNode();

        if ($root_dict->id) {
            foreach ($data as $row) {
                $root = Dictionary::findFirst($root_dict->id);
                $name = trim($row);

                $parent_val = new Dictionary();
                $parent_val->name = $name;
                $parent_val->active = 1;
                $parent_val->appendTo($root);
            }
        }
    }

    public function carTireBrand()
    {
        $data = array(
            "BF Goodrich",
            "Bridgestone",
            "Continental",
            "Debica",
            "Dunlop",
            "Goodyear",
            "Hankook",
            "Kumho",
            "Matador",
            "Michelin",
            "Nokian",
            "Pirelli",
            "Sava",
            "Tigar",
            "Toyo",
            "Uniroyal",
            "Verdestein",
            "Yokohama",
            "Ostalo"
        );

        echo 'Importing tire brands...' . PHP_EOL;

        $root_dict = new Dictionary();
        $root_dict->name = 'AMN - Gume - Proizvođač';
        $root_dict->slug = 'amn_tire_brand';
        $root_dict->active = 1;
        $root_dict->saveNode();

        if ($root_dict->id) {
            foreach ($data as $row) {
                $root = Dictionary::findFirst($root_dict->id);
                $name = trim($row);

                $parent_val = new Dictionary();
                $parent_val->name = $name;
                $parent_val->active = 1;
                $parent_val->appendTo($root);
            }
        }
    }

    public function motorcycles()
    {
        $data = array(
            "Adly    ",
            "Aeon    ",
            "Aprilia ",
            "BMW ",
            "Baotian ",
            "Bashan  ",
            "Benelli ",
            "Benzhou ",
            "Beta    ",
            "Bimota  ",
            "Bombardier  ",
            "Cagiva  ",
            "Cfmoto  ",
            "Chituma ",
            "Cixi Kingring   ",
            "CPI ",
            "ČZ  ",
            "DDR ",
            "Daelim  ",
            "Derbi   ",
            "Ducati  ",
            "Engage  ",
            "Fantic Motor    ",
            "Foshan  ",
            "Galaxy  ",
            "Gamax   ",
            "Geely   ",
            "Generic ",
            "Gilera  ",
            "Harley Davidson ",
            "Hero    ",
            "Honda   ",
            "Huatian ",
            "Husaberg    ",
            "Husqvarna   ",
            "Hyosung ",
            "Hyundai ",
            "Ingkart ",
            "Italjet ",
            "Italscooter ",
            "Jawa    ",
            "Jinlun  ",
            "Jmstar  ",
            "Kangchao    ",
            "Kawasaki    ",
            "Kazuma  ",
            "Keeway  ",
            "Kinroad ",
            "KTM ",
            "Kymco   ",
            "Laverda ",
            "LG  ",
            "Lifan   ",
            "Linhai  ",
            "Lizhong ",
            "LML ",
            "Loncin  ",
            "Longjia ",
            "Luna    ",
            "Maico   ",
            "Malaguti    ",
            "Megelli ",
            "Moto Guzzi  ",
            "MTL ",
            "MV Agusta   ",
            "MZ  ",
            "Ningbo  ",
            "NSU ",
            "Orion   ",
            "Peda    ",
            "Peugeot ",
            "PGO ",
            "Piaggio ",
            "Polaris ",
            "Polini  ",
            "Proton  ",
            "Puch    ",
            "Qingqi  ",
            "Regal-Raptor    ",
            "Renault ",
            "Sachs   ",
            "Sanli   ",
            "Scott   ",
            "Shineray    ",
            "Silverstone ",
            "Simson  ",
            "SMC ",
            "Sonik   ",
            "Spyder  ",
            "Suzuki  ",
            "SYM ",
            "TGB ",
            "TM  ",
            "Tomos   ",
            "Trike   ",
            "Triumph ",
            "Vespa   ",
            "X-Plorer    ",
            "XMotos  ",
            "Xgjao   ",
            "Xingyue ",
            "Xinling ",
            "XTM ",
            "Yamaha  ",
            "Yiying  ",
            "Zhongneng   ",
            "Zhongyu ",
            "Zongshen    ",
            "Ostalo  "
        );

        echo 'Importing motorcycles...' . PHP_EOL;

        $root_dict = new Dictionary();
        $root_dict->name = 'AMN - Motocikli - Marka';
        $root_dict->slug = 'amn_motorcycle_brand';
        $root_dict->active = 1;
        $root_dict->saveNode();

        if ($root_dict->id) {
            foreach ($data as $make) {
                $root = Dictionary::findFirst($root_dict->id);
                $motorcycle_make = trim($make);

                $parent_val = new Dictionary();
                $parent_val->name = $motorcycle_make;
                $parent_val->active = 1;
                $parent_val->appendTo($root);
            }
        }
    }

    public function motorcycleType()
    {
        $data = array(
            "chopper",
            "cross",
            "moped",
            "off-road",
            "quad / atv",
            "skuter",
            "sport",
            "supermoto",
            "tourer",
            "ostalo"
        );

        echo 'Importing motorcycle types...' . PHP_EOL;

        $root_dict = new Dictionary();
        $root_dict->name = 'AMN - Motocikli - Vrsta';
        $root_dict->slug = 'amn_motorcycle_type';
        $root_dict->active = 1;
        $root_dict->saveNode();

        if ($root_dict->id) {
            foreach ($data as $row) {
                $root = Dictionary::findFirst($root_dict->id);
                $name = trim($row);

                $parent_val = new Dictionary();
                $parent_val->name = $name;
                $parent_val->active = 1;
                $parent_val->appendTo($root);
            }
        }
    }

    public function motorcycleEquipment()
    {
        $data = array(
            "čizme",
            "dijelovi",
            "gume",
            "kacige",
            "naočale",
            "odijelo",
            "ostalo"
        );

        echo 'Importing motorcycle equipment...' . PHP_EOL;

        $root_dict = new Dictionary();
        $root_dict->name = 'AMN - Motocikli - Oprema';
        $root_dict->slug = 'amn_motorcycle_equipment';
        $root_dict->active = 1;
        $root_dict->saveNode();

        if ($root_dict->id) {
            foreach ($data as $row) {
                $root = Dictionary::findFirst($root_dict->id);
                $name = trim($row);

                $parent_val = new Dictionary();
                $parent_val->name = $name;
                $parent_val->active = 1;
                $parent_val->appendTo($root);
            }
        }
    }

    public function truckBrand()
    {
        $data = array(
            "Carrier",
            "Citroen",
            "Cormach",
            "DAF",
            "Dautel",
            "FAP",
            "Fiat",
            "Ford",
            "GM",
            "Hiab",
            "Hyundai",
            "Intermix",
            "ISI",
            "Isuzu",
            "Itas",
            "Iveco",
            "Jansky",
            "Kamaz",
            "KIA",
            "Kubota",
            "Liebherr",
            "Magirus-Deutz",
            "MAN",
            "Mercedes-Benz",
            "Mitsubishi",
            "Nissan",
            "Opel",
            "Palfinger",
            "Pegaso",
            "Peugeot",
            "Puch",
            "Putzmeister",
            "Raba",
            "Renault",
            "Roganec",
            "Ruthmann",
            "Scania",
            "Schmitz",
            "Schwarte-Jansky",
            "Schwing",
            "Ssang Yong",
            "Stetter",
            "Steyr",
            "TAM",
            "Tatra",
            "Toyota",
            "Unimog",
            "Vogele",
            "Volkswagen",
            "Volvo",
            "Yamaha",
            "Zastava",
            "Ostalo"
        );

        echo 'Importing truck brands...' . PHP_EOL;

        $root_dict = new Dictionary();
        $root_dict->name = 'AMN - Kamioni - Proizvođač';
        $root_dict->slug = 'amn_truck_brand';
        $root_dict->active = 1;
        $root_dict->saveNode();

        if ($root_dict->id) {
            foreach ($data as $row) {
                $root = Dictionary::findFirst($root_dict->id);
                $name = trim($row);

                $parent_val = new Dictionary();
                $parent_val->name = $name;
                $parent_val->active = 1;
                $parent_val->appendTo($root);
            }
        }
    }

    public function truckType()
    {
        $data = array(
            "tegljač",
            "cisterna",
            "kiper",
            "sanduk",
            "hladnjača",
            "za prijevoz auta",
            "mixer",
            "ostalo"
        );

        echo 'Importing truck types...' . PHP_EOL;

        $root_dict = new Dictionary();
        $root_dict->name = 'AMN - Kamioni - Vrsta';
        $root_dict->slug = 'amn_truck_type';
        $root_dict->active = 1;
        $root_dict->saveNode();

        if ($root_dict->id) {
            foreach ($data as $row) {
                $root = Dictionary::findFirst($root_dict->id);
                $name = trim($row);

                $parent_val = new Dictionary();
                $parent_val->name = $name;
                $parent_val->active = 1;
                $parent_val->appendTo($root);
            }
        }
    }

    public function truckExtra()
    {
        $data = array(
            "kran",
            "spavaća kabina",
            "alarm",
            "grijanje sjedala",
            "tempomat"
        );

        echo 'Importing truck extra equipment...' . PHP_EOL;

        $root_dict = new Dictionary();
        $root_dict->name = 'AMN - Kamioni - Dodatna oprema';
        $root_dict->slug = 'truck_extra_equipment';
        $root_dict->active = 1;
        $root_dict->saveNode();

        if ($root_dict->id) {
            foreach ($data as $row) {
                $root = Dictionary::findFirst($root_dict->id);
                $name = trim($row);

                $parent_val = new Dictionary();
                $parent_val->name = $name;
                $parent_val->active = 1;
                $parent_val->appendTo($root);
            }
        }
    }

    public function deliveryVanBrand()
    {
        $data = array(
            "Bedford",
            "Chevrolet",
            "Chrysler",
            "Citroen",
            "Dacia",
            "Daewoo",
            "Fiat",
            "Ford",
            "GMC",
            "Honda",
            "Hyundai",
            "Iveco",
            "Mazda",
            "Mercedes",
            "Mitsubishi",
            "Nissan",
            "Opel",
            "Peugeot",
            "Piaggio",
            "Renault",
            "Seat",
            "Škoda",
            "Ssang Yong",
            "Suzuki",
            "Tata",
            "Toyota",
            "Volkswagen",
            "Volvo",
            "Yamaha",
            "Zastava",
            "Ostalo"
        );

        echo 'Importing delivery van brands...' . PHP_EOL;

        $root_dict = new Dictionary();
        $root_dict->name = 'AMN - Dostavna vozila - Proizvođač';
        $root_dict->slug = 'amn_delivery_van_brand';
        $root_dict->active = 1;
        $root_dict->saveNode();

        if ($root_dict->id) {
            foreach ($data as $row) {
                $root = Dictionary::findFirst($root_dict->id);
                $name = trim($row);

                $parent_val = new Dictionary();
                $parent_val->name = $name;
                $parent_val->active = 1;
                $parent_val->appendTo($root);
            }
        }
    }

    public function busBrand()
    {
        $data = array(
            "DAF",
            "Ernst Auwarter",
            "Evobus",
            "FAP",
            "Fiat",
            "Isuzu",
            "Iveco",
            "MAN",
            "Marbus",
            "Mercedes-Benz",
            "Mitsubishi",
            "Neoplan",
            "Opel",
            "Peugeot",
            "Renault",
            "Sanos",
            "Setra",
            "TAM",
            "TVM",
            "Volkswagen",
            "Volvo",
            "Ostalo"
        );

        echo 'Importing bus brands...' . PHP_EOL;

        $root_dict = new Dictionary();
        $root_dict->name = 'AMN - Autobusi - Proizvođač';
        $root_dict->slug = 'amn_bus_brand';
        $root_dict->active = 1;
        $root_dict->saveNode();

        if ($root_dict->id) {
            foreach ($data as $row) {
                $root = Dictionary::findFirst($root_dict->id);
                $name = trim($row);

                $parent_val = new Dictionary();
                $parent_val->name = $name;
                $parent_val->active = 1;
                $parent_val->appendTo($root);
            }
        }
    }

    public function busExtra()
    {
        $data = array(
            "ABS",
            "ESP",
            "ASR",
            "grijanje sjedala",
            "tempomat"
        );

        echo 'Importing bus extra equipment...' . PHP_EOL;

        $root_dict = new Dictionary();
        $root_dict->name = 'AMN - Autobusi - Dodatna oprema';
        $root_dict->slug = 'bus_extra_equipment';
        $root_dict->active = 1;
        $root_dict->saveNode();

        if ($root_dict->id) {
            foreach ($data as $row) {
                $root = Dictionary::findFirst($root_dict->id);
                $name = trim($row);

                $parent_val = new Dictionary();
                $parent_val->name = $name;
                $parent_val->active = 1;
                $parent_val->appendTo($root);
            }
        }
    }

    public function trailerBrand()
    {
        $data = array(
            "Ackermann",
            "Agados",
            "Andreoli",
            "Auwaerter",
            "Benalu",
            "Berger",
            "Bertinovec",
            "Blumhardt",
            "Boeckmann",
            "Brenderup",
            "Cardi",
            "Carnehl",
            "Cifa",
            "Contar",
            "Dinkel",
            "Dorgler",
            "Ellebi",
            "Ernst-Riedler",
            "Feldbinder",
            "Fliegl",
            "Goldhofer",
            "Goodrich",
            "Goša",
            "Gotha",
            "Hangler",
            "Harbeck",
            "Humbaur",
            "Intermix",
            "ISI",
            "Itas",
            "Jansky",
            "Kaessbohrer",
            "Kempf",
            "Knapen",
            "Koegel",
            "Kotschenreuther",
            "Krone",
            "Kumlin",
            "Langendorf",
            "Losken",
            "Luck",
            "Meiller",
            "Meirling",
            "Menci",
            "Menke-Janzen",
            "Meusburger",
            "Meyer",
            "Mirofret",
            "MOL",
            "Moslein",
            "Mueller-Mitteltal",
            "Netam",
            "NFP-Eurotrailer",
            "Omep",
            "Orthaus",
            "Peischl",
            "Pongratz",
            "Rice",
            "Rohr",
            "Rolfo",
            "Ropa",
            "Scania",
            "Schmitz",
            "Schwarte-Jansky",
            "Schwarzmueller",
            "Sommer",
            "Spermann",
            "Spitzer",
            "Stas",
            "Tieflader",
            "Torbarina",
            "Viberti",
            "Wackenhut",
            "WM-Meyer",
            "Yuksel",
            "Zorzi",
            "Zovko",
            "Ostalo"
        );

        echo 'Importing trailer brands...' . PHP_EOL;

        $root_dict = new Dictionary();
        $root_dict->name = 'AMN - Prikolice - Proizvođač';
        $root_dict->slug = 'amn_trailer_brand';
        $root_dict->active = 1;
        $root_dict->saveNode();

        if ($root_dict->id) {
            foreach ($data as $row) {
                $root = Dictionary::findFirst($root_dict->id);
                $name = trim($row);

                $parent_val = new Dictionary();
                $parent_val->name = $name;
                $parent_val->active = 1;
                $parent_val->appendTo($root);
            }
        }
    }

    public function vesselType()
    {
        $data = array(
            "gliser",
            "gumenjak",
            "jahta",
            "jedrilica",
            "jet ski",
            "motorni brod",
            "oprema za nautiku",
            "ostalo"
        );

        echo 'Importing vessel types...' . PHP_EOL;

        $root_dict = new Dictionary();
        $root_dict->name = 'AMN - Plovila - Vrsta';
        $root_dict->slug = 'amn_vessel_type';
        $root_dict->active = 1;
        $root_dict->saveNode();

        if ($root_dict->id) {
            foreach ($data as $row) {
                $root = Dictionary::findFirst($root_dict->id);
                $name = trim($row);

                $parent_val = new Dictionary();
                $parent_val->name = $name;
                $parent_val->active = 1;
                $parent_val->appendTo($root);
            }
        }
    }

    public function boatDrive()
    {
        $data = array(
            "jedra",
            "vanbrodski motor",
            "ugrađeni motor",
            "vesla",
            "ostalo"
        );

        echo 'Importing vessel primary drives...' . PHP_EOL;

        $root_dict = new Dictionary();
        $root_dict->name = 'AMN - Plovila - Primarni pogon';
        $root_dict->slug = 'amn_boat_drive';
        $root_dict->active = 1;
        $root_dict->saveNode();

        if ($root_dict->id) {
            foreach ($data as $row) {
                $root = Dictionary::findFirst($root_dict->id);
                $name = trim($row);

                $parent_val = new Dictionary();
                $parent_val->name = $name;
                $parent_val->active = 1;
                $parent_val->appendTo($root);
            }
        }
    }

    public function boatMaterial()
    {
        $data = array(
            "aluminij",
            "čelik",
            "drvo",
            "gumenjak",
            "plastika",
            "ostalo"
        );

        echo 'Importing vessel materials...' . PHP_EOL;

        $root_dict = new Dictionary();
        $root_dict->name = 'AMN - Plovila - Građa trupa';
        $root_dict->slug = 'amn_boat_material';
        $root_dict->active = 1;
        $root_dict->saveNode();

        if ($root_dict->id) {
            foreach ($data as $row) {
                $root = Dictionary::findFirst($root_dict->id);
                $name = trim($row);

                $parent_val = new Dictionary();
                $parent_val->name = $name;
                $parent_val->active = 1;
                $parent_val->appendTo($root);
            }
        }
    }

    public function vesselCondition()
    {
        $data = array(
            "novo",
            "korišteno, ali kao novo",
            "korišteno",
            "oštećeno"
        );

        echo 'Importing vessel conditions...' . PHP_EOL;

        $root_dict = new Dictionary();
        $root_dict->name = 'AMN - Plovila - Stanje';
        $root_dict->slug = 'amn_vessel_condition';
        $root_dict->active = 1;
        $root_dict->saveNode();

        if ($root_dict->id) {
            foreach ($data as $row) {
                $root = Dictionary::findFirst($root_dict->id);
                $name = trim($row);

                $parent_val = new Dictionary();
                $parent_val->name = $name;
                $parent_val->active = 1;
                $parent_val->appendTo($root);
            }
        }
    }

    public function machineWorkingType()
    {
        $data = array(
            "dizalica",
            "građevinski stroj",
            "stroj za obradu",
            "šumarski stroj",
            "viličar",
            "ostalo"
        );

        echo 'Importing working machine types...' . PHP_EOL;

        $root_dict = new Dictionary();
        $root_dict->name = 'AMN - Radni strojevi - Vrsta';
        $root_dict->slug = 'machine_working_type';
        $root_dict->active = 1;
        $root_dict->saveNode();

        if ($root_dict->id) {
            foreach ($data as $row) {
                $root = Dictionary::findFirst($root_dict->id);
                $name = trim($row);

                $parent_val = new Dictionary();
                $parent_val->name = $name;
                $parent_val->active = 1;
                $parent_val->appendTo($root);
            }
        }
    }

    public function machineWorkingBrand()
    {
        $data = array(
            "Atlas",
            "Bomag",
            "Caterpillar",
            "Corghi",
            "Cormach",
            "Euclid",
            "Extec",
            "Hanomag",
            "Hiab",
            "Hitachi",
            "HMF",
            "Hyster",
            "Hyundai",
            "Indeco",
            "Indos",
            "Intermix",
            "Jcb",
            "Komatsu",
            "Liebherr",
            "Linde",
            "Loglift",
            "Montabert",
            "OK",
            "Palfinger",
            "Putzmeister",
            "Radoje Dakić",
            "Riko",
            "RK",
            "Ruthman",
            "Schmidt",
            "Schwing",
            "Sip",
            "Stetter",
            "Takeuchi",
            "TCM",
            "Tehnostroj",
            "Terex",
            "Timberjack",
            "ULT",
            "Vogele",
            "Westfalia",
            "Yanmar",
            "Ostalo"
        );

        echo 'Importing working machine brands...' . PHP_EOL;

        $root_dict = new Dictionary();
        $root_dict->name = 'AMN - Radni strojevi - Proizvođač';
        $root_dict->slug = 'machine_working_brand';
        $root_dict->active = 1;
        $root_dict->saveNode();

        if ($root_dict->id) {
            foreach ($data as $row) {
                $root = Dictionary::findFirst($root_dict->id);
                $name = trim($row);

                $parent_val = new Dictionary();
                $parent_val->name = $name;
                $parent_val->active = 1;
                $parent_val->appendTo($root);
            }
        }
    }

    public function machineAgricultureType()
    {
        $data = array(
            "traktor",
            "kombajn",
            "prikolica",
            "motokultivator",
            "priključak",
            "ostalo"
        );

        echo 'Importing agriculture machine types...' . PHP_EOL;

        $root_dict = new Dictionary();
        $root_dict->name = 'AMN - Poljoprivredni strojevi - Vrsta';
        $root_dict->slug = 'machine_agriculture_type';
        $root_dict->active = 1;
        $root_dict->saveNode();

        if ($root_dict->id) {
            foreach ($data as $row) {
                $root = Dictionary::findFirst($root_dict->id);
                $name = trim($row);

                $parent_val = new Dictionary();
                $parent_val->name = $name;
                $parent_val->active = 1;
                $parent_val->appendTo($root);
            }
        }
    }

    public function machineAgricultureBrand()
    {
        $data = array(
            "Belarus",
            "Hanomag",
            "Kubota",
            "Porsche",
            "Same",
            "Stayer",
            "Tehnostroj",
            "Tomo Vinković",
            "Torpedo",
            "Westfalia",
            "Yanmar",
            "Zetor",
            "Ostalo"
        );

        echo 'Importing agriculture machine brands...' . PHP_EOL;

        $root_dict = new Dictionary();
        $root_dict->name = 'AMN - Poljoprivredni strojevi - Proizvođač';
        $root_dict->slug = 'machine_agriculture_brand';
        $root_dict->active = 1;
        $root_dict->saveNode();

        if ($root_dict->id) {
            foreach ($data as $row) {
                $root = Dictionary::findFirst($root_dict->id);
                $name = trim($row);

                $parent_val = new Dictionary();
                $parent_val->name = $name;
                $parent_val->active = 1;
                $parent_val->appendTo($root);
            }
        }
    }

    public function deleteAllParameters()
    {
        $parameters = Parameter::find();
        if ($parameters && count($parameters)) {
            echo 'Deleting all parameters...' . PHP_EOL;
            foreach ($parameters as $parameter) {
                $parameter->delete();
            }
            $db = new RawDB();
            $db->query('ALTER TABLE parameters AUTO_INCREMENT =1;');
        }
    }

    public function basicParameters()
    {
        $data = array(
            array('name' => 'Ad title', 'type_id' => 'TITLE'),
            array('name' => 'Ad price', 'type_id' => 'CURRENCY'),
            array('name' => 'Ad description', 'type_id' => 'DESCRIPTION'),
            array('name' => 'Ad location', 'type_id' => 'LOCATION'),
            array('name' => 'Ad uploadables', 'type_id' => 'UPLOADABLE')
        );

        echo 'Importing basic parameters...' . PHP_EOL;

        foreach ($data as $row) {
            $parameter = new Parameter();
            $parameter->name = $row['name'];
            $parameter->type_id = $row['type_id'];
            $parameter->create();
        }
    }

    public function customParameters()
    {
        $data = array(
            array('name' => 'Youtube video', 'type_id' => 'URL'),
            array('name' => 'AMN - Auti - Marka i model', 'type_id' => 'DEPENDABLE_DROPDOWN', 'dictionary_slug' => 'vehicle_make_model'),
            array('name' => 'AMN - Godina proizvodnje', 'type_id' => 'YEAR'),
            array('name' => 'AMN - Kilometraža', 'type_id' => 'NUMBER'),
            array('name' => 'AMN - Auti - Vrsta goriva', 'type_id' => 'DROPDOWN', 'dictionary_slug' => 'vehicle_fuel_type'),
            array('name' => 'AMN - Auti - Snaga motora', 'type_id' => 'NUMBER'),
            array('name' => 'AMN - Auti - Radni obujam', 'type_id' => 'NUMBER'),
            array('name' => 'AMN - Registriran do', 'type_id' => 'MONTHYEAR'),
            array('name' => 'AMN - Auti - Pogon', 'type_id' => 'DROPDOWN', 'dictionary_slug' => 'vehicle_drivetrain'),
            array('name' => 'AMN - Auti - Vrsta mjenjača', 'type_id' => 'DROPDOWN', 'dictionary_slug' => 'vehicle_gear_type'),
            array('name' => 'AMN - Auti - Broj brzina', 'type_id' => 'DROPDOWN', 'dictionary_slug' => 'vehicle_gear_number'),
            array('name' => 'AMN - Auti - Broj vrata', 'type_id' => 'DROPDOWN', 'dictionary_slug' => 'vehicle_door_number'),
            array('name' => 'AMN - Auti - Oblik karoserije', 'type_id' => 'DROPDOWN', 'dictionary_slug' => 'vehicle_body_type'),
            array('name' => 'AMN - Auti - Dodatna oprema', 'type_id' => 'CHECKBOXGROUP', 'dictionary_slug' => 'vehicle_extra_equipment'),
            array('name' => 'AMN - Auti - Sigurnost', 'type_id' => 'CHECKBOXGROUP', 'dictionary_slug' => 'vehicle_safety'),
            array('name' => 'AMN - Auti - Sigurnost od krađe', 'type_id' => 'CHECKBOXGROUP', 'dictionary_slug' => 'vehicle_theft_safety'),
            array('name' => 'AMN - Auti - Udobnost', 'type_id' => 'CHECKBOXGROUP', 'dictionary_slug' => 'vehicle_comfort_equipment'),
            array('name' => 'AMN - Auti - Broj sjedala', 'type_od' => 'DROPDOWN', 'dictionary_slug' => 'vehicle_seat_number'),
            array('name' => 'AMN - Novo/Rabljeno', 'type_id' => 'RADIO', 'dictionary_slug' => 'amn_new_used'),
            array('name' => 'AMN - Auti - Oldtimer', 'type_id' => 'CHECKBOX'),
            array('name' => 'AMN - Auti - Stanje', 'type_id' => 'DROPDOWN', 'dictionary_slug' => 'vehicle_condition'),
            array('name' => 'AMN - Vlasnik', 'type_id' => 'DROPDOWN', 'dictionary_slug' => 'amn_owner'),
            array('name' => 'AMN - Auti - Boja', 'type_id' => 'DROPDOWN', 'dictionary_slug' => 'vehicle_color'),
            array('name' => 'AMN - Servisna knjižica', 'type_id' => 'CHECKBOX'),
            array('name' => 'AMN - Auti - Garažiran', 'type_id' => 'CHECKBOX'),
/*
            array('name' => 'AMN - Auti - ', 'type_id' => 'DROPDOWN', 'dictionary_slug' => ''),
            array('name' => 'AMN - Auti - ', 'type_id' => 'DROPDOWN', 'dictionary_slug' => ''),
            array('name' => 'AMN - Auti - ', 'type_id' => 'DROPDOWN', 'dictionary_slug' => ''),
            array('name' => 'AMN - Auti - ', 'type_id' => 'DROPDOWN', 'dictionary_slug' => ''),
            array('name' => 'AMN - Auti - ', 'type_id' => 'DROPDOWN', 'dictionary_slug' => ''),
            array('name' => 'AMN - Auti - ', 'type_id' => 'DROPDOWN', 'dictionary_slug' => ''),
            array('name' => 'AMN - Auti - ', 'type_id' => 'DROPDOWN', 'dictionary_slug' => ''),
            array('name' => 'AMN - Auti - ', 'type_id' => 'DROPDOWN', 'dictionary_slug' => ''),
            array('name' => 'AMN - Auti - ', 'type_id' => 'DROPDOWN', 'dictionary_slug' => ''),
            array('name' => 'AMN - Auti - ', 'type_id' => 'DROPDOWN', 'dictionary_slug' => ''),
            array('name' => 'AMN - Auti - ', 'type_id' => 'DROPDOWN', 'dictionary_slug' => ''),
            array('name' => 'AMN - Auti - ', 'type_id' => 'DROPDOWN', 'dictionary_slug' => ''),
*/
        );

        echo 'Importing custom parameters...' . PHP_EOL;

        foreach ($data as $row) {
            $parameter = new Parameter();
            $parameter->name = $row['name'];
            $parameter->type_id = $row['type_id'];
            if (isset($row['dictionary_slug'])) {
                $dictionary = Dictionary::findFirstBySlug($row['dictionary_slug']);
                if ($dictionary) {
                    $parameter->dictionary_id = $dictionary->id;
                }
            }
            $parameter->create();
        }
    }


    //-------- nekretnine ---------
    public function flatType()
    {
        $data = array(
            "stan u zgradi",
            "stan u kući",
            "stan, višeetažni u zgradi",
            "stan, višeetažni u kući",
            "apartmansko naselje",
            "ostalo"
        );

        echo 'Importing flat types...' . PHP_EOL;

        $root_dict = new Dictionary();
        $root_dict->name = 'Nekretnine - Stanovi - Vrsta';
        $root_dict->slug = 'flat_type';
        $root_dict->active = 1;
        $root_dict->saveNode();

        if ($root_dict->id) {
            foreach ($data as $row) {
                $root = Dictionary::findFirst($root_dict->id);
                $name = trim($row);

                $parent_val = new Dictionary();
                $parent_val->name = $name;
                $parent_val->active = 1;
                $parent_val->appendTo($root);
            }
        }
    }

    public function flatRooms()
    {
        $data = array(
            "garsonijera",
            "1-sobni",
            "1,5-sobni",
            "2-sobni",
            "2,5-sobni",
            "3-sobni",
            "3,5-sobni",
            "4-sobni",
            "5-sobni i više"
        );

        echo 'Importing flat rooms...' . PHP_EOL;

        $root_dict = new Dictionary();
        $root_dict->name = 'Nekretnine - Stanovi - Sobnost';
        $root_dict->slug = 'flat_rooms';
        $root_dict->active = 1;
        $root_dict->saveNode();

        if ($root_dict->id) {
            foreach ($data as $row) {
                $root = Dictionary::findFirst($root_dict->id);
                $name = trim($row);

                $parent_val = new Dictionary();
                $parent_val->name = $name;
                $parent_val->active = 1;
                $parent_val->appendTo($root);
            }
        }
    }

    public function flatPriceFurnishment()
    {
        $data = array(
            "nedovršen građevinski",
            "dovršen, neopremljen",
            "djelomično opremljen",
            "potpuno opremljen"
        );

        echo 'Importing flat price furnishment...' . PHP_EOL;

        $root_dict = new Dictionary();
        $root_dict->name = 'Nekretnine - Stanovi - Opremljenost';
        $root_dict->slug = 'flat_price_furnishment';
        $root_dict->active = 1;
        $root_dict->saveNode();

        if ($root_dict->id) {
            foreach ($data as $row) {
                $root = Dictionary::findFirst($root_dict->id);
                $name = trim($row);

                $parent_val = new Dictionary();
                $parent_val->name = $name;
                $parent_val->active = 1;
                $parent_val->appendTo($root);
            }
        }
    }

    public function flatMaterialsWindows()
    {
        $data = array(
            "drveni",
            "PVC",
            "drvo-aluminij",
            "aluminij",
            "metalni",
            "ostalo"
        );

        echo 'Importing materials windows...' . PHP_EOL;

        $root_dict = new Dictionary();
        $root_dict->name = 'Nekretnine - Tip prozora';
        $root_dict->slug = 're_materials_windows';
        $root_dict->active = 1;
        $root_dict->saveNode();

        if ($root_dict->id) {
            foreach ($data as $row) {
                $root = Dictionary::findFirst($root_dict->id);
                $name = trim($row);

                $parent_val = new Dictionary();
                $parent_val->name = $name;
                $parent_val->active = 1;
                $parent_val->appendTo($root);
            }
        }
    }

    public function flatMaterialsDoors()
    {
        $data = array(
            "vrata protuprovalna",
            "vrata drvena",
            "vrata alu",
            "vrata metalna",
            "vrata PVC",
            "ostalo"
        );

        echo 'Importing materials windows...' . PHP_EOL;

        $root_dict = new Dictionary();
        $root_dict->name = 'Nekretnine - Vanjska vrata';
        $root_dict->slug = 're_materials_doors';
        $root_dict->active = 1;
        $root_dict->saveNode();

        if ($root_dict->id) {
            foreach ($data as $row) {
                $root = Dictionary::findFirst($root_dict->id);
                $name = trim($row);

                $parent_val = new Dictionary();
                $parent_val->name = $name;
                $parent_val->active = 1;
                $parent_val->appendTo($root);
            }
        }
    }

    public function flatMaterialsFloors()
    {
        $data = array(
            "parket lamele",
            "parket masivni",
            "brodski pod",
            "laminat",
            "ostalo"
        );

        echo 'Importing materials floors...' . PHP_EOL;

        $root_dict = new Dictionary();
        $root_dict->name = 'Nekretnine - Tip poda';
        $root_dict->slug = 're_materials_floors';
        $root_dict->active = 1;
        $root_dict->saveNode();

        if ($root_dict->id) {
            foreach ($data as $row) {
                $root = Dictionary::findFirst($root_dict->id);
                $name = trim($row);

                $parent_val = new Dictionary();
                $parent_val->name = $name;
                $parent_val->active = 1;
                $parent_val->appendTo($root);
            }
        }
    }

    public function flatHeatingType()
    {
        $data = array(
            "plin centralno",
            "plin etažno",
            "plinske peći",
            "lož ulje centralno",
            "lož ulje peći",
            "gradska toplana",
            "električno grijanje, centralno",
            "električno grijanje, pojedinačno",
            "drva/ugljen",
            "solarno",
            "klima uređajem s grijanjem",
            "ostalo"
        );

        echo 'Importing heating type...' . PHP_EOL;

        $root_dict = new Dictionary();
        $root_dict->name = 'Nekretnine - Vrsta grijanja';
        $root_dict->slug = 're_heating_type';
        $root_dict->active = 1;
        $root_dict->saveNode();

        if ($root_dict->id) {
            foreach ($data as $row) {
                $root = Dictionary::findFirst($root_dict->id);
                $name = trim($row);

                $parent_val = new Dictionary();
                $parent_val->name = $name;
                $parent_val->active = 1;
                $parent_val->appendTo($root);
            }
        }
    }

    public function flatFloorNumber()
    {
        $data = array(
            "podzemlje",
            "suteren",
            "prizemlje",
            "mezanin",
            "potkrovlje",
            "1. kat",
            "2. kat",
            "3. kat",
            "4. kat",
            "5. kat",
            "6. - 10. kat",
            "11. - 15. kat",
            "16. - 20. kat",
            "21. - 25. kat",
            "26. kat i više"
        );

        echo 'Importing floor number...' . PHP_EOL;

        $root_dict = new Dictionary();
        $root_dict->name = 'Nekretnine - Kat';
        $root_dict->slug = 'floor_number';
        $root_dict->active = 1;
        $root_dict->saveNode();

        if ($root_dict->id) {
            foreach ($data as $row) {
                $root = Dictionary::findFirst($root_dict->id);
                $name = trim($row);

                $parent_val = new Dictionary();
                $parent_val->name = $name;
                $parent_val->active = 1;
                $parent_val->appendTo($root);
            }
        }
    }

    public function flatExtra()
    {
        $data = array(
            "lift",
            "niskoenergetska učinkovitost",
            "klima",
            "pristup invalidima",
            "fiksni telefon",
            "ISDN priključak",
            "ADSL",
            "kablovski pristup",
            "optički pristup",
            "TV antena",
            "satelitski priključak",
            "video nadzor",
            "alarm",
            "bazen",
            "roštilj"
        );

        echo 'Importing flat extras...' . PHP_EOL;

        $root_dict = new Dictionary();
        $root_dict->name = 'Nekretnine - Stanovi - Dodatno';
        $root_dict->slug = 'flat_extra';
        $root_dict->active = 1;
        $root_dict->saveNode();

        if ($root_dict->id) {
            foreach ($data as $row) {
                $root = Dictionary::findFirst($root_dict->id);
                $name = trim($row);

                $parent_val = new Dictionary();
                $parent_val->name = $name;
                $parent_val->active = 1;
                $parent_val->appendTo($root);
            }
        }
    }

    public function flatNearness()
    {
        $data = array(
            "autobus",
            "bolnica",
            "dom zdravlja",
            "fakultet",
            "jezero, more ili rijeka",
            "osnovna škola",
            "park ili šuma",
            "sportski objekti",
            "srednja škola",
            "tramvaj",
            "trgovački centar (veliki)",
            "vlak",
            "vrtić"
        );

        echo 'Importing real estate nearness...' . PHP_EOL;

        $root_dict = new Dictionary();
        $root_dict->name = 'Nekretnine - Blizina';
        $root_dict->slug = 're_nearness';
        $root_dict->active = 1;
        $root_dict->saveNode();

        if ($root_dict->id) {
            foreach ($data as $row) {
                $root = Dictionary::findFirst($root_dict->id);
                $name = trim($row);

                $parent_val = new Dictionary();
                $parent_val->name = $name;
                $parent_val->active = 1;
                $parent_val->appendTo($root);
            }
        }
    }

    public function re_flat_parameters()
    {
        $data = array(
            array('name' => 'Nekretnine - Stanovi - Vrsta stana', 'type_id' => 'DROPDOWN', 'dictionary_slug' => 'flat_type'),
            array('name' => 'Nekretnine - Stanovi - Sobnost', 'type_id' => 'DROPDOWN', 'dictionary_slug' => 'flat_rooms'),
            array('name' => 'Nekretnine - Stanovi - Površina uk.', 'type_id' => 'NUMBER'),
            array('name' => 'Nekretnine - Stanovi - Stambena površina', 'type_id' => 'NUMBER'),
            array('name' => 'Nekretnine - Stanovi - Površina vrta', 'type_id' => 'NUMBER'),
            array('name' => 'Nekretnine - Stanovi - Površina parkirališnih mjesta', 'type_id' => 'NUMBER'),
            array('name' => 'Nekretnine - Stanovi - Vlasnički list', 'type_id' => 'CHECKBOX'),
            array('name' => 'Nekretnine - Stanovi - Građevinska dozvola', 'type_id' => 'CHECKBOX'),
            array('name' => 'Nekretnine - Stanovi - Uporabna dozvola', 'type_id' => 'CHECKBOX'),
            array('name' => 'Nekretnine - Stanovi - Opremljenost', 'type_id' => 'DROPDOWN', 'dictionary_slug' => 'flat_price_furnishment'),
            array('name' => 'Nekretnine - Stanovi - Broj kupaona', 'type_id' => 'NUMBER'),
            array('name' => 'Nekretnine - Stanovi - Broj sanitarnih čvorova', 'type_id' => 'NUMBER'),
            array('name' => 'Nekretnine - Stanovi - Broj terasa', 'type_id' => 'NUMBER'),
            array('name' => 'Nekretnine - Stanovi - Broj balkona', 'type_id' => 'NUMBER'),
            array('name' => 'Nekretnine - Stanovi - Broj parkirališnih mjesta', 'type_id' => 'NUMBER'),
            array('name' => 'Nekretnine - Tip prozora', 'type_id' => 'DROPDOWN', 'dictionary_slug' => 're_materials_windows'),
            array('name' => 'Nekretnine - Vanjska vrata', 'type_id' => 'DROPDOWN', 'dictionary_slug' => 're_materials_doors'),
            array('name' => 'Nekretnine - Tip poda', 'type_id' => 'DROPDOWN', 'dictionary_slug' => 're_materials_floors'),
            array('name' => 'Nekretnine - Stanovi - Grijanje', 'type_id' => 'DROPDOWN', 'dictionary_slug' => 're_heating_type'),
            array('name' => 'Nekretnine - Stanovi - Kat', 'type_id' => 'DROPDOWN', 'dictionary_slug' => 'floor_number'),
            array('name' => 'Nekretnine - Stanovi - Dodatno', 'type_id' => 'DROPDOWN', 'dictionary_slug' => 'flat_extra'),
            array('name' => 'Nekretnine - Blizina', 'type_id' => 'DROPDOWN', 'dictionary_slug' => 're_nearness')
        );

        echo 'Importing flat parameters...' . PHP_EOL;

        foreach ($data as $row) {
            $parameter = new Parameter();
            $parameter->name = $row['name'];
            $parameter->type_id = $row['type_id'];
            if (isset($row['dictionary_slug'])) {
                $dictionary = Dictionary::findFirstBySlug($row['dictionary_slug']);
                if ($dictionary) {
                    $parameter->dictionary_id = $dictionary->id;
                }
            }
            $parameter->create();
        }
    }



}
