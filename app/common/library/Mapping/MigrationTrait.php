<?php

namespace Baseapp\Library\Mapping;

trait MigrationTrait
{

    protected $cat_ids = array(
        '21'         => 'Auto Moto Nautika',
        '2101'       => 'Auto Moto Nautika › Automobili',
        '210101'     => 'Auto Moto Nautika › Automobili › Ponuda',
        '21010101'   => 'Auto Moto Nautika › Automobili › Ponuda › Alfa Romeo',
        '21010102'   => 'Auto Moto Nautika › Automobili › Ponuda › Audi',
        '21010103'   => 'Auto Moto Nautika › Automobili › Ponuda › BMW',
        '21010104'   => 'Auto Moto Nautika › Automobili › Ponuda › Chrysler',
        '21010105'   => 'Auto Moto Nautika › Automobili › Ponuda › Citroen',
        '21010106'   => 'Auto Moto Nautika › Automobili › Ponuda › Daewoo',
        '21010107'   => 'Auto Moto Nautika › Automobili › Ponuda › Daihatsu',
        '21010108'   => 'Auto Moto Nautika › Automobili › Ponuda › Fiat',
        '21010109'   => 'Auto Moto Nautika › Automobili › Ponuda › Ford',
        '21010111'   => 'Auto Moto Nautika › Automobili › Ponuda › Honda',
        '21010112'   => 'Auto Moto Nautika › Automobili › Ponuda › Hyundai',
        '21010113'   => 'Auto Moto Nautika › Automobili › Ponuda › Jaguar',
        '21010114'   => 'Auto Moto Nautika › Automobili › Ponuda › Kia',
        '21010115'   => 'Auto Moto Nautika › Automobili › Ponuda › Lada',
        '21010116'   => 'Auto Moto Nautika › Automobili › Ponuda › Lancia',
        '21010117'   => 'Auto Moto Nautika › Automobili › Ponuda › Lexus',
        '21010118'   => 'Auto Moto Nautika › Automobili › Ponuda › Mazda',
        '21010119'   => 'Auto Moto Nautika › Automobili › Ponuda › Mercedes',
        '21010120'   => 'Auto Moto Nautika › Automobili › Ponuda › Mitsubishi',
        '21010121'   => 'Auto Moto Nautika › Automobili › Ponuda › Nissan',
        '21010122'   => 'Auto Moto Nautika › Automobili › Ponuda › Opel',
        '21010123'   => 'Auto Moto Nautika › Automobili › Ponuda › Peugeot',
        '21010124'   => 'Auto Moto Nautika › Automobili › Ponuda › Porsche',
        '21010125'   => 'Auto Moto Nautika › Automobili › Ponuda › Renault',
        '21010126'   => 'Auto Moto Nautika › Automobili › Ponuda › Rover',
        '21010127'   => 'Auto Moto Nautika › Automobili › Ponuda › Saab',
        '21010128'   => 'Auto Moto Nautika › Automobili › Ponuda › Seat',
        '21010129'   => 'Auto Moto Nautika › Automobili › Ponuda › Smart',
        '21010130'   => 'Auto Moto Nautika › Automobili › Ponuda › Subaru',
        '21010131'   => 'Auto Moto Nautika › Automobili › Ponuda › Suzuki',
        '21010132'   => 'Auto Moto Nautika › Automobili › Ponuda › Škoda',
        '21010133'   => 'Auto Moto Nautika › Automobili › Ponuda › Toyota',
        '21010134'   => 'Auto Moto Nautika › Automobili › Ponuda › VW',
        '21010135'   => 'Auto Moto Nautika › Automobili › Ponuda › Volvo',
        '21010136'   => 'Auto Moto Nautika › Automobili › Ponuda › Zastava',
        '21010137'   => 'Auto Moto Nautika › Automobili › Ponuda › Ostalo',
        '21010138'   => 'Auto Moto Nautika › Automobili › Ponuda › Chevrolet',
        '21010139'   => 'Auto Moto Nautika › Automobili › Ponuda › Dacia',
        '21010140'   => 'Auto Moto Nautika › Automobili › Ponuda › Dodge',
        '21010141'   => 'Auto Moto Nautika › Automobili › Ponuda › Ferrari',
        '21010142'   => 'Auto Moto Nautika › Automobili › Ponuda › Hummer',
        '21010143'   => 'Auto Moto Nautika › Automobili › Ponuda › Isuzu',
        '21010144'   => 'Auto Moto Nautika › Automobili › Ponuda › Jeep',
        '21010145'   => 'Auto Moto Nautika › Automobili › Ponuda › Land Rover',
        '21010146'   => 'Auto Moto Nautika › Automobili › Ponuda › Maserati',
        '21010147'   => 'Auto Moto Nautika › Automobili › Ponuda › Mini',
        '21010148'   => 'Auto Moto Nautika › Automobili › Ponuda › SsangYong',
        '210102'     => 'Auto Moto Nautika › Automobili › Potražnja',
        '210103'     => 'Auto Moto Nautika › Automobili › Zamjena',
        '210104'     => 'Auto Moto Nautika › Automobili › Iznajmljivanje',
        '210105'     => 'Auto Moto Nautika › Automobili › Unajmljivanje',
        '2102'       => 'Auto Moto Nautika › Motocikli',
        '210201'     => 'Auto Moto Nautika › Motocikli › Ponuda',
        '21020101'   => 'Auto Moto Nautika › Motocikli › Ponuda › Motocikli',
        '21020103'   => 'Auto Moto Nautika › Motocikli › Ponuda › Dijelovi i oprema',
        '210202'     => 'Auto Moto Nautika › Motocikli › Potražnja',
        '210203'     => 'Auto Moto Nautika › Motocikli › Zamjena',
        '2103'       => 'Auto Moto Nautika › Autodijelovi',
        '210301'     => 'Auto Moto Nautika › Autodijelovi › Ponuda',
        '21030101'   => 'Auto Moto Nautika › Autodijelovi › Ponuda › Alfa',
        '21030102'   => 'Auto Moto Nautika › Autodijelovi › Ponuda › Audi',
        '21030103'   => 'Auto Moto Nautika › Autodijelovi › Ponuda › BMW',
        '21030104'   => 'Auto Moto Nautika › Autodijelovi › Ponuda › Chrysler',
        '21030105'   => 'Auto Moto Nautika › Autodijelovi › Ponuda › Citroen',
        '21030106'   => 'Auto Moto Nautika › Autodijelovi › Ponuda › Daewoo',
        '21030107'   => 'Auto Moto Nautika › Autodijelovi › Ponuda › Daihatsu',
        '21030108'   => 'Auto Moto Nautika › Autodijelovi › Ponuda › Fiat',
        '21030109'   => 'Auto Moto Nautika › Autodijelovi › Ponuda › Ford',
        '21030110'   => 'Auto Moto Nautika › Autodijelovi › Ponuda › GM',
        '21030111'   => 'Auto Moto Nautika › Autodijelovi › Ponuda › Honda',
        '21030112'   => 'Auto Moto Nautika › Autodijelovi › Ponuda › Hyundai',
        '21030113'   => 'Auto Moto Nautika › Autodijelovi › Ponuda › Jaguar',
        '21030114'   => 'Auto Moto Nautika › Autodijelovi › Ponuda › Kia',
        '21030115'   => 'Auto Moto Nautika › Autodijelovi › Ponuda › Lada',
        '21030116'   => 'Auto Moto Nautika › Autodijelovi › Ponuda › Lancia',
        '21030117'   => 'Auto Moto Nautika › Autodijelovi › Ponuda › Lexus',
        '21030118'   => 'Auto Moto Nautika › Autodijelovi › Ponuda › Mazda',
        '21030119'   => 'Auto Moto Nautika › Autodijelovi › Ponuda › Mercedes',
        '21030120'   => 'Auto Moto Nautika › Autodijelovi › Ponuda › Mitsubishi',
        '21030121'   => 'Auto Moto Nautika › Autodijelovi › Ponuda › Nissan',
        '21030122'   => 'Auto Moto Nautika › Autodijelovi › Ponuda › Opel',
        '21030123'   => 'Auto Moto Nautika › Autodijelovi › Ponuda › Peugeot',
        '21030124'   => 'Auto Moto Nautika › Autodijelovi › Ponuda › Porsche',
        '21030125'   => 'Auto Moto Nautika › Autodijelovi › Ponuda › Renault',
        '21030126'   => 'Auto Moto Nautika › Autodijelovi › Ponuda › Rover',
        '21030127'   => 'Auto Moto Nautika › Autodijelovi › Ponuda › Saab',
        '21030128'   => 'Auto Moto Nautika › Autodijelovi › Ponuda › Seat',
        '21030129'   => 'Auto Moto Nautika › Autodijelovi › Ponuda › Smart',
        '21030130'   => 'Auto Moto Nautika › Autodijelovi › Ponuda › Subaru',
        '21030131'   => 'Auto Moto Nautika › Autodijelovi › Ponuda › Suzuki',
        '21030132'   => 'Auto Moto Nautika › Autodijelovi › Ponuda › Škoda',
        '21030133'   => 'Auto Moto Nautika › Autodijelovi › Ponuda › Toyota',
        '21030134'   => 'Auto Moto Nautika › Autodijelovi › Ponuda › VW',
        '21030135'   => 'Auto Moto Nautika › Autodijelovi › Ponuda › Volvo',
        '21030136'   => 'Auto Moto Nautika › Autodijelovi › Ponuda › Zastava',
        '21030137'   => 'Auto Moto Nautika › Autodijelovi › Ponuda › Ostalo',
        '210302'     => 'Auto Moto Nautika › Autodijelovi › Potražnja',
        '210303'     => 'Auto Moto Nautika › Autodijelovi › Zamjena',
        '2104'       => 'Auto Moto Nautika › Radni i poljoprivredni strojevi',
        '210401'     => 'Auto Moto Nautika › Radni i poljoprivredni strojevi › Ponuda',
        '21040101'   => 'Auto Moto Nautika › Radni i poljoprivredni strojevi › Ponuda › Radni strojevi',
        '21040102'   => 'Auto Moto Nautika › Radni i poljoprivredni strojevi › Ponuda › Poljoprivredni strojevi',
        '21040103'   => 'Auto Moto Nautika › Radni i poljoprivredni strojevi › Ponuda › Priključci i ostalo',
        '210402'     => 'Auto Moto Nautika › Radni i poljoprivredni strojevi › Potražnja',
        '210403'     => 'Auto Moto Nautika › Radni i poljoprivredni strojevi › Zamjena',
        '210404'     => 'Auto Moto Nautika › Radni i poljoprivredni strojevi › Iznajmljivanje',
        '2105'       => 'Auto Moto Nautika › Teretna vozila',
        '210501'     => 'Auto Moto Nautika › Teretna vozila › Ponuda',
        '21050101'   => 'Auto Moto Nautika › Teretna vozila › Ponuda › Kombi i dostavna',
        '21050102'   => 'Auto Moto Nautika › Teretna vozila › Ponuda › Kamioni i tegljači',
        '21050103'   => 'Auto Moto Nautika › Teretna vozila › Ponuda › Autobusi i prikolice',
        '21050104'   => 'Auto Moto Nautika › Teretna vozila › Ponuda › Dijelovi i ostalo ponuda',
        '210502'     => 'Auto Moto Nautika › Teretna vozila › Potražnja',
        '210503'     => 'Auto Moto Nautika › Teretna vozila › Zamjena',
        '210504'     => 'Auto Moto Nautika › Teretna vozila › Iznajmljivanje',
        '210505'     => 'Auto Moto Nautika › Teretna vozila › Unajmljivanje',
        '2107'       => 'Auto Moto Nautika › Plovila',
        '210701'     => 'Auto Moto Nautika › Plovila › Ponuda',
        '21070101'   => 'Auto Moto Nautika › Plovila › Ponuda › Plovila',
        '21070102'   => 'Auto Moto Nautika › Plovila › Ponuda › Ostalo',
        '210702'     => 'Auto Moto Nautika › Plovila › Potražnja',
        '210703'     => 'Auto Moto Nautika › Plovila › Zamjena',
        '2108'       => 'Auto Moto Nautika › Oldtimeri',
        '210801'     => 'Auto Moto Nautika › Oldtimeri › Ponuda',
        '210802'     => 'Auto Moto Nautika › Oldtimeri › Potražnja',
        '210803'     => 'Auto Moto Nautika › Oldtimeri › Zamjena',
        '2110'       => 'Auto Moto Nautika › Autooprema',
        '211001'     => 'Auto Moto Nautika › Autooprema › Ponuda',
        '21100101'   => 'Auto Moto Nautika › Autooprema › Ponuda › Tuning & Styling',
        '21100102'   => 'Auto Moto Nautika › Autooprema › Ponuda › Ostalo',
        '211002'     => 'Auto Moto Nautika › Autooprema › Potražnja',
        '2111'       => 'Auto Moto Nautika › Gume i felge',
        '211101'     => 'Auto Moto Nautika › Gume i felge › Ponuda',
        '211102'     => 'Auto Moto Nautika › Gume i felge › Potražnja',
        '22'         => 'Nekretnine',
        '2201'       => 'Nekretnine › Stanovi',
        '220101'     => 'Nekretnine › Stanovi › Ponuda',
        '22010101'   => 'Nekretnine › Stanovi › Ponuda › Zagreb',
        '2201010101' => 'Nekretnine › Stanovi › Ponuda › Zagreb › Centar',
        '2201010102' => 'Nekretnine › Stanovi › Ponuda › Zagreb › Črnomerec',
        '2201010103' => 'Nekretnine › Stanovi › Ponuda › Zagreb › Dubrava',
        '2201010104' => 'Nekretnine › Stanovi › Ponuda › Zagreb › Maksimir',
        '2201010105' => 'Nekretnine › Stanovi › Ponuda › Zagreb › Medveščak',
        '2201010106' => 'Nekretnine › Stanovi › Ponuda › Zagreb › Novi Zagreb',
        '2201010107' => 'Nekretnine › Stanovi › Ponuda › Zagreb › Pešćenica',
        '2201010108' => 'Nekretnine › Stanovi › Ponuda › Zagreb › Sesvete',
        '2201010109' => 'Nekretnine › Stanovi › Ponuda › Zagreb › Susedgrad',
        '2201010110' => 'Nekretnine › Stanovi › Ponuda › Zagreb › Trešnjevka',
        '2201010111' => 'Nekretnine › Stanovi › Ponuda › Zagreb › Trnje',
        '22010102'   => 'Nekretnine › Stanovi › Ponuda › Zagrebačka županija',
        '22010103'   => 'Nekretnine › Stanovi › Ponuda › Središnja Hrvatska',
        '22010104'   => 'Nekretnine › Stanovi › Ponuda › Sjeverna Hrvatska',
        '22010105'   => 'Nekretnine › Stanovi › Ponuda › Istočna Hrvatska',
        '22010106'   => 'Nekretnine › Stanovi › Ponuda › Istra i Kvarner',
        '22010107'   => 'Nekretnine › Stanovi › Ponuda › Dalmacija',
        '22010108'   => 'Nekretnine › Stanovi › Ponuda › Ostalo',
        '220102'     => 'Nekretnine › Stanovi › Potražnja',
        '220103'     => 'Nekretnine › Stanovi › Zamjena',
        '220104'     => 'Nekretnine › Stanovi › Iznajmljivanje stanova',
        '22010401'   => 'Nekretnine › Stanovi › Iznajmljivanje stanova › Grad Zagreb i Zagrebačka županija',
        '22010402'   => 'Nekretnine › Stanovi › Iznajmljivanje stanova › Ostale regije',
        '22010403'   => 'Nekretnine › Stanovi › Iznajmljivanje stanova › Cimeri',
        '220105'     => 'Nekretnine › Stanovi › Unajmljivanje',
        '2202'       => 'Nekretnine › Kuće',
        '220201'     => 'Nekretnine › Kuće › Ponuda',
        '22020101'   => 'Nekretnine › Kuće › Ponuda › Grad Zagreb',
        '22020102'   => 'Nekretnine › Kuće › Ponuda › Zagrebačka županija',
        '22020103'   => 'Nekretnine › Kuće › Ponuda › Središnja Hrvatska',
        '22020104'   => 'Nekretnine › Kuće › Ponuda › Sjeverna Hrvatska',
        '22020105'   => 'Nekretnine › Kuće › Ponuda › Istočna Hrvatska',
        '22020106'   => 'Nekretnine › Kuće › Ponuda › Istra i Kvarner',
        '22020107'   => 'Nekretnine › Kuće › Ponuda › Dalmacija',
        '22020108'   => 'Nekretnine › Kuće › Ponuda › Ostalo',
        '220202'     => 'Nekretnine › Kuće › Potražnja',
        '220203'     => 'Nekretnine › Kuće › Zamjena',
        '220204'     => 'Nekretnine › Kuće › Iznajmljivanje',
        '220205'     => 'Nekretnine › Kuće › Unajmljivanje',
        '2203'       => 'Nekretnine › Građevinska zemljišta',
        '220301'     => 'Nekretnine › Građevinska zemljišta › Ponuda',
        '22030101'   => 'Nekretnine › Građevinska zemljišta › Ponuda › Zagrebačka i Grad Zagreb',
        '22030102'   => 'Nekretnine › Građevinska zemljišta › Ponuda › Sjeverna Hrvatska',
        '22030103'   => 'Nekretnine › Građevinska zemljišta › Ponuda › Središnja Hrvatska',
        '22030104'   => 'Nekretnine › Građevinska zemljišta › Ponuda › Istočna Hrvatska',
        '22030105'   => 'Nekretnine › Građevinska zemljišta › Ponuda › Istra i Kvarner',
        '22030106'   => 'Nekretnine › Građevinska zemljišta › Ponuda › Dalmacija',
        '22030107'   => 'Nekretnine › Građevinska zemljišta › Ponuda › Ostalo',
        '220302'     => 'Nekretnine › Građevinska zemljišta › Potražnja',
        '220303'     => 'Nekretnine › Građevinska zemljišta › Zamjena',
        '220304'     => 'Nekretnine › Građevinska zemljišta › Iznajmljivanje',
        '220305'     => 'Nekretnine › Građevinska zemljišta › Unajmljivanje',
        '2204'       => 'Nekretnine › Vikendice',
        '220401'     => 'Nekretnine › Vikendice › Ponuda',
        '220402'     => 'Nekretnine › Vikendice › Potražnja',
        '220403'     => 'Nekretnine › Vikendice › Zamjena',
        '2205'       => 'Nekretnine › Apartmani',
        '220501'     => 'Nekretnine › Apartmani › Ponuda',
        '220502'     => 'Nekretnine › Apartmani › Potražnja',
        '220503'     => 'Nekretnine › Apartmani › Zamjena',
        '2206'       => 'Nekretnine › Poslovni prostori',
        '220601'     => 'Nekretnine › Poslovni prostori › Ponuda',
        '22060101'   => 'Nekretnine › Poslovni prostori › Ponuda › Lokali Zagreb',
        '22060102'   => 'Nekretnine › Poslovni prostori › Ponuda › Lokali ostalo',
        '22060104'   => 'Nekretnine › Poslovni prostori › Ponuda › Skladišta i hale',
        '220602'     => 'Nekretnine › Poslovni prostori › Potražnja',
        '220603'     => 'Nekretnine › Poslovni prostori › Zamjena',
        '220604'     => 'Nekretnine › Poslovni prostori › Iznajmljivanje',
        '22060401'   => 'Nekretnine › Poslovni prostori › Iznajmljivanje › Zagreb',
        '22060402'   => 'Nekretnine › Poslovni prostori › Iznajmljivanje › Ostalo',
        '220605'     => 'Nekretnine › Poslovni prostori › Unajmljivanje',
        '2208'       => 'Nekretnine › Poljoprivredna zemljišta',
        '220801'     => 'Nekretnine › Poljoprivredna zemljišta › Ponuda',
        '220802'     => 'Nekretnine › Poljoprivredna zemljišta › Potražnja',
        '220803'     => 'Nekretnine › Poljoprivredna zemljišta › Zamjena',
        '220804'     => 'Nekretnine › Poljoprivredna zemljišta › Iznajmljivanje',
        '220805'     => 'Nekretnine › Poljoprivredna zemljišta › Unajmljivanje',
        '2209'       => 'Nekretnine › Garaže',
        '220901'     => 'Nekretnine › Garaže › Ponuda',
        '220902'     => 'Nekretnine › Garaže › Potražnja',
        '220903'     => 'Nekretnine › Garaže › Zamjena',
        '220904'     => 'Nekretnine › Garaže › Iznajmljivanje',
        '220905'     => 'Nekretnine › Garaže › Unajmljivanje',
        '2210'       => 'Nekretnine › Sobe',
        '221004'     => 'Nekretnine › Sobe › Iznajmljivanje',
        '221005'     => 'Nekretnine › Sobe › Unajmljivanje',
        '2211'       => 'Nekretnine › Grobna mjesta',
        '221101'     => 'Nekretnine › Grobna mjesta › Ponuda',
        '221102'     => 'Nekretnine › Grobna mjesta › Potražnja',
        '23'         => 'Posao',
        '2301'       => 'Posao › Ponuda',
        '230101'     => 'Posao › Ponuda › Ugostiteljstvo i turizam',
        '230102'     => 'Posao › Ponuda › Trgovina i prodaja',
        '230103'     => 'Posao › Ponuda › Administrativna zanimanja',
        '230104'     => 'Posao › Ponuda › Osobne usluge',
        '230105'     => 'Posao › Ponuda › Kućanstvo',
        '230106'     => 'Posao › Ponuda › Čuvanje djece',
        '230107'     => 'Posao › Ponuda › Graditeljstvo i arhitektura',
        '230108'     => 'Posao › Ponuda › Instalacije, popravci i održavanja',
        '230109'     => 'Posao › Ponuda › Promet i transport',
        '230110'     => 'Posao › Ponuda › Elektrotehnika, informatika, strojarstvo',
        '230111'     => 'Posao › Ponuda › Ekonomija, financije i osiguranje',
        '230112'     => 'Posao › Ponuda › Obrazovanje',
        '230113'     => 'Posao › Ponuda › Zdravstvo',
        '230114'     => 'Posao › Ponuda › Ostalo',
        '2302'       => 'Posao › Potražnja',
        '230202'     => 'Posao › Potražnja › Stalni posao',
        '230205'     => 'Posao › Potražnja › Honorarni posao',
        '24'         => 'Mobiteli',
        '2401'       => 'Mobiteli › Ponuda',
        '240101'     => 'Mobiteli › Ponuda › Apple - iPhone',
        '240103'     => 'Mobiteli › Ponuda › Sony-Ericsson',
        '240104'     => 'Mobiteli › Ponuda › LG',
        '240105'     => 'Mobiteli › Ponuda › Motorola',
        '240106'     => 'Mobiteli › Ponuda › Nokia',
        '240110'     => 'Mobiteli › Ponuda › Samsung',
        '240115'     => 'Mobiteli › Ponuda › HTC',
        '240119'     => 'Mobiteli › Ponuda › Blackberry',
        '240121'     => 'Mobiteli › Ponuda › Ostali proizvođači',
        '240150'     => 'Mobiteli › Ponuda › Komponente i oprema',
        '2402'       => 'Mobiteli › Potražnja',
        '2403'       => 'Mobiteli › Kartice i bonovi',
        '240301'     => 'Mobiteli › Kartice i bonovi › Ponuda',
        '25'         => 'Informatika',
        '2501'       => 'Informatika › Ponuda',
        '250101'     => 'Informatika › Ponuda › Računala',
        '250102'     => 'Informatika › Ponuda › Prijenosna računala',
        '250104'     => 'Informatika › Ponuda › Igraće konzole',
        '250105'     => 'Informatika › Ponuda › Pisači',
        '250106'     => 'Informatika › Ponuda › Komponente i ostalo',
        '2502'       => 'Informatika › Potražnja',
        '26'         => 'Tehno kutak',
        '2601'       => 'Tehno kutak › TV',
        '260101'     => 'Tehno kutak › TV › Ponuda',
        '26010101'   => 'Tehno kutak › TV › Ponuda › Prijemnici',
        '26010102'   => 'Tehno kutak › TV › Ponuda › Prijemnici - ostalo',
        '260102'     => 'Tehno kutak › TV › Potražnja',
        '2602'       => 'Tehno kutak › VCR-DVD',
        '260201'     => 'Tehno kutak › VCR-DVD › Ponuda',
        '26020101'   => 'Tehno kutak › VCR-DVD › Ponuda › Uređaji',
        '26020102'   => 'Tehno kutak › VCR-DVD › Ponuda › Ostalo',
        '260202'     => 'Tehno kutak › VCR-DVD › Potražnja',
        '2603'       => 'Tehno kutak › Videokamere',
        '260301'     => 'Tehno kutak › Videokamere › Ponuda',
        '260302'     => 'Tehno kutak › Videokamere › Potražnja',
        '2604'       => 'Tehno kutak › Kućno kino',
        '260401'     => 'Tehno kutak › Kućno kino › Ponuda',
        '260402'     => 'Tehno kutak › Kućno kino › Potražnja',
        '2605'       => 'Tehno kutak › Hi-Fi',
        '260501'     => 'Tehno kutak › Hi-Fi › Ponuda',
        '26050101'   => 'Tehno kutak › Hi-Fi › Ponuda › Uređaji',
        '26050102'   => 'Tehno kutak › Hi-Fi › Ponuda › Komponente i oprema',
        '260502'     => 'Tehno kutak › Hi-Fi › Potražnja',
        '2606'       => 'Tehno kutak › Fotooprema',
        '260601'     => 'Tehno kutak › Fotooprema › Ponuda',
        '260602'     => 'Tehno kutak › Fotooprema › Potražnja',
        '2607'       => 'Tehno kutak › Telefon i fax',
        '260701'     => 'Tehno kutak › Telefon i fax › Ponuda',
        '26070101'   => 'Tehno kutak › Telefon i fax › Ponuda › Telefoni',
        '26070102'   => 'Tehno kutak › Telefon i fax › Ponuda › Ostali uređaji',
        '260702'     => 'Tehno kutak › Telefon i fax › Potražnja',
        '2608'       => 'Tehno kutak › Navigacija (GPS)',
        '260801'     => 'Tehno kutak › Navigacija (GPS) › Ponuda',
        '260802'     => 'Tehno kutak › Navigacija (GPS) › Potražnja',
        '2609'       => 'Tehno kutak › Ostali uređaji',
        '260901'     => 'Tehno kutak › Ostali uređaji › Ponuda',
        '260902'     => 'Tehno kutak › Ostali uređaji › Potražnja',
        '27'         => 'Kućni ljubimci',
        '2701'       => 'Kućni ljubimci › Ponuda',
        '270101'     => 'Kućni ljubimci › Ponuda › Psi',
        '270102'     => 'Kućni ljubimci › Ponuda › Mačke',
        '270103'     => 'Kućni ljubimci › Ponuda › Ptice',
        '270104'     => 'Kućni ljubimci › Ponuda › Ostalo',
        '270105'     => 'Kućni ljubimci › Ponuda › Oprema',
        '270106'     => 'Kućni ljubimci › Ponuda › Parenja',
        '2702'       => 'Kućni ljubimci › Potražnja',
        '2703'       => 'Kućni ljubimci › Poklanjam',
        '28'         => 'Glazbala',
        '2801'       => 'Glazbala › Ponuda',
        '280101'     => 'Glazbala › Ponuda › Instrumenti',
        '280102'     => 'Glazbala › Ponuda › Komponente i ostalo',
        '2802'       => 'Glazbala › Potražnja',
        '2804'       => 'Glazbala › Iznajmljivanje',
        '2805'       => 'Glazbala › Glazbenici',
        '29'         => 'Partnerstvo i usluge',
        '2901'       => 'Partnerstvo i usluge › Usluge',
        '290101'     => 'Partnerstvo i usluge › Usluge › Ponuda',
        '29010104'   => 'Partnerstvo i usluge › Usluge › Ponuda › Prijevoza',
        '29010105'   => 'Partnerstvo i usluge › Usluge › Ponuda › Medicinske',
        '29010106'   => 'Partnerstvo i usluge › Usluge › Ponuda › Kućanske',
        '29010107'   => 'Partnerstvo i usluge › Usluge › Ponuda › Poduke i tečajevi',
        '29010108'   => 'Partnerstvo i usluge › Usluge › Ponuda › Ostale intelektualne usluge',
        '29010109'   => 'Partnerstvo i usluge › Usluge › Ponuda › Knjigovodstvo',
        '29010110'   => 'Partnerstvo i usluge › Usluge › Ponuda › Dizajn i grafičke usluge',
        '29010111'   => 'Partnerstvo i usluge › Usluge › Ponuda › Krojačke',
        '29010112'   => 'Partnerstvo i usluge › Usluge › Ponuda › Čišćenje, održavanje i osiguranje',
        '29010113'   => 'Partnerstvo i usluge › Usluge › Ponuda › Ličilačke',
        '29010114'   => 'Partnerstvo i usluge › Usluge › Ponuda › Bravarske',
        '29010115'   => 'Partnerstvo i usluge › Usluge › Ponuda › Instalacije',
        '29010116'   => 'Partnerstvo i usluge › Usluge › Ponuda › Stolarske',
        '29010117'   => 'Partnerstvo i usluge › Usluge › Ponuda › Tapetarske',
        '29010119'   => 'Partnerstvo i usluge › Usluge › Ponuda › Zidarske',
        '29010120'   => 'Partnerstvo i usluge › Usluge › Ponuda › Završni radovi',
        '29010121'   => 'Partnerstvo i usluge › Usluge › Ponuda › Energetsko certificiranje i građevinska dokumentacija',
        '29010122'   => 'Partnerstvo i usluge › Usluge › Ponuda › Frizerske i kozmetičke',
        '29010123'   => 'Partnerstvo i usluge › Usluge › Ponuda › Svadbe i svečanosti',
        '29010124'   => 'Partnerstvo i usluge › Usluge › Ponuda › Servisi',
        '290102'     => 'Partnerstvo i usluge › Usluge › Potražnja',
        '2902'       => 'Partnerstvo i usluge › Partnerstvo-trgovina',
        '290201'     => 'Partnerstvo i usluge › Partnerstvo-trgovina › Ponuda',
        '30'         => 'Građenje i opremanje',
        '3001'       => 'Građenje i opremanje › Profesionalna oprema',
        '300101'     => 'Građenje i opremanje › Profesionalna oprema › Ponuda',
        '30010101'   => 'Građenje i opremanje › Profesionalna oprema › Ponuda › Ugostiteljska oprema',
        '30010102'   => 'Građenje i opremanje › Profesionalna oprema › Ponuda › Uredska oprema',
        '30010103'   => 'Građenje i opremanje › Profesionalna oprema › Ponuda › Medicinska oprema',
        '30010104'   => 'Građenje i opremanje › Profesionalna oprema › Ponuda › Industrijski i ostali strojevi',
        '30010105'   => 'Građenje i opremanje › Profesionalna oprema › Ponuda › Ručni alati',
        '30010106'   => 'Građenje i opremanje › Profesionalna oprema › Ponuda › Ostala oprema i strojevi',
        '300102'     => 'Građenje i opremanje › Profesionalna oprema › Potražnja',
        '300103'     => 'Građenje i opremanje › Profesionalna oprema › Iznajmljivanje',
        '3002'       => 'Građenje i opremanje › Građevinski materijali',
        '300201'     => 'Građenje i opremanje › Građevinski materijali › Ponuda',
        '300202'     => 'Građenje i opremanje › Građevinski materijali › Potražnja',
        '31'         => 'Slobodno vrijeme',
        '3101'       => 'Slobodno vrijeme › Rekreacija',
        '310101'     => 'Slobodno vrijeme › Rekreacija › Ponuda',
        '31010101'   => 'Slobodno vrijeme › Rekreacija › Ponuda › Sportska oprema',
        '31010102'   => 'Slobodno vrijeme › Rekreacija › Ponuda › Kamping',
        '31010103'   => 'Slobodno vrijeme › Rekreacija › Ponuda › Bicikli',
        '31010105'   => 'Slobodno vrijeme › Rekreacija › Ponuda › Oružje, lov i ribolov',
        '310102'     => 'Slobodno vrijeme › Rekreacija › Potražnja',
        '3107'       => 'Slobodno vrijeme › Literatura',
        '310701'     => 'Slobodno vrijeme › Literatura › Ponuda',
        '310702'     => 'Slobodno vrijeme › Literatura › Potražnja',
        '3109'       => 'Slobodno vrijeme › Kolekcionarstvo',
        '310901'     => 'Slobodno vrijeme › Kolekcionarstvo › Ponuda',
        '310902'     => 'Slobodno vrijeme › Kolekcionarstvo › Potražnja',
        '3110'       => 'Slobodno vrijeme › Hobi',
        '311001'     => 'Slobodno vrijeme › Hobi › Ponuda',
        '311002'     => 'Slobodno vrijeme › Hobi › Potraznja',
        '32'         => 'Dom i vrt',
        '3203'       => 'Dom i vrt › Namještaj',
        '320301'     => 'Dom i vrt › Namještaj › Ponuda',
        '32030101'   => 'Dom i vrt › Namještaj › Ponuda › Sobni',
        '32030102'   => 'Dom i vrt › Namještaj › Ponuda › Kuhinjski',
        '32030103'   => 'Dom i vrt › Namještaj › Ponuda › Kupaonski',
        '32030104'   => 'Dom i vrt › Namještaj › Ponuda › Vrtni',
        '32030105'   => 'Dom i vrt › Namještaj › Ponuda › Ostalo',
        '320302'     => 'Dom i vrt › Namještaj › Potražnja',
        '3204'       => 'Dom i vrt › Pokućstvo',
        '320401'     => 'Dom i vrt › Pokućstvo › Ponuda',
        '320402'     => 'Dom i vrt › Pokućstvo › Potražnja',
        '3205'       => 'Dom i vrt › Antikviteti',
        '320501'     => 'Dom i vrt › Antikviteti › Ponuda',
        '320502'     => 'Dom i vrt › Antikviteti › Potražnja',
        '3206'       => 'Dom i vrt › Umjetnine',
        '320601'     => 'Dom i vrt › Umjetnine › Ponuda',
        '320602'     => 'Dom i vrt › Umjetnine › Potražnja',
        '3207'       => 'Dom i vrt › Kućanski aparati',
        '320701'     => 'Dom i vrt › Kućanski aparati › Ponuda',
        '32070101'   => 'Dom i vrt › Kućanski aparati › Ponuda › Bijela tehnika',
        '32070102'   => 'Dom i vrt › Kućanski aparati › Ponuda › Ostalo',
        '320702'     => 'Dom i vrt › Kućanski aparati › Potražnja',
        '3208'       => 'Dom i vrt › Grijanje i hlađenje',
        '320801'     => 'Dom i vrt › Grijanje i hlađenje › Ponuda',
        '32080102'   => 'Dom i vrt › Grijanje i hlađenje › Ponuda › Ogrjev',
        '32080105'   => 'Dom i vrt › Grijanje i hlađenje › Ponuda › Bojleri, peći, radijatori',
        '320802'     => 'Dom i vrt › Grijanje i hlađenje › Potražnja',
        '3209'       => 'Dom i vrt › Osiguranje doma',
        '320901'     => 'Dom i vrt › Osiguranje doma › Ponuda',
        '320902'     => 'Dom i vrt › Osiguranje doma › Potražnja',
        '3210'       => 'Dom i vrt › Uređenje okućnice',
        '321001'     => 'Dom i vrt › Uređenje okućnice › Ponuda',
        '321002'     => 'Dom i vrt › Uređenje okućnice › Potražnja',
        '3211'       => 'Dom i vrt › Kućno i sobno bilje',
        '321101'     => 'Dom i vrt › Kućno i sobno bilje › Ponuda',
        '321102'     => 'Dom i vrt › Kućno i sobno bilje › Potražnja',
        '33'         => 'Turizam',
        '3301'       => 'Turizam › Ljetovanje/zimovanje',
        '330101'     => 'Turizam › Ljetovanje/zimovanje › Istra i Kvarner',
        '330102'     => 'Turizam › Ljetovanje/zimovanje › Dalmacija',
        '330103'     => 'Turizam › Ljetovanje/zimovanje › Kontinentalna Hrvatska',
        '330104'     => 'Turizam › Ljetovanje/zimovanje › Ostalo',
        '3302'       => 'Turizam › Potražnja',
        '3303'       => 'Turizam › Najam plovila',
        '3304'       => 'Turizam › Vikend turizam',
        '3310'       => 'Turizam › Ostalo',
        '34'         => 'Za vas i vašu obitelj',
        '3401'       => 'Za vas i vašu obitelj › Dječja oprema',
        '340101'     => 'Za vas i vašu obitelj › Dječja oprema › Ponuda',
        '340102'     => 'Za vas i vašu obitelj › Dječja oprema › Potražnja',
        '3402'       => 'Za vas i vašu obitelj › Odjeća i obuća',
        '340201'     => 'Za vas i vašu obitelj › Odjeća i obuća › Ponuda',
        '34020101'   => 'Za vas i vašu obitelj › Odjeća i obuća › Ponuda › Odjeća i obuća',
        '34020102'   => 'Za vas i vašu obitelj › Odjeća i obuća › Ponuda › Modni dodaci i kozmetika',
        '340202'     => 'Za vas i vašu obitelj › Odjeća i obuća › Potražnja',
        '36'         => 'Poljoprivreda',
        '3601'       => 'Poljoprivreda › Ponuda',
        '360101'     => 'Poljoprivreda › Ponuda › Biljke i sadnice',
        '360102'     => 'Poljoprivreda › Ponuda › Domaće životinje i divljač',
        '360103'     => 'Poljoprivreda › Ponuda › Oprema',
        '360104'     => 'Poljoprivreda › Ponuda › Domaći proizvodi',
        '3602'       => 'Poljoprivreda › Potražnja',
        '38'         => 'Školske knjige i pribor',
        '3801'       => 'Školske knjige i pribor › Ponuda',
        '380101'     => 'Školske knjige i pribor › Ponuda › Knjige za 1. - 4. razred osnovne škole',
        '380102'     => 'Školske knjige i pribor › Ponuda › Knjige za 5. - 8. razred osnovne škole',
        '380103'     => 'Školske knjige i pribor › Ponuda › Knjige za gimnazije',
        '380104'     => 'Školske knjige i pribor › Ponuda › Knjige za strukovne škole',
        '380105'     => 'Školske knjige i pribor › Ponuda › Školski pribor',
        '380106'     => 'Školske knjige i pribor › Ponuda › Školska oprema',
        '3802'       => 'Školske knjige i pribor › Potražnja',
        '40'         => 'Razno',
        '4001'       => 'Razno › Alternativa',
        '4002'       => 'Razno › Skrb',
        '4003'       => 'Razno › Na dar',
        '4004'       => 'Razno › Mjenjačnica',
        '4005'       => 'Razno › Obavijesti',
        '4020'       => 'Razno › Ostalo',
        '41'         => 'Brak i veze',
        '4101'       => 'Brak i veze › Muškarci',
        '4102'       => 'Brak i veze › Žene',
        '42'         => 'Osobni kontakti',
        '4201'       => 'Osobni kontakti › Muškarci',
        '4202'       => 'Osobni kontakti › Žene',
        '4203'       => 'Osobni kontakti › Parovi',
        '4204'       => 'Osobni kontakti › Opuštajuće masaže',
        '43'         => 'Financijske usluge',
        '4301'       => 'Financijske usluge › Ponuda'
    );

    protected $import_categories = array(
        // OK
        '21' => array(
            'name'      => 'Auto Moto Nautika',
            'online_id' => 2,
            'items'     => array(
                '2101' => array(
                    'name'         => 'Automobili',
                    'online_id'    => 3,
                    'extract_data' => 'extract_car_data_from_content',
                    'items'        => array(
                        '210101' => array(
                            'name'      => 'Ponuda',
                            'online_id' => 4,
                            'parameter' => array(
                                'type' => 'match_str',
                                'slug' => 'vehicle_make_model'
                            ),
                            'items'          => array(
                                '21010101' => array('name' => 'Alfa Romeo'),
                                '21010102' => array('name' => 'Audi'),
                                '21010103' => array('name' => 'BMW'),
                                '21010104' => array('name' => 'Chrysler'),
                                '21010105' => array('name' => 'Citroen'),
                                '21010106' => array('name' => 'Daewoo'),
                                '21010107' => array('name' => 'Daihatsu'),
                                '21010108' => array('name' => 'Fiat'),
                                '21010109' => array('name' => 'Ford'),
                                '21010111' => array('name' => 'Honda'),
                                '21010112' => array('name' => 'Hyundai'),
                                '21010113' => array('name' => 'Jaguar'),
                                '21010114' => array('name' => 'Kia'),
                                '21010115' => array('name' => 'Lada'),
                                '21010116' => array('name' => 'Lancia'),
                                '21010117' => array('name' => 'Lexus'),
                                '21010118' => array('name' => 'Mazda'),
                                '21010119' => array('name' => 'Mercedes'),
                                '21010120' => array('name' => 'Mitsubishi'),
                                '21010121' => array('name' => 'Nissan'),
                                '21010122' => array('name' => 'Opel'),
                                '21010123' => array('name' => 'Peugeot'),
                                '21010124' => array('name' => 'Porsche'),
                                '21010125' => array('name' => 'Renault'),
                                '21010126' => array('name' => 'Rover'),
                                '21010127' => array('name' => 'Saab'),
                                '21010128' => array('name' => 'Seat'),
                                '21010129' => array('name' => 'Smart'),
                                '21010130' => array('name' => 'Subaru'),
                                '21010131' => array('name' => 'Suzuki'),
                                '21010132' => array('name' => 'Škoda'),
                                '21010133' => array('name' => 'Toyota'),
                                '21010134' => array('name' => 'VW'),
                                '21010135' => array('name' => 'Volvo'),
                                '21010136' => array('name' => 'Zastava'),
                                '21010137' => array('name' => 'Ostalo'),
                                '21010138' => array('name' => 'Chevrolet'),
                                '21010139' => array('name' => 'Dacia'),
                                '21010140' => array('name' => 'Dodge'),
                                '21010141' => array('name' => 'Ferrari'),
                                '21010142' => array('name' => 'Hummer'),
                                '21010143' => array('name' => 'Isuzu'),
                                '21010144' => array('name' => 'Jeep'),
                                '21010145' => array('name' => 'Land Rover'),
                                '21010146' => array('name' => 'Maserati'),
                                '21010147' => array('name' => 'Mini'),
                                '21010148' => array('name' => 'SsangYong')
                            )
                        ),
                        '210102' => array(
                            'name'      => 'Potražnja',
                            'online_id' => 5,
                        ),
                        '210103' => array(
                            'name'      => 'Zamjena',
                            'online_id' => 4,
                            'parameter' => array(
                                'type'  => 'value',
                                'value' => 1,
                                'slug'  => 're_exchange'
                            ),
                        ),
                        '210104' => array(
                            'name'      => 'Iznajmljivanje',
                            'online_id' => 7
                        ),
                        '210105' => array(
                            'name'      => 'Unajmljivanje',
                            'online_id' => 8
                        )
                    )
                ),
                '2102' => array(
                    'name'      => 'Motocikli',
                    'online_id' => 9,
                    'items'     => array(
                        '210201' => array(
                            'name'      => 'Ponuda',
                            'online_id' => 10,
                            'items'     => array(
                                '21020101' => array('name' => 'Motocikli'),
                                '21020103' => array(
                                    'name'      => 'Dijelovi i oprema',
                                    'online_id' => 510
                                )
                            )
                        ),
                        '210202' => array(
                            'name'      => 'Potražnja',
                            'online_id' => 11,
                        ),
                        '210203' => array(
                            'name'      => 'Zamjena',
                            'online_id' => 10,
                            'parameter' => array(
                                'type'  => 'value',
                                'value' => 1,
                                'slug'  => 're_exchange'
                            ),
                        )
                    )
                ),
                '2103' => array(
                    'name'  => 'Autodijelovi',
                    'items' => array(
                        '210301' => array(
                            'name'  => 'Ponuda',
                            'parameter' => array(
                                'type' => 'match_str',
                                'slug' => 'vehicle_make_model'
                            ),
                            'online_id' => 204,
                            'items' => array(
                                '21030101' => array('name' => 'Alfa'),
                                '21030102' => array('name' => 'Audi'),
                                '21030103' => array('name' => 'BMW'),
                                '21030104' => array('name' => 'Chrysler'),
                                '21030105' => array('name' => 'Citroen'),
                                '21030106' => array('name' => 'Daewoo'),
                                '21030107' => array('name' => 'Daihatsu'),
                                '21030108' => array('name' => 'Fiat'),
                                '21030109' => array('name' => 'Ford'),
                                '21030110' => array('name' => 'GM'),
                                '21030111' => array('name' => 'Honda'),
                                '21030112' => array('name' => 'Hyundai'),
                                '21030113' => array('name' => 'Jaguar'),
                                '21030114' => array('name' => 'Kia'),
                                '21030115' => array('name' => 'Lada'),
                                '21030116' => array('name' => 'Lancia'),
                                '21030117' => array('name' => 'Lexus'),
                                '21030118' => array('name' => 'Mazda'),
                                '21030119' => array('name' => 'Mercedes'),
                                '21030120' => array('name' => 'Mitsubishi'),
                                '21030121' => array('name' => 'Nissan'),
                                '21030122' => array('name' => 'Opel'),
                                '21030123' => array('name' => 'Peugeot'),
                                '21030124' => array('name' => 'Porsche'),
                                '21030125' => array('name' => 'Renault'),
                                '21030126' => array('name' => 'Rover'),
                                '21030127' => array('name' => 'Saab'),
                                '21030128' => array('name' => 'Seat'),
                                '21030129' => array('name' => 'Smart'),
                                '21030130' => array('name' => 'Subaru'),
                                '21030131' => array('name' => 'Suzuki'),
                                '21030132' => array('name' => 'Škoda'),
                                '21030133' => array('name' => 'Toyota'),
                                '21030134' => array('name' => 'VW'),
                                '21030135' => array('name' => 'Volvo'),
                                '21030136' => array('name' => 'Zastava'),
                                '21030137' => array('name' => 'Ostalo')
                            )
                        ),
                        '210302' => array(
                            'name'      => 'Potražnja',
                            'online_id' => 205
                        ),
                        '210303' => array(
                            'name'      => 'Zamjena',
                            'online_id' => 204,
                            'parameter' => array(
                                'type'  => 'value',
                                'value' => 1,
                                'slug'  => 're_exchange'
                            ),

                        )
                    )
                ),
                '2104' => array(
                    'name'  => 'Radni i poljoprivredni strojevi',
                    'items' => array(
                        '210401' => array(
                            'name'  => 'Ponuda',
                            'items' => array(
                                '21040101' => array(
                                    'name'      => 'Radni strojevi',
                                    'online_id' => 802
                                ),
                                '21040102' => array(
                                    'name'      => 'Poljoprivredni strojevi',
                                    'online_id' => 793
                                ),
                                '21040103' => array(
                                    'name'      => 'Priključci i ostalo',
                                    'online_id' => 792
                                )
                            )
                        ),
                        '210402' => array(
                            'name'      => 'Potražnja',
                            'online_id' => 796
                        ),
                        '210403' => array(
                            'name'      => 'Zamjena',
                            'online_id' => 794,
                            'parameter' => array(
                                'type'  => 'value',
                                'value' => 1,
                                'slug'  => 're_exchange'
                            )
                        ),
                        '210404' => array(
                            'name'      => 'Iznajmljivanje',
                            'online_id' => 833
                        )
                    )
                ),
                '2105' => array(
                    'name'  => 'Teretna vozila',
                    'items' => array(
                        '210501' => array(
                            'name'  => 'Ponuda',
                            'items' => array(
                                '21050101' => array(
                                    'name'      => 'Kombi i dostavna',
                                    'online_id' => 217
                                ),
                                '21050102' => array(
                                    'name'      => 'Kamioni i tegljači',
                                    'online_id' => 14
                                ),
                                '21050103' => array(
                                    'name'      => 'Autobusi i prikolice',
                                    'online_id' => 16
                                ),
                                '21050104' => array(
                                    'name'      => 'Dijelovi i ostalo ponuda',
                                    'online_id' => 17
                                )
                            )
                        ),
                        '210502' => array(
                            'name'      => 'Potražnja',
                            'online_id' => array(
                                'fallback_id'   => 507,
                                'keyword'       => 'potražnja',
                                'method'        => 'get_online_category_id_by_keyword'
                            )
                        ),
                        '210503' => array(
                            'name'      => 'Zamjena',
                            'online_id' => array(
                                'fallback_id'   => 17,
                                'keyword'       => 'zamjena',
                                'method'        => 'get_online_category_id_by_keyword'
                            ),
                            'parameter' => array(
                                'type'  => 'value',
                                'value' => 1,
                                'slug'  => 're_exchange'
                            )
                        ),
                        '210504' => array(
                            'name'      => 'Iznajmljivanje',
                            'online_id' => 17
                        ),
                        '210505' => array(
                            'name'      => 'Unajmljivanje',
                            'online_id' => 507
                        )
                    )
                ),
                '2107' => array(
                    'name'  => 'Plovila',
                    'items' => array(
                        '210701' => array(
                            'name'  => 'Ponuda',
                            'items' => array(
                                '21070101' => array(
                                    'name'      => 'Plovila',
                                    'online_id' => 862
                                ),
                                '21070102' => array(
                                    'name'      => 'Ostalo',
                                    'online_id' => 862
                                )
                            )
                        ),
                        '210702' => array(
                            'name'      => 'Potražnja',
                            'online_id' => 863
                        ),
                        '210703' => array(
                            'name'      => 'Zamjena',
                            'online_id' => 227,
                            'parameter' => array(
                                'type'  => 'value',
                                'value' => 1,
                                'slug'  => 're_exchange'
                            )
                        )
                    )
                ),
                '2108' => array(
                    'name'  => 'Oldtimeri',
                    'items' => array(
                        '210801' => array(
                            'name' => 'Ponuda',
                            'online_id' => 4,
                            'parameter' => array(
                                'type'  => 'value',
                                'value' => 1,
                                'slug'  => 'oldtimer_vehicle'
                            )
                        ),
                        '210802' => array(
                            'name'      => 'Potražnja',
                            'online_id' => 5
                        ),
                        '210803' => array(
                            'name'       => 'Zamjena',
                            'online_id'  => 4,
                            'parameters' => array(
                                'oldtimer_vehicle' => array(
                                    'type'  => 'value',
                                    'value' => 1
                                ),
                                're_exchange' => array(
                                    'type'  => 'value',
                                    'value' => 1
                                )
                            )
                        ),
                    )
                ),
                '2110' => array(
                    'name'  => 'Autooprema',
                    'items' => array(
                        '211001' => array(
                            'name'  => 'Ponuda',
                            'items' => array(
                                '21100101' => array(
                                    'name'      => 'Tuning & Styling',
                                    'online_id' => 885
                                ),
                                '21100102' => array(
                                    'name'      => 'Ostalo',
                                    'online_id' => 204
                                )
                            )
                        ),
                        '211002' => array(
                            'name'      => 'Potražnja',
                            'online_id' => 205
                        )
                    )
                ),
                '2111' => array(
                    'name'  => 'Gume i felge',
                    'items' => array(
                        '211101' => array(
                            'name'      => 'Ponuda',
                            'online_id' => 518
                        ),
                        '211102' => array(
                            'name'      => 'Potražnja',
                            'online_id' => 519
                        )
                    )
                )
            )
        ),
        // OK
        '22' => array(
            'name'      => 'Nekretnine',
            'online_id' => 24,
            'items'     => array(
                '2201' => array(
                    'name'         => 'Stanovi',
                    'online_id'    => 26,
                    'items'        => array(
                        '220101' => array(
                            'name'      => 'Ponuda',
                            'online_id' => 231,
                            'items'     => array(
                                '22010101' => array(
                                    'name'  => 'Zagreb',
                                    'items' => array(
                                        '2201010101' => array(
                                            'name'      => 'Centar',
                                            'parameter' => array(
                                                'type'  => 'value',
                                                'value' => array(
                                                    'country_id'      => 1,
                                                    'county_id'       => 7442,
                                                    'city_id'         => 7508,
                                                    'municipality_id' => 7510
                                                ),
                                                'slug'  => 'ad_location'
                                            ),
                                        ),
                                        '2201010102' => array(
                                            'name'      => 'Črnomerec',
                                            'parameter' => array(
                                                'type'  => 'value',
                                                'value' => array(
                                                    'country_id'      => 1,
                                                    'county_id'       => 7442,
                                                    'city_id'         => 7445,
                                                    'municipality_id' => 7447
                                                ),
                                                'slug'  => 'ad_location'
                                            ),
                                        ),
                                        '2201010103' => array(
                                            'name'      => 'Dubrava',
                                            'parameter' => array(
                                                'type'  => 'value',
                                                'value' => array(
                                                    'country_id'      => 1,
                                                    'county_id'       => 7442,
                                                    'city_id'         => 7461,
                                                    'municipality_id' => 7464
                                                ),
                                                'slug'  => 'ad_location'
                                            ),
                                        ),
                                        '2201010104' => array(
                                            'name'      =>'Maksimir',
                                            'parameter' => array(
                                                'type'  => 'value',
                                                'value' => array(
                                                    'country_id'      => 1,
                                                    'county_id'       => 7442,
                                                    'city_id'         => 7526,
                                                    'municipality_id' => 7530
                                                ),
                                                'slug'  => 'ad_location'
                                            ),
                                        ),
                                        '2201010105' => array(
                                            'name'      => 'Medveščak',
                                            'parameter' => array(
                                                'type'  => 'value',
                                                'value' => array(
                                                    'country_id'      => 1,
                                                    'county_id'       => 7442,
                                                    'city_id'         => 7508,
                                                    'municipality_id' => 7517
                                                ),
                                                'slug'  => 'ad_location'
                                            ),
                                        ),
                                        '2201010106' => array(
                                            'name'      => 'Novi Zagreb',
                                            'parameter' => array(
                                                'type'  => 'value',
                                                'value' => array(
                                                    'country_id' => 1,
                                                    'county_id'  => 7442,
                                                    'city_id'    => 7534
                                                ),
                                                'slug'  => 'ad_location'
                                            ),
                                        ),
                                        '2201010107' => array(
                                            'name'      => 'Pešćenica',
                                            'parameter' => array(
                                                'type'  => 'value',
                                                'value' => array(
                                                    'country_id'      => 1,
                                                    'county_id'       => 7442,
                                                    'city_id'         => 7561,
                                                    'municipality_id' => 7573
                                                ),
                                                'slug'  => 'ad_location'
                                            ),
                                        ),
                                        '2201010108' => array(
                                            'name'      => 'Sesvete',
                                            'parameter' => array(
                                                'type'  => 'value',
                                                'value' => array(
                                                    'country_id'      => 1,
                                                    'county_id'       => 7442,
                                                    'city_id'         => 7621,
                                                    'municipality_id' => 7649
                                                ),
                                                'slug'  => 'ad_location'
                                            ),
                                        ),
                                        '2201010109' => array(
                                            'name'      => 'Susedgrad',
                                            'parameter' => array(
                                                'type'  => 'value',
                                                'value' => array(
                                                    'country_id'      => 1,
                                                    'county_id'       => 7442,
                                                    'city_id'         => 7600,
                                                    'municipality_id' => 7617
                                                ),
                                                'slug'  => 'ad_location'
                                            ),
                                        ),
                                        '2201010110' => array(
                                            'name'      => 'Trešnjevka',
                                            'parameter' => array(
                                                'type'  => 'value',
                                                'value' => array(
                                                    'country_id' => 1,
                                                    'county_id'  => 7442,
                                                    'city_id'    => 7669
                                                ),
                                                'slug'  => 'ad_location'
                                            ),
                                        ),
                                        '2201010111' => array(
                                            'name'      => 'Trnje',
                                            'parameter' => array(
                                                'type'  => 'value',
                                                'value' => array(
                                                    'country_id'      => 1,
                                                    'county_id'       => 7442,
                                                    'city_id'         => 7683,
                                                    'municipality_id' => 7689
                                                ),
                                                'slug'  => 'ad_location'
                                            ),
                                        )
                                    )
                                ),
                                '22010102' => array('name' => 'Zagrebačka županija'),
                                '22010103' => array('name' => 'Središnja Hrvatska'),
                                '22010104' => array('name' => 'Sjeverna Hrvatska'),
                                '22010105' => array('name' => 'Istočna Hrvatska'),
                                '22010106' => array('name' => 'Istra i Kvarner'),
                                '22010107' => array('name' => 'Dalmacija'),
                                '22010108' => array('name' => 'Ostalo'),
                            )
                        ),

                        '220102' => array(
                            'name'      => 'Potražnja',
                            'online_id' => 232
                        ),
                        '220103' => array(
                            'name'      => 'Zamjena',
                            'online_id' => 231,
                            'parameter' => array(
                                'type'  => 'value',
                                'value' => 1,
                                'slug'  => 're_exchange'
                            ),
                        ),
                        '220104' => array(
                            'name'      => 'Iznajmljivanje stanova',
                            'online_id' => 234,
                            'items'     => array(
                                '22010401' => array(
                                    'name'      => 'Grad Zagreb i Zagrebačka županija',
                                    'parameter' => array(
                                        'type'  => 'value',
                                        'value' => array(
                                            'country_id' => 1,
                                            'county_id'  => 7442
                                        ),
                                        'slug'  => 'ad_location'
                                    ),
                                ),
                                '22010402' => 'Ostale regije',
                                '22010403' => array(
                                    'name'      => 'Cimeri',
                                    'online_id' => array(
                                        'fallback_id'   => 236,
                                        'keyword'       => 'cimeri',
                                        'method'        => 'get_online_category_id_by_keyword'
                                    ),
                                )
                            )
                        ),
                        '220105' => array(
                            'name'      => 'Unajmljivanje',
                            'online_id' => 235
                        )
                    )
                ),
                '2202' => array(
                    'name'         => 'Kuće',
                    'online_id'    => 25,
                    'items'        => array(
                        '220201' => array(
                            'name'      => 'Ponuda',
                            'online_id' => 238,
                            'items'     => array(
                                '22020101' => array(
                                    'name'      => 'Grad Zagreb',
                                    'parameter' => array(
                                        'type'  => 'value',
                                        'value' => array(
                                            'country_id' => 1,
                                            'county_id'  => 7442
                                        ),
                                        'slug'  => 'ad_location'
                                    )
                                ),
                                '22020102' => array(
                                    'name'      => 'Zagrebačka županija',
                                    'parameter' => array(
                                        'type'  => 'value',
                                        'value' => array(
                                            'country_id' => 1,
                                            'county_id'  => 7760
                                        ),
                                        'slug'  => 'ad_location'
                                    )
                                ),
                                '22020103' => array('name' => 'Središnja Hrvatska'),
                                '22020104' => array('name' => 'Sjeverna Hrvatska'),
                                '22020105' => array('name' => 'Istočna Hrvatska'),
                                '22020106' => array('name' => 'Istra i Kvarner'),
                                '22020107' => array('name' => 'Dalmacija'),
                                '22020108' => array('name' => 'Ostalo')
                            )
                        ),
                        '220202' => array(
                            'name'      => 'Potražnja',
                            'online_id' => 239
                        ),
                        '220203' => array(
                            'name'      => 'Zamjena',
                            'online_id' => 238,
                            'parameter' => array(
                                'type'  => 'value',
                                'value' => 1,
                                'slug'  => 're_exchange'
                            ),
                        ),
                        '220204' => array(
                            'name'      => 'Iznajmljivanje',
                            'online_id' => 240
                        ),
                        '220205' => array(
                            'name'      => 'Unajmljivanje',
                            'online_id' => 241
                        )
                    )
                ),
                '2203' => array(
                    'name'         => 'Građevinska zemljišta',
                    'online_id'    => 29,
                    'parameter' => array(
                        'type'  => 'value',
                        'value' => 2170,
                        'slug'  => 're_land_type'
                    ),
                    'items'        => array(
                        '220301' => array(
                            'name'      => 'Ponuda',
                            'online_id' => 246,
                            'items'     => array(
                                '22030101' => array(
                                    'name'      => 'Zagrebačka i Grad Zagreb',
                                    'parameter' => array(
                                        'type'  => 'value',
                                        'value' => array(
                                            'country_id' => 1,
                                            'county_id'  => 7760
                                        ),
                                        'slug'  => 'ad_location'
                                    )
                                ),
                                '22030102' => array('name' => 'Sjeverna Hrvatska'),
                                '22030103' => array('name' => 'Središnja Hrvatska'),
                                '22030104' => array('name' => 'Istočna Hrvatska'),
                                '22030105' => array('name' => 'Istra i Kvarner'),
                                '22030106' => array('name' => 'Dalmacija'),
                                '22030107' => array('name' => 'Ostalo'),
                            )
                        ),
                        '220302' => array(
                            'name'      => 'Potražnja',
                            'online_id' => 247
                        ),
                        '220303' => array(
                            'name'      => 'Zamjena',
                            'online_id' => 246,
                            'parameter' => array(
                                'type'  => 'value',
                                'value' => 1,
                                'slug'  => 're_exchange'
                            ),
                        ),
                        '220304' => array(
                            'name'      => 'Iznajmljivanje',
                            'online_id' => 248
                        ),
                        '220305' => array(
                            'name'      => 'Unajmljivanje',
                            'online_id' => 249
                        )
                    )
                ),
                '2204' => array(
                    'name'         => 'Vikendice',
                    'online_id'    => 30,
                    'parameter' => array(
                        'type'  => 'value',
                        'value' => 2173,
                        'slug'  => 're_vacation_type'
                    ),
                    'items'        => array(
                        '220401' => array(
                            'name'      => 'Ponuda',
                            'online_id' => 250,
                        ),
                        '220402' => array(
                            'name'      => 'Potražnja',
                            'online_id' => 251
                        ),
                        '220403' => array(
                            'name'      => 'Zamjena',
                            'online_id' => 250,
                            'parameter' => array(
                                'type'  => 'value',
                                'value' => 1,
                                'slug'  => 're_exchange'
                            ),
                        )
                    )
                ),
                '2205' => array(
                    'name'         => 'Apartmani',
                    'online_id'    => 30,
                    'parameter' => array(
                        'type'  => 'value',
                        'value' => 2172,
                        'slug'  => 're_vacation_type'
                    ),
                    'items'        => array(
                        '220501' => array(
                            'name'      => 'Ponuda',
                            'online_id' => 250,
                        ),
                        '220502' => array(
                            'name'      => 'Potražnja',
                            'online_id' => 251
                        ),
                        '220503' => array(
                            'name'      => 'Zamjena',
                            'online_id' => 250,
                            'parameter' => array(
                                'type'  => 'value',
                                'value' => 1,
                                'slug'  => 're_exchange'
                            ),
                        )
                    )
                ),
                '2206' => array(
                    'name'         => 'Poslovni prostori',
                    'online_id'    => 28,
                    'items'        => array(
                        '220601' => array(
                            'name'      => 'Ponuda',
                            'online_id' => 242,
                            'items'     => array(
                                '22060101' => array(
                                    'name'       => 'Lokali Zagreb',
                                    'parameters' => array(
                                        're_commercial_object_type' => array(
                                            'type'  => 'value',
                                            'value' => 2156 // ugostiteljski lokal
                                        ),
                                        'ad_location'           => array(
                                            'type'  => 'value',
                                            'value' => array(
                                                'country_id' => 1,
                                                'county_id'  => 7442
                                            )
                                        )
                                    )
                                ),
                                '22060102' => array(
                                    'name'      => 'Lokali ostalo',
                                    'parameter' => array(
                                        'type'  => 'value',
                                        'value' => 2156,    // ugostiteljski lokal
                                        'slug'  => 're_commercial_object_type'
                                    )
                                ),
                                '22060104' => array(
                                    'name'  => 'Skladišta i hale',
                                    'parameter' => array(
                                        'type'  => 'value',
                                        'value' => 2152,    // skladište
                                        'slug'  => 're_commercial_object_type'
                                    )
                                )
                            )
                        ),
                        '220602' => array(
                            'name'      => 'Potražnja',
                            'online_id' => 243
                        ),
                        '220603' => array(
                            'name'      => 'Zamjena',
                            'online_id' => 242,
                            'parameter' => array(
                                'type'  => 'value',
                                'value' => 1,
                                'slug'  => 're_exchange'
                            ),
                        ),
                        '220604' => array(
                            'name'      => 'Iznajmljivanje',
                            'online_id' => 244,
                            'items'     => array(
                                '22060401' => array(
                                    'name'      => 'Zagreb',
                                    'parameter' => array(
                                        'type'  => 'value',
                                        'value' => array(
                                            'country_id' => 1,
                                            'county_id'  => 7442
                                        ),
                                        'slug'  => 'ad_location'
                                    )
                                ),
                                '22060402' => array('name' => 'Ostalo'),
                            )
                        ),
                        '220605' => array(
                            'name'      => 'Unajmljivanje',
                            'online_id' => 245
                        )
                    )
                ),
                '2208' => array(
                    'name'         => 'Poljoprivredna zemljišta',
                    'online_id'    => 29,
                    'parameter' => array(
                        'type'  => 'value',
                        'value' => 2169,
                        'slug'  => 're_land_type'
                    ),
                    'items'        => array(
                        '220801' => array(
                            'name'      => 'Ponuda',
                            'online_id' => 246
                        ),
                        '220802' => array(
                            'name'      => 'Potražnja',
                            'online_id' => 247
                        ),
                        '220803' => array(
                            'name'      => 'Zamjena',
                            'online_id' => 246,
                            'parameter' => array(
                                'type'  => 'value',
                                'value' => 1,
                                'slug'  => 're_exchange'
                            ),
                        ),
                        '220804' => array(
                            'name'      => 'Iznajmljivanje',
                            'online_id' => 248
                        ),
                        '220805' => array(
                            'name'      => 'Unajmljivanje',
                            'online_id' => 249
                        )
                    )
                ),
                '2209' => array(
                    'name'         => 'Garaže',
                    'online_id'    => 31,
                    'items'        => array(
                        '220901' => array(
                            'name'      => 'Ponuda',
                            'online_id' => 254
                        ),
                        '220902' => array(
                            'name'      => 'Potražnja',
                            'online_id' => 255
                        ),
                        '220903' => array(
                            'name'      => 'Zamjena',
                            'online_id' => 254,
                            'parameter' => array(
                                'type'  => 'value',
                                'value' => 1,
                                'slug'  => 're_exchange'
                            ),
                        ),
                        '220904' => array(
                            'name'      => 'Iznajmljivanje',
                            'online_id' => 256
                        ),
                        '220905' => array(
                            'name'      => 'Unajmljivanje',
                            'online_id' => 257
                        )
                    )
                ),
                '2210' => array(
                    'name'         => 'Sobe',
                    'online_id'    => 27,
                    'items'        => array(
                        '221004' => array(
                            'name'      => 'Iznajmljivanje',
                            'online_id' => array(
                                'fallback_id' => 236,
                                'keyword'     => 'cimeri',
                                'method'      => 'get_online_category_id_by_keyword'
                            ),
                        ),
                        '221005' => array(
                            'name'      => 'Unajmljivanje',
                            'online_id' => array(
                                'fallback_id' => 236,
                                'keyword'     => 'cimeri',
                                'method'      => 'get_online_category_id_by_keyword'
                            ),
                        )
                    )
                ),
                '2211' => array(
                    'name'         => 'Grobna mjesta',
                    'online_id'    => 32,
                    'items'        => array(
                        '221101' => array(
                            'name'      => 'Ponuda',
                            'online_id' => 258
                        ),
                        '221102' => array(
                            'name'      => 'Potražnja',
                            'online_id' => 259
                        )
                    )
                ),
            )
        ),
        // OK
        '23' => array(
            'name'  => 'Posao',
            'online_id' => 36,
            'items' => array(
                '2301' => array(
                    'name'      => 'Ponuda',
                    'online_id' => 36,
                    'items'     => array(
                        '230101' => array(
                            'name'      => 'Ugostiteljstvo i turizam',
                            'parameter' => array(
                                'type'  => 'value',
                                'value' => 2497,
                                'slug'  => 'job_category_type'
                            )
                        ),
                        '230102' => array(
                            'name'      => 'Trgovina i prodaja',
                            'parameter' => array(
                                'type'  => 'value',
                                'value' => 2496,
                                'slug'  => 'job_category_type'
                            )
                        ),
                        '230103' => array(
                            'name'      => 'Administrativna zanimanja',
                            'parameter' => array(
                                'type'  => 'value',
                                'value' => 2477,
                                'slug'  => 'job_category_type'
                            )
                        ),
                        '230104' => array(
                            'name'      => 'Osobne usluge',
                            'parameter' => array(
                                'type'  => 'value',
                                'value' => 2501,  // Ostalo?
                                'slug'  => 'job_category_type'
                            )
                        ),
                        '230105' => array(
                            'name'      => 'Kućanstvo',
                            'parameter' => array(
                                'type'  => 'value',
                                'value' => 2501,  // Ostalo?
                                'slug'  => 'job_category_type'
                            )
                        ),
                        '230106' => array(
                            'name'      => 'Čuvanje djece',
                            'parameter' => array(
                                'type'  => 'value',
                                'value' => 2501,  // Ostalo?
                                'slug'  => 'job_category_type'
                            )
                        ),
                        '230107' => array(
                            'name'      => 'Graditeljstvo i arhitektura',
                            'parameter' => array(
                                'type'  => 'value',
                                'value' => 2484,  // Graditeljstvo i geodezija
                                'slug'  => 'job_category_type'
                            )
                        ),
                        '230108' => array(
                            'name'      => 'Instalacije, popravci i održavanja',
                            'parameter' => array(
                                'type'  => 'value',
                                'value' => 2486,
                                'slug'  => 'job_category_type'
                            )
                        ),
                        '230109' => array(
                            'name'      => 'Promet i transport',
                            'parameter' => array(
                                'type'  => 'value',
                                'value' => 2495,
                                'slug'  => 'job_category_type'
                            )
                        ),
                        '230110' => array(
                            'name'      => 'Elektrotehnika, informatika, strojarstvo',
                            'parameter' => array(
                                'type'  => 'value',
                                'value' => 2483,    // informatika je posebna kategorija (2485)
                                'slug'  => 'job_category_type'
                            )
                        ),
                        '230111' => array(
                            'name'      => 'Ekonomija, financije i osiguranje',
                            'parameter' => array(
                                'type'  => 'value',
                                'value' => 2482,
                                'slug'  => 'job_category_type'
                            )
                        ),
                        '230112' => array(
                            'name'      => 'Obrazovanje',
                            'parameter' => array(
                                'type'  => 'value',
                                'value' => 2500,
                                'slug'  => 'job_category_type'
                            )
                        ),
                        '230113' => array(
                            'name'      => 'Zdravstvo',
                            'parameter' => array(
                                'type'  => 'value',
                                'value' => 2499,
                                'slug'  => 'job_category_type'
                            )
                        ),
                        '230114' => array(
                            'name'      => 'Ostalo',
                            'parameter' => array(
                                'type'  => 'value',
                                'value' => 2501,
                                'slug'  => 'job_category_type'
                            )
                        ),
                    )
                ),
                '2302' => array(
                    'name'      => 'Potražnja',
                    'online_id' => 37,
                    'items'     => array(
                        '230202' => array(
                            'name'      => 'Stalni posao',
                            'parameter' => array(
                                'type'  => 'value',
                                'value' => 2503,
                                'slug'  => 'job_employment_type'
                            )
                        ),
                        '230205' => array(
                            'name'      => 'Honorarni posao',
                            'parameter' => array(
                                'type'  => 'value',
                                'value' => 2508,
                                'slug'  => 'job_employment_type'
                            )
                        )
                    )
                )
            )
        ),
        // OK
        '24' => array(
            'name'  => 'Mobiteli',
            'online_id' => 107,
            'items' => array(
                '2401' => array(
                    'name'      => 'Ponuda',
                    'online_id' => 368,
                    'items'     => array(
                        '240101' => array(
                            'name'      => 'Apple - iPhone',
                            'parameter' => array(
                                'type'  => 'value',
                                'value' => array(2189),
                                'slug'  => 'mobile_brand_model'
                            )
                        ),
                        '240103' => array(
                            'name'      => 'Sony-Ericsson',
                            'parameter' => array(
                                'type'  => 'value',
                                'value' => array(2203),
                                'slug'  => 'mobile_brand_model'
                            )
                        ),
                        '240104' => array(
                            'name'      => 'LG',
                            'parameter' => array(
                                'type'  => 'value',
                                'value' => array(2199),
                                'slug'  => 'mobile_brand_model'
                            )
                        ),
                        '240105' => array(
                            'name'      => 'Motorola',
                            'parameter' => array(
                                'type'  => 'value',
                                'value' => array(2200),
                                'slug'  => 'mobile_brand_model'
                            )
                        ),
                        '240106' => array(
                            'name'      => 'Nokia',
                            'parameter' => array(
                                'type'  => 'value',
                                'value' => array(2201),
                                'slug'  => 'mobile_brand_model'
                            )
                        ),
                        '240110' => array(
                            'name'      => 'Samsung',
                            'parameter' => array(
                                'type'  => 'value',
                                'value' => array(2202),
                                'slug'  => 'mobile_brand_model'
                            )
                        ),
                        '240115' => array(
                            'name'      => 'HTC',
                            'parameter' => array(
                                'type'  => 'value',
                                'value' => array(2198),
                                'slug'  => 'mobile_brand_model'
                            )
                        ),
                        '240119' => array(
                            'name'      => 'Blackberry',
                            'parameter' => array(
                                'type'  => 'value',
                                'value' => array(2196),
                                'slug'  => 'mobile_brand_model'
                            )
                        ),
                        '240121' => array(
                            'name'      => 'Ostali proizvođači',
                            'parameter' => array(
                                'type'  => 'value',
                                'value' => array(2204),
                                'slug'  => 'mobile_brand_model'
                            )
                        ),
                        '240150' => array(
                            'name'      => 'Komponente i oprema',
                            'online_id' => 378
                        ),
                    )
                ),
                '2402' => array(
                    'name'      => 'Potražnja',
                    'online_id' => 369
                ),
                '2403' => array(
                    'name'      => 'Kartice i bonovi',
                    'online_id' => 108,
                    'items'     => array(
                        '240301' => array(
                            'name'      => 'Ponuda',
                            'online_id' => 366
                        )
                    )
                )
            )
        ),
        // OK
        '25' => array(
            'name'  => 'Informatika',
            'online_id' => 110,
            'items' => array(
                '2501' => array(
                    'name'      => 'Ponuda',
                    'online_id' => 388,
                    'items'     => array(
                        '250101' => array(
                            'name'      => 'Računala',
                            'online_id' => array(
                                'fallback_id' => 370,
                                'keyword'     => 'informatika_racunala',
                                'method'      => 'get_online_category_id_by_keyword'
                            )
                        ),
                        '250102' => array(
                            'name'      => 'Prijenosna računala',
                            'online_id' => array(
                                'fallback_id' => 372,
                                'keyword'     => 'informatika_laptop',
                                'method'      => 'get_online_category_id_by_keyword'
                            )
                        ),
                        '250104' => array(
                            'name'      => 'Igraće konzole',
                            'online_id' => array(
                                'fallback_id' => 682,
                                'keyword'     => 'informatika_konzole',
                                'method'      => 'get_online_category_id_by_keyword'
                            )
                        ),
                        '250105' => array(
                            'name'      => 'Pisači',
                            'online_id' => array(
                                'fallback_id' => 673,
                                'keyword'     => 'informatika_printer',
                                'method'      => 'get_online_category_id_by_keyword'
                            )
                        ),
                        '250106' => array(
                            'name'      => 'Komponente i ostalo',
                            'online_id' => array(
                                'fallback_id' => 378,
                                'keyword'     => 'informatika_komponente',
                                'method'      => 'get_online_category_id_by_keyword'
                            )
                        ),
                    )
                ),
                '2502' => array(
                    'name'      => 'Potražnja',
                    'online_id' => 389
                )
            )
        ),
        // OK
        '26' => array(
            'name'  => 'Tehno kutak',
            'online_id' => 97,
            'items' => array(
                '2601' => array(
                    'name'      => 'TV',
                    'online_id' => 733,
                    'items'     => array(
                        '260101' => array(
                            'name'      => 'Ponuda',
                            'parameter' => array(
                                'type'  => 'value',
                                'value' => 2746,
                                'slug'  => 'multimedia_tv_type'
                            ),
                            'items'     => array(
                                '26010101' => array(
                                    'name'      => 'Prijemnici',
                                ),
                                '26010102' => array(
                                    'name'      => 'Prijemnici - ostalo',
                                )
                            )
                        ),
                        '260102' => array(
                            'name'      => 'Potražnja',
                            'online_id' => 744
                        )
                    )
                ),
                '2602' => array(
                    'name'      => 'VCR-DVD',
                    'online_id' => 739,
                    'items'     => array(
                        '260201' => array(
                            'name'      => 'Ponuda',
                            'items'     => array(
                                '26020101' => array(
                                    'name'      => 'Uređaji',
                                    'online_id' => 739
                                ),
                                '26020102' => array(
                                    'name'      => 'Ostalo',
                                    'online_id' => 739
                                )
                            )
                        ),
                        '260202' => array(
                            'name'      => 'Potražnja',
                            'online_id' => 744
                        )
                    )
                ),
                '2603' => array(
                    'name'      => 'Videokamere',
                    'online_id' => 730,
                    'items'     => array(
                        '260301' => array(
                            'name'      => 'Ponuda',
                            'parameter' => array(
                                'type'  => 'value',
                                'value' => 2763,
                                'slug'  => 'multimedia_photo_type'
                            ),
                        ),
                        '260302' => array(
                            'name'      => 'Potražnja',
                            'online_id' => 732
                        )
                    )
                ),
                '2604' => array(
                    'name'      => 'Kućno kino',
                    'online_id' => 740,
                    'items'     => array(
                        '260401' => array(
                            'name'      => 'Ponuda',
                            'online_id' => 740,
                            'parameter' => array(
                                'type'  => 'value',
                                'value' => 2748,
                                'slug'  => 'multimedia_tv_type'
                            ),
                        ),
                        '260402' => array(
                            'name'      => 'Potražnja',
                            'online_id' => 744
                        )
                    )
                ),
                '2605' => array(
                    'name'      => 'Hi-Fi',
                    'online_id' => 707,
                    'items'     => array(
                        '260501' => array(
                            'name'      => 'Ponuda',
                            'parameter' => array(
                                'type'  => 'value',
                                'value' => 2731,
                                'slug'  => 'multimedia_audio_type'
                            ),
                            'items'     => array(
                                '26050101' => array(
                                    'name'      => 'Uređaji',
                                ),
                                '26050102' => array(
                                    'name'      => 'Komponente i oprema',
                                )
                            )
                        ),
                        '260502' => array(
                            'name'      => 'Potražnja',
                            'online_id' => 712
                        )
                    )
                ),
                '2606' => array(
                    'name'      => 'Fotooprema',
                    'online_id' => 731,
                    'parameter' => array(
                        'type'  => 'value',
                        'value' => 2761,
                        'slug'  => 'multimedia_photo_type'
                    ),
                    'items'     => array(
                        '260601' => array(
                            'name'      => 'Ponuda',
                        ),
                        '260602' => array(
                            'name'      => 'Potražnja',
                            'online_id' => 732
                        )
                    )
                ),
                '2607' => array(
                    'name'      => 'Telefon i fax',
                    'online_id' => 864,
                    'items'     => array(
                        '260701' => array(
                            'name'      => 'Ponuda',
                            'parameter' => array(
                                'type'  => 'value',
                                'value' => 2774,
                                'slug'  => 'multimedia_phone_type'
                            ),
                            'items'     => array(
                                '26070101' => array(
                                    'name'      => 'Telefoni',
                                ),
                                '26070102' => array(
                                    'name'      => 'Ostali uređaji',
                                )
                            )
                        ),
                        '260502' => array(
                            'name'      => 'Potražnja',
                            'online_id' => 865
                        )
                    )
                ),
                '2608' => array(
                    'name'      => 'Navigacija (GPS)',
                    'online_id' => 745,
                    'items'     => array(
                        '260801' => array(
                            'name'      => 'Ponuda',
                        ),
                        '260802' => array(
                            'name'      => 'Potražnja',
                            'online_id' => 717
                        )
                    )
                ),
                '2609' => array(
                    'name'      => 'Ostali uređaji',
                    'online_id' => 866,
                    'items'     => array(
                        '260901' => array(
                            'name'      => 'Ponuda',
                        ),
                        '260902' => array(
                            'name'      => 'Potražnja',
                            'online_id' => 520
                        )
                    )
                )
            )
        ),
        // OK
        '27' => array(
            'name'  => 'Kućni ljubimci',
            'online_id' => 148,
            'items' => array(
                '2701' => array(
                    'name'      => 'Ponuda',
                    'online_id' => 154,
                    'items'     => array(
                        '270101' => array(
                            'name'      => 'Psi',
                            'online_id' => 149
                        ),
                        '270102' => array(
                            'name'      => 'Mačke',
                            'online_id' => 150
                        ),
                        '270103' => array(
                            'name'      => 'Ptice',
                            'online_id' => 151
                        ),
                        '270104' => array(
                            'name'      => 'Ostalo',
                            'online_id' => 154
                        ),
                        '270105' => array(
                            'name'      => 'Oprema',
                            'online_id' => 609
                        ),
                        '270106' => array(
                            'name'      => 'Parenja',
                            'online_id' => 156
                        )
                    )
                ),
                '2702' => array(
                    'name'      => 'Potražnja',
                    'online_id' => 157
                ),
                '2703' => array(
                    'name'      => 'Poklanjam',
                    'online_id' => 157
                )
            )
        ),
        // OK
        '28' => array(
            'name'  => 'Glazbala',
            'online_id' => 163,
            'items' => array(
                '2801' => array(
                    'name'      => 'Ponuda',
                    'online_id' => 430,
                    'items'     => array(
                        '280101' => array(
                            'name'      => 'Instrumenti',
                            'online_id' => 430
                        ),
                        '280102' => array(
                            'name'      => 'Komponente i ostalo',
                            'online_id' => 638
                        )
                    )
                ),
                '2802' => array(
                    'name'      => 'Potražnja',
                    'online_id' => 425
                ),
                '2804' => array(
                    'name'      => 'Iznajmljivanje',
                    'online_id' => 426
                ),
                '2805' => array(
                    'name'      => 'Glazbenici',
                    'online_id' => 165
                )
            )
        ),
        // OK
        '29' => array(
            'name'  => 'Partnerstvo i usluge',
            'items' => array(
                '2901' => array(
                    'name'      => 'Usluge',
                    'online_id' => 312,
                    'items'     => array(
                        '290101' => array(
                            'name'      => 'Ponuda',
                            'items'     => array(
                                '29010104' => array(
                                    'name'      => 'Prijevoza',
                                    'online_id' => 302
                                ),
                                '29010105' => array(
                                    'name'      => 'Medicinske',
                                    'online_id' => 292
                                ),
                                '29010106' => array(
                                    'name'      => 'Kućanske',
                                    'online_id' => 300
                                ),
                                '29010107' => array(
                                    'name'      => 'Poduke i tečajevi',
                                    'online_id' => 298
                                ),
                                '29010108' => array(
                                    'name'      => 'Ostale intelektualne usluge',
                                    'online_id' => 280
                                ),
                                '29010109' => array(
                                    'name'      => 'Knjigovodstvo',
                                    'online_id' => 284
                                ),
                                '29010110' => array(
                                    'name'      => 'Dizajn i grafičke usluge',
                                    'online_id' => 280  // intelektualne usluge
                                ),
                                '29010111' => array(
                                    'name'      => 'Krojačke',
                                    'online_id' => 286
                                ),
                                '29010112' => array(
                                    'name'      => 'Čišćenje, održavanje i osiguranje',
                                    'online_id' => 268
                                ),
                                '29010113' => array(
                                    'name'      => 'Ličilačke',
                                    'online_id' => 306   // soboslikarske
                                ),
                                '29010114' => array(
                                    'name'      => 'Bravarske',
                                    'online_id' => 266
                                ),
                                '29010115' => array(
                                    'name'      => 'Instalacije',
                                    'online_id' => 278
                                ),
                                '29010116' => array(
                                    'name'      => 'Stolarske',
                                    'online_id' => 308
                                ),
                                '29010117' => array(
                                    'name'      => 'Tapetarske',
                                    'online_id' => 312  // ostale usluge
                                ),
                                '29010119' => array(
                                    'name'      => 'Zidarske',
                                    'online_id' => 274  // građevinske
                                ),
                                '29010120' => array(
                                    'name'      => 'Završni radovi',
                                    'online_id' => 274  // građevinske
                                ),
                                '29010121' => array(
                                    'name'      => 'Energetsko certificiranje i građevinska dokumentacija',
                                    'online_id' => 276
                                ),
                                '29010122' => array(
                                    'name'      => 'Frizerske i kozmetičke',
                                    'online_id' => 272
                                ),
                                '29010123' => array(
                                    'name'      => 'Svadbe i svečanosti',
                                    'online_id' => 310
                                ),
                                '29010124' => array(
                                    'name'      => 'Servisi',
                                    'online_id' => 294  // Usluge/Servisi aparata i uređaja/Ponuda
                                ),
                            )
                        ),
                        '290102' => array(
                            'name'      => 'Potražnja',
                            'online_id' => 313   // usluge (općenito)
                        )
                    )
                ),
                '2902' => array(
                    'name'      => 'Partnerstvo-trgovina',
                    'items'     => array(
                        '290201' => array(
                            'name'      => 'Ponuda',
                            'online_id' => 38
                        )
                    )
                )
            )
        ),
        // OK
        '30' => array(
            'name'  => 'Građenje i opremanje',
            'items' => array(
                '3001' => array(
                    'name'      => 'Profesionalna oprema',
                    'online_id' => 90,
                    'items'     => array(
                        '300101' => array(
                            'name'      => 'Ponuda',
                            'items'     => array(
                                '30010101' => array(
                                    'name'      => 'Ugostiteljska oprema',
                                    'online_id' => 316
                                ),
                                '30010102' => array(
                                    'name'      => 'Uredska oprema',
                                    'online_id' => 319
                                ),
                                '30010103' => array(
                                    'name'      => 'Medicinska oprema',
                                    'online_id' => 322
                                ),
                                '30010104' => array(
                                    'name'      => 'Industrijski i ostali strojevi',
                                    'online_id' => 824
                                ),
                                '30010105' => array(
                                    'name'      => 'Ručni alati',
                                    'online_id' => 793
                                ),
                                '30010106' => array(
                                    'name'      => 'Ostala oprema i strojevi',
                                    'online_id' => 834
                                )
                            )
                        ),
                        '300102' => array(
                            'name'      => 'Potražnja',
                            'online_id' => 831   // Ostala oprema i strojevi (Potražnja)
                        ),
                        '300103' => array(
                            'name'      => 'Iznajmljivanje',
                            'online_id' => 833   // Ostala oprema i strojevi (Iznajmljivanje)
                        )
                    )
                ),
                '3002' => array(
                    'name'      => 'Građevinski materijali',
                    'online_id' => 412,
                    'items'     => array(
                        '300201' => array(
                            'name'      => 'Ponuda',
                            'online_id' => 412
                        ),
                        '300202' => array(
                            'name'      => 'Potražnja',
                            'online_id' => 413
                        )
                    )
                )
            )
        ),

        '31' => array(
            'name'  => 'Slobodno vrijeme',
            'items' => array(
                '3101' => array(
                    'name'      => 'Rekreacija',
                    'items'     => array(
                        '310101' => array(
                            'name'      => 'Ponuda',
                            'items'     => array(
                                '31010101' => array(
                                    'name'      => 'Sportska oprema',
                                    'online_id' => 473
                                ),
                                '31010102' => array(
                                    'name'      => 'Kamping',
                                    'online_id' => 465
                                ),
                                '31010103' => array(
                                    'name'      => 'Bicikli',
                                    'online_id' => 447
                                ),
                                '31010105' => array(
                                    'name'      => 'Oružje, lov i ribolov',
                                    'online_id' => 467
                                )
                            )
                        ),
                        '310102' => array(
                            'name'      => 'Potražnja',
                            'online_id' => 474
                        )
                    )
                ),
                '3107' => array(
                    'name'      => 'Literatura',
                    'items'     => array(
                        '310701' => array(
                            'name'      => 'Ponuda',
                            'online_id' => 625
                        ),
                        '310702' => array(
                            'name'      => 'Potražnja',
                            'online_id' => 442
                        )
                    )
                ),
                '3109' => array(
                    'name'      => 'Kolekcionarstvo',
                    'online_id' => 691,
                    'items'     => array(
                        '310901' => array(
                            'name'      => 'Ponuda',
                            'online_id' => 691
                        ),
                        '310902' => array(
                            'name'      => 'Potražnja',
                            'online_id' => 693
                        )
                    )
                ),
                '3110' => array(
                    'name'      => 'Hobi',
                    'items'     => array(
                        '311001' => array(
                            'name'      => 'Ponuda',
                            'online_id' => 198          // I još... -> Ostalo
                        ),
                        '311002' => array(
                            'name'      => 'Potražnja',
                            'online_id' => 198          // I još... -> Ostalo
                        )
                    )
                )
            )
        ),
        // OK
        '32' => array(
            'name'  => 'Dom i vrt',
            'items' => array(
                '3203' => array(
                    'name'      => 'Namještaj',
                    'online_id' => 414,
                    'items'     => array(
                        '320301' => array(
                            'name'      => 'Ponuda',
                            'items'     => array(
                                '32030101' => array(
                                    'name'      => 'Sobni',
                                    'online_id' => 394,
                                    'parameter' => array(
                                        'type'  => 'value',
                                        'value' => 2369,
                                        'slug'  => 'gio_living_room_furniture_type'
                                    )
                                ),
                                '32030102' => array(
                                    'name'      => 'Kuhinjski',
                                    'online_id' => 398,
                                    'parameter' => array(
                                        'type'  => 'value',
                                        'value' => 2376,
                                        'slug'  => 'gio_kitchen_type'
                                    )
                                ),
                                '32030103' => array(
                                    'name'      => 'Kupaonski',
                                    'online_id' => 400,
                                    'parameter' => array(
                                        'type'  => 'value',
                                        'value' => 2384,
                                        'slug'  => 'gio_bathroom_type'
                                    )
                                ),
                                '32030104' => array(
                                    'name'      => 'Vrtni',
                                    'online_id' => 408,
                                    'parameter' => array(
                                        'type'  => 'value',
                                        'value' => 2407,
                                        'slug'  => 'gio_garden_funiture_type'
                                    )
                                ),
                                '32030105' => array(
                                    'name'      => 'Ostalo',
                                    'online_id' => 414
                                )
                            )
                        ),
                        '300102' => array(
                            'name'      => 'Potražnja',
                            'online_id' => 415
                        )
                    )
                ),
                '3204' => array(
                    'name'      => 'Pokućstvo',
                    'online_id' => 398,
                    'items'     => array(
                        '320401' => array(
                            'name'      => 'Ponuda',
                            'online_id' => 398,
                            'parameter' => array(
                                'type'  => 'value',
                                'value' => 2378,
                                'slug'  => 'gio_kitchen_type'
                            )

                        ),
                        '320402' => array(
                            'name'      => 'Potražnja',
                            'online_id' => 399
                        )
                    )
                ),
                '3205' => array(
                    'name'      => 'Antikviteti',
                    'online_id' => 689,
                    'items'     => array(
                        '320501' => array(
                            'name'      => 'Ponuda',
                            'online_id' => 689
                        ),
                        '320502' => array(
                            'name'      => 'Potražnja',
                            'online_id' => 693
                        )
                    )
                ),
                '3206' => array(
                    'name'      => 'Umjetnine',
                    'online_id' => 690,
                    'items'     => array(
                        '320601' => array(
                            'name'      => 'Ponuda',
                            'online_id' => 619
                        ),
                        '320602' => array(
                            'name'      => 'Potražnja',
                            'online_id' => 693
                        )
                    )
                ),
                '3207' => array(
                    'name'      => 'Kućanski aparati',
                    'online_id' => 392,
                    'items'     => array(
                        '320701' => array(
                            'name'      => 'Ponuda',
                            'online_id' => 392,
                            'items'     => array(
                                '32070101' => array(
                                    'name' => 'Bijela tehnika',
                                    'parameter' => array(
                                        'type'  => 'value',
                                        'value' => array(2346),
                                        'slug'  => 'gio_home_appliance_type'
                                    )
                                ),
                                '32070102' => array(
                                    'name' => 'Ostalo',
                                    'parameter' => array(
                                        'type'  => 'value',
                                        'value' => array(2361),
                                        'slug'  => 'gio_home_appliance_type'
                                    )
                                )
                            )
                        ),
                        '320702' => array(
                            'name'      => 'Potražnja',
                            'online_id' => 393
                        )
                    )
                ),
                '3208' => array(
                    'name'      => 'Grijanje i hlađenje',
                    'online_id' => 390,
                    'items'     => array(
                        '320801' => array(
                            'name'      => 'Ponuda',
                            'online_id' => 390,
                            'items'     => array(
                                '32080102' => array('name' => 'Ogrjev'),
                                '32080105' => array('name' => 'Bojleri, peći, radijatori')
                            )
                        ),
                        '320802' => array(
                            'name'      => 'Potražnja',
                            'online_id' => 391
                        )
                    )
                ),
                '3209' => array(
                    'name'      => 'Osiguranje doma',
                    'online_id' => 410,
                    'items'     => array(
                        '320901' => array(
                            'name'      => 'Ponuda',
                            'online_id' => 410
                        ),
                        '320902' => array(
                            'name'      => 'Potražnja',
                            'online_id' => 411
                        )
                    )
                ),
                '3210' => array(
                    'name'      => 'Uređenje okućnice',
                    'online_id' => 408,
                    'items'     => array(
                        '321001' => array(
                            'name'      => 'Ponuda',
                            'online_id' => 408
                        ),
                        '321002' => array(
                            'name'      => 'Potražnja',
                            'online_id' => 409
                        )
                    )
                ),
                '3211' => array(
                    'name'      => 'Ostalo',
                    'online_id' => 414,
                    'items'     => array(
                        '321101' => array(
                            'name'      => 'Ponuda',
                            'online_id' => 414
                        ),
                        '321102' => array(
                            'name'      => 'Potražnja',
                            'online_id' => 415
                        )
                    )
                ),
            )
        ),
        // OK
        '33' => array(
            'name'  => 'Turizam',
            'items' => array(
                '3301' => array(
                    'name'      => 'Ljetovanje/zimovanje',
                    'online_id' => 503,
                    'parameter' => array(
                        'type'  => 'value',
                        'value' => 2722,
                        'slug'  => 'tourism_type'
                    ),
                    'items'     => array(
                        '330101' => array('name' => 'Istra i Kvarner'),
                        '330102' => array('name' => 'Dalmacija'),
                        '330103' => array('name' => 'Kontinentalna Hrvatska'),
                        '330104' => array('name' => 'Ostalo'),
                    )
                ),
                '3302' => array(
                    'name'      => 'Potražnja',
                    'online_id' => 504
                ),
                '3303' => array(
                    'name'      => 'Najam plovila',
                    'online_id' => 227
                ),
                '3304' => array(
                    'name'      => 'Vikend turizam',
                    'online_id' => 503,
                    'parameter' => array(
                        'type'  => 'value',
                        'value' => 2721,
                        'slug'  => 'tourism_type'
                    )
                ),
                '3310' => array(
                    'name'      => 'Ostalo',
                    'online_id' => 503
                )
            )
        ),
        // OK
        '34' => array(
            'name'  => 'Za vas i vašu obitelj',
            'items' => array(
                '3401' => array(
                    'name'      => 'Dječja oprema',
                    'items'     => array(
                        '340101' => array(
                            'name'      => 'Ponuda',
                            'online_id' => 557,
                            'parameter' => array(
                                'type'  => 'value',
                                'value' => 2678,
                                'slug'  => 'kids_equipment_type'
                            )
                        ),
                        '340102' => array(
                            'name'      => 'Potražnja',
                            'online_id' => 488
                        )
                    )
                ),
                '3402' => array(
                    'name'      => 'Odjeća i obuća',
                    'items'     => array(
                        '340201' => array(
                            'name'      => 'Ponuda',
                            'items'     => array(
                                '34020101' => array(
                                    'name'      => 'Odjeća i obuća',
                                    'online_id' => array(
                                        'fallback_id' => 433,
                                        'keyword'     => 'ponuda_odjeca_obuca',
                                        'method'      => 'get_online_category_id_by_keyword'
                                    )
                                ),
                                '34020102' => array(
                                    'name'      => 'Modni dodaci i kozmetika',
                                    'online_id' => array(
                                        'fallback_id' => 687,
                                        'keyword'     => 'ponuda_modni_dodaci_kozmetika',
                                        'method'      => 'get_online_category_id_by_keyword'
                                    )
                                )
                            )
                        ),
                        '340202' => array(
                            'name'      => 'Potražnja',
                            'online_id' => array(
                                'fallback_id' => 434,
                                'keyword'     => 'potraznja_moda_kozmetika',
                                'method'      => 'get_online_category_id_by_keyword'
                            )
                        )
                    )
                )
            )
        ),
        // OK
        '36' => array(
            'name'  => 'Poljoprivreda',
            'items' => array(
                '3601' => array(
                    'name'      => 'Ponuda',
                    'items'     => array(
                        '360101' => array(
                            'name'      => 'Biljke i sadnice',
                            'online_id' => 495
                        ),
                        '360102' => array(
                            'name'      => 'Domaće životinje i divljač',
                            'online_id' => 535
                        ),
                        '360103' => array(
                            'name'      => 'Oprema',
                            'online_id' => 501
                        ),
                        '360104' => array(
                            'name'      => 'Domaći proizvodi',
                            'online_id' => 542
                        )
                    )
                ),
                '3602' => array(
                    'name'      => 'Potražnja',
                    'online_id' => 502, // Oprema i ostalo -> Potražnja
                )
            )
        ),
        // OK
        '38' => array(
            'name'  => 'Školske knjige i pribor',
            'items' => array(
                '3801' => array(
                    'name'      => 'Ponuda',
                    'online_id' => 441,
                    'items'     => array(
                        '380101' => array(
                            'name'      => 'Knjige za 1. - 4. razred osnovne škole',
                            'parameter' => array(
                                'type'  => 'value',
                                'value' => 2593,
                                'slug'  => 'literature_type'
                            )
                        ),
                        '380102' => array(
                            'name'      => 'Knjige za 5. - 8. razred osnovne škole',
                            'parameter' => array(
                                'type'  => 'value',
                                'value' => 2594,
                                'slug'  => 'literature_type'
                            )
                        ),
                        '380103' => array(
                            'name'      => 'Knjige za gimnazije',
                            'parameter' => array(
                                'type'  => 'value',
                                'value' => 2595,
                                'slug'  => 'literature_type'
                            )
                        ),
                        '380104' => array(
                            'name'      => 'Knjige za strukovne škole',
                            'parameter' => array(
                                'type'  => 'value',
                                'value' => 2596,
                                'slug'  => 'literature_type'
                            )
                        ),
                        '380105' => array(
                            'name'      => 'Školski pribor',    // Dječji kutak > Školski pribor i oprema
                            'online_id' => 491,
                            'parameter' => array(
                                'type'  => 'value',
                                'value' => 2783, // Pribor
                                'slug'  => 'kids_school_equipment_type'
                            )
                        ),
                        '380106' => array(
                            'name'      => 'Školska oprema',    // Dječji kutak > Školski pribor i oprema
                            'online_id' => 491,
                            'parameter' => array(
                                'type'  => 'value',
                                'value' => 2784, // Oprema
                                'slug'  => 'kids_school_equipment_type'
                            )
                        )
                    )
                ),
                '3802' => array(
                    'name'      => 'Potražnja',
                    'online_id' => 442,
                )
            )
        ),
        // OK
        '40' => array(
            'name'  => 'Razno',
            'items' => array(
                '4001' => array(
                    'name'      => 'Alternativa',
                    'online_id' => array(
                        'fallback_id' => 445,
                        'keyword'     => 'alternativa',
                        'method'      => 'get_online_category_id_by_keyword'
                    )
                ),
                '4002' => array(
                    'name'      => 'Skrb',
                    'online_id' => array(
                        'fallback_id' => 314,
                        'keyword'     => 'skrb',
                        'method'      => 'get_online_category_id_by_keyword'
                    )
                ),
                '4003' => array(
                    'name'      => 'Na dar',
                    'online_id' => array(
                        'fallback_id' => 443,
                        'keyword'     => 'na_dar',
                        'method'      => 'get_online_category_id_by_keyword'
                    )
                ),
                '4004' => array(
                    'name'      => 'Mjenjačnica',
                    'online_id' => 195
                ),
                '4005' => array(
                    'name'      => 'Obavijesti',
                    'online_id' => 196
                ),
                '4020' => array(
                    'name'      => 'Ostalo',
                    'online_id' => 198
                ),
            )
        ),
        // OK
        '41' => array(
            'name'  => 'Brak i veze',
            'items' => array(
                '4101' => array(
                    'name'      => 'Muškarci',
                    'online_id' => array(
                        'fallback_id' => 479,
                        'keyword'     => 'brak_veze_muskarci',
                        'method'      => 'get_online_category_id_by_keyword'
                    )
                ),
                '4102' => array(
                    'name'      => 'Žene',
                    'online_id' => array(
                        'fallback_id' => 477,
                        'keyword'     => 'brak_veze_zene',
                        'method'      => 'get_online_category_id_by_keyword'
                    )
                )
            )
        ),
        // OK
        '42' => array(
            'name'      => 'Osobni kontakti',
            'online_id' => 192,
            'items'     => array(
                '4201' => array(
                    'name'      => 'Muškarci',
                    'parameter' => array(
                        'type'  => 'value',
                        'value' => array(2680,2682),
                        'slug'  => 'escort_types'
                    )
                ),
                '4202' => array(
                    'name'      => 'Žene',
                    'parameter' => array(
                        'type'  => 'value',
                        'value' => array(2680,2681),
                        'slug'  => 'escort_types'
                    )
                ),
                '4203' => array(
                    'name'      => 'Parovi',
                    'parameter' => array(
                        'type'  => 'value',
                        'value' => array(2680,2683),
                        'slug'  => 'escort_types'
                    )
                ),
                '4204' => array(
                    'name'      => 'Opuštajuće masaže',
                    'parameter' => array(
                        'type'  => 'value',
                        'value' => array(2684),
                        'slug'  => 'escort_types'
                    )
                )
            )
        ),
        // OK
        '43' => array(
            'name'      => 'Financijske usluge',
            'online_id' => 199,
            'items' => array(
                '4301' => array('name' => 'Ponuda')
            )
        )
    );

    private function extract_car_data_from_content($content)
    {
        $car_data = array();

        preg_match_all("/(?'car_engine_power'\d+)+kW|(?'car_production_year'\d+)+\.g\.|reg\.(?'car_registrated_untill'\d+\/\d+)\.|(?'car_mileage'\d+)tkm|(?'car_engine_capacity'\d+)ccm|(?'car_garaged'garažiran)+|(?'car_service_book'serv\.knj\.)+|(?'car_used'rabljeno)|(?'car_price_eur'\d+)\s*(€|E|[Ee]ur[a]*)|(?'car_price_kn'\d+)\s*k[u]*n[a]*|(?'car_aircondition'klima)|(?'car_myst_lights'maglenke)|(?'car_airbag'zračni)|((?'car_door_number'\d)\s*vrata)|(?'car_el_windows'el.pod.)/", $content, $matches);

        if ($matches) {
            if (isset($matches['car_engine_power'])) {
                foreach ($matches['car_engine_power'] as $value) {
                    if (intval($value)) {
                        $car_data['vehicle_engine_power'] = intval($value);
                        break;
                    }
                }
            }
            if (isset($matches['car_production_year'])) {
                foreach ($matches['car_production_year'] as $value) {
                    if (intval($value)) {
                        if (intval($value) >= 0 && intval($value) < 20) {
                            $car_data['vehicle_production_year'] = intval("20$value");
                        } elseif (intval($value) >= 1900) {
                            $car_data['vehicle_production_year'] = intval($value);
                        } else {
                            $car_data['vehicle_production_year'] = intval("19$value");
                        }
                        break;
                    }
                }
            }
            if (isset($matches['car_registrated_untill'])) {
                foreach ($matches['car_registrated_untill'] as $value) {
                    if (trim($value)) {
                        $exploded_data = explode('/', trim($value));
                        if (count($exploded_data) == 2) {
                            $car_data['vehicle_registrated_untill'] = '20' . trim($exploded_data[1]) . '-' . trim($exploded_data[0]);
                        }
                        break;
                    }
                }
            }
            if (isset($matches['car_mileage'])) {
                foreach ($matches['car_mileage'] as $value) {
                    if (intval($value)) {
                        $car_data['vehicle_mileage'] = intval($value) * 1000;
                        break;
                    }
                }
            }
            if (isset($matches['car_engine_capacity'])) {
                foreach ($matches['car_engine_capacity'] as $value) {
                    if (intval($value)) {
                        $car_data['vehicle_engine_capacity'] = intval($value);
                        break;
                    }
                }
            }
            if (isset($matches['car_garaged'])) {
                foreach ($matches['car_garaged'] as $value) {
                    if (trim($value)) {
                        $car_data['garaged_vehicle'] = 1;
                        break;
                    }
                }
            }
            if (isset($matches['car_service_book'])) {
                foreach ($matches['car_service_book'] as $value) {
                    if (trim($value)) {
                        $car_data['service_booklet'] = 1;
                        break;
                    }
                }
            }
            if (isset($matches['car_used'])) {
                foreach ($matches['car_used'] as $value) {
                    if (trim($value)) {
                        $car_data['new_used'] = 1415;
                        break;
                    }
                }
            }
            if (isset($matches['car_price_eur'])) {
                foreach ($matches['car_price_eur']  as $value) {
                    if (intval($value)) {
                        $car_data['car_price'] = array(
                            'currency_id' => 2,
                            'value' => intval($value)
                        );
                        break;
                    }
                }
            }
            if (isset($matches['car_price_kn'])) {
                foreach ($matches['car_price_kn'] as $value) {
                    if (intval($value)) {
                        $car_data['car_price'] = array(
                            'currency_id' => 1,
                            'value' => intval($value)
                        );
                        break;
                    }
                }
            }
            if (isset($matches['car_price_kn'])) {
                foreach ($matches['car_price_kn'] as $value) {
                    if (intval($value)) {
                        $car_data['car_price'] = array(
                            'currency_id' => 1,
                            'value' => intval($value)
                        );
                        break;
                    }
                }
            }
            if (isset($matches['car_aircondition'])) {
                foreach ($matches['car_aircondition'] as $value) {
                    if (trim($value)) {
                        $car_data['car_aircondition'] = 1453;
                        break;
                    }
                }
            }
            $car_extra_equipment = array();
            if (isset($matches['car_myst_lights'])) {
                foreach ($matches['car_myst_lights'] as $value) {
                    if (trim($value)) {
                        $car_extra_equipment[] = 1374;
                        break;
                    }
                }
            }
            if (isset($matches['car_airbag'])) {
                foreach ($matches['car_airbag'] as $value) {
                    if (trim($value)) {
                        $car_data['car_airbag'] = array(1356, 1357);
                        break;
                    }
                }
            }
            if (isset($matches['car_door_number'])) {
                foreach ($matches['car_door_number'] as $value) {
                    if (intval($value)) {
                        $ids = array(
                            '2' => 1139,
                            '3' => 1340,
                            '4' => 1341,
                            '5' => 1342,
                            '6' => 1343
                        );
                        if (isset($ids[trim($value)]) && !empty($ids[trim($value)])) {
                            $car_data['car_door_number'] = (int)$ids[trim($value)];
                        }
                        break;
                    }
                }
            }
            $car_comfort_equipment = array();
            if (isset($matches['car_el_windows'])) {
                foreach ($matches['car_el_windows'] as $value) {
                    if (trim($value)) {
                        $car_comfort_equipment[] = 1398;    // el. podizanje prednjih stakala
                        $car_comfort_equipment[] = 1399;    // el. podizanje stražnjih stakala
                        break;
                    }
                }
            }

            if (count($car_extra_equipment)) {
                $car_data['vehicle_extra_equipment'] = $car_extra_equipment;
            }
            if (count($car_comfort_equipment)) {
                $car_data['vehicle_comfort_equipment'] = $car_comfort_equipment;
            }
        }

        return $car_data;
    }

    private function get_online_category_id_by_keyword($content, $transaction_type, $fallback_id = null)
    {
        $category_id = $fallback_id;

        $content = trim($content);
        $content = !empty($content) ? mb_strtolower($content, 'UTF-8') : null;
        if ($content) {
            if ('cimeri' == $transaction_type) {
                // we don't know exactly what transaction type we're talking
                // about so we have to search for all possible types
                if (preg_match('/(?<!\pL)(traž[eim]+)|(prima[mju]*)(?!\pL)/', $content)) {
                    $category_id = 237;
                }
            } elseif ('informatika_racunala' == $transaction_type) {
                if (preg_match('/(?<!\pL)(traž[eim]+)|(kupujem)(?!\pL)/', $content)) {
                    $category_id = 371;
                }
            } elseif ('informatika_laptop' == $transaction_type) {
                if (preg_match('/(?<!\pL)(traž[eim]+)|(kupujem)(?!\pL)/', $content)) {
                    $category_id = 373;
                }
            } elseif ('informatika_konzole' == $transaction_type) {
                if (preg_match('/(?<!\pL)(traž[eim]+)|(kupujem)(?!\pL)/', $content)) {
                    $category_id = 385;
                }
            } elseif ('informatika_printer' == $transaction_type) {
                if (preg_match('/(?<!\pL)(traž[eim]+)|(kupujem)(?!\pL)/', $content)) {
                    $category_id = 383;
                }
            } elseif ('informatika_komponente' == $transaction_type) {
                if (preg_match('/(?<!\pL)(traž[eim]+)|(kupujem)(?!\pL)/', $content)) {
                    $category_id = 379;
                }
            } elseif ('na_dar' == $transaction_type) {
                // we don't know exactly what transaction type we're talking
                // about so we have to search for all possible types
                if (preg_match('/(?<!\pL)(traž[eim]+)(?!\pL)/', $content)) {
                    $category_id = 444;
                }
            } elseif ('alternativa' == $transaction_type) {
                // we don't know exactly what transaction type we're talking
                // about so we have to search for all possible types
                if (preg_match('/(?<!\pL)(traž[eim]+)(?!\pL)/', $content)) {
                    $category_id = 446;
                }
            } elseif ('skrb' == $transaction_type) {
                // we don't know exactly what transaction type we're talking
                // about so we have to search for all possible types
                if (preg_match('/(?<!\pL)(traž[eim]+)(?!\pL)/', $content)) {
                    $category_id = 315;
                }
            } elseif ('brak_veze_muskarci' == $transaction_type) {
                // we don't know exactly what transaction type we're talking
                // about so we have to search for all possible types
                if (preg_match('/(?<!\pL)(traž[eim]+)|(upozn[atio]+)(?!\pL)/', $content)) {
                    $category_id = 480;
                }
            } elseif ('brak_veze_zene' == $transaction_type) {
                // we don't know exactly what transaction type we're talking
                // about so we have to search for all possible types
                if (preg_match('/(?<!\pL)(traž[eim]+)|(upozn[atil]+)(?!\pL)/', $content)) {
                    $category_id = 478;
                }
            } elseif ('mobiteli_potraznja' == $transaction_type) {
                $category_id = 369;
                /*
                if (preg_match('/(?<!\pL)(apple|iphone)(?!\pL)/', $content)) {
                    $category_id = 344;
                } elseif (preg_match('/(?<!\pL)(blackberry)(?!\pL)/', $content)) {
                    $category_id = 346;
                } elseif (preg_match('/(?<!\pL)(huawei)(?!\pL)/', $content)) {
                    $category_id = 348;
                } elseif (preg_match('/(?<!\pL)(htc)(?!\pL)/', $content)) {
                    $category_id = 350;
                } elseif (preg_match('/(?<!\pL)(lg)(?!\pL)/', $content)) {
                    $category_id = 352;
                } elseif (preg_match('/(?<!\pL)(motorola)(?!\pL)/', $content)) {
                    $category_id = 354;
                } elseif (preg_match('/(?<!\pL)(nokia)(?!\pL)/', $content)) {
                    $category_id = 356;
                } elseif (preg_match('/(?<!\pL)(samsung)(?!\pL)/', $content)) {
                    $category_id = 358;
                } elseif (preg_match('/(?<!\pL)(sony|ericsson)(?!\pL)/', $content)) {
                    $category_id = 360;
                }
                */
            } elseif ('ponuda_odjeca_obuca' == $transaction_type) {
                if (preg_match('/(?<!\pL)(tenisice)|(cipel[ice]+)|(opanke)|(čizm[ice]+)|(cizm[ice]+)(?!\pL)/', $content)) {
                    $category_id = 435;
                } else {
                    $category_id = 433;
                }
            } elseif ('ponuda_modni_dodaci_kozmetika' == $transaction_type) {
                if (preg_match('/(?<!\pL)(krema)|(kreme)|(losion[ie]*)(?!\pL)/', $content)) {
                    $category_id = 439;
                } else {
                    $category_id = 437;
                }
            } elseif ('potraznja_moda_kozmetika' == $transaction_type) {
                if (preg_match('/(?<!\pL)(tenisice)|(cipel[ice]+)|(opanke)|(čizm[ice]+)|(cizm[ice]+)(?!\pL)/', $content)) {
                    $category_id = 436;
                } elseif (preg_match('/(?<!\pL)(krema)|(kreme)|(losion[ie]*)(?!\pL)/', $content)) {
                    $category_id = 440;
                } elseif (preg_match('/(?<!\pL)(torb[ae]+)|(ruksak)(?!\pL)/', $content)) {
                    $category_id = 438;
                } else {
                    $category_id = 434;
                }
            } else {
                if (preg_match('/(?<!\pL)(kamion[e]*)|(tegljač[e]*)(?!\pL)/', $content)) {
                    if ($transaction_type == 'potražnja') {
                        $category_id = 507;
                    } elseif ($transaction_type == 'zamjena') {
                        $category_id = 14;
                    }
                } elseif (preg_match('/\b(kombi|dostavn)\b/', $content)) {
                    if ($transaction_type == 'potražnja') {
                        $category_id = 507;
                    } elseif ($transaction_type == 'zamjena') {
                        $category_id = 217;
                    }
                } elseif (preg_match('/\bautobus\b/', $content)) {
                    if ($transaction_type == 'potražnja') {
                        $category_id = 507;
                    } elseif ($transaction_type == 'zamjena') {
                        $category_id = 16;
                    }
                }
            }
        }

        return $category_id;
    }

}
