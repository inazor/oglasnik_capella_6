<?php

namespace Baseapp\Library\PDF417;

interface RendererInterface
{
    public function render(BarcodeData $data);
}
