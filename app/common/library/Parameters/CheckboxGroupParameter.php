<?php

namespace Baseapp\Library\Parameters;

use Baseapp\Models\Dictionaries as Dictionary;

class CheckboxGroupParameter extends BaseParameter
{
    protected $group_type = 'custom';
    protected $settings_location = 'data';
    protected $type = 'CHECKBOXGROUP';
    protected $accept_dictionary = true;
    protected $can_default = false;
    protected $can_default_type = '';
    protected $can_other = false;
    protected $can_prefix_suffix = false;
    protected $is_dependable = false;
    protected $is_required = false;
    public $is_searchable = false;
    public $is_searchable_type = array('CHECKBOX', 'RADIO', 'DROPDOWN');

    public function setValue($values_to_set)
    {
        if (is_array($values_to_set)) {
            $values = array();
            $json_values = array();
            foreach ($values_to_set as $value) {
                $values[] = $value;

                $json_value = array(
                    'value' => (int) $value
                );
                $dictionary = Dictionary::findFirst($value);
                if ($dictionary) {
                    $json_value['text_value'] = $dictionary->name;
                }
                $json_values[] = $json_value;
            }

            if (count($values)) {
                $this->setPreparedData(array(
                    'id' => (int) $this->parameter->id,
                    'values' => $values
                ));

                $this->setPreparedDataJson(
                    'ad_params_' . $this->parameter->id,
                    $json_values
                );
            }
        }
    }

    public function process()
    {
        $this->validate_cannot_be_empty();
        if (isset($this->data['ad_params_' . $this->parameter->id])) {
            $this->setValue($this->data['ad_params_' . $this->parameter->id]);
        }
    }

    public function render_input()
    {
        parent::render_input();

        $render_data = array(
            'row'  => false,
            'html' => '',
            'js'   => ''
        );

        if (!$this->is_hidden) {
            $parameter_id = 'parameter_' . $this->parameter->id;
            $parameter_name = 'ad_params_' . $this->parameter->id;
            $required_html = ($this->parameter_settings->is_required ? ' <abbr title="' . $this->getValidationString('required_field') . '">*</abbr>' : '');

            $div_class = $this->render_as_full_row ? 'col-lg-12' : 'col-lg-3 col-md-3';
            $render_data['html'] .= '<div class="' . $div_class . (isset($this->errors) && $this->errors->filter($parameter_name) ? ' has-error' : '') . '">' . PHP_EOL;
            $render_data['html'] .= '    <label class="control-label">' . trim($this->parameter_settings->label_text) . $required_html . '</label>' . PHP_EOL;

            // for loop begin
            $dictionary_level_values = $this->parameter->getDictionaryValues($this->parameter->dictionary_id);
            if ($dictionary_level_values) {
                $values_count = count($dictionary_level_values);
                $first_checkbox_margin = ' style="margin-top:0"';
                $prefix_space = '';
                if ($values_count > 4) {
                    $render_data['html'] .= '    <div id="' . $parameter_id . '" class="moreless_items">' . PHP_EOL;
                    $prefix_space = '    ';
                }
                foreach ($dictionary_level_values as $value) {
                    $checkbox_selected = (isset($this->data) && isset($this->data[$parameter_name]) ? in_array($value->id, $this->data[$parameter_name]) : false);
                    $render_data['html'] .= $prefix_space . '    <div class="checkbox checkbox-primary"' . $first_checkbox_margin . '>' . PHP_EOL;
                    $render_data['html'] .= $prefix_space . '        <input type="checkbox" name="' . $parameter_name . '[]" id="' . $parameter_name . '_' . $value->id . '" value="' . $value->id . '"' . ($checkbox_selected ? ' checked="checked"' : '') . ' />' . PHP_EOL;
                    $render_data['html'] .= $prefix_space . '        <label class="control-label" for="' . $parameter_name . '_' . $value->id . '">' . trim($value->name) . '</label>' . PHP_EOL;
                    $render_data['html'] .= $prefix_space . '    </div>' . PHP_EOL;
                    $first_checkbox_margin = '';
                }
                if ($values_count > 4) {
                    $render_data['html'] .= '    </div>' . PHP_EOL;
                }
            }

            if (isset($this->errors) && $this->errors->filter($parameter_name)) {
                $render_data['html'] .= '      <p class="help-block">' . current($this->errors->filter($parameter_name))->getMessage() . '</p>' . PHP_EOL;
            } elseif (isset($this->parameter_settings->help_text) && trim($this->parameter_settings->help_text)) {
                $render_data['html'] .= '      <p class="help-block">' . trim($this->parameter_settings->help_text) . '</p>' . PHP_EOL;
            }

            $render_data['html'] .= '</div>' . PHP_EOL;
        }

        return $render_data;
    }

}
