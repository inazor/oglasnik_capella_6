<?php

namespace Baseapp\Library\Parameters;

use Baseapp\Library\Utils;
use Baseapp\Models\Currency;
use Phalcon\Di;

class CurrencyParameter extends BaseParameter
{
    protected $group_type = 'standard';
    protected $settings_location = 'data';
    protected $type = 'CURRENCY';
    protected $accept_dictionary = false;
    protected $can_default = false;
    protected $can_default_type = '';
    protected $can_other = false;
    protected $can_prefix_suffix = true;
    protected $is_dependable = false;
    protected $is_required = false;
    public $is_searchable = true;
    public $is_searchable_type = array('INTERVAL_TEXT');

    public function setValue($price)
    {
        if (is_array($price) && isset($price['value']) && trim($price['value']) && isset($price['currency_id']) && intval($price['currency_id'])) {
            $this->ad->assignPrice($price['value']);
            $this->ad->currency_id = (int)$price['currency_id'];
            $currency_short_name = '';
            if ($this->ad->currency_id) {
                $ad_currency = Currency::findFirst($this->ad->currency_id);
                if ($ad_currency) {
                    $currency_short_name = !empty($ad_currency->short_name) ? ' ' . trim($ad_currency->short_name) : '';
                }
            }

            $this->setPreparedDataJson(
                'ad_params_' . $this->parameter->id,
                array(
                    'type'        => 'price',
                    'value'       => $price['value'],
                    'text_value'  => number_format(intval($price['value']), 0, '', '.') . $currency_short_name,
                    'currency_id' => (int) $price['currency_id']
                )
            );
        }
    }

    public function process()
    {
        if ($this->validation && $this->parameter_settings->is_required) {
            $this->validation->add('ad_price', new \Baseapp\Extension\Validator\Currency(array(
                'messageDigit' => '\'' . $this->parameter_settings->label_text . '\' ' . $this->getValidationString('should_contain_only_numbers'),
                'message' => '\'' . $this->parameter_settings->label_text . '\' ' . $this->getValidationString('is_not_valid')
            )));
        }

        if (isset($this->data['ad_price'])) {
            $price_data = array(
                'value'       => $this->data['ad_price'],
                'currency_id' => (int)$this->data['ad_price_currency_id']
            );
            $this->setValue($price_data);
        }
    }

    public function render_input()
    {
        parent::render_input();

        $render_data = array(
            'row'  => false,
            'html' => '',
            'js'   => ''
        );

        if (!$this->is_hidden) {
            $currencies = Currency::all();

            $parameter_id = 'parameter_price';
            $parameter_input_id = $parameter_id . '_input';
            $parameter_name = 'ad_price';
            $required_html = ($this->parameter_settings->is_required ? ' <abbr title="' . $this->getValidationString('required_field') . '">*</abbr>' : '');

            $parameter_value = '';
            if (isset($this->data) && isset($this->data[$parameter_name])) {
                $parameter_value = trim($this->data[$parameter_name]);
            }
            $parameter_value = strip_tags($parameter_value);

            $parameter_placeholder = ($this->parameter_settings->placeholder_text ? ' placeholder="' . $this->parameter_settings->placeholder_text . '"' : '');

            if (isset($this->parameter_settings->currency_id) && intval($this->parameter_settings->currency_id)) {
                $parameter_currency = $currencies[$this->parameter_settings->currency_id];
            } else {
                $default_currency_id = Di::getDefault()->getShared('config')->payment->default_currency_id;
                $parameter_currency = $currencies[$default_currency_id];
            }

            $div_class = $this->render_as_full_row ? 'col-lg-12' : 'col-lg-3 col-md-3';
            $render_data['html'] .= '<div class="' . $div_class . '">' . PHP_EOL;
            $render_data['html'] .= '    <div class="form-group' . (isset($this->errors) && $this->errors->filter($parameter_name) ? ' has-error' : '') . '">' . PHP_EOL;
            $render_data['html'] .= '        <label class="control-label" for="' . $parameter_id . '">' . trim($this->parameter_settings->label_text) . $required_html . '</label>' . PHP_EOL;

            $render_data['html'] .= '        <div class="input-group">' . PHP_EOL;
            $render_data['html'] .= '            <input type="text" class="form-control text-right" id="' . $parameter_input_id . '" value="' . $parameter_value . '"' . $parameter_placeholder . ' />' . PHP_EOL;
            $render_data['html'] .= '            <input type="hidden" name="' . $parameter_name . '" id="' . $parameter_id . '" value="' . $parameter_value . '" />' . PHP_EOL;
            $render_data['html'] .= '            <input type="hidden" name="' . $parameter_name . '_currency_id" value="' . $parameter_currency['id'] . '" />' . PHP_EOL;
            $render_data['html'] .= '            <span class="input-group-addon">' . trim($parameter_currency['short_name']) . '</span>' . PHP_EOL;
            $render_data['html'] .= '        </div>' . PHP_EOL;

            if (isset($this->errors) && $this->errors->filter($parameter_name)) {
                $render_data['html'] .= '      <p class="help-block">' . current($this->errors->filter($parameter_name))->getMessage() . '</p>' . PHP_EOL;
            } elseif (isset($this->parameter_settings->help_text) && trim($this->parameter_settings->help_text)) {
                $render_data['html'] .= '      <p class="help-block">' . trim($this->parameter_settings->help_text) . '</p>' . PHP_EOL;
            }

            $render_data['html'] .= '    </div>' . PHP_EOL;
            $render_data['html'] .= '</div>' . PHP_EOL;

            $render_data['js'] = <<<JS

    \$('#parameter_price_input').autoNumeric({aSep:'.', aDec:',', mDec:'0', lZero:'deny', vMax: '99999999999999.99'});
    \$('#parameter_price_input').change(function(){
        \$('#$parameter_id').val(\$('#parameter_price_input').autoNumeric('get'));
    });
    \$('#$parameter_id').val(\$('#parameter_price_input').autoNumeric('get'));
JS;
        }

        return $render_data;
    }

}
