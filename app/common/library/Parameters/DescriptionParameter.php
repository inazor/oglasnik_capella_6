<?php

namespace Baseapp\Library\Parameters;

use Baseapp\Library\Utils;

class DescriptionParameter extends BaseParameter
{
    protected $group_type = 'standard';
    protected $settings_location = 'data';
    protected $type = 'DESCRIPTION';
    protected $accept_dictionary = false;
    protected $can_default = false;
    protected $can_default_type = '';
    protected $can_other = false;
    protected $can_prefix_suffix = false;
    protected $is_dependable = false;
    protected $is_required = true;
    public $is_searchable = false;
    public $is_searchable_type = array('TEXT');
    protected $max_offline_length = 256;

    public function setValue($description_value, $description_offline_value = '')
    {
        $description_value = strip_tags(Utils::str_normalize_punctuation($description_value), '<br><br/>');

        if (trim($description_value)) {
            $this->setPreparedDataJson(
                'ad_params_' . $this->parameter->id,
                array(
                    'type' => 'description',
                    'text_value' => (string) $description_value
                )
            );

            // set the property of an Ad object
            $this->ad->description = (string) $description_value;
        }

        if (trim($description_offline_value)) {
            $this->setPreparedDataJson(
                'ad_params_' . $this->parameter->id . '_offline',
                array(
                    'type' => 'description_offline',
                    'text_value' => (string) strip_tags($description_offline_value)
                )
            );

            // set the property of an Ad object
            $this->ad->description_offline = (string) strip_tags($description_offline_value);
        }
    }

    public function process()
    {
        $this->getParameterSettings();
        if ($this->validation && $this->parameter_settings->is_required) {
            $this->validation->add('ad_description', new \Phalcon\Validation\Validator\PresenceOf(array(
                'message' => '\'' . $this->parameter_settings->label_text . '\' ' . $this->getValidationString('cannot_be_empty')
            )));
        }

        $description_value = !empty($this->data['ad_description']) ? trim(strip_tags($this->data['ad_description'], '<br><br/>')) : '';
        $description_offline_value = !empty($this->data['ad_description_offline']) ? trim(strip_tags($this->data['ad_description_offline'])) : '';

        $this->setValue($description_value, $description_offline_value);
    }


    public function render_input()
    {
        parent::render_input();

        $render_data = array(
            'row'  => true,
            'html' => '',
            'js'   => ''
        );

        if (!$this->is_hidden) {
            $textarea_rows = 3;
            if ('backend' === $this->module || 'suva' === $this->module ) {
                $textarea_rows = 10;
            }

            $parameter_id = 'parameter_description';
            $parameter_name = 'ad_description';
            $required_html = ($this->parameter_settings->is_required ? ' <abbr title="' . $this->getValidationString('required_field') . '">*</abbr>' : '');

            if (isset($this->data) && isset($this->data[$parameter_name])) {
                $parameter_default_value = trim($this->data[$parameter_name]);
            } else {
                $parameter_default_value = ($this->parameter_settings->default_value ? $this->parameter_settings->default_value : '');
            }
            $parameter_default_value = strip_tags($parameter_default_value);

            $parameter_placeholder = ($this->parameter_settings->placeholder_text ? ' placeholder="' . $this->parameter_settings->placeholder_text . '"' : '');

            $render_data['html'] .= '<div id="' . $parameter_name . '_box" class="' . ('backend' === $this->module ? 'col-lg-6 col-md-6' : 'col-lg-12') . '">' . PHP_EOL;
            $render_data['html'] .= '    <div class="form-group' . (isset($this->errors) && $this->errors->filter($parameter_name) ? ' has-error' : '') . '">' . PHP_EOL;
            $render_data['html'] .= '        <label class="control-label" for="' . $parameter_id . '">' . trim($this->parameter_settings->label_text) . $required_html . '</label>' . PHP_EOL;
            $render_data['html'] .= '        <textarea class="form-control" name="' . $parameter_name . '" id="' . $parameter_id . '" rows="' . $textarea_rows . '" cols="40"' . $parameter_placeholder . '>' . $parameter_default_value . '</textarea>' . PHP_EOL;

            if (isset($this->errors) && $this->errors->filter($parameter_name)) {
                $render_data['html'] .= '      <p class="help-block">' . current($this->errors->filter($parameter_name))->getMessage() . '</p>' . PHP_EOL;
            } elseif (isset($this->parameter_settings->help_text) && trim($this->parameter_settings->help_text)) {
                $render_data['html'] .= '      <p class="help-block">' . trim($this->parameter_settings->help_text) . '</p>' . PHP_EOL;
            }

            $render_data['html'] .= '    </div>' . PHP_EOL;
            $render_data['html'] .= '</div>' . PHP_EOL;

            /**
             * In backend module, for description field we need to create another 'description_offline' field where ad's
             * description for offline usage will be saved/generated
             */
            if ('backend' === $this->module || 'suva' === $this->module ) {
                $parameter_default_value_offline = $parameter_default_value;
                if (isset($this->data[$parameter_name . '_offline']) && !empty($this->data[$parameter_name . '_offline'])) {
                    $parameter_default_value_offline = trim($this->data[$parameter_name . '_offline']);
                }
                $parameter_default_value_offline = Utils::str_truncate($parameter_default_value_offline, $this->max_offline_length);
                $parameter_id_offline = $parameter_id . '_offline';
                $parameter_id_offline_avus_id = $parameter_id_offline . '_avus_id';
                $maxlength_label_id = $parameter_id_offline . '_maxlength_label';

                $render_data['html'] .= '<div id="' . $parameter_name . '_offline_box" class="col-lg-6 col-md-6">' . PHP_EOL;
                $render_data['html'] .= '    <div class="form-group' . (isset($this->errors) && $this->errors->filter($parameter_name . '_offline') ? ' has-error' : '') . '">' . PHP_EOL;
                $render_data['html'] .= '        <label class="control-label" for="' . $parameter_id . '_offline">' . trim($this->parameter_settings->label_text) . ' za tisak <small class="text-muted" id="' . $parameter_id_offline_avus_id . '"></small></label>' . PHP_EOL;
                $render_data['html'] .= '        <textarea class="form-control" name="' . $parameter_name . '_offline" id="' . $parameter_id_offline . '" rows="' . $textarea_rows . '" cols="40"' . $parameter_placeholder . ' maxlength="' . $this->max_offline_length . '">' . $parameter_default_value_offline . '</textarea>' . PHP_EOL;
                $error_msg = '';
                if (isset($this->errors) && $this->errors->filter($parameter_name . '_offline')) {
                    $error_msg = current($this->errors->filter($parameter_name . '_offline'))->getMessage();
                }
                $refresh_button_id = 'refresh_' . $parameter_name . '_offline_btn';
                $render_data['html'] .= '        <p class="help-block"><span id="' . $maxlength_label_id . '" class="pull-left"></span><span id="' . $refresh_button_id . '" class="btn btn-primary btn-xs pull-right" title="Refresh"><span class="fa fa-refresh"></span></span>' . $error_msg . '</p>' . PHP_EOL;
                $render_data['html'] .= '    </div>' . PHP_EOL;
                $render_data['html'] .= '</div>' . PHP_EOL;

                $render_data['js'] = <<<JS

    \$('#$parameter_id_offline').maxlength({
        counterContainer: \$('#$maxlength_label_id')
    });
    \$('#$refresh_button_id').click(function(){
        \$.post(
            '/admin/ads/refreshOfflineDescription',
            \$('#$parameter_id_offline').closest('form').serializeArray(),
            function(json) {
                if (json.status) {
                    \$('#$parameter_id_offline_avus_id').html(json.data.avus);
                    \$('#$parameter_id_offline').val(json.data.content).trigger('change');
                } else if ('undefined' !== typeof json.msg) {
                    alert(json.msg);
                }
            },
            'json'
        );
    });
JS;
            }
        }

        return $render_data;
    }

}
