<?php

namespace Baseapp\Library\Parameters;

class NumberParameter extends BaseParameter
{
    protected $group_type = 'custom';
    protected $settings_location = 'data';
    protected $type = 'NUMBER';
    protected $accept_dictionary = false;
    protected $can_default = true;
    protected $can_default_type = 'text';
    protected $can_other = false;
    protected $can_prefix_suffix = true;
    protected $is_dependable = false;
    protected $is_required = false;
    public $is_searchable = false;
    public $is_searchable_type = array('TEXT', 'INTERVAL_TEXT', 'INTERVAL_SLIDER');

    public function process()
    {
        $this->validate_cannot_be_empty();
        $this->validate_greater_than_zero();
        if (isset($this->data['ad_params_' . $this->parameter->id])) {
            $parameter_post_value = trim(strip_tags($this->data['ad_params_' . $this->parameter->id]));

            if ($parameter_post_value) {
                $this->validation->add('ad_params_' . $this->parameter->id, new \Phalcon\Validation\Validator\Regex(array(
                    'pattern' => '/^[0-9,.]+$/',
                    'message' => '\'' . $this->parameter_settings->label_text . '\' ' . $this->getValidationString('should_be_a_number')
                )));

                if (is_numeric($parameter_post_value)) {
                    /**
                     * If we have both ',' and '.', it means we have a nice formated string representing a float value
                     * so we need to format this number to a float look-a-like string...
                     */
                    if (strpos($parameter_post_value, '.') !== false && strpos($parameter_post_value, ',') !== false) {
                        $parameter_post_value = str_replace(',', '.', str_replace('.', '', $parameter_post_value));
                    }

                    $float_value = floatval($parameter_post_value);
                    $int_value = intval($parameter_post_value);

                    if ($float_value && $int_value != $float_value) {
                        $parameter_post_value = $float_value;
                    } else {
                        $parameter_post_value = $int_value;
                    }

                    $this->setValue($parameter_post_value);
                }
            }
        }
    }

    public function render_input()
    {
        parent::render_input();

        $render_data = array(
            'row'  => false,
            'html' => '',
            'js'   => ''
        );

        if (!$this->is_hidden) {
            $parameter_id = 'parameter_' . $this->parameter->id;
            $parameter_input_id = $parameter_id . '_input';
            $parameter_name = 'ad_params_' . $this->parameter->id;
            $required_html = ($this->parameter_settings->is_required ? ' <abbr title="' . $this->getValidationString('required_field') . '">*</abbr>' : '');
            $decimal_places = 0;
            $min_allowed_value = 0;
            $max_allowed_value = 0;
            if (isset($this->parameter_settings->number_decimals) && intval($this->parameter_settings->number_decimals)) {
                $decimal_places = intval($this->parameter_settings->number_decimals);
            }
            if (isset($this->parameter_settings->number_min_value) && intval($this->parameter_settings->number_min_value)) {
                $min_allowed_value = intval($this->parameter_settings->number_min_value);
            }
            if (isset($this->parameter_settings->number_max_value) && intval($this->parameter_settings->number_max_value)) {
                $max_allowed_value = intval($this->parameter_settings->number_max_value);
            }

            if (isset($this->data) && isset($this->data[$parameter_name])) {
                $parameter_default_value = trim(strip_tags($this->data[$parameter_name]));
            } else {
                $parameter_default_value = trim(strip_tags($this->parameter_settings->default_value ? $this->parameter_settings->default_value : ''));
            }
            $parameter_default_value = is_numeric($parameter_default_value) ? floatval($parameter_default_value) : '';

            // One more check to make sure $parameter_default_value is not '0' (and set it to empty string if so)
            if ($parameter_default_value == 0) {
                $parameter_default_value = '';
            }

            // Check that the value is within min/max allowed when they're both specified
            if ($max_allowed_value > 0 && $min_allowed_value > 0 && $parameter_default_value) {
                if ($parameter_default_value >= $min_allowed_value && $parameter_default_value <= $max_allowed_value) {
                    $parameter_default_value = number_format(floatval($parameter_default_value), $decimal_places, '.', '');
                } else {
                    $parameter_default_value = '';
                }
            }

            $parameter_placeholder = ($this->parameter_settings->placeholder_text ? ' placeholder="' . $this->parameter_settings->placeholder_text . '"' : '');

            $div_class = $this->render_as_full_row ? 'col-lg-12' : 'col-lg-3 col-md-3';
            $render_data['html'] .= '<div class="' . $div_class . '">' . PHP_EOL;
            $render_data['html'] .= '    <div class="form-group' . (isset($this->errors) && $this->errors->filter($parameter_name) ? ' has-error' : '') . '">' . PHP_EOL;
            $render_data['html'] .= '        <label class="control-label" for="' . $parameter_id . '">' . trim($this->parameter_settings->label_text) . $required_html . '</label>' . PHP_EOL;
            $render_data['html'] .= '        <input type="hidden" id="' . $parameter_id . '" name="' . $parameter_name . '" value="' . $parameter_default_value . '" />' . PHP_EOL;

            if (trim($this->parameter_settings->prefix) || trim($this->parameter_settings->suffix)) {
                $render_data['html'] .= '        <div class="input-group">' . PHP_EOL;
                if (trim($this->parameter_settings->prefix)) {
                    $render_data['html'] .= '            <span class="input-group-addon">' . trim($this->parameter_settings->prefix) . '</span>' . PHP_EOL;
                }
                $render_data['html'] .= '            <input type="text" class="form-control text-right" id="' . $parameter_input_id . '" value="' . $parameter_default_value . '"' . $parameter_placeholder . ' />' . PHP_EOL;
                if (trim($this->parameter_settings->suffix)) {
                    $render_data['html'] .= '            <span class="input-group-addon">' . trim($this->parameter_settings->suffix) . '</span>' . PHP_EOL;
                }
                $render_data['html'] .= '        </div>' . PHP_EOL;
            } else {
                $render_data['html'] .= '        <input type="text" class="form-control text-right" id="' . $parameter_input_id . '" value="' . $parameter_default_value . '"' . $parameter_placeholder . ' />' . PHP_EOL;
            }

            if (isset($this->errors) && $this->errors->filter($parameter_name)) {
                $render_data['html'] .= '      <p class="help-block">' . current($this->errors->filter($parameter_name))->getMessage() . '</p>' . PHP_EOL;
            } elseif (isset($this->parameter_settings->help_text) && trim($this->parameter_settings->help_text)) {
                $render_data['html'] .= '      <p class="help-block">' . trim($this->parameter_settings->help_text) . '</p>' . PHP_EOL;
            }

            $render_data['html'] .= '    </div>' . PHP_EOL;
            $render_data['html'] .= '</div>' . PHP_EOL;

            // jquery.autoNumeric doesn't expect thousands separator in vMin/vMax
            $vMin = ($min_allowed_value ? ', vMin:\'' . number_format($min_allowed_value, $decimal_places, '.', '') . '\'' : '');
            $vMax = ($max_allowed_value ? ', vMax:\'' . number_format($max_allowed_value, $decimal_places, '.', '') . '\'' : '');
            $vMinMax = $vMin . $vMax;

            $render_data['js'] = <<<JS

    \$('#$parameter_input_id').autoNumeric({aSep:'.', aDec:',', mDec:'$decimal_places', lZero:'deny'$vMinMax});
    \$('#$parameter_input_id').change(function(){
        var currentValue = \$('#$parameter_input_id').autoNumeric('get');
        \$('#$parameter_id').val((currentValue > 0 ? currentValue : ''));
        if (currentValue == 0) {
            \$('#$parameter_input_id').val('');
        }
    });
    \$('#$parameter_input_id').trigger('change');
JS;
        }
        return $render_data;
    }

}
