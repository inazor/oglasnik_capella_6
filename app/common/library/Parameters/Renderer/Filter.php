<?php

namespace Baseapp\Library\Parameters\Renderer;

class Filter extends BaseRenderer
{
    protected $renderer;

    public $type;
    public $module;
    public $fieldset_parameter;
    public $parameter_settings;
    public $parameter;
    public $data;
    public $errors;
    public $get_only_sql_filter_data = false;

    public function init($type)
    {
        switch ($type) {
            case 'TITLE':
                $this->renderer = new Filter\Title();
                break;

            case 'CURRENCY':
                $this->renderer = new Filter\Currency();
                break;

            case 'LOCATION':
                if (isset($this->parameter_settings[0]->is_searchable_options->type) && 'DEPENDABLE_DROPDOWN_MULTIPLE' === $this->parameter_settings[0]->is_searchable_options->type) {
                    // multiple select
                    $this->renderer = new Filter\LocationMultiple();
                } else {
                    // single select
                    $this->renderer = new Filter\Location();
                }
                break;

            case 'LOCATION_MULTIPLE':
                $this->renderer = new Filter\LocationMultiple();
                break;

            case 'UPLOADABLE':
                $this->renderer = new Filter\Uploadable();
                break;

            case 'YEAR':
                $this->renderer = new Filter\Year();
                break;

            case 'MONTHYEAR':
                $this->renderer = new Filter\MonthYear();
                break;

            case 'DROPDOWN':
                $this->renderer = new Filter\Dropdown();
                break;

            case 'DROPDOWN_MULTIPLE':
                $this->renderer = new Filter\DropdownMultiple();
                break;

            case 'DEPENDABLE_DROPDOWN':
                $this->renderer = new Filter\DependableDropdown();
                break;

            case 'DEPENDABLE_DROPDOWN_MULTIPLE':
                $this->renderer = new Filter\DependableDropdownMultiple();
                break;

            case 'TEXT':
                $this->renderer = new Filter\Text();
                break;

            case 'CHECKBOX':
                $this->renderer = new Filter\Checkbox();
                break;

            case 'RADIO':
                $this->renderer = new Filter\Radio();
                break;

            case 'INTERVAL_DATE':
                $this->renderer = new Filter\IntervalDate();
                break;

            case 'INTERVAL_TEXT':
                $this->renderer = new Filter\IntervalText();
                break;

            case 'INTERVAL_SLIDER':
                $this->renderer = new Filter\IntervalSlider();
                break;

            default:
                $this->renderer = null;
        }

        return $this->renderer ? true : false;
    }

    public function render_filter_combine($tmp_filters = array())
    {
        $combined_filters = null;

        if (count($tmp_filters)) {
            $combined_filters = array();
            $columns = array();
            $where_conditions = array();
            $where_params = array();
            $where_types = array();
            $having_conditions = null;
            //$having_params = array();
            //$having_types = array();

            foreach ($tmp_filters as $tmp_filter) {
                if (isset($tmp_filter['columns'])) {
                    $columns[] = $tmp_filter['columns'];
                }
                if (isset($tmp_filter['condition'])) {
                    $where_conditions[] = $tmp_filter['condition'];
                }
                if (isset($tmp_filter['params'])) {
                    $where_params = array_merge($where_params, $tmp_filter['params']);
                }
                if (isset($tmp_filter['types']) && is_array($tmp_filter['types'])) {
                    $where_types = array_merge($where_types, $tmp_filter['types']);
                }
                if (isset($tmp_filter['having'])) {
                    $having_conditions = $tmp_filter['having'];
                }
                /*
                if (isset($tmp_filter['having_params'])) {
                    $having_params = array_merge($having_params, $tmp_filter['having_params']);
                }
                if (isset($tmp_filter['having_types']) && is_array($tmp_filter['having_types'])) {
                    $having_types = array_merge($having_types, $tmp_filter['having_types']);
                }
                */
            }

            $combined_filters = array();
            if (count($columns)) {
                $combined_filters['columns'] = $columns;
            }
            if (count($where_conditions)) {
                $combined_filters['where_conditions'] = '(' . implode(' AND ', array_unique($where_conditions)) . ')';
                if (count($where_params)) {
                    $combined_filters['where_params'] = $where_params;
                }
                if (count($where_types)) {
                    $combined_filters['where_types'] = $where_types;
                }
            }
            if ($having_conditions) {
                $combined_filters['having_conditions'] = $having_conditions;
                /*
                if (count($having_params)) {
                    $combined_filters['having_params'] = $having_params;
                }
                if (count($having_types)) {
                    $combined_filters['having_types'] = $having_types;
                }
                */
            }

            if (count($combined_filters) === 0) {
                $combined_filters = null;
            }
        }

        return $combined_filters;
    }

    public function getMarkup()
    {
        $markup = null;

        if ($this->renderer) {
            $this->renderer->setModule($this->module);
            $this->renderer->setFieldsetParameter($this->fieldset_parameter);
            $this->renderer->setData($this->data);

            $markup = $this->renderer->getMarkup();
        }

        return $markup;
    }

}
