<?php

namespace Baseapp\Library\Parameters\Renderer\Filter;

use Baseapp\Library\Parameters\Renderer\Filter;

class IntervalText extends Filter
{

    public function getMarkup()
    {
        $markup_data = array(
            'html'        => '',
            'js'          => '',
            'filter'      => null,
            'email_agent' => false
        );

        if (!$this->is_hidden) {
            $markup_data['assets'] = array(
                'css' => array(),
                'js'  => array(
                    'assets/vendor/jquery.autoNumeric.js'
                )
            );

            $cast_type = 'UNSIGNED';
            // TODO: fix casting as DECIMAL
            //       -> this is producing errors - for now PHQL doesn't know how to cast something to DECIMAL...
            // ---------------------------------------------------------------------------------------------------------
            //if (isset($this->parameter_settings->number_decimals) && intval($this->parameter_settings->number_decimals)) {
            //    $cast_type = 'DECIMAL(20,' . intval($this->parameter_settings->number_decimals) . ')';
            //}

            $parameter_id   = 'parameter_' . $this->parameter->id;
            $parameter_name = 'ad_params_' . $this->parameter->id;

            $parameter_value = array(
                'from' => '',
                'to'   => ''
            );

            if (isset($this->data)) {
                $tmp_from = 0;
                $tmp_to = 0;
                if (isset($this->data[$parameter_name . '_from']) && trim($this->data[$parameter_name . '_from'])) {
                    $tmp_from = intval(str_replace(array('.', ','), '', $this->data[$parameter_name . '_from']));
                }
                if (isset($this->data[$parameter_name . '_to']) && trim($this->data[$parameter_name . '_to'])) {
                    $tmp_to = intval(str_replace(array('.', ','), '', $this->data[$parameter_name . '_to']));
                }

                if ($tmp_from || $tmp_to) {
                    if ($tmp_from && $tmp_to) {
                        $parameter_value['from'] = min($tmp_from, $tmp_to);
                        $parameter_value['to'] = max($tmp_from, $tmp_to);
                    } elseif ($tmp_from) {
                        $parameter_value['from'] = $tmp_from;
                    } elseif ($tmp_to) {
                        $parameter_value['to'] = $tmp_to;
                    }
                }

                $tmp_filters = array();

                $model_alias = 'ap_' . $this->parameter->id;
                // test if we have at least one variable set... in case we have, we will will need to add first condition
                // (it will be parameter_id)
                if ($parameter_value['from'] || $parameter_value['to']) {
                    $markup_data['email_agent'] = true;
                    $tmp_filters[] = array(
                        'condition' => '['.$model_alias.'].parameter_id = :parameter_' . $this->parameter->id .'_id:',
                        'params' => array(
                            'parameter_' . $this->parameter->id . '_id' => $this->parameter->id,
                        ),
                        'types' => array(
                            'parameter_' . $this->parameter->id . '_id' => \PDO::PARAM_INT
                        )
                    );
                    if ($parameter_value['from'] && $parameter_value['to']) {
                        if ($parameter_value['from'] == $parameter_value['to']) {
                            $tmp_filters[] = array(
                                'condition' => 'CAST([' . $model_alias . '].value AS ' . $cast_type . ') = :parameter_' . $this->parameter->id . '_value:',
                                'params' => array(
                                    'parameter_' . $this->parameter->id . '_value' => $parameter_value['from'],
                                ),
                                'types' => array(
                                    'parameter_' . $this->parameter->id . '_value' => \PDO::PARAM_INT,
                                )
                            );
                        } else {
                            $tmp_filters[] = array(
                                'condition' => '(CAST([' . $model_alias . '].value AS ' . $cast_type . ') BETWEEN :parameter_' . $this->parameter->id . '_from_value: AND :parameter_' . $this->parameter->id . '_to_value:)',
                                'params' => array(
                                    'parameter_' . $this->parameter->id . '_from_value' => $parameter_value['from'],
                                    'parameter_' . $this->parameter->id . '_to_value' => $parameter_value['to']
                                ),
                                'types' => array(
                                    'parameter_' . $this->parameter->id . '_from_value' => \PDO::PARAM_INT,
                                    'parameter_' . $this->parameter->id . '_to_value' => \PDO::PARAM_INT
                                )
                            );
                        }
                    } else {
                        // we're not sure which value is set, so we test both of them
                        if ($parameter_value['from']) {
                            $tmp_filters[] = array(
                                'condition' => 'CAST([' . $model_alias . '].value AS ' . $cast_type . ') >= :parameter_' . $this->parameter->id . '_from_value:',
                                'params' => array(
                                    'parameter_' . $this->parameter->id . '_from_value' => $parameter_value['from']
                                ),
                                'types' => array(
                                    'parameter_' . $this->parameter->id . '_from_value' => \PDO::PARAM_INT
                                )
                            );
                        } else {
                            $parameter_value['from'] = '';
                        }
                        if ($parameter_value['to']) {
                            $tmp_filters[] = array(
                                'condition' => 'CAST([' . $model_alias . '].value AS ' . $cast_type . ') <= :parameter_' . $this->parameter->id . '_to_value:',
                                'params' => array(
                                    'parameter_' . $this->parameter->id . '_to_value' => $parameter_value['to']
                                ),
                                'types' => array(
                                    'parameter_' . $this->parameter->id . '_to_value' => \PDO::PARAM_INT
                                )
                            );
                        } else {
                            $parameter_value['to'] = '';
                        }
                    }
                }

                if ($combined_filter = $this->render_filter_combine($tmp_filters)) {
                    $markup_data['filter'] = array(
                        'model' => 'Baseapp\Models\AdsParameters',
                        'alias' => $model_alias,
                        'filter' => $combined_filter
                    );
                }
            }

            $suffix = $this->parameter_settings->suffix ? $this->parameter_settings->suffix : '';
            $filter_label = $this->parameter_settings->label_text;
            if (isset($this->parameter_settings->is_searchable_options->label_text) && !empty($this->parameter_settings->is_searchable_options->label_text)) {
                $filter_label = trim($this->parameter_settings->is_searchable_options->label_text);
            }

            $markup_data['html'] .= '<h3>' . $filter_label . '</h3>' . PHP_EOL;
            $markup_data['html'] .= '<div class="form-group">' . PHP_EOL;
            $markup_data['html'] .= '    <div class="input-group">' . PHP_EOL;
            $markup_data['html'] .= '        <span class="input-group-addon">Od</span>' . PHP_EOL;
            $markup_data['html'] .= '        <input type="text" id="' . $parameter_id . '_from" name="' . $parameter_name . '_from" value="' . $parameter_value['from'] . '" class="form-control text-right" placeholder="Min." data-type="number" data-default="0" data-bit-name="' . $filter_label . ' ' . $suffix . ' od">' . PHP_EOL;
            if ($suffix) {
                $markup_data['html'] .= '        <span class="input-group-addon">' . $suffix . '</span>' . PHP_EOL;
            }
            $markup_data['html'] .= '    </div>' . PHP_EOL;
            $markup_data['html'] .= '</div>' . PHP_EOL;
            $markup_data['html'] .= '<div class="form-group">' . PHP_EOL;
            $markup_data['html'] .= '    <div class="input-group">' . PHP_EOL;
            $markup_data['html'] .= '        <span class="input-group-addon">Do</span>' . PHP_EOL;
            $markup_data['html'] .= '        <input type="text" id="' . $parameter_id . '_to" name="' . $parameter_name . '_to" value="' . $parameter_value['to'] . '" class="form-control text-right" placeholder="Max." data-type="number" data-default="0" data-bit-name="' . $filter_label . ' ' . $suffix . ' do">' . PHP_EOL;
            if ($suffix) {
                $markup_data['html'] .= '        <span class="input-group-addon">' . $suffix . '</span>' . PHP_EOL;
            }
            $markup_data['html'] .= '    </div>' . PHP_EOL;
            $markup_data['html'] .= '</div>' . PHP_EOL;

            $element_selectors = '#' . $parameter_id . '_from, #' . $parameter_id . '_to';
            $markup_data['js'] = <<<JS

    \$('$element_selectors').autoNumeric({aSep:'.', aDec:',', mDec:'0'});
JS;
        }

        return $markup_data;
    }

}
