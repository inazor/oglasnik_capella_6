<?php

namespace Baseapp\Library\Parameters\Renderer\Filter;

use Baseapp\Library\Parameters\Renderer\Filter;
use Baseapp\Models\Locations;

class LocationMultiple extends Filter
{

    public function getMarkup()
    {
        $markup_data = array(
            'html'        => '',
            'js'          => '',
            'filter'      => null,
            'email_agent' => false
        );

        $db_level_names = array('country_id', 'county_id', 'city_id', 'municipality_id');

        if ($this->parameter_settings) {
            $tmp_filters = array();

            if (count($this->parameter_settings)) {
                $curr_level = 1;

                $markup_data['js'] = <<<JS

    function parameter_location_onChange(\$source, \$target) {
        \$target.find('option').remove();
        if ('undefined' != typeof \$source.val()) {
            var location_source_values = \$source.val();
            if (location_source_values) {
                if (location_source_values.length === 1 && '' != location_source_values[0]) {
                    \$.get(
                        '/ajax/location/' + location_source_values[0] + '/values',
                        function(data) {
                            if (data.length) {
                                \$target.append(
                                    //\$('<option/>').attr('value', '').text('')
                                );
                                \$.each(data, function(idx, data){
                                    \$target.append(
                                        \$('<option/>').attr('value', data.id).text(data.name)
                                    );
                                });
                            }
                            \$('#' + \$target.attr('id') + '_box').slideDown(function(){
                                \$target.prop('disabled', false);
                                \$target.selectpicker('refresh');
                            });
                        },
                        'json'
                    );
                } else {
                    \$('#' + \$target.attr('id') + '_box').slideUp(function(){
                        \$target.prop('disabled', true);
                        \$target.selectpicker('refresh');
                    });
                }
            } else {
                \$('#' + \$target.attr('id') + '_box').slideUp(function(){
                    \$target.prop('disabled', true);
                    \$target.selectpicker('refresh');
                });
            }
        } else {
            \$('#' + \$target.attr('id') + '_box').slideUp(function(){
                \$target.prop('disabled', true);
                \$target.selectpicker('refresh');
            });
        }
        \$target.prop('selectedIndex', 0).change();
    }
JS;

                $prev_level = null;
                $all_dropdowns = array();
                $curr_location = new Locations();
                $markup_data['html'] .= '<h3>Lokacija</h3>' . PHP_EOL;
                $markup_data['html'] .= '<div class="form-group onlyDeselect">' . PHP_EOL;
                foreach ($this->parameter_settings as $level) {
                    $parameter_level_id = 'parameter_location_level_' . $curr_level;
                    $parameter_level_name = 'ad_location_' . $curr_level;

                    if (isset($this->data) && isset($this->data[$parameter_level_name])) {
                        $level_selected_value = null;
                        if (is_array($this->data[$parameter_level_name]) && count($this->data[$parameter_level_name])) {
                            if (count($this->data[$parameter_level_name]) > 1) {
                                $level_selected_value = array();
                                foreach ($this->data[$parameter_level_name] as $val) {
                                    if (intval($val)) {
                                        $level_selected_value[] = intval($val);
                                    }
                                }
                                if (empty($level_selected_value)) {
                                    $level_selected_value = null;
                                }
                            } else {
                                $level_selected_value = intval($this->data[$parameter_level_name][0]);
                            }
                        } elseif (intval($this->data[$parameter_level_name])) {
                            $level_selected_value = intval($this->data[$parameter_level_name]);
                        }
                    } else {
                        $level_selected_value = null;
                    }
                    if ($level_selected_value) {
                        $markup_data['email_agent'] = true;
                    }

                    if (1 === $level->is_hidden) {
                        if (null === $level_selected_value) {
                            $level_selected_value = $level->default_value ? intval($level->default_value) : null;
                        }

                        //$markup_data['html'] .= '<input type="hidden" name="' . $parameter_level_name . '" id="' . $parameter_level_id . '" value="' . ($level_selected_value ? $level_selected_value : '') . '" />' . PHP_EOL;
                    } elseif ($level->is_searchable) {

                        $filter_label = $level->label_text;
                        if (isset($level->is_searchable_options->label_text) && !empty($level->is_searchable_options->label_text)) {
                            $filter_label = trim($level->is_searchable_options->label_text);
                        }

                        $all_dropdowns[] = '#'.$parameter_level_id;
                        $required_html = '';
                        $placeholder_text = isset($level->placeholder_text) && trim($level->placeholder_text) ? trim($level->placeholder_text) : null;

                        $markup_data['html'] .= '    <div id="' . $parameter_level_id . '_box" class="margin-bottom-sm"' . (!$curr_location ? ' style="display:none"' : '') . '>' . PHP_EOL;
                        $markup_data['html'] .= '        <label for="' . $parameter_level_id . '">' . $filter_label . '</label>' . PHP_EOL;
                        $markup_data['html'] .= '        <div class="icon_dropdown">' . PHP_EOL;
                        $markup_data['html'] .= '            <select class="form-control" id="' . $parameter_level_id . '" name="' . $parameter_level_name . '[]" data-type="select" data-default="" multiple="multiple" data-bit-name="' . $filter_label . '"' . ($placeholder_text ? ' title="' . $placeholder_text . '"' : '') . '>' . PHP_EOL;

                        // for loop begin
                        if ($curr_location) {
                            $location_level_values = Locations::getCachedChildren($curr_location->id);
                            if ($location_level_values) {
                                foreach ($location_level_values as $value) {
                                    $selected_option = '';
                                    if ($level_selected_value) {
                                        if (is_array($level_selected_value)) {
                                            $selected_option = in_array(intval($value->id), $level_selected_value) ? ' selected="selected"' : '';
                                        } else {
                                            $selected_option = intval($value->id) === intval($level_selected_value) ? ' selected="selected"' : '';
                                        }
                                    }
                                    $markup_data['html'] .= '                <option value="' . ((int) $value->id) . '"' . $selected_option . '>' . trim($value->name) . '</option>' . PHP_EOL;
                                }
                            }
                        }
                        // for loop end;

                        $markup_data['html'] .= '            </select>' . PHP_EOL;
                        $markup_data['html'] .= '        </div>' . PHP_EOL;
                        $markup_data['html'] .= '    </div>' . PHP_EOL;

                        if ($prev_level) {
                            $prev_parameter_level_id = 'parameter_location_level_' . $prev_level;
                            $markup_data['js'] .= <<<JS

    \$('#$prev_parameter_level_id').change(function(){
        parameter_location_onChange(\$(this), \$('#$parameter_level_id'));
    });
JS;
                        }
                    }

                    if ($level_selected_value && isset($db_level_names[($curr_level - 1)])) {
                        if (is_array($level_selected_value)) {
                            $tmp_filters[] = array(
                                'condition' => 'ad.' . $db_level_names[($curr_level - 1)] . ' IN (' . implode(',', $level_selected_value) . ')',
                                'params' => array(),
                                'types' => array()
                            );
                        } else {
                            $tmp_filters[] = array(
                                'condition' => 'ad.' . $db_level_names[($curr_level - 1)] . ' = :' . $db_level_names[($curr_level - 1)] . ':',
                                'params' => array(
                                    $db_level_names[($curr_level - 1)] => intval($level_selected_value)
                                ),
                                'types' => array(
                                    $db_level_names[($curr_level - 1)] => \PDO::PARAM_INT
                                )
                            );
                        }
                    }

                    $prev_level = $curr_level;

                    // if we have something selected in current level, then, in next iteration we will fetch currently
                    // selected level's value children
                    $curr_location = null;
                    if ($level_selected_value && !is_array($level_selected_value) && (string) intval($level_selected_value) === (string) $level_selected_value) {
                        $curr_location = Locations::findFirst($level_selected_value);
                    }

                    $curr_level++;
                }
                $markup_data['html'] .= '</div>' . PHP_EOL;

                if (count($all_dropdowns)) {
                    $all_dropdowns = implode(', ', $all_dropdowns);
                    $markup_data['js'] .= <<<JS

    \$('$all_dropdowns').selectpicker({
        style: 'btn btn-default',
        noneSelectedText: '',
        liveSearch: true,
        actionsBox: true,
        selectAllText: 'Označi sve',
        deselectAllText: 'Poništi',
        confirmSelectionText: 'Potvrdi odabir',
        noneResultsText: '',
        mobile: is_mobile_browser(),
        iconBase: 'fa',
        tickIcon: 'fa-check text-primary',
        dropupAuto: false
    });
    \$('$all_dropdowns').on('loaded.bs.select', function(e) {
        var \$selectAllBtn = $(e.target).closest('.bootstrap-select').find('.actions-btn.bs-select-all');
        if (\$selectAllBtn.length) {
            \$selectAllBtn.remove();
        }
    });
JS;
                }

            }
            $markup_data['filter'] = $this->render_filter_combine($tmp_filters);
        }

        return $markup_data;
    }

}

