<?php

namespace Baseapp\Library\Parameters\Renderer\MapSearchFilter;

use Baseapp\Library\Parameters\Renderer\MapSearchFilter;
use Baseapp\Library\Utils;
use Phalcon\Di;

class Currency extends MapSearchFilter
{

    public function getMarkup()
    {
        $markup_data = array(
            'html'        => '',
            'js'          => '',
            'filter'      => null,
            'email_agent' => false,
            'full_row'    => false,
            'name'        => 'ad_price'
        );

        if (!$this->is_hidden) {
            $parameter_category_id = $this->fieldset_parameter->category_id;

            $markup_data['assets'] = array(
                'css' => array(),
                'js'  => array(
                    'assets/vendor/jquery.autoNumeric.js'
                )
            );

            $parameter_id        = 'parameter_price';
            $parameter_name      = 'ad_price';

            $parameter_value = array(
                'from' => '',
                'to'   => ''
            );

            $default_currency_id = Di::getDefault()->getShared('config')->payment->default_currency_id;
            $currencies = \Baseapp\Models\Currency::all();

            if (isset($this->data)) {
                $input_from = null;
                $input_to   = null;

                if (isset($this->data[$parameter_name . '_code'])) {
                    $currency_code = trim($this->data[$parameter_name . '_code']);
                } else {
                    $currency_code = trim($currencies[$default_currency_id]['short_name']);
                }
                $currency = $currencies[$currency_code];

                if (isset($this->data[$parameter_name . '_from']) && trim($this->data[$parameter_name . '_from'])) {
                    $input_from = intval(str_replace(array('.', ','), '', $this->data[$parameter_name . '_from']));
                }
                if (isset($this->data[$parameter_name . '_to']) && trim($this->data[$parameter_name . '_to'])) {
                    $input_to = intval(str_replace(array('.', ','), '', $this->data[$parameter_name . '_to']));
                }

                $tmp_from = 0;
                $tmp_to = 0;
                if ($input_from && $input_to) {
                    $tmp_from = min($input_from, $input_to);
                    $tmp_to = max($input_from, $input_to);
                } else {
                    if ($input_from) {
                        $tmp_from = $input_from;
                    }
                    if ($input_to) {
                        $tmp_to = $input_to;
                    }
                }

                $parameter_value['from'] = $tmp_from;
                $parameter_value['to'] = $tmp_to;

                $tmp_filters = array();
                if ($parameter_value['from']) {
                    $param_from_name = 'price_from';
                    $tmp_filters[] = array(
//                        'condition' => '(ad.price / ' . $currency['exchange_rate'] . ') >= :price_from:',
                        'condition' => '(ad.price / currency.exchange_rate) >= :' . $param_from_name . ':',
                        'params' => array(
                            $param_from_name => Utils::kn2lp(($parameter_value['from'] / $currency['exchange_rate']))
                        ),
                        'types' => array(
                            $param_from_name => \PDO::PARAM_INT
                        )
                    );
                } else {
                    $parameter_value['from'] = '';
                }
                if ($parameter_value['to']) {
                    $param_to_name = 'price_to';
                    $tmp_filters[] = array(
//                        'condition' => '(ad.price / ' . $currency['exchange_rate'] . ') <= :price_to:',
                        'condition' => '(ad.price / currency.exchange_rate) <= :' . $param_to_name . ':',
                        'params' => array(
                            $param_to_name => Utils::kn2lp(($parameter_value['to'] / $currency['exchange_rate']))
                        ),
                        'types' => array(
                            $param_to_name => \PDO::PARAM_INT
                        )
                    );
                } else {
                    $parameter_value['to'] = '';
                }

               // var_dump($tmp_filters);exit;

                if (count($tmp_filters)) {
                    $markup_data['filter'] = $this->render_filter_combine($tmp_filters);
                    $markup_data['filter']['innerJoin'] = array(
                        'currency' => array(
                            'model'     => 'Baseapp\Models\Currency', 
                            'condition' => 'ad.currency_id = currency.id', 
                            'alias'     => 'currency'
                        )
                    );
                }
            }

            $filter_currency_string = '';
            $filter_currency_code = '';
            if (isset($this->parameter_settings->currency_id) && $this->parameter_settings->currency_id) {
                $filter_currency = $currencies[$this->parameter_settings->currency_id];
                if ($filter_currency) {
                    $filter_currency_string = $filter_currency['short_name'];
                    $filter_currency_code = $filter_currency['short_name'];
                }
            }

/*
            if (!$this->get_only_sql_filter_data) {
                $filter_label = trim($this->parameter_settings->label_text);
                $markup_data['html'] .= '    <div class="col-md-6">' . PHP_EOL;
                $markup_data['html'] .= '        <label>' . $filter_label . '</label>' . PHP_EOL;
                if ('' !== $filter_currency_code && $filter_currency['id'] != $default_currency_id) {
                    $markup_data['html'] .= '        <input type="hidden" name="' . $parameter_name . '_code" value="' . $filter_currency_code . '" />' . PHP_EOL;
                }
                $markup_data['html'] .= '        <div class="row">' . PHP_EOL;
                $markup_data['html'] .= '            <div class="col-md-6">' . PHP_EOL;
                $markup_data['html'] .= '                <div class="form-group">' . PHP_EOL;
                $markup_data['html'] .= '                    <div class="input-group">' . PHP_EOL;
                $markup_data['html'] .= '                        <span class="input-group-addon">Od</span>' . PHP_EOL;
                $markup_data['html'] .= '                        <input type="text" id="' . $parameter_id . '_from" name="' . $parameter_name . '_from" value="' . $parameter_value['from'] . '" class="form-control text-right" data-type="number" data-default="0" data-bit-name="' . $filter_label . ' od" data-bit-value="' . $parameter_value['from'] . '">' . PHP_EOL;
                if ($filter_currency_string) {
                    $markup_data['html'] .= '                        <span class="input-group-addon">' . $filter_currency_string . '</span>' . PHP_EOL;
                }
                $markup_data['html'] .= '                    </div>' . PHP_EOL;
                $markup_data['html'] .= '                </div>' . PHP_EOL;
                $markup_data['html'] .= '            </div>' . PHP_EOL;
                $markup_data['html'] .= '            <div class="col-md-6">' . PHP_EOL;
                $markup_data['html'] .= '                <div class="form-group">' . PHP_EOL;
                $markup_data['html'] .= '                    <div class="input-group">' . PHP_EOL;
                $markup_data['html'] .= '                        <span class="input-group-addon">Do</span>' . PHP_EOL;
                $markup_data['html'] .= '                        <input type="text" id="' . $parameter_id . '_to" name="' . $parameter_name . '_to" value="' . $parameter_value['to'] . '" class="form-control text-right" data-type="number" data-default="0" data-bit-name="' . $filter_label . ' do" data-bit-value="' . $parameter_value['to'] . '">' . PHP_EOL;
                if ($filter_currency_string) {
                    $markup_data['html'] .= '                        <span class="input-group-addon">' . $filter_currency_string . '</span>' . PHP_EOL;
                }
                $markup_data['html'] .= '                    </div>' . PHP_EOL;
                $markup_data['html'] .= '                </div>' . PHP_EOL;
                $markup_data['html'] .= '            </div>' . PHP_EOL;
                $markup_data['html'] .= '        </div>' . PHP_EOL;
                $markup_data['html'] .= '    </div>' . PHP_EOL;
            }
*/

            $autoNumericOptions = $parameter_id . '_options';
            $autoNumericFromName = '#' . $parameter_id . '_from';
            $autoNumericFromObject = $parameter_id . '_from_autoNumeric';
            $autoNumericToName = '#' . $parameter_id . '_to';
            $autoNumericToObject = $parameter_id . '_to_autoNumeric';

            $markup_data['js'] = <<<JS

    var $autoNumericOptions = {aSep:'.', aDec:',', mDec:'0', wEmpty:'zero', lZero:'deny'};
    var \$$autoNumericFromObject = \$('$autoNumericFromName').autoNumeric($autoNumericOptions);
    var \$$autoNumericToObject = \$('$autoNumericToName').autoNumeric($autoNumericOptions);
JS;
        }

        return $markup_data;
    }

}
