<?php

namespace Baseapp\Library\Parameters\Renderer\View;

use Baseapp\Library\Parameters\Renderer\View;
use Baseapp\Models\Dictionaries as Dictionary;

class DictionaryParameter extends View
{
    public function render()
    {
        $html     = '';
        $tmp_html = '';

        if ('DEPENDABLE_DROPDOWN' == $this->type && isset($this->parameter_settings) && count($this->parameter_settings)) {
            $curr_level = 1;
            foreach ($this->parameter_settings as $level) {
                if (!$level->is_hidden) {
                    $parameter_name     = 'ad_params_' . $this->parameter->parameter_id;
                    $parameter_level_id = 'level_' . $curr_level;

                    if (isset($this->data->$parameter_name->$parameter_level_id)) {
                        $parameter_level = $this->data->$parameter_name->$parameter_level_id;

                        $level_selected_value = null;
                        if (isset($parameter_level->text_value)) {
                            $level_selected_value = trim($parameter_level->text_value);
                        } elseif (isset($parameter_level->dictionary_id) && intval($parameter_level->dictionary_id)) {
                            $dictionary = Dictionary::findFirst(intval($parameter_level->dictionary_id));
                            if ($dictionary) {
                                $level_selected_value = $dictionary->name;
                            }
                        }

                        if ($level_selected_value) {
                            if ('group' === $this->fieldset_style) {
                                $tmp_html .= '        <li>' . $level_selected_value . '</li>' . PHP_EOL;
                            } else {
                                $tmp_html .= '    <span class="color-light">' . trim($level->label_text) . ':</span> ' . $level_selected_value . '<br>' . PHP_EOL;
                            }
                        }
                    }
                }

                $curr_level++;
            }
        } elseif ('CHECKBOXGROUP' == $this->type) {
            $parameter_name = 'ad_params_' . $this->parameter->parameter_id;

            if (isset($this->data->$parameter_name) && count($this->data->$parameter_name)) {
                $chosen_dict_ids = array();
                $chosen_values = array();

                foreach ($this->data->$parameter_name as $parameter_option) {
                    if (isset($parameter_option->text_value)) {
                        $chosen_values[] = trim($parameter_option->text_value);
                    } elseif (isset($parameter_option->value) && intval($parameter_option->value)) {
                        $chosen_dict_ids[] = intval($parameter_option->value);
                    }
                }

                if (count($chosen_values)) {
                    if ('group' === $this->fieldset_style) {
                        foreach ($chosen_values as $chosen_value) {
                            $row_value = trim($chosen_value);
                            $tmp_html .= '        <li>' . $row_value . '</li>' . PHP_EOL;
                        }
                    } else {
                        $tmp_html .= '    <span class="color-light">' . trim($this->parameter_settings->label_text) . ':</span> ' . implode(', ', $chosen_values) . '<br>' . PHP_EOL;
                    }
                } elseif (count($chosen_dict_ids)) {
                    $chosen_dict_values = Dictionary::find(
                        array(
                            'columns' => 'name',
                            'conditions' => 'id in (' . implode(',', $chosen_dict_ids) . ')',
                            'order' => 'lft'
                        )
                    );
                    if ($chosen_dict_values && count($chosen_dict_values)) {
                        $merge_array = array();
                        $chosen_tmp_html = '';
                        foreach ($chosen_dict_values as $row) {
                            $row_name = trim($row->name);
                            $merge_array[] = $row_name;

                            $chosen_tmp_html .= '    <span class="color-light">' . trim($this->parameter_settings->label_text) . ':</span> ' . $row_name . '<br>' . PHP_EOL;
                        }

                        if ('group' === $this->fieldset_style) {
                            $tmp_html .= '        <li>' . implode(', ', $merge_array) . '</li>' . PHP_EOL;
                        } else {
                            $tmp_html .= $chosen_tmp_html;
                        }
                    }
                }
            }
        } else {
            $parameter_name = 'ad_params_' . $this->parameter->parameter_id;
            if (!$this->parameter_settings->is_hidden && isset($this->data->$parameter_name)) {
                $parameter_options = $this->data->$parameter_name;

                $selected_value = null;
                if (isset($parameter_options->text_value)) {
                    $selected_value = trim($parameter_options->text_value);
                } elseif (isset($parameter_options->dictionary_id) && intval($parameter_options->dictionary_id)) {
                    $dictionary = Dictionary::findFirst(intval($parameter_options->dictionary_id));
                    if ($dictionary) {
                        $selected_value = $dictionary->name;
                    }
                }

                if ($selected_value) {
                    if ('group' === $this->fieldset_style) {
                        $tmp_html .= '        <li>' . $selected_value . '</li>' . PHP_EOL;
                    } else {
                        $tmp_html .= '    <span class="color-light">' . trim($this->parameter_settings->label_text) . ':</span> ' . $selected_value . '<br>' . PHP_EOL;
                    }
                }
            }
        }

        if (trim($tmp_html)) {
            $html = '<div class="col-sm-6">' . PHP_EOL;
            if ('group' === $this->fieldset_style) {
                $html .= '    <span class="color-light">' . trim($this->parameter_settings->label_text) . '</span>' . PHP_EOL;
                $html .= '    <ul>' . PHP_EOL;
                $html .= $tmp_html;
                $html .= '    </ul>' . PHP_EOL;
            } else {
                $html .= $tmp_html;
            }
            $html .= '</div>' . PHP_EOL;

            return array(
                'html' => trim($html)
            );
        }

        return null;
    }

}
