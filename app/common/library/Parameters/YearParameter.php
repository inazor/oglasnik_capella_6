<?php

namespace Baseapp\Library\Parameters;

class YearParameter extends BaseParameter
{
    protected $group_type = 'custom';
    protected $settings_location = 'data';
    protected $type = 'YEAR';
    protected $accept_dictionary = false;
    protected $can_default = true;
    protected $can_default_type = 'dropdown';
    protected $can_other = false;
    protected $can_prefix_suffix = false;
    protected $is_dependable = false;
    protected $is_required = false;
    public $is_searchable = false;
    public $is_searchable_type = array('INTERVAL_DROPDOWN');

    public function setValue($value)
    {
        if (trim($value)) {
            $this->setPreparedData(array(
                'id' => (int) $this->parameter->id,
                'value' => (string) $value
            ));

            $this->setPreparedDataJson(
                'ad_params_' . $this->parameter->id,
                array(
                    'value' => (int) $value,
                    'text_value' => (string) $value
                )
            );
        }
    }

    public function process()
    {
        $this->validate_cannot_be_empty();
        if (isset($this->data['ad_params_' . $this->parameter->id])) {
            $this->setValue(trim(strip_tags($this->data['ad_params_' . $this->parameter->id])));
        }
    }

    public function render_input()
    {
        parent::render_input();

        $render_data = array(
            'row'  => false,
            'html' => '',
            'js'   => ''
        );

        if (!$this->is_hidden) {
            $parameter_id = 'parameter_' . $this->parameter->id;
            $parameter_name = 'ad_params_' . $this->parameter->id;

            if (isset($this->data)) {
                $selected_value = isset($this->data[$parameter_name]) ? (is_numeric($this->data[$parameter_name]) ? intval($this->data[$parameter_name]) : trim($this->data[$parameter_name])) : null;
            } else {
                $selected_value = $this->parameter_settings->default_value ? intval($this->parameter_settings->default_value) : null;
            }
            $selected_value = strip_tags($selected_value);

            $required_html = ($this->parameter_settings->is_required ? ' <abbr title="' . $this->getValidationString('required_field') . '">*</abbr>' : '');

            $div_class = $this->render_as_full_row ? 'col-lg-12' : 'col-lg-3 col-md-3';
            $render_data['html'] .= '<div class="' . $div_class . '">' . PHP_EOL;
            $render_data['html'] .= '    <div class="form-group' . (isset($this->errors) && $this->errors->filter($parameter_name) ? ' has-error' : '') . '">';
            $render_data['html'] .= '        <label class="control-label" for="' . $parameter_id . '">' . trim($this->parameter_settings->label_text) . $required_html . '</label>' . PHP_EOL;
            $render_data['html'] .= '        <div class="icon_dropdown">' . PHP_EOL;
            $render_data['html'] .= '            <select class="form-control show-tick" name="' . $parameter_name . '" id="' . $parameter_id . '">' . PHP_EOL;
            $render_data['html'] .= '                <option value=""></option>' . PHP_EOL;

            $curr_time = time();
            if ('Year' === $this->parameter_settings->interval_begin_period) {
                $tmp_starting_time = mktime(0, 0, 0, date('n', $curr_time), 1, (date('Y', $curr_time) + $this->parameter_settings->interval_begin_value));
            } elseif ('Month' === $this->parameter_settings->interval_begin_period) {
                $tmp_starting_time = mktime(0, 0, 0, (date('n', $curr_time) + $this->parameter_settings->interval_begin_value), 1, date('Y', $curr_time));
            } elseif ('Number' === $this->parameter_settings->interval_begin_period) {
                $tmp_starting_time = mktime(0, 0, 0, date('n', $curr_time), 1, $this->parameter_settings->interval_begin_value);
            }
            if ('Year' === $this->parameter_settings->interval_end_period) {
                $tmp_ending_time = mktime(0, 0, 0, date('n', $curr_time), 1, (date('Y', $curr_time) + $this->parameter_settings->interval_end_value));
            } elseif ('Month' === $this->parameter_settings->interval_end_period) {
                $tmp_ending_time = mktime(0, 0, 0, (date('n', $curr_time) + $this->parameter_settings->interval_end_value), 1, date('Y', $curr_time));
            } elseif ('Number' === $this->parameter_settings->interval_end_period) {
                $tmp_ending_time = mktime(0, 0, 0, date('n', $curr_time), 1, $this->parameter_settings->interval_end_value);
            }

            $starting_year = date('Y', min($tmp_starting_time, $tmp_ending_time));
            $ending_year = date('Y', max($tmp_starting_time, $tmp_ending_time));

            // for loop begin
            if ($this->parameter_settings->is_reverse) {
                for ($curr_year = $ending_year; $curr_year >= $starting_year; $curr_year--) {
                    $selected_option = $selected_value && $selected_value == intval($curr_year) ? ' selected="selected"' : '';
                    $render_data['html'] .= '                <option value="' . ((int) $curr_year) . '"' . $selected_option . '>' . trim($curr_year) . '</option>' . PHP_EOL;
                }
            } else {
                for ($curr_year = $starting_year; $curr_year <= $ending_year; $curr_year++) {
                    $selected_option = $selected_value && $selected_value == intval($curr_year) ? ' selected="selected"' : '';
                    $render_data['html'] .= '                <option value="' . ((int) $curr_year) . '"' . $selected_option . '>' . trim($curr_year) . '</option>' . PHP_EOL;
                }
            }
            // for loop end;
            $render_data['html'] .= '            </select>' . PHP_EOL;
            $render_data['html'] .= '        </div>' . PHP_EOL;

            if (isset($this->errors) && $this->errors->filter($parameter_name)) {
                $render_data['html'] .= '        <p class="help-block">' . current($this->errors->filter($parameter_name))->getMessage() . '</p>' . PHP_EOL;
            } elseif (isset($this->parameter_settings->help_text) && trim($this->parameter_settings->help_text)) {
                $render_data['html'] .= '        <p class="help-block">' . trim($this->parameter_settings->help_text) . '</p>' . PHP_EOL;
            }

            $render_data['html'] .= '    </div>' . PHP_EOL;
            $render_data['html'] .= '</div>' . PHP_EOL;

            $liveSearch = 'true';
            if (($ending_year - $starting_year) <= 20) {
                $liveSearch = 'false';
            }

            $style = 'btn btn-default';
            $noneSelectedText = trim($this->parameter_settings->placeholder_text) ? trim($this->parameter_settings->placeholder_text) : $this->getValidationString('select_something');
            $noneResultsText  = $this->getValidationString('no_values_found');

            $render_data['js'] .= <<<JS

    \$('#$parameter_id').selectpicker({
        style: '$style',
        size: 8,
        noneSelectedText: '$noneSelectedText',
        liveSearch: $liveSearch,
        noneResultsText: '$noneResultsText',
        mobile: OGL.is_mobile_browser,
        iconBase: 'fa',
        tickIcon: 'fa-check text-primary'
    });
JS;
        }

        return $render_data;
    }

}
