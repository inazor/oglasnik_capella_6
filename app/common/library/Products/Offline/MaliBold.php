<?php

namespace Baseapp\Library\Products\Offline;

use Baseapp\Library\Products\Offline;

class MaliBold extends Offline
{
    protected $id = 'mali-bold';
    protected $code = 'MaliBPT';
    protected $title = 'Mali bold oglas';
    protected $description = '<p>Izdvojite se od ostalih malih oglasa i istaknite svoj oglas boldani slovima. Za redovito izlaženje zakupuite pakete objava.</p>';

    protected $avus_adformat_single = 'Mali bold single T';
    protected $avus_adlayout_single = 'Bold Pmali';
    protected $avus_adformat_multi  = 'Mali bold pack T';
    protected $avus_adlayout_multi  = 'Bold Pmali';

    protected $options = array(
        '1 objava' => '3000',
        '2 objave' => '4900',
        '4 objave' => '8200',
        '8 objava' => '16400'
    );

    protected $extras = false;
}
