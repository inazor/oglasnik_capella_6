<?php

namespace Baseapp\Library\Products\Offline;

use Baseapp\Library\Products\Offline;
use Baseapp\Models\Users;

class MaliPoslovni extends Offline
{
    protected $id = 'mali-poslovni';
    protected $code = 'MaliCT';
    protected $title = 'Mali poslovni oglas';
    protected $description = '<p>Ukoliko niste privatna osoba, tada biste trebali zakupiti najmanje Mali poslovni oglas za pojedinačnu prodaju. Ako želite biti sigurni da će vaš oglas redovito izlaziti, zakupite paket objava i uštedite.</p>';

    protected $avus_adformat_single = 'Mali komercijalni single T+';
    protected $avus_adlayout_single = 'Obični';
    protected $avus_adformat_multi  = 'Mali komercijalni Pack T+';
    protected $avus_adlayout_multi  = 'Obični';

    protected $options = array(
        '1 objava' => '2200',
        '2 objave' => '3300',
        '4 objave' => '6200',
        '8 objava' => '12400'
    );

    protected $extras = false;

    protected $payment_required_before_publication = true;

    protected function configureByUser(Users $user)
    {
        if ($user->type == $user::TYPE_PERSONAL) {
            $this->isDisabled(true);
        }
    }
}
