<?php

namespace Baseapp\Library\Products\Online;

trait NoHomepageFeaturingGetExtrasTrait
{
    public function getExtras()
    {
        // Since roughly May 2017, 'Objava na naslovnici' is no longer a thing,
        // so, if that's set in $this->extras (from let's say, some stored serialized product somewhere etc)
        // let's make sure it's not returned
        $extras_copy = $this->extras;
        if (isset($extras_copy['Objava na naslovnici'])) {
            unset($extras_copy['Objava na naslovnici']);
        }

        // Don't return an empty array, make it the same as when some product types set protected $extras = false
        if (is_array($extras_copy) && empty($extras_copy)) {
            $extras_copy = false;
        }

        return $extras_copy;
    }
}
