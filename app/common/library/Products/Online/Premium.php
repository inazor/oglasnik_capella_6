<?php

namespace Baseapp\Library\Products\Online;

use Baseapp\Library\Products\Online;

class Premium extends Online
{
    use NoHomepageFeaturingGetExtrasTrait;

    protected $id = 'premium';
    protected $title = 'Top oglas';
    protected $description = <<<HTML
<ul>
<li>Nalazi se na samom vrhu (iznad osnovnih/malih oglasa) - korisnici «moraju vidjeti» vaš oglas</li>
<li>Zelenom bojom odskače od ostalih oglasa i plijeni pažnju korisnika</li>
<li>Najnoviji oglasi su na vrhu; poredani po datumu objave</li>
<li>Ne zaboravite na opciju „Skok na vrh“ kako bi uvijek bili ispred konkurencije</li>
</ul>
HTML;

    protected $options = array(
        '5 dana'  => '10100',
        '15 dana' => '20200',
        '30 dana' => '34400'
    );

    protected $extras = array(
        'Objava na naslovnici' => array(
            '3 dana' => '25000',
            '7 dana' => '35000',
        )
    );

    protected $push_up = '2500';

    protected $sort_idx = 10;
}
