<?php

namespace Baseapp\Library\Products;

use Baseapp\Library\Debug;
use Baseapp\Library\Products\Online\Pushup;
use Baseapp\Models\Ads;

class PushupProductsSelector extends ProductsSelector
{
    protected $products        = array();
    protected $chosen_products = array();
    protected $ads             = null;
    protected $post_data       = null;

    public function __construct($ads = null, $post_data = null)
    {
        $this->ads       = $ads;
        $this->post_data = $post_data;

        $this->process();

        return $this;
    }

    public function getAds()
    {
        return $this->ads;
    }

    public function getPostData()
    {
        return $this->post_data;
    }

    protected function process()
    {
        // Creates and customizes the price of each new Pushup product
        foreach ($this->ads as $ad) {
            $product = $this->createProductFromAd($ad);
            if (null !== $product) {
                $this->addProduct($product);
                if ($product->isChosen()) {
                    $this->addChosenProduct($product);
                }
            }
        }

        $this->ensureAtLeastOneChosenProduct();
    }

    /**
     * @param ProductsInterface $product
     * @param null|string $group Unused
     *
     * @return bool
     */
    protected function addProduct(ProductsInterface $product, $group = null)
    {
        $this->products[] = $product;

        return true;
    }

    /**
     * @param ProductsInterface $product
     * @param null|string $group Unused
     *
     * @return bool
     */
    protected function addChosenProduct(ProductsInterface $product, $group = null)
    {
        $this->chosen_products[] = $product;

        return true;
    }

    /**
     * Return flat array/list of chosen products
     *
     * @return ProductsInterface[]
     */
    public function getChosenProductsFlat()
    {
        return $this->chosen_products;
    }

    /**
     * @param Ads $ad
     *
     * @return ProductsInterface
     */
    protected function createProductFromAd(Ads $ad)
    {
        $product = null;

        $ad_product = $ad->getProduct('online');
        if ($ad_product) {
            $product = new Pushup();
            $product->setCostFromProduct($ad_product);
            $product->setAd($ad);
            $product->isChosen(true);
        }

        return $product;
    }

    /**
     * @return ProductsInterface[]
     */
    public function getProducts()
    {
        return $this->products;
    }

    /**
     * @return void
     */
    public function getPossibleList()
    {
        // FIXME: Create ProductsSelectorInterface and have it contain only the stuff that's needed for
        // all products selectors instead of extending the base and doing shit like this
        throw new \BadMethodCallException(__METHOD__ . ' should not be called on PushupProductsSelector');
    }

    public function getGrouped()
    {
        // FIXME: Create ProductsSelectorInterface and have it contain only the stuff that's needed for
        // all products selectors instead of extending the base and doing shit like this
        throw new \BadMethodCallException(__METHOD__ . ' should not be called on PushupProductsSelector');
    }

    public function getChosenProductsGrouped()
    {
        // FIXME: Create ProductsSelectorInterface and have it contain only the stuff that's needed for
        // all products selectors instead of extending the base and doing shit like this
        throw new \BadMethodCallException(__METHOD__ . ' should not be called on PushupProductsSelector');
    }
}
