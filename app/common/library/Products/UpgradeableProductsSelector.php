<?php

namespace Baseapp\Library\Products;

use Baseapp\Library\Debug;
use Baseapp\Models\Ads;
use Baseapp\Models\Users;

class UpgradeableProductsSelector extends OrderableEnabledProductsSelector
{
    protected $ad;
    protected $preset_products;

    protected $originally_selected_option = null;

    public function __construct(Ads $ad, array &$post_data = null, array $preset_products = null)
    {
        $this->ad        = $ad;
        $this->category  = $ad->getCategory();
        $this->user      = $ad->getUser();
        $this->post_data = $post_data;

        $this->storeOriginallySelectedOption();

        $this->setPresetProducts($preset_products);

        $this->process();
    }

    public function storeOriginallySelectedOption()
    {
        $online_product = $this->ad->getProduct('online');
        if ($online_product) {
            $this->originally_selected_option = $online_product->getSelectedOption();
        }
    }

    public function getStoredOriginallySelectedOption()
    {
        return $this->originally_selected_option;
    }

    public function setPresetProducts(array $preset_products = null)
    {
        $this->preset_products = $preset_products;

        // Reformat the array so it's keyed on fqcn
        if (!empty($this->preset_products)) {
            $keyed = array();
            foreach ($this->preset_products as &$p) {
                $keyed[$p->getFqcn()] = $p;
            }
            unset($p);
            $this->preset_products = $keyed;
        }
    }

    public function getPresetProducts()
    {
        return $this->preset_products;
    }

    /**
     * Overridden to provide only paid-for products
     *
     * @return array
     */
    public function getPossibleList()
    {
        $product_list = array(
            'Online' => array(
                'IstaknutiOnline',
                'IstaknutiNoExtras',
                'Premium',
                'PremiumNoExtras',
                // 'Pinky'
            ),
            'Offline' => array(
                // 'MaliPrivatni',
                'MaliFoto',
                'MaliBold',
                'IstaknutiOffline',
                'IstaknutiBoja'
            )
        );

        // Prepend 'MaliPoslovni' to 'Offline' group for company users
        if (Users::TYPE_COMPANY == $this->user->type) {
            array_unshift($product_list['Offline'], 'MaliPoslovni');
        }

        return $product_list;
    }

    /**
     * Overridden implementation supporting our slightly different use case
     */
    public function process()
    {
        // Used to check which base product is currently active and to set it as free if not expired
        $current_online_product_fqcn = null;
        $current_online_product      = $this->ad->getProduct('online');
        if ($current_online_product) {
            $current_online_product_fqcn = $current_online_product->getFqcn();
        }

        $has_ad_product_expired = $this->ad->isExpired();

        // echo \Baseapp\Library\Debug::dump($preset_hash);
        // exit;

        // Go through all the possible products, mark any pre-set products, and modify stuff for our specific upgrade use-cases
        foreach ($this->getPossibleList() as $group => $classes) {
            $group_lowercase = strtolower($group);

            foreach ($classes as $class_name) {
                // Create a default instance
                $product = $this->createProduct($group, $class_name);
                $fqcn    = $product->getFqcn();

                // Skip offering the product that the Ad currently has
                if ('online' === $group_lowercase && ! $has_ad_product_expired && $fqcn === $current_online_product_fqcn) {
                    continue;
                }

                // If we have a pre-set product matching this one, replace the defaultly-created instance with a pre-set one
                if (empty($this->post_data) && isset($this->preset_products[$fqcn])) {
                    /** @var ProductsInterface $product */
                    $product = $this->preset_products[$fqcn];
                    // Make sure the preselected product gets fresh prices
                    $product->configureByCategory($this->category);
                    $product->isChosen(true);
                }

                /**
                 * If/when we encounter the current Ad's product, make sure to insert another
                 * price option (and pre-select it) which makes the base product free (if it's still valid, that is,
                 * not expired). This allows the user to purchase additional extras on it without
                 * paying for the base thing again.
                 */
                /*
                if ('online' === $group_lowercase && ! $has_ad_product_expired && $fqcn === $current_online_product_fqcn) {
                    // Get remaining number of days
                    $days_until_expiry = $this->ad->getDaysUntilExpiry();
                    if ($days_until_expiry) {
                        // $days_until_expiry_label = 'Do isteka (' . $days_until_expiry . ' dana)';
                        $product->setSelectedOptionBackup($this->getStoredOriginallySelectedOption());
                        // Insert the new option and mark it se selected
                        $product->insertOptionIfNotExists('Do isteka', 0, true);
                    }

                    // Make sure to not offer extras that would go past the Ad's (ad product's really) expiry date
                    if ($product->hasExtras()) {
                        $extras          = $product->getExtras();
                        $selected_extras = $product->getSelectedExtras();
                        $extras_modified = false;
                        foreach ($extras as $extras_group => $pricing) {
                            if (is_array($pricing)) {
                                foreach ($pricing as $price_key => $price_in_lipas) {
                                    $extra_duration_days = $product->getDurationDaysFromString($price_key);
                                    if ($extra_duration_days > $days_until_expiry) {
                                        // Remove the extra since it would be past the expiry date
                                        unset($extras[$extras_group][$price_key]);
                                        $extras_modified = true;
                                        // Make sure to clear any such selected extras too
                                        if (isset($selected_extras[$extras_group]) && $selected_extras[$extras_group] == $price_key) {
                                            $product->unsetSelectedExtras($extras_group);
                                        }
                                    }
                                }
                            }
                        }
                        // We've changed extras, reflect that
                        if ($extras_modified) {
                            $product->setExtras($extras);
                        }
                    }
                }
                */

                // Handle posted data
                if (!empty($this->post_data)) {
                    $product->selectViaPost($this->post_data);
                }

                // Add to internal list
                $this->addProduct($product, $group_lowercase);

                // Add to internal chosen list if chosen
                if ($product->isChosen()) {
                    $this->addChosenProduct($product, $group_lowercase);
                }
            }
        }

        $this->ensureAtLeastOneChosenProduct();
    }

    /**
     * Overridden variant which avoids the problem of preselectViaPost() being called
     * automatically in Products\Base::__construct() (which causes problems since 'Do isteka' is not
     * a valid option at construction time, but only exists later when we force-create it).
     *
     * @param string $group
     * @param string $class_name
     *
     * @return ProductsInterface
     */
    protected function createProduct($group, $class_name)
    {
        $fq_class_name = $this->buildFullClassName($group, $class_name);
        /* @var $product ProductsInterface */
        $product = new $fq_class_name($this->category, $this->user);

        return $product;
    }
}
