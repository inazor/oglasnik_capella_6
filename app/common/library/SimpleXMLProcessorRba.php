<?php

namespace Baseapp\Library;

use Baseapp\Models\Orders;
use Phalcon\Mvc\Model\Resultset;

class SimpleXMLProcessorRba extends SimpleXMLReader
{
    protected $called = false;
    protected $warnings = array();
    protected $completed = array();
    protected $warnings_for_export = array();

    protected $data = array();

    protected $stats = array(
        'elements'       => 0,
        'pbo_matches'    => 0,
        'pbo_duplicates' => 0,
        'completed'      => 0
    );

    public function __construct()
    {
        // By node name
        // $this->addCallback('promet', array($this, 'callbackPromet'));

        // By xpath
        // $this->addCallback('/root/izvod/dan/promet', array($this, 'callbackPromet'));

        // Xpath for the new ISO 20022-based xml bank statement file
        $this->addCallback('/Document/BkToCstmrStmt/Stmt/Ntry', array($this, 'callbackNtry'));
    }

    public function callbackPromet(SimpleXMLReader $reader)
    {
        $this->called = true;
        $this->stats['elements']++;

        $xml = $reader->expandSimpleXml();
        $pbo = (string) $xml->pbo;

        if (Utils::str_has_pbo_prefix($pbo)) {
            $this->stats['pbo_matches']++;

            $total    = (string) $xml->iznos;
            $total_lp = Utils::kn2lp($total);

            // Store what appears to be a prefix-matched pbo we might be interested in
            // while skipping duplicates (those already encountered in the same file)
            if (!isset($this->data[$pbo])) {
                $this->data[$pbo] = array(
                    'total'    => $total,
                    'total_lp' => $total_lp,
                    'pbo'      => $pbo,
                );
            } else {
                $this->stats['pbo_duplicates']++;
            }
        }

        // A callback has to return true in order for others to continue
        return true;
    }

    public function callbackNtry(SimpleXMLReader $reader)
    {
        $this->called = true;
        $this->stats['elements']++;

        $xml = $reader->expandSimpleXml();

        $pbo_full = null;

        // zyt: 26.07.2016.
        // RBA appears to have moved things around a bit (or something).
        // Either way, the PBO we care about is now in <Ntry> that has a child
        // element of <CdtDbtInd> whose text value is CRDT. Once we find those,
        // we look for <NtryDtls><TxDtls><Refs><RmtInf><Strd><CdtrRefInf><Ref> el,
        // or in xpath terms: NtryDtls/TxDtls/RmtInf/Strd/CdtrRefInf/Ref
        // Note:
        // Bank had kept just 'HR99' for example in the path above during June 2016 (
        // and our PBO was in <EndToEndId>), but at some point in July they seem to have
        // changed it.
        $is_crdt = (isset($xml->CdtDbtInd) && ('CRDT' === (string) $xml->CdtDbtInd));
        if ($is_crdt) {
            $pbo_full = (string) $xml->NtryDtls->TxDtls->RmtInf->Strd->CdtrRefInf->Ref;

            // TODO/FIXME: We could test if whatever we got above is shorter than 4 chars
            // and then try the older path, but it might not be needed at all for now, and
            // they'll probably change shit again soon anyway...
        } else {
            // Bail early if not the right type of <Ntry>
            // All callbacks must return true for others to proceed
            return true;
        }

        // Cleanup our PBO so we can match it (if we're the right type of <Ntry>)
        $pbo = null;
        if (!empty($pbo_full)) {
            // Strips commonly spotted prefixes which appeared in the new xml format
            $pbo = trim(str_replace(array('HR00', 'HR ', 'HR'), '', $pbo_full));
            // error_log($pbo_full . ' -> ' . $pbo);
        }

        // Proceed if we have something that matches our prefix
        if (!empty($pbo) && Utils::str_has_pbo_prefix($pbo)) {
            $this->stats['pbo_matches']++;

            $total    = (string) $xml->Amt;
            $total_lp = Utils::kn2lp($total);

            // Store what appears to be a prefix-matched pbo we might be interested in
            // while skipping duplicates (those already encountered in the same file)
            if (!isset($this->data[$pbo])) {
                $this->data[$pbo] = array(
                    'total'    => $total,
                    'total_lp' => $total_lp,
                    'pbo'      => $pbo,
                );
            } else {
                $this->stats['pbo_duplicates']++;
            }
        }

        // A callback has to return true in order for others to continue
        return true;
    }

    /**
     * Process payment data collected during XML parsing stage (via callback) using
     * a single query to fetch all the matched orders and then iterating over the
     * resultset (as opposed to querying each single Order as we encounter it)
     */
    public function processFoundPbos()
    {
        // Prepare an array mapping our actual order ids with matching xml payment data
        $xml_orders = array();
        foreach ($this->getData() as $pbo => $xml_order_data) {
            $order_id = (int) Utils::str_remove_pbo_prefix($pbo);
            $xml_orders[$order_id] = $xml_order_data;
        }

        // Bail if we have nothing to work with
        if (empty($xml_orders)) {
            return;
        }

        // Query and process each Order separately
        $order_ids = array_keys($xml_orders);
        foreach ($order_ids as $order_id) {
            $order = Orders::findFirst($order_id);
            if (!$order) {
                // No such Order here for whatever reason
                $xml_pbo = $xml_orders[$order_id]['pbo'];
                // Matching output of $order->ident() for logging/audit purposes
                $error_msg                   = 'Order #' . $order_id . ' (PBO: ' . $xml_pbo . ') does not exist in our DB';
                $this->warnings[]            = $error_msg;
                $this->warnings_for_export[] = array(
                    'ID'  => $order_id,
                    'PBO' => $xml_pbo,
                    'MSG' => $error_msg
                );
            } else {
                // We've found something, process it
                $this->processFoundOrder($order, $xml_orders[$order_id]);
            }
        }
    }

    /**
     * @param Orders $order
     * @param array $xml_data
     *
     * @return bool
     */
    protected function processFoundOrder(Orders $order, $xml_data = array())
    {
        $ident = $order->ident();

        // Should we finalize or not (modified as certain checks pass/fail)
        $finalize = true;

        // Check if status allows changes at all
        if (!($order->isStatusChangeable())) {
            $warning_msg                 = $ident . ' is not changeable (status=' . $order->getStatusText() . ')';
            $this->warnings[]            = $warning_msg;
            $this->warnings_for_export[] = array(
                'ID'  => $order->id,
                'PBO' => $order->getPbo(),
                'MSG' => $warning_msg
            );
            // Don't even bother attempting to finalize such entities
            $finalize = false;
        }

        // Check that payment amount matches the required amount
        $xml_total    = $xml_data['total'];
        $xml_total_lp = $xml_data['total_lp'];
        if ($order->getTotalRaw() !== $xml_total_lp) {
            $warning_msg                 = $ident . ' - payment amount mismatch (payment: ' . $xml_total . ', required: ' . $order->getTotal() . ')';
            $this->warnings[]            = $warning_msg;
            $this->warnings_for_export[] = array(
                'ID'  => $order->id,
                'PBO' => $order->getPbo(),
                'MSG' => $warning_msg
            );
            // No need to even attempt finalizing these
            $finalize = false;
        }

        // Finalize the order/purchase if various conditions above were met
        $success = false;
        if ($finalize) {
            $finalized = $order->finalizePurchase();
            // Collect data about success/failure for log/audit purposes
            if ($finalized) {
                $this->stats['completed']++;
                $completed_msg     = $ident . ' successfully finalized';
                $this->completed[] = $completed_msg;
            } else {
                $error_msg  = $ident . ' failed to finalize/complete';
                $error_data = $order->getMessages();
                if (!empty($error_data)) {
                    $error_data = var_export($error_data, true);
                }
                if (!empty($error_data)) {
                    $error_msg .= ' (error data: ' . $error_data . ')';
                }
                $this->warnings[]            = $error_msg;
                $this->warnings_for_export[] = array(
                    'ID'  => $order->id,
                    'PBO' => $order->getPbo(),
                    'MSG' => $error_msg
                );
            }
            $success = $finalized;
        }

        return $success;
    }

    public function called()
    {
        return $this->called;
    }

    public function getWarnings($for_export = false)
    {
        return $for_export ? $this->warnings_for_export : $this->warnings;
    }

    public function getCompletedMessages()
    {
        return $this->completed;
    }

    public function getStats()
    {
        return $this->stats;
    }

    public function getData()
    {
        return $this->data;
    }
}
