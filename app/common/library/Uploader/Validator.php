<?php

namespace Baseapp\Library\Uploader;

use Baseapp\Library\Utils;
use Phalcon\Http\Request\File;

class Validator
{
    const MESSAGE_INVALID_MIN_SIZE = 'The file `%s` file is too small. Minimum allowed is `%s`';
    const MESSAGE_INVALID_MAX_SIZE = 'The file `%s` is too big. Maximum allowed is `%s`';
    const MESSAGE_INVALID_EXTENSION = 'File `%s` is not allowed. Allowed extensions: `%s`';
    const MESSAGE_INVALID_MIME_TYPE = 'File `%s` is not allowed. Allowed mime types: `%s`';
    const MESSAGE_INVALID_UPLOAD_DIR = 'Directory `%s` does not exist';
    const MESSAGE_INVALID_UPLOAD_DIR_PERMISSIONS = 'Directory `%s` is not writable';
    const MESSAGE_NO_FILE_UPLOADED = 'No file was uploaded';
    const MESSAGE_UNKNOWN_PHP_CORE_ERROR = 'Unknown PHP core error occured';

    /**
     * @var array
     */
    public $errors = [];

    /**
     * Check minimum file size
     *
     * @param File $file
     * @param $value
     *
     * @return bool
     */
    public function checkMinsize(File $file, $value)
    {
        if (is_array($value)) {
            $value = $value[key($value)];
        }

        if ($file->getSize() < (int) $value) {
            $this->errors[] = sprintf(static::MESSAGE_INVALID_MIN_SIZE, $file->getName(), Utils::format_filesize($value));
            return false;
        }

        return true;
    }

    /**
     * Check maximum file size
     *
     * @param File $file
     * @param $value
     *
     * @return bool
     */
    public function checkMaxsize(File $file, $value)
    {
        if (is_array($value)) {
            $value = $value[key($value)];
        }

        if ($file->getSize() > (int) $value) {
            $this->errors[] = sprintf(static::MESSAGE_INVALID_MAX_SIZE, $file->getName(), Utils::format_filesize($value));
            return false;
        }

        return true;
    }

    /**
     * Check allowed extensions
     *
     * @param File $file
     * @param $value
     *
     * @return bool
     */
    public function checkExtensions(File $file, $value)
    {
        if (!is_array($value)) {
            $value = [$value];
        }

        if (!in_array(strtolower($file->getExtension()), $value)) {
            $this->errors[] = sprintf(static::MESSAGE_INVALID_EXTENSION, $file->getName(), implode(',', $value));
            return false;
        }

        return true;
    }

    /**
     * Check allowed mime types
     *
     * @param File $file
     * @param mixed $value
     *
     * @return bool
     */
    public function checkMimes(File $file, $value)
    {
        if (!is_array($value)) {
            $value = [$value];
        }

        if (!in_array($file->getRealType(), $value)) {
            $this->errors[] = sprintf(static::MESSAGE_INVALID_MIME_TYPE, $file->getName(), implode(',', $value));
            return false;
        }

        return true;
    }

    /**
     * Check upload directory
     *
     * @param File|null $file
     * @param $value
     *
     * @return bool
     */
    public function checkDirectory(File $file = null, $value)
    {
        if (is_array($value)) {
            $value = $value[key($value)];
        }

        if (!file_exists($value)) {
            if (!$rs = @mkdir($value, 0777, true)) {
                // Failed creating the directory
                $this->errors[] = sprintf(static::MESSAGE_INVALID_UPLOAD_DIR, $value);
                return false;
            }
        }

        if (!is_writable($value)) {
            $this->errors[] = sprintf(static::MESSAGE_INVALID_UPLOAD_DIR_PERMISSIONS, $value);
            return false;
        }

        return true;
    }

    public function checkPhpErrors(File $file)
    {
        $result = true;
        $error_code = $file->getError();

        if (UPLOAD_ERR_OK != $error_code) {
            switch ($error_code) {
                // TODO/FIXME: implement other common error codes/messages
                case UPLOAD_ERR_NO_FILE:
                    $message = static::MESSAGE_NO_FILE_UPLOADED;
                    break;
                default:
                    $message = static::MESSAGE_UNKNOWN_PHP_CORE_ERROR;
                    break;
            }

            $this->errors[] = $message;

            $result = false;
        }

        return $result;
    }
}
