<?php

namespace Baseapp\Library\Validations;

use Baseapp\Extension\Validation;
use Phalcon\Validation\Validator\Email as EmailValidator;
use Phalcon\Validation\Validator\PresenceOf;

class EmailShareForm
{
    public function __construct() {
        $this->validation = new Validation();
        $this->setup();
    }

    public function setup()
    {
        $this->validation->add('sender_email', new PresenceOf(array(
            'message' => 'Molimo popunite svoju e-mail adresu'
        )));

        $this->validation->add('sender_email', new EmailValidator(array(
            'message' => 'Neispravna e-mail adresa'
        )));

        $this->validation->add('recipients', new PresenceOf(array(
            'message' => 'Molimo upisati bar jednu email adresu'
        )));

        $this->validation->setLabels(array(
            'sender_email' => 'Vaša e-mail adresa',
            'recipients' => 'Primatelji',
        ));
    }

    public function add($field, $validator)
    {
        $this->validation->add($field, $validator);
    }

    public function get()
    {
        return $this->validation;
    }

    public function validate($data)
    {
        return $this->get()->validate($data);
    }

    public function getMessages() {
        return $this->validation->getMessages();
    }
}
