<?php

namespace Baseapp\Library\Validations;

class Products
{

    protected $unique = array('name');

    public function __construct() {
        $this->validation = new \Baseapp\Extension\Validation();
        $this->setup();
    }

    public function setup()
    {
        $this->validation->add('name', new \Phalcon\Validation\Validator\PresenceOf(array(
            'message' => 'Name cannot be empty'
        )));

        $this->validation->add('default_expire_time', new \Phalcon\Validation\Validator\PresenceOf(array(
            'message' => 'Default expire times cannot be empty'
        )));

        $this->validation->setLabels(array(
            'name' => 'Name',
            'default_expire_time' => 'Default expire times',
            'type' => 'Type'
        ));
    }

    public function add($field, $validator)
    {
        $this->validation->add($field, $validator);
    }

    public function set_unique($unique) {
        $this->unique = $unique;
    }

    public function get()
    {
        $this->add_uniques();
        return $this->validation;
    }

    private function add_uniques() {
        if (is_array($this->unique) && !empty($this->unique)) {
            foreach ($this->unique as $field_name) {
                $this->validation->add($field_name, new \Baseapp\Extension\Validator\Uniqueness(array(
                    'model' => '\Baseapp\Models\Products',
                    'message' => ucfirst($field_name) . ' already exists'
                )));
            }
        }
    }

    public function validate($data)
    {
        return $this->get()->validate($data);
    }

    public function getMessages() {
        return $this->validation->getMessages();
    }

}
