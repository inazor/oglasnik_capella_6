<?php

namespace Baseapp\Library\Validations;

class UsersFrontendCompany extends UsersFrontend
{
    public function setup()
    {
        // Company validations are in addition to those of "regular" users
        parent::setup();

        // oib
        $this->validation->add('oib', new \Phalcon\Validation\Validator\PresenceOf(array(
            'message' => 'Molimo upišite OIB pravne osobe'
        )));
        $this->validation->add('oib', new \Baseapp\Extension\Validator\OibSimple(array(
            'message' => 'Upisani OIB nema traženi broj znamenki',
            'messageDigit' => 'Upisani OIB trebao bi se sastojati samo od brojki'
        )));
        // Avoids duplicate OIBs
        $this->validation->add('oib', new \Baseapp\Extension\Validator\Uniqueness(array(
            'model' => '\Baseapp\Models\Users',
            'message' => 'Već postoji poslovni subjekt s upisanim OIB-om'
        )));

        // TODO: do we need to know if a company is VAT-exempt or not? A checkbox or something? (competition has it it seems, not really sure why)

        $fields = array(
            'first_name' => 'Ime odgovorne osobe',
            'last_name' => 'Prezime odgovorne osobe',
            'company_name' => 'Naziv poslovnog subjekta',
            'birth_date' => 'Datum osnivanja',
            'phone1' => 'Telefon #1',
            'country_id' => 'Država',
            'zip_code' => 'Poštanski broj',
            'city' => 'Grad',
            'address' => 'Adresa'
        );
        foreach ($fields as $field => $label) {
            $this->validation->add($field, new \Phalcon\Validation\Validator\PresenceOf(array(
                'message' => $label . ' je neophodno polje'
            )));
        }

        $labels = array(
            'oib' => 'OIB',
            'phone2' => 'Telefon #2'
        );
        $labels = array_merge($fields, $labels);

        $this->validation->setLabels($labels);
    }
}
