<?php

namespace Baseapp\Library\Validations;


class UsersFrontendEdit extends Users
{
    protected $unique = array('oib');

    public function setup()
    {
        $this->validation->add('first_name', new \Phalcon\Validation\Validator\PresenceOf(array(
            'message' => 'Molimo upišite Vaše ime'
        )));
        $this->validation->add('last_name', new \Phalcon\Validation\Validator\PresenceOf(array(
            'message' => 'Molimo upišite Vaše prezime'
        )));

        // oib
        $this->validation->add('oib', new \Phalcon\Validation\Validator\PresenceOf(array(
            'message' => 'Molimo upišite vaš OIB'
        )));
        $this->validation->add('oib', new \Baseapp\Extension\Validator\OibSimple(array(
            'message' => 'Upisani OIB nema traženi broj znamenki',
            'messageDigit' => 'Upisani OIB trebao bi se sastojati samo od brojki'
        )));

        $this->validation->setLabels(array(
            'first_name' => 'Ime',
            'last_name' => 'Prezime',
            'oib' => 'OIB'
        ));
    }

    public function set_unique($unique) {
        $this->unique = $unique;
    }

    public function get()
    {
        $this->add_uniques();
        return $this->validation;
    }

    private function add_uniques() {
        if (is_array($this->unique) && !empty($this->unique)) {
            foreach ($this->unique as $field_name) {
                $this->validation->add($field_name, new \Baseapp\Extension\Validator\Uniqueness(array(
                    'model' => self::MODEL,
                    'message' => ucfirst($field_name) . ' - korisnik već postoji'
                )));
            }
        }
    }

}
