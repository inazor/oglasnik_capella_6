<?php

namespace Baseapp\Library\Validations;

use Baseapp\Extension\Validation;

abstract class UsersShops
{
    const MODEL = '\Baseapp\Models\UsersShops';

    protected $unique = array('slug');

    public function __construct() {
        $this->validation = new Validation();
        $this->setup();
    }

    // validation implementations in inherited/concrete classes should reside here
    abstract public function setup();

    public function set_unique($unique) {
        $this->unique = $unique;
    }

    /**
     * @return \Baseapp\Extension\Validation
     */
    public function get()
    {
        $this->add_uniques();
        return $this->validation;
    }

    /**
     * @return \Phalcon\Validation\Message\Group
     */
    public function getMessages()
    {
        return $this->get()->getMessages();
    }

    /**
     * @param $data
     *
     * @return \Phalcon\Validation\Message\Group
     */
    public function validate($data)
    {
        return $this->get()->validate($data);
    }
}
