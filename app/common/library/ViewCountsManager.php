<?php

namespace Baseapp\Library;

use Phalcon\Mvc\User\Component;

class ViewCountsManager extends Component
{
    const GROUP               = 'vcm';
    const NAME_ALLKEYS        = 'vcm_cached_key_names';
    const CACHE_KEY_SEPARATOR = '.';

    private $cookie = array(
        'exists'           => false,
        'visited_listings' => array(),
        'expiration'       => 0
    );

    /**
     * The manager should be constructed early on during the request
     */
    public function __construct()
    {
        // Checks/sets our private cookie array which we later rely on 1);
        $this->checkCookie();
    }

    /**
     * Initializes our internal cookie store if our view tracking cookie is present in the request
     */
    public function checkCookie()
    {
        // Is our cookie set?
        $has_cookie = $this->cookies->has('ogl_visits');
        if ($has_cookie) {
            $cookie_visits    = $this->cookies->get('ogl_visits')->getValue();
            $visited_listings = $expirations = array();

            foreach ($cookie_visits as $content) {
                // Valid cookie?
                if (preg_match('/^(([0-9]+b[0-9]+a?)+)$/', $content) === 1) {
                    // Gets single id with expiration
                    $expiration_ids = explode('a', $content);

                    // Checks every expiration => id pair
                    foreach ($expiration_ids as $pair) {
                        $pair = explode('b', $pair);
                        $expirations[] = (int) $pair[0];
                        $visited_listings[(int) $pair[1]] = (int) $pair[0];
                    }
                }
            }

            $this->cookie = array(
                'exists'           => true,
                'visited_listings' => $visited_listings,
                'expiration'       => max($expirations)
            );
        }
    }

    public function registerCheckRequestForShutdown($ad_id)
    {
        // Using a shutdown manager we can now control whether the callbacks should really
        // execute or not (because there are scenarios in which when we don't want them to execute, i.e. PHPUnit tests)
        $di = $this->getDI();
        if ($di->has('shutdownManager')) {
            $sm = $di->get('shutdownManager');
            if ($sm) {
                $sm->append(array($this, 'checkRequest'), $ad_id);
            }
        }
    }

    /**
     * Deletes view count data for the given Ad id
     *
     * @param int $id
     *
     * @return bool
     */
    public function deleteByAdId($id)
    {
        /**
         * @var $db \Phalcon\Db\AdapterInterface
         */
        $db = $this->getDI()->getShared('db');
        return $db->execute("DELETE FROM ads_views WHERE ad_id = " . (int) $id);
    }

    /**
     * Returns a timestamp for the specified $number of minutes/hours/days/weeks/months/years from now
     *
     * @param $type
     * @param $number
     * @param bool $timestamp
     *
     * @return int
     */
    public function getTimestamp($type, $number, $timestamp = true)
    {
        $converter = array(
            'minutes' => 60,
            'hours' => 3600,
            'days' => 86400,
            'weeks' => 604800,
            'months' => 2592000,
            'years' => 946080000
        );

        return ($timestamp ? time() : 0) + $number * $converter[$type];
    }

    /**
     * @param array|int $id
     *
     * @return int
     */
    private function normalizeId($id)
    {
        // If an array is received in $id, grab the first element of the array
        // TODO: fix this, it's coming from the way ShutdownManager is handling callback params
        if (is_array($id) && isset($id[0]) && !empty($id[0])) {
            $id = $id[0];
        }

        $id = (int) $id;

        return $id;
    }

    public function getTotalCount($id)
    {
        $id = $this->normalizeId($id);

        $views_sql = "SELECT `count` AS views FROM ads_views WHERE ad_id = " . $id . " AND type = 4";

        /**
         * @var $db \Phalcon\Db\AdapterInterface
         */
        $db = $this->getDI()->getShared('db');
        $result = $db->query($views_sql);
        $result->setFetchMode(\Phalcon\Db::FETCH_BOTH);

        $results = $result->fetchAll();
        $views   = isset($results[0][0]) ? (int) $results[0][0] : 0;

        return $views;
    }

    public function getTotalCountMany(array $ids)
    {
        $views_sql = "SELECT ad_id, `count` AS views FROM ads_views WHERE ad_id IN (" . implode(', ', $ids ) . ") AND type = 4";

        /**
         * @var $db \Phalcon\Db\AdapterInterface
         */
        $db = $this->getDI()->getShared('db');
        $result = $db->query($views_sql);
        $result->setFetchMode(\Phalcon\Db::FETCH_BOTH);

        $results = $result->fetchAll();

        return $results;
    }

    /**
     * Main method to call when a request for an ad is made.
     * Called from frontend Baseapp\Frontend\Controllers\AdsController::viewAction()
     *
     * @param int $ad_id
     */
    public function checkRequest($ad_id)
    {
        $ad_id = $this->normalizeId($ad_id);

        if (!$ad_id) {
            return;
        }

        // If it's a robot, we skip counting
/*
        if ($this->isRobot()) {
            return;
        }
*/

        // Cookie already exists?
        if ($this->cookie['exists']) {
            // Ad already viewed but not expired?
            if (in_array($ad_id, array_keys($this->cookie['visited_listings']), true) && time() < $this->cookie['visited_listings'][$ad_id]) {
                // Updates cookie but skips counting the visit
                $this->saveCookie($ad_id, $this->cookie, false);
                return;
            } else {
                // Updates cookie
                $this->saveCookie($ad_id, $this->cookie);
            }
        } else {
            // Sets new cookie
            $this->saveCookie($ad_id);
        }

        // Count visit
        $this->countVisit($ad_id);
    }

    /**
     * @return null|false|\Phalcon\Cache\BackendInterface
     */
    private function getCacheBackend()
    {
        static $backend = null;

        // Only get it once per request
        if (null === $backend) {
            $di           = $this->getDI();
            $config       = $di->getShared('config');
            $service_name = $config->viewCounts->options['cacheBackend'];
            if ($di->has($service_name)) {
                /**
                 * @var $backend \Phalcon\Cache\BackendInterface
                 */
                $backend = $di->getShared($service_name);
                if (null === $backend) {
                    $backend = false;
                }
            }
        }

        return $backend;
    }

    /**
     * @param $id
     * @param array $cookie
     * @param bool $expired
     */
    private function saveCookie($id, $cookie = array(), $expired = true)
    {
        // Calculate the expiration timestamp based on config values
        $expiration = $this->getTimestamp(
            // Seconds/minutes/hours/days/weeks/months/years
            $this->config->viewCounts->options['visitDurationUnit'],
            // Number of units above
            $this->config->viewCounts->options['visitDurationNumber']
        );

        // Is it a new cookie?
        if (empty($cookie)) {
            // No current cookie values specified, set our stuff
            $this->cookies->set('ogl_visits[0]', $expiration . 'b' . $id, $expiration);
        } else {
            // Existing cookie, modifying stored values and checking expirations

            // Adds new id or changes expiration date if id already exists
            if ($expired) {
                $cookie['visited_listings'][$id] = $expiration;
            }

            // Creates copy for better foreach performance
            $visited_listings_expirations = $cookie['visited_listings'];

            // Gets current gmt time
            $time = time();

            // Checks whether viewed id has expired - no need to keep it in cookie (reduces cookie size)
            foreach ($visited_listings_expirations as $ad_id => $expiry) {
                if ($time > $expiry) {
                    unset($cookie['visited_listings'][$ad_id]);
                }
            }

            // Sets new last expiration date if needed
            $cookie['expiration'] = max($cookie['visited_listings']);

            $cookies = $imploded = array();

            // Creates pairs
            foreach ($cookie['visited_listings'] as $id => $exp) {
                $imploded[] = $exp . 'b' . $id;
            }

            // Splits cookie into chunks (4000 bytes to make sure it is safe for every browser)
            $chunks = str_split(implode('a', $imploded), 4000);

            // More then one chunk?
            if (count($chunks) > 1) {
                $last_id = '';

                foreach ($chunks as $chunk_id => $chunk) {
                    // New chunk
                    $chunk_c = $last_id.$chunk;

                    // Is it a full-length chunk?
                    if (4000 === strlen($chunk)) {
                        // Gets last part
                        $last_part = strrchr($chunk_c, 'a');

                        // Gets last id
                        $last_id = substr($last_part, 1);

                        // Adds new full-length chunk
                        $cookies[$chunk_id] = substr($chunk_c, 0, strlen($chunk_c) - strlen($last_part));
                    } else {
                        // Adds last chunk
                        $cookies[$chunk_id] = $chunk_c;
                    }
                }
            } else {
                // Only one chunk
                $cookies[] = $chunks[0];
            }

            // Set needed number of cookies
            foreach ($cookies as $key => $value) {
                $this->cookies->set('ogl_visits[' . $key . ']', $value, $cookie['expiration']);
            }
        }
    }

    /**
     * @param null $enabled
     *
     * @return bool
     */
    public function cachingEnabled($enabled = null)
    {
        if (null === $enabled) {
            $enabled = $this->getCacheBackend();
        } else {
            $enabled = false;
        }

        // If available (we have the cache service), check if it's explicitly disabled by flushInterval=0 config value
        if ($enabled) {
            $di       = $this->getDI();
            $config   = $di->getShared('config');
            $interval = isset($config->viewCounts->options['flushInterval']) ? $config->viewCounts->options['flushInterval'] : 0;
            $enabled  = ($interval <= 0) ? false : true;
        }

        return $enabled;
    }

    public function countVisit($id, $count = 1)
    {
        $cache_key_names = array();
        $using_cache     = $this->cachingEnabled();

        if ($using_cache) {
            $cache = $this->getCacheBackend();
        }

        // Gets day, week, month and year
        $date = explode('-', date('W-d-m-Y'));

        foreach(array(
            0 => $date[3].$date[2].$date[1], // day like 20140324
            1 => $date[3].$date[0],          // week like 201439
            2 => $date[3].$date[2],          // month like 201405
            3 => $date[3],                   // year like 2014
            4 => 'total'                     // total views
        ) as $type => $period)
        {
            if ($using_cache) {
                // TODO: add a wrapper for building key names which will take group into account
                $cache_key = $id . self::CACHE_KEY_SEPARATOR . $type . self::CACHE_KEY_SEPARATOR . $period;
                // TODO: replace with add() when/if available, avoiding a race condition
                /*
                $exists = $cache->exists($cache_key);
                if (!$exists) {
                    $cache->save($cache_key, 0);
                }
                */
                $cache->add($cache_key, 0);
                $cache->increment($cache_key, 1);
                $cache_key_names[] = $cache_key;
            } else {
                // Hit the db directly
                // TODO: investigate queueing these queries on the 'shutdown' hook instead of running them instantly?
                $this->dbInsert($id, $type, $period, 1);
            }
        }

        // Update the list of cache keys to be flushed
        if ($using_cache && !empty($cache_key_names)) {
            $this->updateCachedKeysListIfNeeded($cache_key_names);
        }
    }

    /**
     * Updates the single cache key which holds a list of all the cache keys
     * that need to be flushed to the db.
     *
     * The value of that special cache key is a giant string containing key names separated with the `|` character.
     * Each such key name then consists of 3 elements: $id, $type, $period (separated by a `.` character).
     * Examples:
     * 62053.0.20150327|62053.1.201513|62053.2.201503|62053.3.2015|62053.4.total|62180.0.20150327|62180.1.201513|62180.2.201503|62180.3.2015|62180.4.total
     * A single key is `62053.0.20150327` and that key's data is: $id = 62053, $type = 0, $period = 20150327
     *
     * This data format proved more efficient (avoids the (un)serialization overhead completely + duplicates filtering is a string search now)
     *
     * @param array $key_names
     *
     * @return bool
     */
    private function updateCachedKeysListIfNeeded($key_names = array())
    {
        $cache = $this->getCacheBackend();

        if (!$cache) {
            return false;
        }

        // TODO: cache key names/group wrapper!
        // $existing_list = $cache->get( self::NAME_ALLKEYS, self::GROUP );
        $existing_list = $cache->get(self::NAME_ALLKEYS);
        if (!$existing_list) {
            $existing_list = '';
        }

        $list_modified = false;

        // Modifying the list contents if/when needed
        if (empty($existing_list)) {
            // The simpler case of an empty initial list where we just
            // transform the specified key names into a string
            $existing_list = implode('|', $key_names);
            $list_modified = true;
        } else {
            // Searching each specified key name and appending it if it's not found
            foreach ($key_names as $key_name) {
                if (false === strpos($existing_list, $key_name)) {
                    $existing_list .= '|' . $key_name;
                    $list_modified = true;
                }
            }
        }

        // Save modified list back in cache
        if ($list_modified) {
            // wp_cache_set( self::NAME_ALLKEYS, $existing_list, self::GROUP );
            // TODO: cache key names/groups wrapper!
            $result = $cache->save(self::NAME_ALLKEYS, $existing_list);
        } else {
            $result = false;
        }

        return $result;
    }

    /**
     * Flushes views data stored in the cache down into the database and
     * clears the list of cache keys we've flushed
     */
    public function flushCacheToDb()
    {
        $cache = $this->getCacheBackend();

        if (!$cache) {
            return false;
        }

        // TODO: cache key groups/wrapper!
        // $key_names = $memcache->get( self::NAME_ALLKEYS, self::GROUP );
        $key_names = $cache->get(self::NAME_ALLKEYS);

        if (!$key_names) {
            $key_names = array();
        } else {
            // Create an array out of a string that's stored in the cache
            $key_names = explode('|', $key_names);
        }

        foreach ($key_names as $key_name) {
            // Get values stored within the key name itself
            list($id, $type, $period) = explode(self::CACHE_KEY_SEPARATOR, $key_name);

            // TODO: cache key wrapper/grouping!
            // $count = $memcache->get($key_name, self::GROUP );
            // Get the cached count value
            $count = $cache->get($key_name);

            // Store cached value in the db
            $this->dbInsert($id, $type, $period, $count);

            // Clear the cache key we just flushed
            // wp_cache_delete( $key_name, self::GROUP );
            // TODO: cache key group/wrapper!
            $cache->delete($key_name);
        }

        // Delete the key holding the list itself after we've successfully flushed it
        if (!empty($key_names)) {
            // wp_cache_delete( self::NAME_ALLKEYS, self::GROUP );
            // TODO: cache key group/wrapper!
            $cache->delete(self::NAME_ALLKEYS);
        }

        return true;
    }

    public function setTotalViewCount($id, $count)
    {
        $count = (int) $count;

        return $this->dbInsert($id, 4, 'total', $count);
    }

    private function dbInsert($id, $type, $period, $count = 1)
    {
        $count = (int) $count;
        if (!$count) {
            $count = 1;
        }

        /**
         * @var $db \Phalcon\Db\AdapterInterface
         */
        $db = $this->getDI()->getShared('db');

        $sql = "INSERT INTO `ads_views` (ad_id, `type`, `period`, `count`)
                VALUES (:ad_id, :type, :period, :count1)
                ON DUPLICATE KEY UPDATE `count` = `count` + :count2";

        $params = array(
            ':ad_id'  => $id,
            ':type'   => $type,
            ':period' => $period,
            ':count1' => $count,
            ':count2' => $count
        );

        return $db->execute($sql, $params);
    }

    /**
     * Returns true if the specified User-Agent matches a list of common bot names (case insensitive)
     *
     * @param string|null $ua Defaults to null (which checks $_SERVER['HTTP_USER_AGENT'] if available)
     *
     * @return bool
     */
    private function isRobot($ua = null)
    {
        if (null === $ua) {
            $ua = isset($_SERVER['HTTP_USER_AGENT']) ? $_SERVER['HTTP_USER_AGENT'] : null;
        }

        $ua = trim($ua);

        if (empty($ua)) {
            return false;
        }

        $robots = array(
            'bot', 'b0t', 'Acme.Spider', 'Ahoy! The Homepage Finder', 'Alkaline', 'Anthill', 'Walhello appie',
            'Arachnophilia', 'Arale', 'Araneo', 'ArchitextSpider', 'Aretha', 'ARIADNE', 'arks', 'AskJeeves',
            'ASpider (Associative Spider)', 'ATN Worldwide', 'AURESYS', 'BackRub', 'Bay Spider', 'Big Brother',
            'Bjaaland', 'BlackWidow', 'Die Blinde Kuh', 'Bloodhound', 'BSpider', 'CACTVS Chemistry Spider', 'Calif',
            'Cassandra', 'Digimarc Marcspider/CGI', 'ChristCrawler.com', 'churl', 'cIeNcIaFiCcIoN.nEt', 'CMC/0.01',
            'Collective', 'Combine System', 'Web Core / Roots', 'Cusco', 'CyberSpyder Link Test', 'CydralSpider',
            'Desert Realm Spider', 'DeWeb(c) Katalog/Index', 'DienstSpider', 'Digger', 'Direct Hit Grabber',
            'DownLoad Express', 'DWCP (Dridus\' Web Cataloging Project)', 'e-collector', 'EbiNess',
            'Emacs-w3 Search Engine', 'ananzi', 'esculapio', 'Esther', 'Evliya Celebi', 'FastCrawler', 'Felix IDE',
            'Wild Ferret Web Hopper #1, #2, #3', 'FetchRover', 'fido', 'KIT-Fireball', 'Fish search', 'Fouineur',
            'Freecrawl', 'FunnelWeb', 'gammaSpider, FocusedCrawler', 'gazz', 'GCreep', 'GetURL', 'Golem',
            'Grapnel/0.01 Experiment', 'Griffon', 'Gromit', 'Northern Light Gulliver', 'Harvest', 'havIndex',
            'HI (HTML Index) Search', 'Hometown Spider Pro', 'ht://Dig', 'HTMLgobble', 'Hyper-Decontextualizer',
            'IBM_Planetwide', 'Popular Iconoclast', 'Ingrid', 'Imagelock', 'IncyWincy', 'Informant',
            'Infoseek Sidewinder', 'InfoSpiders', 'Inspector Web', 'IntelliAgent', 'Iron33', 'Israeli-search',
            'JavaBee', 'JCrawler', 'Jeeves', 'JumpStation', 'image.kapsi.net', 'Katipo', 'KDD-Explorer', 'Kilroy',
            'LabelGrabber', 'larbin', 'legs', 'Link Validator', 'LinkScan', 'LinkWalker', 'Lockon', 'logo.gif Crawler',
            'Lycos', 'Mac WWWWorm', 'Magpie', 'marvin/infoseek', 'Mattie', 'MediaFox', 'MerzScope', 'NEC-MeshExplorer',
            'MindCrawler', 'mnoGoSearch search engine software', 'moget', 'MOMspider', 'Monster', 'Motor', 'Muncher',
            'Muninn', 'Muscat Ferret', 'Mwd.Search', 'Internet Shinchakubin', 'NDSpider', 'Nederland.zoek',
            'NetCarta WebMap Engine', 'NetMechanic', 'NetScoop', 'newscan-online', 'NHSE Web Forager', 'Nomad',
            'nzexplorer', 'ObjectsSearch', 'Occam', 'HKU WWW Octopus', 'OntoSpider', 'Openfind data gatherer',
            'Orb Search', 'Pack Rat', 'PageBoy', 'ParaSite', 'Patric', 'pegasus', 'The Peregrinator', 'PerlCrawler 1.0',
            'Phantom', 'PhpDig', 'PiltdownMan', 'Pioneer', 'html_analyzer', 'Portal Juice Spider', 'PGP Key Agent',
            'PlumtreeWebAccessor', 'Poppi', 'PortalB Spider', 'GetterroboPlus Puu', 'Raven Search', 'RBSE Spider',
            'RoadHouse Crawling System', 'ComputingSite Robi/1.0', 'RoboCrawl Spider', 'RoboFox', 'Robozilla', 'RuLeS',
            'Scooter', 'Sleek', 'Search.Aus-AU.COM', 'SearchProcess', 'Senrigan', 'SG-Scout', 'ShagSeeker', 'Shai\'Hulud',
            'Sift', 'Site Valet', 'SiteTech-Rover', 'Skymob.com', 'SLCrawler', 'Inktomi Slurp', 'Smart Spider', 'Snooper',
            'Spanner', 'Speedy Spider', 'spider_monkey', 'Spiderline Crawler', 'SpiderMan', 'SpiderView(tm)',
            'Site Searcher', 'Suke', 'suntek search engine', 'Sven', 'Sygol', 'TACH Black Widow', 'Tarantula',
            'tarspider', 'Templeton', 'TeomaTechnologies', 'TITAN', 'TitIn', 'TLSpider', 'UCSD Crawl', 'UdmSearch',
            'URL Check', 'URL Spider Pro', 'Valkyrie', 'Verticrawl', 'Victoria', 'vision-search', 'Voyager', 'W3M2',
            'WallPaper (alias crawlpaper)', 'the World Wide Web Wanderer', 'w@pSpider by wap4.com', 'WebBandit Web Spider',
            'WebCatcher', 'WebCopy', 'webfetcher', 'Webinator', 'weblayers', 'WebLinker', 'WebMirror', 'The Web Moose',
            'WebQuest', 'Digimarc MarcSpider', 'WebReaper', 'webs', 'Websnarf', 'WebSpider', 'WebVac', 'webwalk',
            'WebWalker', 'WebWatch', 'Wget', 'whatUseek Winona', 'Wired Digital', 'Weblog Monitor', 'w3mir',
            'WebStolperer', 'The Web Wombat', 'The World Wide Web Worm', 'WWWC Ver 0.2.5', 'WebZinger', 'XGET'
        );

        // TODO: profile this, it might be too slow, perhaps we should replace it
        // with a giant case-insensitive regex that we can build easily from the array above
        foreach ($robots as $robot) {
            if (stripos($_SERVER['HTTP_USER_AGENT'], $robot) !== false) {
                return true;
            }
        }

        return false;
    }
}
