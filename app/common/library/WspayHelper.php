<?php

namespace Baseapp\Library;

use Baseapp\Models\Locations;
use Baseapp\Models\Users;
use Phalcon\Mvc\User\Component;

class WspayHelper extends Component
{
    public $required_fields = array(
        'ShopID'         => null,
        'ShoppingCartID' => null,
        'TotalAmount'    => null,
        'Signature'      => null,
        'ReturnURL'      => null,
        'CancelURL'      => null,
        'ReturnErrorURL' => null,
        'PaymentPlan'    => '0000' // 00 installments, 00 months delay
    );

    public $optional_fields = array(
        'CustomerFirstName' => null,
        'CustomerLastName'  => null,
        'CustomerAddress'   => null,
        'CustomerCity'      => null,
        'CustomerZIP'       => null,
        'CustomerCountry'   => null,
        'CustomerPhone'     => null,
        'CustomerEmail'     => null,
    );

    protected $config = null;

    public function __construct($required = array(), $optional = array())
    {
        $config = $this->getDI()->getShared('config')->payment->wspay;
        if ($config) {
            $this->config = $config;
            $this->required_fields['ShopID'] = $config->shopid;
        }

        // Allows presetting some stuff via constructor params
        $this->setRequiredData($required);

        // Allows presetting optional stuff via constructor
        if ($optional instanceof Users) {
            $buyer = $optional;
            $this->optional_fields['CustomerFirstName'] = !empty($buyer->first_name) ? trim($buyer->first_name) : null;
            $this->optional_fields['CustomerLastName']  = !empty($buyer->last_name) ? trim($buyer->last_name) : null;
            $this->optional_fields['CustomerAddress']   = !empty($buyer->address) ? trim($buyer->address) : null;
            $this->optional_fields['CustomerCity']      = !empty($buyer->city) ? trim($buyer->city) : null;
            $this->optional_fields['CustomerZIP']       = !empty($buyer->zip_code) ? trim($buyer->zip_code) : null;
            if ($buyer->Country) {
                $buyers_country = $buyer->Country;
            } elseif ($buyer->country_id) {
                $buyers_country = Locations::findFirst($buyer->country_id);
            }
            if (!empty($buyers_country)) {
                $this->optional_fields['CustomerCountry'] = !empty($buyers_country->name) ? trim($buyers_country->name) : null;
            }

            $phone = !empty($buyer->phone1) ? trim($buyer->phone1) : (!empty($buyer->phone2) ? trim($buyer->phone2) : null);
            $this->optional_fields['CustomerPhone'] = $phone;
            $this->optional_fields['CustomerEmail'] = !empty($buyer->email) ? trim($buyer->email) : null;
        } elseif (is_array($optional)) {
            foreach ($optional as $k => $v) {
                $this->optional_fields[$k] = $v;
            }
        }
    }

    public function setRequiredData(array $data)
    {
        if (!is_array($data) || empty($data)) {
            return false;
        }

        foreach ($data as $k => $v) {
            if ('TotalAmount' === $k) {
                $this->setTotalAmount($v);
            } else {
                $this->required_fields[$k] = $v;
            }
        }
    }

    public function setTotalAmount($amount)
    {
        $this->required_fields['TotalAmount'] = number_format($amount, 2, ',', '');
    }

    public function getTotalAmount()
    {
        return $this->formatAmount($this->required_fields['TotalAmount']);
    }

    protected function formatAmount($amount)
    {
        $amount = intval(str_replace(',', '', trim($amount)));

        return $amount;
    }

    /**
     * Returns the Form URL where WSPay data is to be posted (depends on `env` key from config.ini)
     *
     * @return string
     */
    protected function apiURL()
    {
        if ('live' === $this->config->env || 'prod' === $this->config->env) {
            // Live environment doesn't have a /test/ path prefixed
            $env = '';
        } else {
            $env = 'test';
        }

        return 'https://form' . $env . '.wspay.biz/Authorization.aspx';
    }

    /**
     * Generate and md5 hash in WSPay format which is used to sign POST data
     *
     * @return null|string md5 signature
     */
    public function getSignature()
    {
        $signature = null;

        $shop_id          = trim($this->config->shopid);
        $secret_key       = trim($this->config->secret);
        $shopping_cart_id = trim($this->required_fields['ShoppingCartID']);
        $total_amount     = $this->getTotalAmount();

        if ($shop_id && $secret_key && $shopping_cart_id && $total_amount) {
            $signature = md5($shop_id . $secret_key . $shopping_cart_id . $secret_key . $total_amount . $secret_key);
        }

        return $signature;
    }

    /**
     * Verify signature received from WSPay
     *
     * @param array $request_data Array of data received from WSPay
     *
     * @return bool
     */
    public function verifyReturnedSignature(array $request_data = array())
    {
        $signature = null;

        $shop_id          = trim($this->config->shopid);
        $secret_key       = trim($this->config->secret);
        $shopping_cart_id = trim($request_data['ShoppingCartID']);
        $success          = intval($request_data['Success']);
        $approval_code    = trim($request_data['ApprovalCode']);
        $gotten_sig       = trim($request_data['Signature']);

        if ($gotten_sig && $shop_id && $secret_key && $shopping_cart_id && $success && $approval_code) {
            $signature = md5($shop_id . $secret_key . $shopping_cart_id . $secret_key . $success . $secret_key . $approval_code . $secret_key);
        }

        return ($signature === $gotten_sig);
    }

    public function getFormMarkup($required = null)
    {
        if (null !== $required) {
            $this->setRequiredData($required);
        }

        $this->required_fields['Signature'] = $this->getSignature();

        $fields = array_merge($this->required_fields, $this->optional_fields);

        $form = '<form name="pay" action="' . $this->apiURL() . '" method="post">';
        foreach ($fields as $name => $value) {
            if ($value) {
                $form .= '<input type="hidden" name="' . $name . '" value="' . $value . '">';
            }
        }
        $form .= '<button class="btn btn-primary btn-lg" type="submit" name="pay">Plaćanje karticom<span class="fa fa-arrow-circle-right fa-fw"></span></button>';
        $form .= '</form>';

        return $form;
    }
}
