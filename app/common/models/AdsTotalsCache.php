<?php

namespace Baseapp\Models;

use Phalcon\Di;
use Phalcon\Mvc\Model\Behavior\Timestampable;

class AdsTotalsCache extends BaseModel
{
    protected $id;

    public $url;
    public $url_hash;
    public $total;
    public $created_at;
    public $modified_at;

    public function initialize()
    {
        // No need to send entire table columns, just the ones that change
        $this->useDynamicUpdate(true);

        /**
         * If underlying tables have these columns, they'll be set automatically,
         * and if they don't, nothing happens
         */
        $options = array(
            'beforeValidationOnCreate' => array(
                'field' => 'created_at',
                'format' => 'Y-m-d H:i:s'
            ),
            'beforeValidationOnUpdate' => array(
                'field' => 'modified_at',
                'format' => 'Y-m-d H:i:s'
            )
        );

        // TODO: perhaps it would be better to make Timestampable a Trait and set it up
        // explicitly for each Model that should have it...
        $this->addBehavior(new Timestampable($options));
    }

    public function getSource()
    {
        return 'ads_totals_cache';
    }

    public static function findFirstByUrl($url)
    {
        return self::findFirst(array(
            'url = :url:',
            'bind' => array(
                'url' => $url
            )
        ));
    }

    public static function findFirstByUrlHash($hashed_url)
    {
        return self::findFirst(array(
            'url_hash = :hash:',
            'bind' => array(
                'hash' => $hashed_url
            )
        ));
    }

    public static function insertIgnoreOrUpdate($url, $total)
    {
        $url_hash = md5($url);
        $total = (int) $total;

        $db = Di::getDefault()->get('db');
        $sql = "INSERT IGNORE INTO ads_totals_cache (id, url, url_hash, total) VALUES (?, ?, ?, ?) ON DUPLICATE KEY UPDATE total=?, modified_at=CURRENT_TIMESTAMP";

        return $db->execute($sql, array(null, $url, $url_hash, $total, $total));
    }

    /**
     * @param $url
     * @param $total
     *
     * @return bool
     */
    public static function store($url, $total)
    {
        $record = self::findFirstByUrl($url);
        if (!$record) {
            $record = new self();
            $record->url = $url;
            $record->url_hash = md5($url);
        }

        $record->total = $total;

        $saved = $record->save();
/*
        if (!$saved) {
            var_dump($record->getMessages());
            exit;
        }
*/
        return $saved;
    }

    /**
     * @param $url
     *
     * @return int|string|null
     */
    public static function get($url)
    {
        $result = null;

        $record = self::findFirstByUrlHash(md5($url));
        if ($record) {
            $result = $record->total;
        }

        return $result;
    }

    /**
     * @param array $urls
     *
     * @return array
     */
    public static function getMulti(array $urls)
    {
        $hashes = array();

        foreach ($urls as $url) {
            $hashes[] = md5($url);
        }

        return self::getMultiByHashes($hashes);
    }

    /**
     * @param array $url_hashes
     *
     * @return array
     */
    public static function getMultiByHashes(array $url_hashes)
    {
        $results = array();

        // Bail early if needed otherwise PHQL complains - can't work with empty arrays
        if (empty($url_hashes)) {
            return $results;
        }

        // Workaround for PHQL AST caching bug
        // https://github.com/phalcon/cphalcon/issues/10616
        // https://github.com/phalcon/cphalcon/issues/10616#issuecomment-120658155
        // Long story short, multiple queries during the same request with the same array-named-params
        // were still reusing previously generated query ASTs, so we got "invalid param number" errors...
        // This way the names are dynamic and avoid the caching bug.
        static $calls = null;
        if (null === $calls) {
            $calls = 0;
        }
        $calls++;

        $records = self::find(array(
            'conditions' => 'url_hash IN ({hashes' . $calls . ':array})',
            'bind'       => array(
                'hashes' . $calls => $url_hashes
            )
        ));

        foreach ($records as $record) {
            $results[$record->url] = $record->total;
        }

        return $results;
    }
}
