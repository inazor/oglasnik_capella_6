<?php

namespace Baseapp\Models;

use Baseapp\Bootstrap;
use Phalcon\Cache\Exception as PhCacheException;
use Phalcon\Di;
use Phalcon\Mvc\Model\Behavior\Timestampable;
use Phalcon\Mvc\Model;
use Baseapp\Library\DateUtils;

/**
 * A BaseModel other models can inherit in order to keep code duplication
 * within models under control.
 *
 * Of course, since we're in PHP land, overridden child methods have to explicitly
 * call parent:: methods that they override, if they wish to execute code defined in the parent
 * (aka, PHP doesn't do it automatically since it would be considered 'tightly coupled')
 */

abstract class BaseModel extends Model
{
    const MEMCACHED_KEY = null;

    /**
     * An array of [$type => ['list', 'of', 'names_or_properties', 'having', 'that', 'type']].
     * Supported $type values: 'int', 'bool', 'boolint', 'date', 'string'.
     *
     * Used in `BaseModel::initialize_model_with_post()` to do rudimentary checks/sets
     * for data/fields being set/assigned via POST.
     *
     * Example:
     * ```
     * protected $fields_types_map = [
     *   'int'     => ['category_id'],
     *   'boolint' => ['active'],
     *   'date'    => ['publish_date'],
     *   'string'  => ['slug', 'title', 'excerpt', 'content', 'meta_title', 'meta_description', 'meta_keywords']
     * ];
     * ```
     *
     * @var array
     * @access protected
     * @see \Baseapp\Models\CmsArticles (`add_new()` and `save_changes()` are calling inherited `initialize_model_with_post()`)
     */
    protected $fields_types_map = array();

    protected $seo_fields_map = array(
        'title'       => 'name',
        'description' => 'excerpt'
    );

    public function initialize()
    {
        // No need to send entire table columns, just the ones that change
        $this->useDynamicUpdate(true);

        /**
         * If underlying tables have these columns, they'll be set automatically,
         * and if they don't, nothing happens
         */
        $options = array(
            'beforeValidationOnCreate' => array(
                'field' => 'created_at',
                'format' => 'Y-m-d H:i:s'
            ),
            'beforeValidationOnUpdate' => array(
                'field' => 'modified_at',
                'format' => 'Y-m-d H:i:s'
            )
        );

        // Some models have these fields/dates stored as INT (for speed, or that's the idea at least)
        if ($this instanceof Ads || $this instanceof AdsHomepage) {
            $options['beforeValidationOnCreate']['format'] = 'U';
            $options['beforeValidationOnUpdate']['format'] = 'U';
        }

        // TODO: perhaps it would be better to make Timestampable a Trait and set it up
        // explicitly for each Model that should have it...

        // No longer attaching Timestampable behavior for absolutely everything
        if (!($this instanceof AdsParameters)) {
            $this->addBehavior(new Timestampable($options));
        }
    }

    public function disable_notnull_validations()
    {
        $this->setup(array('notNullValidations' => false));
    }

    public function enable_notnull_validations()
    {
        $this->setup(array('notNullValidations' => true));
    }

    /**
     * Phalcon currently has 2 different kinds of validations, and we've been getting
     * 2 different kind of results in various Controllers, depending on where they come from:
     * 1. \Phalcon\Mvc\Model\Validator (returned as an array of \Phalcon\Mvc\Model\Message objects)
     * 2. \Phalcon\Validation (returned as \Phalcon\Validation\Message\Group)
     *
     * We prefer working with the latter, so this method overrides the model's getMessages()
     * method in order to transform the messages into a \Phalcon\Validation\Message\Group
     *
     * TODO/FIXME: Not every model extends the BaseModel, so there could still be cases
     * where one can get "old" message objects returned... we could create a new BaseModel with
     * nothing but _really_ basic stuff in it (because the current BaseModel implies timestampable behaviour)
     * and move the current BaseModel's timestamping behaviour into a BaseModelTimestampable (but it might still be too early to tell),
     * and on top of that we're already having troubles with "inheritance vs composition" - perhaps it's already time
     * to switch to using Traits
     *
     * @link https://github.com/phalcon/cphalcon/issues/2050
     *
     * @param null $filter
     *
     * @return \Phalcon\Validation\Message\Group
     */
    public function getMessages($filter = null)
    {
        /* @var \Phalcon\Mvc\Model\MessageInterface[] $parent_messages */
        $parent_messages = parent::getMessages($filter);
        if ($parent_messages) {
            $group = new \Phalcon\Validation\Message\Group();
            foreach ($parent_messages as $message) {
                $group->appendMessage(
                    new \Phalcon\Validation\Message(
                        'Model: ' . $message->getMessage(),
                        $message->getField(),
                        $message->getType()
                    )
                );
            }
            unset($message);
            return $group;
        }
    }

    /**
     * Builds and executes an SQL query in order to read the specified DB column definition
     * and return an array of possible values (used for ENUM fields mostly)
     *
     * @param string $column
     *
     * @return array
     */
    public function get_field_options($column)
    {
        $sql  = 'SHOW COLUMNS ';
        $sql .= ' FROM ' . $this->getSource();
        $sql .= " LIKE '" . $column . "'";

        /** @var \Phalcon\Db\AdapterInterface */
        $db = $this->getDI()->getShared('db');

        $result = $db->fetchOne($sql);

        if (!is_array($result)) {
            $result = (array) $result;
        }

        // Match options using regex
        preg_match('~\((.*)\)~is', $result['Type'], $options);

        // Replace single quotes
        $options = str_replace("'", '', $options[1]);
        $options = explode(',', $options);

        // Store the default value if provided
        /*
        if (isset($result['Default'])) {
            $default = trim($result['Default']);
            if (!empty($default)) {
                $options['default'] = $default;
            }
        }
        */

        return $options;
    }

    /**
     * Returns the record's title/name/whatever we want... Can (and should be) overridden
     * for each specific model.
     * Used in the backend when we wan't to display an error message or show what's about
     * to be deleted etc...
     *
     * @return mixed|null
     */
    public function ident()
    {
        $ident = 'Unknown';

        if (isset($this->title)) {
            $ident = $this->readAttribute('title');
        } elseif (isset($this->name)) {
            $ident = $this->readAttribute('name');
        } elseif (isset($this->username)) {
            $ident = $this->readAttribute('username');
        } elseif (isset($this->slug)) {
            $ident = $this->readAttribute('slug');
        }

        $ident .= sprintf(' [ID: %s]', $this->readAttribute('id'));

        return $ident;
    }

    public function set_meta_title($meta_title = null)
    {
        $this->meta_title = $meta_title;
    }

    public function get_meta_title()
    {
        return isset($this->meta_title) ? $this->meta_title : null;
    }

    public function set_meta_description($meta_description = null)
    {
        $this->meta_description = $meta_description;
    }

    public function get_meta_description()
    {
        return isset($this->meta_description) ? $this->meta_description : null;
    }

    public function set_meta_keywords($meta_keywords = null)
    {
        $this->meta_keywords = $meta_keywords;
    }

    public function get_meta_keywords()
    {
        return isset($this->meta_keywords) ? $this->meta_keywords : null;
    }

    /**
     * Helper method to get SEO meta tags
     */
    public function getSEOMeta($key)
    {
        $seo_meta = null;

        switch (trim($key)) {
            case 'title':
                $seo_title_field = isset($this->seo_fields_map['title']) ? $this->seo_fields_map['title'] : null;
                if ($meta_title = $this->get_meta_title()) {
                    $seo_meta = $meta_title;
                } elseif ($seo_title_field && isset($this->$seo_title_field) && '' !== trim($this->$seo_title_field)) {
                    $seo_meta = trim($this->$seo_title_field);
                }
                break;
            case 'description':
                $seo_description_field = isset($this->seo_fields_map['description']) ? $this->seo_fields_map['description'] : null;
                if ($meta_description = $this->get_meta_description()) {
                    $seo_meta = $meta_description;
                } elseif ($seo_description_field && isset($this->$seo_description_field) && '' !== trim($this->$seo_description_field)) {
                    $seo_meta = trim($this->$seo_description_field);
                }
                break;
            case 'keywords':
                $seo_meta = $this->get_meta_keywords();
                break;
            default:
                if (isset($this->$key) && '' !== trim($this->$key)) {
                    $seo_meta = trim($this->$key);
                }
        }

        return $seo_meta;
    }

    /**
     * @param \Phalcon\Http\RequestInterface $request
     */
    public function initialize_model_with_post($request)
    {
        if (!empty($this->fields_types_map)) {
            $setter_closure = function (&$model, $field, $type, $default = null) use ($request) {
                switch ($type) {
                    case 'int':
                        $value = (int) $request->getPost($field);
                        if ($value > 0) {
                            $model->$field = $value;
                        }
                        break;
                    case 'bool':
                        $model->$field = $request->hasPost($field);
                        break;
                    case 'boolint':
                        $model->$field = $request->hasPost($field) ? 1 : 0;
                        break;
                    case 'date':
                        $value = trim($request->getPost($field));
                        if ($value) {
                            $model->$field = DateUtils::toDB($value);
                        }
                        break;
                    case 'dateint':
                        $value = trim($request->getPost($field));
                        if ($value) {
                            $model->$field = strtotime($value);
                        }
                        break;
                    case 'string':
                    default:
                        $value = trim($request->getPost($field));
                        if ($value) {
                            $model->$field = (string) $value;
                        } else {
                            $model->$field = $default;
                        }
                        break;
                }
            };

            foreach ($this->fields_types_map as $type => $fields) {
                if (!empty($fields)) {
                    foreach ($fields as $field_name) {
                        $setter_closure($this, $field_name, $type);
                    }
                }
            }
        }
    }

    /**
     * @return \Baseapp\Extension\Cache\Backend\MyMemcache|null
     */
    public static function getMemcache()
    {
        $di = Di::getDefault();
        $cache = null;
        if ($di->has('memcache')) {
            try {
                /* @var $cache \Baseapp\Extension\Cache\Backend\MyMemcache */
                $cache = $di->getShared('memcache');
            } catch (PhCacheException $e) {
                Bootstrap::log($e);
            }
        }

        return $cache;
    }
}
