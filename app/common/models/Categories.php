<?php

namespace Baseapp\Models;

use Baseapp\Bootstrap;
use Phalcon\Di;
use Phalcon\Mvc\Model\Resultset;
use Baseapp\Library\Validations\Categories as CategoriesValidations;
use Baseapp\Library\Behavior\NestedSet as NestedSetBehavior;
use Baseapp\Traits\FormModelControllerTrait;
use Baseapp\Traits\NestedSetActions;
use Baseapp\Traits\MemcachedMethods;
use Baseapp\Models\CategoriesSettings as CategorySettings;
use Baseapp\Library\Parameters\Parametrizator;

/**
 * Categories Model
 */
class Categories extends BaseModelBlamable
{
    use FormModelControllerTrait;
    use NestedSetActions;
    use MemcachedMethods;

    const MEMCACHED_KEY = 'categoryTree';

    const FALLBACK_IMAGES_DIR_URL = 'assets/img/fallback-images';
    const FALLBACK_IMAGES_DIR_PATH = ROOT_PATH . '/public/' . self::FALLBACK_IMAGES_DIR_URL;

    public $name          = '';
    public $paid_category = 0;

    /**
     * Categories initialize
     */
    public function initialize()
    {
        parent::initialize();

        $this->addBehavior(new NestedSetBehavior(array(
            'hasManyRoots'    => false,
            'rootAttribute'   => 'root_id',
            'rightAttribute'  => 'rght',
            'parentAttribute' => 'parent_id',
        )));

        /**
         * Every category can belong to a certain TransactionType. In case a TransactionType is set, this category can
         * be parametrized and can accept ads.
         */
        $this->belongsTo('transaction_type_id', __NAMESPACE__ . '\TransactionTypes', 'id', array(
            'alias'      => 'TransactionType',
            'reusable'   => true,
            'foreignKey' => array(
                'action' => \Phalcon\Mvc\Model\Relation::ACTION_CASCADE
            )
        ));

        /**
         * Every category can have many fieldsets in which will be placed parameters.
         */
        $this->hasMany('id', __NAMESPACE__ . '\CategoriesFieldsets', 'category_id', array(
            'alias'      => 'Fieldsets',
            'foreignKey' => array(
                'action' => \Phalcon\Mvc\Model\Relation::ACTION_CASCADE
            )
        ));

        /**
         * Every category can have many fieldset parameters
         */
        $this->hasMany('id', __NAMESPACE__ . '\CategoriesFieldsetsParameters', 'category_id', array(
            'alias'      => 'FieldsetParameters',
            'foreignKey' => array(
                'action' => \Phalcon\Mvc\Model\Relation::ACTION_CASCADE
            )
        ));

        /**
         * Every category has one CategoriesSettings
         */
        $this->hasOne('id', __NAMESPACE__ . '\CategoriesSettings', 'category_id', array(
            'alias'    => 'Settings',
            'reusable' => true
        ));
    }

    public function beforeSave()
    {
        // handle the json field
        $json = (array) $this->json;
        if (!empty($json)) {
            $json = json_encode($json);
        } else {
            $json = null;
        }
        $this->json = $json;
    }

    public function afterFetch()
    {
        // handle the json field
        $this->json = $this->json ? json_decode($this->json) : new \stdClass;
    }

    /**
     * Get Category settings (or just a specific field from it)
     *
     * @param string $field Specific field to get from category settings
     *
     * @return null|mixed|\Baseapp\Models\CategoriesSettings
     */
    public function getSettings($field = null)
    {
        $category_settings = $this->Settings;
        $result = $category_settings;

        if (null !== $field && isset($result->$field)) {
            $result = !empty($result->$field) ? $result->$field : null;
        }

        return $result;
    }

    protected function getCachableData()
    {
        $tree = $this->getTree();

        $relatedModelSettings = CategorySettings::find();
        if ($relatedModelSettings) {
            foreach ($relatedModelSettings as $relatedSettings) {
                $categoryId = (int) $relatedSettings->category_id;
                $settings = (object) $relatedSettings->toArray();
                unset($settings->id, $settings->category_id);
                if (isset($tree[$categoryId])) {
                    $settings->publication_print_mapping = trim($settings->publication_print_mapping) ? json_decode($settings->publication_print_mapping) : null;
                    $settings->products_cfg_json = trim($settings->products_cfg_json) ? json_decode($settings->products_cfg_json) : null;
                    $tree[$categoryId]->settings = $settings;
                }
            }
        }

        return $tree;
    }

    protected static function get_mapped_category_ids()
    {
        static $mapped_category_ids = array();
        static $mapped_categories = array();

        if (empty($mapped_category_ids) || empty($mapped_categories)) {
            $mapped_categories = CategorySettings::find(array(
                'conditions' => 'publication_print_mapping IS NOT NULL',
                'columns'    => 'category_id'
            ));

            if ($mapped_categories->valid()) {
                foreach($mapped_categories as $row) {
                    $mapped_category_ids[] = intval($row['category_id']);
                }
            }
        }

        return $mapped_category_ids;
    }

    public static function getMappedStatus($category)
    {
        $can_be_mapped = false;
        $mapped = false;
        $mapped_status = '<span class="text-danger">Not mapped</span>';

        if ($category) {
            if ($category->transaction_type_id) {
                $can_be_mapped = true;
                if (in_array($category->id, self::get_mapped_category_ids())) {
                    $mapped = true;
                    $mapped_status = '<span class="text-success">Mapped</span>';
                }
            } else {
                $mapped_status = '<span class="text-muted">Cannot be mapped (No transaction type)</span>';
            }
        }

        return array(
            'can_be' => $can_be_mapped,
            'is'     => $mapped,
            'text'   => $mapped_status
        );
    }

    protected static function get_parametrized_category_ids()
    {
        static $parametrized_category_ids = array();
        static $parametrized_categories = array();

        if (empty($parametrized_category_ids) || empty($parametrized_categories)) {
            $parametrized_categories = CategoriesFieldsetsParameters::find(array(
                'columns' => 'category_id',
                'group'   => 'category_id'
            ));

            if ($parametrized_categories->valid()) {
                foreach($parametrized_categories as $row) {
                    $parametrized_category_ids[] = intval($row['category_id']);
                }
            }
        }

        return $parametrized_category_ids;
    }

    public static function getParametersStatus($category)
    {
        $can_be_parametrized = false;
        $parametrized = false;
        $parametrized_status = '<span class="text-danger">Not parametrized</span>';

        if ($category) {
            if ($category->transaction_type_id) {
                $can_be_parametrized = true;

                if (in_array($category->id, self::get_parametrized_category_ids())) {
                    $parametrized = true;
                    $parametrized_status = '<span class="text-success">Parametrized</span>';
                }
            } else {
                $parametrized_status = '<span class="text-muted">Cannot be parametrized (No transaction type)</span>';
            }
        }

        return array(
            'can_be' => $can_be_parametrized,
            'is'     => $parametrized,
            'text'   => $parametrized_status
        );
    }

    public static function getProductsStatus($category)
    {
        $can_have_products = false;
        $has_products = false;
        $has_products_status = '<span class="text-success">Has products</span>';

        if ($category) {
            if ($category->transaction_type_id) {
                $can_have_products = true;
                $has_products = true;
            } else {
                $has_products_status = '<span class="text-muted">Cannot have products (No transaction type)</span>';
            }
        }

        return array(
            'can_be' => $can_have_products,
            'is'     => $has_products,
            'text'   => $has_products_status
        );
    }

    public function getModerationType()
    {
        $moderation_type = 'post';

        $cat_settings = $this->Settings;

        if (!$cat_settings && isset($this->id)) {
            $cat_settings = CategorySettings::findFirstByCategoryId($this->id);
        }

        if ($cat_settings && !empty($cat_settings->moderation_type)) {
            $moderation_type = $cat_settings->moderation_type;
        }

        return $moderation_type;
    }

    /**
     * getBreadcrumbs method
     *
     * Helper method to get current category's breadcrumbs.
     *
     * @return null|\Baseapp\Models\Categories[]
     */
    public function getBreadcrumbs()
    {
        $nodePath = $this->getNodePath();

        $categoryPathIDs = array();
        foreach ($nodePath as $id => $category) {
            $categoryPathIDs[] = $category->id;
        }

        if (empty($categoryPathIDs)) {
            return null;
        }

        $categoryPathIDs = implode(',', $categoryPathIDs);
        $builder = new \Phalcon\Mvc\Model\Query\Builder();
        $builder
            ->addFrom('Baseapp\Models\Categories', 'c')
            ->where('c.id IN (' . $categoryPathIDs . ')')
            ->orderBy('FIELD(c.id,' . $categoryPathIDs . ')');

        return $builder->getQuery()->execute();
    }

    /**
     * Helper method to populate models data with POST values
     * @param  \Phalcon\Http\RequestInterface $request
     */
    protected function populate_model_with_POST($request)
    {
        $this->parent_id = (int) $request->getPost('parent_id');
        if (intval($request->getPost('transaction_type_id'))) {
            $this->transaction_type_id = (int) $request->getPost('transaction_type_id');
        } else {
            $this->transaction_type_id = null;
        }
        $this->name = (string) $request->getPost('name');
        $this->url = (string) $request->getPost('url');
        $this->excerpt = trim(strip_tags($request->getPost('excerpt'))) ? trim(strip_tags($request->getPost('excerpt'))) : null;
        $this->paid_category = $request->hasPost('paid_category') ? 1 : 0;
        $this->active = $request->hasPost('active') ? 1 : 0;

        $category_settings = $this->Settings;
        if (!$category_settings) {
            $category_settings = new CategorySettings();
        }

        $category_settings->moderation_type = (trim($request->getPost('moderation_type')) ? trim($request->getPost('moderation_type')) : 'none');
        if (intval($request->getPost('transaction_type_id'))) {
            $category_settings->default_ad_expire_time = intval($request->getPost('default_ad_expire_time')) ? (int) $request->getPost('default_ad_expire_time') : null;
        } else {
            $category_settings->default_ad_expire_time = null;
        }
        $category_settings->meta_title = trim(strip_tags($request->getPost('meta_title'))) ? trim(strip_tags($request->getPost('meta_title'))) : null;
        $category_settings->meta_description = trim(strip_tags($request->getPost('meta_description'))) ? trim(strip_tags($request->getPost('meta_description'))) : null;
        $category_settings->meta_keywords = trim(strip_tags($request->getPost('meta_keywords'))) ? trim(strip_tags($request->getPost('meta_keywords'))) : null;

        // age restriction settings
        $category_settings->age_restriction = $request->hasPost('age_restriction') ? 1 : 0;
        if ($category_settings->age_restriction == 1) {
            $category_settings->min_age_allowed = trim(strip_tags($request->getPost('min_age_allowed'))) ? intval($request->getPost('min_age_allowed')) : null;
            $category_settings->min_age_html = trim(strip_tags($request->getPost('min_age_html'))) ? trim($request->getPost('min_age_html')) : null;
        } else {
            $category_settings->min_age_allowed = null;
            $category_settings->min_age_html = null;
        }

        $this->Settings = $category_settings;
        $this->populate_model_json_with_POST($request);
    }

    /**
     * Helper method to populate model's json field with POST values
     * @param  \Phalcon\Http\RequestInterface $request
     */
    protected function populate_model_json_with_POST($request)
    {
        // FancyName
        $this->json->fancy_name = $request->getPost('fancy_name', 'string', $request->getPost('name', 'string', null));

        // category icon
        $this->json->icon = $request->getPost('category_icon', 'string', null);

        // fallback image
        $this->json->fallback_image = $request->getPost('category_fallback_image', 'string', null);

        // search by location enabled?
        $this->json->search_by_location = $request->hasPost('search_by_location') ? 1 : 0;

        // run through first-level json properties and remove those with null or empty string (leave false for now!)
        foreach ($this->json as $property => $value) {
            if (null === $value || '' === trim($value)) {
                unset($this->json->$property);
            }
        }
    }

    /**
     * Create a Category method
     *
     * @param \Phalcon\Http\RequestInterface $request
     * @param \Baseapp\Models\Categories $parent
     *
     * @return $this|\Phalcon\Mvc\Model\MessageInterface[]|\Phalcon\Validation\Message\Group|void
     */
    public function add_new($request, $parent)
    {
        $this->populate_model_with_POST($request);

        $validation = new CategoriesValidations();
        $validation->add('name', new \Baseapp\Extension\Validator\Uniqueness(array(
            'model' => '\Baseapp\Models\Categories',
            'extra_conditions' => array(
                'lft > :parent_left: AND rght < :parent_right: AND level = :level:',
                array(
                    'parent_left' => $parent->lft,
                    'parent_right' => $parent->rght,
                    'level' => $parent->level + 1
                )
            ),
            'message' => 'Category name should be unique within its parent',
        )));

        $messages = $validation->validate($request->getPost());

        if (count($messages)) {
            return $validation->getMessages();
        } else {
            if (true === $this->appendTo($parent)) {
                $this->deleteMemcachedData();
                return $this;
            } else {
                \Baseapp\Bootstrap::log($this->getMessages());
                return $this->getMessages();
            }
        }
    }

    /**
     * Save Category data method
     *
     * @param \Phalcon\Http\RequestInterface $request
     * @param \Baseapp\Models\Categories $parent
     *
     * @return $this|\Phalcon\Mvc\Model\MessageInterface[]|\Phalcon\Validation\Message\Group|void
     */
    public function save_changes($request, $parent)
    {
        $validation = new CategoriesValidations();

        $name_lc      = mb_strtolower($this->name, 'UTF-8');
        $name_post_lc = mb_strtolower($request->getPost('name'), 'UTF-8');
        if ($name_lc != $name_post_lc) {
            $validation->add('name', new \Baseapp\Extension\Validator\Uniqueness(array(
                'model' => '\Baseapp\Models\Categories',
                'extra_conditions' => array(
                    'lft > :parent_left: AND rght < :parent_right: AND level = :level:',
                    array(
                        'parent_left' => $parent->lft,
                        'parent_right' => $parent->rght,
                        'level' => $parent->level + 1
                    )
                ),
                'message' => 'Category name should be unique within its parent',
            )));
        }
        // avoid validation for certain fields that haven't been changed
        $unique_if_changed = array('url');
        $require_unique = array();
        foreach ($unique_if_changed as $field) {
            if ($this->$field != $request->getPost($field)) {
                $require_unique[] = $field;
            }
        }
        $validation->set_unique($require_unique);

        $messages = $validation->validate($request->getPost());

        $this->populate_model_with_POST($request);

        if (count($messages)) {
            return $validation->getMessages();
        } else {
            // if item was moved to another parent then $parent->id and $this->parent()->id differ!
            if ((int) $this->parent()->id != (int) $parent->id) {
                $save_result = $this->moveAsLast($parent);
            } else {
                $save_result = $this->saveNode();
            }
            if (true === $save_result) {
                $this->deleteMemcachedData();
                return $this;
            } else {
                \Baseapp\Bootstrap::log($this->getMessages());
                return $this->getMessages();
            }
        }
    }

    /**
     * Active/Inactive category, depending on the previous state
     *
     * @return bool Whether the update was successful or not
     */
    public function activeToggle()
    {
        $this->disable_notnull_validations();
        $this->active = (int) !$this->active;
        $result = $this->saveNode();
        $this->enable_notnull_validations();
        $this->deleteMemcachedData();
        return $result;
    }

    public static function getCategoriesWithoutTransactionType()
    {
        $category_ids = array();

        $categories_without_transaction_type_array = self::find(array(
            'transaction_type_id IS NULL',
            'columns' => array('id'),
            'order' => 'lft'
        ))->toArray();

        foreach ($categories_without_transaction_type_array as $cat) {
            $category_ids[] = (int) $cat['id'];
        }

        return $category_ids;
    }

    public static function getCategoriesThatDontAcceptAds()
    {
        $category_ids = array();

        $tree = Di::getDefault()->get(self::MEMCACHED_KEY);
        unset($tree[0]);
        unset($tree[1]);
        foreach ($tree as $category) {
            if ($category->active == 0 || $category->transaction_type_id == null) {
                $category_ids[] = $category->id;
            }
        }
        unset($tree);

        return $category_ids;
    }

    /**
     * Get all roots from the tree
     *
     * As this is actually a single root tree, this method returns direct descendants of the real 'root' node
     *
     * @return \Baseapp\Models\Categories
     */
    public static function getRoots() {
        if ($available_categories = self::findFirst('lft = 1')) {
            return $available_categories->children();
        }

        return null;
    }

    /**
     * Get current category's path
     *
     * @param string $separator Separator between subcategories/nodes
     * @param bool $root_node Whether to include the root node or no
     * @param bool $with_links Should the nodes be links or not
     *
     * @return string
     */
    public function getPath($separator = ' › ', $root_node = false, $with_links = false) {
        /**
         * Checking if the first argument is being used as boolean, meaning
         * we're supposed to treat $separator as default value.
         */
        if (is_bool($separator)) {
            $root_node = $separator;
            $separator = ' › ';
        } elseif ('' == trim($separator)) {
            $separator = ' › ';
        }

        $category_path = array();
        $category_path_links = [];
        if ($category_ancestors = $this->getNodePath()) {
            foreach ($category_ancestors as $category_ancestor) {
                $category_path[]       = $category_ancestor->name;
                if ($with_links) {
                    $url                   = self::buildHref($category_ancestor->url);
                    $category_path_links[] = sprintf('<a href="%s">%s</a>', $url, $category_ancestor->name);
                }
            }
        }

        if (false == $root_node && isset($category_path[0])) {
            unset($category_path[0]);
            if ($with_links && isset($category_path_links[0])) {
                unset($category_path_links[0]);
            }
        }

        if ($with_links) {
            $category_path = implode($separator, $category_path_links);
        } else {
            $category_path = implode($separator, $category_path);
        }

        return $category_path;
    }

    public function getPathLinksMarkup($separator = ' › ', $root_node = false)
    {
        return $this->getPath($separator, $root_node, true);
    }

    public function getFilters($data = null)
    {
        $parametrizator = new Parametrizator();
        $parametrizator->setCategory($this);
        $parametrizator->setModule('frontend');
        if ($data) {
            $parametrizator->setData($data);
        }
        return $parametrizator->getFilters();
    }

    /**
     * Get age restrictions data for the current category.
     * If there are no restrictions on the category itself, we try getting them from the first
     * parent that might have them.
     *
     * @return null|array
     */
    public function getAgeRestriction()
    {
        $age_restriction = null;

        $builder = new \Phalcon\Mvc\Model\Query\Builder();
        $builder->columns(array('cs.*'));
        $builder->addFrom(
            'Baseapp\Models\Categories',
            'c'
        );
        $builder->innerJoin(
            'Baseapp\Models\CategoriesSettings',
            'cs.category_id = c.id AND c.parent_id IS NOT NULL AND cs.age_restriction = 1',
            'cs'
        );
        $builder->where(
            'c.lft <= :category_left: AND c.rght >= :category_right:',
            array(
                'category_left'  => $this->lft,
                'category_right' => $this->rght
            ),
            array(
                'category_left'  => \PDO::PARAM_INT,
                'category_right' => \PDO::PARAM_INT
            )
        );
        $builder->orderBy('c.lft DESC');
        $builder->limit(1);
        $results = $builder->getQuery()->execute();

        if ($results && count($results) == 1) {
            $category_settings = $results[0];
            if ($category_settings->age_restriction) {
                $age_restriction = array(
                    'category_id' => $category_settings->category_id,
                    'min_age'     => $category_settings->min_age_allowed,
                    'html'        => $category_settings->min_age_html,
                    'url'         => $this->get_url()
                );
            }
        }

        return $age_restriction;
    }

    public function get_url(array $query_string = null)
    {
        $link = null;

        if (isset($this->id)) {
            //$url  = '/category/' . $this->id;
            $url = '/' . $this->url;
            // TODO: $query_string without 'url' service is not supported currently (and probably not even used anyway)

            // Use the 'url' resolver service if present, making it more flexible
            $di = $this->getDI();
            if ($di->has('url')) {
                $url = $di->get('url')->get($this->url, $query_string);
            }

            $link = $url;
        }

        return $link;
    }

    public static function buildHref($url, array $query_string = null)
    {
        return Di::getDefault()->get('url')->get($url, $query_string);
    }

    /**
     * @return array|\Phalcon\Mvc\Model\ResultsetInterface
     */
    public function getSiblings()
    {
        $siblings = array();

        if ($this->transaction_type_id) {
            $siblings = self::find(array(
                'conditions' => 'parent_id = :parent_id: AND transaction_type_id IS NOT NULL AND NOT(parent_id = 1)',
                'bind'       => array(
                    'parent_id' => $this->parent_id
                ),
                'order'      => 'lft ASC'
            ));
        }

        return $siblings;
    }

    public static function getPossibleFallbackImagesList()
    {
        $allowedExtensions = array(
            'svg'  => 'SVG images'
        );

        $groupedIcons = array(
            'SVG images'  => array()
        );

        // Collect all the image files
        $dir = self::FALLBACK_IMAGES_DIR_PATH;
        $di = Di::getDefault();
        foreach (new \DirectoryIterator($dir) as $file) {
            if ($file->isDot()) {
                continue;
            }
            $filenameExtension = mb_strtolower($file->getExtension(),'UTF-8');
            if (isset($allowedExtensions[$filenameExtension])) {
                $groupID                  = $allowedExtensions[$filenameExtension];
                $filenameBaseName         = $file->getBasename('.' . $filenameExtension);
                $filenameBaseNameFormated = ucfirst(implode(' ', explode('-', preg_replace('/-+/', '-', str_replace('_', '-', $filenameBaseName)))));
                $filename                 = $file->getFilename();

                $url = $di->get('url')->get(self::FALLBACK_IMAGES_DIR_URL . '/' . $filename);
                $data_content = '<span class="icon no-thumb-svg" style="background-image:url(' . $url . ')" alt="' . $filenameBaseNameFormated . '"></span>';
                $data_content .= '<small>' . $filenameBaseNameFormated . ' <span class="text-muted">(' . $filename . ')</span></small>';

                $groupedIcons[$groupID][$filename] = array(
                    'text' => $filenameBaseNameFormated,
                    'attributes' => array(
                        'data-content' => $data_content
                    )
                );
            }
        }

        foreach ($groupedIcons as $groupID => &$icons) {
            if (count($icons)) {
                ksort($icons);
            } else {
                $groupedIcons[$groupID] = null;
                unset($groupedIcons[$groupID]);
            }
        }

        return $groupedIcons;
    }

    public static function getAvailableIcons()
    {
        $allowedExtensions = array(
            'png'  => 'PNG images',
            'jpg'  => 'JPEG images',
            'jpeg' => 'JPEG images',
            'gif'  => 'GIF images',
            'svg'  => 'SVG images'
        );

        $groupedIcons = array(
            'SVG images'  => array(),
            'PNG images'  => array(),
            'JPEG images' => array(),
            'GIF images'  => array()
        );

        // Collect all the image files
        $dir = ROOT_PATH . '/public/assets/img/categories';
        $di = Di::getDefault();
        foreach (new \DirectoryIterator($dir) as $file) {
            if ($file->isDot()) {
                continue;
            }
            $filenameExtension = mb_strtolower($file->getExtension(),'UTF-8');
            if (isset($allowedExtensions[$filenameExtension])) {
                $groupID                  = $allowedExtensions[$filenameExtension];
                $filenameBaseName         = $file->getBasename('.' . $filenameExtension);
                $filenameBaseNameFormated = ucfirst(implode(' ', explode('-', preg_replace('/-+/', '-', str_replace('_', '-', $filenameBaseName)))));
                $filename                 = $file->getFilename();

                $groupedIcons[$groupID][$filename] = array(
                    'text' => $filenameBaseNameFormated,
                    'attributes' => array(
                        'data-content' => '<span class="icon" style="background-image:url(' . $di->get('url')->get('assets/img/categories/' . $filename) . ')" alt="' . $filenameBaseNameFormated . '"></span><small>' . $filenameBaseNameFormated . ' <span class="text-muted">(' . $filename . ')</span></small>'
                    )
                );
            }
        }

        foreach ($groupedIcons as $groupID => &$icons) {
            if (count($icons)) {
                ksort($icons);
            } else {
                $groupedIcons[$groupID] = null;
                unset($groupedIcons[$groupID]);
            }
        }

        return $groupedIcons;
    }

    public function getFirstChildWithTransactionType()
    {
        $firstChildWithTransactionType = null;
//        $tree                          = self::getCachedTree();
        $tree                          = self::getMemcachedData();
        $currTreeCategory              = $tree[$this->id];

        if ($currTreeCategory && count($currTreeCategory->children)) {
            foreach ($currTreeCategory->children as $child) {
                if ($child->transaction_type_id) {
                    $firstChildWithTransactionType = $child;
                    break;
                }
            }
        }

        return $firstChildWithTransactionType;
    }

    /**
     * Returns the custom "noImage" fallback image for the specified category or false.
     *
     * Specified category's parents are checked all the way up the tree until a parent
     * with a custom fallback image is found. If there isn't any, we return false.
     *
     * @param int|Categories $category
     * @return null|string
     */
    public static function getFallbackImage($category)
    {
        $fallback_image = false;

        if ($category instanceof Categories) {
            $category_id = $category->id;
        } else {
            $category_id = (int) $category;
        }

        $tree = self::getMemcachedData();
        $node = isset($tree[$category_id]) ? $tree[$category_id] : null;
        // $parent_id = $node->parent_id;

        if ($node) {
            $fallback_image = isset($node->json->fallback_image) && !empty($node->json->fallback_image) ? $node->json->fallback_image : null;
            $parent_id = $node->parent_id;
            while ($parent_id > 0 && !$fallback_image) {
                if (isset($tree[$parent_id])) {
                    $parent_node = $tree[$parent_id];
                    $parent_id   = $parent_node->parent_id;
                    $fallback_image = isset($parent_node->json->fallback_image) && !empty($parent_node->json->fallback_image) ? $parent_node->json->fallback_image : null;
                } else {
                    // Bail if no node with such a parent exists?
                    $parent_id = 0;
                }
            }
        }

        if (null !== $fallback_image) {
            // Return the full path/url for the fallback image
            $fallback_image = self::FALLBACK_IMAGES_DIR_URL . '/' . $fallback_image;
        }

        return $fallback_image;
    }
}
