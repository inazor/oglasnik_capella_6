<?php

namespace Baseapp\Models;

use Baseapp\Library\Products\Online\Pinky;
use Baseapp\Library\Products\Online\Platinum;
use Baseapp\Library\Products\ProductsInterface;
use Baseapp\Traits\CachedAllCollection;

/**
 * CategoriesSettings Model
 */
class CategoriesSettings extends BaseModelBlamable
{
    use CachedAllCollection;

    public $id;
    public $category_id;
    public $moderation_type;
    public $default_ad_expire_time;
    public $meta_title;
    public $meta_description;
    public $ad_desc_tpl;
    public $age_restriction;
    public $min_age_allowed;
    public $min_age_html;

    public $products_cfg_json;

    static $cache = array();

    /**
     * CategoriesSettings initialize
     */
    public function initialize()
    {
        parent::initialize();

        // Every CategoriesSettings belongs to a category
        $this->belongsTo(
            'category_id', __NAMESPACE__ . '\Categories', 'id', array(
                'alias'    => 'Category',
                'reusable' => true
            )
        );

        // Cache entire table on initialize
        self::$cache = $this->cachedAll();
    }

    /**
     * A somewhat generic getter.
     *
     * @param string|null $field
     * @param string|null $key
     * @param string|null $sub_key
     *
     * @return CategoriesSettings|mixed
     */
    public function get($field = null, $key = null, $sub_key = null)
    {
        $settings = $this;

        // Return the entire model object when no specific field is requested
        $settings_data = $settings;

        // If a specific field is requested, return that
        if (null !== $field) {
            if (!property_exists($settings, $field)) {
                throw new \InvalidArgumentException('Field/property `' . $field . '` not found in `$settings` (CategorySettings)!');
            }
            // We have no easy way of knowing if the specific field really is json encoded in the first place
            // but since all products-related fields currently do, we're always decoding here (using assoc arrays)
            $settings_data = json_decode($settings->{$field}, true);
        }

        // Allows retrieving data for a single nested key from within the field's entire json_encoded array
        if (null !== $key && isset($settings_data[$key])) {
            $settings_data = $settings_data[$key];

            // Check second level if first level had something
            if (null !== $sub_key && isset($settings_data[$sub_key])) {
                $settings_data = $settings_data[$sub_key];
            }
        }

        return $settings_data;
    }

    public static function getEnabledSpecialsFromConfigData($config)
    {
        $enabled_specials = array();
        $special_list     = self::getSpecialProductsList();

        // If $config is a string and not yet an array, treat it as json and decode it, looking for 'online' key
        // which holds the online products configuration data for this categoryproduct config data
        if (!is_array($config) && is_string($config) && !empty($config)) {
            $config = json_decode($config, true);
            if (isset($config['online']) && !empty($config['online'])) {
                $config = $config['online'];
            }
        }

        // Loop over special products and check if config has it enabled
        foreach ($special_list as $p) {
            if (self::isSpecialProductEnabledInConfig($p, $config)) {
                $enabled_specials[] = $p->getId();
            }
        }

        return $enabled_specials;
    }

    public static function isSpecialProductEnabledInConfig(ProductsInterface $product, $config)
    {
        $fqcn        = $product->getFqcn();
        $product_cfg = isset($config[$fqcn]) ? $config[$fqcn] : null;

        return ($product_cfg && !$product_cfg['disabled']);
    }

    /**
     * Returns the list of ProductsInterface instances that we consider "special", which currently means:
     * - their behavior and presentation is different
     * - they are currently not orderable/purchasable by regular means
     *
     * @return ProductsInterface[]
     */
    public static function getSpecialProductsList()
    {
        /** @var ProductsInterface[] $specials */
        static $specials = null;

        if (null === $specials) {
            // The order here dictates how they appear on sections/categories
            // in case there are both of those products active
            $specials = array(
                new Platinum(),
                new Pinky(),
            );
        }

        return $specials;
    }

    /**
     * @return array|null
     */
    public static function getAgeRestrictedCategoryIdsList()
    {
        // TODO/FIXME: this is here just so that the model initializes and then caches properly
        // for cases when it hasn't already been used somewhere during the request
        static $foo = null;
        if (null === $foo) {
            $foo = new self();
        }

        // Go through the cache and collect only those records that have age_restriction > 0
        $records = array();
        foreach (self::$cache as $id => $data) {
            if ($data['age_restriction'] > 0) {
                $records[] = $data;
            }
        }

        $list = null;
        if (!empty($records)) {
            $list = array();
            foreach ($records as $k => $data) {
                $list[$data['category_id']] = $data['min_age_allowed'];
            }
        }

        return $list;
    }

    /**
     * @return array
     */
    public static function getListOfAllCategoriesWithEnabledSpecialProducts()
    {
        static $list = null;

        if (null === $list) {
            $list = array();
            foreach (self::$cache as $id => $data) {
                $json_string = null;
                if (isset($data['products_cfg_json']) && !empty($data['products_cfg_json'])) {
                    $json_string      = $data['products_cfg_json'];
                    $enabled_specials = self::getEnabledSpecialsFromConfigData($json_string);
                    if (!empty($enabled_specials)) {
                        $list[$id] = $enabled_specials;
                    }
                }
            }
        }

        return $list;
    }

    private static function getProductForCategory($category_id, $type, $product)
    {
        $category = isset(self::$cache[$category_id]) ? self::$cache[$category_id] : null;
        if (!$category) {
            return false;
        }

        $productsConfig = isset($category['products_cfg_json']) && $category['products_cfg_json'] ? json_decode($category['products_cfg_json']) : null;
        if (!$productsConfig) {
            return false;
        }

        return isset($productsConfig->$type->$product) ? $productsConfig->$type->$product : null;
    }

    public static function isProductEnabled($category_id, $type, $product, $extras = null)
    {
        $product = self::getProductForCategory($category_id, $type, $product);
        if (!$product) {
            return false;
        }

        if ($extras) {
            return isset($product->extras->$extras);
        }

        return true;
    }

    public static function isProductPushable($category_id, $type, $product)
    {
        $product = self::getProductForCategory($category_id, $type, $product);
        if (!$product) {
            return false;
        }

        return (isset($product->push_up) && $product->push_up > 0);
    }
}
