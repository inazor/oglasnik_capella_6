<?php

namespace Baseapp\Models;

/**
 * CmsArticlesTags Model
 */
class CmsArticlesTags extends BaseModelBlamable
{
    /**
     * CmsArticlesTags initialize
     */
    public function initialize()
    {
        parent::initialize();

        $this->belongsTo(
            'article_id', __NAMESPACE__ . '\CmsArticles', 'id',
            array(
                'alias' => 'Article',
                'reusable' => true
            )
        );
        $this->belongsTo(
            'tag_id', __NAMESPACE__ . '\Tags', 'id',
            array(
                'alias' => 'Tag',
                'reusable' => true
            )
        );
    }
}
