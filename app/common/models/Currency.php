<?php

namespace Baseapp\Models;

use Baseapp\Traits\CachedAllCollection;

/**
 * Currency Model
 */
class Currency extends BaseModelBlamable
{
    use CachedAllCollection;

    /**
     * Currency initialize
     */
    public function initialize()
    {
        $this->hasMany(
            'id', __NAMESPACE__ . '\Ads', 'currency_id',
            array(
                'alias'    => 'Ads',
                'reusable' => true,
            )
        );

        $this->hasMany(
            'id', __NAMESPACE__ . '\CategoriesFieldsetsParameters', 'currency_id',
            array(
                'alias'    => 'Parameters',
                'reusable' => true
            )
        );

        parent::initialize();

        // Cache entire table on initialize
        $this->cachedAll();
    }

    /**
     * Overriding CachedAllCollection\cachedAllBuildCacheData so that we can
     * change the cached data so that it's keyed on both "id" and "short_name"
     *
     * @inheritdoc
     */
    public function cachedAllBuildCacheData($records)
    {
        $data = array();

        foreach ($records as $record) {
            $data[$record->id] = $data[$record->short_name] = $record->toArray();
        }

        return $data;
    }

    public static function getIDArray()
    {
        return static::all();
    }

    // as self::getIDArray() returns doubled array (both with id and short_name
    // as keys), we need this to filter-out those rows where key is represented
    // with a short_name
    public static function getIDArrayOnly()
    {
        $rows = self::getIDArray();
        $currencies = array();

        foreach ($rows as $row_key => $row_value) {
            if (intval($row_key)) {
                $currencies[$row_key] = $row_value;
            }
        }

        return $currencies;
    }
}
