<?php

namespace Baseapp\Models;

/**
 * FailedLogins
 * Stores unsuccessful login attempts for both registered and anonymous users
 */
class FailedLogins extends BaseModel
{
    // 4 hours in seconds
    const DEFAULT_EXPIRY = 14400;

    public function getSource()
    {
        return 'failed_logins';
    }

    public function get_expiry()
    {
        return (time() + self::DEFAULT_EXPIRY);
    }

    public function expired()
    {
        $expires = $this->get_expiry();
        return (property_exists($this, 'attempted') && null !== $this->attempted && ($this->attempted < $expires));
    }

    public function initialize()
    {
        parent::initialize();

        $this->belongsTo('user_id', __NAMESPACE__ . '\Users', 'id', array(
            'alias' => 'user'
        ));

        // Let's just try always nuking all the expired login attempts
        // whenever we invoke this model and see how that behaves
        // if (mt_rand(1, 100) === 1) {
            $this->delete_all_expired();
        // }

        // zyt: since we're now deleting all the expired records, there's
        // no need to check for this one in particular, since it would've been
        // deleted above anyway?

        // Check and immediately delete this specific record if it's expired
        /*if ($this->expired()) {
            $this->delete();
        }*/
    }

    public function beforeValidationOnCreate()
    {
        $this->attempted = time();
        $this->attempted_at = date('Y-m-d H:i:s', $this->attempted);
    }

    /**
     * Deletes all expired/old login attempts
     */
    public function delete_all_expired()
    {
        $sql = 'DELETE FROM `' . $this->getSource() . '` WHERE `attempted` < :time';
        $params = array(':time' => $this->get_expiry());
        $this->getDI()->getShared('db')->execute($sql, $params);
    }
} 
