<?php

namespace Baseapp\Models;

use Phalcon\Mvc\Model\Resultset;
use Baseapp\Bootstrap;

class Locations extends BaseModel
{
    // filtering levels since all the data is in one table for now
    const LEVEL_COUNTRIES      = 1;
    const LEVEL_COUNTIES       = 2;
    const LEVEL_CITIES         = 3;
    const LEVEL_MUNICIPALITIES = 4;

    const MEMCACHED_KEY = 'location';
    const CACHE_TIME    = 0; // 0 => forever; null for no caching at all

    /**
     * Ads initialize
     */
    public function initialize()
    {
        parent::initialize();

        $this->hasMany('id', __NAMESPACE__ . '\Users', 'country_id', array(
            'alias'      => 'CountryUsers',
            'reusable'   => true
        ));
        $this->hasMany('id', __NAMESPACE__ . '\Users', 'county_id', array(
            'alias'      => 'CountyUsers',
            'reusable'   => true
        ));
    }

    /**
     * Set correct db table
     */
    public function getSource()
    {
        return 'location';
    }

    /**
     * Returns the name of the db column related to given level
     *
     * @return string
     */
    public static function getDBColumnName($level = 1)
    {
        $columns = array(
            'country_id',
            'county_id',
            'city_id',
            'municipality_id',
            'latlng'
        );
        if (isset($columns[($level - 1)])) {
            return trim($columns[($level - 1)]);
        }

        return null;
    }

    /**
     * Returns the (root-level) list of countries for use in dropdowns mostly
     *
     * @return \Phalcon\Mvc\Model\ResultsetInterface
     */
    public static function findCountries() {
        return static::getCachedChildren();
    }

    /**
     * Returns counties belonging to the specified $country_id or ALL the counties
     * in case $country_id was not specified
     *
     * @param int $country_id
     *
     * @return \Phalcon\Mvc\Model\ResultsetInterface
     */
    public static function findCounties($country_id = 1) {
        return static::getCachedChildren($country_id);
    }

    /**
     * Returns a list of cities belonging to the specified $county_id or ALL the cities
     * in case $county_id was not specified.
     *
     * @param null $county_id
     *
     * @return \Phalcon\Mvc\Model\ResultsetInterface
     */
    public static function findCities($county_id = null) {
        $params = array(
            'level=' . self::LEVEL_CITIES,
            'order' => 'sort_order DESC, name ASC, id ASC',
            'columns' => 'id, name, name_en'
        );

        if (null !== $county_id) {
            $params[0] .= ' AND parent_id = ' . (int) $county_id;
        }

        $cities = self::find($params);
        $cities->setHydrateMode(Resultset::HYDRATE_OBJECTS);
        return $cities;
    }

    public static function autosuggest($term, $starting_level = 1)
    {
        $locations = self::find(array(
            'conditions' => 'name LIKE CONCAT(\'%\', :term:, \'%\') AND level >= :starting_level:',
            'columns'    => 'id, level, name, zoom, lat, lng, north, south, west, east',
            'order'      => 'level ASC, name ASC',
            'bind'       => array(
                'term'           => '%' . $term . '%',
                'starting_level' => $starting_level
            )
        ));
        $locations->setHydrateMode(Resultset::HYDRATE_OBJECTS);

        return $locations;
    }

    /**
     * @param array $ids
     *
     * @return null|\Phalcon\Mvc\Model\ResultsetInterface
     */
    public static function findMultiple(array $ids)
    {
        $locations = null;
        // error_log(var_export($ids, true));
        if (!empty($ids)) {
            $condition = 'id IN (' . implode(', ', $ids) . ')';
            $locations = static::find(array(
                'conditions' => $condition,
                'hydration'  => Resultset::HYDRATE_OBJECTS
            ));
        }

        return $locations;
    }

// TODO: this should be implemented somehow so we could speed up and remove unneeded queries to db as much as possible
    public static function to_hierarchy()
    {
        $tree = array();

        $hierarchy_sql = <<<RawSQL
SELECT
    l1.id AS country_id,
    l1.name AS country_name,
    l2.id AS county_id,
    l2.name AS county_name,
    l3.id AS city_id,
    l3.name AS city_name,
    l4.id AS municipality_id,
    l4.name AS municipality_name
FROM location AS l1
LEFT OUTER JOIN location AS l2 ON l2.parent_id = l1.id
LEFT OUTER JOIN location AS l3 ON l3.parent_id = l2.id
LEFT OUTER JOIN location AS l4 ON l4.parent_id = l3.id
WHERE l1.parent_id IS NULL
ORDER BY l1.sort_order DESC, country_id, country_name, county_name, city_name, municipality_name
RawSQL;

        // Base model
        $location = new self();

        // Execute the query
        $rows = new \Phalcon\Mvc\Model\Resultset\Simple(null, $location, $location->getReadConnection()->query($hierarchy_sql));

        return $rows;
    }

    /**
     * Helper function so we can get children of the current location and not to expose real function name (children) :)
     */
    public function values()
    {
        $locationID = isset($this->id) && $this->id ? $this->id : null;
        return static::getCachedChildren($locationID);
    }

    private static function getChildrenFromDB($id = null)
    {
        if ($id) {
            $conditions = 'parent_id = :location_id:';
            $bind       = array(
                'location_id' => (int) $id
            );
        } else {
            $conditions = 'level = 1';
            $bind       = array();
        }

        return static::find(array(
            'conditions' => $conditions,
            'bind'       => $bind,
            'order'      => 'sort_order DESC, name ASC, id ASC',
            'hydration'  => Resultset::HYDRATE_OBJECTS
        ));
    }

    public static function getCachedChildren($id = null)
    {
        $locationChildren = null;
        $cacheKey         = static::MEMCACHED_KEY . '_' . ($id ? $id : 'countries');
        $memcache         = null;

        if (null !== static::CACHE_TIME && $memcache = static::getMemcache()) {
            // try to get location's children from cache
            $cachedChildren = $memcache->get($cacheKey);
            if ($cachedChildren) {
                $locationChildren = $cachedChildren;
                if (!$locationChildren || count($locationChildren) == 0) {
                    $locationChildren = null;
                } else {
                    //Bootstrap::log($cacheKey . ' found in cache');
                }
            }
        }

        if (!$locationChildren) {
            $locationChildren = static::getChildrenFromDB($id);

            if (null !== static::CACHE_TIME && $memcache) {
                $memcache->save($cacheKey, $locationChildren, static::CACHE_TIME);
                //Bootstrap::log($cacheKey . ' SAVED to cache');
            }
        }

        return $locationChildren;
    }

    public function hasChildren($get_from_cache = true)
    {
        $has_children = false;
        $children = $get_from_cache ? static::getCachedChildren($this->id) : static::getChildrenFromDB($this->id);
        if ($children && count($children)) {
            $has_children = true;
        }
        return $has_children;
    }

    /**
     * @return array
     */
    public static function getRegionsSeoData()
    {
        $data = array(
            'istra' => array(
                'name' => 'Istra',
                // 'counties' => array('Istarska'),
                'presets' => array(
                    'ad_location_1' => '1',
                    'ad_location_2' => '1136'
                )
            ),
            'srednja-dalmacija' => array(
                'name' => 'Srednja Dalmacija',
                // 'counties' => array('Splitsko-dalmatinska'),
                'presets' => array(
                    'ad_location_1' => '1',
                    'ad_location_2' => '5677'
                )
            ),
            'juzna-dalmacija' => array(
                'name' => 'Južna Dalmacija',
                // 'counties' => array('Dubrovačko-neretvanska'),
                'presets' => array(
                    'ad_location_1' => '1',
                    'ad_location_2' => '842'
                )
            ),
            'sjeverna-dalmacija' => array(
                'name' => 'Sjeverna Dalmacija',
                // 'counties' => array('Šibensko-kninska','Zadarska'),
                'presets' => array(
                    'ad_location_1' => '1',
                    'ad_location_2' => array('6174', '7149')
                )
            ),
            'kvarner-i-primorje' => array(
                'name' => 'Kvarner i Primorje',
                // 'counties' => array('Ličko-senjska','Primorsko-goranska'),
                'presets' => array(
                    'ad_location_1' => '1',
                    'ad_location_2' => array('3460', '4559')
                )
            ),
            'kontinentalna-hrvatska' => array(
                'name' => 'Kontinentalna Hrvatska',
                /*
                'counties' => array(
                    'Bjelovarsko-bilogorska', 'Međimurska', 'Krapinsko-zagorska', 'Varaždinska', 'Zagrebačka',
                    'Grad Zagreb', 'Sisačko-moslavačka', 'Koprivničko-križevačka', 'Karlovačka', 'Ličko-senjska',
                    'Virovitičko-podravska', 'Vukovarsko-srijemska', 'Brodsko-posavska', 'Osječko-baranjska'
                ),
                */
                'presets' => array(
                    'ad_location_1' => '1',
                    'ad_location_2' => array(
                        '232', '3755', '3002', '6416', '7760',
                        '7442', '5188', '2682', '1867', '3460',
                        '6766', '6985', '588', '3920',
                    )
                )
            ),
            'inozemstvo' => array(
                'name' => 'Inozemstvo',
                // 'counties' => 'Inozemstvo',
                'presets' => array(
                    // 'ad_location_1' => '',
                    'ad_location_2' => '8571'
                )
            )
        );

        return $data;
    }

    public function getBounds()
    {
        $bounds = array();
        if (isset($this->north) && !empty($this->north)) {
            $bounds['north'] = $this->north;
        }
        if (isset($this->south) && !empty($this->south)) {
            $bounds['south'] = $this->south;
        }
        if (isset($this->west) && !empty($this->west)) {
            $bounds['west'] = $this->west;
        }
        if (isset($this->east) && !empty($this->east)) {
            $bounds['east'] = $this->east;
        }

        return count($bounds) == 4 ? $bounds : null;
    }
}
