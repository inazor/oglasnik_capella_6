<?php

namespace Baseapp\Models;

class MediaCrops extends BaseModelBlamable
{
    public $id;

    public $media_id;
    public $style_id;

    public $x;
    public $y;
    public $width;
    public $height;

    public function initialize()
    {
        parent::initialize();

        $this->belongsTo(
            'media_id', __NAMESPACE__ . '\Media', 'id',
            array(
                'alias'      => 'Media',
                'reusable'   => true,
            )
        );

        $this->belongsTo(
            'style_id', __NAMESPACE__ . '\ImageStyles', 'id',
            array(
                'alias'      => 'ImageStyle',
                'reusable'   => true,
            )
        );
    }

    public function getSource()
    {
        return 'media_crops';
    }
} 
