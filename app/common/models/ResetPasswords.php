<?php

namespace Baseapp\Models;

/**
 * ResetPasswords
 * Stores the reset password tokens and their evolution
 *
 * Expired reset password tokens should be deleted automatically
 * Successfully used password tokens should be deleted
 */
class ResetPasswords extends BaseModelBlamable
{
    // 2 hours in seconds
    const EXPIRES_AFTER = 7200;

    /**
     * @var string Generated random token in plain text (to be sent in an email, it's stored hashed in the database)
     */
    private $token_plain = null;

    public function initialize()
    {
        parent::initialize();

        $this->skipAttributes(array(
            // not storing plain text token in the database
            'token_plain'
        ));

        $this->belongsTo('user_id', __NAMESPACE__ . '\Users', 'id', array(
            'alias' => 'user'
        ));

        // Do randomized garbage collection
        if (mt_rand(1, 100) === 1) {
            $this->delete_expired();
        }

        // Check and immediately delete this specific record if it's expired
        if (property_exists($this, 'expires') && null !== $this->expires && $this->expires < time()) {
            $this->delete();
        }
    }

    /**
     * Deletes ALL expired tokens
     */
    public function delete_expired()
    {
        $this->getDI()->getShared('db')->execute('DELETE FROM `' . $this->getSource() . '` WHERE `expires` < :time', array(':time' => time()));
    }

    /**
     * Deletes all the tokens belonging to a specific user
     * If no specific $user_id is provided, it takes the $user_id of the current token owner
     *
     * @param int|null $user_id The user_id for which to delete all tokens
     */
    public function delete_user_tokens($user_id = null)
    {
        // if no user_id is specified, deletes the current user's tokens
        if (null === $user_id) {
            $user_id = $this->user->id;
        }

        // WARN: this keeps no records about the deletion in audit tables
        $this->getDI()->getShared('db')->execute('DELETE FROM `' . $this->getSource() . '` WHERE `user_id` = :user_id', array(':user_id' => (int) $user_id));

        /*
        $existing_tokens = $this->find(array('user_id=:user_id:', 'bind'=> array('user_id' => (int) $user_id)));
        foreach ($existing_tokens as $existing_token) {
            $existing_token->delete();
        }
        */
    }

    /**
     * Generates a random string that can be used safely in URLs
     *
     * @return string Random string used as a password reset token (effectively a one-time-password)
     */
    public function generate_random_token()
    {
        // TODO: in theory, we should check if a token we just generated already exists in the db
        // and create another one...
        $code = preg_replace('/[^a-zA-Z0-9]/', '', base64_encode(openssl_random_pseudo_bytes(24)));
        return $code;
    }

    /**
     * Returns the current (randomly generated) token in plain text.
     * This is to be sent in an email message to the user.
     *
     * If the random token has not been generated yet, it will be before returning it.
     *
     * @return string Random token (plain text) to be used as a password reset token
     */
    public function get_token_plain()
    {
        if (null === $this->token_plain) {
            $this->token_plain = $this->generate_random_token();
        }
        return $this->token_plain;
    }

    /**
     * Before create the user assign a password
     */
    public function beforeValidationOnCreate()
    {
        // set expiry
        $this->expires = time() + self::EXPIRES_AFTER;

        /**
         * We hash the token before it's stored in the database so that anyone
         * with "DB only" access cannot reset users' passwords easily
         *
         * When the token is sent in an email, it's plain text form is sent
         * and when we validate the token we compare the hashes
         */
        $this->token = $this->getDI()->getShared('auth')->hash($this->get_token_plain());
    }

    /**
     * Sends an e-mail to users automatically after creating the token record,
     * allowing them to reset their password
     */
    public function afterCreate()
    {
        $token = $this->get_token_plain();
        $this->user->send_pwdreset_email($token);
    }
}
