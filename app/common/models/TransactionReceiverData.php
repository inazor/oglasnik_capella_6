<?php

namespace Baseapp\Models;

class TransactionReceiverData
{
    public $name;
    public $street;
    public $place;
    public $iban;
    public $model;
    public $reference;

    public static function fromObject($object)
    {
        $instance            = new self();
        $instance->name      = $object->name;
        $instance->street    = $object->street;
        $instance->place     = $object->place;
        $instance->iban      = $object->iban;
        $instance->model     = $object->model;
        $instance->reference = $object->reference;

        return $instance;
    }

    /**
     * @param int|string $reference_no
     *
     * @return \stdClass
     */
    public static function createOglasnikReceiverObject($reference_no)
    {
        $receiver            = new \stdClass();
        $receiver->name      = 'Oglasnik d.o.o.';
        $receiver->street    = 'Savska cesta 41';
        $receiver->place     = '10000 Zagreb';
        $receiver->iban      = 'HR1624840081100153885';
        $receiver->model     = '00';
        $receiver->reference = $reference_no;

        return $receiver;
    }
}
