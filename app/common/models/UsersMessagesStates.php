<?php

namespace Baseapp\Models;

use Baseapp\Library\Utils;
use Baseapp\Library\RawDB;

class UsersMessagesStates extends BaseModelBlamable
{
    public $id;
    public $user_id;
    public $user_message_id;
    public $read_at;
    public $deleted = 0;

    public function initialize()
    {
        parent::initialize();

        $this->belongsTo(
            'user_id', __NAMESPACE__ . '\Users', 'id',
            array(
                'alias'      => 'User',
                'reusable'   => true,
            )
        );
        $this->belongsTo(
            'user_message_id', __NAMESPACE__ . '\UsersMessages', 'id',
            array(
                'alias'      => 'Message',
                'reusable'   => true,
            )
        );
    }

    public function getSource()
    {
        return 'users_messages_states';
    }

    public static function markAsRead($user_id, $message_id)
    {
        $msg_ids = array();

        if (is_array($message_id)) {
            $msg_ids = $message_id;
        } elseif (strpos($message_id, ',') !== false) {
            $msg_ids = explode(',', $message_id);
        } elseif ((int) $message_id) {
            $msg_ids[] = (int) $message_id;
        }

        if (count($msg_ids)) {
            $readAt = date('Y-m-d H:i:s', Utils::getRequestTime());
            $values = array();
            foreach ($msg_ids as $msg_id) {
                $values[] = "($user_id, $msg_id, '$readAt')";
            }

            $sql = 'INSERT INTO ' . (new self)->getSource() . ' (user_id, user_message_id, read_at) VALUES ' . implode(', ', $values);

            $db = new RawDB();
            return $db->query($sql);
        }

        return false;
    }

    public static function getOrCreateNew($user_message)
    {
        $state = self::findFirst(array(
            'user_id = :user_id: AND user_message_id = :user_message_id:',
            'bind' => array(
                'user_id'         => $user_message->user_id,
                'user_message_id' => $user_message->id
            )
        ));
        if (!$state) {
            $state = new self();
            $state->user_id = $user_message->user_id;
            $state->user_message_id = $user_message->id;
            $state->read_at = date('Y-m-d H:i:s', Utils::getRequestTime());
            $state->deleted = 0;
        }

        return $state;
    }

}
