<?php

namespace Baseapp\Traits;

use Baseapp\Bootstrap;
use Baseapp\Models\Audit;

trait AuditLog
{
    /**
     * Stores an arbitrary audit log message using the Audit model.
     * Collects info from the dispatcher about the current controller (or cli task), action and parameters.
     *
     * Returns whatever Audit::save() returns.
     *
     * @param string|null $message Optional, defaults to current request uri (or cli args). Info about whatever occurred.
     *
     * @return bool
     */
    protected function saveAudit($message = null)
    {
        $audit = new Audit();

        // We collect different things based on where we're called from
        if ('cli' === PHP_SAPI) {
            // Grab CLI mode data
            $audit->user_id  = null;
            $audit->username = 'system';

            $audit->controller = 'CLI Task: ' . $this->dispatcher->getTaskName();

            $audit->ip = 'cli';

            // If no explicit message provided, store the argv as message
            if (empty($message)) {
                $audit->message = implode(' ', $_SERVER['argv']);;
            } else {
                $audit->message = $message;
            }
        } else {
            // Grab standard request mode data
            $request = $this->getDI()->getRequest();

            // User details, if known
            $user = $this->getDI()->getShared('auth')->get_user();
            if ($user) {
                $audit->user_id  = $user->id;
                $audit->username = $user->username;
            } else {
                $audit->user_id  = null;
                $audit->username = 'anonymous';
            }

            $audit->controller = $this->dispatcher->getControllerName();
            $audit->ip = $request->getClientAddress(true);

            // If no explicit message provided, store the request uri as message
            if (empty($message)) {
                $audit->message = $request->getURI();
            } else {
                $audit->message = $message;
            }
        }

        // Controller action and params (fetched same way regardless of which app is called)
        $audit->action = $this->dispatcher->getActionName();
        $params = $this->dispatcher->getParams();
        if (!empty($params)) {
            $audit->params = var_export($params, true);
        }

        // A few more details...
        $audit->type = 'L';
        $audit->created_at = date('Y-m-d H:i:s');

        $saved = $audit->save();
        if (!$saved) {
            Bootstrap::log($audit->getMessages());
        }

        return $saved;
    }
}
