<?php

namespace Baseapp\Traits;

use Baseapp\Library\PDF417;
use Baseapp\Models\Orders;
use Baseapp\Models\Users;
use Baseapp\Models\TransactionData;
use Baseapp\Models\TransactionReceiverData;
use Phalcon\Http\ResponseInterface;

trait BarcodeControllerMethods
{
    /**
     * Outputs a barcode image for the specified Order (based on the token, and the order
     * assigned to that token)
     *
     * @param string|null $token
     *
     * @return ResponseInterface
     */
    public function barcodeAction($token = null)
    {
        $format = $this->request->getQuery('format', 'string', 'png');

        $token = $this->request->getQuery('token', 'string', $token);
        $token_data = $this->getSessionTokenData($token);

        $errors = ! $this->hasRequiredTokenData($token_data, array('order_id'));

        if (!$errors) {
            $order = Orders::findFirst($token_data['order_id']);
            if (!$order) {
                $errors = true;
            }
        }

        if ($errors) {
            $this->do_403();
        } else {
            $this->disable_view();
            $img = $this->getHub3ImgBarcode($order, array('format' => $format));
            $this->response->setContent($img);
            $this->response->setContentType('image/' . $format);
            return $this->response;
        }
    }

    public function barcodeOrderAction($order_id = null)
    {
        $format = $this->request->getQuery('format', 'string', 'png');

        $errors = false;

        $order = Orders::findFirst($order_id);
        if (!$order) {
            $errors = true;
        }

        // Check the order belongs to the current user
        if ($order->user_id != $this->current_user->id) {
            $errors = true;
        }

        if ($errors) {
            $this->do_403();
        } else {
            $this->disable_view();
            $img = $this->getHub3ImgBarcode($order, array('format' => $format));
            $this->response->setContent($img);
            $this->response->setContentType('image/' . $format);
            return $this->response;
        }
    }

    public function getHub3ImgBarcode(Orders $order, $options = array())
    {
        $data = $this->buildHub3BarcodeData($order);

        $default = [
            'format'  => 'png',
            'quality' => 90,
            'scale'   => 2,
            'ratio'   => 3,
            'padding' => 0,
            'bgColor' => '#f9f9f9'
        ];
        $opts = array_merge($default, $options);

        $img_renderer = new PDF417\Renderers\ImagickImageRenderer($opts);
        $img = $img_renderer->render($data);

        return $img;
    }

    public function getHub3SvgBarcode(Orders $order)
    {
        $data = $this->buildHub3BarcodeData($order);

        $renderer = new PDF417\Renderers\SvgRenderer(array(
            'pretty' => false,
            'scale' => 2,
            'ratio' => 3,
            'color' => 'black',
        ));
        $svg = $renderer->render($data);

        return $svg;
    }

    public function buildHub3BarcodeData(Orders $order)
    {
        $transaction = new \stdClass();
        $transaction->amount = $order->getTotal();
        $transaction->purpose = 'SCVE';
        $transaction->description = 'Plaćanje po narudžbi ' . $order->getPbo();

        $sender = new \stdClass();

        $user = null;
        if (isset($this->auth) && $this->auth->get_user()) {
            $user = $this->auth->get_user();
        }

        if (!$user && isset($order->user_id)) {
            $user = Users::findFirst($order->user_id);
        }

        if ($user) {
            $sender->name = $user->first_name;
            if (!empty($user->last_name)) {
                $sender->name .= ' ' . $user->last_name;
            }
            if (!empty($user->address)) {
                $sender->street = $user->address;
            }
            if (!empty($user->city) && !empty($user->zip_code)) {
                $sender->place = $user->zip_code . ' ' . $user->city;
            }
        } else {
            $sender->name   = '';
            $sender->street = '';
            $sender->place  = '';
        }

        $transaction->sender   = $sender;
        $transaction->receiver = TransactionReceiverData::createOglasnikReceiverObject($order->getPbo());

        $transaction_data        = TransactionData::fromObject($transaction);
        $transaction_data_string = $transaction_data->toString();

        $pdf417 = new PDF417();
        // Required by HUB3 standard
        $pdf417->setSecurityLevel(4);
        $pdf417->setColumns(9);

        $data = $pdf417->encode($transaction_data_string);

        return $data;
    }
}
