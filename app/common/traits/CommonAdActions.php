<?php

namespace Baseapp\Traits;

use Baseapp\Models\Ads;
use Baseapp\Library\Tool;
use Baseapp\Library\Utils;
use Baseapp\Library\RawDB;

/**
 * Trait CommonAdActions
 */
trait CommonAdActions
{

    /**
     * Get user's paginated ads
     *
     * @param \Baseapp\Models\Users $user User we'll working with
     * @param string $listing_type Type of ads we want (active/expired/deactivated/inactive)
     * @param null|string $pagination_base_url Base url
     * @param null|int $limit Amount of records per page. If null, defaults to config value for frontend pagination
     * @param null|string $searchTerm Search term to filter ads
     *
     * @return null|array
     */
    protected function getPaginatedAds($user, $listing_type, $pagination_base_url = null, $limit = null, $searchTerm = null)
    {
        $resulting_array = null;

        if (null === $limit) {
            $limit = $this->config->settingsFrontend->pagination_items_per_page;
        }

        $return_builder = true;
        $ads = new Ads();
        switch($listing_type) {
            case 'all':
                $builder = $ads->getAllAds($user->id, $return_builder);
                $pagination_base_url = !$pagination_base_url ? 'moj-kutak/svi-oglasi' : $pagination_base_url;
                break;
            case 'active':
                $sort_column = null;
                $sort = $this->request->get('sort', 'string', $this->config->adSorting->default_value);
                if (isset($this->config->adSorting[$sort])) {
                    $sort_column = $this->config->adSorting[$sort]['column'];
                }
                $builder = $ads->getActiveAds($user->id, $return_builder, $sort_column);
                $pagination_base_url = !$pagination_base_url ? 'moj-kutak/aktivni-oglasi' : $pagination_base_url;
                break;
            case 'expired':
                $builder = $ads->getExpiredAds($user->id, $return_builder);
                $pagination_base_url = !$pagination_base_url ? 'moj-kutak/istekli-oglasi' : $pagination_base_url;
                break;
            case 'deactivated':
                $builder = $ads->getDeactivatedAds($user->id, $return_builder);
                $pagination_base_url = !$pagination_base_url ? 'moj-kutak/deaktivirani-oglasi' : $pagination_base_url;
                break;
            case 'inactive':
                $builder = $ads->getInactiveAds($user->id, $return_builder);
                $pagination_base_url = !$pagination_base_url ? 'moj-kutak/neobjavljeni-oglasi' : $pagination_base_url;
                break;
            case 'sold':
                $builder = $ads->getSoldAds($user->id, $return_builder);
                $pagination_base_url = !$pagination_base_url ? 'moj-kutak/prodani-oglasi' : $pagination_base_url;
                break;
            case 'favorite':
                $builder = $ads->getFavoriteAds($user->id, $return_builder);
                $pagination_base_url = !$pagination_base_url ? 'moj-kutak/spremljeni-oglasi' : $pagination_base_url;
                break;
        }

        if (isset($builder)) {
            $page = 1;
            if ($this->request->hasQuery('page')) {
                $page = $this->request->getQuery('page', 'int', 1);
                if ($page <= 0) {
                    $page = 1;
                }
            }

            if ($searchTerm) {
                $searchTerm = trim($searchTerm);
                $parsed_searchTerm = Utils::strip_boolean_operator_characters(Utils::remove_accents($searchTerm));
                $parsed_searchTerm = Utils::build_boolean_mode_operators($parsed_searchTerm, '+');
                $parsed_searchTerm = Utils::quote_unquoted_words_with_dots($parsed_searchTerm);
                $parsed_searchTerm_lc = trim(mb_strtolower($parsed_searchTerm, 'UTF-8'));

                $builder->innerJoin('Baseapp\Models\AdsSearchTerms', 'ad.id = ast.ad_id', 'ast');
                $builder->andWhere(
                    'FULLTEXT_MATCH_BMODE(ast.search_data_unaccented, :parsed_searchTerm:)',
                    array(
                        'parsed_searchTerm' => $parsed_searchTerm_lc
                    ),
                    array(
                        'parsed_searchTerm' => \PDO::PARAM_STR
                    )
                );
            }

            $paginator = new \Phalcon\Paginator\Adapter\QueryBuilder(array(
                'builder' => $builder,
                'limit'   => $limit,
                'page'    => $page
            ));
            $current_page = $paginator->getPaginate();

            // Trigger a 404 page for a non-existent page number (but only when there are actual results to show)
            if ($current_page && $current_page->total_pages > 0 && $page > $current_page->total_pages) {
                $resulting_array = array(
                    'error'       => true,
                    'trigger_404' => true
                );
            } else {
                // Process the resulting page and extend with some additional data (ad image if it exists)
                $results_array = Ads::buildResultsArray($current_page->items, 'frontend', $user);
                $current_page->items = $results_array;

                $pagination_links = Tool::pagination(
                    $current_page,
                    $pagination_base_url,
                    'pagination',
                    $this->config->settingsFrontend->pagination_count_out,
                    $this->config->settingsFrontend->pagination_count_in,
                    $this->router->getModuleName() == 'frontend' ? 'hr' : 'en'
                );

                $resulting_array = array(
                    'error'      => false,
                    'ads'        => $current_page->items,
                    'total'      => $current_page->total_items,
                    'pagination' => $pagination_links
                );
            }
        }

        return $resulting_array;
    }

    public function getCumulativeAdsCounters($user, $searchTerm = null)
    {
        if ($searchTerm) {
            $searchTerm = trim($searchTerm);
            $parsed_searchTerm = Utils::strip_boolean_operator_characters(Utils::remove_accents($searchTerm));
            $parsed_searchTerm = Utils::build_boolean_mode_operators($parsed_searchTerm, '+');
            $parsed_searchTerm = Utils::quote_unquoted_words_with_dots($parsed_searchTerm);
            $parsed_searchTerm_lc = trim(mb_strtolower($parsed_searchTerm, 'UTF-8'));

            $sql = "SELECT
                    COUNT(*) as total,
                    SUM(IF(ad.active = 1, 1, 0)) AS total_active,
                    SUM(IF(ad.manual_deactivation = 1, 1, 0)) AS total_deactivated,
                    SUM(IF((ad.active = 0 AND ad.manual_deactivation = 0 AND ad.sold = 0 AND ad.moderation IN ('waiting', 'nok', 'ok') AND (ad.expires_at IS NULL OR ad.expires_at >= '" . Utils::getRequestTime() . "')), 1, 0)) AS total_unpublished,
                    SUM(IF((ad.active = 0 AND ad.expires_at < '" . Utils::getRequestTime() . "'), 1, 0)) AS total_expired,
                    SUM(IF(ad.sold = 1, 1, 0)) AS total_sold
                FROM ads AS ad
                INNER JOIN ads_search_terms AS ast ON ad.id = ast.ad_id
                WHERE ad.soft_delete = 0 AND ad.user_id = " . $user->id . " AND MATCH(ast.search_data_unaccented) AGAINST ('" . $parsed_searchTerm_lc . "' IN BOOLEAN MODE)";
        } else {
            $sql = "SELECT
                    COUNT(*) as total,
                    SUM(IF(ad.active = 1, 1, 0)) AS total_active,
                    SUM(IF(ad.manual_deactivation = 1, 1, 0)) AS total_deactivated,
                    SUM(IF((ad.active = 0 AND ad.manual_deactivation = 0 AND ad.sold = 0 AND ad.moderation IN ('waiting', 'nok', 'ok') AND (ad.expires_at IS NULL OR ad.expires_at >= '" . Utils::getRequestTime() . "')), 1, 0)) AS total_unpublished,
                    SUM(IF((ad.active = 0 AND ad.expires_at < '" . Utils::getRequestTime() . "'), 1, 0)) AS total_expired,
                    SUM(IF(ad.sold = 1, 1, 0)) AS total_sold
                FROM ads AS ad
                WHERE ad.soft_delete = 0 AND ad.user_id = " . $user->id;
        }

        $db = new RawDB(array(
            'options'      => array(
                \PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8',
                \PDO::ATTR_EMULATE_PREPARES   => false,
                \PDO::ATTR_STRINGIFY_FETCHES  => false,
            )
        ));

        return $db->findFirst($sql);
    }
}
