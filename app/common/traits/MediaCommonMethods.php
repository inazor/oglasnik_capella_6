<?php

namespace Baseapp\Traits;

use Baseapp\Models\Ads;
use Baseapp\Models\CmsArticles;
use Baseapp\Models\Media;
use Baseapp\Models\Categories;

trait MediaCommonMethods
{
    /**
     * Returns Media belonging to this entity using a raw PHQL JOIN
     *
     * @param null|int $limit
     *
     * @return \Phalcon\Mvc\Model\ResultsetInterface
     */
    public function get_media($limit = null)
    {
        // TODO: implement some caching for potential multiple calls during the same request

        $order_by = 'ORDER BY sort_idx';

        if ($this instanceof Ads) {
            $link_model_name = 'Baseapp\Models\AdsMedia';
            $link_model_pk = 'ad_id';
        } elseif ($this instanceof CmsArticles) {
            $link_model_name = 'Baseapp\Models\CmsArticlesMedia';
            $link_model_pk = 'article_id';
        }

        $phql = 'SELECT m.* ' .
            'FROM Baseapp\Models\Media AS m ' .
            'JOIN ' . $link_model_name . ' AS linked_model ' .
            'ON m.id = linked_model.media_id ' .
            'WHERE linked_model.' . $link_model_pk .' = ' . $this->id .
            $order_by;

        if (null !== $limit) {
            $phql .= ' LIMIT ' . (int) $limit;
        }

        $linked_media = $this->getModelsManager()->executeQuery($phql);

        return $linked_media;
    }

    /**
     * Get a thumb (or any valid ImageStyle really) of the Ad's first Media
     *
     * @param null $style {@see \Baseapp\Models\Media::styles_adapter()}
     * @param bool $force_create
     * @param Categories|int $category {@see \Baseapp\Models\Media::get_thumb()}
     *
     * @return \Baseapp\Library\Images\Thumb|null
     */
    public function get_thumb($style = null, $force_create = false, $category = null)
    {
        $thumb = null;

        $linked_media = $this->get_media(1);
        if ($linked_media) {
            $first = $linked_media->getFirst();
            if ($first) {
                /* @var $first Media */
                $thumb = $first->get_thumb($style, $force_create, $category);
            }
        }

        if (!$thumb) {
            // Prep a default thumb for cases when there is no media at all or thumb creation failed
            $category_fallback_image = null;
            if ($category) {
                $category_fallback_image = Categories::getFallbackImage($category);
            }
            $thumb = (new Media())->getNoImageThumb($style, ':(', $category_fallback_image);
        }

        return $thumb;
    }
}
