<?php

namespace Baseapp\Traits;

trait NestedSetActions
{

    /**
     * Get node's children IDs
     * @param  boolean $include_root_node_id Do we want to include root node's IDs?
     * @return array                         Requested IDs or an empty array in clase a root node was not found
     */
    public function getChildrenIDs($include_root_node_id = false)
    {
        $children_ids = array();
        if ($include_root_node_id) {
            $children_ids[] = (int) $this->id;
        }
        foreach ($this->descendants() as $child) {
            $children_ids[] = (int) $child->id;
        }
        return $children_ids;
    }

}
