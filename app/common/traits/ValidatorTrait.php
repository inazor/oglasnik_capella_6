<?php

namespace Baseapp\Traits;

use Phalcon\Validation;

trait ValidatorTrait
{
    /** @var Validation */
    protected $validator;
    protected $attribute;

    /**
     * @param Validation $validator
     * @param $attribute
     * @return bool
     */
    public function validate(Validation $validator, $attribute)
    {
        $this->validator = $validator;
        $this->attribute = $attribute;

        $value = $this->resolveValue();

        if (is_array($value)) {
            return $this->validateArray($value);
        }

        return $this->validateSingle($value);
    }

    /**
     * @param array $values
     * @return bool
     */
    protected function validateArray(array $values)
    {
        $valid = true;

        foreach ($values As $value) {
            if (is_array($value)) {
                $valid = $valid && $this->validateArray($value);
            } else {
                $valid = $valid && $this->validateSingle($value);
            }
        }

        return $valid;
    }

    private function resolveValue()
    {
        $name = $this->resolveName();
        if (is_array($name)) {
            $value = $this->validator->getValue($name[1]);
            return isset($value[$name[2]]) ? $value[$name[2]] : null;
        }
        return $this->validator->getValue($name);
    }

    private function resolveName()
    {
        $matches = array();
        if (preg_match('/^([a-zA-Z0-9\-\_]+)\[([a-zA-Z0-9\-\_]*)\](\[([a-zA-Z0-9\-\_]*)\])?$/', $this->attribute, $matches)) {
            return $matches;
        }
        return $this->attribute;
    }

    abstract protected function validateSingle($value);
}
