<?php

namespace Baseapp\Frontend\Controllers;

use Phalcon\Dispatcher;
use Baseapp\Bootstrap;
use Baseapp\Library\AdsFiltering;
use Baseapp\Library\LandingPagesHelpers;
use Baseapp\Library\MapSearch;
use Baseapp\Library\Utils;
use Baseapp\Models\Ads;
use Baseapp\Models\AdsTotalsCache;
use Baseapp\Models\Categories;
use Baseapp\Models\Dictionaries;
use Baseapp\Models\Locations;
use Baseapp\Models\UsersEmailAgents;
use Baseapp\Models\UsersShopsFeatured;
use Baseapp\Traits\CategoryTreeHelpers;
use Baseapp\Traits\RandomSpecialProductsHelpers;

/**
 * Frontend Categories Controller
 */
class CategoriesController extends IndexController
{
    use RandomSpecialProductsHelpers;
    use CategoryTreeHelpers;

    protected $sort_data;
    protected $maintain_sort_param = false;

    protected $category_ids = array();

    /**
     * Overriding afterExecuteRoute in order to provide some global view variables.
     *
     * @param Dispatcher $dispatcher
     *
     * @return bool
     */
    public function afterExecuteRoute(Dispatcher $dispatcher)
    {
        // Making sure the parent is called too!
        parent::afterExecuteRoute($dispatcher);

        $this->add_default_sidebar_banner_zones();

        $this->add_banner_zone('mobile_top', '2285080', '<div id="zone2285080" class="goAdverticum"></div>');
        $this->add_banner_zone('mobile_pagination', '2285057', '<div id="zone2285057" class="goAdverticum"></div>');

        $this->add_banner_zone('listing_7', '2285047', '<div id="zone2285047" class="goAdverticum"></div>');
        $this->add_banner_zone('listing_14', '4084895', '<div id="zone4084895" class="goAdverticum"></div>');

        $this->view->setVar('banner_zone_ids', $this->get_active_zone_ids());
    }

    /**
     * If this action is executed (forwarded to) it means we weren't able to confirm
     * the visitors minimum required age (needed/specified on the category level) and we shouldn't let them
     * see the content until they confirm they're old enough
     *
     * @param array $restriction
     * @param bool $auto_deny
     * @param string|null $next_url
     * @param array $og_meta Array of open graph data (key/value)
     *
     * @return mixed
     */
    public function ageRestrictionAction($restriction, $auto_deny, $next_url, $og_meta = array())
    {
        // Only allowing access to this action via forwarding for now
        if (!$this->dispatcher->wasForwarded()) {
            return $this->trigger_404();
        }

        $category = Categories::findFirst($restriction['category_id']);

        $this->view->pick('categories/age-restriction');

        // Override potentially set 'og:image' with a custom one for age-restricted requests (or set one if not specified)
        $og_meta['og:image'] = $this->url->get('assets/img/fb-age-restricted-og-image.jpg');
        $this->setOpenGraphMeta($og_meta);

        // Open Graph description overrides any potentially previously set site meta desc (even the default one)
        if (isset($og_meta['og:description'])) {
            $this->site_desc = $this->escaper->escapeHtmlAttr($og_meta['og:description']);
        }

        $this->view->setVar('category', $category);
        $this->view->setVar('age_restriction_html', $restriction['html']);
        $this->view->setVar('auto_deny', $auto_deny);

        // Setting up the next_url view var and making it at least somewhat safe
        $url = $next_url;
        if (empty($url) && isset($restriction['url']) && !empty($restriction['url'])) {
            $url = $restriction['url'];
        }

        if (!empty($url)) {
            $url = $this->get_safe_redir_path($url);
        }

        $this->view->setVar('next_url', $url);
    }

    public function confirmAgeAction($entity_id)
    {
        $next_url = $this->request->get('next', 'string', null);
        if (!empty($next_url)) {
            $next_url = $this->get_safe_redir_path($next_url);
        }

        $entity_id = (int) $entity_id;
        if (!$entity_id) {
            $this->flashSession->error('Nedostaje ID kategorije!');
            return $this->redirect_back();
        }

        $category = Categories::findFirst($entity_id);

        if (!$category) {
            $this->flashSession->error(sprintf('Kategorija nije pronađena [ID: %s]', $entity_id));
            return $this->redirect_back();
        }

        $age_restriction = $category->getAgeRestriction();
        if ($age_restriction) {
            $this->session->set('category_' . $age_restriction['category_id'] . '_age_confirm', true);
        }

        return $this->redirect_to($next_url ? $next_url : $category->get_url());
    }

    /**
     * Currently used to decide if inactive categories/dictionaries should "work" or not (when opening direct/old
     * links, or for the purposes of them testing stuff out before publishing it).
     *
     * @return bool Returns true if the user has a "higher" role.
     */
    protected function isPrivilegedUser()
    {
        $user = $this->auth->get_user();
        if (!$user) {
            $result = false;
        } else {
            // If user has any of these roles, we consider it privileged
            $result = $user->hasRole(array('admin', 'content', 'support', 'supersupport', 'sales', 'finance', 'moderator', 'developer'));
        }

        return $result;
    }

    /**
     * Returns nothing for privileged users, allowing them to (pre)view inactive categories/dictionaries.
     * Regular/anon users should only see 'active' stuff.
     *
     * @return string
     */
    protected function getActiveSqlConditionDependingOnUser()
    {
        $active_condition = ' AND active = 1';
        if ($this->isPrivilegedUser()) {
            $active_condition = '';
        }

        return $active_condition;
    }

    /**
     * SEO helper action which forwards to viewAction with the appropriate params
     *
     * @return mixed
     */
    public function slugAction()
    {
        // Removing first forward slash
        $full_url = ltrim($this->request->getURI(), '/');

        // Get everything before the question mark, effectively stripping the query portion of the url
        $full_slug = strtok($full_url, '?');

        // Forwarding to view action which knows how to deal with these things
        return $this->dispatcher->forward(
            array(
                'action' => 'view',
                'params' => array($full_slug)
            )
        );
    }

    /**
     * View Action
     */
    public function viewAction($params = null)
    {
        $id_type = 'id';
        $value   = null;

        if (!empty($params)) {
            if (is_array($params)) {
                // If it's an array, join all values with a '/' and query by that slug
                $id_type = 'url';
                $value = implode('/', $params);
            } elseif (ctype_digit($params)) {
                $value = (int) $params;
            } else {
                $id_type = 'url';
                $value = trim($params);
            }
        }

        if (!$value) {
            return $this->trigger_404();
        }

        $active_condition = $this->getActiveSqlConditionDependingOnUser();

        if ('id' === $id_type) {
            $category = Categories::findFirst(
                array(
                    'conditions' => 'id = :id' . $active_condition,
                    'bind' => array(
                        'id' => $value
                    )
                )
            );
        } else {
            $category = Categories::findFirst(
                array(
                    'conditions' => 'url = :url:' . $active_condition,
                    'bind'       => array(
                        'url' => $value
                    )
                )
            );
        }

        $page_title_suffix = null;
        $preset_filters_data = null;
        if (!$category) {
            $do_404 = true;

            // Last attempt, if we couldn't match anything, check if the last part of
            // the url matches certain dictionary values (dependable dropdowns only for now), and if so,
            // show results as if those values were chosen via filtering
            $parts = explode('/', $value);
            $data = $this->prepareCategoryAndPresetFiltersDataFromUrlParts($parts);
            if (!empty($data)) {
                $do_404 = $data['do_404'];

                // Important to leave this $category variable or code further below breaks since it
                // relies on this kind-of-global variable...
                // TODO/FIXME: refactor so it uses a controller `category` member or something
                $category = $data['category'];

                if (isset($data['preset_filters_data'])) {
                    $preset_filters_data = $data['preset_filters_data'];
                } else {
                    // If no preset data was found, actually trigger a 404 since it means an
                    // invalid param/slug was specified or similar...
                    $do_404 = true;
                }

                if (isset($data['found_nodes'])) {
                    $page_title_parts = array();
                    foreach ($data['found_nodes'] as $node) {
                        if ($node instanceof \stdClass) {
                            $node_name = $node->name;
                        } elseif (is_array($node) && isset($node['name'])) {
                            $node_name = $node['name'];
                        }

                        if (!empty($node_name)) {
                            $page_title_parts[] = $node_name;
                        }
                    }
                    $page_title_suffix = implode(' ', $page_title_parts);
                }
            }

            if ($do_404) {
                return $this->trigger_404();
            }
        }

        /*
        if ($firstChildWithTransactionType = $category->getFirstChildWithTransactionType()) {
            return $this->redirect_to($firstChildWithTransactionType->url, 301);
        }
        */

        // Build OG/SEO meta stuff
        if ($category_settings = $category->getSettings()) {
            $category->set_meta_title($category_settings->meta_title);
            $category->set_meta_description($category_settings->meta_description);
        }

        if (!empty($page_title_suffix)) {
            $page_title_suffix = ' ' . $page_title_suffix;
        }
        $title_parts = array(
            $category->getSEOMeta('title'),
            $page_title_suffix
        );
        $page_title = LandingPagesHelpers::buildTitle($title_parts);
        // $page_title = $category->getSEOMeta('title') . $page_title_suffix . ' | Oglasnik.hr';
        $this->tag->setTitle($page_title);
        $this->site_desc = $category->getSEOMeta('description');

        $og_meta         = array(
            'og:title'       => $page_title,
            'og:url'         => $this->url->get($this->requested_uri), // contains scheme, host, etc
            'og:description' => $this->site_desc
        );
        $this->setOpenGraphMeta($og_meta);

        // Make sure our age checking is finished before doing anything else
        $restricted = $this->ageRestrictionCheckRequest($category->getAgeRestriction(), $og_meta);
        if ($restricted) {
            // Just return since ageRestrictionAction() should've created/disabled the output
            return;
        }

        // TODO/FIXME: just dump generated markup from cached categoryTree
        $this->view->setVar('breadcrumbs', $category->getBreadcrumbs());

        // Check paging parameter
        $page = 1;
        if ($this->request->hasQuery('page')) {
            $page = $this->request->getQuery('page', 'int', 1);
            if ($page <= 0) {
                $page = 1;
            }

            // Someone trying ?page=1 manually, redirect (avoiding duplicate content)
            if ('1' === $_GET['page']) {
                return $this->redirect_to($this->buildHref($category, $page = 1), 301);
            }
        }

/*
        $abort = false;
        if ($page > 50) {
            $this->flashSession->notice('Vaša pretraga je otišla predaleko, pokušajte s preciznijim uvjetima.');
            $abort = true;
        }
*/
        $is_first_page = ($page == 1);

        $this->sort_data = $this->getSortParamData();
        if ($this->sort_data['current'] !== $this->sort_data['default']) {
            $this->maintain_sort_param = true;
        }

        // set the appropriate ads listing viewType (grid/list)
        $this->view->setVar('viewType', $this->getViewTypeData());

/*
        $email_agent_id = $this->request->getQuery('ea_id', 'int', null);
        if ($email_agent_id) {
            $email_agent = UsersEmailAgents::findFirst($email_agent_id);
        } else {
            $email_agent = new UsersEmailAgents();
        }
*/

        $cacheResults  = true;
        $cacheLifeTime = 120;
        $slickLoaded   = false;

        if ($shopping_windows_markup = UsersShopsFeatured::buildCategoryShoppingWindowsMarkup($category, $cacheResults, $cacheLifeTime)) {
            $this->assets->addCss('assets/vendor/slick/slick.css');
            $this->assets->addJs('assets/vendor/slick/slick.min.js');
            $slickLoaded = true;
        }
        $this->view->setVar('shopping_windows_markup', $shopping_windows_markup);
        $this->view->setVar('category', $category);
        $this->view->setVar('category_url', $category->get_url());

        // Pre-select main search dropdown
        $this->view->setVar('main_search_category', $category->id);

        // Show custom landing page links on first page of category listing only
        $landing_pages_links = array();
        $landing_pages_links_counts = array();
        if ($is_first_page) {
            $landing_pages_links = LandingPagesHelpers::getLinksCollection($category->get_url(), $this->requested_path);
            if (!empty($landing_pages_links)) {
                $landing_pages_links_counts = LandingPagesHelpers::getLinksCollectionCounts($landing_pages_links);
            }

            $this->view->setVar('is_first_page', $is_first_page);
        }

        $this->view->setVar(
            'landing_pages_links_markup',
            LandingPagesHelpers::buildVerticalLinksMarkup($landing_pages_links, $landing_pages_links_counts, 4)
        );

/*
        $mapSearchCategory = MapSearch::getRootMapSearchableCategory($category->id);
        $this->view->setVar('mapSearchCategory', $mapSearchCategory);
*/

        $adsFiltering = new AdsFiltering($page);
        $adsFiltering->cacheResults($cacheResults);
        $adsFiltering->cacheLifeTime($cacheLifeTime);
        $adsFiltering->cachePrefix('cat-' . $category->id);
        $adsFiltering->setLoggedInUser($this->auth->get_user());
        $adsFiltering->setSortParamData($this->getSortParamData());
        $adsFiltering->setBaseURL($category->url);
        $adsFiltering->setAdCategory($category);
        $adsFiltering->getChildCategoriesAsArray(true);
        $adsFiltering->autodetectFiltersFromCategory(true);
        $adsFiltering->setPresetFiltersData($preset_filters_data);
        $adsFiltering->allowSpecialProducts(false);

        $ads = $adsFiltering->getAds();

        // zyt: Need to get and store this info before getSpecialProducts() clobbers all over these flags
        // in order to use them further below to store AdsTotalsCache
        $db_hit = $adsFiltering->dbHit();
        $user_initiated_filtering = $adsFiltering->wasFilteredBecauseOfUserAction();

        $this->view->setVar('user_initiated_filtering', $user_initiated_filtering);

        if ($flashSession = $adsFiltering->getFlashSession()) {
            $this->flashSession->$flashSession['type']($flashSession['text']);
        }
        if ($adsFiltering->shouldTrigger404()) {
            return $this->trigger_404();
        }
        $specialProductsArray = $adsFiltering->getSpecialProducts($hydrate = true, $limit = null, $random = true);

        if (!$adsFiltering->hasError()) {
            $this->buildSortingDropdown();

            // leave this here for now in case we'll have to rollback carousel changes
/*
            if ($specialProductsArray) {
                if (is_array($ads) && !empty($ads)) {
                    $ads = array_merge($specialProductsArray, $ads);
                }
            }
*/

            if ($specialProductsArray) {
                $carouselNeeded       = false;
                $dummyAds             = array();
                $specialProductsCount = count($specialProductsArray);

                if ($specialProductsCount > 3) {
                    $carouselNeeded = true;
                    $dummyAdsNeeded = 3 - (count($specialProductsArray) % 3);
                    if (3 > $dummyAdsNeeded) {
                        $dummyAds = Ads::getMarkupArrayWithDummyProductAds($specialProductsArray[0]['online_product_id'], $dummyAdsNeeded);
                    }
                }
                $this->view->setVar('specialProductsArray', $specialProductsArray);
                $this->view->setVar('dummyAds', $dummyAds);

                if ($carouselNeeded && !$slickLoaded) {
                    $this->assets->addCss('assets/vendor/slick/slick.css');
                    $this->assets->addJs('assets/vendor/slick/slick.min.js');
                    $slickLoaded = true;
                }
                $this->view->setVar('carouselNeeded', $carouselNeeded);
            }

            $this->view->setVar('ads', $ads);
            $totalAdsCount = $adsFiltering->getTotalAdsCount();
            $this->view->setVar('total_items', $totalAdsCount);
            if ($pagination = $adsFiltering->getPagination()) {
                $this->view->setVar('pagination_links_top', $pagination['top']);
                $this->view->setVar('pagination_links', $pagination['bottom']);
            }
            if ($generatedViewVars = $adsFiltering->getGeneratedViewVars()) {
                foreach ($generatedViewVars as $varTitle => $varValue) {
                    $this->view->setVar($varTitle, $varValue);
                }
            }
            $this->assets->addJs('assets/js/categories-view.js');
            // setup assets
            if ($pageSpecificJS = $adsFiltering->getPageSpecificJS()) {
                $this->scripts['page_specific_js'] = $pageSpecificJS;
            }
            if ($pageAssetsJS = $adsFiltering->getPageAssetsJS()) {
                foreach ($pageAssetsJS as $assetJS) {
                    $this->assets->addJs($assetJS);
                }
            }
            if ($pageAssetsCSS = $adsFiltering->getPageAssetsCSS()) {
                foreach ($pageAssetsCSS as $assetCSS) {
                    $this->assets->addCss($assetCSS);
                }
            }
            if ($adSubmitCategoryID = $adsFiltering->getAdSubmitCategory()) {
                $this->submit_ad_category = $category->id;
            }
        }

        // Store the total count of whatever we've displayed for later use, but only if no user-initiated-filtering
        // occurred (aka "default" listings/conditions) and only if it's a fresh db hit
        if ($db_hit && !$user_initiated_filtering) {
            /*
            error_log('storing AdsTotalCache for ' . $this->requested_path);
            error_log(var_export($db_hit, true));
            error_log(var_export($user_initiated_filtering, true));
            */
            $total = isset($totalAdsCount) ? $totalAdsCount : 0;
            $this->storeAdsTotalsCache($this->requested_path, $total);
        }
    }

    /**
     * Returns true if there aren't any extra query string parameters present in the current request
     *
     * @return bool
     */
    protected function isDefaultListingRequest()
    {
        $is_default_url = false;

        // If there's only one query string param present, and that's the default/internal Phalcon `_url` one,
        // it meas there's no additional query strings present in the request.

        // TODO/FIXME: This will break for some ?utm_source or similar links, which is kind of bad...

        $qs_array = $this->request->getQuery();
        if (1 === count($qs_array) && isset($qs_array['_url'])) {
            $is_default_url = true;
        }

        return $is_default_url;
    }

    protected function storeAdsTotalsCache($url = null, $total = 0)
    {
        if (null === $url) {
            $url = $this->requested_path;
        }

        try {
            // $result = AdsTotalsCache::store($url, $total);
            $result = AdsTotalsCache::insertIgnoreOrUpdate($url, $total);
        } catch (\Exception $e) {
            Bootstrap::log($e);
            $result = false;
        }

        return $result;
    }

    public function allAction()
    {
        $this->tag->setTitle(LandingPagesHelpers::buildTitle(array('Sve kategorije')));
        $this->site_desc = 'Popis svih kategorija na Oglasnik.hr portalu';

        $this->assets->addJs('assets/vendor/imagesloaded.pkgd.min.js');
        $this->assets->addJs('assets/vendor/masonry.pkgd.min.js');
        $this->assets->addJs('assets/js/categories-all.js');
    }

    protected function buildHref($category, $page = null)
    {
        $query = $this->request->getQuery();
        unset($query['_url']);

        $page = (int) $page;

        if ($page >= 2) {
            $query['page'] = $page;
        }

        // having ?page=1 is not needed and creates duplicate content
        if (1 === $page) {
            unset($query['page']);
        }

        return $category->get_url($query);
    }

    /**
     * @param array $url_parts
     *
     * @return array|null
     */
    protected function prepareCategoryAndPresetFiltersDataFromUrlParts(array $url_parts)
    {
        $data = null;

        if (is_array($url_parts) && !empty($url_parts)) {
            // Grab the last url element and treat it as a value to search for
            $parameter_url_value = array_pop($url_parts);
            $parameter_url_parent_value = null;

            $parts_count = count($url_parts);
            if ($parts_count > 1) {
                // $parameter_url_value represent the second level value to check for in this case,
                // while making sure what we search for is really a parent of the specified value.
                // We need to check the dictionary for that...

                // search bottom-up starting from the end
                $parameter_url_parent_value = array_pop($url_parts);
            }

            $active_condition = $this->getActiveSqlConditionDependingOnUser();

            // Use everything else as the url of the category we need to find, since elements
            // of $parts that we're searching for have already been removed due to array_pop() usage
            $category_url = implode('/', $url_parts);
            $category = Categories::findFirst(
                array(
                    'conditions' => 'url = :url:' . $active_condition,
                    'bind'       => array(
                        'url' => $category_url
                    )
                )
            );

            // Only move on if we've found the category
            // TODO/FIXME: this feels like duplicate work that should be taken care of earlier anyway?
            if ($category) {
                $data = array(
                    'category' => $category,
                    'do_404' => false
                );
                // Checking first if $parameter_url_value matches certain location-specific slugs, and if so,
                // use that to preset some location-based parameters
                $regions_data = Locations::getRegionsSeoData();
                if ($regions_data && isset($regions_data[$parameter_url_value])) {
                    $data['preset_filters_data'] = $regions_data[$parameter_url_value]['presets'];
                    $data['found_nodes'] = array($regions_data[$parameter_url_value]);
                } else {
                    // Otherwise, try looking for dependable dropdown params/dictionaries and their slugs
                    $found_nodes = $this->findMatchingDictionaryValues($category, $parameter_url_value, $parameter_url_parent_value);
                    if (!empty($found_nodes)) {
                        $preset_filters_data = array();
                        foreach ($found_nodes as $node) {
                            $preset_filters_data['ad_params_' . $node->parameter_id . '_' . ($node->level-1)] = $node->id;
                        }
                        $data['preset_filters_data'] = $preset_filters_data;
                        $data['found_nodes'] = $found_nodes;
                        // If found fewer nodes than we searched for, it might mean they're inactive (or just wrongly
                        // given via url), either way, we mark the data as '404'
                        if ($parts_count > 1 && count($found_nodes) <= 1) {
                            $data['do_404'] = true;
                        }
                    }
                }
            }
        }

        return $data;
    }

    /**
     * @param Categories $category
     * @param string $url_value
     * @param string|null $parent_url_value
     *
     * @return Dictionaries[]|array|null
     */
    protected function findMatchingDictionaryValues(Categories $category, $url_value, $parent_url_value = null)
    {
        $results = null;

        $possible_dicts = $this->getCategoryDictionaries($category->id);
        if (!empty($possible_dicts)) {

            $active_only = true;
            if ($this->isPrivilegedUser()) {
                $active_only = false;
            }

            // Loop over all matched dictionaries until we find a match by the specified slug on any level
            foreach ($possible_dicts as $result) {
                /* @var $dict Dictionaries */
                $dict = $result['dict'];

                if ($active_only && !$dict->active) {
                    continue;
                }

                $dict_tree = $dict->getTree();

                // Looking through the "flattened" list of children for the given dictionary
                $subtree_items = $dict_tree[$result['dictionary_id']]->children;

                // If a parent is specified, find it first
                if (null !== $parent_url_value) {
                    $search_for = $parent_url_value;
                } else {
                    $search_for = $url_value;
                }

                foreach ($subtree_items as $item) {
                    // Skip inactive nodes if/when searching only active
                    if ($active_only && !$item->active) {
                        continue;
                    }

                    $item_slug = Utils::slugify($item->name);
                    if ($search_for == $item_slug) {
                        // Keep parameter_id around for later
                        $item->parameter_id = $result['parameter_id'];
                        $results[] = $item;

                        // Check if parent was specified, and if so make sure that we return the child of that parent
                        if (null !== $parent_url_value) {
                            // We found the parent above, now find that parent's actual child node which
                            // is the value we need to match and return it
                            $children = $item->children;
                            foreach ($children as $child_node) {
                                // Skip inactive nodes if/when searching only active
                                if ($active_only && !$child_node->active) {
                                    continue;
                                }
                                // Comparing node's slug to what's in the url...
                                $child_node_slug = Utils::slugify($child_node->name);
                                if ($child_node_slug == $url_value) {
                                    // Keep parameter_id around for later
                                    $child_node->parameter_id = $result['parameter_id'];
                                    $results[] = $child_node;
                                }
                            }
                        }

                        break 2;
                    }
                }
            }

        }

        return $results;
    }

    protected function getCategoryDictionaries($category_id)
    {
        $db = $this->getDI()->getShared('db');

        // TODO/FIXME: use models or something... this is just quick & dirty
        $sql = "
          select
            p.id as parameter_id,
            p.dictionary_id as dictionary_id,
            cfp.category_id as category_id,
            cfp.category_fieldset_id as category_fieldset_id,
            p.slug as parameter_slug,
            cfp.parameter_slug as cfp_slug
          from
            categories_fieldsets_parameters cfp INNER JOIN parameters p ON (cfp.parameter_id = p.id)
          WHERE
            cfp.category_id = " . $category_id . " AND p.type_id = 'DEPENDABLE_DROPDOWN'";

        $result = $db->query(trim($sql));
        $result->setFetchMode(\Phalcon\Db::FETCH_BOTH);

        $results = $result->fetchAll();
        $dictionaries = null;
        foreach ($results as $result) {
            $dict_id = $result['dictionary_id'];
            $dictionaries[$dict_id] = array(
                'dictionary_id' => $dict_id,
                'parameter_id'  => $result['parameter_id'],
                'dict'          => Dictionaries::findFirst($dict_id)
            );
        }

        return $dictionaries;
    }
}
