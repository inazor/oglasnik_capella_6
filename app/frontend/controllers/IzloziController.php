<?php

namespace Baseapp\Frontend\Controllers;

use Baseapp\Library\Utils;
use Baseapp\Library\WspayHelper;
use Baseapp\Models\Orders;
use Baseapp\Models\UsersShops;
use Baseapp\Models\UsersShopsFeatured;
use Baseapp\Traits\BarcodeControllerMethods;
use Phalcon\Dispatcher;
use Phalcon\Escaper;
use Phalcon\Http\Response;

class IzloziController extends IndexController
{
    use BarcodeControllerMethods;

    const BACK_URL = 'moj-kutak/trgovina#izlozi';

    protected $current_user;

    /**
     * Overriding beforeExecuteRoute in order to provide some global view variables and secure access.
     *
     * @param Dispatcher $dispatcher
     *
     * @return bool
     */
    public function beforeExecuteRoute(Dispatcher $dispatcher)
    {
        // Making sure the parent is called too!
        parent::beforeExecuteRoute($dispatcher);

        // This controller should not be accessible for anon users
        if (!$this->auth->logged_in()) {
            $this->disable_view();
            $this->redirect_signin();

            return false;
        }

        $this->current_user = $this->auth->get_user();

        if (method_exists($this, 'setLayoutFeature')) {
            // turn off the search bar
            $this->setLayoutFeature('search_bar', false);
            $this->setLayoutFeature('full_page_width', true);
        }

        $this->view->setVar('current_action', $dispatcher->getActionName());
        $this->view->setVar('current_action_method', $dispatcher->getActiveMethod());
    }

    /**
     * Overriding afterExecuteRoute in order to provide some global view variables.
     *
     * @param Dispatcher $dispatcher
     *
     * @return bool
     */
    public function afterExecuteRoute(Dispatcher $dispatcher)
    {
        // Making sure the parent is called too!
        parent::afterExecuteRoute($dispatcher);

        // turn off all banner zones for this controller
        $this->view->setVar('banners', false);
    }

    /**
     * Returns true if the specified UsersShops exists and belongs to the currently logged-in user
     *
     * @param UsersShops $entity
     *
     * @return bool
     */
    protected function checkEntity(UsersShops $entity)
    {
        if (!$entity) {
            return false;
        }

        // Check that the current user is the shop owner
        $owner = $entity->getOwner();
        if ($owner && $owner->id != $this->current_user->id) {
            return false;
        }

        return true;
    }

    /**
     * Returns the UsersShops instance (specified by id or given a UsersShops instance) if the
     * currently logged in user owns it.
     *
     * @param UsersShops|int $shop
     *
     * @return bool|UsersShops
     */
    protected function getAndCheckShop($shop)
    {
        if (!($shop instanceof UsersShops)) {
            $shop = UsersShops::findFirst((int) $shop);
        }

        if (!$this->checkEntity($shop)) {
            return false;
        } else {
            return $shop;
        }
    }

    /**
     * Returns true if the currently logged-in user is the owner of
     * both the shopping window and the shop itself.
     *
     * @param UsersShopsFeatured $window
     *
     * @return bool
     */
    protected function isCurrentUserValidOwner(UsersShopsFeatured $window)
    {
        // Check that the current user is the same for both
        // the shopping window and the shop that the window belongs to
        $window_shop = $window->Shop;
        if ($window_shop) {
            $user_shop = $this->current_user->Shop;
            if ($user_shop) {
                if ($user_shop->id == $window_shop->id) {
                    return true;
                }
            }
        }

        return false;
    }

    /**
     * Perform various checks and return the UsersShopsFeatured instance.
     * In case of errors a ResponseInterface is returned indicating where to redirect when an error occurs.
     *
     * @param $entity_id
     *
     * @return \Phalcon\Http\ResponseInterface|UsersShopsFeatured|\Phalcon\Mvc\Model
     */
    protected function getAndCheckWindowOrRedirect($entity_id)
    {
        $window = UsersShopsFeatured::findFirst($entity_id);
        if (!$window || null === $entity_id) {
            $this->flashSession->error('Nepoznati izlog.');
            $result = $this->redirect_to(self::BACK_URL);
        }

        if (!isset($result)) {
            if (!$this->isCurrentUserValidOwner($window)) {
                $this->flashSession->error('Niste vlasnik tog izloga.');
                $result = $this->redirect_to(self::BACK_URL);
            }
        }

        if (!isset($result)) {
            $result = $window;
        }

        return $result;
    }

    public function indexAction()
    {
        return $this->redirect_to(self::BACK_URL);
    }

    /**
     * @param string|int $entity_id
     *
     * @return \Phalcon\Http\ResponseInterface
     */
    public function obrisiAction($entity_id = null)
    {
        $window = $this->getAndCheckWindowOrRedirect($entity_id);

        // In case of errors, return the redirect response
        if ($window instanceof Response) {
            return $window;
        }

        // Mark it as deleted
        $window->update(array('deleted' => 1));

        $this->flashSession->success('Izlog je uspješno obrisan.');
        return $this->redirect_to(self::BACK_URL);
    }

    /**
     * @param string|int $entity_id
     *
     * @return \Phalcon\Http\ResponseInterface
     */
    public function deaktivirajAction($entity_id = null)
    {
        $window = $this->getAndCheckWindowOrRedirect($entity_id);

        // In case of errors, return the redirect response
        if ($window instanceof Response) {
            return $window;
        }

        // Additionally check if this shopping window's status is awaiting payment
        // and do not allow deactivation if so, since we don't display those links in that case either
        // for some reason
        $status = $window->getStatusArray();
        if (1 == $status['code']) {
            $this->flashSession->error('Izlog čeka plaćanje i nije ga moguće deaktivirati.');
            return $this->redirect_to(self::BACK_URL);
        }

        // Mark it as inactive
        $window->update(array('active' => 0));

        $this->flashSession->success('Izlog je uspješno deaktiviran.');
        return $this->redirect_to(self::BACK_URL);
    }

    /**
     * @param string|int $entity_id
     *
     * @return \Phalcon\Http\ResponseInterface
     */
    public function aktivirajAction($entity_id = null)
    {
        $window = $this->getAndCheckWindowOrRedirect($entity_id);

        // In case of errors, return the redirect response
        if ($window instanceof Response) {
            return $window;
        }

        // Mark it as active
        $window->update(array('active' => 1));

        $this->flashSession->success('Izlog je uspješno aktiviran.');
        return $this->redirect_to(self::BACK_URL);
    }

    public function paymentAction($order_id = null)
    {
        $allow_payment = true;

        $page_title = sprintf('Izlozi - plaćanje narudžbe');
        $this->tag->setTitle($page_title);

        // Basic param checking
        if (!$order_id) {
            $allow_payment = false;
        }

        $order = null;
        if ($order_id) {
            $order = Orders::findFirst($order_id);
        }

        // Making sure the Order exists
        if ($allow_payment && !$order) {
            $allow_payment = false;

            $message = '<h2>Ooops!</h2><p>Nije moguće pronaći podatke o vašoj narudžbi!</p>';
            $this->flashSession->error($message);
        }

        // Making sure the current user is the one that created it
        if ($allow_payment && $order->user_id != $this->current_user->id) {
            $allow_payment = false;

            $message = '<h2>Ooops!</h2><p>To nije vaša narudžba!</p>';
            $this->flashSession->error($message);
        }

        // Making sure it's not expired or finalized or similar...
        if ($allow_payment && $order && ($order->isExpired() || !$order->isStatusChangeable())) {
            $allow_payment = false;

            $message = '<h2>Ooops!</h2><p>Narudžbu više nije moguće provesti (istekla/finalizirana)!</p>';
            $this->flashSession->error($message);
        }

        // Making sure there's a cost attached to it
        if ($allow_payment && $order->getTotalRaw() <= 0) {
            $allow_payment = false;

            $message = '<h2>Ooops!</h2><p>Narudžba nije provodiva!</p>';
            $this->flashSession->error($message);
        }


        // If any errors occurred, bail back to 'moj-kutak/trgovina'
        if (!$allow_payment) {
            $this->redirect_to(self::BACK_URL);
        }

        // Checking 'offline' payment submit confirmation and redirecting
        if ($this->request->isPost('next')) {
            // We're done here, the order should be fulfilled offline
            $this->flashSession->success('<h2>Super!</h2><p>Vaša narudžba je zaprimljena i čeka aktivaciju!</p>');
            return $this->redirect_to(self::BACK_URL);
        }

        // Abort early to prevent errors below if we don't have an order to work with
        if (!$order) {
            return;
        }

        // Build payment thing
        $url_args = array();
        $payment_data = array(
            'ShoppingCartID' => $order->getPbo(),
            'TotalAmount'    => $order->getTotal(),
            'ReturnURL'      => $this->url->get('izlozi/payment/' . $order->id, $url_args + ['return' => 1]),
            'CancelURL'      => $this->url->get('izlozi/payment/' . $order->id, $url_args + ['cancel' => 1]),
            'ReturnErrorURL' => $this->url->get('izlozi/payment/' . $order->id, $url_args + ['error' => 1])
        );
        $helper = new WspayHelper($payment_data, $this->current_user);

        // If we get an error back from WSPay
        if ($this->request->getQuery('error')) {
            $escaper = new Escaper();
            $cart_id = $this->request->getQuery('ShoppingCartID', 'string');
            $remote_message = $escaper->escapeHtml($this->request->getQuery('ErrorMessage', 'string'));
            $data = json_encode($this->request->getQuery());

            $log_message = 'WSPay returned an error for Order #' . $escaper->escapeHtml($cart_id) . ': ' . $remote_message;
            $log_message .= "\n" . $data;
            $this->saveAudit($log_message);

            $this->flashSession->error('<h2>Greška</h2><p>WSPay vratio: ' . $remote_message . '. Molimo pokušajte ponovo ili odaberite drugi način plaćanja.</p>');
        } elseif ($this->request->getQuery('cancel')) {
            // TODO: Do we even have to bother with anything in case of cancel? Why?
        } elseif ($this->request->getQuery('return') && $this->request->getQuery('Success')) {
            // Verifying received data from WSpay
            $query_vars = $this->request->getQuery();
            $valid_signature = $helper->verifyReturnedSignature($query_vars);
            if (true === $valid_signature) {
                // Great success!
                $save_data = array(
                    'payment_method' => strtolower($this->request->getQuery('PaymentType', 'string')),
                    'approval_code'  => $this->request->getQuery('ApprovalCode')
                );
                $finalized = $order->finalizePurchase($save_data);
                if ($finalized) {
                    // $this->deleteSessionTokenData($token);
                    $this->flashSession->success('<h2>Odlično!</h2><p>Vaša narudžba je uspješno obrađena!</p>');
                    return $this->redirect_to(self::BACK_URL);
                } else {
                    $this->flashSession->error('<h2>Greška!</h2><p>Izvršenje plaćene narudžbe nije uspjelo. Molimo kontaktirajte korisničku podršku!</p>');
                }
            }
        }

        // Display payment method choices
        $this->view->setVar('order_total_formatted', Utils::format_money($order->getTotal(), true));
        $this->view->setVar('order_pbo', $order->getPbo());
        $this->view->setVar('order_table_markup', $order->getPaymentRecapTableMarkup());
        $this->view->setVar('rendered_form',  $helper->getFormMarkup());

        // $barcode_src = $this->url->get('izlozi/barcode-order/' . $order->id);
        $barcode_src = $order->get2DBarcodeInlineSvg();
        $this->view->setVar('order_id', $order->id);
        $this->view->setVar('payment_data_table_markup', Orders::buildPaymentDataTableMarkup($order, $barcode_src));
        $this->view->setVar('offline_button_markup', Orders::buildOfflineConfirmButtonMarkup('izlozi', 'Naruči'));

        $this->assets->addJs('assets/js/payment-method-toggle.js');
        $this->assets->addJs('assets/js/ie-11-nav-cache-bust.js');
    }
}
