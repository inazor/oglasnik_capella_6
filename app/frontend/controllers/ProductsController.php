<?php

namespace Baseapp\Frontend\Controllers;

use Baseapp\Library\Products\BackendProductsSelector;
use Baseapp\Library\Products\ProductsInterface;
use Baseapp\Library\Products\ProductsSelector;
use Baseapp\Models\Categories;
use Baseapp\Models\Users;
use Phalcon\Dispatcher;
use Phalcon\Http\Response;

class ProductsController extends IndexController
{
    const STATUS_BAD_REQUEST = 400;
    const STATUS_ACCESS_DENIED = 403;

    /**
     * @var \Phalcon\Http\Response
     */
    public $response;

    // Overridden initialize() to prevent a bunch of un-needed crap from IndexController and/or BaseFrontendController
    public function initialize()
    {
        // $this->initJsonResponse();
    }

    /**
     * Triggered on Listeners/Controllers before executing the controller/action method.
     *
     * @param Dispatcher $dispatcher
     *
     * @return bool|void
     */
    public function beforeExecuteRoute(Dispatcher $dispatcher)
    {
        $this->initJsonResponse();

        return parent::beforeExecuteRoute($dispatcher);
    }

    protected function initJsonResponse()
    {
        $this->disable_view();

        $this->response = new Response();
        $this->response->setContentType('application/json', 'UTF-8');
    }

    public function indexAction()
    {
        return $this->redirect_to('/');
    }

    /**
     * @param ProductsSelector $selector
     * @param string $group
     * @param string $class_name
     * @param Categories|null $category
     * @param Users|null $user
     * @param array|null $post_data
     *
     * @return ProductsInterface
     */
    protected function createProduct(ProductsSelector $selector, $group, $class_name, Categories $category = null, Users $user = null, $post_data = null)
    {
        $fq_class_name = $selector->buildFullClassName($group, $class_name);

        if (!class_exists($fq_class_name)) {
            throw new \InvalidArgumentException('Invalid product group/class specified');
        }

        /* @var $product ProductsInterface */
        $product = new $fq_class_name($category, $user, $post_data);

        return $product;
    }

    /**
     * @return Response
     */
    public function buildAction()
    {
        try {
            // Make sure we have an authenticated admin user
            if (!$this->auth->logged_in(array('admin'))) {
                throw new \Exception('Forbidden', self::STATUS_ACCESS_DENIED);
            }

            // Validate some params...
            $category_id       = $this->request->get('category_id', 'int', null);
            $user_id           = $this->request->get('user_id', 'int', null);
            $product_group     = $this->request->get('product_group', 'string', 'Online');
            $product_classname = $this->request->get('product_class', 'string', 'Osnovni');

            $category = null;
            $user     = null;
            $product  = null;

            if (!empty($category_id)) {
                $category = Categories::findFirst($category_id);
            }

            if (!empty($user_id)) {
                $user = Users::findFirst($user_id);
            }

            if (!$category) {
                throw new \InvalidArgumentException('Invalid or missing `category_id` parameter');
            }

            if (!$user) {
                throw new \InvalidArgumentException('Invalid or missing `user_id` parameter');
            }

            if (!empty($product_group) && !empty($product_classname)) {
                $selector = new BackendProductsSelector($category);
                $product  = $this->createProduct($selector, $product_group, $product_classname, $category, $user);
            }

            $this->response->setJsonContent(serialize($product));
        } catch (\Exception $e) {
            // Transform caught exceptions into http error 400 and json 'error' message
            $code = $e->getCode();
            $msg  = 'Bad request';

            if (empty($code)) {
                $code = self::STATUS_BAD_REQUEST;
            }

            if ($code === self::STATUS_ACCESS_DENIED) {
                $msg = 'Forbidden';
            }

            $this->response->setStatusCode($code, $msg);
            $this->response->setJsonContent(array('error' => $e->getMessage()));
        }

        return $this->response;
    }
}
