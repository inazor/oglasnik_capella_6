<?php

namespace Baseapp\Frontend\Controllers;

use Baseapp\Extension\CsrfException;
use Phalcon\Dispatcher;
use Baseapp\Extension\Tag;
use Baseapp\Library\AdsFiltering;
use Baseapp\Library\Utils;
use Baseapp\Library\Validations\ContactForm;
use Baseapp\Models\UsersShops;
use Baseapp\Models\Ads;
use Baseapp\Models\Categories;
use Baseapp\Models\UsersMessages;
use Baseapp\Library\Tool;
use Phalcon\Validation;
use Baseapp\Traits\InfractionReportsHelpers;

/**
 * Frontend Trgovina Controller
 */
class TrgovinaController extends IndexController
{
    use InfractionReportsHelpers;

    public $infractionReportModelClass = 'Baseapp\Models\UsersShops';

    /**
     * Overriding afterExecuteRoute in order to provide some global view variables.
     *
     * @param Dispatcher $dispatcher
     *
     * @return bool
     */
    public function afterExecuteRoute(Dispatcher $dispatcher)
    {
        // Making sure the parent is called too!
        parent::afterExecuteRoute($dispatcher);

        //$this->add_banner_zone('sidebar_left_1', '2285083', '<div id="zone2285083" class="goAdverticum"></div>');
        //$this->add_banner_zone('listing_7', '2285047', '<div id="zone2285047" class="goAdverticum"></div>');
        //$this->add_banner_zone('listing_14', '4084895', '<div id="zone4084895" class="goAdverticum"></div>');

        $this->view->setVar('banner_zone_ids', $this->get_active_zone_ids());
    }

    /**
     * Index Action
     */
    public function indexAction()
    {
        // nothing here for now
    }

    /**
     * View the shop details, ads, etc...
     *
     * @param $slug
     *
     * @return mixed|\Phalcon\Http\ResponseInterface
     * @throws \Baseapp\Extension\CsrfException
     */
    public function viewAction($slug)
    {
        /**
         * @var UsersShops|false $shop
         */
        $shop = UsersShops::findFirstBySlug($slug);

        if (!$shop) {
            return $this->trigger_404();
        }

        if (!$shop->isActive()) {
            $profile_page_link = $shop->User->get_ads_page();
            return $this->redirect_to($profile_page_link);
        }

        $base_url   = 'trgovina/' . $slug;
        $this->view->setVar('shop_url', $base_url);
        $shop_owner = $shop->getOwner();
        $auth_user  = $this->auth->get_user();

        $this->setLayoutFeature('full_page_width', true);

        $logged_in_owner = false;
        if ($auth_user && $auth_user->id == $shop_owner->id) {
            $logged_in_owner = true;
        }

        $show_contact_form = true;
        $contact_form_url = $this->url->get($base_url);
        $show_contact_modal_on_load = false;

        // You don't have to contact yourself, at least not via this site...
        if ($logged_in_owner) {
            $show_contact_form = false;
        }
        // TODO/FUTURE: perhaps certain users or user-states or whatever will
        // have the contact form disabled for some reason... or maybe they can pay
        // for that privilege :) -- we can disable it easily here

        // Setup re-captcha for anon users if contact form is in use
        if ($show_contact_form && empty($auth_user)) {
            // $this->assets->addJs('https://www.google.com/recaptcha/api.js?hl=hr');
            $this->assets->collection('head')->addJs('https://www.google.com/recaptcha/api.js?hl=hr');
            $this->view->setVar('recaptcha_sitekey', $this->recaptcha_sitekey);
        }

        // Process user contact form submission
        if ($this->request->isPost()) {
            $abort               = false;
            $validation_messages = null;

            // If/when csrf checking fails, return an empty 403
            try {
                $this->check_csrf();
            } catch (CsrfException $ex) {
                return $this->do_403();
            }

            // Anon users need a verified captcha response
            if (empty($auth_user)) {
                $g_response = $this->request->getPost('g-recaptcha-response');
                $validation_messages = new Validation\Message\Group();
                if (empty($g_response)) {
                    $abort = true;
                    $validation_messages->appendMessage(new Validation\Message('Molimo potvrdite da niste robot', 'captcha'));
                }
                if (!$abort) {
                    $recaptcha = new \ReCaptcha\ReCaptcha($this->recaptcha_secret);
                    $response  = $recaptcha->verify($g_response, $this->request->getClientAddress(true));
                    if (!$response->isSuccess()) {
                        $abort = true;
                        foreach ($response->getErrorCodes() as $code) {
                            $validation_messages->appendMessage(new Validation\Message('Captcha greška: ' . $code, 'captcha'));
                        }
                    }
                }
            }

            // Don't even bother doing our validations if recaptcha failed
            if (!$abort) {
                $validation          = new ContactForm();
                $validation_messages = $validation->validate($this->request->getPost());
            }

            $messageData = array(
                'user_id'      => $shop->user_id,
                'entity'       => 'shops',
                'entity_id'    => $shop->id,
                'sender_name'  => $this->request->getPost('name', 'string'),
                'sender_email' => $this->request->getPost('email'),
                'message'      => $this->request->getPost('message')
            );


            if ($this->request->isAjax()) {
                $this->disable_view();
                $this->response->setContentType('application/json', 'UTF-8');
                $response_array = array('status' => false);

                if (!count($validation_messages)) {
                    if (UsersMessages::send($messageData)) {
                        $response_array['status'] = true;
                        $response_array['msg']    = 'Poruka uspješno poslana korisniku';
                    } else {
                        $response_array['msg'] = 'Slanje poruke nije uspjelo (serverska greška)!';
                    }
                } else {
                    // Show errors
                    $errors = array();
                    foreach ($validation_messages as $message) {
                        $errors[] = array(
                            'field' => $message->getField(),
                            'value' => $message->getMessage()
                        );
                    }
                    $response_array['msg']    = '<h2>Oops!</h2><p>Molimo ispravite uočene greške.</p>';
                    $response_array['errors'] = $errors;
                }
                $this->response->setJsonContent($response_array);
                return $this->response;
            } else {
                if (!count($validation_messages)) {
                    if (UsersMessages::send($messageData)) {
                        $this->flashSession->success('Poruka uspješno poslana korisniku');
                    } else {
                        $this->flashSession->error('Slanje poruke nije uspjelo (serverska greška)!');
                    }

                    return $this->redirect_self();
                } else {
                    // Show errors
                    $this->view->setVar('errors', $validation_messages);
                    $show_contact_modal_on_load = true;
                    $this->flashSession->error('<h2>Oops!</h2><p>Molimo ispravite uočene greške.</p>');
                }
            }
        }

        $this->view->setVar('viewType', $this->getViewTypeData());

        $adTitle      = $this->request->has('ad_params_title') ? trim($this->request->get('ad_params_title')) : null;
        $picturesOnly = $this->request->has('ad_params_uploadable') && (int) $this->request->get('ad_params_uploadable', 'int', 0) ? (int) $this->request->get('ad_params_uploadable', 'int', 0) : null;
        $adPriceFrom  = $this->request->has('ad_price_from') && (int) $this->request->get('ad_price_from', 'int', 0) ? (int) $this->request->get('ad_price_from', 'int', 0) : null;
        $adPriceTo    = $this->request->has('ad_price_to') && (int) $this->request->get('ad_price_to', 'int', 0) ? (int) $this->request->get('ad_price_to', 'int', 0) : null;
        $filtersData  = array(
            'TITLE'      => array('ad_params_title' => $adTitle),
            'UPLOADABLE' => array('ad_params_uploadable' => $picturesOnly),
            'CURRENCY'   => array('ad_price_code' => 'HRK', 'ad_price_from' => $adPriceFrom, 'ad_price_to' => $adPriceTo)
        );
        $this->view->setVar('fields', array(
            'ad_params_title' => $adTitle,
            'ad_params_uploadable' => $picturesOnly,
            'ad_price_from' => $adPriceFrom,
            'ad_price_to' => $adPriceTo
        ));

        // Get users ads listing
        $adsFiltering = new AdsFiltering();
        $adsFiltering->cacheResults(true);
        $adsFiltering->cachePrefix('shop-' . $shop->id . '-ads');
        $adsFiltering->cacheLifeTime(120);
        $adsFiltering->setLoggedInUser($auth_user);
        $adsFiltering->setCustomFilter($filtersData);
        $adsFiltering->setSortParamData($this->getSortParamData($shop->default_sorting_order));
        $adsFiltering->allowSpecialProducts(true);
        $adsFiltering->setBaseURL($base_url);
        $adsFiltering->setAdUser($shop_owner);
        $adsFiltering->showOnlyRootCategoriesCounts(false);
        $adsFiltering->alwaysShowCompleteCategoriesTree(true);
        if ($searchCategoryId = $this->request->has('category_id') ? intval($this->request->get('category_id')) : null) {
            $adsFiltering->setAdCategoryByID($searchCategoryId);
        }

        $ads = $adsFiltering->getAds();
        if ($flashSession = $adsFiltering->getFlashSession()) {
            $this->flashSession->$flashSession['type']($flashSession['text']);
        }
        if ($adsFiltering->shouldTrigger404()) {
            return $this->trigger_404();
        }
        if (!$adsFiltering->hasError()) {
            $this->buildSortingDropdown($shop->default_sorting_order);
            $this->view->setVar('ads', $ads);
            $this->view->setVar('searchCategoryId', $searchCategoryId);
            if ($pagination = $adsFiltering->getPagination()) {
                $this->view->setVar('pagination_links_top', $pagination['top']);
                $this->view->setVar('pagination_links', $pagination['bottom']);
            }
            if ($generatedViewVars = $adsFiltering->getGeneratedViewVars()) {
                foreach ($generatedViewVars as $varTitle => $varValue) {
                    $this->view->setVar($varTitle, $varValue);
                }
            }
            // setup assets
            if ($pageSpecificJS = $adsFiltering->getPageSpecificJS()) {
                $this->scripts['page_specific_js'] = $pageSpecificJS;
            }
            if ($pageJS = $adsFiltering->getPageAssetsJS()) {
                foreach ($pageJS as $assetJS) {
                    $this->assets->addJs($assetJS);
                }
            }
            if ($pageCSS = $adsFiltering->getPageAssetsCSS()) {
                foreach ($pageCSS as $assetCSS) {
                    $this->assets->addCss($assetCSS);
                }
            }
        }

        $this->tag->setTitle($shop->getName());
        $this->view->setVar('shop', $shop);
        $shopPublicDetails = $shop->get_public_details();
        $this->view->setVar('shop_info_markup', UsersShops::buildInfoMarkup($shopPublicDetails));
        $this->view->setVar('public_details', $shopPublicDetails);
        $this->view->setVar('shop_owner', $shop_owner);
        $this->view->setVar('logged_in_owner', $logged_in_owner);
        $this->view->setVar('show_contact_form', $show_contact_form);
        $this->view->setVar('contact_form_url', $contact_form_url);
        $this->assets->addJs('//maps.googleapis.com/maps/api/js?v=3.exp&signed_in=true&language=hr&key=' . $this->config->google->maps->browserKey);
        $this->assets->addJs('assets/js/google-map-wrapper.js');
        $this->assets->addJs('assets/js/trgovina.js');

        if ($show_contact_modal_on_load) {
            $this->scripts[] = 'jQuery(\'#modal-send-message\').modal(\'show\');';
        }
    }

    protected function getAdsCategories($shop, $category = null, $filters = null)
    {
        $builder = $this->modelsManager->createBuilder();
        $builder->columns(
            array(
                'category.*',
                'COUNT(ad.id) AS ad_count'
            )
        );
        $builder->addFrom('Baseapp\Models\Categories', 'ad_category');
        $builder->innerJoin('Baseapp\Models\Ads', 'ad.category_id = ad_category.id AND ad.active = 1 AND ad.is_spam = 0', 'ad');
        $builder->innerJoin('Baseapp\Models\Categories', 'category.lft <= ad_category.lft AND category.rght >= ad_category.rght', 'category');
        $where_array = array(
            'category.parent_id = :parent_category:',
            'ad.user_id = :user_id:',
        );
        $where_params = array(
            'parent_category' => ($category && !empty($category->id) ? $category->id : 1),
            'user_id'         => $shop->user_id,
        );
        $where_types = array(
            'parent_category' => \PDO::PARAM_INT,
            'user_id'         => \PDO::PARAM_INT,
        );
        $builder->where(implode(' AND ', $where_array), $where_params, $where_types);
        $builder->groupBy(array('category.id'));
        $builder->orderBy('category.lft ASC');
        //$builder->having('ad_count > 0');

        $query_result = $builder->getQuery()->execute();

        $shop_categories_html = array();

        if ($category && !empty($category->id)) {
            $shop_categories_html[] = '<p><a href="' . $shop->getUrl('url') . '">Svi oglasi</a></p>';
        }

        if (count($query_result) == 0 && ($category && !empty($category->id)) && $filters) {
            $shop_categories_html[] = '<p>' . $category->getPath() . '</p>';
            foreach ($filters['html'] as $row) {
                $shop_categories_html[] = $row;
            }
        } elseif ($query_result) {
            $category_li = array();

            foreach ($query_result as $shop_category) {
                $shop_category_url = $shop->getUrl('url') . '?category_id=' . $shop_category->category->id;
                $category_li[] = '<li><a href="' . $shop_category_url . '">' . $shop_category->category->name . '</a><span class="ad_count">(<span class="text-primary">' . $shop_category->ad_count . '</span>)</span></li>';
            }

            if (count($category_li)) {
                $shop_categories_html[] = '<p>Kategorije</p>';
                $shop_categories_html[] = '<ul class="submenu">' . implode('', $category_li) . '</ul>';
            }
        }

        $this->view->setVar('shop_current_category', ($category ? $category->getPath() : null));
        $this->view->setVar('shop_categories_html', $shop_categories_html);
        $this->view->setVar('shop_has_categories', (count($shop_categories_html) ? true : false));
    }

    /**
     * Helper method to get category filters and if they exists, set everything needed
     * to the view..
     *
     * @param \Baseapp\Models\Categories $category
     * @return null|array
     */
    protected function getFiltersResponse($category = null)
    {
        $category_has_filters = false;
        $category_filters_data = null;

        if ($category) {
            $filter_data = null;
            if ($this->request->isPost()) {
                $filter_data = $this->request->getPost();
            } elseif ($this->request->getQuery()) {
                $filter_data = $this->request->getQuery();
            }

            $category_filters_data = $category->getFilters($filter_data);

            if (is_array($category_filters_data['html']) && count($category_filters_data['html'])) {
                $category_has_filters = true;
                $this->view->setVar('category_filters_html', $category_filters_data['html']);

                if (count($category_filters_data['js'])) {
                    $this->scripts['category_specific_js'] = implode(PHP_EOL, $category_filters_data['js']);
                }

                if (isset($category_filters_data['assets'])) {
                    if (isset($category_filters_data['assets']['js']) && count($category_filters_data['assets']['js'])) {
                        foreach ($category_filters_data['assets']['js'] as $asset_js) {
                            $this->assets->addJs($asset_js);
                        }
                    }

                    if (isset($category_filters_data['assets']['css']) && count($category_filters_data['assets']['css'])) {
                        foreach ($category_filters_data['assets']['css'] as $asset_css) {
                            $this->assets->addCss($asset_css);
                        }
                    }
                }
            }

            if (isset($category_filters_data['form']) && $category_filters_data['form']) {
                $this->submit_ad_category = $category->id;
            }

            $this->view->setVar('category_form_filters', $category_filters_data['form']);
        }

        $this->view->setVar('category_has_filters', $category_has_filters);

        return $category_filters_data;
    }

    protected function getTotalActiveShopAds($shop)
    {
        $count = Ads::count('user_id = ' . $shop->user_id . ' AND active = 1 AND is_spam = 0');

        return $count;
    }

    /**
     * Helper method to get all ads that fulfill search criteria selected by user
     */
    private function getPaginatedAds($shop, $category = null, $filters = null, $page = 1, $child_categories_included = true)
    {
        $resulting_array = null;

        $builder = $this->modelsManager->createBuilder();
        $builder->addFrom('Baseapp\Models\Ads', 'ad');
        $builder->innerJoin('Baseapp\Models\Currency', 'ad.currency_id = currency.id', 'currency');

        $where_array  = array(
            'ad.user_id = :user_id:',
            'ad.active = 1',
            'ad.is_spam = 0'
        );
        $where_params = array(
            'user_id'    => $shop->user_id,
        );
        $where_types  = array(
            'user_id'    => \PDO::PARAM_INT,
        );

        if ($category) {
            $builder->innerJoin('Baseapp\Models\Categories', 'ad.category_id = category.id', 'category');

            if ($child_categories_included) {
                $where_array[] = 'category.lft >= :category_left:';
                $where_params['category_left'] = $category->lft;
                $where_array[] = 'category.rght <= :category_right:';
                $where_params['category_right'] = $category->rght;
            } else {
                $where_array[] = 'category.id = :category_id:';
                $where_params['category_id'] = $category->id;
            }
        }

        $columns = array('ad.*');

        $shop_url         = $shop->getUrl('url');
        $shop_current_url = $shop_url . ($category ? '?category_id=' . $category->id : '');

        if (count($filters['filters'])) {
            foreach ($filters['filters'] as $filter) {
                if (isset($filter['model'])) {
                    $builder->innerJoin($filter['model'], $filter['alias'].'.ad_id = ad.id', $filter['alias']);
                    if (isset($filter['filter'])) {
                        if (isset($filter['filter']['columns'])) {
                            $columns = array_merge($columns, $filter['filter']['columns']);
                        }
                        if (isset($filter['filter']['where_conditions'])) {
                            $where_array[] = $filter['filter']['where_conditions'];
                            if (isset($filter['filter']['where_params'])) {
                                $where_params = array_merge($where_params, $filter['filter']['where_params']);
                            }
                            if (isset($filter['filter']['where_types'])) {
                                $where_types = array_merge($where_types, $filter['filter']['where_types']);
                            }
                        }
                        if (isset($filter['filter']['having_conditions'])) {
                            $having_array[] = $filter['filter']['having_conditions'];
                        }
                    }
                } else {
                    if (isset($filter['columns'])) {
                        $columns = array_merge($columns, $filter['columns']);
                    }
                    if (isset($filter['where_conditions'])) {
                        $where_array[] = $filter['where_conditions'];
                        if (isset($filter['where_params'])) {
                            $where_params = array_merge($where_params, $filter['where_params']);
                        }
                        if (isset($filter['where_types'])) {
                            $where_types = array_merge($where_types, $filter['where_types']);
                        }
                    }
                    if (isset($filter['having_conditions'])) {
                        $having_array[] = $filter['having_conditions'];
                    }
                }
            }
        }
        if (!count($where_types)) {
            $builder->where(implode(' AND ', $where_array), $where_params, $where_types);
        } else {
            $builder->where(implode(' AND ', $where_array), $where_params);
        }
        if (!empty($having_array)) {
            $builder->having(implode(' AND ', $having_array));
        }
        $builder->columns($columns);
        $builder->groupBy(array('ad.id'));

        $order_by = $this->buildOrderByParam($shop);
        $builder->orderBy($order_by);

        $paginator = new \Phalcon\Paginator\Adapter\QueryBuilder(
            array(
                'builder' => $builder,
                'limit'   => $this->config->settingsFrontend->pagination_items_per_page,
                'page'    => $page
            )
        );
        $current_page = $paginator->getPaginate();

        if ($current_page) {
            $resulting_array = array(
                'error'        => false,
                'shop_has_ads' => true,
                'default_tab'  => 'oglasi'
            );

            if ($current_page->total_pages > 0 && $page > $current_page->total_pages) {
                // user has (most probably manually changed the page number) so
                // we'll show him a 404 error page
                $resulting_array = array(
                    'error'       => true,
                    'trigger_404' => true,
                    'shop_has_ads' => false,
                    'default_tab'  => $shop->getAbout() ? 'onama' : 'kontakt'
                );
            } elseif (count($current_page->items) == 0) {
                // current request returned no ads..

                if ($category) {
                    // checked filters found no ads, so we need to show an info
                    // msg to user
                    $this->getAdsCategories($shop, $category, $filters);
                    $resulting_array['error'] = true;
                    $resulting_array['error_msg'] = 'Nije pronađen nijedan oglas koji zadovoljava postavljene kriterije';
                } else {
                    $resulting_array['shop_has_ads'] = false;
                    // case when there are no active ads in a shop... we have to
                    // disable 'Oglasi' tab and check if shop has about text
                    $resulting_array['default_tab'] = $shop->getAbout() ? 'onama' : 'kontakt';
                }
            } else {
                // Process the resulting page and extend with some additional data (ad image if it exists)
                $results_array = Ads::getEnrichedFrontendBasicResultsArray($current_page->items);
                $current_page->items = $results_array;

                $this->getAdsCategories($shop, $category, $filters);

                $pagination_links = Tool::pagination(
                    $current_page,
                    $shop_current_url,
                    'pagination',
                    $this->config->settingsFrontend->pagination_count_out,
                    $this->config->settingsFrontend->pagination_count_in
                );

                $resulting_array['ads']        = $current_page->items;
                $resulting_array['pagination'] = $pagination_links;
            }

            $this->view->setVar('category', $category);
            $this->view->setVar('shop_url', $shop_url);
        } else {
            $resulting_array = array(
                'error'        => false,
                'shop_has_ads' => false,
                'default_tab'  => $shop->getAbout() ? 'onama' : 'kontakt'
            );
        }

        return $resulting_array;
    }

}
