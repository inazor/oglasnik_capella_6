<?php

namespace Baseapp\Frontend\Controllers;

use Baseapp\Library\Debug;
use Baseapp\Library\Products\GroupedProductsChooser;
use Baseapp\Library\Products\ProductsInterface;
use Baseapp\Library\Products\UpgradeableProductsSelector;
use Baseapp\Library\Utils;
use Baseapp\Library\WspayHelper;
use Baseapp\Models\Ads;
use Baseapp\Models\Orders;
use Baseapp\Traits\BarcodeControllerMethods;
use Phalcon\Dispatcher;
use Phalcon\Escaper;
use Phalcon\Http\ResponseInterface;
use Phalcon\Mvc\Model;
use Phalcon\Validation;

class UpgradeController extends IndexController
{
    use BarcodeControllerMethods;

    protected $current_user;

    const SESSION_TOKEN_NAME = 'tokens-upgrade';

    /**
     * @var GroupedProductsChooser|null
     */
    protected $products_chooser = null;

    // Load zendesk for the entire controller as requested
    protected $load_zendesk_widget_script = true;

    /**
     * Overriding beforeExecuteRoute in order to provide some global view variables and secure access.
     *
     * @param Dispatcher $dispatcher
     *
     * @return bool
     */
    public function beforeExecuteRoute(Dispatcher $dispatcher)
    {
        // Making sure the parent is called too!
        parent::beforeExecuteRoute($dispatcher);

        // This controller should not be accessible for anon users
        if (!$this->auth->logged_in()) {
            $this->disable_view();
            $this->redirect_signin();
            return false;
        }
        $this->current_user = $this->auth->get_user();

        if (method_exists($this, 'setLayoutFeature')) {
            // turn off the search bar
            $this->setLayoutFeature('search_bar', false);
        }

        $this->view->setVar('current_action', $dispatcher->getActionName());
        $this->view->setVar('current_action_method', $dispatcher->getActiveMethod());
    }

    /**
     * Overriding afterExecuteRoute in order to provide some global view variables.
     *
     * @param Dispatcher $dispatcher
     *
     * @return bool
     */
    public function afterExecuteRoute(Dispatcher $dispatcher)
    {
        // Making sure the parent is called too!
        parent::afterExecuteRoute($dispatcher);

        // turn off all banner zones for this controller
        $this->view->setVar('banners', false);
    }

    protected function checkEntity(Ads $entity)
    {
        if (!$entity) {
            return false;
        }

        // Check that the current user is the owner of the Ad
        $owner = $entity->getUser();
        if ($owner && $owner->id != $this->current_user->id) {
            return false;
        }

        return true;
    }

    protected function modelMessagesTransformer(Model $model)
    {
        // TODO: extend Phalcon\Validation\Message\Group to encapsulate this via __toString() or something...
        $msgs = array();
        foreach ($model->getMessages() as $msg) {
            $msgs[] = $msg->getMessage();
        }
        $msg_string = implode(', ', $msgs);

        return $msg_string;
    }

    /**
     * Creates a hexadecimal representation of a random token. Resembles
     * an SHA1 hash, but it's not. And it's better (or so they say).
     *
     * @link http://stackoverflow.com/a/14869745
     *
     * The resulting string will be twice as long as the random bytes we generate;
     * Each byte encoded to hex is 2 characters. 20 bytes will be 40 characters of hex.
     * Using 20 bytes, we have 256^20 or 1,461,501,637,330,902,918,203,684,832,716,283,019,655,932,542,976 unique
     * output values. This is identical to SHA1's 160-bit (20-byte) possible outputs.
     *
     * @return string
     */
    protected function createRandomToken()
    {
        $token = bin2hex(openssl_random_pseudo_bytes(20));

        return $token;
    }

    /**
     * Helper to encapsulate and reduce code duplication when checking required session token data
     * at various points in the ad upgrade/purchase stages
     *
     * @param array|null $data Session token data
     * @param array $keys List of key names in $data which have to exist and be non-empty
     *
     * @return bool False if any data is missing, true if everything's ok
     */
    protected function hasRequiredTokenData(array $data = null, $keys = array())
    {
        $errors = false;

        if (empty($data)) {
            $errors = true;
        }

        if (!is_array($keys) && is_string($keys)) {
            $keys = (array) $keys;
        }

        if (!$errors) {
            foreach ($keys as $key) {
                if (!isset($data[$key]) || empty($data[$key])) {
                    $errors = true;
                    break;
                }
            }
        }

        return !$errors;
    }

    /**
     * @param string $token Ad submission token
     * @param array $data Data to store (or merge if the token already exists)
     */
    protected function storeSessionTokenData($token, $data = array())
    {
        $tokens = $this->session->get(self::SESSION_TOKEN_NAME, array());

        if (!isset($tokens[$token])) {
            $tokens[$token] = $data;
        } else {
            $tokens[$token] = array_merge($tokens[$token], $data);
        }

        $this->session->set(self::SESSION_TOKEN_NAME, $tokens);
    }

    /**
     * @param $token string Ad submission token
     * @param null $key Optional, specific key of data stored under the specified $token bucket. Returns null if key does not exist
     *
     * @return array|mixed|null
     */
    protected function getSessionTokenData($token, $key = null)
    {
        $data = null;

        $tokens = $this->session->get(self::SESSION_TOKEN_NAME, array());

        if (isset($tokens[$token])) {
            $data = $tokens[$token];
            // Allows fetching a single key (if specified) from the store if it exists. Returns null if it doesn't.
            if (null !== $key) {
                if (isset($data[$key])) {
                    $data = $data[$key];
                } else {
                    $data = null;
                }
            }
        }

        return $data;
    }

    /**
     * @param $token
     *
     * @return bool
     */
    protected function deleteSessionTokenData($token)
    {
        $tokens = $this->session->get(self::SESSION_TOKEN_NAME, array());
        if (isset($tokens[$token])) {
            unset($tokens[$token]);
            $this->session->set(self::SESSION_TOKEN_NAME, $tokens);
            return true;
        }

        return false;
    }

    /**
     * @param string|null $token
     *
     * @return int
     */
    protected function getStepForToken($token = null)
    {
        $step = 1;

        $stored_step = $this->getSessionTokenData($token, 'step');
        if (null !== $stored_step) {
            $step = $stored_step;
        }

        return $step;
    }

    /**
     * @param int $default
     *
     * @return bool True if all checks pass, false otherwise
     */
    protected function isRequestedStepAndTokenValid($default = 1)
    {
        $error = false;

        // Check for GET `step` parameter, falling back to possibly provided $default
        $specified_step = (int) $this->request->get('step', 'int', $default);
        if (!$specified_step || $specified_step < 0) {
            $error = true;
        }

        if (!$error && $specified_step > 1) {
            // First make sure we're within allowed upper bounds
            $steps       = $this->getWizardSteps();
            $steps_total = count($steps);
            if ($specified_step > $steps_total) {
                $error = true;
            }

            // Now make sure we have a token to work with
            $token = null;
            if (!$error) {
                $token = $this->request->get('token', 'string', null);
            }

            // Make sure session-stored step of the token is ok compared
            // to the one specified in the GET parameters
            if (!$error && $token) {
                // Requested step has to be lower or equal to what's stored for it on the server-side
                $stored_step = $this->getStepForToken($token);
                if (!($specified_step <= $stored_step)) {
                    $error = true;
                }

                // Also make sure the requested step actually exists among our defined steps
                if (!$error) {
                    if (!isset($steps[$specified_step])) {
                        $error = true;
                    }
                }
            }
        }

        return !$error;
    }

    /**
     * Builds and returns the "steps wizard" markup.
     *
     * @param int $active The step considered 'active' or 'current'
     * @param null|string $uri Link/href passed as 3rd parameter to `buildStepHref()`
     *
     * @return string
     */
    protected function buildStepsMarkup($active = 1, $uri = null)
    {
        $steps = $this->getWizardSteps();

        // Check if we're given a token via GET
        $token = $this->request->get('token', 'string', null);

        $markup = '<div class="row wizard-steps">';

        foreach ($steps as $k => $step_data) {
            $is_active    = ($k == $active);
            $is_done      = ($k < $active);

            $classnames = array();
            if ($is_active) {
                $classnames[] = 'active';
            }
            if ($is_done) {
                $classnames[] = 'done';
            }

            $step_markup = '<span class="wizard-step">' . $step_data['title'] . '</span>';
            if (!$is_active) {
                // Current step not active, but not everything should be clickable, depending on where
                // we are in the process

                // TODO: determine if something else needs to be clickable (and when, if so)
                $do_link = false;

                // If we have a token, check how far we've come according to the
                // token info from the session and do not build links for stuff we haven't
                // already reached
                if ($token) {
                    $token_step = $this->getStepForToken($token);
                    if ($k <= $token_step) {
                        $do_link = true;
                    }
                }

                if ($do_link) {
                    $step_link   = $this->buildStepHref($k, $token, $uri);
                    $step_markup = sprintf('<a href="%s">%s</a>', $step_link, $step_markup);
                }
            }

            $classnames_string = '';
            if (!empty($classnames)) {
                $classnames_string = ' ' . implode(' ', $classnames);
            }

            $markup .= '<div class="col-sm-3 col-xs-6' . $classnames_string . '">';
            $markup .= $step_markup;
            $markup .= '</div>';
        }

        $markup .= '</div>';

        return $markup;
    }

    /**
     * @param int $step
     *
     * @return int
     */
    protected function getCurrentStep($step = 1)
    {
        $checks_ok = $this->isRequestedStepAndTokenValid($step);

        if ($checks_ok) {
            $step = $this->request->get('step', 'int', $step);
        }

        $steps = $this->getWizardSteps();
        if (!isset($steps[$step])) {
            $step = 1;
        }

        return (int) $step;
    }

    protected function buildStepHref($step = 1, $token = null, $uri = 'upgrade')
    {
        $query = $this->request->getQuery();
        unset($query['_url']);

        if ($step >= 1) {
            $query['step'] = $step;
        }

        if (null !== $token) {
            $query['token'] = $token;
        }

        // having ?step=1 is not needed
        if (1 === $step) {
            unset($query['step']);
        }

        $href = $this->url->get($uri, $query);
        return $href;
    }

    /**
     * @return array
     */
    protected function getWizardSteps()
    {
        static $steps = array(
            1 => array(
                'view'  => 'chunks/ads-submission-products-chooser',
                'title' => 'Odabir vrste oglasa'
            ),
            2 => array(
                'view'  => 'chunks/ads-submission-payment',
                'title' => 'Plaćanje',
            )
        );

        return $steps;
    }

    /**
     * @param string|null $token
     * @param array|null $token_data
     * @param string|null $message
     *
     * @return \Phalcon\Http\ResponseInterface
     */
    protected function redirectToLastKnownStepWithMessage($token, $token_data, $message = null)
    {
        if (null !== $message) {
            $this->flashSession->error($message);
        }

        if (null !== $token_data && is_array($token_data) && !empty($token_data)) {
            $last_reached_step = $token_data['step'];
        } else {
            $last_reached_step = 1;
        }

        return $this->redirect_to($this->buildStepHref($last_reached_step, $token));
    }

    /**
     * @param Orders|null $order
     *
     * @return array|null|ProductsInterface[]
     */
    protected function getChosenProductsFromOrder(Orders $order = null)
    {
        $chosen = null;

        if ($order instanceof Orders) {
            $ordered_items = $order->Items;
            if ($ordered_items->valid()) {
                $chosen = array();
                foreach ($ordered_items as $item) {
                    $product = $item->getProduct();
                    if ($product instanceof ProductsInterface) {
                        $chosen[] = $product;
                    }
                }
            }
        }

        return $chosen;
    }

    /**
     * @param Ads $ad
     * @param Orders|null $order
     *
     * @return GroupedProductsChooser
     */
    protected function setupAndReturnProductsChooser(Ads $ad, $order = null)
    {
        $post_data = $this->request->getPost();

        $order_products = $this->getChosenProductsFromOrder($order);
        $selection = new UpgradeableProductsSelector($ad, $post_data, $order_products);

        $chooser = new GroupedProductsChooser($selection);

        $this->assets->addJs('assets/js/classified-submit-choose-ad-type.js');

        // Fucking 'offline-products' checkbox and maintaining its state in all cases...
        $has_offline_products_in_order = false;
        if (null !== $order_products) {
            // If one of $order_products is an offline product, check the checkbox
            foreach ($order_products as $p) {
                if ($p->isOfflineType()) {
                    $has_offline_products_in_order = true;
                    break;
                }
            }
            if ($has_offline_products_in_order) {
                $post_data['offline-products'] = true;
            }
        } else {
            // Existing ad might have an offline product, check the box for that case by default
            $ad_products = $ad->getProducts();
            if (!empty($ad_products['offline'])) {
                // Existing Ad has/had an offline product, check the checkbox
                $post_data['offline-products'] = true;
            }
        }

        $this->view->setVar('products_chooser_markup', $chooser->getMarkup($post_data));
        $this->view->setVar('products_cost_total', $chooser->getChosenProductsTotalCostDisplay());

        $chosen_products = $chooser->getChosenProducts('grouped');
        $billing_info = array();
        foreach ($chosen_products as $group => $group_products) {
            foreach ($group_products as &$product) {
                $billing_info[$group][] = $product->getShoppingCartInfo();
            }
        }
        $this->view->setVar('billing_info', $billing_info);

        return $chooser;
    }

    public function indexAction($entity_id = null)
    {
        /**
         * @var Ads $ad
         */
        $ad = Ads::findFirst($entity_id);
        $ok = $this->checkEntity($ad);

        if (!$ok) {
            $this->flashSession->error('Neispravni parametri izdvajanja oglasa');
            return $this->redirect_to('moj-kutak');
        }

        $this->view->setVar('ad', $ad);

        $current_href = 'upgrade/' . $entity_id;

        // Perform sanity checks for requested step and token vars, and if something is not quite right
        // redirect back to the beginning
        $sanity_checks_ok = $this->isRequestedStepAndTokenValid();
        if (!$sanity_checks_ok) {
            // Bail back to the beginning if anything is weird
            return $this->redirect_to($current_href);
        }

        // Prep some variables
        $steps             = $this->getWizardSteps();
        $current_step      = $this->getCurrentStep();
        $current_step_data = $steps[$current_step];

        // Build wizard view markup
        $this->view->setVar('steps_wizard_markup', $this->buildStepsMarkup($current_step, $current_href));

        // Set appropriate page title
        $page_title = sprintf('Izdvajanje oglasa - Korak %s - %s', $current_step, $current_step_data['title']);
        $this->tag->setTitle($page_title);

        $this->assets->addJs('assets/js/ie-11-nav-cache-bust.js');

        // Grab/set common vars
        $token      = $this->request->get('token', 'string', null);
        $token_data = $this->getSessionTokenData($token);

        switch ($current_step) {
            // Product chooser
            case 1:
            default:
                $result = $this->processStep1($ad, $token, $token_data);
                break;
            // Payment
            case 2:
                $result = $this->processStep2($ad, $token, $token_data);
                break;
        }

        // If any of the step $result above is already a ResponseInterface instance, return that
        if ($result instanceof ResponseInterface) {
            return $result;
        }

        // Always pick the "current" view
        $this->view->pick($current_step_data['view']);
        $this->view->setVar('stepType', 'upgrade');
    }

    /**
     * Handles the Products chooser, creating/updating the potential Order upon submit
     *
     * @param Ads $ad
     * @param string|null $token
     * @param array|null $token_data
     *
     * @return void|ResponseInterface
     */
    protected function processStep1(Ads $ad, $token = null, $token_data = array())
    {
        // We could have an order created earlier on step 2 and then have gone back to change it completely
        $order = null;
        if (isset($token_data['order_id']) && !empty($token_data['order_id'])) {
            $order = Orders::findFirst($token_data['order_id']);
        }

        if (null === $token) {
            $token = $this->createRandomToken();
        }

        $chooser = $this->setupAndReturnProductsChooser($ad, $order);

        $show_offline_phone_warning = false;
        if (!$ad->phone1 && !$ad->phone2) {
            $show_offline_phone_warning = true;
        }
        $this->view->setVar('show_offline_phone_warning', $show_offline_phone_warning);

        if ($this->request->hasPost('next')) {
            $result = $this->processStep1Submit($ad, $order, $token, 'offline', $chooser);
            if ($result instanceof ResponseInterface) {
                return $result;
            } elseif ($result instanceof Validation\Message\Group) {
                $this->view->setVar('errors', $result);
            }
        }
    }

    /**
     * Handles ordering chosen products and redirecting to step 2.
     *
     * @param Ads $ad
     * @param Orders|null $order Optional, a new Order is created for $ad if one is not provided
     * @param string|null $token Optional, a new one is created if one is not provided
     * @param string $payment_method Optional, defaults to 'offline'
     * @param GroupedProductsChooser $chooser
     *
     * @return ResponseInterface|Validation\Message\Group
     */
    protected function processStep1Submit(Ads $ad, Orders $order = null, $token = null, $payment_method = 'offline', GroupedProductsChooser $chooser)
    {
        /*if (null === $token) {
            $token = $this->createRandomToken();
        }*/

        // Cancel the order in case it was created earlier somehow
        if ($this->request->hasPost('cancel')) {
            if ($order) {
                $order->save(array('status' => Orders::STATUS_CANCELLED));
            }

            // Order cancelled, clearing current token's data and redirecting
            $this->deleteSessionTokenData($token);
            $this->flashSession->success('Vaša narudžba je uspješno stornirana');
            return $this->redirect_to('moj-kutak');
        }

        // Showing an error if/when someone chooses a combination that would result in 0 cost
        if ($chooser->getChosenProductsTotalCost() <= 0) {
            $this->flashSession->error('Odabrite barem jedan proizvod s opcijama');
            return $this->redirect_to($this->buildStepHref(1, $token, 'upgrade/' . $ad->id));
        }

        /** @var ProductsInterface[] $products */
        $products = $chooser->getChosenProducts('flat');

        // TODO: unset products with cost <= 0 if needed? Or elsewhere?

        // Creates a new (or updates an existing) order, depending on the value of $order
        $order = Orders::saveForAd(
            $ad,
            $order,
            $products,
            array(
                'payment_method' => $payment_method,
                'status'         => Orders::STATUS_NEW
            )
        );

        // If successful, redirect to step 2, otherwise stay and show errors
        if ($order instanceof Orders) {
            $next_step = 2;
            $this->storeSessionTokenData(
                $token,
                array(
                    'ad_id'           => $ad->id,
                    'step'            => $next_step,
                    // 'order'        => $order,
                    'order_id'        => $order->id
                )
            );

            $url = $this->buildStepHref($next_step, $token, 'upgrade/' . $ad->id);

            return $this->redirect_to($url);
        } else {
            $this->flashSession->error('<strong>Greška!</strong> Došlo je do greške prilikom spremanja narudžbe.');

            $errors         = new Validation\Message\Group();
            $errors->appendMessage(new Validation\Message($this->modelMessagesTransformer($order)));

            return $errors;
        }
    }

    /**
     * Handles payment/order recap + payment method chooser + cc payment provider handling + finish up when done
     *
     * @param Ads $ad
     * @param null $token
     * @param array|null $token_data
     *
     * @return ResponseInterface
     */
    protected function processStep2(Ads $ad, $token = null, $token_data = array())
    {
        if (!$this->hasRequiredTokenData($token_data, array('order_id'))) {
            return $this->redirectToLastKnownStepWithMessage($token, $token_data);
        }

        $order_id = $token_data['order_id'];
        $order    = Orders::findFirst($order_id);

        if (!$order) {
            $message = '<strong>Ooops!</strong> Nije moguće pronaći podatke o vašoj narudžbi!';
            return $this->redirectToLastKnownStepWithMessage($token, $token_data, $message);
        }

        /**
         * zyt: 23.09.2016.
         * The Order we're currently working with could've been cancelled either via /moj-kutak/otkazi-narudzbu or
         * by some other means in between the time it was created and now that we landed on this url again...
         * In case we're here and it's a cancelled order, handle all the things that are done
         * on processStep1Submit in order to avoid duplicating code (and so that we don't have
         * ads with latest_payment_state=cancelled when we're about to re-new the now-cancelled
         * order that still appears to be existing in the session since step2 was never fully
         * submitted).
         */
        if ($order->status == Orders::STATUS_CANCELLED) {
            $result = $this->processStep1Submit($ad, $order, $token);
            if ($result instanceof ResponseInterface) {
                return $result;
            }/* elseif ($result instanceof Validation\Message\Group) {
                $this->view->setVar('errors', $result);
            }*/
        }

        // Checking 'offline' payment submit confirmation and redirecting
        if ($this->request->isPost('next')) {
            // We're done here, the order should be fulfilled offline
            $this->deleteSessionTokenData($token);
            $this->flashSession->success('<strong>Super!</strong> Vaša narudžba je zaprimljena i čeka aktivaciju!');
            return $this->redirect_to('moj-kutak');
        }

        // Build payment thing
        $url_args = array(
            'step'  => 2,
            'token' => $token
        );
        $payment_data = array(
            'ShoppingCartID' => $order->getPbo(),
            'TotalAmount'    => $order->getTotal(),
            'ReturnURL'      => $this->url->get('upgrade/' . $ad->id, $url_args + ['return' => 1]),
            'CancelURL'      => $this->url->get('upgrade/' . $ad->id, $url_args + ['cancel' => 1]),
            'ReturnErrorURL' => $this->url->get('upgrade/' . $ad->id, $url_args + ['error' => 1])
        );
        $helper = new WspayHelper($payment_data, $this->current_user);

        // Example error request we get from WSPay:
        if ($this->request->getQuery('error')) {
            // upgrade/$ad_id?step=2&token=83b6fef8f17b772c417f4d8587631b87aafb1032&error=1&CustomerFirstname=sda
            // &CustomerSurname=asd&CustomerAddress=neka+tamo&CustomerCountry=Hrvatska&CustomerZIP=da+nebi
            // &CustomerCity=ma+je&CustomerEmail=asd@asdf.com&CustomerPhone=123456&ShoppingCartID=900000164
            // &Lang=HR&DateTime=20150621022730&Amount=350&ECI=&PaymentType=VISA&PaymentPlan=0000
            // &ShopPostedPaymentPlan=0000&Success=0&ApprovalCode=&ErrorMessage=ODBIJENO
            $escaper = new Escaper();
            $cart_id = $this->request->getQuery('ShoppingCartID', 'string');
            $remote_message = $escaper->escapeHtml($this->request->getQuery('ErrorMessage', 'string'));
            $data = json_encode($this->request->getQuery());

            $log_message = 'WSPay returned an error for Order #' . $escaper->escapeHtml($cart_id) . ': ' . $remote_message;
            $log_message .= "\n" . $data;
            $this->saveAudit($log_message);

            $this->flashSession->error('<strong>Greška</strong> WSPay vratio: ' . $remote_message . '. Molimo pokušajte ponovo ili odaberite drugi način plaćanja.');
        } elseif ($this->request->getQuery('cancel')) {
            // TODO: Do we even have to bother with anything in case of cancel? Why?
        } elseif ($this->request->getQuery('return') && $this->request->getQuery('Success')) {
            // Verifying received data from WSpay
            $query_vars = $this->request->getQuery();
            $valid_signature = $helper->verifyReturnedSignature($query_vars);
            if (true === $valid_signature) {
                // Great success!
                $save_data = array(
                    'payment_method' => strtolower($this->request->getQuery('PaymentType', 'string')),
                    'approval_code'  => $this->request->getQuery('ApprovalCode')
                );
                $finalized = $order->finalizePurchase($save_data);
                if ($finalized) {
                    $this->deleteSessionTokenData($token);
                    $this->flashSession->success('<strong>Odlično!</strong> Vaša narudžba je uspješno obrađena!');
                    return $this->redirect_to('moj-kutak');
                } else {
                    $this->flashSession->error('<strong>Greška!</strong> Izvršenje plaćene narudžbe nije uspjelo. Molimo kontaktirajte korisničku podršku!');
                }
            }
        }

        // Display payment method choices
        $this->view->setVar('order_total_formatted', Utils::format_money($order->getTotal(), true));
        $this->view->setVar('order_pbo', $order->getPbo());
        $this->view->setVar('order_table_markup', $order->getPaymentRecapTableMarkup());
        $this->view->setVar('rendered_form',  $helper->getFormMarkup());

        // $barcode_src = $this->url->get('upgrade/barcode', array('token' => $token));
        $barcode_src = $order->get2DBarcodeInlineSvg();
        $this->view->setVar('order_id', $order->id);
        $this->view->setVar('payment_data_table_markup', Orders::buildPaymentDataTableMarkup($order, $barcode_src));
        $this->view->setVar('offline_button_markup', Orders::buildOfflineConfirmButtonMarkup('upgrade', 'Naruči'));

        $this->assets->addJs('assets/js/payment-method-toggle.js');
    }
}
