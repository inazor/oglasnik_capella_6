<section class="pad-xs-only-lr">
    {% set category_breadcrumbs = category.getBreadcrumbs() %}
    <div class="underline margin-bottom-2em">
        <h1 class="underline">
        {% for breadcrumb in category_breadcrumbs %}
            {% if not loop.first %}
                <a href="{{ breadcrumb.get_url() }}">{{ breadcrumb.name }}</a>{% if not loop.last %} - {% endif %}
            {% endif %}
        {% endfor %}
        </h1>
    </div>

    <div class="ad-listing-wrapper no-sidebar">
        <div>{{ age_restriction_html }}</div>
        {% if auto_deny %}
            <p><strong>Molimo unesite vaš datum rođenja u sekciji <a href="{{ url('moj-kutak/podaci') }}">Moj kutak - Osobni podaci</a></strong>.</p>
        {% endif %}
        <div class="age-restriction buttons-wrapper">
            {% if auto_deny %}
            {{ linkTo([null, 'OK', 'class':'btn btn-default btn-sm btn-reset']) }}
            {% else %}
            {{ linkTo(['category/confirm-age/' ~ category.id ~ (next_url ? '?next=' ~ next_url : ''), 'Imam dovoljno godina!', 'class':'btn btn-primary btn-sm']) }}
            &nbsp;
            {{ linkTo([null, 'Nemam dovoljno godina!', 'class':'btn btn-default btn-sm btn-reset']) }}
            {% endif %}
        </div>
    </div>
</section>
