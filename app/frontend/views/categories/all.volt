<h1 class="h2-like section-title">{{ 'Sve kategorije'|category_name_markup }}</h1>

{% if categories is defined and categories and categories is iterable %}
<section class="homepage-detailed-category-list">
    {{ partial('chunks/categories-tree-structured', ['categories':categories, 'rowClass':'masonry-categories', 'itemClass':'masonry-item'] )}}
</section>
{% endif %}
