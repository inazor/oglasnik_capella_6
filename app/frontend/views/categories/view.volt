{% include 'chunks/breadcrumbs.volt' %}

{% set shouldReverseColumns = false %}
{% if categoryFiltersArray is defined and categoryFormFilters %}
    {% set shouldReverseColumns = true %}
{% endif %}

{{ form(category_url, 'id':'category_filters', 'class':'js-allow-double-submit', 'method':'get') }}

{% set current_category = categoryTree[category.id]|default(null) %}
{% if current_category %}
    {% set current_category_path = current_category.path %}
    {% set current_category_fancy_name = current_category.name %}
    {% if (current_category_path['fancy_name'] is defined) %}
        {% set current_category_fancy_name = current_category_path['fancy_name'] %}
    {% endif %}
    <h1 class="h2-like section-title">{{ current_category_fancy_name|category_name_markup }}</h1>
{% endif %}

    <p class="total-results visible-xs hidden-sm hidden-md hidden-lg">Pronađenih oglasa: {{ total_items|money }}</p>

    <div id="landing"></div>

    <div class="hidden-xs">
    {{ landing_pages_links_markup }}
    </div>

    <a name="classifieds"></a>

    {{ childCategoriesMarkup|default('') }}

    {% if childCategoriesMarkup|default('') == '' %}
    <div id="filterBitsContainer">
        <div class="row no-gutters-xs margin-top-md">
            <div class="col-md-3 side-col">
                <p style="margin-top:7px !important;">
                    {% if shouldReverseColumns %}
                    <a class="hidden-md hidden-lg btn display-block filters-anchor-link margin-bottom-1em" href="#filters-anchor">
                        Filtriraj oglase
                    </a>
                    {% endif %}
                    <span class="total-results-sticky hidden-xs">
                    Pronađenih oglasa: {{ total_items|money }}
                    {% if mapSearchCategory|default(null) %}
                        {% set mapSearchCategoryID = 'parent_category_id=' ~ category.id %}
                        {% if category.transaction_type_id %}
                            {% set mapSearchCategoryID = 'category_id=' ~ category.id %}
                        {% endif %}
                        <br/><a href="{{ this.url.get('search/map?' ~ mapSearchCategoryID) }}">Pretraži {{ mapSearchCategory.name|default('')|lower }} na karti <span class="fa fa-chevron-right fa-fw"></span></a>
                    {% endif %}
                    </span>
                </p>
            </div>
            <div class="col-md-6 filterBits hidden"></div>
        </div>
    </div>
    {% endif %}

    {{ shopping_windows_markup }}

<div{% if shouldReverseColumns %} class="reordered-on-mobile"{% endif %}>
    <div class="row no-gutters-xs">
        <div id="filters-anchor" class="hidden-xs listings-side-col side-col col-md-3{{ childCategoriesMarkup|default('') == '' ? ' margin-top-lg' : '' }}">
            {% if childCategoriesMarkup|default('') == '' %}
            <h2 class="blue"><img class="margin-right-sm" src="{{ url('assets/img/icn_filter_category_01.svg') }}" alt="">Filtriranje oglasa</h2>
            <input type="hidden" id="category_id" value="{{ category.id }}" />
            {{ categoryTreeCountsMarkup|default('') }}
                {% if categoryFiltersArray is defined %}
                    {% for categoryFilter in categoryFiltersArray['shown'] %}{{ categoryFilter }}{% endfor %}
                        {% if categoryFiltersArray['hidden']|length %}
                        <div class="hidden-filters">
                            {% for categoryFilter in categoryFiltersArray['hidden'] %}{{ categoryFilter }}{% endfor %}
                        </div>
                        <div class="hidden-filters-more margin-top-1em margin-bottom-1em"><span class="more-btn">Prikaži više filtera <span class="fa fa-chevron-down fa-fw"></span></span></div>
                        {% endif %}
                        {% if categoryFormFilters -%}
                            <div class="filters-main-buttons{{ (categoryFiltersArray['hidden']|length == 0 ? ' margin-top-md' : '') }}">
                                <button class="btn btn-primary width-full text-bold margin-bottom-10px" type="submit">Pretraži</button>
                                <button class="btn width-full btn-reset btn-grey btn-reset-inactive text-bold margin-bottom-10px" type="reset">Poništi filter</button>
                            </div>
                            {% set showEmailAgentButton = false %}
                            {% if showEmailAgentButton %}
                            &nbsp;<div>
                                <span class="btn btn-block btn-info text-bold" id="btnSaveAsEmailAgent">Spremi kao EmailAgent</span>
                                <small class="text-muted">Ukoliko želite primati mail obavijesti vezano za trenutno postavljenu pretragu spremite ovu pretragu kao Email agent.</small>
                            </div>
                            {% endif %}
                        {%- endif %}
                {% endif %}
            {% else %}
                <p class="hidden-xs" style="margin-top:15px !important;">
                    Pronađenih oglasa: {{ total_items|money }}
                    {% if mapSearchCategory|default(null) %}
                        {% set mapSearchCategoryID = 'parent_category_id=' ~ category.id %}
                        {% if category.transaction_type_id %}
                            {% set mapSearchCategoryID = 'category_id=' ~ category.id %}
                        {% endif %}
                    <br/><a href="{{ this.url.get('search/map?' ~ mapSearchCategoryID) }}">Pretraži {{ mapSearchCategory.name|default('')|lower }} na karti <span class="fa fa-chevron-right fa-fw"></span></a>
                    {% endif %}
                </p>
            {% endif %}

            {% include 'chunks/banners-sidebar-left.volt' %}
        </div>

        <div class="listings-main-col col-md-6">
            <div class="grid-sort-options margin-bottom-sm row no-gutters-xs">
                <div class="col-xs-6 col-sm-7 upper-pagination">{% if pagination_links_top is defined and pagination_links_top %}{{ pagination_links_top }}{% endif %}</div>
                <div class="col-xs-6 col-sm-5 sort-dropdown">
                    {{ sort_dropdown }}
                </div>
            </div>

            {{ flashSession.output() }}
            {% set narrowGridMode = true %}
            {% include 'chunks/ads-mixed.volt' %}
        </div>
        <div class="col-md-3 listings-banner-col-right hidden-xs hidden-sm">
            {% include 'chunks/banners-sidebar-right.volt' %}
        </div>
    </div>
</div>

{% if banners and banner_728x90 is defined %}
<div class="banner w728 center banner-728x90 hidden-xs margin-top-sm margin-bottom-sm">{{ banner_728x90 }}</div>
{% endif %}

{{ endForm() }}
