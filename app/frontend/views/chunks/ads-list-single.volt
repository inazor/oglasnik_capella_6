{% set ad_box_classes = 'ad-box ad-box-wide no-hover no-checkbox category-listing clearfix' %}
{% if ad_is_pushable is defined and ad_is_pushable %}
    {% set ad_box_classes = ad_box_classes ~ ' ad-box-pushable' %}
{% endif %}
{% if ad['online_product_id'] is defined %}
    {% set ad_box_classes = ad_box_classes ~ ' izdvojeno-' ~ ad['online_product_id']|slugify %}
{% endif %}
<a href="{{ ad['frontend_url'] }}" class="{{ ad_box_classes }}">
    {%- set thumb = ad['thumb_pic'] -%}
    {{ thumb.addClass(thumb.getOrientation()) }}
    <div class="lazy image-wrapper image-wrapper-bg full-width {{ thumb.getClass() }}" data-src="{{ thumb.src }}">
        {% if (not(ad['location'] is empty)) %}<span class="location hidden-sm hidden-md hidden-lg"><i class="fa fa-map-marker" aria-hidden="true"></i> {{ ad['location'] }}</span>{% endif %}
        {% if (not(ad['media_of_total'] is empty)) %}<span class="media-cnt hidden-sm hidden-md hidden-lg">{{ ad['media_of_total'] }}</span>{% endif %}
    </div>
    <noscript>
        <div class="image-wrapper image-wrapper-bg full-width {{ thumb.getClass() }}" style="background-image:url({{ thumb.src }});">
            {% if (not(ad['location'] is empty)) %}<span class="location hidden-sm hidden-md hidden-lg"><i class="fa fa-map-marker" aria-hidden="true"></i> {{ ad['location'] }}</span>{% endif %}
            {% if (not(ad['media_of_total'] is empty)) %}<span class="media-cnt hidden-sm hidden-md hidden-lg">{{ ad['media_of_total'] }}</span>{% endif %}
        </div>
    </noscript>
    <div class="pad-xs-only-lr">
        <h3 class="classified-title">{{ ad['title']|escape_attr|striptags }}</h3>
        <div class="info-wrapper">
            {% set adUserTypeIcon = '' %}
            {% if ad['shop'] is defined %}
                {% set adUserTypeIcon = '<span class="listings-icon icon shop" title="' ~ ad['shop'].title ~ '"></span>' %}
            {% else %}
                {% set adUserTypeIcon = '<span class="listings-icon icon fa fa-user"></span>' %}
            {% endif %}
            {% if ad['description_tpl'] %}
            <div class="description">{{ ad['description_tpl'] }}</div>
            {% else %}
            <p class="description">{{ ad['description']|default('')|striptags|truncate(150)|nl2br }}</p>
            {% endif %}
        </div>

        {% if ad['price']['main'] > 0 %}
        <div class="price-block" title="Cijena: {{ ad['price']['main'] }}{% if ad['price']['other'] is defined %} ≈ {{ ad['price']['other'] }}{% endif %}">
            Cijena:
            <span class="price-values">
                <span class="price-kn">{{ ad['price']['main'] }}</span>
                {% if ad['price']['other'] is defined %}<span class="almost-equals">≈</span> <span class="price-euro">{{ ad['price']['other'] }}</span>{% endif %}
            </span>
        </div>
        {% else %}
        <div class="price-block price-unavailable">
            Više informacija potražite u oglasu
        </div>
        {% endif %}

        <div class="meta ad-box-end">
            {% if adUserTypeIcon %}
                {{ adUserTypeIcon }}
            {% endif %}
            <span class="date">{{ ad['sort_date'] }}</span>
            {% if ad_is_pushable is defined and ad_is_pushable %}
            <div class="up-sell-box" data-link="{{ url('push-up/' ~ ad['id']) }}" data-classified-id="{{ ad['id'] }}" data-product-id="push-up">
                <div class="input-group input-group-sm">
                    <span class="form-control up-sell-title">Želite Vaš oglas iznad ostalih?</span>
                    <span class="input-group-btn"><span class="btn btn-primary">Skok na vrh</span></span>
                </div>
            </div>
            {% endif %}
            {{ ad['view_count']|default('') ? ' | Prikaza: ' ~ ad['view_count'] : '' }}
        </div>
    </div>
</a>
