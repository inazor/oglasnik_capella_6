<div class="col-md-3 col-xs-6">
    {% set adMapJson = Models_Ads__getMapJsonForAd(ad) %}
    {% set adMapInfo = adMapJson['info'] is defined ? adMapJson['info'] : null %}
    <div class="ad-box ad-box-default margin-bottom-sm{{ ad['online_product_id'] is defined ? ' izdvojeno-' ~ ad['online_product_id']|slugify : '' }}" data-id="{{ ad['id'] }}" data-marker-json="{{ adMapJson|json_encode|escape_attr }}">
        {% set thumb = ad['thumb_pic'] %}
        <div class="image-wrapper">
            <a href="{{ ad['frontend_url'] }}" class="classified-img">{{ thumb.setAlt(ad['title']|striptags|truncate(80)|escape_attr).getTag() }}</a>
            {% if ad['user'] is defined and ad['user'] %}
            {% set adUser = ad['user'] %}
            {% if adUser.username != 'avus' %}
                {% set avatar = adUser.getAvatar().setClass('avatar-header').setWidth('30').setHeight('30') %}
                <a href="{{ adUser.get_ads_page() }}" class="user-info">
                    {{ avatar }}
                    {% set users_username = (adUser.type == 1 ? adUser.username : adUser.company_name) %}
                    {% if ad['shop'] is defined %}{% set users_username = ad['shop'].title %}{% endif %}
                    <span class="username">{{ users_username }}</span>
                </a>
            {% endif %}
            {% endif %}
            <div class="meta hidden-xs">
                <span class="date">{{ ad['sort_date'] }}</span><br>
                {% if ad['location'] is defined %}<span class="location">{{ ad['location'] }}</span>{% endif %}
            </div>
            {% if adMapInfo and adMapInfo['right'] is iterable %}
            <div class="meta-right hidden-xs">
                {% for rightInfo in adMapInfo['right'] %}
                <span>{{ rightInfo }}</span>
                {% endfor %}
            </div>
            {% endif %}
        </div>
        {% set ad_title_orig = ad['title'] %}
        {% set ad_title = ad['title']|striptags|truncate(80)|escape_attr %}
        <h3><a href="{{ ad['frontend_url'] }}">{{ ad_title }}</a></h3>
        <div class="classified-footer">
            {% if ad['price'] %}
            {% set adPrices = ad['price'] %}
            <span class="price-kn">{{ adPrices['main'] }}</span>
            {% if adPrices['other'] %}<span class="price-euro hidden-xs">{{ adPrices['other'] }}</span>{% endif %}
            {% else %}
            <span class="price-kn">&nbsp;</span>
            {% endif %}

            {% if ad['user_id'] != loggedInUserId|default(ad['user_id']) %}
            <span class="toggle-favorite disabled add-to-favs hidden-xs" data-id="{{ ad['id'] }}">Dodaj u favorite</span>
            {% endif %}
        </div>
    </div>
</div>
