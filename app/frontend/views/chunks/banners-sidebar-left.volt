{# some terrible hacks to not repeat myself :) #}
{% if banners %}
    <div class="sidebar-rectangle-holder margin-top-md hidden-xs hidden-sm">
        {% for i in 1..5 %}
            {% set banner_slot_name = 'banner_sidebar_left_' ~ i %}
            <?php if (isset($$banner_slot_name) && !empty($$banner_slot_name)) { ?>
            <div class="banner text-center margin-top-sm"><?php echo $$banner_slot_name; ?></div>
            <?php } ?>
        {% endfor %}
    </div>
{% endif %}
