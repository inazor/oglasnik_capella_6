{% set defaultMaxLevelDepth = 4 %}
{% set forbiddenCategoryNames = ['ponuda','prodaja','potražnja','iznajmljivanje','unajmljivanje','najam'] %}
<div class="row{{ rowClass|default('') ? ' ' ~ rowClass : ''}}">
{% for category in categories %}
    <div class="col-md-3 col-sm-6{{ itemClass|default('') ? ' ' ~ itemClass : ''}}">
        <ul class="main-category">
            {{ partial('chunks/categories-tree-structured-category', ['category':category, 'maxLevelDepth':maxLevelDepth|default(defaultMaxLevelDepth), 'forbiddenCategoryNames':forbiddenCategoryNames]) }}
        </ul>
    </div>
    {% if loop.index % 4 == 0 %}
    <div class="clearfix"></div>
    {% elseif loop.index % 2 == 0 %}
    <div class="clearfix visible-sm-block"></div>
    {% endif %}
{% endfor %}
</div>
