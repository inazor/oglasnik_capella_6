{% if not (article_media is empty) and article_media|length > 1 %}
<div id="blueimp-gallery" class="blueimp-gallery blueimp-gallery-controls" data-start-slideshow="true" data-filter=":even">
    <div class="slides"></div>
    <h3 class="title"></h3>
    <a class="prev">‹</a>
    <a class="next">›</a>
    <a class="close">×</a>
    <a class="play-pause"></a>
    <ol class="indicator"></ol>
</div>
<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 content-gallery">
    {% for media in article_media %}
    <div class="gallery-item">
        {% set thumb = media.get_thumb('300x220') %}
        {% set large = media.get_thumb('GalleryBig') %}
        <a href="{{ large.getSrc() }}"><img width="{{ thumb.getWidth() }}" height="{{ thumb.getHeight() }}" src="{{ thumb.getSrc() }}" alt="{{ article.title|escape_attr }}"></a>
    </div>
    {% endfor %}
</div>
<div class="clearfix"><!--IE--></div>
{% endif %}
