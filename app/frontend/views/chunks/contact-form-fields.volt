{% set field = 'name' %}
<div class="form-group{{ errors is defined and errors.filter(field) ? ' has-error' : '' }}">
    <label class="control-label" for="{{ field }}">
        Vaše ime
        <abbr title="Obavezno polje">*</abbr>
    </label>
    {% set field_value_default = '' %}
    {% if auth.logged_in() %}
        {% set logged_in_user = auth.get_user() %}
        {% set field_value_default = logged_in_user.getDisplayName() %}
    {% endif %}
    {{ textField([field, 'class':'form-control', 'placeholder':'Ime i prezime', 'value':(_POST[field] is defined ? _POST[field] : field_value_default)]) }}
    {% if errors is defined and errors.filter(field) %}
        <p class="help-block">{{ current(errors.filter(field)).getMessage() }}</p>
    {% endif %}
</div>

{% set field = 'email' %}
<div class="form-group{{ errors is defined and errors.filter(field) ? ' has-error' : '' }}">
    <label class="control-label" for="{{ field }}">
        E-mail adresa
        <abbr title="Obavezno polje">*</abbr>
    </label>
    {% set field_value_default = '' %}
    {% if auth.logged_in() %}
        {% set logged_in_user = auth.get_user() %}
        {% set field_value_default = logged_in_user.email %}
    {% endif %}
    {{ emailField([field, 'class':'form-control', 'placeholder':'E-mail adresa', 'value':(_POST[field] is defined ? _POST[field] : field_value_default)]) }}
    {% if errors is defined and errors.filter(field) %}
        <p class="help-block">{{ current(errors.filter(field)).getMessage() }}</p>
    {% endif %}
</div>

{% set field = 'message' %}
<div class="form-group{{ errors is defined and errors.filter(field) ? ' has-error' : '' }}">
    <label class="control-label" for="{{ field }}">
        Poruka
        <abbr title="Obavezno polje">*</abbr>
    </label>
    {{ textArea([field, 'class':'form-control', 'rows':'10', 'value':(_POST[field] is defined ? _POST[field] : '')]) }}
    {% if errors is defined and errors.filter(field) %}
        <p class="help-block">{{ current(errors.filter(field)).getMessage() }}</p>
    {% endif %}
</div>

{% if not (recaptcha_sitekey is empty) %}
    {% set field = 'captcha' %}
    <div class="form-group{{ errors is defined and errors.filter(field) ? ' has-error' : '' }}">
        <div class="g-recaptcha" data-sitekey="{{ recaptcha_sitekey }}"></div>
        {% if errors is defined and errors.filter(field) %}
            <p class="help-block">{{ current(errors.filter(field)).getMessage() }}</p>
        {% endif %}
    </div>
{% endif %}
