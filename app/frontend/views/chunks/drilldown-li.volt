{% for item in items %}
{% if item.active %}
<li>
    {% set children = item.children %}
    {% set hasChildren = (count(children) > 0) %}
    {% set paid_category_text = (mark_paid_categories ? (item.paid_category ? ' <span class="text-danger" title="Naplatna kategorija">*</span>' : '') : '') %}
    <a href="#" data-id="{{ item.id }}">{{ item.name ~ paid_category_text }}</a>
    {% if hasChildren %}
    {{ partial('chunks/drilldown-ul', ['items': children, 'class': '', 'mark_paid_categories': (mark_paid_categories ? mark_paid_categories : false)]) }}
    {% endif %}
</li>
{% endif %}
{% endfor %}
