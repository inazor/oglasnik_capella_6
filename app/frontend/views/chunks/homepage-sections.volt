<div class="row homepage-sections{{ full_with_homepage_sections|default(false) ? ' full-width' : '' }}">
    {% for homepage_section in homepage_sections %}{% include('chunks/homepage-sections-single') %}{% endfor %}
</div>
