{# Frontend Infraction Report View #}

<div class="smaller-vertical-gap no-tabs">
    <div class="title-single">
        <h1>{{ infractionReportTitle|default('Prijava radi kršenja pravila portala Oglasnik.hr') }}</h1>
        {% if infractionReportDescription is defined %}<p>{{ infractionReportDescription }}</p>{% endif %}
    </div>

    {{ flashSession.output() }}

    <section>
        <h2 class="underline"><span class="underline">{{ entity['modelName'] }}: {{ entity['title']|striptags }}</span></h2>
        <div class="row">
            <div class="col-lg-6 col-md-6 col-sm-8">
                {{ form(NULL, 'id':'frm_infraction_report', 'method':'post', 'autocomplete':'off') }}
                    {{ hiddenField('_csrftoken') }}
                    {{ hiddenField(['entity_id', 'value':entity['modelPkVal']]) }}
                    <div class="form-group">
                        <label class="control-label" for="reported_by">Tvoje ime</label>
                        {{ textField(['reported_by', 'class':'form-control', 'readonly':'readonly', 'value':auth.get_user().username]) }}
                    </div>
                    {% set field = 'report_reason' %}
                    <div class="form-group{{ errors is defined and errors.filter(field) ? ' has-error' : '' }}">
                        <label class="control-label" for="{{ field }}">
                            Razlog prijave
                            <abbr title="Obavezno polje">*</abbr>
                        </label>
                        <div class="icon_dropdown">{{ report_reason_dropdown }}</div>
                        {% if errors is defined and errors.filter(field) %}
                        <p class="help-block">{{ current(errors.filter(field)).getMessage() }}</p>
                        {% endif %}
                    </div>
                    <div class="form-group">
                        {% set field = 'message' %}
                        <label class="control-label" for="{{ field }}">
                            Komentar
                        </label>
                        {{ textArea([field, 'class':'form-control', 'rows':'5', 'value':(_POST[field] is defined ? _POST[field] : '')]) }}
                        {% if errors is defined and errors.filter(field) %}
                        <p class="help-block">{{ current(errors.filter(field)).getMessage() }}</p>
                        {% endif %}
                    </div>
                    <button class="btn btn-primary pull-right" type="submit">Prijavi <span class="fa fa-caret-right fa-fw"></span></button>
                {{ endForm() }}
            </div>
        </div>
    </section>
</div>
