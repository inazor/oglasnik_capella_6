<div class="modal fade modal-inspect-user-map" id="modal-inspect-user-map" tabindex="-1" role="dialog" aria-labelledby="modal-inspect-user-map-label">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-body">
                {% if mapTitle|default('') %}<h2 class="margin-0">{{ mapTitle }}</h2>{% endif %}
                {% if mapSubTitle|default('') %}<p class="margin-top-0">{{ mapSubTitle }}</p>{% endif %}
                <div class="border-radius border overflow-hidden margin-bottom-sm">
                    <div id="googleMapContainer" style="width:100%;height:420px;" data-address="{{ address }}"><!--IE--></div>
                </div>
                <img class="margin-right-sm" src="{{ url('assets/img/icn_map.png') }}" alt="">{{ address }}
            </div>
        </div>
    </div>
</div>
