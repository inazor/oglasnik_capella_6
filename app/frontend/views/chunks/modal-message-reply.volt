<div class="modal fade modal-send-message" id="modal-send-reply" tabindex="-1" role="dialog" aria-labelledby="modal-send-message-label">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            {{ form(formAction|default(null), 'id':'frm_modal_reply', 'method':'post', 'autocomplete':'off', 'class':'ajax-form') }}
                {{ hiddenField('_csrftoken') }}
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="modal-send-message-label">Odgovor na online upit</h4>
                </div>
                <div class="destination">
                    <div class="row">
                        <div class="col-sm-6">
                            Pošiljatelj:<br>
                            <strong>{{ (sender.type == 1 ? sender.username : sender.company_name) }}</strong>
                        </div>
                        <div class="col-sm-6">
                            Primatelj:<br/>
                            <strong class="recipient"></strong>
                        </div>
                    </div>
                </div>
                <div class="alert text-center hidden"></div>
                <div class="modal-body">
                    {% set field = 'replyMessage' %}
                    <div class="form-group{{ errors is defined and errors.filter(field) ? ' has-error' : '' }}">
                        <label for="{{ field }}" class="control-label">Poruka<abbr title="Obavezno polje">*</abbr></label>
                        {{ textArea([field, 'class':'form-control', 'placeholder':'Vaša poruka', 'rows':'8', 'value':'']) }}
                        <p class="help-block">{{ errors is defined and errors.filter(field) ? current(errors.filter(field)).getMessage() : '' }}</p>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary width-full modal-btn-send text-center light-blue">Pošalji poruku</button>
                </div>
                {{ endForm() }}
        </div>
    </div>
</div>
