{# pwdchanged email tpl #}
<p>Poštovani <b>{{ username }}</b>,</p>

<p>Obavještavamo Vas da je Vaša lozinka za pristup web stranicama <b>Oglasnik.hr</b> uspješno promjenjena.</p>

<p>Naša korisnička podrška dostupna je na <a href="tel:+38516102885"><strong>01/6102-885</strong></a> ili <a href="mailto:podrska@oglasnik.hr">podrska@oglasnik.hr</a>.</p>

<p>Srdačan pozdrav,<br>Oglasnik.hr</p>
