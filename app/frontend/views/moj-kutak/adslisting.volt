{# MyAccount ads listing #}
{% include 'chunks/moj-kutak-profile-header.volt' %}

<div class="side-col col-md-3 margin-top-lg">
    <div class="hidden-xs">
        <h2 class="margin-top-lg">Moji oglasi</h2>
        <ul class="user-cp-menu">
            <li{{ (current_action == 'sviOglasi' ? ' class="active"' : '') }}>{{ linkTo(['moj-kutak/svi-oglasi', 'Svi oglasi']) }} <span class="fr">({{ cumulativeAdsCounters['total'] }})</span></li>
            <li{{ (current_action == 'aktivniOglasi' ? ' class="active"' : '') }}>{{ linkTo(['moj-kutak/aktivni-oglasi', 'Aktivni oglasi']) }} <span class="fr">({{ cumulativeAdsCounters['total_active'] }})</span></li>
            <li{{ (current_action == 'istekliOglasi' ? ' class="active"' : '') }}>{{ linkTo(['moj-kutak/istekli-oglasi', 'Istekli oglasi']) }} <span class="fr">({{ cumulativeAdsCounters['total_expired'] }})</span></li>
            <li{{ (current_action == 'deaktiviraniOglasi' ? ' class="active"' : '') }}>{{ linkTo(['moj-kutak/deaktivirani-oglasi', 'Deaktivirani oglasi']) }} <span class="fr">({{ cumulativeAdsCounters['total_deactivated'] }})</span></li>
            <li{{ (current_action == 'neobjavljeniOglasi' ? ' class="active"' : '') }}>{{ linkTo(['moj-kutak/neobjavljeni-oglasi', 'Neobjavljeni oglasi']) }} <span class="fr">({{ cumulativeAdsCounters['total_unpublished'] }})</span></li>
            <li{{ (current_action == 'prodaniOglasi' ? ' class="active"' : '') }}>{{ linkTo(['moj-kutak/prodani-oglasi', 'Prodani oglasi']) }} <span class="fr">({{ cumulativeAdsCounters['total_sold'] }})</span></li>
            <li class="separator"><!--IE--></li>
            <li{{ (current_action == 'podijeliOglase' ? ' class="active"' : '') }}>{{ linkTo(['moj-kutak/podijeli-oglase', 'Podijeli oglase']) }}</li>
        </ul>
    </div>

    {{ form(null, 'id':'my_classifieds', 'class':'js-allow-double-submit', 'method':'get') }}
        <h3 class="margin-top-md">Traži po pojmu</h3>
        <div class="form-group">
            <div class="input-group">
                <input type="text" id="term" name="term" value="{{ searchTerm|default('') }}" class="form-control" placeholder="Upiši pojam...">
                <span class="input-group-btn">
                    <button class="btn btn-primary btn-default-padding" type="submit"><span class="fa fa-search"></span></button>
                </span>
            </div>
        </div>
    {{ endForm() }}
</div>

<div class="col-md-9 margin-top-lg">

    {{ flashSession.output() }}

    {% if current_action == 'podijeliOglase' %}
        <div class="form-group">
            <label class="control-label">Stalni link na sve vaše aktivne oglase koji možete poslati prijateljima</label>
            {{ textArea(['shareable_link', 'readonly':true, 'class':'form-control', 'value': shareable_link, 'style':'cursor:text']) }}
        </div>
        <div class="actions margin-top-sm width-full text-right">
            <button id="btnCopy2CLipboard" data-clipboard-text="{{ shareable_link }}" title="Kliknite ovdje da bi kopirali link">Kopiraj link &nbsp;<span class="fa fa-caret-right"></span></button>
        </div>
    {% elseif ads is defined and ads is iterable %}
        <p class="small margin-top-0">Ukupan broj oglasa: {{ results_total }}</p>

        <div>
        {% if ads|length > 0 %}
            {% for ad in ads %}
                {{ partial('moj-kutak/adslisting-detailed-chunk', ['ad': ad]) }}
            {% endfor %}

            {% if bulkActions is defined and bulkActions is iterable %}
            <div class="margin-top-sm">
                <div class="bulk-actions margin-top-md position-relative">
                    <input class="select-all checkbox checkbox-light-blue" type="checkbox" id="select-all" name="select-all"><label for="select-all"></label>
                    {% for bulkAction in bulkActions %}
                    {% set buttonAttributes = '' %}
                    {% if bulkAction['attributes'] is defined and bulkAction['attributes'] is iterable %}
                        {% for buttonAttr, buttonAttrValue in bulkAction['attributes'] %}
                            {% set buttonAttributes = buttonAttributes ~ buttonAttr ~ '="' ~ buttonAttrValue ~ '" ' %}
                        {% endfor %}
                    {% endif %}
                    <button{{ buttonAttributes ? ' ' ~ buttonAttributes|trim : '' }} disabled>{{ bulkAction['title'] }}</button>
                    {% endfor %}
                </div>
            </div>
            {% endif %}
            {% if pagination_links %}{{ pagination_links }}{% endif %}
        {% else %}
            <div class="alert alert-warning">Nema oglasa u ovom prikazu</div>
        {% endif %}
        </div>
    {% endif %}
</div>

{% include 'chunks/moj-kutak-profile-footer.volt' %}

<div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="deleteModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Zatvori"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="deleteModalLabel"><span class="text-danger">Upozorenje</span></h4>
            </div>
            {{ form(NULL, 'id': 'frm_ads_delete', 'method': 'post', 'autocomplete': 'off') }}
                {{ hiddenField(['next', 'value': next_url]) }}
                <div class="modal-body">
                    <p>
                        Odabrali ste <span class="text-danger">brisanje vašeg oglasa</span>. Ta akcija je nepovratna i
                        trajno će obrisati vaš oglas. Jeste li sigurni da to doista želite?
                    </p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Ne, odustani</button>
                    <button type="submit" class="btn btn-primary">Da, obriši oglas</button>
                </div>
            {{ endForm() }}
        </div>
    </div>
</div>

<div class="modal fade" id="bulkActionModal" tabindex="-1" role="dialog" aria-labelledby="bulkActionModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Zatvori"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="bulkActionModalLabel"><span class="text-danger">Upozorenje</span></h4>
            </div>
            {{ form(NULL, 'id':'frm_bulk_action', 'method':'post', 'autocomplete':'off') }}
                {{ hiddenField(['next', 'value':next_url]) }}
                <div class="modal-body"><p></p></div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Ne, odustani</button>
                    <button type="submit" class="btn btn-primary">Da, želim</button>
                </div>
            {{ endForm() }}
        </div>
    </div>
</div>

<div class="modal fade" id="orderCancellationModal" tabindex="-1" role="dialog" aria-labelledby="orderCancellationModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Zatvori"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="orderCancellationModalLabel"><span class="text-danger">Upozorenje</span></h4>
            </div>
            {{ form(NULL, 'id':'frm_order_cancellation', 'method':'post', 'autocomplete':'off') }}
                {{ hiddenField(['next', 'value':next_url]) }}
                <div class="modal-body">
                    <p>
                        Odabrali ste <span class="text-danger">otkazivanje narudžbe vašeg oglasa</span>. Ta akcija je 
                        nepovratna i trajno će otkazati vašu narudžbu.
                    </p>
                    <p>
                        Jeste li sigurni da želite nastaviti s otkazivanjem narudžbe?
                    </p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Ne, odustani</button>
                    <button type="submit" class="btn btn-primary">Da, otkaži narudžbu</button>
                </div>
            {{ endForm() }}
        </div>
    </div>
</div>
