{# MyAccount messages #}
{% include 'chunks/moj-kutak-profile-header.volt' %}

{% set loadContactFormModal = false %}

<div class="col-lg-9 col-md-9 margin-top-lg">
    {{ flashSession.output() }}

    {% if page.items|length %}
    <div class="panel-group" id="messages" role="tablist" aria-multiselectable="true">
        {% for message in page.items %}
            <div class="border-radius border-dark msg-box margin-bottom-sm msg-{{ message['read'] ? 'read' : 'new' }}" data-message-id="{{ message['id'] }}" data-message-read="{{ message['read'] ? '1' : '0' }}">
                <div class="box-header">
                    {{ message['title'] }}
                    <span class="msg-meta">
                        <span class="msg-date">Primljeno: <b>{{ message['created_at'] }}</b></span>
                        {#<span class="msg-id">ID: {{ message['id'] }}</span>#}
                    </span>
                </div>
                <div class="box-body">
                    <b>{{ message['sender_name'] }}</b>{# <span class="mail">{{ message['sender_email'] ? ' (' ~ message['sender_email'] ~ ')' : '' }}</span>#}
                    <p>{{ message['message'] }}</p>
                </div>
                <div class="box-footer message-actions text-right">
                    {% if message['canReply'] %}
                    {% set loadContactFormModal = true %}
                    <span class="btn btn-sm btn-reply" data-toggle="modal" data-target="#modal-send-reply" data-recipient-name="{{ message['sender_name'] }}"{{ message['sender_email'] ? ' data-recipient-email="' ~ message['sender_email'] ~ '"' : '' }} data-reply-url="{{ url('moj-kutak/odgovori-na-poruku/' ~ message['id']) }}"><span class="fa fa-mail-reply fa-fw"></span> Odgovori</span>
                    {% endif %}
                    <span class="btn btn-sm btn-danger" data-toggle="modal" data-target="#deleteModal" data-form-url="{{ url('moj-kutak/obrisi-poruku/' ~ message['id']) }}"><span class="fa fa-trash fa-fw"></span></span>
                </div>
            </div>
        {% endfor %}
    </div>
    {% if pagination_links %}{{ pagination_links }}{% endif %}
    {% else %}
    <p>Trenutno nemate niti jednu poruku.</p>
    {% endif %}
</div>

<div class="col-md-3 margin-top-lg inbox-sidebar">
    <h2 class="margin-top-0">Upravljanje Obavijestima</h2>
    <p>Ovdje možete vidjeti i upravljati s porukama/obavijestima koje vam šalju drugi korisnici Oglasnika zainteresirani za vaše oglase te primati novosti koje vam šalje Oglasnik.</p>
</div>

{% include 'chunks/moj-kutak-profile-footer.volt' %}

{% if loadContactFormModal %}
{{ partial('chunks/modal-message-reply', ['sender':current_user]) }}
{% endif %}
{{ partial('chunks/modal-delete', ['deleteModalWarning':'Da li ste sigurni da želite nastaviti s brisanjem ove poruke?<br /><span class="text-danger">Brisanje je <strong>nepovratna akcija</strong>!</span>']) }}
