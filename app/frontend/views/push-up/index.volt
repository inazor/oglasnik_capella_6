<section>
    <div class="underline margin-bottom-2em">
        <h1 class="underline">Skok na vrh</h1>
    </div>
    <p>Kupovinom opcije ‘Skok na vrh’ Vaš će se oglas jednokratno pozicionirati na vrhu liste u svojoj kategoriji.</p>
    <section>
        {{ flashSession.output() }}
        <h2 class="underline margin-bottom"><span class="underline">Oglas(i)</span></h2>
        {{ form(NULL, 'id':'frm_pushup_submit', 'method':'post', 'autocomplete':'off') }}
        {{ hiddenField('_csrftoken') }}
        {{ products_table }}
        <div class="margin-top-2em">
            <button class="btn btn-primary pull-right" type="submit" name="next" id="submitter" value="1"><span class="button-text">Plaćanje</span> <span class="fa fa-arrow-circle-right fa-fw"></span></button>
        </div>
        {{ endform() }}
    </section>
</section>
