{% set main_pic_thumb = null %}
{% if not (content_media is empty) and content_media.valid() %}
    {% set main_pic = content_media[0] %}
    {% set main_pic_thumb = main_pic.get_thumb('CMS-800x440').getSrc() %}
{% endif %}

<div class="margin-top-md">
    <div class="row">
        <div class="main-col col-lg-9 article">
            {% if main_pic_thumb %}<img width="100%" src="{{ main_pic_thumb }}" />{% endif %}
            <div class="row">
                <div class="col-md-offset-1 col-md-10">
                    <h1 class="no-top-margin">{{ content.title }}</h1>
                    {% if content.excerpt %}<div class="excerpt">{{ content.excerpt }}</div>{% endif %}
                    {{ content.content }}
                </div>
            </div>
        </div>
        <div class="side-col col-lg-3">
            {% if banners and banner_home_300x250 is defined %}
            <div class="banner sidebar-rectangle-holder banner-rect-articles hidden-sm hidden-xs">{{ banner_home_300x250 }}</div>
            {% endif %}
        </div>
    </div>
</div>
