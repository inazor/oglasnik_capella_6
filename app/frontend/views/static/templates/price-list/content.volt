{% if inArticle_media %}
    {% set main_pic_thumb = null %}
    {% if not (content_media is empty) and content_media.valid() %}
        {% set main_pic = content_media[0] %}
        {% set main_pic_thumb = main_pic.get_thumb('CMS-800x440').getSrc() %}
    {% endif %}
{% endif %}
<div class="border-radius border-dark margin-bottom-sm">
    <div class="box-header">
        <h2 style="margin:0 !important; padding-top:0 !important; border-top:0 !important">{{ content.title }}</h2>
        {% if content.content is defined and content.excerpt %}<p>{{ content.excerpt }}</p>{% endif %}
    </div>
    <div class="box-body">
        {% if inArticle_media and main_pic_thumb %}<img width="100%" src="{{ main_pic_thumb }}" />{% endif %}
        {{ content.content is defined ? content.content : (content.excerpt is defined and content.excerpt ? content.excerpt : '') }}
    </div>
</div>
