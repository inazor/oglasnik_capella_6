<div class="page-header page-header-user-dashboard xs-margin-offset-minus-10px">
    <div class="position-relative fixed-height">
        {% set profile_bg = user.getProfileBg() %}
        <div class="profile-picture overflow-hidden {{ profile_bg.getClass() }}" style="background-image:url({{ profile_bg.getSrc() }});"></div>
        <div class="container">
            {% if logged_in_owner %}
            <a class="izmjeni-sliku" href="{{ this.url.get('moj-kutak/coverbg') }}">Izmjeni svoju cover sliku</a>
            {% endif %}
            {% set avatar = user.getAvatar() %}
            <div class="border-radius avatar-holder overflow-hidden">
                {%- if logged_in_owner %}<a title="Izmjeni svoj avatar" href="{{ this.url.get('moj-kutak/avatar') }}">{% endif -%}
                    {{ avatar }}
                {%- if logged_in_owner %}<span class="edit-photo"></span>{% endif -%}
                {%- if logged_in_owner %}</a>{% endif -%}
            </div>
        </div>
    </div>
</div>
<div class="user-cp-nav xs-margin-offset-minus-10px">
    <div class="container">
        <div class="row">
            <div class="col-md-9 col-sm-offset-3 user-header-nav-wrapper">
                <div class="text-right pad-xs-5-only-lr">
                    {% if show_contact_form %}
                    {% set report_link = auth.logged_in() ? 'user/report/' ~ user.username : 'user/signin?next=user/report/' ~ user.username -%}
                    {{ linkTo([report_link, 'Prijavi korisnika', 'id':'btnReportShop', 'class':'btn secondary-red inverse']) }}
                    <button data-toggle="modal" data-target="#modal-send-message">Kontaktiraj</button>
                    {% endif %}
                </div>
            </div>
        </div>
    </div>
</div>

<a name="classifieds"></a>

<div class="container user-cp margin-top-lg reordered-on-mobile no-padding-xs">
    {{ form(null, 'id':'user_classifieds', 'class':'js-allow-double-submit', 'method':'get') }}
    <div class="row no-gutters-xs">
        <div id="filters-anchor" class="listings-side-col side-col col-md-3 hidden-xs">
            <div class="user-details hidden-sm hidden-xs">
                {{ user_info_markup }}
            </div>

            <h2 class="blue"><img class="margin-right-sm" src="{{ url('assets/img/icn_filter_category_01.svg') }}" alt="">Filtriranje oglasa</h2>
            {% if searchHasCategories is defined and searchHasCategories %}
                <h3 class="margin-top-md">Kategorije oglasa</h3>
                {% if searchCategoryId %}{{ hiddenField(['category_id', 'value':searchCategoryId, 'data-type':'text', 'data-default':'']) }}{% endif %}
                <div class="classifieds-categories-listing-sidebar">
                    {{ categoryTreeCountsMarkup }}
                    {% if needsShowAll %}
                        <div class="hidden-filters-more"><a class="all-cats-toggle more-btn" href="{{ showAllToggleHref }}">{{ showAllToggleText }}</a></div>
                    {% endif %}
                </div>
            {% endif %}

            <h3 class="margin-top-md">Traži po pojmu</h3>
            <div class="form-group">
                <div class="input-group filter-search">
                    <input type="text" class="form-control" placeholder="Upišite pojam..." name="ad_params_title" id="parameter_title" value="{{ fields['ad_params_title']|default('') }}" data-type="text" data-default="">
                    <span class="input-group-btn">
                        <button class="btn btn-search" type="submit">
                            <span class="fa fa-fw fa-search"></span>
                        </button>
                    </span>
                </div>
            </div>
            <div class="form-group checkbox checkbox-primary">
                <input type="checkbox" id="ad_params_uploadable" name="ad_params_uploadable" value="1"{{ fields['ad_params_uploadable']|default(0) ? ' checked="checked"' : '' }} data-type="checkbox" data-default="false">
                <label for="ad_params_uploadable">Prikaži samo sa slikom</label>
            </div>

            <h3>Cijena</h3>
            <div class="form-group">
                <div class="input-group">
                    <span class="input-group-addon">Od</span>
                    <input type="text" id="parameter_price_from" name="ad_price_from" value="{{ fields['ad_price_from']|default('') }}" class="form-control text-right" data-type="number" data-default="0">
                    <span class="input-group-addon">HRK</span>
                </div>
            </div>
            <div class="form-group">
                <div class="input-group">
                    <span class="input-group-addon">Do</span>
                    <input type="text" id="parameter_price_to" name="ad_price_to" value="{{ fields['ad_price_to']|default('') }}" class="form-control text-right" data-type="number" data-default="0">
                    <span class="input-group-addon">HRK</span>
                </div>
            </div>

            <div class="margin-top-md">
                <button class="btn btn-primary width-full text-bold margin-bottom-md" type="submit">Pretraži</button>
                <button class="btn width-full btn-reset btn-grey btn-reset-inactive text-bold margin-bottom-md" type="reset">Poništi filter</button>
            </div>
        </div>
        <div class="listings-main-col col-md-6">
            {{ flashSession.output() }}

            <div class="user-details visible-sm visible-xs hidden-md hidden-lg">
                {{ user_info_markup }}
            </div>

            <div id="filterBitsContainer">
                <a class="hidden-md hidden-lg btn display-block filters-anchor-link" href="#filters-anchor">
                    Filtriraj oglase
                </a>
            </div>

            <div class="grid-sort-options margin-bottom-sm row no-gutters-xs">
                <div class="col-xs-6 col-sm-7 upper-pagination">{% if pagination_links_top is defined and pagination_links_top %}{{ pagination_links_top }}{% endif %}</div>
                <div class="col-xs-6 col-sm-5 sort-dropdown">
                    {% if ads is defined and ads|length %}
                        {{ sort_dropdown }}
                    {% endif %}
                </div>
            </div>
            {% set narrowGridMode = true %}
            {% set enableUpSells = false %}
            {% include 'chunks/ads-mixed.volt' %}
        </div>
        <div class="col-md-3 listings-banner-col-right hidden-xs hidden-sm">
        </div
    </div>
    {{ endForm() }}
</div>

{% if show_contact_form %}
    {{ partial('chunks/modal-message-user', ['receiver': user, 'formAction':contact_form_url, 'messageEntityField':'user_id', 'messageEntityID':user.id]) }}
{% endif %}
