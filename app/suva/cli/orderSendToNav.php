<?php
require 'autoload.php';

use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;

$base_url = 'http://ogladev.oglasnik.hr';

// Use a specific cookie jar
$jar = new GuzzleHttp\Cookie\CookieJar;

$post_data = array(
    'username' => 'nremenar',
    'password' => 'nremenar1',
    'submit' => 'submit',
    'next' => 'suva/obrade/navisionSendFiskal'
);

$client = new Client([
    'base_uri' => $base_url,
    'cookies' => $jar,
    'allow_redirects' => true,
    'debug' => true,
    'auth' => ['staging', 'Yov}:5GXXt5']
]);

$csrf_token = false;
$login_response = $client->get('/user/signin');
$code = $login_response->getStatusCode();
if (200 === $code) {
	$login_markup = (string) $login_response->getBody();
	$matches = array();
	$match = preg_match('/<input type="hidden" id="_csrftoken" name="_csrftoken" value="(.*?)"/', $login_markup, $matches);
	if (!empty($matches)) {
		if (!empty($matches[1])) {
			$csrf_token = $matches[1];
		}
	}
}

if (null !== $csrf_token) {
	$post_data['_csrftoken'] = $csrf_token;
	$response = $client->post(
        '/user/signin', [
                'form_params' => $post_data,
        ]
	);

	file_put_contents('test.json', $response->getBody());
}
