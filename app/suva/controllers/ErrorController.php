<?php

namespace Baseapp\Suva\Controllers;

use Baseapp\Traits\ErrorControllerTrait;

class ErrorController extends IndexController
{
    use ErrorControllerTrait;
}
