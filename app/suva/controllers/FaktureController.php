<?php

namespace Baseapp\Suva\Controllers;

use Phalcon\Dispatcher;
use Phalcon\Exception;
use Phalcon\Http\Request\FileInterface;
use Phalcon\Paginator\Adapter\QueryBuilder;
use Phalcon\Mvc\Model\Query;

use Baseapp\Bootstrap;
use Baseapp\Controllers\BaseSuvaController;
use Baseapp\Extension\Tag;
use Baseapp\Traits\CrudActions;
use Baseapp\Traits\CrudHelpers;

use Baseapp\Library\Debug;
use Baseapp\Library\SimpleXMLProcessorRba;
use Baseapp\Library\Tool;
use Baseapp\Library\Utils;

use Baseapp\Suva\Models\Ads;
use Baseapp\Suva\Models\FiscalLocations;
use Baseapp\Suva\Models\N_Discounts;
use Baseapp\Suva\Models\N_Insertions;
use Baseapp\Suva\Models\N_Products;
use Baseapp\Suva\Models\Orders;
use Baseapp\Suva\Models\OrdersItems;
use Baseapp\Suva\Models\OrdersItemsEx;
use Baseapp\Suva\Models\OrdersStatuses;
use Baseapp\Suva\Models\Products;
use Baseapp\Suva\Models\Publications;
use Baseapp\Suva\Models\Users;

use Phalcon\Mvc\View;
use mPDF;

use Phalcon\Db\Adapter\Pdo;
//use Baseapp\Library\Db\Adapter\Mssql;


use Baseapp\Suva\Library\Nava;
// use Baseapp\Library\Mssql;

class FaktureController extends IndexController {
    use CrudActions;
    use CrudHelpers;

	

	
    protected $session_import_warnings_key = 'orders_bank_xml_upload_import_warnings';

    public $crud_model_class = 'Baseapp\Suva\Models\Orders';

    protected $allowed_roles = array('admin', 'finance', 'content');

    protected function getPossibleOrderByOptions() {
        $order_bys = array(
            'created_at'   => 'Date created',
            'modified_at'  => 'Date modified',
            'valid_until'  => 'Valid until',
            'id'           => 'Order ID'
        );

        return $order_bys;
    }

    protected function getStylingData() {
        $status_names = Orders::getAllStatuses();

        $data = array(
            Orders::STATUS_NEW       => array(
                'title'     => $status_names[Orders::STATUS_NEW],
                'icon'      => '<span class="fa fa-fw"></span>',
                'classname' => 'info'
            ),
            Orders::STATUS_CANCELLED => array(
                'title'     => $status_names[Orders::STATUS_CANCELLED],
                'icon'      => '<span class="fa fa-fw"></span>',
                'classname' => 'warning'
            ),
            Orders::STATUS_EXPIRED   => array(
                'title'     => $status_names[Orders::STATUS_EXPIRED],
                'icon'      => '<span class="fa fa-fw"></span>',
                'classname' => 'danger'
            ),
            Orders::STATUS_COMPLETED => array(
                'title'     => $status_names[Orders::STATUS_COMPLETED],
                'icon'      => '<span class="fa fa-fw"></span>',
                'classname' => 'success'
            ),
			
			
			5       => array(
                'title'     => $status_names[5],
                'icon'      => '<span class="fa fa-fw"></span>',
                'classname' => 'info'
            ),
            6 => array(
                'title'     => $status_names[6],
                'icon'      => '<span class="fa fa-fw"></span>',
                'classname' => 'warning'
            ),
            7   => array(
                'title'     => $status_names[7],
                'icon'      => '<span class="fa fa-fw"></span>',
                'classname' => 'danger'
            ),
            8 => array(
                'title'     => $status_names[8],
                'icon'      => '<span class="fa fa-fw"></span>',
                'classname' => 'success'
            )
			
        );

        return $data;
    }

    /**
     * Index Action
     */
    public function indexAction( $pUserId = 0, $pStatus = 0 ) {
// 		$this->printr($_REQUEST);
// 		$this->printr($_POST);
        $this->tag->setTitle('Fakture');
		
	    $this->assets->addCss('assets/vendor/jasny-bootstrap-3.1.3-dist/css/jasny-bootstrap.min.css');
        $this->assets->addJs('assets/vendor/jasny-bootstrap-3.1.3-dist/js/jasny-bootstrap.min.js');
		
// 		//error_log($this->request->getPost('n_status'));

		//DODAVANJE U CONTEXT ILI BRISANJE AKO JE POZVANA FUNKCIJA BEZ ORDER STATUSA

		// ovo ne radi
// 		if ($pStatus > 0){
// 			$os = \Baseapp\Suva\Models\OrdersStatuses::findFirst($pStatus);
// 			if ($os) $_SESSION['_context']['selected_order_status_id'] = $os->id;
// 			else return $this->greska("Wrong Order status ID supplied ( $pStatus )");
// 		} elseif ($this->request->hasPost('n_status')){
// 			$os = \Baseapp\Suva\Models\OrdersStatuses::findFirst( (int)$this->request->getPost('n_status') );
// 			if ($os) $_SESSION['_context']['selected_order_status_id'] = $os->id;
// 		}
		
		//borna - ovo radi
		if ($pStatus > 0){
			$os = \Baseapp\Suva\Models\OrdersStatuses::findFirst($pStatus);
			if ($os) $_SESSION['_context']['selected_order_status_id'] = $os->id;
			else return $this->greska("Wrong Order status ID supplied ( $pStatus )");
		} elseif ($_GET['n_status'] > 0){
			$os = \Baseapp\Suva\Models\OrdersStatuses::findFirst( (int)$_GET['n_status'] );
			if ($os) $_SESSION['_context']['selected_order_status_id'] = $os->id;
		}
		
		else unset ($_SESSION['_context']['selected_order_status_id']);
		
		$fisk_error = Nava::getJirZikFromNav();
		if ($fisk_error) $this->view->setVar('fisk_error',  $fisk_error);
		

        $page               = $this->getPageParam();
        $n_search_status    = $this->n_processSearchStatus();
        $search_for         = $this->processSearchParam();
        $mode               = $this->processSearchMode();
        $dir                = $this->processAscDesc('desc');
        $order_by           = $this->processOrderBy('created_at');
        $limit              = $this->processLimit();

        // If we spot a PBO prefix in there, remove it from the searched string
        // and force the search field to 'id'
        $forced_field = null;
        if (Utils::str_has_pbo_prefix($search_for)) {
            $search_for   = trim(Utils::str_remove_pbo_prefix($search_for));
            $forced_field = 'id';
        }

        Tag::setDefaults(array(
            'q'        => $search_for,
            'mode'     => $mode,
            'dir'      => $dir,
            'order_by' => $order_by,
            'limit'    => $limit
        ), true);

        // Status dropdown
        // $any_status = array(0 => 'Svi statusi');
        // $statuses   = $any_status + Orders::n_getAllStatuses();
        // $status     = $this->processOptions('status', $statuses, null);

        // $n_svi_statusi = OrdersStatuses::find()->toArray();
		// $n_svi = $this->processOptions('n_svi', $n_svi_statusi, null);

				
		// $n_statuses   = Nava::getAllowedStatuses($this->auth->get_user()->id, $n_search_status);
		// $n_status     = $this->processOptions('n_status', $n_statuses, null);
		
		// if (!$n_search_status && $pStatus){
			// $next_statuses   = Nava::getAllowedStatuses($this->auth->get_user()->id, $pStatus);
		// } else {
			// $next_statuses   = Nava::getAllowedStatuses($this->auth->get_user()->id, $n_search_status);
		// }



        // Available search fields
        $available_fields = array(
            'id',
            'ad_id',
            'user_id',
            'payment_method',
            'total',
            'cart',
            'created_at',
            'modified_at',
            'valid_until'
        );
        $field = $this->processOptions('field', $available_fields, $forced_field);
        Tag::setDefault('field', $field);
        if (null === $field || empty($field)) {
            $field = 'all';
        }

        // Build the query
        $builder = $this->modelsManager->createBuilder()
            ->columns(array(
				$this->crud_model_class . '.id',
				$this->crud_model_class . '.user_id',
				$this->crud_model_class . '.status',
				$this->crud_model_class . '.total',
				$this->crud_model_class . '.n_agency_commission_amount',
				$this->crud_model_class . '.payment_method',
				$this->crud_model_class . '.valid_until',
				$this->crud_model_class . '.created_at',
				$this->crud_model_class . '.modified_at',
				$this->crud_model_class . '.n_status',

				'Baseapp\Suva\Models\Users.type',
				'Baseapp\Suva\Models\Users.email',
				'Baseapp\Suva\Models\Users.username',
				'Baseapp\Suva\Models\Users.phone1',
				'Baseapp\Suva\Models\Users.phone2',
				'Baseapp\Suva\Models\Users.company_name',
				'Baseapp\Suva\Models\Users.first_name',
				'Baseapp\Suva\Models\Users.last_name',
				'Baseapp\Suva\Models\Users.oib',
				'Baseapp\Suva\Models\Users.address',
				'Baseapp\Suva\Models\Users.city',
				'Baseapp\Suva\Models\Users.zip_code',
				'Baseapp\Suva\Models\Users.county_id',
				'Baseapp\Suva\Models\Users.country_id'
            ))
            ->from($this->crud_model_class);
            /*
                    $builder->leftJoin(
                        'Baseapp\Suva\Models\OrdersItems',
                        'Baseapp\Suva\Models\OrdersItems.order_id = ' . $this->crud_model_class . '.id'
                    );
            */

        // Special case the search for orders matching a specific ad_id
        if ('ad_id' === $field) {
            // This is bugged (causes a duplicate join statement for users table... go Phalcon!)
            // $builder->addFrom('Baseapp\Suva\Models\OrdersItems');
            // $builder->andWhere('Baseapp\Suva\Models\OrdersItems.ad_id = ' . (int) $search_for);
			
			//SAMO AKTIVNI OI
            $builder->innerJoin(
                'Baseapp\Suva\Models\OrdersItems',
                'Baseapp\Suva\Models\Orders.id = Baseapp\Suva\Models\OrdersItems.order_id AND Baseapp\Suva\Models\OrdersItems.ad_id = ' . (int) $search_for
            );
            $builder->groupBy($this->crud_model_class . '.id');
        }

        $builder->leftJoin(
            'Baseapp\Suva\Models\Users',
            'Baseapp\Suva\Models\Users.id = ' . $this->crud_model_class . '.user_id'
        );

        // if ($status > 0) {
            // $builder->andWhere($this->crud_model_class . '.n_status = :status:', array('status' => $status));
        // }

        if ($n_search_status > 0) {
            $builder->andWhere($this->crud_model_class . '.n_status = :n_status:', array('n_status' => $n_search_status));
        }
		

		
// 			  if ($pUserId > 0 ) {
//             $builder->andWhere($this->crud_model_class . '.user_id = :vrij:', array('vrij' => $pUserId));
//         }

// 		//OVO SA DVA PARAMETRA NE RADI
		if ($pStatus > 0) {
            $builder->andWhere($this->crud_model_class . '.n_status = '.$pStatus);
        }
		
		if ($pUserId > 0) {
            $builder->andWhere($this->crud_model_class . '.user_id = '.$pUserId);
        }

        // If a specific field is specified, search only that one, otherwise search across all the fields
        // (but only if there's an actual search term entered)
        $fields_to_search = (null === $field || 'all' === $field) ? $available_fields : array($field);
        $search_params = Utils::build_phalcon_search_sql_params($fields_to_search, $search_for, $mode);
        if (null !== $search_params && is_array($search_params)) {
            $conditions = array();
            $binds = array();
            foreach ($search_params as $fld => $val) {
                // Skipping ad_id field
                if ('ad_id' === $fld) {
                    continue;
                }
                $field_bind_name = sprintf(':%s:', $fld);
                $conditions[] = $this->crud_model_class . '.' . $fld . ' LIKE ' . $field_bind_name;
                $binds[$fld] = $val;
                // $builder->orWhere($fld . ' LIKE ' . $field_bind_name, array($fld => $val));
            }
            if (!empty($conditions)) {
                $builder->andWhere(implode(' OR ', $conditions), $binds);
            }
        }

        $builder->orderBy($this->crud_model_class . '.' . $order_by . ' ' . strtoupper($dir));

        $paginator_params = array(
            'builder' => $builder,
            'limit'   => $limit,
            'page'    => $page
        );
        $paginator    = new QueryBuilder($paginator_params);
        $current_page = $paginator->getPaginate();

        // Populates the detail records of each order record about to be shown on this page by only
        // issuing one query with an IN() statement
        //$results_array = Orders::buildResultsArray($current_page->items);
        //$current_page->items = $results_array;

        // If JSON or CSV (with latest XML Bank statement warnings) are requested,
        // dump the result and return the response early
        if ($this->request->hasQuery('json') || $this->request->hasQuery('import-warnings')) {
            // FIXME/TODO:
            // Again, really just making sure the 'support' user doesn't
            // end up here, because its allowed on the controller level.
            // But when just reading the code, one wonders why this check is here...
            if ($this->auth->logged_in(array('admin', 'finance'))) {
                $this->disable_view();

                if ($this->request->hasQuery('json')) {
                    $results_array_modified = $this->modifySearchResultsForJsonExport($results_array);
                    $this->response->setContentType('application/json', 'UTF-8');
                    $this->response->setJsonContent($results_array_modified);
                } elseif ($this->request->hasQuery('import-warnings')) {
                    if ($this->has_session_import_warnings()) {
                        $this->response->setContentType('text/csv', 'UTF-8');
                        $this->response->setHeader('Content-Disposition', 'attachment; filename="import-warnings.csv"');
                        $this->response->setContent($this->get_session_import_warnings());
                        // clear the session where we store last import's errors/warnings
                        $this->session->set($this->session_import_warnings_key, array());
                    } else {
                        return $this->redirect_to('suva/fakture');
                    }
                }
                return $this->response;
            } else {
                $this->flashSession->error($this->insufficient_privileges);
            }
        }

        $pagination_links = Tool::pagination(
            $current_page,
            'suva/fakture',
            'pagination',
            $this->config->settingsSuva->pagination_count_out,
            $this->config->settingsSuva->pagination_count_in
        );

		
		
		
		
		//dodat allowed actions na $current_page
// 		dodavanje varijablu svih ordera
		// $orders = Array();
		// foreach( $current_page->items as $order){
			// //dohvat dopuštenih akcija za promjenu statusa ordera
			// //$order['status_change_actions'] = Nava::arrToDict(Nava::getAllowedActions($this->auth->get_user()->id, $order['n_status'], 'order-status'));

			// //dohvat dopuštenih akcija uz order
			// //$order['actions'] = Nava::arrToDict(Nava::getAllowedActions($this->auth->get_user()->id, $order['n_status'], 'orders'));
			// error_log ("tu sam 11");
			// //dohvat order itema - ako postoje dohvat i akcija za order iteme (iste su za sve order iteme pa se mogu spremiti na razini ordera)
			// $txtSql =	
				// "select "
				// ."	oi.id "
				// ."	,oi.order_id "
				// ."	,oi.product_id "
				// ."	,oi.ad_id "
				// ."	,oi.n_publications_id "
				// ."	,(case when p.name is not null then p.name else 'N/A' end) as 'title' "
				// ."	,oi.price "
				// ."	,oi.qty "
				// ."	,oi.total "
				// ."	,oi.n_products_id "
				// ."	,oi.n_first_published_at "
				// ."	,oi.n_expires_at  "
				// ." from orders_items oi "
				// ."	left join n_products p on (oi.n_products_id = p.id) "
				// ." where oi.order_id =".$order['id']
				// ."	and oi.n_is_active = 1";
			// $ordersItems = Nava::arrToDict( Nava::sqlToArray ( $txtSql ) );
			// //error_log(print_r("ORDERS ITEMS OD ORDERA SQL:".$txtSql." POLJE ".$order['id'].":".$ordersItems,true));	
			
			// if ($order['details']) unset($order['details']);
			// if ($order['table_markup']) unset($order['table_markup']);
			
			
			// //error_log(print_r($ordersItems,true));
			// if ($ordersItems){
				// $order['orders_items'] = $ordersItems;
				// $order['orders_items_actions'] = Nava::arrToDict(Nava::getAllowedActions($this->auth->get_user()->id, $order['n_status'], 'orders-items'));
			// }
			// $orders[$order['id']] = $order;
		// }
		
 		//error_log(print_r($orders,true));
		
		$this->view->setVar('p_user_id', $pUserId);
		$this->view->setVar('p_n_status', $pStatus);

		$this->view->setVar('n_status_name', OrdersStatuses::findFirst($n_search_status)->description);
		$this->view->setVar('page', $current_page);
		$this->view->setVar('pagination_links', $pagination_links);
		//$this->view->setVar('styling_data', $this->getStylingData());

		$this->view->setVar('json_download_href', $this->buildJsonExportHref());
		$this->view->setVar('import_warnings_download_href', $this->buildImportWarningsHref());
 
		$this->view->setVar('orders', $orders);
		$this->view->setVar('orders_statuses',  Nava::arrToDict(OrdersStatuses::find()->toArray()));
		$this->view->setVar('next_statuses',  $next_statuses);
		$this->view->setVar('publications',  Nava::arrToDict(Publications::find()->toArray()));
		
// 		$this->view->setVar('can_user_perform',  Nava::canUserPerformAction('actOrderFieldSubcustomerEdit', $this->auth->get_user()->id));
		
    }

	public function crudAction($pEntityId = null) {
		$this->printr($_SESSION['context'], "FaktureController::crudAction: context");
		$this->n_crud_action("Orders", $pEntityId); //ovo je definirano u BaseController
		$this->view->pick('fakture/crud');
	}
	
    protected function has_session_import_warnings() {
        $has_import_warnings = false;

        $session_import_warnings = array();

        if ($this->session->has($this->session_import_warnings_key)) {
            $session_import_warnings = $this->session->get($this->session_import_warnings_key);
        }

        if (is_array($session_import_warnings) && count($session_import_warnings)) {
            $has_import_warnings = true;
        }

        return $has_import_warnings;
    }

    protected function get_session_import_warnings() {
        $content = '';

        if ($this->has_session_import_warnings()) {
            $warnings = $this->session->get($this->session_import_warnings_key);

            $curr_row = 1;
            $content .= '"No",';
            $content .= '"Filename",';
            $content .= '"Order #",';
            $content .= '"PBO",';
            $content .= '"Error/Warning"' . PHP_EOL;
            foreach ($warnings as $warning) {
                foreach ($warning['warnings'] as $row) {
                    $content .= $curr_row . ',';
                    $content .= '"' . $warning['filename'] . '",';
                    $content .= '"' . $row['ID'] . '",';
                    $content .= '"' . $row['PBO'] . '",';
                    $content .= '"' . $row['MSG'] . '"' . PHP_EOL;
                    $curr_row++;
                }
            }
        }

        return $content;
    }

    /**
     * Builds and returns the link/href to be used for exporting latest import warnings from uploaded/parsed
     * XML bank statements.
     *
     * @return string
     */
    protected function buildImportWarningsHref() {
        $href = null;

        if ($this->has_session_import_warnings()) {
            $query = $this->request->getQuery();
            unset($query['_url']);
            $query['import-warnings'] = 1;

            $href = $this->url->get('suva/fakture', $query);
        }

        return $href;
    }

    /**
     * Builds and returns the link/href to be used for exporting the current page of search results
     * as JSON. It should maintain any existing query parameters and append/modify our `json=1` param
     * (which we're currently using to detect if results should be dumped to JSON or not).
     *
     * @return string
     */
    protected function buildJsonExportHref() {
        $query = $this->request->getQuery();
        unset($query['_url']);
        $query['json'] = 1;


        return $href;
    }

    /**
     * Transforms data returned by `Orders::buildResultsArray()` into a slightly
     * different array (specified by Oglasnik, since they want their JSON export fields
     * named differently and certain data in a different format)
     *
     * @param array $results
     *
     * @return array
     */
    protected function modifySearchResultsForJsonExport(array $results) {
        $modified_results = array();
        $order_modified   = array();

        foreach ($results as $order) {
            $order_modified['ID'] = $order['id'];

            // Merge user invoice details
            $user_invoice_details            = Users::buildInvoiceDetailsArray($order);
            $order_modified                  = array_merge($order_modified, $user_invoice_details);

            $order_modified['USER_ID']       = $order['user_id'];
            $order_modified['USERNAME']      = $order['username'];

            // Add other stuff they required
            $order_modified['INVOICENUMBER'] = $order['pbo'];
            $order_modified['AMOUNTTOPAY']   = $order['total'];
            $order_modified['AMOUNTTOPAYLP'] = $order['total_lp'];
            $order_modified['INVOICEDATE']   = $order['modified_at'];

            // Adding some stuff I think will come in handy down the line
            $order_modified['STATUS']     = $order['status'];
            $order_modified['STATUSTEXT'] = $order['status_text'];

            if ('offline' === $order['payment_method'] || empty($order['payment_method'])) {
                $order_modified['PAYMENTMETHOD'] = 0;
                $order_modified['CREDITCARDID']  = 'opca_virman';
            } else {
                $order_modified['PAYMENTMETHOD'] = 1;
                $order_modified['CREDITCARDID']  = $order['payment_method'];
            }

            $order_modified['PRODUCTS'] = array();
            foreach ($order['details'] as $k => $product) {
                $price_kn = Utils::lp2kn($product->price);
                $total_kn = Utils::lp2kn($product->total);

                // Simple attempt to get durations as requested. Might suffice.
                $duration = $this->extractDurationNumbersFromDurationPhrases($product->title);
                if (null !== $duration) {
                    $order_modified['PRODUCTS'][$k]['DURATION'] = $duration;
                }

                $order_modified['PRODUCTS'][$k]['PRODUCT_ID']             = $product->product_id;
                $order_modified['PRODUCTS'][$k]['PRODUCTNAME']            = $product->title;
                $order_modified['PRODUCTS'][$k]['PRODUCTQTY']             = $product->qty;
                $order_modified['PRODUCTS'][$k]['PRODUCTPRICE']           = $price_kn;
                $order_modified['PRODUCTS'][$k]['PRODUCTPRICELP']         = $product->price;
                $order_modified['PRODUCTS'][$k]['PRODUCTPRICETOTAL']      = $total_kn;
                $order_modified['PRODUCTS'][$k]['PRODUCTPRICETOTALLP']    = $product->total;
                $order_modified['PRODUCTS'][$k]['PRODUCTDISCOUNT']        = 0;
                $order_modified['PRODUCTS'][$k]['PRODUCTDISCOUNTPRICE']   = $price_kn;
                $order_modified['PRODUCTS'][$k]['PRODUCTDISCOUNTPRICELP'] = $product->price;
            }

            $modified_results[] = $order_modified;
        }

        return $modified_results;
    }

    /**
     * @param $string
     *
     * @return array|int|null
     */
    protected function extractDurationNumbersFromDurationPhrases($string) {
        $duration = null;

        // Looking for numbers separated by a space followed by certain known words
        $regex = '/(\d+)\s(dana?|objav[ae]?)/i';

        $matches = array();
        $matched = preg_match_all($regex, $string, $matches);
        if (false !== $matched && $matched > 0) {
            // Only want the numbers (first capturing group)
            if (isset($matches[1]) && !empty($matches[1])) {
                $duration = array_map('intval', $matches[1]);
            }
        }

        if (1 === count($duration)) {
            $duration = (int) $duration[0];
        }

        return $duration;
    }

    /**
     * @param int|null $order_id
     *
     * @return Orders|false
     */
    protected function checkEntity($order_id = null) {
        /* @var $entity Orders */
        $entity = Orders::findFirst((int) $order_id);
        if (!$entity) {
            $this->flashSession->error('Invalid Order specified!');
        }

        if ($entity) {
            if (!$entity->isStatusChangeable()) {
                $this->flashSession->error(sprintf('%s (status=%s) cannot be modified any more!', $entity->ident(), $entity->n_getStatusText()));
                // Resetting back to false so that controller actions can abort/redirect properly
                $entity = false;
            }
        }

        return $entity;
    }

    /**
     * @param $entity Orders
     * @param int $status
     *
     * @return \Phalcon\Http\ResponseInterface
     */
    public function insertToCartAction($ad_id){
		// $this->poruka("Poziva se insertToCartAction FaktureController.php 661");
        $product_id  = $_POST['offline-products'];
        $discount_id = $_POST['offline-discounts'];
        $publications_id = $_POST['offline-publications'];
        $offline_issues = $_POST['offline-issues'];

        $user_id = $_POST['user_id'];
 
         $this->disable_view();
         $this->response->setContentType('application/json', 'UTF-8');
         $response_array = array('status' => true);

        if (!$this->request->isAjax()) {
            $this->response->setStatusCode(400, 'Bad Request');
            $response_array['msg'] = 'Bad Request';
        } else {
            
            $order = Orders::findFirst(
                        array(
                            "conditions" => "n_status = 1 AND user_id = ?1",
                            "bind" => array(1 => $_POST['user_id'])
                            )
                        );

            if (!$order){
                $order = new Orders();

                $order->user_id = $user_id;
                $order->status = 1;
                $order->created_at = date('Y-m-d H:i:s');
                $order->created_at = date('Y-m-d H:i:s');
                $order->ip = $this->request->getClientAddress();
                $order->n_status = 1;

                if ($order->save() == false) {
                $response_array['status'] = false;
                $err_msg = "";
                foreach ($order->getMessages() as $message) {
                         $err_msg .= $message . "\n\r";
                }

                 $response_array['msg'] = $err_msg;

            }
            }



            $product = N_Products::findFirst((int)$product_id);
            
            if ($discount_id > 0){
                $discount = N_Discounts::findFirst((int)$discount_id);    
                $discount_amount = $discount->amount;
            }else{
                $discount_amount = 0;
            }
            
                
            $product_add = new OrdersItemsEx();     
           
            $product_add->n_products_id = $product_id;
            if ($discount_id > 0){
                $product_add->n_discounts_id = $discount_id;
            }
            $product_add->n_publications_id = $publications_id;
            $product_add->order_id = $order->id;
            $product_add->ad_id = $ad_id;
            $product_add->price = $product->unit_price * ((100 - $discount_amount) / 100) * 100;
            $product_add->qty = count($offline_issues);
            $product_add->title = $product->name . " - " . $product_add->qty . " objava" ;
            $product_add->total =  $product_add->price * $product_add->qty;
            $product_add->tax_rate = 0;
            $product_add->tax_amount = 0;
            $product_add->product = serialize($product_add);
            $product_add->product_id = $product->name;

            if ($product_add->save() == false) {
                $response_array['status'] = false;
                $err_msg = "";
                foreach ($product_add->getMessages() as $message) {
                         $err_msg .= $message . "\n\r";
                }

                 $response_array['msg'] = $err_msg;

            }else{

                $order->total = $order->total + ($product_add->total / 100 );
               
                if ($order->save() == false) {
                    $response_array['status'] = false;
                            $err_msg = "";
                            foreach ($order->getMessages() as $message) {
                                     $err_msg .= "INSERT INSERTION - " . $message . "\n\r";
                            }

                            $response_array['msg'] = $err_msg;
                }

                if ($offline_issues){

                    foreach ($offline_issues as $n_insertion) {
                        
                        $n_issue = new N_Insertions();

                        $n_issue->id = new \Phalcon\Db\RawValue('""');
                        $n_issue->product_id = $product_id;
                        $n_issue->orders_items_id = $product_add->id;
                        $n_issue->issues_id = $n_insertion;

                        if ($n_issue->save() == false) {
                            $response_array['status'] = false;
                            $err_msg = "";
                            foreach ($n_issue->getMessages() as $message) {
                                     $err_msg .= "INSERT INSERTION - " . $message . "\n\r";
                            }

                            $response_array['msg'] = $err_msg;
                        } 
                    }
                }

                if ($response_array['status'] == true){
                    $response_array['msg'] = 'Added item!';
                }

            }

       }

        $this->response->setJsonContent($response_array);
        return $this->response;

    }
		
	protected function saveStatus($entity, $status) {
        $result = $entity->save(array('status' => $status));
        if ($result) {
            // Do some extra stuff when an order is canceled from the suva
            if ($entity::STATUS_CANCELLED === $status) {
                $entity->handleCancellationOfOrderItems();
            }
            $this->flashSession->success(sprintf('%s status changed to "%s" successfully', $entity->ident(), $entity->n_getStatusText()));
        } else {
            $errors = $this->errorMessagesTransformer($entity);
            Bootstrap::log($errors);
            $this->flashSession->error(sprintf('Failed saving (%s) status! Error(s): %s', $entity->ident(), $errors));
        }

        return $this->redirect_back();
    }

    public function statusAction($order_id, $status_id) {
        $entity = $this->checkEntity((int)$order_id);

        if (!$entity) {
            return $this->redirect_back();
        }
         if (!$status_id || !is_numeric($status_id)) {
            return $this->redirect_back();
        }
        $result = $entity->save(array('n_status' => (int)$status_id));
        if ($result) {
            // Do some extra stuff when an order is canceled from the suva
            if ($entity::STATUS_CANCELLED === $status) {
                $entity->handleCancellationOfOrderItems();
            }
            $this->flashSession->success(sprintf('%s status changed to "%s" successfully', $entity->ident(), $entity->n_getStatusText()));
        } else {
            $errors = $this->errorMessagesTransformer($entity);
            Bootstrap::log($errors);
            $this->flashSession->error(sprintf('Failed saving (%s) status! Error(s): %s', $entity->ident(), $errors));
        }


        return $this->redirect_back();
    }

    protected function errorMessagesTransformer($entity) {
        // TODO: extend Phalcon\Validation\Message\Group to encapsulate this via __toString() or something...
        $msgs = array();
        foreach ($entity->getMessages() as $msg) {
            $msgs[] = $msg->getMessage();
        }
        $msg_string = implode(', ', $msgs);

        return $msg_string;
    }

    public function completeAction($order_id = null)  {
        $entity = $this->checkEntity($order_id);

        if (!$entity) {
            return $this->redirect_back();
        }

        $success = $entity->finalizePurchase();
        if ($success) {
            $this->flashSession->success(sprintf('%s finalized successfully', $entity->ident()));
        } else {
            // FIXME: more detailed error message? Could be expired, or already finalized, or cancelled etc...
            $this->flashSession->error(sprintf('Failed finalizing purchase for %s', $entity->ident()));
        }

        return $this->redirect_back();
    }

	// Handle file upload
    public function bankXmlUploadAction() {
        // Only 'admin' and 'finance' roles should be allowed here
        if (!$this->auth->logged_in(array('admin', 'finance'))) {
            $this->flashSession->error($this->insufficient_privileges);
            return $this->redirect_to('suva/fakture');
        }

        if (!$this->request->hasFiles()) {
            $this->flashSession->error('Nothing uploaded!');
            return $this->redirect_to('suva/fakture');
        }

        $files = $this->request->getUploadedFiles();

        if (count($files)) {
            // clear the session for last uploaded set of files
            $this->session->set($this->session_import_warnings_key, array());

            foreach ($files as $file) {
                $this->processBankXML($file);
            }
        }

        return $this->redirect_to('suva/fakture');
    }

    /**
     * Process bank's payments xml file
     *
     * @param FileInterface|string $file Filepath (as a string) or a \Phalcon\Http\Request\FileInterface
     *
     * @throws \Exception
     */
    protected function processBankXML($file) {
        if ($file instanceof FileInterface) {
            $filepath = $file->getTempName();
            $filename = $file->getName();
        } else {
            $filepath = $file;
            $filename = basename($filepath);
        }

        $processor = new SimpleXMLProcessorRba();

        // Avoiding errors/warnings from libxml since the RBA xml DTD is missing
        if (!$processor->open($filepath, LIBXML_NOERROR | LIBXML_NOWARNING)) {
            throw new \RuntimeException('Unable to open XML file');
        }

        // Trying to fully/properly validate the XML fails since the referenced DTD is missing:
        // `xmlns="http://www.etna.hr/schemas/Izvod"` -> 404
        /*
        $processor->setParserProperty(\XMLReader::VALIDATE, true);
        if (!$processor->isValid()) {
            $this->flashSession->error('Failed to validate XML');
        }
        */

        $processor->parse();
        $processor->close();

        if ($processor->called()) {

            $audit_log_prefix = 'Bank XML upload (' . $filename . '): ';
            $this->flashSession->success('XML (' . $filename . ') successfully parsed.');

            $processor->processFoundPbos();

            $stats = $processor->getStats();
            if ($stats['pbo_matches'] > 0) {
                $message_matches = sprintf('Found %d payment record(s) matching our PBO prefix', $stats['pbo_matches']);
                if ($stats['pbo_duplicates'] > 0) {
                    $message_matches .= sprintf(' (%d duplicate(s))', $stats['pbo_duplicates']);
                }
                $this->flashSession->notice($message_matches);
                $this->saveAudit($audit_log_prefix . $message_matches);
            } else {
                $message_no_matches = 'No payments matching our PBO prefix were found';
                $this->flashSession->notice($message_no_matches);
                $this->saveAudit($audit_log_prefix . $message_no_matches);
            }
            if ($stats['completed'] > 0) {
                $message_completed = sprintf('Finalized %d orders', $stats['completed']);
                $this->flashSession->notice($message_completed);
                $this->saveAudit($audit_log_prefix . $message_completed);

                // Save info about finalized/completed orders in the audit log
                $completed_messages = $processor->getCompletedMessages();
                if (!empty($completed_messages)) {
                    foreach ($completed_messages as $message) {
                        $this->saveAudit($audit_log_prefix . $message);
                    }
                }
            }

            // Show any warnings that might have have occurred
            $warnings = $processor->getWarnings();
            if (!empty($warnings)) {
                foreach ($warnings as $warning) {
                    $this->flashSession->warning($warning);
                    $this->saveAudit($audit_log_prefix . $warning);
                }

                // get warnings formatted for export also
                $export_warnings = $processor->getWarnings($for_export = true);
                if (!empty($export_warnings)) {
                    $session_order_export_warnings = $this->session->get($this->session_import_warnings_key);
                    $session_order_export_warnings[] = array(
                        'filename' => $filename,
                        'warnings' => $export_warnings,
                    );
                    $this->session->set($this->session_import_warnings_key, $session_order_export_warnings);
                }
            }
        } else {
            $this->flashSession->error('XML parser callbacks have not been called! Is the XML file structure ok?');
        }
    }

    // Overriding deleteAction in order to completely prevent deletion
    public function deleteAction($entity_id = null) {
        $this->flashSession->error('Fakture ne mogu biti izbrisane!');
        return $this->redirect_back();
    }

 	public function deleteItemAction($entity_id = null) {

        $item = OrdersItems::findFirst($entity_id);

        if ($item != false) {
            if ($item->delete() == false) {
                $msg = "Sorry, we can't delete the item right now: \n";

                foreach ($item->getMessages() as $message) {
                    $msg = $message . "\n";
                }
            } else {
                $msg = "The item was deleted successfully!";

                $order = Orders::findFirst($item->order_id);
                $order->total = $order->total - $item->total;

                $order->save();

            }
        }

        $this->flashSession->error($msg);
        return $this->redirect_back();
    }

    public function previewAction(Orders $order_id = null){

        $this->assets->addCss('assets/vendor/jasny-bootstrap-3.1.3-dist/css/jasny-bootstrap.min.css');
        $this->assets->addJs('assets/vendor/jasny-bootstrap-3.1.3-dist/js/jasny-bootstrap.min.js');

        $order = Orders::findFirst((int) $order_id);
        $user = Users::findFirst((int) $order->user_id);

         $order->sales_rep = Users::find(array(
          'conditions' => 'id=:n_sales_rep_id:',
          'bind'       => array('n_sales_rep_id' => $order->n_sales_rep_id)
    ));

            $order->discount = N_Discounts::find(array(
          'conditions' => 'id=:n_discounts_id:',
          'bind'       => array('n_discounts_id' => $order->n_discounts_id)
    ));

        $order->items = OrdersItems::find(array(
              'conditions' => 'order_id=:order_id:',
              'bind'       => array('order_id' => $order->id)
        ));

        $this->tag->setTitle('Fakture');
        $this->view->setLayout('common');
        $this->view->setVar('user', $user);
        $this->view->setVar('order', $order);


    }
		
	public function createPdfActionOLD($order_id, $pStatus = null) {
		//error_log ("createPdfAction:".$order_id."  #######  1");
		//error_log ("tu sam 2");

		$this->assets->addCss('assets/vendor/jasny-bootstrap-3.1.3-dist/css/jasny-bootstrap.min.css');
		$this->assets->addJs('assets/vendor/jasny-bootstrap-3.1.3-dist/js/jasny-bootstrap.min.js');

		$order = Orders::findFirst((int) $order_id);
		if (!$order) return $this->greska("Nije pronađen Order br. $order_id");
		$user = Users::findFirst((int) $order->user_id);

		$order->items = OrdersItems::find(array(
			'conditions' => 'order_id=:order_id:',
			'bind'       => array('order_id' => $order->id)
		));
		
		if ($pStatus){
			//sko je definiran status (ako se želi printat ponuda umjesto fakture)
			$order->n_status = $pStatus;
		}

		$order->sales_rep = Users::findFirst($order->n_sales_rep_id);
		
		
		$order->discount = N_Discounts::find(array(
			'conditions' => 'id=:n_discounts_id:',
			'bind'       => array('n_discounts_id' => $order->n_discounts_id)
		));
		
		//formiranje arraya koji sadrži popuste u za svaki order-item
		//$order->items stavit u array
		$oiDiscounts = Array();
		$ordersItems = Nava::sqlToArray("select id from orders_items where n_is_active = 1 and order_id = $order->id");
		foreach ($ordersItems as $oi){
			$oiDiscRec = Nava::SqlToArray("select d.* from n_orders_items_discounts oid join n_discounts d on (oid.discounts_id = d.id) where oid.orders_items_id = ".$oi['id']);
			$oiDiscounts[$oi['id']]  = $oiDiscRec ? $oiDiscRec : Array();
		}
		
		
		$this->view->setVar('oi_discounts', $oiDiscounts);
		//error_log("POPUSTI: ".print_r($oiDiscounts,true));
		//error_log("Order item: ".print_r($ordersItems,true));
		//error_log ("tu sam 3");
		$this->tag->setTitle('Fakture');
		$this->view->setLayout('common');
		$this->view->setVar('user', $user);
		$this->view->setVar('order', $order);
		$this->view->setVar('products', Nava::arrToDict(Products::find()->toArray()));
		$this->view->setVar('kreirao', Users::findFirst($order->user_id));
		
		$this->view->setVar('debug', $order->toArray());
		

		if($order->n_sub_customer > 0){
			$this->view->setVar('sub_customer', Users::findFirst((int)$order->n_sub_customer));
		}
		
		$mpdf = new mPDF();

		$this->view->start();
		$this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
		//$this->view->pick("fakture/intro");
		$this->view->render('fakture', 'pdfDoc');
		$this->view->finish();
		$content = $this->view->getContent();
		//$html = $this->view->partial('fakture/intro');
		$mpdf->WriteHTML($content );
		$mpdf->Output("test.php", "I");
		$this->view->disable();
		
		//error_log(print_r($order->toArray(), true));
		//error_log ("createPdfAction:".$order_id."  #######  99");
		
		
}
	
	public function fiskalAction(){

		$txtSql = "select max([No_]) as numb from [Oglasnik d_o_o_".'$'."Sales Invoice Header] where [Posting Date] = '2016-02-29'";
		
		$txtSql = "select * from [Oglasnik d_o_o_".'$'."Sales Invoice Header] where [External Document No_] = '900000015'";
		//povezani su preko [External Document No_]
		
		//$txtSql = "select min([No_]) as numb from [Oglasnik d_o_o_".'$'."Sales Invoice Header] where [Issuer Protection Code] > ''";
		//$txtSql = "select min([External Document No_]) as numb from [Oglasnik d_o_o_".'$'."Sales Invoice Header] where [Issuer Protection Code] > '' and [External Document No_] > '9'";
		$this->view->setVar('podaci',Nava::msSqlToArray($txtSql));	
		
		//u sve orders koji nemaju pbo ubacit pbo:
		$orders = Orders::find(
			array(
				"n_pbo is Null",
			)
		);
		foreach ($orders as $order) {
			if ($order->getPbo()){
				Nava::runSql("update orders set n_pbo = '".$order->getPbo()."' where id = ".$order->id);
			}
		}
		
		//DOHVATIT SVE FAKTURE KOJE NEMAJU JIR I ZIK, U NAV-U POTRAŽITI JIR I ZIK I PREBACITI GA. AKO NE POSTOJI STAVITI N/A
		$sveFaktBez = Nava::arrToDict(Nava::sqlToArray("select * from orders where n_invoice_jir is null"));
		//$sveFaktBez = Nava::arrToDict(Nava::sqlToArray("select * from orders"));
		foreach ( $sveFaktBez as $fakt ){
			$nav = Nava::msSqlToArray(
				"select "
				." sih.[Issuer Protection Code] as zik "
				.", sih.[Unique Invoice No_] as jir "
				.", convert(varchar(30),sih.[Posting DateTime],120) as date_time "
				." from [Oglasnik d_o_o_".'$'."Sales Invoice Header] sih "
				." where [External Document No_] = '".$fakt['n_pbo']."' "
			);
			if ($nav){
				//error_log (gettype($nav[0]['date_time']));
				//error_log ($nav[0]['date_time']);
				Nava::runSql(
					"update orders set "
					." n_invoice_zik = '".$nav[0]['zik']."' "
					.", n_invoice_jir = '".$nav[0]['jir']."' "
					.", n_invoice_date_time_issue = '".$nav[0]['date_time']."' "
					." where id = ".$fakt['id']
				); 
			} else {
				Nava::runSql(
					"update orders set "
					." n_invoice_zik = 'N/A' "
					.", n_invoice_jir = 'N/A' "
					." where id = ".$fakt['id']	
				);
			}
			
			//TODO: fakture koje su dobile JIR i ZIK idu u status plaćeno
		}
		
		
		
		
	}
	
	public function add_common_crud_assets() {
//         $this->assets->addCss('assets/vendor/jquery-ui-1.11.2-custom/jquery-ui.min.css');
//         $this->assets->addJs('assets/vendor/jquery-ui-1.11.2-custom/jquery-ui.min.js');

        $this->assets->addCss('assets/vendor/magicsuggest/magicsuggest-min.css');
        $this->assets->addJs('assets/vendor/magicsuggest/magicsuggest-min.js');

//         $this->assets->addCss('assets/vendor/datepicker3.css');
//         $this->assets->addJs('assets/vendor/bootstrap-datepicker.js');

// //         $this->assets->addJs('assets/suva/js/classifieds-edit.js');
// 		$this->assets->addJs('assets/suva/js/input_search.js');

//         $this->assets->addCss('assets/vendor/awesome-bootstrap-checkbox.css');
//         $this->assets->addJs('assets/js/classified-submit-choose-ad-type.js');
    }
	
	public function editAction($entity_id) {
 		$ime_foldera = "fakture";
    	$ime_objekta = "Orders";
		
         $entity_id = (int) $entity_id;

        $model = $this->crud_model_class;
		
				$this->add_common_crud_assets();
		
		
        /* @var $entity \Baseapp\Suva\Models\Users */
        $entity = $model::findFirst($entity_id);

        if (!$entity) {
            $this->flashSession->error(sprintf($this->ime_objekta.' not found [Model: %s, ID: %s]', $this->crud_model_class, $entity_id));
            return $this->redirect_back();
        }
       
       $this->view->setVar('error_check', $_POST); 

        if ($this->request->isPost()) {

                 
                $result = $entity->save_changes($this->request);
                if ($result instanceof Orders) {
					
					//TODO: RIJEŠITI!!!
					if ($_REQUEST['payment_method']) Nava::runSql("update orders set payment_method = '".$_REQUEST['payment_method']."' where id = $entity_id");
					if ($_REQUEST['n_payment_methods_id']) Nava::runSql("update orders set n_payment_methods_id = ".$_REQUEST['n_payment_methods_id']." where id = $entity_id");
					if ($_REQUEST['n_sales_rep_id']) Nava::runSql("update orders set n_sales_rep_id = ".$_REQUEST['n_sales_rep_id']." where id = $entity_id");
					if ($_REQUEST['n_sub_customer']) Nava::runSql("update orders set n_sub_customer = ".$_REQUEST['n_sub_customer']." where id = $entity_id");
					
 					if ($_REQUEST['created_at']) Nava::runSql("update orders set created_at = ".Nava::dateToSqlDate($_REQUEST['created_at'])." where id = $entity_id");
					if ($_REQUEST['modified_at']) Nava::runSql("update orders set modified_at = ".Nava::dateToSqlDate($_REQUEST['modified_at'])." where id = $entity_id");
					if ($_REQUEST['valid_until']) Nava::runSql("update orders set valid_until = ".Nava::dateToSqlDate($_REQUEST['valid_until'])." where id = $entity_id");
					
					if ($_REQUEST['n_payment_date']) Nava::runSql("update orders set n_payment_date = ".Nava::dateToSqlDate($_REQUEST['n_payment_date'])." where id = $entity_id");
					if ($_REQUEST['n_invoice_due_date']) Nava::runSql("update orders set n_invoice_due_date = ".Nava::dateToSqlDate($_REQUEST['n_invoice_due_date'])." where id = $entity_id");
					if ($_REQUEST['n_quotation_date']) Nava::runSql("update orders set n_quotation_date = ".Nava::dateToSqlDate($_REQUEST['n_quotation_date'])." where id = $entity_id");
					if ($_REQUEST['n_invoice_date_time_issue']) Nava::runSql("update orders set n_invoice_date_time_issue = ".Nava::dateToSqlDate($_REQUEST['n_invoice_date_time_issue'])." where id = $entity_id");
					
					$this->flashSession->success("<PRE>".print_r($_REQUEST, true)."</pre>".'<strong>'.$this->ime_objekta.' saved successfully!</strong>');
                    //$this->flashSession->success('<pre>'.print_r($_REQUEST,true).'</pre><strong>'.$this->ime_objekta.' saved successfully!</strong>');
                    // Determine which save button was used
					$this->view->setVar('error_check', $_POST);
                    $save_action = $this->get_save_action();
                    if ('save' === $save_action) {
                        return $this->redirect_self();
                    } else {
                        $next_url = $this->get_next_url();
                        return $this->redirect_to($next_url);
                    }
                } else {
					
                    $this->view->setVar('errors', $result);
                    $this->flashSession->error('<strong>Error!</strong> Fix all fields that failed to validate.'.$err_msg);
                }

        }
			$this->tag->setTitle('Edit '.$this->ime_objekta);
			$form_title_long = 'Edit '.$this->ime_objekta;
			$form_action = 'edit';
			$this->view->setVar('entity', $entity);
			$this->view->setVar('entity_name', $this->ime_objekta);

			$this->view->setVar('form_title_long', $form_title_long);
			$this->view->setVar('form_action', $form_action);
			$this->view->setVar('user_name', Users::findFirst($entity->user_id)->username);
			$this->view->setVar('fiscal_location_name', FiscalLocations::findFirst($entity->n_fiscal_location_id)->name);
			$this->view->setVar('sales_rep', Users::findFirst($entity->n_sales_rep_id));
			$this->view->setVar('sub_customer', Users::findFirst($entity->n_sub_customer));
			$this->view->setVar('payment_methods', array('','cash', 'amex','diners','mastercard','offline','visa'));
			
		
		
		
    }
	
}