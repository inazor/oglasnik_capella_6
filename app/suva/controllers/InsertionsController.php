<?php

namespace Baseapp\Suva\Controllers;

class InsertionsController extends IndexController{

    public function indexAction() {
			$this->tag->setTitle("Insertions");
			$this->n_query_index("Insertions"); //ovo je definirano u BaseController
    }

	public function crudAction($pEntityId = null) {
		//$this->printr($_REQUEST);
		$this->n_crud_action("Insertions", $pEntityId); //ovo je definirano u BaseController
		$this->view->pick('insertions/crud');
	}
}