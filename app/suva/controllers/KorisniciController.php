<?php

namespace Baseapp\Suva\Controllers;

use Phalcon\Dispatcher;
use Baseapp\Extension\Tag;
use Baseapp\Library\Tool;
use Baseapp\Library\Utils;
use Baseapp\Suva\Models\Roles;
//require_once(Baseapp\Suva\Models\Users);
use Baseapp\Suva\Models\Users;
use Baseapp\Suva\Models\Locations;
use Baseapp\Traits\CrudActions;
use Baseapp\Traits\CrudHelpers;
use Baseapp\Traits\ParametrizatorHelpers;
use Phalcon\Paginator\Adapter\QueryBuilder;

use Baseapp\Suva\Models\UsersContacts;
use Baseapp\Suva\Models\Categories;
use Baseapp\Suva\Models\OrdersStatuses;
use Baseapp\Suva\Models\FiscalLocations;
use Baseapp\Suva\Library\Nava;

/**
 * Backend Users Controller
 */
class KorisniciController extends IndexController{
    use ParametrizatorHelpers;
    use CrudActions;
    use CrudHelpers;

    /* @var \Baseapp\Suva\Models\Users */
    public $crud_model_class = 'Baseapp\Suva\Models\Users';

    protected $allowed_roles = array('admin', 'support', 'finance', 'moderator');
	
	// public function beforeExecuteRoute(Dispatcher $dispatcher){
		
		// error_log("KorisniciController: beforeExecuteRoute");
	// }

	public function crudAction($pEntityId = null) {
		//$this->printr($_REQUEST, "KorisniciController.php:crudAction REQUEST");  
		//$this->printr($_SESSION['_context'] , "KorisniciController.php:crudAction CONTEXT" ); 
		//error_log("korisnici::crudAction 1");
		
		$defaults = array();
		if (array_key_exists('_default_n_source',$_SESSION['_context'])){
			$defaults['n_source'] = $_SESSION['_context']['_default_n_source'];
		}
		$this->n_crud_action("Users", $pEntityId, false, $defaults ); //show default parameters je maknut jer nem ože bit na korisniku, jer traži i orabrane role
		//error_log("korisnici::crudAction 2");
		$this->view->pick('korisnici/crud');
		//error_log("korisnici::crudAction 3");
		
	}
	
	public function quickFindAction(){
		$this->poruka("QuickFind");
		if (!array_key_exists('_context', $_SESSION)) {
			$this->greska ("System error: Context not supplied.");
			return $this->redirect_back();
		}
		if (!array_key_exists('selected_phone_number', $_SESSION['_context'])) {
			$this->greska ("System error: Search parameter not supplied.");
			return $this->redirect_back();
		}
		$selPhoneNo = $_SESSION['_context']['selected_phone_number'];

		$this->poruka ($selPhoneNo);
		//resetirati filtere
		unset ($_SESSION['_context']['filter']);
		unset ($_SESSION['_context']['default_filter']);
		unset ($_SESSION['_context']['order_by']);
		unset ($_SESSION['_context']['from_rec']);
		unset ($_SESSION['_context']['to_rec']);
		unset ($_SESSION['_context']['count_rec']); 


		
		$uc = new \Baseapp\Suva\Models\UsersContacts();
		$uc->name = $selPhoneNo;
		$strSearch = $uc->makeSearchString();
		
		// $this->poruka ($strSearch);
		
		
		$txtSql = 
			" select user_id "
			." from n_users_contacts "
			." where "
			." 	name like '%$strSearch%' "
			."  group by user_id "
			."  limit 1000 ";
		
	
		// $this->poruka($txtSql);
		// error_log("KorisniciController:quickFindAction txtSql:".$txtSql);
		
		//$this->poruka ($txtSql);
		$rs = Nava::sqlToArray($txtSql);
		if (count($rs) > 0){
			$ids = array();
			foreach($rs as $row){
				array_push($ids, $row['user_id']);
			}
			//kriterij može biti i lista vrijednosti
			$_SESSION['_context']['default_filter']['id'] = $ids;
			$_SESSION['_context']['keep_default_filter'] = true; //da se kod poziva preko dispatchera ne izbriše 
			$_SESSION['_context']['keep_list_params'] = true; 
			
			$this->poruka(count($rs). " user/s found");
		}
		else $this->greska ("No user/s found");


		$this->poruka(count($rs). " user/s found");
		
		//error_log("Korisnicicontroller::quickfind SESSION CONTEXT nakon porebacivanja iz requesta ".print_r($_SESSION['_context'],true));
			
		$this->dispatcher->forward(
            array(
                "controller" => "korisnici",
                "action"     => "index",
                "params"     => array()
            )
        );
	}


    public function indexAction() {
		//$this->printr($_SESSION['_context']);
		$_SESSION['_context']['step_rec'] = 20;
		$this->tag->setTitle("Users");
		
		/*
		ako je poslan parametar Role_id onda se koristi join
		1. dodaje se array join
		2. iz konteksta se miče filter __n_users_roles__id
		
		*/
		$joins = null;
		if ( array_key_exists( 'filter', $_SESSION['_context'] )  && is_array($_SESSION['_context']['filter'])  && array_key_exists('__UsersRoles__role_id' ,$_SESSION['_context']['filter'])){ 
			$joins = array (
				array(
					'joinedModelTable' => 'roles_users',
					'originalModelField' => 'id',
					'joinedModelField' => 'user_id',
					'joinedModelConditions' => array (
						array(
							'field' => 'role_id', 
							'operator' => '=',
							'value' => $_SESSION['_context']['filter']['__UsersRoles__role_id']
						)
					)
				)
			);
		
			//unset ($_SESSION['_context']['filter']['__UsersRoles__id']);
		}
		
		 
		$_SESSION['_context']['default_order_by']['id'] = 'desc';
		
		$this->n_query_index("Users", null, $joins); //ovo je definirano u BaseController
		
		unset($_SESSION['_context']['default_filter']['id']);
		
		//error_log("NIK korisniciaController:n_index CONTEXT".print_r($_SESSION['_context'],true));
    }	
	
	public function chooseAction( $pUserId = null, $edit = null ){
		/*
			TODO: Čemu služi parametar edit i kada se poziva
		*/
		
		if ($pUserId > 0){
			$user = \Baseapp\Suva\Models\Users::findFirst( $pUserId);
			if (!$user) return $this->greska ("Nije pronađen korisnik s ID-jem $pUserId");
			$_SESSION['_context']['selected_user_id'] = $user->id;
			//error_log("KorisniciController:chooseAction: selected_user_id je ".$_SESSION['_context']['selected_user_id']);
		} 
		else {
			if (! array_key_exists("_context", $_SESSION)) return $this->greska("Nije definirana varijabla context");
			if (! array_key_exists("selected_user_id", $_SESSION['_context'])) return $this->greska("Nije definirana varijabla selected_user_id");
			
			$id = $_SESSION['_context']['selected_user_id'];
			if ( !$id > 0 ) return $this->greska ("Poslan je netočan ID odabranog korisnika, $id");
		
			$user = \Baseapp\Suva\Models\Users::findFirst( $id);
			if (!$user) return $this->greska ("Nije pronađen korisnik s ID-jem $id");
		}	
		
		//IGOR: redirectanje umjesto forwardanja.
		// dodana je mogućnost da se edit pokreće preko user id-ja, pa da traži prvi ad
		
		//389 - ako fali neki od obaveznih podataka na korisniku, 
		//nakon choose user neka pošalje poruku "Unijeti obavezne podatke" i neka prebaci na editor korisnika.
		if ( $user->save() == false ){
			$this->response->redirect("suva/korisnici/crud/$user->id");
			$greske = $user->getMessages();
			foreach ($greske as $greska)  
				$err_msg .= $greska.", ";
			$this->flashSession->error('<strong>Error!</strong> Fix all fields that failed to validate.'.$err_msg);
		}
		else{
				if($edit > 0){
					$_SESSION['_context']['_active_tab'] = 'users_ads';
					$_SESSION['_context']['_keep_active_tab_setting'] = true;
					$this->response->redirect("suva/korisnici/crud/$user->id");
				}else{
					$_SESSION['_context']['_active_tab'] = 'users_ads';
					$_SESSION['_context']['_keep_active_tab_setting'] = true;
					$this->response->redirect("suva/ads2/edit/0/$user->id");
				}
		}
	}
	
    
	public function createAction() {
		$this->assets->addJs('assets/backend/js/classifieds-index.js');
		
        // Only 'admin' and 'support' roles allowed to create new users
        if (!$this->auth->logged_in(array('admin', 'support'))) {
            $this->flashSession->error($this->insufficient_privileges);
            return $this->redirect_to('suva/users');
        }

        $this->tag->setTitle('Create user');


        // Use the same form/view as editAction
        $this->view->pick('korisnici/edit');

        // New users can't be impersonated yet obviously
        $this->view->setVar('show_impersonate_link', false);

        $form_title_long = 'Add user';
        $form_action = 'add';
        $send_email = $this->request->hasPost('send_email') ? 1 : 0;
				
		$korisnik = new Users();
		$korisnik->create();
		$backend_signup = $korisnik->backend_signup($this->request);
		$this->view->setVar('errors', $backend_signup);
				 

        $user = new Users();
        if ($this->request->isPost()) {
            $backend_signup = $user->backend_signup($this->request);
            if ($backend_signup instanceof Users) {
                $email_notice = '';
                if ($this->request->getPost('action') == 'add' && $this->request->hasPost('send_email')) {
                    $email_notice = 'Activation link has been sent.';
                }
                $this->flashSession->success('<strong>User successfully created!</strong> ' . $email_notice);
                $save_action = $this->get_save_action();
                $next_url = $this->get_next_url();
							
// 				svaki korisnik kod kreiranja ima rolu login
				if (is_int((int)$backend_signup->id)){             
                    Nava::runSql(
                        "    delete ru "
                        ."    from roles_users ru "
                        ."        join roles r on (ru.role_id = r.id and r.name = 'login')  "
                        ."    where ru.user_id = ".$backend_signup->id
                    );

                    Nava::runSql("insert into roles_users (user_id, role_id) values (".$backend_signup->id.",(select id from roles where name = 'login'))");
                }
				
                // Override next url to go to entity edit in save button case
                if ('save' === $save_action) {
                    $next_url = 'suva/korisnici/edit/' . $user->id;
                }
                return $this->redirect_to($next_url);
            } else {
                $this->flashSession->error('<strong>Error!</strong> Fix all fields that failed to validate.');
                $this->view->setVar('errors', $backend_signup);
            }
        } else {
            $user->assignDefaults(array('newsletter' => 1));
        }

        $this->country_dropdown($user);
        $this->county_dropdown($user);
        $this->setup_roles_checkboxes($user);

        $this->view->setVar('user', $user);
        $this->view->setVar('send_email', $send_email);
        $this->view->setVar('form_title_long', $form_title_long);
        $this->view->setVar('form_action', $form_action);
    }
	
		/**
     * Edit Action
     */
	public function editAction($entity_id = null) {

		
		$this->assets->addJs('assets/backend/js/classifieds-index.js');
		
        $entity_id = (int) $entity_id;
		//$this->printr($_REQUEST);

        $model = $this->crud_model_class;
        /* @var $entity \Baseapp\Suva\Models\Users */
        $entity = $model::findFirst($entity_id);
		// //error_log(print_r($entity->oib));

		
		
		
		//error_log("korisnici edit action 1");
		
        if (!$entity) {
            $this->flashSession->error(sprintf('Entity not found [Model: %s, ID: %s]', $this->crud_model_class, $entity_id));
            return $this->redirect_back();
        }

        $this->tag->setTitle('Uređivanje podataka o korisniku '.$entity->username);

		
        //iz ads controllera radi modal popupa     
        

        $form_title_long = 'Uređivanje podataka o korisniku '.$entity->username;
        $form_action = 'edit';

        if ($this->request->isPost() && strlen($_REQUEST['_contact_action']) > 1){
			
			//error_log("korisnici edit action 2");
						switch($_REQUEST['_contact_action']){
							case "contact_edit":
								$checkedContactId = $this->request->hasPost('_contact_id') ? $this->request->getPost('_contact_id') : null;
								$checkedValue = $this->request->hasPost('_contact_value') ? $this->request->getPost('_contact_value') : null;
								if($checkedContactId && $checkedValue)
								Nava::runSql("update n_users_contacts set name = '$checkedValue' where id = $checkedContactId");
							break;
							
							case "contact_delete":
								$checkedContactId = $this->request->hasPost('_contact_id') ? $this->request->getPost('_contact_id') : null;
								if($checkedContactId)
								Nava::runSql("delete from n_users_contacts where id = $checkedContactId");
							break;
							
							case "contact_create":
								$checkedUserId = $this->request->hasPost('_contact_user_id') ? $this->request->getPost('_contact_user_id') : null;
								$checkedType = $this->request->hasPost('_contact_type') ? $this->request->getPost('_contact_type') : null;
								$checkedValue = $this->request->hasPost('_contact_value') ? $this->request->getPost('_contact_value') : null;
								if($checkedUserId && $checkedValue >'' && $checkedType) {
									//error_log("Ads::n_after_save 11 checked checkedUserId = $checkedUserId");
									//error_log("Ads::n_after_save 12 checked checkedValue = $checkedValue");
									//error_log("Ads::n_after_save 12 checked checkedType = $checkedType");
									
									$txtSql = "insert into n_users_contacts(name,type,user_id) values ('".$checkedValue."','".$checkedType."', ".$checkedUserId.")";
									//error_log("Ads::n_after_save 13 checked txtSql = $txtSql");
									Nava::runSql($txtSql);
								} 
							break;
								
						}
						
				}
				elseif ($this->request->isPost()) {
					//error_log("korisnici edit action 3");
            if ($this->auth->logged_in(array('admin', 'support'))) {
// 				$this->printr($_REQUEST);
				
				
                $saved = $entity->backend_save($this->request);
                if ($saved instanceof Users) {
                    //error_log("korisnici edit action 4");
                    					//TODO: RIJEŠITI!!!
					if ($_REQUEST['n_r1']) $n_r1 = 1; else $n_r1 = 0;
					if ($_REQUEST['n_is_salesman']) $n_is_salesman = 1; else $n_is_salesman = 0;
					$txtSql = "update users set n_r1 = $n_r1, n_is_salesman = $n_is_salesman where id = $entity_id";
					Nava::runSql($txtSql);
					
					//$this->flashSession->success("<pre>".print_r($_REQUEST,true)."</pre>");
					//$this->flashSession->success("<pre>".$txtSql."</pre>");
					$oib = (int)$_REQUEST['oib'];
					if($oib > 0){
						$oib_list = Nava::sqlToArray("select oib from users where oib = $oib");

						if($oib_list[1] > 0){
							$this->greska("oib vec postoji u bazi");
						}
					}
					
					

					$phone1 = $_REQUEST['phone1'];
					$phone2 = $_REQUEST['phone2'];
					$email = $_REQUEST['email'];
					
					
					if($phone1 > 0)
						$phone1_exsists = UsersContacts::findFirst("name = '" . $phone1 . "'");
					
					if($phone2 != '')				
						$phone2_exsists = UsersContacts::findFirst("name = '" . $phone2 . "'");
					
					if($email > '')
						$email_exsists = UsersContacts::findFirst("name = '" . $email . "'");
								
					if(!$phone1_exsists && $phone1 != $phone2){
						Nava::runSql("insert into n_users_contacts(name,type,user_id) values ('".$phone1."','phone', ".$entity_id.")");
					}
					
					if($phone2 != '' && !$phone2_exsists && $phone1 != $phone2){					
						Nava::runSql("insert into n_users_contacts(name,type,user_id) values ('".$phone2."','phone', ".$entity_id.")");
					}
					
					if(!$email_exsists ){
						Nava::runSql("insert into n_users_contacts(name,type,user_id) values ('".$email."','email', ".$entity_id.")");
					}
					
					
					
					
					


					
					// if($phone1 && $email > ''){
						// $contact_list = Nava::sqlToArray("select name from n_users_contacts where user_id = $entity_id");	
						// foreach($contact_list as $cl){		
							// if($cl[name] != $phone1){
								// 
							// }
							// if($cl[name] != $email){
								// Nava::runSql("insert into n_users_contacts(name,type,user_id) values ('".$email."','email', ".$entity_id.")");
							// }
						
							
						// }
						
						
						
					// }
					
					
					if ($_REQUEST['n_sales_rep_id']) Nava::runSql("update users set n_sales_rep_id = ".$_REQUEST['n_sales_rep_id']." where id = $entity_id");
					if ($_REQUEST['n_payment_type_id']) Nava::runSql("update users set n_payment_type_id = ".$_REQUEST['n_payment_type_id']." where id = $entity_id");
					//if ($_REQUEST['n_sub_customer']) Nava::runSql("update orders set n_sub_customer = ".$_REQUEST['n_sub_customer']." where id = $entity_id");
                    $_REQUEST['n_fiscal_location_id'] = $_REQUEST['fiscal_location_id'];
					if ($_REQUEST['n_fiscal_location_id']) Nava::runSql("update users set n_fiscal_location_id = ".$_REQUEST['n_fiscal_location_id']." where id = $entity_id");
                    $this->flashSession->success('<strong>User saved successfully!</strong>');
                    // Determine which save button was used
                    $save_action = $this->get_save_action();
                    if ('save' === $save_action) {
                        return $this->redirect_self();
                    } else {
                        $next_url = $this->get_next_url();
                        return $this->redirect_to($next_url);
                    }
                } else {
                    $this->view->setVar('errors', $saved);
                    $this->flashSession->error('<strong>Error!</strong> Fix all fields that failed to validate.');
                }
            } else {
                $this->flashSession->error($this->insufficient_privileges);
            }
        }

        // Check if impersonation link should be visible _ NIJE VIDLJIV
        $show_impersonate_link = false;
/*         if ($this->auth->logged_in(array('admin', 'support'))) {
            if ($entity && $entity->canBeImpersonated()) {
                $show_impersonate_link = true;
            }
        } */
      
        // Populates the hidden dropdown for the 'create new ad' modal
        $this->category_id_dropdown(
            'category_id',
            $filter_category_id,
            array(
                'disabled' => Categories::getCategoriesWithoutTransactionType()
            )
        );
		$this->category_id_dropdown(
            'display_ad_category_id',
            $filter_category_id,
            array(
                'disabled' => Categories::getCategoriesWithoutTransactionType()
            )
        );
        $this->view->setVar('show_impersonate_link', $show_impersonate_link);
//error_log("korisnici edit action 5");
        $this->country_dropdown($entity);
        $this->county_dropdown($entity);
        $this->setup_roles_checkboxes($entity);

        $this->view->setVar('user', $entity);
        
        $this->view->setVar('form_title_long', $form_title_long);
        $this->view->setVar('form_action', $form_action);
        $this->view->setVar('orders_statuses', Nava::arrToDict(OrdersStatuses::find()->toArray()));
        $this->view->setVar('fiscal_locations', Nava::arrToDict(FiscalLocations::find()->toArray()));
        
        $this->view->setVar('sales_rep', Users::findFirst($entity->n_sales_rep_id));
        $this->view->setVar('agency', Users::findFirst($entity->n_agency_id));
		$this->view->setVar('payment_types', Nava::arrToDict(Nava::sqlToArray("select * from n_payment_types where is_active = 1")));
		
		$this->view->setVar('x_user_id',$entity->id);
		$this->view->setVar('x_username',$entity->username);
		$_SESSION['x_user_id'] = $entity->id;
		$_SESSION['x_username'] = $entity->username;
		
		//$this->flashSession->success("<pre>".print_r(Nava::arrToDict(Nava::sqlToArray("select * from n_payment_types where is_active = 1")),true)."</pre>");
		
		
       
      

    }

    /**
     * Helper method to process user type filter dropdown
     *
     * @param string $default Default value
     * @return string|null
     */
    protected function processFilterUserRole($default = 'all') {
        $user_role_option = $this->processOptions(
            'user_role',
            array(
                'all'       => 'Display all users',
                'admin'     => 'Display admins only',
                'moderator' => 'Display moderators only',
                'content'   => 'Display content creators only',
                'support'   => 'Display support users only',
                'finance'   => 'Display finance users only'
            ),
            (string) $default,
            'string'
        );

        if (trim($user_role_option)) {
            return (string)$user_role_option;
        }

        return null;
    }

    protected function getPossibleOrderByOptions() {
        $order_bys = array(
            'id'          => 'ID',
            'created_at'  => 'Date created',
            'modified_at' => 'Date modified',
            'type'        => 'User type',
            'username'    => 'Username',
            'email'       => 'Email'
        );

        return $order_bys;
    }

    protected function county_dropdown($user) {
        if (isset($user->county_id)) {
            Tag::setDefault('county_id', $user->county_id);
        }
        $counties = Locations::findCounties($user->country_id);
        $county_dropdown = Tag::select(array(
            'county_id',
            $counties,
            'using'      => array('id', 'name'),
            'useEmpty'   => true,
            'emptyText'  => 'Odaberite županiju...',
            'emptyValue' => '',
            'class'      => 'form-control'
        ));
        $this->view->setVar('county_dropdown', $county_dropdown);
        $this->view->setVar('county_dropdown_hidden', count($counties) === 0);
    }

    protected function country_dropdown($user) {
        if (isset($user->country_id)) {
            Tag::setDefault('country_id', $user->country_id);
        }
        $country_dropdown = Tag::select(array(
            'country_id',
            Locations::findCountries(),
            'using'      => array('id', 'name'),
            'useEmpty'   => true,
            'emptyText'  => 'Odaberite državu...',
            'emptyValue' => '',
            'class'      => 'form-control'
        ));
        $this->view->setVar('country_dropdown', $country_dropdown);
    }

    /**
     * @param \Baseapp\Suva\Models\Users $user
     */
    protected function setup_roles_checkboxes($user) {
        $current_roles = $user->get_roles_joined();

        $has_dev_role = false;
        $selected_role_names = array();
        foreach ($current_roles as $current_role) {
            $selected_role_names[] = $current_role['name'];
            if ('developer' === $current_role['name']) {
                $has_dev_role = true;
            }
        }

        // Maintain posted roles in case of errors
        if (isset($_POST['roles'])) {
            $selected_role_names = $_POST['roles'];
        }

        $all_roles = Roles::find()->toArray();

        // Remove 'developer' role from the list of roles for those that shouldn't see it
        if (!$has_dev_role) {
            $all_roles = Utils::array_where($all_roles, function($k, $role){
                return ('developer' !== $role['name']);
            });
        }

        $all_roles_names = array();
        foreach ($all_roles as $role) {
            $all_roles_names[$role['name']] = $role['description'];
        }

        $this->view->setVar('selected_roles', $selected_role_names);
        $this->view->setVar('roles', $all_roles_names);
    }
    
    /**
     * banToggle Action
     */
    public function banToggleAction() {
        $params = $this->router->getParams();
        if ($user_id = (int) $params[0]) {
            if ($user = Users::findFirst(array('id=:user_id:', 'bind' => array('user_id' => $user_id)))) {
                if ($user->banToggle()) {
                    $this->flashSession->success('Ban status for user <strong>' . $user->username . '</strong> changed successfully.');
                } else {
                    $this->flashSession->error('Failed changing ban status for user <strong>'.$user->username.'</strong>.');
                }
            } else {
                $this->flashSession->error('User with ID: <strong>'.$user_id.'</strong> not found.');
            }
        }

        return $this->redirect_back();
    }

    /**
     * Switches the currently logged in admin to another user's account.
     *
     * First we check if the current user should be able to do it.
     * Then we check if the target user allows it.
     *
     * If everything checks out then we create some audit log records,
     * logout the current user, log in as the target user, and redirect to the site homepage.
     */
    public function impersonateAction($user_id = null) {
        if (null === $user_id) {
            $this->flashSession->error('Missing required parameter for user switch.');
            return $this->redirect_back();
        }

        // Check if currently logged-in user has 'admin' or 'support' role
        if (!$this->auth->logged_in(array('admin', 'support'))) {
            $this->flashSession->error('Insufficient privileges.');
            return $this->redirect_back();
        }

        // Check target user existence and roles
        /** @var Users $target_user */
        $target_user = Users::findFirst($user_id);
        if (!$target_user) {
            $this->flashSession->error('Invalid User specified.');
            return $this->redirect_back();
        }

        // Target user must have login role
        if (!$target_user->hasRole('login')) {
            $this->flashSession->error('Target user has no "login" role and cannot be switched to!');
            return $this->redirect_back();
        }

        // Target user must not have admin role (impersonating other admins sounds dangerous)
        if ($target_user->hasRole(array('admin', 'support'))) {
            $this->flashSession->error('Target user has "admin" or "support" role and cannot be switched to!');
            return $this->redirect_back();
        }

        // We're all good, proceed with user switching

        $current_user = $this->auth->get_user();
        $current_user_ident = $current_user->ident();
        $current_user_role = $current_user->hasRole('admin') ? 'Admin' : 'Support';

        // Logout the current user
        $this->saveAudit(sprintf($current_user_role . ' user switch: logging out %s', $current_user_ident));
        if ($this->hybridauth) {
            $this->hybridauth->logoutAllProviders();
        }
        $logged_out = $this->auth->logout();
        // Login as the target user
        if ($logged_out) {
            $this->auth->login_impersonate($target_user);
            $this->saveAudit(sprintf($current_user_role . ' user switch: logged in %s as %s.', $current_user_ident, $target_user->ident()));
        }

        // Redirect to homepage
        return $this->redirect_home();
    }

    public function fiscalAction(){
         $fiscal_location  = $_POST['fiscal_lok'];

        
         $this->disable_view();
         $this->response->setContentType('application/json', 'UTF-8');
         $response_array = array('status' => true);

        if (!$this->request->isAjax()) {
            $this->response->setStatusCode(400, 'Bad Request');
            $response_array['msg'] = 'Bad Request';
        } else {
            
                $user = Users::findFirst($this->auth->get_user()->id);

                $user->n_fiscal_location_id = $fiscal_location;

                if ( false == $user->save()) {
                        $response_array['status'] = false;
                        $response_array['msg'] = "Cannot be added";

                }



                if ($response_array['status'] == true){

                     $response_array['msg'] = 'Fiscal location '  .  $fiscal_location . ' added for user: ' . $user->first_name . " " . $user->last_name;

                }   

       }

        $this->response->setJsonContent($response_array);
        return $this->response;
    }
  
    public function category_id_dropdown($dropdown_field_name, $selected = null, $options = array()) {

        // Category tree is used so we don't have to call getPath for every node,
        // (and instead just get already queried data) + to populate the categories dropdown
        $model = new Categories();
        $tree = $this->getDI()->get('categoryTree');
        $selectables = $model->getSelectablesFromTreeArray($tree, '', $with_root = false, $depth = null, $breadcrumbs = true);

        // Prevent invalid choices from being set as default
        if (!array_key_exists($selected, $selectables)) {
            $selected = null;
        }

        // Preselected value, if there's a valid one
        if ($selected) {
            Tag::setDefault($dropdown_field_name, $selected);
        }

        // Process $selectables before using them in Tag::select() in order to
        // display a more useful dropdown (for when it's not being handled by selectpicker())
        if (isset($options['subtext']) && !$options['subtext']) {
            foreach ($selectables as $k => $data) {
                if (isset($data['attributes'])) {
                    if (isset($data['attributes']['data-subtext'])) {
                        $subtext = $data['attributes']['data-subtext'];
                        $selectables[$k]['text'] = $subtext . ' › ' . $selectables[$k]['text'];
                        unset($selectables[$k]['attributes']['data-subtext']);
                    }
                }
            }
            unset($options['subtext']);
        }

        // Default options
        $defaults = array(
            $dropdown_field_name,
            $selectables,
            'useEmpty'   => true,
            'emptyText'  => 'Choose category...',
            'emptyValue' => '',
            'class'      => 'form-control'
        );

        // Merge passed in $options
        $options = array_merge($defaults, $options);

        // Build the dropdown
        $category_id_dropdown = Tag::select($options);
        $this->view->setVar($dropdown_field_name . '_dropdown', $category_id_dropdown);

        // Set the entire tree as a view variable (for displaying an article's category path or similar)
        $this->view->setVar('tree', $tree);

        return $tree;
    }

}
