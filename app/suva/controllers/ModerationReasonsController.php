<?php

namespace Baseapp\Suva\Controllers;

class ModerationReasonsController extends IndexController{

    public function indexAction() {
			$this->tag->setTitle("Moderation Reasons");
			$this->n_query_index("ModerationReasons"); //ovo je definirano u BaseController
    }

		public function crudAction($pEntityId = null) {
			$this->n_crud_action("ModerationReasons", $pEntityId); //ovo je definirano u BaseController
			$this->view->pick('moderation-reasons/crud');
		}
}