<?php

namespace Baseapp\Suva\Controllers;

use Phalcon\DI;
use Phalcon\Dispatcher;
use Baseapp\Extension\Tag;
use Baseapp\Library\Tool;
use Baseapp\Library\Utils;
use Baseapp\Suva\Models\Roles;
use Baseapp\Suva\Models\Locations;
use Baseapp\Traits\CrudActions;
use Baseapp\Traits\CrudHelpers;
use Baseapp\Traits\ParametrizatorHelpers;
use Phalcon\Paginator\Adapter\QueryBuilder;

//glavna
use Baseapp\Suva\Models\OrdersItems; //igor
//foreign key klase
use Baseapp\Suva\Models\Categories;
use Baseapp\Suva\Models\Products;
use Baseapp\Suva\Models\Publications;
use Baseapp\Suva\Models\Users;
use Baseapp\Suva\Models\Orders;
use Baseapp\Suva\Models\OrdersStatuses;
use Baseapp\Suva\Models\Ads;


use Baseapp\Suva\Models\Discounts; //igor
use Baseapp\Suva\Models\N_Insertions;
use Baseapp\Suva\Models\N_Products;
use Baseapp\Suva\Library\Nava;
use Phalcon\Mvc\View\Engine\Volt;

/**
 * Suva Users Controller
 */
class OrdersItemsController extends IndexController {
    use ParametrizatorHelpers;
    use CrudActions;
    use CrudHelpers;
	
    public $crud_model_class = 'Baseapp\Suva\Models\OrdersItems';
    public $controller_folder = "orders-items";
    public $entity_title = "Order Items";

	
	public function crudAction($pEntityId = null) {
		//$this->printr($_REQUEST);
		$this->view->pick('orders-items/crud');
		$this->n_crud_action("OrdersItems", $pEntityId); //ovo je definirano u BaseController
	}
	
    public function add_common_crud_assets() {
        $this->assets->addJs('assets/vendor/jquery.mask.min.js');
		$this->assets->addJs('assets/suva/js/orders-items.js');
    }
	
	public function n_add_volt_assets($entity){
		$this->assets->addJs('assets/vendor/jquery.mask.min.js');
		$this->assets->addJs('assets/suva/js/orders-items.js');
		
 		//$this->view->setVar('entity_name', 'Orders Items');
		
		if ($entity->order_id > 0) {
			$order = Orders::findFirst($entity->order_id);
			$this->view->setVar('order', $order);
			$this->view->setVar('user', Users::findFirst($order->user_id));
			$this->view->setVar('order_status', OrdersStatuses::findFirst($order->n_status));
			if($order->n_sales_rep_id > 0 ){
				$this->view->setVar("sales_rep_name",Users::findFirst($order->n_sales_rep_id)->username);
				if($debug) $this->poruka($order->n_sales_rep_id);
			}
		}
		if ($entity->ad_id > 0) {
			$ad = Ads::findFirst($entity->ad_id);
			$this->view->setVar('ad', $ad);
			$this->view->setVar('user', Users::findFirst($ad->user_id));
		}
		if ($entity->n_publications_id > 0) $this->view->setVar('publication', Publications::findFirst($entity->n_publications_id));
		if ($entity->n_products_id > 0) $this->view->setVar('product', Products::findFirst($entity->n_products_id));
		if ($entity->n_category_id > 0) $this->view->setVar('category', Categories::findFirst($entity->n_category_id));
		
		$this->view->setVar('entity', $entity);
		$this->view->setVar('errors', null);
		
		$this->view->setVar('publications', Nava::arrToDict(Nava::sqlToArray("select * from n_publications where is_active = 1")));
		$this->view->setVar('categories', Nava::categoriesWithPathsDict());
		
// 		if ($entity->id > 0) $this->view->setVar('discounts', Nava::getDiscountsForOrderItem($entity->id));
		//if ($entity->id > 0) $this->view->setVar('discounts', Nava::getCheckedDiscountsForOrderItem($entity->id));
		
		
// 		if ($entity->id > 0) $this->view->setVar('issues', Nava::getInsertionsForOrderItem($entity->id));
		$issues = Nava::getCheckedInsertionsForOrderItem($entity->id);
		//error_log('issue_list_start');
       // //error_log($entity->id);
        //error_log(print_r($issues,true));
        //error_log('issue_list_end');
		
// 		//error_log(print_r($test,true));

		//proć kroz listu issues, prebrojit koliko ih ima od datuma prije današnjeg dana, i maknut ih iz array-a. Poslat poruku "odabrano je n objava prije današnjeg datuma. Klikom na Save bit će izbrisane. Za povratak kliknuti Cancel"
// 		$this->poruka($issues);
		$n = 0;
		$past_issues_arr = array();
		
		$last_issue_element = end($issues);
		$last_issue_explode = explode(" ", $last_issue_element['date_published']);
		$last_issue_date = $last_issue_explode[0];

		$next_issues_list = Nava::sqlToArray("select * from n_issues where date_published > '$last_issue_date'");
// 		//error_log(print_r($next_issues_list,true));
// 		//error_log(print_r($last_issue_element,true));

		foreach( $next_issues_list as $i => $issue){
			$issue_explode = explode(" ", $issue['date_published']);
// 			//error_log($issue_explode);
			$issue_exploded = strtotime($issue_explode[0]);
			//error_log($issue_exploded);
			
			if(date('D', $issue_exploded) == 'Mon') unset($next_issues_list[$i]);
			if(date('D', $issue_exploded) == 'Wen') unset($next_issues_list[$i]);
			if(date('D', $issue_exploded) == 'Sat') unset($next_issues_list[$i]);
			if(date('D', $issue_exploded) == 'Sun') unset($next_issues_list[$i]);
			if(date('D', $issue_exploded) == 'Tue') $next_issues_list[$i]['date_published'] = date('d.m.Y', $issue_exploded).' Uto';
			if(date('D', $issue_exploded) == 'Thu') $next_issues_list[$i]['date_published'] = date('d.m.Y', $issue_exploded).' Čet';
			if(date('D', $issue_exploded) == 'Fri') $next_issues_list[$i]['date_published'] = date('d.m.Y', $issue_exploded).' Pet';

			
// 			//error_log($issue_exploded);
			//error_log('issue list');
			
// 			//error_log(date('D', $issue_exploded));
		}
		//error_log(print_r($next_issues_list,true));
		$this->view->setVar('next_issues', $next_issues_list);
		
		foreach($issues as $i => $issue){
			
			//IGOR: nije preko datuma nego preko statusa
			//if($issue['date_published'] < date("Y-m-d H:i:s")){
			if($issue['status'] != 'prima')	{
				$n++;
				
				array_push($past_issues_arr,$issues[$i]);
// 				array_push($past_issues_arr, 13);
// 				$this->poruka("test");
				unset($issues[$i]);
				
			}
			
		}
		
		$this->view->setVar('past_issues', $past_issues_arr);
		
		
		if($n > 0 ) $this->poruka("Odabrano je ".$n." objave prije današnjeg datuma.Klikom na Save bit će izbrisane. Za povratak kliknuti Cancel");
		if ($entity->id > 0) $this->view->setVar('issues', $issues);

		if ($entity->n_publications_id > 0)
			$this->view->setVar('products', Nava::arrToDict(Nava::getProductsWithPricesForCategoryAndPublication($entity->n_category_id, $entity->n_publications_id )));

	}

    public function indexAction() {
        return $this->redirect_to('admin/fakture');
    }

	public function createAction( $pAdId = null, $pOrderId = null, $pUserId = null ) {
		/*POZIVANJE CREATE:
		- prvi put bez posta da se generira volt
		- prvo snimanje sa postom da se kreira item, nakon toga redirecta na edit
		*/
		
		//PROVJERA ISPRAVNOSTI POZIVA
		if ($pAdId && !$pOrderId && !$pUserId) { 				
			//1 - kreira se preko ad-a
		} elseif (!$pAdId && $pOrderId && !$pUserId ){
			//2. - kreira se preko ordera
		} elseif (!$pAdId && !$pOrderId && $pUserId ){
			//3. - kreira se preko usera
		} else $this->greska("Greška u parametrima poziva: pAdId = $pAdId, pOrderId = $pOrderId, pUserId = $pUserId");

		/*
		Funkcija se poziva na slijedeće načine:
		1. slanjem pAdId 
		2. Add Display Ad - šalje se pOrderId
		3. preko post-a nakon sšto se stisne save na već generiranoj formi
		
		kad se kreira display ad preko editora korisnika, forma šalje GET request (nije post), i šalje parametar display_ad_category_id
		*/
		
		if ($this->request->isPost()) {
			//pozvana je nakon snimanja, rezultat posta se zapisuje u bazu i redirecta se na edit
			//$this->poruka("IS POST");
			$entity = new $this->crud_model_class();
			$entity->n_set_default_values();
			$entity->n_update_model_with_post($this->request);
			
			//$entity->n_before_save($this->request);
			
			//ovo je greska koja se salje iz modela ordersitems iz funkcije before save
			//ako postoji pushup proizvod a ad nije moderiran
			$greska = $entity->n_before_save($this->request,$this->auth->get_user()->id);
			//ako je varijabla puna onda nemoj kreirat ako nije onda kreiraj
			if ($greska) { $this->greska($greska); return $this->redirect_self(); }

			$result = $entity->create();
			
			//if ($debug) //error_log("createAction 50");
			if ($result){
				//ako je uspješno snimljeno
				if ($debug) $this->poruka($entity->toArray());
				
				$greska = $entity->n_after_save($this->request);
				if ($greska) { $this->greska($greska); return; }
				
				$this->flashSession->success('<strong>Order Item Nr. '.$entity->id.' has been created.</strong> ');

				// Override next url to go to entity edit in save button case
				if ($this->get_save_action() === 'save') return $this->redirect_to('admin/'.$this->controller_folder.'/edit/'.$entity->id);
				else return $this->redirect_to($this->get_next_url());
			} else {
				//ako nije kreiran novi red u bazi
				foreach ($entity->getMessages() as $message)  $err_msg .= $message.", ";
				$this->flashSession->error('<strong>Error!</strong> Fix all fields that failed to validate.'.$err_msg);
				$this->view->setVar('errors', $result);
			}
		} else {
			//nije post, poziva se prvi put, kreira se novi entity da bi mogao raditi volt (ne snima ga se ovdje)
			$entity = new $this->crud_model_class();
			$entity->n_set_default_values();
			
			if ($pAdId) {
				$entity->ad_id = $pAdId;
				$entity->n_category_id = Ads::findFirst($entity->ad_id)->category_id;
			}
			if ($pOrderId)	$entity->order_id = $pOrderId;
			if ($pUserId)	$this->view->setVar('x_user_id', $pUserId); //za traženje ordera u statusu cart prilikom snimanja order itema
			
			$this->n_add_volt_assets($entity);

			$this->tag->setTitle('Create Order Item');
			$this->view->pick($this->controller_folder.'/edit'); // Use the same form/view as editAction
			$this->view->setVar('form_title_long','Add new Order Item');
			$this->view->setVar('form_action', 'add');
			$this->view->setVar('panel-heading', 'add');
		}
	}
	
    public function editAction($entity_id = null) {
		$model = $this->crud_model_class;
		$entity = $model::findFirst($entity_id);
 		// $this->printr($_REQUEST);
		// $this->poruka ("ENTITY");
		// $this->printr($entity->toArray());

        if (!$entity) {
            $this->flashSession->error("Entity not found [Model: $model, ID: $entity_id]");
            return $this->redirect_back();
		}		
 		//$this->printr($_REQUEST);

		if ($this->request->isPost()) {
			$entity->initialize_model_with_post($this->request);
			
			$error = $entity->n_before_save($this->request, $this->auth->get_user()->id);
			if ($error) {
				$this->greska($error);
				return $this->redirect_self();
				//return;
			}
			$saved = $entity->update();
			if ($saved) {
				$error = $entity->n_after_save($this->request);
				if ($error) return $this->greska($error);
				
//				$this->printr(OrdersItems::findFirst($entity->id)->toArray());
				
				$this->flashSession->success("<strong>Order Item No.".$entity->id." saved.</strong>");
				
				// $this->poruka ("ENTITY after saved");
				// $this->printr($entity->toArray());
				 
				// Determine which save button was used
				if ($this->get_save_action() === 'save') return $this->redirect_self();
				else  return $this->redirect_to($this->get_next_url());
			} else {
				//NIJE SNIMLJENO
				$err_msg = "";
				foreach ($entity->getMessages() as $message) $err_msg .= $message . "\n\r";
				$this->flashSession->error('<strong>Error!</strong> Fix all fields that failed to validate.' . $err_msg);
			}
		} else {
			//ako nije bilo posta nema snimanja, samo se pokreće volt
			$this->view->setVar('ad_taker_username', $ad_taker_username);
			$this->n_add_volt_assets($entity);
			$this->tag->setTitle('Edit Order Item');
			$this->view->setVar('form_title_long', "Edit order item");
			$this->view->setVar('form_action', 'edit');
			
			// $this->poruka ("prije forme");
			// $this->printr($entity->toArray());
		}
		$this->view->pick('orders-items/crud');
    }
	
	public function viewAction($entity_id = null) {
		$model = $this->crud_model_class;
		$entity = $model::findFirst($entity_id);

        if (!$entity) {
            $this->flashSession->error("Entity not found [Model: $model, ID: $entity_id]");
            return $this->redirect_back();
		}	
		$this->n_add_volt_assets($entity);
		$this->view->pick($this->controller_folder.'/edit');
		$this->tag->setTitle('Edit Order Item');
		$this->view->setVar('form_title_long', "Edit order item");
		$this->view->setVar('form_action', 'view');
    }
	  
    // public function moveToCartAction($entity_id = null) {

		// //polje id-jeva koji se prebacuju u cart
		// $oi_ids = array();
		
		// //dodaj u popis id_jeva parametar funkcije ako je poslan
		// if ($entity_id > 0)	array_push ( $oi_ids, (int)$entity_id );
		
		// //dodaj u popis id-jeva odabrane id-jeve ako su poslani u polju _orders_items_ids
		// if ($this->request->isPost())
			// if ($this->request->hasPost('_orders_items_ids'))
				// foreach( $this->request->getPost('_orders_items_ids') as $oi_id )
					// array_push( $oi_ids, $oi_id );
		
		// foreach ($oi_ids as $oi_id){
			// $oi = \Baseapp\Suva\Models\OrdersItems::findFirst( $oi_id );
			// if ($oi){
				// $cart = $oi->User()->getCart();
				// $oi->order_id = $cart->id;
				// $oi->save();
				// $cart->updateTotal();
				// $order->updateTotal();
				// $this->poruka ("Order Item $oi_id has been moved to cart. New Total for Order No. $order->id is $order->total .");
			// }
		// }
		// return $this->redirect_back();
	// } 

	
    public function copyToCartAction($entity_id = null) {

		//polje id-jeva koji se prebacuju u cart
		$oi_ids = array();
		
		//dodaj u popis id_jeva parametar funkcije ako je poslan
		if ($entity_id > 0)	array_push ( $oi_ids, (int)$entity_id );
		
		//dodaj u popis id-jeva odabrane id-jeve ako su poslani u polju _orders_items_ids
		if ($this->request->isPost())
			if ($this->request->hasPost('_orders_items_ids'))
				foreach( $this->request->getPost('_orders_items_ids') as $oi_id )
					array_push( $oi_ids, $oi_id );
		
		foreach ($oi_ids as $oi_id){
			$oi = \Baseapp\Suva\Models\OrdersItems::findFirst( $oi_id );
			if ($oi){
				
				$cart = $oi->User()->getCart();
				$newOi = $oi->makeCopy();
				$newOi->order_id = $cart_id;
				$newOi->save();
				$this->poruka ("Order Item $oi_id has been copied to cart.");
			}
		}
		return $this->redirect_back();
	} 

	
    public function deleteAction($entity_id = null) {

			$entity_id = (int) $entity_id;

			$model = $this->crud_model_class;
			/* @var $entity \Baseapp\Suva\Models\Users */
			$entity = $model::findFirst($entity_id);

			if (!$entity) {
					$this->flashSession->error(sprintf('Entity not found [Model: %s, ID: %s]', $this->crud_model_class, $entity_id));
					return $this->redirect_back();
			}

			$order = Orders::findFirst($entity->order_id);
			
			// IZMIJENI ORDER_ID NA ORDER ITEMU
			Nava::sqlToArray("delete from orders_items where id = ".$entity->id);
			return $this->redirect_back();	
	} 
	
}

