<?php

namespace Baseapp\Suva\Controllers;

class PublicationsCategoriesMappingsController extends IndexController{

    public function indexAction( $pPublicationsId = null, $pAdId = null ) {
		//$this->printr($_REQUEST , "PublicationsCategoriesMappingsController:indexAction:REQUEST");
		//$this->printr($_SESSION['_context'], "PublicationsCategoriesMappingsController:indexAction:CONTEXT");
		
		$this->tag->setTitle("Cat. Mappings for Publications");
		
		if ($pPublicationsId > 0){
			$publication = \Baseapp\Suva\Models\Publications::findFirst( $pPublicationsId );
			if ($publication){
				$this->tag->setTitle("CMAP for $publication->name");
				
				//Postavljanje defaultnog filtera
				if (!array_key_exists('default_filter', $_SESSION['_context'])) 
					$_SESSION['_context']['default_filter'] = array();
				$_SESSION['_context']['default_filter']['publication_id'] = $publication->id;
			}
			else {
				$this->sysMessage ("PublicationsCategoriesMappingsController::indexAction - Publication $publicationId not found.");
				return $this->redirect_back();
			}
		}
		elseif ($pAdId > 0){
			$pcmIds = \Baseapp\Suva\Library\libPCM::getIds(
				array (
					"modelName" => "PublicationsCategoriesMappings"
					
					,"filters" => array (
						array(
							"modelName" => "Ads"
							,"whereField" => 'id'
							,"whereValue" => $pAdId
						)
					)
				)
			);
			if (!array_key_exists('default_filter', $_SESSION['_context'])) 
					$_SESSION['_context']['default_filter'] = array();
				
			if ( count($pcmIds) > 0 ){
				$_SESSION['_context']['default_filter']['id'] = $pcmIds;
			}
			else{
				$_SESSION['_context']['default_filter']['id'] = 0;
			} 
				
			//$this->printr($pcmIds, "PCM IDS");
		}
		
		$_SESSION['_context']['step_rec'] = 20; //broj prikazanih elemenata na listi

		$this->n_query_index("PublicationsCategoriesMappings"); //ovo je definirano u BaseController
    }
	
	public function crudAction($pEntityId = null) {
		//$this->printr($_REQUEST);
		//$this->printr($_SESSION['_context'], "PublicationsCategoriesMappingsController:crudAction:CONTEXT");
		if (array_key_exists('_default_publication_id',$_SESSION['_context']))
			$defaults = array('publication_id' => (int)$_SESSION['_context']['_default_publication_id'] );
		
		$this->view->pick('publications-categories-mappings/crud');
		$this->n_crud_action("PublicationsCategoriesMappings", $pEntityId, false, $defaults ); //ovo je definirano u BaseController
	}

}