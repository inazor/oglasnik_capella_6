<?php

namespace Baseapp\Suva\Controllers;


class UsersContactsController extends IndexController{

    public function indexAction( $pUserId = null, $pCommand = null, $pParameter = null ) {
		//$this->poruka ("userscontacts");
		
		if($pCommand == 'delete' and $pParameter > null){
			//error_log('delete1');
			$ucs = \Baseapp\Suva\Models\UsersContacts::findFirst("id = $pParameter");
			$ucs->delete();
		}
		//error_log ("UsersContactsController:indexAction 1");
		// $this->printr($_SESSION['_context']);
		if (! $pUserId > 0){
			$this->greska("User Id not supplied for list of contacts.");
			return;
		}
		//error_log("UsersContactsController:indexAction 1 pUserid;$pUserid");
		//$_SESSION['_context']['step_rec'] = 20;
		$this->tag->setTitle("UsersContacts");
		$ucs = \Baseapp\Suva\Models\UsersContacts::find(array("user_id = $pUserId ", "order" => " id desc "));
		$ids = array();
		foreach ($ucs as $uc)
			$ids[$uc->id] = $uc->id;
		$this->view->setVar('ids', $ids);
		$this->view->setVar('user_id', $pUserId);
		
		// $this->view->setVar('user_id', $pUserId);
		
		//$this->n_query_index("Discounts"); //ovo je definirano u BaseController
    }

	public function crudAction($pUserId = null, $pEntityId = null) {
		//$this->printr($_REQUEST);
		//error_log ("BaseSuvaController: crudAction, User ID, $pUserid, Entity ID, $pEntityId");
		if ( ($pUserId == 0 || $pUserId == null ) && !$pEntityId){
			$this->greska ("User Id  and entity not supplied");
		}
		
		if ($pUserId)
			$defaultValues = array(
				"user_id" => $pUserId,
				"sub_type" => null,
			);
		else $defaultValues = null;
		

		// $this->view->pick('ads2/subContact');
		$this->n_crud_action("UsersContacts", $pEntityId, false, $defaultValues); //ovo je definirano u BaseController
		
		
	}
	
	public function deleteAction($pEntityId = null) {
		//$this->printr($_REQUEST);
		$ucs = \Baseapp\Suva\Models\UsersContacts::findFirst("id = $pEntityId");
		//error_log('UserContacts::deleteAction 1');
		$user_id = $ucs->user_id;
		
		$ucs->delete();
		//error_log('UserContacts::deleteAction 2');
		//error_log("IZBRISANO");
		
		$this->poruka("users contact no. e has been deleted");
// 		$this->dispatcher->forward(
//             array(
//                 "controller" => "users-contacts",
//                 "action"     => "index",
//                 "params"     => array( $user_id )
//             )
//         );
// 		$this->view->pick("users-contacts/index/$user_id");
		$this->view->pick("users-contacts/index");

        // Pick "views-dir/books/list" as view to render
		$this->view->pick(array(1 => $user_id));
//         $this->view->pick(array($user_id));
// 		$rec_ids = \Baseapp\Suva\Models\UsersContacts::find("user_id = $user_id");
// 		$ids = array();
// 		foreach($rec_ids as $rec_id){
// 			array_push($ids,$rec_id->id);
// 		}
// 		$this->view->setVar('ids', $ids);
		//error_log('UserContacts::deleteAction 3');
		// error_log ("BaseSuvaController: crudAction, User ID, $pUserid, Entity ID, $pEntityId");
		// if ( ($pUserId == 0 || $pUserId == null ) && !$pEntityId){
			// $this->greska ("User Id  and entity not supplied");
		// }
		
		// if ($pUserId)
			// $defaultValues = array(
				// "user_id" => $pUserId,
				// "sub_type" => null,
			// );
		// else $defaultValues = null;
		
		
		//testirati?
		//$this->view->pick('users-contacts/index/'.$ucs->user_id);
		// testirat i ovo return $this->response->redirect($_SERVER['HTTP_REFERER']);
		// $this->n_crud_action("UsersContacts", $pEntityId, false, $defaultValues); //ovo je definirano u BaseController
		
		
	}
	
	

	
	
}
