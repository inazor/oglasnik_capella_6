<?php

namespace Baseapp\Suva\Library;

/**
 * SimpleXMLReader is a wrapper around XMLReader providing fast, simple and hopefully
 * memory efficient SAX-based reading (and simple XPath querying) of huge XML files.
 */
class SimpleXMLReader extends \XMLReader
{
    /**
     * Callbacks
     *
     * @var array
     */
    protected $callbacks = array();

    /**
     * Depth
     *
     * @var int
     */
    protected $current_depth = 0;

    /**
     * Previous depth
     *
     * @var int
     */
    protected $prev_depth = 0;

    /**
     * Stack of parsed nodes
     *
     * @var array
     */
    protected $parsed_nodes = array();

    /**
     * Stack of node types
     *
     * @var array
     */
    protected $node_types = array();

    /**
     * Stack of node positions
     *
     * @var array
     */
    protected $node_counter = array();

    /**
     * Add a callback for a node
     *
     * @param string $xpath
     * @param Callable $callback
     * @param $type
     *
     * @return $this
     * @throws \Exception
     */
    public function addCallback($xpath, $callback, $type = \XMLREADER::ELEMENT)
    {
        if (isset($this->callbacks[$type][$xpath])) {
            throw new \Exception("Callback `$xpath:$type` already exists");
        }
        if (!is_callable($callback)) {
            throw new \Exception("Callback `$xpath:$type` is not callable");
        }
        $this->callbacks[$type][$xpath] = $callback;

        return $this;
    }

    /**
     * Remove a node's callback
     *
     * @param string $xpath
     * @param int $type
     *
     * @return $this
     * @throws \Exception
     */
    public function removeCallback($xpath, $type = \XMLREADER::ELEMENT)
    {
        if (!isset($this->callbacks[$type][$xpath])) {
            throw new \Exception("Unspecified callback `$xpath:$type`");
        }
        unset($this->callbacks[$type][$xpath]);

        return $this;
    }

    /**
     * Moves the cursor to the next node in the document
     *
     * @link http://php.net/manual/en/xmlreader.read.php
     * @return bool True on success, false on failure.
     * @throws \Exception
     */
    public function read()
    {
        $read = parent::read();

        if ($this->depth < $this->prev_depth) {
            if (!isset($this->parsed_nodes[$this->depth])) {
                throw new \Exception('Invalid xml: missing items in SimpleXMLReader::$parsed_nodes');
            }
            if (!isset($this->node_counter[$this->depth])) {
                throw new \Exception('Invalid xml: missing items in SimpleXMLReader::\$node_counter');
            }
            if (!isset($this->node_types[$this->depth])) {
                throw new \Exception('Invalid xml: missing items in SimpleXMLReader::\$node_types');
            }
            $this->parsed_nodes = array_slice($this->parsed_nodes, 0, $this->depth + 1, true);
            $this->node_counter = array_slice($this->node_counter, 0, $this->depth + 1, true);
            $this->node_types   = array_slice($this->node_types, 0, $this->depth + 1, true);
        }

        if (isset($this->parsed_nodes[$this->depth]) &&
            $this->localName == $this->parsed_nodes[$this->depth] &&
            $this->nodeType == $this->node_types[$this->depth]
        ) {

            $this->node_counter[$this->depth] = $this->node_counter[$this->depth] + 1;
        } else {
            $this->parsed_nodes[$this->depth] = $this->localName;
            $this->node_types[$this->depth]   = $this->nodeType;
            $this->node_counter[$this->depth] = 1;
        }
        $this->prev_depth = $this->depth;

        return $read;
    }

    /**
     * Returns current node's xpath
     *
     * @param bool $counter
     *
     * @return string
     * @throws \Exception
     */
    public function currentXpath($counter = false)
    {
        if (count($this->node_counter) != count($this->parsed_nodes) && count($this->node_counter) != count($this->node_types)) {
            throw new \Exception('Empty reader');
        }

        $result = '';
        foreach ($this->parsed_nodes as $depth => $name) {
            switch ($this->node_types[$depth]) {
                case self::ELEMENT:
                    $result .= '/' . $name;
                    if ($counter) {
                        $result .= '[' . $this->node_counter[$depth] . ']';
                    }
                    break;

                case self::TEXT:
                case self::CDATA:
                    $result .= '/text()';
                    break;

                case self::COMMENT:
                    $result .= '/comment()';
                    break;

                case self::ATTRIBUTE:
                    $result .= "[@{$name}]";
                    break;
            }
        }

        return $result;
    }

    /**
     * Run the parser invoking added callbacks
     *
     * @throws \Exception
     */
    public function parse()
    {
        if (empty($this->callbacks)) {
            throw new \Exception('No callbacks added.');
        }

        $continue = true;
        while ($continue && $this->read()) {
            if (!isset($this->callbacks[$this->nodeType])) {
                continue;
            }
            if (isset($this->callbacks[$this->nodeType][$this->name])) {
                $continue = call_user_func($this->callbacks[$this->nodeType][$this->name], $this);
            } else {
                $xpath = $this->currentXpath(false); // without node counter
                if (isset($this->callbacks[$this->nodeType][$xpath])) {
                    $continue = call_user_func($this->callbacks[$this->nodeType][$xpath], $this);
                } else {
                    $xpath = $this->currentXpath(true); // with node counter
                    if (isset($this->callbacks[$this->nodeType][$xpath])) {
                        $continue = call_user_func($this->callbacks[$this->nodeType][$xpath], $this);
                    }
                }
            }
        }
    }

    /**
     * Run XPath query on current node
     *
     * @param string $path
     * @param string $version
     * @param string $encoding
     * @param string $className
     *
     * @return array(SimpleXMLElement)
     */
    public function expandXpath($path, $version = '1.0', $encoding = 'UTF-8', $className = null)
    {
        return $this->expandSimpleXml($version, $encoding, $className)->xpath($path);
    }

    /**
     * Expands the current node into a string
     *
     * @param string $version
     * @param string $encoding
     * @param null $className
     *
     * @return mixed
     */
    public function expandString($version = "1.0", $encoding = "UTF-8", $className = null)
    {
        return $this->expandSimpleXml($version, $encoding, $className)->asXML();
    }

    /**
     * Expands the current node into a \SimpleXMLElement
     *
     * @param string $version
     * @param string $encoding
     * @param string $className
     *
     * @return \SimpleXMLElement
     */
    public function expandSimpleXml($version = '1.0', $encoding = 'UTF-8', $className = null)
    {
        $element  = $this->expand();
        $document = new \DomDocument($version, $encoding);
        if ($element instanceof \DOMCharacterData) {
            $name = array_splice($this->parsed_nodes, -2, 1);
            $name = (isset($name[0]) && $name[0] ? $name[0] : 'root');
            $node = $document->createElement($name);
            $node->appendChild($element);
            $element = $node;
        }
        $node = $document->importNode($element, true);
        $document->appendChild($node);

        return simplexml_import_dom($node, $className);
    }

    /**
     * Expands the current node into a \DomDocument
     *
     * @param string $version
     * @param string $encoding
     *
     * @return \DomDocument
     */
    public function expandDomDocument($version = '1.0', $encoding = 'UTF-8')
    {
        $element  = $this->expand();
        $document = new \DOMDocument($version, $encoding);
        if ($element instanceof \DOMCharacterData) {
            $name = array_splice($this->parsed_nodes, -2, 1);
            $name = (isset($name[0]) && $name[0] ? $name[0] :' "root"');
            $node = $document->createElement($name);
            $node->appendChild($element);
            $element = $node;
        }
        $node = $document->importNode($element, true);
        $document->appendChild($node);

        return $document;
    }
}
