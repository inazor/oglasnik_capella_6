<?php

namespace Baseapp\Suva\Library;



use Baseapp\Suva\Library\Nava;
use Baseapp\Suva\Library\libAppSetting;
use \Phalcon\Logger\Adapter\File as FileAdapter;


 
class libAVUS {

	
	const ISDEBUG = null;
	
	
	
	
	//syncanje svih oglasa koji u Avusu imaju još insertiona
	public static function syncAvusAdsWithRemainingInsertions(){
		
		$old_error_log = ini_get('error_log');
		ini_set('error_log',ROOT_PATH."/logs/syncAvusAds.log");
		//izlaz ako se već izvršava obrada		
		$isProccessRunning = libAppSetting::get('avus_process_running');
		if ($isProccessRunning == 1 ){
			Nava::greska("Obrada se već izvršava, ponovo pokrenite obradu kasnije. $isProccessRunning");
			//return;
		}
		libAppSetting::set('avus_process_running', true);
		
		//BORNA - loganje u poseban error log
		$log_path = ROOT_PATH."/logs/syncAvusAds.log";
		$logger = new FileAdapter($log_path);
		
		
		ini_set('max_execution_time', 150000);
		ini_set ('request_terminate_timeout', 150000);
		if (self::ISDEBUG) error_log("syncAvusAdsWithRemainingInsertions");
		Nava::statusMessage ("Pokrenuta je sinhronizacija oglasa iz AVUSA");
		Nava::poruka("Pokrenuta je sinhronizacija oglasa iz AVUSA");
		
		{ //dohvat iz avusa
			Nava::statusMessage ("Dohvaćanje podataka iz AVUSa");
			//popis insertiona po oglasima iz avusa se uspoređuje s popisom insertiona po oglasima iz SUVE i traže se razlike
			$lastDate = date("Y-m-d");
			$txtSql = "
						select 					
					o.ID 
					,( o.CREATEDINSERTIONS - o.ISSUEDINSERTIONS - o.BLOCKEDINSERTIONS) as RemainingInsertions
					, o.ADTEXT
					, o.CUSTOMERID
					, o.CLASSID
					, o.CREATEDINSERTIONS
					, o.ISSUEDINSERTIONS
					, o.BLOCKEDINSERTIONS
					, o.ADLAYOUTID
					, o.ACCOUNTEDINSERTIONS
					, o.TOTALPRICEPUBLISHED
					,c.number as customer_number
					,cid.taxnumber as customer_taxnumber
					,a.first_name
					,a.last_name
					,a.street
					,a.postcode
					,a.city
					,a.countryabr
					,a.contactperson
					,a.email
					,a.telareacode
					,a.telno

				from 
					g_orders o
					join customers c on (o.CUSTOMERID = c.ID)
						join g_customerinvoicedata cid on (c.id = cid.customerid)
						left join addressee a on (c.addresseeid = a.adressee_id)
				where 1=1
					and o.LASTOBJECTID in (2,4)
					and o.LASTDATE >= '$lastDate'	
					-- and o.ID = 13413843
				order by o.ID 				
			";
			 
			$rsAvus = self::sqlToArray ( $txtSql );  //lista oglasa iz AVUSA s ID oglasa i brojem preostalih insertiona

			
			if (! is_array($rsAvus)) {
				Nava::greska("Query $txtSql nije vratio rezultate");
				if (self::ISDEBUG) error_log ("Query $txtSql nije vratio rezultate");
				return;
			}
			
			//micanje oglasa koji nisu plaćeni a trebali bi biti
			foreach($rsAvus as $index => $row){
				if ( $row['ACCOUNTEDINSERTIONS'] == 0 && $row['TOTALPRICEPUBLISHED'] > 0 ) {
					unset ($rsAvus[$index] );
				}
			}
			
			$countRsAvus = 	count($rsAvus);
			if (self::ISDEBUG) error_log ("U AVUSU je pronađeno $countRsAvus oglasa s objavama u budućnosti");
			Nava::poruka("U AVUSU je pronađeno $countRsAvus oglasa s objavama u budućnosti");
			
			
			foreach ($rsAvus as $rec)
				$maxAvusId = $rec['ID'];	
			Nava::poruka("Zadnji uneseni oglas u AVUS je $maxAvusId." );
		
		}
		
		{// updateanje online oglasa koji su već importirani iz AVUS-a obradom
		// $txtSql = "
			// update ads set n_avus_order_id = null where n_source <>'avus';
		// ";
		// Nava::runSql($txtSql);

		//pokrece se samo jedan put
		// $txtSql = "

			// update ads a
				// join ads_additional_data ai on (a.id = ai.ad_id)
			// set 
				// a.n_avus_order_id = ai.avus_id,
				// a.n_source = 'avus'
			// where
				// a.n_source in ('frontend','backend','n/a')
				// and a.n_avus_order_id is null;
		// ";
			
		Nava::statusMessage("Updateanje online oglasa koji su već importirani iz AVUS-a obradom");
		Nava::poruka("Updateanje online oglasa koji su već importirani iz AVUS-a obradom");
		// Nava::runSql ($txtSql);
		}
		
		
		{ //DOHVAT IZ SUVE
		
		$sqlWhere = "0";
			foreach($rsAvus as $row)
				$sqlWhere .= ",".$row['ID'];
		
			Nava::statusMessage ("Dohvaćanje podataka iz SUVE");
			
			$txtSql = "
				select 
					ins.RemainingInsertions
					,a.n_avus_order_id
					,a.n_avus_class_id
					,a.n_avus_customer_number
					,a.n_dont_sync_with_avus
					,a.n_avus_adlayout_id
					,a.n_source
					,cm. categoryMappingId
					,p.productId
					,u.userId
					,u.userFirstName
					,u.userLastName
					,u.userOib
					,u.userAddress
					,u.userCity
					,u.userZip
					,u.userPhone
					,u.userEmail
					,u.userSource
					,u.userCustomerNumber
					,a.id
					,cm.countCategories
					,u.countUsers
					,p.countProducts
					,aa.countSuvaAds
				from 
	
					ads a 
						left join 
							(
								select
									avus_class_id
									,count(*) as countCategories
									,max(id) as categoryMappingId
								from 
									n_categories_mappings
								group by 
									avus_class_id
								) cm on (a.n_avus_class_id = cm.avus_class_id )
						left join (
								select
									avus_adlayout_id
									,count(*) as countProducts
									,max(id) as productId
								from 
									n_products
								group by 
									avus_adlayout_id
								) p on (a.n_avus_adlayout_id = p.avus_adlayout_id )
						left join (
								select
									n_avus_customer_id
									,count(*) as countUsers
									,max(id) as userId
									,max(first_name) as userFirstName
									,max(last_name) as userLastName
									,max(oib) as userOib
									,max(email) as userEmail
									,max(address) as userAddress
									,max(city) as userCity
									,max(zip_code) as userZip
									,max(phone1) as userPhone
									,max(n_source) as userSource
									,max(n_avus_customer_number) as userCustomerNumber
								from 
									users
								where
									n_source = 'avus'
								group by 
									n_avus_customer_id
								) u on ( a.n_avus_customer_number = u.n_avus_customer_id )
						left join (
							select
								n_avus_order_id,
								count(*) as countSuvaAds
							from 
								ads
							group by 
								n_avus_order_id
								) aa on (a.n_avus_order_id = aa.n_avus_order_id )
						left join (
							select
								count(*) as RemainingInsertions
								,in2.ad_id
							from 
								n_insertions_avus in2
								join n_issues iss on (in2.issue_id = iss.id and	iss.date_published > now() )
							group by 
								ad_id
								) ins on (ins.ad_id = a.id )
	
				where 1=1
					/* and a.n_source = 'avus'  */
					and a.n_avus_order_id in ( $sqlWhere )
					

				group by 
					ins.RemainingInsertions
					,a.n_avus_order_id
					,a.n_avus_class_id
					,a.n_avus_customer_number
					,a.n_avus_adlayout_id
					,a.n_dont_sync_with_avus
					,cm. categoryMappingId
					,p.productId
					,u.countUsers
					,u.userId
					,u.userFirstName
					,u.userLastName
					,u.userOib
					,u.userAddress
					,u.userCity
					,u.userZip
					,u.userPhone
					,u.userSource
					,a.id
					,cm.countCategories
					,p.countProducts
					,aa.countSuvaAds
					";

			//error_log ($txtSql);
			$rsSuva = Nava::sqlToArray( $txtSql );  //lista oglasa iz SUVE s brojem AVUS oglasa i brojem preostalih insertiona
			 
			//return;
			//brisanje insertiona na suvinim oglasima kojih nema na rsavus
			Nava::statusMessage("Brisanje budućih oglasa iz Suve kojih nema u Avusu");
			Nava::poruka("Brisanje budućih oglasa iz Suve kojih nema u Avusu");
			
			$txtSql = "
				delete from n_insertions_avus
				where id in (
					select * from (

					select
	
						in2.id
					from 
						n_insertions_avus in2
						join n_issues iss on (in2.issue_id = iss.id and	iss.date_published > now() )
					
						join ads a on (in2.ad_id = a.id)
					where a.n_avus_order_id not in ( $sqlWhere )
						
					) p1
				)
			
			";
			//error_log($txtSql);
			Nava::runSql ($txtSql);
			
			//return;
			
			
			if (self::ISDEBUG) Nava::sysMessage("U SUVI je pronađeno ".count($rsSuva)." oglasa s preostalim insertionima");
			Nava::poruka("U SUVI je pronađeno ".count($rsSuva)." oglasa s preostalim insertionima");
			
			
			Nava::statusMessage("Getting list of imported users in SUVA");
			$rs = Nava::sqlToArray("select max(id) as id, n_avus_customer_id from users where n_source = 'avus' group by n_avus_customer_id ");
			$dictSuvaUsers = Array();
			foreach ($rs as $row){
				$dictSuvaUsers[ $row['n_avus_customer_id']  ] = $row['id'];
			}
			//error_log(print_r($dictSuvaUsers, true));

			Nava::statusMessage("Getting list of categories in SUVA");
			$rs = Nava::sqlToArray("select max(id) as id, avus_class_id from n_categories_mappings where avus_class_id is not null group by avus_class_id ");
			$dictSuvaCategories = Array();
			foreach ($rs as $row){
				$dictSuvaCategories[ $row['avus_class_id']  ] = $row['id'];
			}
			//error_log(print_r($dictSuvaCategories, true));
			
			Nava::statusMessage("Getting list of products in SUVA");
			$rs = Nava::sqlToArray("select max(id) as id, avus_adlayout_id from n_products where avus_adlayout_id is not null group by avus_adlayout_id ");
			$dictSuvaProducts = Array();
			foreach ($rs as $row){
				$dictSuvaProducts[ $row['avus_adlayout_id']  ] = $row['id'];
			}
			//error_log(print_r($dictSuvaProducts, true));
			
			Nava::statusMessage ("Obrada podataka iz SUVE");
			//kreirati Dictionary za pretragu
			$dictSuva = Array();
			foreach ($rsSuva as $recSuva)
				$dictSuva[ $recSuva['n_avus_order_id'] ] = $recSuva;

			$noImportedAds = 0;
			$noUpdatedInsertions = 0;
			$noUpdatedUsers = 0;
			$noUpdatedCategories = 0;
			$noUpdatedProducts = 0;
			$noAvusDataErrors = 0;

		}
		
		

		
		$rsSuvaFutureIssues = \Baseapp\Suva\Models\Issues::find(array(
			" publications_id = 2 and date_published > now()"
			, 'order' => 'date_published asc, id'
			, 'limit' => $pNoInsertions
			));
		$suvaInsertionsAvailable =  $rsSuvaFutureIssues->count();
		
		//uspoređivati podatke iz Avusa s podacima iz Suve
		$i = 0;
		foreach ( $rsAvus as $i=>$recAvus ){
			$i++;
			if(isset($errLogMsg))
				Nava::sysMessage($errLogMsg);
			if(isset($errLogErr))
				Nava::sysError($errLogErr);
			
			$errLogMsg = "libAVUS: processing avus ad $avusId ( $i / $countRsAvus ) ";
			$errLogErr = "libAVUS: processing avus ad $avusId ( $i / $countRsAvus ) ";
			Nava::statusMessage ("Processing $i of $countRsAvus ");
			
			//pomoćne varijable - iz baze
			
			//pomoćne varijable - iz baze - AVUS
			$avusId = $recAvus['ID']; 
			$avusRemaining = $recAvus['RemainingInsertions'];
			$avusUser = $recAvus['CUSTOMERID'];
			$avusUserId = $recAvus['CUSTOMERID'];
			$avusUserFirstName = self::naToNull_upper(self::avusCharsetToUtf8( $recAvus['first_name']) );
			$avusUserLastName = self::naToNull_upper(self::avusCharsetToUtf8( $recAvus['last_name']) );
			$avusUserOib = str_replace('*', '',  str_replace('.', '', (string)$recAvus['customer_taxnumber'] ) );
			$avusUserPhone = self::avusCharsetToUtf8( ($recAvus ['telareacode'] ? $recAvus['telareacode']."/" : "").$recAvus['telno']);
			$avusCustomerNumber = self::avusCharsetToUtf8( (string)$recAvus['customer_number'] );
			$avusUserAddress = self::naToNull_upper(self::avusCharsetToUtf8( $recAvus['street']) );
			$avusUserCity = self::naToNull_upper(self::avusCharsetToUtf8( $recAvus['city']) );
			$avusUserZip = self::naToNull_upper(self::avusCharsetToUtf8( $recAvus['postcode']) );
			$avusUserEmail = self::naToNull_upper(self::avusCharsetToUtf8( $recAvus['email']) );
			
			//5.6.2017 email je obavezno polje prema tome generiramo novi email ako ne postoji u avusu
			$avusUserEmail = $avusUserEmail > '' ? $avusUserEmail : 'avus_user_'.$avusUserId.'@oglasnik.hr';
			
			$avusIsPaidAd = $recAvus['ACCOUNTEDINSERTIONS'] > 0 ? true : false;
			
			$avusProduct = $recAvus['ADLAYOUTID'];
			$avusCategory = $recAvus['CLASSID'];

			//pomoćne varijable - iz baze - SUVA
			$keyExists = array_key_exists($avusId, $dictSuva);
			if (!$keyExists)Nava::poruka("Za avus oglas $avusId ne postoji oglas u suvi");
						
			$recSuva = $keyExists ?  $dictSuva[ $avusId ] : null;
			
			$suvaId = $keyExists ? $recSuva['id'] : null; 
			$suvaRemaining = $keyExists ? $recSuva['RemainingInsertions'] : null ;
			$suvaUser = $keyExists ? $recSuva['n_avus_customer_number'] : null;
			$suvaSource = $keyExists ? $recSuva['n_source'] : null;


			$suvaUserId = $keyExists ? $recSuva['userId'] : null;
			$suvaUserFirstName = $keyExists ? self::naToNull_upper( $recSuva['userFirstName'] ) : null;
			$suvaUserLastName = $keyExists ?  self::naToNull_upper( $recSuva['userLastName'] ) : null;
			$suvaUserOib = $keyExists ? $recSuva['userOib'] : null;
			$suvaUserAddress = $keyExists ?  self::naToNull_upper( $recSuva['userAddress'] ) : null;
			$suvaUserCity = $keyExists ?  self::naToNull_upper( $recSuva['userCity'] ) : null;
			$suvaUserZip = $keyExists ?  self::naToNull_upper( $recSuva['userZip'] ) : null;
			$suvaUserPhone = $keyExists ?  self::naToNull_upper( $recSuva['userPhone'] ) : null;
			$suvaUserEmail = $keyExists ? $recSuva['userEmail'] : null;
			$suvaUserSource = $keyExists ? $recSuva['userSource'] : null;

			$suvaProduct = $keyExists ? $recSuva['n_avus_adlayout_id'] : null;
			$suvaCategory = $keyExists ? $recSuva['n_avus_class_id'] : null;
			
			$suvaProductId = $keyExists ? $recSuva['productId'] : null;
			$suvaCategoryId = $keyExists ? $recSuva['categoryMappingId'] : null;
			
			$suvaCountProducts = $recSuva['countProducts'];
			$suvaCountCategories = $recSuva['countCategories'];
			$suvaCountUsers = $recSuva['countUsers'];
			
			$suvaCountIdenticalAds = $recSuva['countSuvaAds'];


			if ($keyExists && $recSuva['n_dont_sync_with_avus'] == 1 ){
				Nava::poruka("<h3>AVUS OGLAS $avusId  je blokiran u SUVI (oglas $suvaId ) i neće se sinhronizirati</h3>");
				continue;
			}


			
			//varijable više razine
			$suvaMissingIssues = $avusRemaining > $suvaInsertionsAvailable ? $avusRemaining - $suvaInsertionsAvailable : null; 
			
			$isSuvaAdExisting = $keyExists;
			
			$avusInsertionsEqualSuvaInsertions = $keyExists && $avusRemaining == $suvaRemaining ? true : false;
			
			$avusUserExistsInSuvaUsersList = array_key_exists( (int)$avusUser, $dictSuvaUsers );
			$avusProductExistsInSuvaProductsList = array_key_exists( (int)$avusProduct, $dictSuvaProducts );
			$avusCategoryExistsInSuvaCategoriesList = array_key_exists( (int)$avusCategory, $dictSuvaCategories );
			
			$multipleSuvaProductsPointToAvusProduct = $suvaCountProducts > 1;
			$multipleSuvaCategoriesPointToAvusCategory = $suvaCountCategories > 1;
			$multipleSuvaUsersPointToAvusUser = $suvaCountUsers > 1;
			
			
			  
			//varijable najviše razine
			
			//ima li greški na podacima iz AVUSA ili SUVE
			$isErrorsInAvusData = ! ( $avusUserId > 0 && strlen($avusUserOib) > 0 ) ? true : false;
			$isErrorsInSuvaData = 
				!$avusProductExistsInSuvaProductsList 
				|| !$avusCategoryExistsInSuvaCategoriesList
				|| $multipleSuvaProductsPointToAvusProduct
				|| $multipleSuvaCategoriesPointToAvusCategory
				|| $multipleSuvaUsersPointToAvusUser
				|| $suvaCountIdenticalAds > 1
				|| $suvaMissingIssues > 0 ? true : false;
			
			//treba li importirat novi oglas iz suve i/ili dodat insertione 
			$isAvusAdCanBeImported = !($suvaId > 0) && $suvaMissingIssues === null && !$isErrorsInAvusData && !$isErrorsInSuvaData ? true : false;
			$isInsertionsCanBeAdded = !$isErrorsInSuvaData
				&& (
					( $isSuvaAdExisting && ( $avusRemaining != $suvaRemaining ) )
					||
					( !$isSuvaAdExisting )
				) ? true : false;
			if($isInsertionsCanBeAdded){
				Nava::poruka("insertions can be added, true");
				Nava::poruka("id avusa je ".$recAvus['ID']);
				if($isErrorsInSuvaData)
					Nava::poruka("is errors in suva data, true");
				if($isSuvaAdExisting)
					Nava::poruka("is suva ad existing, true");				
					Nava::poruka("avus remaining : ".$avusRemaining);
					Nava::poruka("suva remaining: ".$suvaRemaining);
			}
				
			//treba li importirat novog usera ili izmijenit podatke o postojećemu
			$isNewUserCanBeImportedFromAvus = $suvaCountUsers == 0 && !$avusUserExistsInSuvaUsersList && !$isErrorsInAvusData ? true : false;
			$isExistingUserCanBeUpdated = $suvaCountUsers == 1 && $suvaUserId > 0 && $avusUserOib == $suvaUserOib && $suvaUserSource == 'avus'
				&& !( 
					$avusUserFirstName == $suvaUserFirstName 
					&& $avusUserLastName == $suvaUserLastName 
					&& $avusUserAddress == $suvaUserAddress 
					&& $avusUserCity == $suvaUserCity 
					&& $avusUserZip == $suvaUserZip 
					&& $avusUserPhone == $suvaUserPhone 
				) ? true : false;	
				
			//treba li izmijenit proizvod ili kategoriju (ako su na AVUS oglasu izmijenjeni)
			$isProductOnExistingAdCanBeUpdated = $suvaId > 0 && $avusProduct != $suvaProduct && $avusProductExistsInSuvaProductsList ? true : false;
			$isCategoryOnExistingAdCanBeUpdated = $suvaId > 0 && $avusCategory != $suvaCategory && $avusCategoryExistsInSuvaCategoriesList ? true : false;
			$isOfflineDescriptionOnExistingAdCanBeUpdated =  $suvaId > 0 && $suvaSource == 'avus_online' ? true : false;
			
			
			//AKO AVUS OGLAS SE MOŽE IMPROTIRATI, PROVJERITI JOŠ JE LI POSTOJI SUVA USER S ISTIM EMAILOM KAO AVUS USER
			//02.06.2017
			$extraMessage = '';
			if ($isNewUserCanBeImportedFromAvus && $avusUserEmail ){
				//pronadji u suvi ima li već user s tim emailom - radi email_unique
				$existingSuvaUserId = $suvaUserId ? $suvaUserId : -1;
				$otherSuvaUserWithEmail = \Baseapp\Suva\Models\Users::findFirst("email = '$avusUserEmail' and id <> $existingSuvaUserId");
				if ($otherSuvaUserWithEmail ){
					$isNewUserCanBeImportedFromAvus = false;
					$isAvusAdCanBeImported = false;
					$extraMessage .="<br/>U Suvi postoji korisnik $otherSuvaUserWithEmail->id s istim emailom kao avus korisnik $avusUserId";
				}
			}
		
			if ($extraMessage){
				Nava::poruka($extraMessage);
			}

			//treba li prikazat poruku da se obrađuje oglas
			$isNoticeAboutAdShouldBeDisplayed = 
				$isErrorsInSuvaData
				|| $isErrorsInAvusData
				|| $isAvusAdCanBeImported
				|| $isNewUserCanBeImportedFromAvus
				|| $isProductOnExistingAdCanBeUpdated
				|| $isCategoryOnExistingAdCanBeUpdated
				|| $isOfflineDescriptionOnExistingAdCanBeUpdated
				|| $isExistingUserCanBeUpdated ? true : false;

			
			//GLAVNI IF-ovi - PRIKAZ PORUKA NA EKRANU I OBRADE
			if ( $isNoticeAboutAdShouldBeDisplayed && !$isAvusAdCanBeImported ){

			Nava::poruka("<h4>
				Obrađuje se avus oglas $avusId ( $i / $countRsAvus )
				<br/>. . . . . . .user $avusUser, product $avusProduct, category $avusCategory, insertions $avusRemaining,   
				<br/>. . . . . . .AVUS DATA OIB : <b>$avusUserOib</b>, First Name : <b>$avusUserFirstName</b>, Last Name : <b>$avusUserLastName</b>, Address: <b>$avusUserAddress</b>, City <b>$avusUserCity</b>, Zip: <b>$avusUserZip</b>, Phone: <b>$avusUserPhone</b>
						
				<br/>. . . . . . .Nađeno $suvaCountIdenticalAds SUVA Oglasa:  ID $suvaId, user $suvaUser, product $suvaProduct, category $suvaCategory, insertions $suvaRemaining
				<br/>. . . . . . .Podaci u Suvi: OIB : <b>$suvaUserOib</b>,  First Name : <b>$suvaUserFirstName</b>, Last name: <b>$suvaUserLastName</b>, Address: <b>$suvaUserAddress</b>, City <b>$suvaUserCity</b>, Zip: <b>$suvaUserZip</b>, Phone: <b>$suvaUserPhone</b>
				<br/>. . . . . . . .suvaId: $suvaId, suvaCountUsers: $suvaCountUsers, suvaUserId: $suvaUserId, suvaCountCategories: $suvaCountCategories, suvaCategoryId: $suvaCategoryId, suvaCountProducts: $suvaCountProducts, suvaProductId: $suvaProductId
					$extraMessage
				</h4>	
				");
			}

			if ( $isProductOnExistingAdCanBeUpdated ){
				$errLogMsg .=" changing SUVA product $suvaProduct to $avusProduct";
				Nava::poruka ("Proizvod  na avusu je $avusProduct a na suvi je $suvaProduct, mijenja se proizvod na SUVA oglasu");

				//nije isti proizvod, sinhroniziranje s Avusom
				$noUpdatedProducts++;
				Nava::runSql ("update ads set n_avus_adlayout_id = $avusProduct where id = $suvaId");
				
				$suvaProduct = $avusProduct;
				
				//self::setProductIdForAvusId ( $suvaAd , $avusProduct );
			}
			
			if ( $isCategoryOnExistingAdCanBeUpdated ){
				$errLogMsg .=" changing SUVA category $suvaCategory to $avusCategory";
				
				Nava::poruka ("Kategorija  na avusu je $avusCategory a na suvi je $suvaCategory, mijenja se kategorija na SUVA oglasu");

				//nije isti proizvod, sinhroniziranje s Avusom
				$noUpdatedProducts++;
				Nava::runSql ("update ads set n_avus_class_id = $avusCategory where id = $suvaId");
				
				$suvaCategory = $avusCategory;
				//self::setProductIdForAvusId ( $suvaAd , $avusProduct );
			}
			
			if ( $isOfflineDescriptionOnExistingAdCanBeUpdated ){
				$errLogMsg .=" changing offline description for ad imported by Agim";
				Nava::poruka (" mijenja se offline description na SUVA oglasu");

				//nije isti proizvod, sinhroniziranje s Avusom
				$adDescription = self::mysql_escape_mimic(self::avusCharsetToUtf8($recAvus['ADTEXT']));
				Nava::runSql ("update ads set description = '$adDescription' ,n_source = 'avus' where id = $suvaId");
				
				//self::setProductIdForAvusId ( $suvaAd , $avusProduct );
			}			
			
			
				
			if ( $isErrorsInAvusData ){
				$errLogErr .= " wrong USER data in AVUS , user ID = $avusUser";
				//greška u podacima iz AVUSA, iz Avusa je došao oglas ali nisu došli dobri podaci o  useru
				Nava::greska("3. 
					Za oglas $avusId su iz Avusa stigli krivi podaci o useru: n_avus_customer_id = $avusUser.:
					<br/> OIB : <b>$avusUserOib</b>, First Name : <b>$avusUserFirstName</b>, Last Name : <b>$avusUserLastName</b>, Address: <b>$avusUserAddress</b>, City <b>$avusUserCity</b>, Zip: <b>$avusUserZip</b>, Phone: <b>$avusUserPhone</b>
				");		
				Nava::greska("Obrada AVUS oglasa $avusId je prekinuta. Ispravite grešku i ponovite obradu ");
				$noAvusDataErrors++;
				continue;
			}
			
			if ( $isNewUserCanBeImportedFromAvus ){
				$errLogMsg .= " importing AVUS user $avusUser to SUVA ";
				//ako postoji oglas u Suvi ali nema korisnika, a poslani su svi bitni podaci preko Avusa, radi se novi korisnik u Suvi
				//Nava::poruka ("select * from users where n_avus_customer_id = $avusUser");
				Nava::poruka("2. 
					Za AVUS oglas $avusId: Ne postoji SUVA korisnik koji na sebi ima n_avus_customer_id = $avusUser. Importira se AVUS korisnik:
					<br/> OIB : <b>$avusUserOib</b>, First Name : <b>$avusUserFirstName</b>, Last Name : <b>$avusUserLastName</b>, Address: <b>$avusUserAddress</b>, City <b>$avusUserCity</b>, Zip: <b>$avusUserZip</b>, Phone: <b>$avusUserPhone</b>
				");
	
				// ako avus custome rnumber počinje sa P onda je privatni inače je poslovni
				
				if (substr($avusCustomerNumber,0,1) == 'P')
					$jeFirma = false;
				else
					$jeFirma = true;
	
				$suvaUser = $avusUserId;

				$suvaUserId = $avusUser;
				$suvaUserFirstName = $jeFirma ? 'n/a' : $avusUserFirstName;
				$suvaUserLastName = $jeFirma ? 'n/a' : $avusUserLastName;
				$suvaUserCompanyName = $jeFirma ? $avusUserLastName : '';
				$suvaUserType = $jeFirma ? 2 : 1;
				$suvaUserOib = $avusUserOib;
				$suvaUserAddress = $avusUserAddress;
				$suvaUserCity = $avusUserCity;
				$suvaUserZip = $avusUserZip;
				$suvaUserPhone = $avusUserPhone;
				$suvaUserEmail = $avusUserEmail;
				$suvaUserSource = 'avus';

				$suvaProduct = $avusProduct;
				$suvaCategory = $avusCategory;
	
	
				$user = new \Baseapp\Suva\Models\Users();
				$user->n_set_default_values($user->getSource());
		
				$user->n_generateUsernamePassword();
				$user->username = "avus_user_$avusUserId";
				$user->email = $suvaUserEmail;
				$user->first_name = $suvaUserFirstName;
				$user->last_name = $suvaUserLastName;
				$user->company_name = $suvaUserCompanyName;
				$user->type = $suvaUserType;
				$user->address = $avusUserAddress ? $avusUserAddress : 'n/a';
				$user->city = $avusUserCity ? $avusUserCity : 'n/a';
				$user->zip_code = $avusUserZip ? $avusUserZip : 'n/a';
				$user->phone1 = $avusUserPhone ? $avusUserPhone : 'n/a';
				$user->n_avus_customer_number = $avusCustomerNumber;
				$user->n_avus_customer_id = $avusUserId;
				$user->n_source = 'avus';
				$user->oib = $avusUserOib ? $avusUserOib : 'n/a';
				$user->n_avus_sync_status = 'synced';
		
				$success = $user->create();
				if ( $success === true ){	
					$errLogMsg .= " SuCCEDED!! ";
					$suvaUserId = $user->id;
					$suvaUser = $user->id;
					if ( $isSuvaAdExisting && $suvaId > 0 ) Nava::runSql ("update ads set user_id = $suvaUserId where id = $suvaId");
					$dictSuvaUsers[ $avusUser ] = $suvaUserId;
				}
				else {
					$greskeModela = $user->getMessages();
					foreach ($greskeModela as $greska)  
						$errLogErr.= $greska.", ";						
					$errLogErr.= " FAILED!! ";
					Nava::greska("Dodavanje korisnika u SUVU nije uspjelo (AVUS CUSTOMER ID: $avusUser)");
					Nava::greska("Obrada AVUS oglasa $avusId je prekinuta. Ispravite grešku i ponovite obradu (2)");
					continue;
				}
			}

			if ( $isExistingUserCanBeUpdated){
				//postoji jedan user ali ima drukčije podatke, treba ažurirati s podacima iz Avusa
				$errLogMsg .= " updating SUVA user with AVUS data from user $avusUser ";
				Nava::poruka ("5. 
					Na Avus oglasu $avusId (SUVA ID: $suvaId ) postoji user koji je ranije importiran iz Avusa (avus ID :$avusUser , Suva ID $suvaUser, OIB:$avusUserOib )
					<br/> Podaci u Avusu: First Name : <b>$avusUserFirstName</b>, Last Name : <b>$avusUserLastName</b>, Address: <b>$avusUserAddress</b>, City <b>$avusUserCity</b>, Zip: <b>$avusUserZip</b>, Phone: <b>$avusUserPhone</b>
					<br/> Podaci u Suvi: First Name : <b>$suvaUserFirstName</b>, Last name: <b>$suvaUserLastName</b>, Address: <b>$suvaUserAddress</b>, City <b>$suvaUserCity</b>, Zip: <b>$suvaUserZip</b>, Phone: <b>$suvaUserPhone</b>
					<br/> - podaci u Suvi će se ažurirati s podacima iz Avusa, ako su uneseni u AVUS. 
				");
				
				$user = \Baseapp\Suva\Models\Users::findFirst($suvaUserId);
			
				if ( strlen($avusUserFirstName) > 0 ) $user->first_name = $avusUserFirstName ;
				if ( strlen($avusUserLastName) > 0 ) $user->last_name = $avusUserLastName;
				if ( strlen($avusUserAddress) > 0 ) $user->address = $avusUserAddress ;
				if ( strlen($avusUserCity) > 0 ) $user->city = $avusUserCity;
				if ( strlen($avusUserZip) > 0 ) $user->zip_code = $avusUserZip;
				if ( strlen($avusUserPhone) > 0 ) $user->phone1 = $avusUserPhone ;
				$user->email = $avusUserEmail ? $avusUserEmail : "avus_$avusUserId";
				if ( strlen($avusCustomerNumber) > 0 ) $user->n_avus_customer_number = $avusCustomerNumber;
				
				$success = $user->update();
				if ( $success === true ){	
					if (self::ISDEBUG)  Nava::sysMessage("Updatean je suva user $suvaUserId je novi user na temelju AVUS usera: $avusUserId ");
				}
				else {
					Nava::greska("Greska kod updateanja SUVA usera $suvaUserId.");
				}
			}
				
			
			if ( $isErrorsInSuvaData ){
				//greška u podacima iz SUVE, iz Avusa je došao oglas ali nisu došli dobri podaci o  useru
				$msgTxt = " GREŠKA U PODACIMA IZ SUVE za AVUS oglas $avusId. Obrada je prekinuta. Ispravite grešku i ponovite obradu (1) ";
				if ( !$avusProductExistsInSuvaProductsList ) $msgTxt.="<br/> Ne postoji proizvod koji ima avus_adlayout_id = $avusProduct";
				if ( $multipleSuvaProductsPointToAvusProduct ) $msgTxt.="<br/> Postoji $suvaCountProducts proizvoda koji imaju avus_adlayout_id = $avusProduct";
				if ( !$avusCategoryExistsInSuvaCategoriesList ) $msgTxt.="<br/> Ne postoji kategorija koji ima avus_class_id = $avusCategory";
				if ( $multipleSuvaCategoriesPointToAvusCategory ) $msgTxt.="<br/> Postoji $suvaCountCategories kategorija koji imaju avus_class_id = $avusCategory";
				if ( $multipleSuvaUsersPointToAvusUser ) $msgTxt.="<br/> Postoji $suvaCountUsers usera koji su importirani iz AVUSA koji imaju n_avus_customer_id = $avusUser";
				if ( $suvaMissingIssues ) $msgTxt.="<br/> Avus oglas ima još $avusRemaining izdanja a u Suvi je dostupno $suvaInsertionsAvailable";
				if ( $suvaCountIdenticalAds > 1 ) $msgTxt.="<br/> U Suvi postoji $suvaCountIdenticalAds Avus oglasa koji imaju n_avus_order_id = $avusId";
				
				
				
				// !$avusProductExistsInSuvaProductsList 
				// || !$avusCategoryExistsInSuvaCategoriesList
				// || $multipleSuvaProductsPointToAvusProduct
				// || $multipleSuvaCategoriesPointToAvusCategory
				// || $multipleSuvaUsersPointToAvusUser
				// || $suvaCountIdenticalAds > 1
				// || $suvaMissingIssues > 0 ? true : false;
				
				$errLogErr .=" wrong data in SUVA data ";
				Nava::greska($msgTxt);
				continue;				
			}	
		
			
			if ( $isAvusAdCanBeImported ){
				//oglas ne postoji u SUVI i treba ga dodati
				$errLogMsg .= "  Importing AVUS AD $avusId ";
				
				Nava::poruka ("8. Avus oglas $avusId ne postoji u Suvi i dodaje se");
				$noImportedAds++;

				$suvaId = null;
				
				$newAdDescription = self::mysql_escape_mimic(self::avusCharsetToUtf8($recAvus['ADTEXT']));
				$newAdTitle = self::mysql_escape_mimic(self::avusCharsetToUtf8(substr($recAvus['ADTEXT'],0,75)));
				$now = date("Y-m-d h:i:s", time());

				$suvaProduct = $avusProduct;
				$suvaCategory = $avusCategory;
				$suvaUser = $avusUser;
				
				$suvaUserId = $dictSuvaUsers[$avusUser];
				
				
				
				$txtSql = "
					insert into ads (
						category_id
						,user_id
						,title
						,description
						,description_offline
						,created_at
						,product_sort
						,n_source
						,n_frontend_sync_status
						,n_avus_order_id
						,n_avus_customer_number
						,n_avus_class_id
						,n_avus_adlayout_id
						,n_avus_remaining_insertions
					)
					values(
						4
						,$suvaUserId
						,'$newAdTitle'
						,'$newAdDescription'
						,'$newAdDescription'
						,'$now'
						,1
						,'avus'
						,'no_sync'
						,$avusId
						,$avusUser
						,$avusCategory
						,$avusProduct
						,$avusRemaining
					)
				";
				
				if (self::ISDEBUG) Nava::sysMessage( "Inserting avus AD $avusOrderId ");
				Nava::runSql ($txtSql);
				//Nava::poruka($txtSql);
				
				$suvaId = Nava::sqlToArray("select max(id) as id from ads")[0]['id'];
				$suvaRemaining = 0;
			}
			
			if ( $isInsertionsCanBeAdded ){
				
				if ( ! ( $suvaId > 0 ) ){
					//moguće je ako se  dogodo greška  pri kreiranju novog oglasa
					Nava::greska ("Insertioni ne mogu biti dodani jer ne postoji ID oglasa (vjerojatno je došlo do greške kod kreiranja novog oglasa) ");
					Nava::greska("Obrada AVUS oglasa $avusId je prekinuta. Ispravite grešku i ponovite obradu ");
					continue;
				}
				
				//nema dovoljno izdanja 
				Nava::poruka("Za oglas AVUS ID: $avusId,  SUVA ID: $suvaId, potrebno je dodati $avusRemaining izdanja, na SUVA oglasu ih je $suvaRemaining. Ažuriraju se insertioni");
				//nije isti broj insertiona, ažuriraju se insertioni
				$noUpdatedInsertions++;
				
				//brisanje insertiona na Adu koji se odnose na buduća izdanja (prethodna izdanja se ne brišu)
				$txtSql = "
					delete 
					from 
						n_insertions_avus
					where 
						ad_id = $suvaId
						and issue_id in (
							select 
								id
							from 
								n_issues
							where
								publications_id = 2 
								and date_published > now()
						)
				";
				Nava::runSql ($txtSql);
				
				$errLogMsg .= "   adding $avusRemaining insertions to suva AD ";
				$i = 0;
				$rsSuvaFutureIssues->rewind();
				foreach  ($rsSuvaFutureIssues as $issue ){
					$i++;
					if ($i > $avusRemaining ) continue;
					$sqlIsPaid = $avusIsPaidAd === true ? '1' : '0';
					$txtSql = "insert into n_insertions_avus (ad_id, issue_id, date_published, is_paid ) values ($suvaId, $issue->id, '$issue->date_published',  $sqlIsPaid)";
					Nava::sysMessage($txtSql);
					Nava::runSql($txtSql);
				}
				
				Nava::sysMessage ("buducih ubaceno u bazu: ".Nava::sqlToArray("select count(*) as broj from n_insertions_avus where ad_id = $suvaId and date_published > now()")[0]['broj']);
				
				
				//self::refreshFutureInsertions( $suvaId, $avusRemaining );	
			}
			
		}
		Nava::poruka("12. <B>AVUS sync finished.</B> <br/> Imported ads: $noImportedAds , errors in AVUS on $noAvusDataErrors ads, insertions updated on $noUpdatedInsertions ads, users changed on $noUpdatedUsers ads, products changed on $noUpdatedProducts ads and categories changed on $noUpdatedCategories ads.");
		
		libAppSetting::set('avus_process_running', false);
		ini_set('error_log',$old_error_log);
	}
	
	public static function naToNull_upper( $value ){
		$result = $value != 'n/a' && strlen( $value ) > 0 ? strtoupper( $value ) : null;
		
		//neka slova iz AVUSA se ne prebacuju pomoću funckije strtoupper, vjer. redi različitih kodnih stranica
		$result = str_replace ('đ', 'Đ', $result);
		$result = str_replace ('ć', 'Ć', $result);
		$result = str_replace ('č', 'Č', $result);
		$result = str_replace ('š', 'Š', $result);
		$result = str_replace ('ž', 'Ž', $result);
		
		return $result;
		
		
	}
	
	
		
	public static function refreshExistingSuvaAdWithDataFromAvus ( \Baseapp\Suva\Models\Ads $ad , $row = null){
		if (self::ISDEBUG) error_log("refreshExistingSuvaAdWithDataFromAvus");
		//oglas koji je već importiran iz Avusa se sinhronizira (insertioni, tekst, user, kategorija)
		if ( ! $ad->n_avus_order_id > 0 ) return null;
		
		if (!$row) {
			$txtSql = "
				select
					*
				from 
					g_orders 
				where  ID = $ad->n_avus_order_id
			";
			$row = self::sqlToRow ( $txtSql );
		}
		if ($row){
			$defaultUserId = libAppSetting::get('avus_default_user_id_for_import'); //dodaje se defaultni, kasnije se synca
			$description = self::mysql_escape_mimic(self::avusCharsetToUtf8($row['ADTEXT']));
			$title = self::mysql_escape_mimic(self::avusCharsetToUtf8(substr($row['ADTEXT'],0,75)));
			
			$now = date("Y-m-d h:i:s", time());
			$avusOrderId = $row['ID'];
			$avusCustNumber = $row['CUSTOMERID'];
			$avusClassId = $row['CLASSID'];
			$avusAdlayoutId = $row['ADLAYOUTID'];
			$avusRemainingInsertions = intval($row['CREATEDINSERTIONS']) - intval($row['ISSUEDINSERTIONS']) - intval($row['BLOCKEDINSERTIONS']);
			
		
			$txtSql = "
				update ads set
					category_id = 4
					,title = '$title'
					,description = '$description'
					,description_offline = '$description'
					,modified_at = '$now'
					,n_avus_order_id = $avusOrderId
					,n_avus_customer_number = $avusCustNumber
					,n_avus_class_id = $avusClassId
					,n_avus_adlayout_id = $avusAdlayoutId
					,n_avus_remaining_insertions = $avusRemainingInsertions
				where 
					id = $ad->id
			";
			Nava::runSql ($txtSql);
			return $ad->id;
		}
		else{
			return null;
		}
	}
	
	public static function insertAvusAdInSuva ( $row ){
		if (self::ISDEBUG) error_log("insertAvusAdInSuva");
		if (! $row ) return null;
				//ne postoji oglas u SUVI
		$userId = libAppSetting::get('avus_default_user_id_for_import'); //defaultni avus user id
		
		$description = self::mysql_escape_mimic(self::avusCharsetToUtf8($row['ADTEXT']));
		$title = self::mysql_escape_mimic(self::avusCharsetToUtf8(substr($row['ADTEXT'],0,75)));
		//$title = substr($description,0,80);
		 
		
		$now = date("Y-m-d h:i:s", time());
		
		$avusCustNumber = $row['CUSTOMERID'];
		
		$avusRemainingInsertions = intval($row['CREATEDINSERTIONS']) - intval($row['ISSUEDINSERTIONS']) - intval($row['BLOCKEDINSERTIONS']);
		$avusOrderId = $row['ID'];
		$avusClassId = $row['CLASSID'];
		$avusAdlayoutId = $row['ADLAYOUTID'];

		$txtSql = "
			insert into ads (
				category_id
				,user_id
				,title
				,description
				,description_offline
				,created_at
				,product_sort
				,n_source
				,n_frontend_sync_status
				,n_avus_order_id
				,n_avus_customer_number
				,n_avus_class_id
				,n_avus_adlayout_id
				,n_avus_remaining_insertions
			)
			values(
				4
				,$userId
				,'$title'
				,'$description'
				,'$description'
				,'$now'
				,1
				,'avus'
				,'no_sync'
				,$avusOrderId
				,$avusCustNumber
				,$avusClassId
				,$avusAdlayoutId
				,$avusRemainingInsertions
			)
		"; 
		
		if (self::ISDEBUG) error_log( "Inserting avus AD $avusOrderId ");
		//error_log( $txtSql );
		//error_log("UBAČEN JE NOVI OGLAS IZ AVUSA: $txtSql");
		Nava::runSql ($txtSql);
		
		
		$rs = Nava::sqlToArray("select id from ads where n_avus_order_id = $avusOrderId");
		foreach( $rs as $row)
			return $row['id'];
		return null;
	}
	
	public static function refreshFutureInsertions( $pAdId, $pNoInsertions ){
			
		//brisanje insertiona na Adu koji se odnose na buduća izdanja (prethodna izdanja se ne brišu)
		$txtSql = "
			delete 
			from 
				n_insertions_avus
			where 
				ad_id = $pAdId
				and issue_id in (
					select 
						id
					from 
						n_issues
					where
						publications_id = 2 
						and date_published > now()
				)
		";
		Nava::runSql ($txtSql);
		
		//Ubacivanje AVUS insertiona za buduća izdanja
		$futureIssues = \Baseapp\Suva\Models\Issues::find(array(
			" publications_id = 2 and date_published > now()"
			, 'order' => 'date_published asc'
			, 'limit' => $pNoInsertions
			));
		
		foreach  ($futureIssues as $issue ){
			$txtSql = "insert into n_insertions_avus (ad_id, issue_id) values ($pAdId, $issue->id)";
			Nava::runSql($txtSql);
		}
			

	}
	
	public static function refreshInsertionsForFutureIssues ( \Baseapp\Suva\Models\Ads $ad ){
		if ( $ad->n_avus_remaining_insertions > 0 ){
			
			if (self::ISDEBUG) error_log("Remaining insertions = $ad->n_avus_remaining_insertions");
			
			// BRISANJE BUDUCIH INSERTIONA ???
			// NAVA::runSql("delete from n_insertions_avus as nia join ads as a on nia.ad_id = a.id 
			// WHERE nia.ad_id == a.id");
			
			
			//brisanje insertiona na Adu koji se odnose na buduća izdanja (prethodna izdanja se ne brišu)
			$txtSql = "
				delete 
				from 
					n_insertions_avus
				where 
					ad_id = $ad->id
					and issue_id in (
						select 
							id
						from 
							n_issues
						where
							publications_id = 2 
							and date_published > now()
					)
			";
			Nava::runSql ($txtSql);
			
			//Ubacivanje AVUS insertiona za buduća izdanja
			$futureIssues = \Baseapp\Suva\Models\Issues::find(array(
				" publications_id = 2 and date_published > now()"
				, 'order' => 'date_published asc'
				, 'limit' => $ad->n_avus_remaining_insertions
				));
			
			$brojDostupnih = $futureIssues->count();
			if ( $brojDostupnih < $ad->n_avus_remaining_insertions ) {
				if (self::ISDEBUG) error_log ("Za oglas AVUS ID: $ad->n_avus_order_id,  SUVA ID: $ad->id, potrebno je dodati $ad->n_avus_remaining_insertions izdanja, a dostupno ih je $brojDostupnih . Dodati još ".($ad->n_avus_remaining_insertions - $brojDostupnih)." izdanja i ponovo pokrenuti obradu");
				
				Nava::greska("Za oglas AVUS ID: $ad->n_avus_order_id,  SUVA ID: $ad->id, potrebno je dodati $ad->n_avus_remaining_insertions izdanja, a dostupno ih je $brojDostupnih . Dodati još ".($ad->n_avus_remaining_insertions - $brojDostupnih)." izdanja i ponovo pokrenuti obradu");
			}
			
			foreach  ($futureIssues as $issue ){
				if (self::ISDEBUG) error_log("Dodavanje insertiona za oglas: AVUS ID: $ad->n_avus_order_id,  SUVA ID: $ad->id, ISSUE: $issue->id, datum $issue->date_published  ");
				//Nava::poruka ("adding insertion for issue $issue->id $issue->date_published ");
				$txtSql = "insert into n_insertions_avus (ad_id, issue_id) values ($ad->id, $issue->id)";
				Nava::runSql($txtSql);
			}
		}
		else{
			Nava::poruka("libAVUS:refreshInsertionsForFutureIssues - GREŠKA Ad $pAd->id nema n_avus_remaining_insertions");
			//error_log("Oglas: AVUS ID: $ad->n_avus_order_id,  SUVA ID: $ad->id nema n_avus_remaining_insertions");
		}

	}

	public static function setUserIdForAvusAd ( \Baseapp\Suva\Models\Ads $ad , $avusCustomerId = null){
		//za AVUS oglas koji na sebi ima defaultnog usera traži usera, najprije u SUVI pa onda u AVUSU
		Nava::poruka ("setUserIdForAvusAd:: avusCustomerId = $avusCustomerId");
		if ( $avusCustomerId ){
			//traži se user u Suvi koji je matchan na avus customera
			$user = \Baseapp\Suva\Models\Users::findFirst ("n_avus_customer_id = $avusCustomerId ");
			if ($user){
				//ako postoji user u suvi koji ima isti customer:id kao avusUser onda se updatea
				Nava::runSql ("update ads set user_id = $user->id where id = $ad->id ");
				return $user->id;
			}
			else {
				//ako ne postoji user u suvi traži se u avusu
				$recAvusCustomer = self::getAvusCustomer($avusCustomerId);
				if ($recAvusCustomer){
					$suvaUserId = self::insertAvusUserInSuva ( $recAvusCustomer );
					return $suvaUserId;
				}
				else {
					Nava::greska ("U Avusu nije pronađen customer s brojem $avusCustomerId koji je na AVUS oglasu $ad->n_avus_order_id");
					return null;
				}
			}
		} 
		else {
			//traženje na stari način
		
			$defaultAvusUserId = libAppSetting::get('avus_default_user_id_for_import');
			$user = $ad->n_avus_customer_number > 0 ? \Baseapp\Suva\Models\Users::findFirst("n_avus_customer_id = $ad->n_avus_customer_number") : null;
			
			if ( ! $ad->user_id === $defaultAvusUserId && $user && $ad->user_id === $user->id ){
				//poslan je oglas koji na sebi već ima valjanog usera
				Nava::poruka("libAvus:setUserIdForAvusAd  Oglas AVUS ID: $ad->n_avus_order_id,  SUVA ID: $ad->id već ima usera ID = $user->id, $user->first_name $user->last_name $user->company_name");
				return $ad->user_id;
			}
			if ( ! $ad->user_id === $defaultAvusUserId && $user && $ad->user_id !== $user->id ){
				//poslane je oglas koji na sebi već ima valjanog usera, ali je user drukčiji (npr. izmijenjen je na oglasu u AVUSU pa je syncan)
				Nava::runSql ("update ads set user_id = $user->id");
				if ( self::ISDEBUG ) error_log ("Na Oglasu: AVUS ID: $ad->n_avus_order_id,  SUVA ID: $ad->id je izmijenjen user, sada je: ID = $user->id, $user->first_name $user->last_name $user->company_name  koji je pronađen u SUVI");
				return $user->id;
			}

			if ( ! $ad->user_id === $defaultAvusUserId && $user ){
				//oglas na sebi ima usera, ali tog usera nema u SUVI - GREŠKA
				Nava::poruka ("libAVUS:setUserIdForAvusAd Oglas: AVUS ID: $ad->n_avus_order_id,  SUVA ID: $ad->id već ima usera ID = $ad->user_id kojeg NEMA U SUVI!!");
				return null;
			}
			elseif ( $ad->user_id === $defaultAvusUserId && $user ){
				//oglas ima defaultnog usera, ali user iz AVUS-a postoji u SUVI
				Nava::runSql ("update ads set user_id = $user->id");
				if ( self::ISDEBUG ) error_log ("Na Oglas: AVUS ID: $ad->n_avus_order_id,  SUVA ID: $ad->id je dodan user ID = $user->id, $user->first_name $user->last_name $user->company_name  koji je pronađen u SUVI");
				return $user->id;
			}
			elseif ( $ad->user_id === $defaultAvusUserId && ! $user && $ad->n_avus_customer_number > 0 ){
				//oglas ima defaultnog usera, ali user iz AVUS-a NE postoji u SUVI, traži se u AVUSU
				$txtSql = "
					select top 1 
						c.id as customer_id
						,c.number as customer_number
						,cid.taxnumber as customer_taxnumber
						,a.first_name
						,a.last_name
						,a.street
						,a.postcode
						,a.city
						,a.countryabr
						,a.contactperson
						,a.email
						,a.telareacode
						,a.telno
					from
						customers c
							join g_customerinvoicedata cid on (c.id = cid.customerid)
							left join addressee a on (c.addresseeid = a.adressee_id)
					where 1=1 
						and c.id = $ad->n_avus_customer_number
					"; 
				$rsAvusUser = self::sqlToArray($txtSql);
				if (  count($rsAvusUser) == 1 ){
					foreach( $rsAvusUser as $recAvusUser )
						$newId = self::insertAvusUserInSuva ( $recAvusUser );
					if( $newId > 0){
						$newUser = \Baseapp\Suva\Models\Users::findFirst($newId);
						$txtSql = "update ads set user_id = $newId where id = $ad->id ";
						Nava::runSql($txtSql);
						if ( self::ISDEBUG ) error_log ("Na Oglas: AVUS ID: $ad->n_avus_order_id,  SUVA ID: $ad->id je dodan novi user ID = $newUser->id, $newUser->first_name $newUser->last_name $newUser->company_name  koji je pronađen u AVUSU pod brojem $ad->n_avus_customer_number");
						return $newId;
					}
					else {
						Nava::poruka("libAVUS:setUserIdForAvusAd Greska prilikom dodavanja AVUS Usera $ad->n_avus_customer_number u SUVU");
						return null;
					}
				}
				else{
					//error_log("GREŠKA: U Avusu je pronađeno ".count($rsAvusUser)." usera s ID-jem $ad->n_avus_customer_number ");
					return null;
				}
			}
			elseif ( $user && $ad->n_avus_customer_number == $user->n_avus_customer_id && $ad->user_id !== $user->id ){
				//Oglasu dodijeljen krivi user, a sad je pronađen pravi. Ima ovih slučaja 200-injak, dodati pravog usera adu
				if ( self::ISDEBUG ) error_log("za Oglas: AVUS ID: $ad->n_avus_order_id,  SUVA ID: $ad->id je stavljen user $ad->user_id, a prema polju avus_customer_id je matching user $user->id. ad->user_id se mijenja iz $ad->user_id u $user->id .");
				Nava::runSql("update ads set user_id = $user->id where id = $ad->id");
			}
			else {
				Nava::greska ("GREŠKA kod dohvata usera $ad->n_avus_customer_number za Oglas: AVUS ID: $ad->n_avus_order_id,  SUVA ID: $ad->id , ad->user_id = $ad->user_id , defaultAvusUserId = $defaultAvusUserId, user->id = $user->id");
				if (is_object($user)) error_log ("USER:".print_r($user->toArray(), true));
				//error_log ("AD:".print_r($ad->toArray(), true));
				return null;
			}
		}
	}

	public static function setCategoryMappingIdForAvusAd ( \Baseapp\Suva\Models\Ads $avusAd, $pAvusClassId = null ){
		$avusClassId = $pAvusClassId;
		
		if ( $avusClassId > 0){
			//traži se categorymapping u Suvi koji je matchan na avus class id
			$catMap = \Baseapp\Suva\Models\CategoriesMappings::findFirst ("avus_class_id = $avusClassId ");
			if ($catMap){
				//ako postoji catmep u suvi koji ima isti classid kao avuClassId onda se updatea
				Nava::runSql ("update ads set n_avus_class_id = $catMap->avus_class_id where id = $avusAd->id ");
				return $catMap->id;
			}
			else {
				Nava::greska ("U SUVI nije pronađen CategoryMapping koji na sebi ima avus_class_id $avusClassId (na AVUS oglasu $avusAd->n_avus_class_id )");
			}
		} 
		else {
			//stari način
		
			if ( $avusAd->n_avus_class_id > 0 ){
				$catMap = \Baseapp\Suva\Models\CategoriesMappings::findFirst("avus_class_id = $avusAd->n_avus_class_id	");
				if( $catMap ){
					if ( $avusAd->n_category_mapping_id == $catMap->id ){
						//Nava::poruka ("Ad $avusAd->id ( Class ID: $avusAd->n_avus_class_id ) already linked to Category $catMap->id");
					}	
					else {
						Nava::runSql ("update ads set n_category_mapping_id = $catMap->id where id = $avusAd->id");
						if (self::ISDEBUG) error_log("Syncronized avus AD $avusAd->id with category $catMap->id ");
					}
				}
				else {
					Nava::greska("Found no category for Ad $avusAd->id ( Class ID: $avusAd->n_avus_class_id )");
				}
			}
			else{
					Nava::greska("Ad $avusAd->id (AVUS Order ID: $avusAd->n_avus_order_id ) does not have Class ID");
			}

		}
	}
		
	public static function setProductIdForAvusId ( \Baseapp\Suva\Models\Ads $suvaAd , $pAvusProductId = null ){
			$avusProductId = $pAvusProductId > 0 ? $pAvusProductId : -1;
			if ( $suvaAd->n_avus_adlayout_id != $avusProductId){
				$product = \Baseapp\Suva\Models\Products::findFirst( " avus_adlayout_id = $avusProductId " );
				if ($product){
					Nava::poruka ("Suva Ad $suvaAd->id (AVUS ID: $suvaAd->n_avus_order_id ) avus product id changed from $suvaAd->n_avus_adlayout_id to $avusProductId");
					Nava::runSql ("update ads set n_avus_adlayout_id = $avusProductId where id = $suvaAd->id");
					return $product->id;
				}
				else {
					Nava::greska ("U SUVI nije pronađen proizvod koji na sebi ima avus_adlayout_id $pAvusProductId");
					return null;
				}
			}
			else{
				return true;
			}
			
	}
	
	public static function insertAvusUserInSuvaFast ( 
		$avusUserId  
		,$avusUserFirstName
		,$avusUserLastName
		,$avusUserOib
		,$avusUserPhone
		,$avusCustomerNumber
		,$avusUserAddress
		,$avusUserCity
		,$avusUserZip
		,$avusUserEmail
	){
		//ubacivanje jednog korisnika u SUVU nakon pokretanja sql-a na avusu

		$user = new \Baseapp\Suva\Models\Users();
		$user->n_set_default_values($user->getSource());
		
		$user->n_generateUsernamePassword();
		$user->username = "avus_user_$avusUserId";
		$user->email = $avusUserEmail;
		
		$user->first_name = $avusUserFirstName ? $avusUserFirstName : 'n/a';
		$user->last_name = $avusUserLastName ? $avusUserLastName : 'n/a';
		$user->address = $avusUserAddress ? $avusUserAddress : 'n/a';
		$user->city = $avusUserCity ? $avusUserCity : 'n/a';
		$user->zip_code = $avusUserZip ? $avusUserZip : 'n/a';
		$user->phone1 = $avusUserPhone ? $avusUserPhone : 'n/a';
		$user->n_avus_customer_number = $avusCustomerNumber;
		$user->n_avus_customer_id = $avusUserId;
		$user->n_source = 'avus';
		$user->oib = $avusUserOib ? $avusUserOib : 'n/a';
		$user->n_avus_sync_status = 'synced';
		
		$success = $user->create();
		if ( $success === true ){	
			Nava::poruka("Kreiran je novi suva user ( ID: $user->id ) na temelju AVUS usera: $avusUserId  $avusCustomerNumber ");
			return $user->id;
		}
		else {
			Nava::greska($user->n_create(), "Greska kod kreiranja usera: ".print_r($user->toArray(),true));
			return null;
		}
	}	

	public static function insertAvusUserInSuva ( $row = null ){
		//ubacivanje jednog korisnika u SUVU nakon pokretanja sql-a na avusu
		if (! $row ) return null;
		
		$oib = (string)$row['customer_taxnumber'];
		$oib = str_replace('.', '', $oib);
		$oib = str_replace('*', '', $oib);
		
		$customerId = (int)$row['customer_id'];
		$customerNumber = self::avusCharsetToUtf8((string)$row['customer_number']);
		$firstName = self::avusCharsetToUtf8($row['first_name']);
		$lastName = self::avusCharsetToUtf8($row['last_name']);
		$address = self::avusCharsetToUtf8($row['street']);
		$city = self::avusCharsetToUtf8($row['city']);
		$phone = self::avusCharsetToUtf8(
			($row['telareacode'] ? $row['telareacode']."/" : "").$row['telno']
			);

		// error_log("syncAvusUsersAction 13");
		//error_log("Kreiranje novog usera na temelju AVUS usera:".$row['customer_id']." ".$row['customer_number']);
		$user = new \Baseapp\Suva\Models\Users();
		$user->n_set_default_values($user->getSource());
		
		$user->n_generateUsernamePassword();
		
		//$user->email = $row['email'] ? $row['email'] : "avus_".$row['customer_number'];
		$user->email = $row['email'] ? $row['email'] : "avus_".$row['customer_id'];
		$user->first_name = $firstName ? $firstName : 'n/a';
		$user->last_name = $lastName ? $lastName : 'n/a';
		$user->address = $address ? $address : 'n/a';
		$user->city = $city ? $city : 'n/a';
		$user->zip_code = $row['postcode'] ? $row['postcode'] : 'n/a';
		$user->phone1 = $phone ? $phone : 'n/a';
		$user->n_avus_customer_number = $customerNumber;
		$user->n_avus_customer_id = $customerId;
		$user->n_source = 'avus';
		$user->oib = $oib;
		$user->n_avus_sync_status = 'synced';
		
		$success = $user->create();
		if ( $success === true ){	
			if (self::ISDEBUG)  error_log("Kreiran je novi user na temelju AVUS usera:".$row['customer_id']." ".$row['customer_number']);
			// Nava::printr($user->toArray(),"Kreiran je user $user->id");
		}
		else {
			Nava::poruka($user->n_create(), "Greska kod kreiranja usera: ".print_r($user->toArray(),true));
		}
		return $user->id;
	}	

	//syncanje svih novih oglasa iz avusa, pamti koji je zadnji syncan
	public static function syncNewAvusAds(  ){
		//synca oglase iz avusa od zadnjeh syscanog (podatak u appSettings)
		//ako oglas već postoji u SUVI onda mu osvježava insertione i tekst iz AVUSA

		$appExecutionTime = ini_get('max_execution_time');
		ini_set('max_execution_time', 150000);
		ini_set ('request_terminate_timeout', 150000);
		
		$lastImportedAvusAdId = libAppSetting::get( 'LastImportedAvusAdId', 0 );
		
		$txtSql = "
			select top 50000
				*
				,ID 
			from 
				g_orders 
			where 1=1
				and lastdate > '2017-03-01'
				and lastobjectid in (2,4)
				and ( (createdinsertions - blockedinsertions) > 0 )
				and ( (createdinsertions - issuedinsertions) > 0 )
				and totalpricepublished = totalpricecreated
				and ID > $lastImportedAvusId
			order by ID
		";
		
		$rsAvusAds = self::sqlToArray($txtSql);

		Nava::poruka("Found ".$rsAvusAds->num_rows." ads to sync");
		$i = 1;
		if(!empty($rsAvusAds)){	
		
			if (self::ISDEBUG) error_log ("dohvaćeno ".count($rsAvusAds)." oglasa.");
			foreach( $rsAvusAds as $row ){
				$i++;
				$avusOrderId = $row['ID'];
				if (self::ISDEBUG) error_log ( "$i - obrađuje se oglas $avusOrderId" );	
				self::syncAvusAd ( $row );
			}
		}
		ini_set('max_execution_time', $appExecutionTime);
		ini_set('request_terminate_timeout', $appExecutionTime);
	}	
	


	
	//pomoćne funkcije
	public function getAvusCustomer ( $pCustomerId = null ){
		$customerId = $pCustomerId > 0 ? $pCustomerId : -1;
		//oglas ima defaultnog usera, ali user iz AVUS-a NE postoji u SUVI, traži se u AVUSU
		$txtSql = "
			select top 1 
				c.id as customer_id
				,c.number as customer_number
				,cid.taxnumber as customer_taxnumber
				,a.first_name
				,a.last_name
				,a.street
				,a.postcode
				,a.city
				,a.countryabr
				,a.contactperson
				,a.email
				,a.telareacode
				,a.telno
			from
				customers c
					join g_customerinvoicedata cid on (c.id = cid.customerid)
					left join addressee a on (c.addresseeid = a.adressee_id)
			where 1=1 
				and c.id = $customerId
			"; 
		$rsAvusUser = self::sqlToArray($txtSql);
		
		if (  count($rsAvusUser) == 1 ){
			return $rsAvusUser[0];
		}
		else {
				return null;
		}
	}

	public static function avusCharsetToUtf8 ($pText){
		
		$r = $pText;
		
		$r = str_replace(chr(154), chr(0xc5).chr(0xa1), $r); //š
		$r = str_replace(chr(138), chr(0xc5).chr(0xa0), $r); //Š
		$r = str_replace(chr(230), chr(0xc4).chr(0x87), $r); //ć
		$r = str_replace(chr(198), chr(0xc4).chr(0x86), $r); //Ć
		$r = str_replace(chr(232), chr(0xc4).chr(0x8d), $r); //č
		$r = str_replace(chr(200), chr(0xc4).chr(0x8c), $r); //Č
		$r = str_replace(chr(158), chr(0xc5).chr(0xbe), $r); //ž
		$r = str_replace(chr(142), chr(0xc5).chr(0xbd), $r); //Ž
		$r = str_replace(chr(128), 'EUR', $r); //€
		
		$r = str_replace(chr(240), chr(0xc4).chr(0x91), $r); //đ
		$r = str_replace(chr(208), chr(0xc4).chr(0x90), $r); //Đ
		
		$r = str_replace(chr(252),chr(0xc5).chr(0xb1), $r); //uu
		$r = str_replace(chr(220), chr(0xc5).chr(0xb0), $r); //UU

		
		return $r;
	}
	
	public static function mysql_escape_mimic($inp) { 
		if(is_array($inp)) 
			return array_map(__METHOD__, $inp); 

		if(!empty($inp) && is_string($inp)) { 
			return str_replace(array('//','\\', "\0", "\n", "\r", "'", '"', "\x1a"), array('////','\\\\', '\\0', '\\n', '\\r', "\\'", '\\"', '\\Z'), $inp); 
		} 

		
		return $inp; 
	} 

	
	//debugging funkcije
	public static function runTestSql( $pSql = null ){
		 
		if ( !$pSql ) {
			return Nava::greska ("SQL COMMAND NOT SUPPLIED");
		}
		
		//error_log($pSql);
		Nava::poruka($pSql);
		
			$db = new \PDO('dblib:host=195.245.255.21;port=23458;database=avus21;tdsLevel=CS_TDS', 'blackknight', 'blackknight');
			//Nava::poruka("Connected successfully to AVUS, running query $pSql"); 
			$stmt = $db->prepare($pSql);

			$rs = $db->query($pSql);
		
		
		
		//error_log("RS:".print_r($rs,true));
		
		try{
			//rezultat u tablicu
			//prvi red tablice - naslov
			if ( is_object($rs)  ){
				$row = $rs->fetch();
				//$row = $rs[0];
				if ($row){
					$htmTable = "<table border = '1'><tr>";
					$pair = false;
					foreach( $row as $key => $value ){
						if ( $pair === true )
							unset ( $row[$key] );
						$pair = $pair === true ? false : true;
					}
					//self::printr($row, "ROW 2");
					foreach( $row as $key => $value ){
						$htmTable = $htmTable."<td><b>$key</b></td>";
					}
				}
			}
			else{
				$htmTable = "<table border = '1'>";
			}
			$htmTable .="</tr>";
			foreach( self::sqlToArray($pSql) as $row ){
				//izbacuje svaki drugi red iz recordseta, jer ih PDO duplira
				$pair = false;
				$arrKeys = array();
				foreach( $row as $key => $value ){
					if ( $pair === true )
						unset ( $row[$key] );
					$pair = $pair === true ? false : true;
				}
				
				$htmTable .= "<tr>";
				foreach( $row as $key => $value ){
					$htmTable .="<td>$value</td>";
				}
				$htmTable .="</tr>";
				//Nava::printr($row);
			}
			$htmTable .= "</table>";
			Nava::poruka ("<b>Result:</b><br/>$htmTable");
			
			//error_log($htmTable);
		}
		catch( \PDOException $e){ 
			
			Nava::greska("PDO ERROR:".$e->getMessage());
			Nava::printr($db->errorInfo());
			Nava::poruka ("Data retrieved so far:<br/>$htmTable</td></tr></table>");
		}
	}
	
	
	//SQL funkcije
	
	public static function sqlToArray( $pSql = null){		
		
		if ( strlen($pSql) < 5) {			
			return Nava::greska("libAVUS::sqlToArray Parameterr pSql not supplied");
		}
	
		try{			
			$db = new \PDO('dblib:host=195.245.255.21;port=23458;database=avus21;tdsLevel=CS_TDS', 'blackknight', 'blackknight');
			//Nava::poruka("Connected successfully to AVUS, running query $pSql"); 
			$stmt = $db->prepare($pSql);

			$rs = $db->query($pSql);
			//error_log($pSql);
			$result = $rs->fetchAll();  //bez ovoga se ne može više puta pokrenuti foreach			
			return $result;
		}
		catch( \PDOException $e){ 			
			Nava::greska("PDO ERROR:".$e->getMessage());
			
			// Nava::printr($db->errorInfo());
		}
	}
	
	public static function sqlToRow ( $pSql = null ){
		$rs = self::sqlToArray($pSql);
		if ($rs){
			foreach ($rs as $row)
				return $row;
		}
		else 
			return null;
	}
	
	
	//ispravljanje greški
	
	public static function ispraviGreskuUser(){
		
		$suvaUsers = \Baseapp\Suva\Models\Users::find("type = 2 and n_avus_customer_number like 'P%' ");
		foreach ($suvaUsers as $user){
			
			$txtSql = "
				select 						
					c.number as customer_number
					,cid.taxnumber as customer_taxnumber
					,a.first_name
					,a.last_name
					,a.street
					,a.postcode
					,a.city
					,a.countryabr
					,a.contactperson
					,a.email
					,a.telareacode
					,a.telno

				from 
					customers c
						join g_customerinvoicedata cid on (c.id = cid.customerid)
						left join addressee a on (c.addresseeid = a.adressee_id)
				where 1=1
					and c.number = '$user->n_avus_customer_number'
			";
			
			//error_log ($txtSql);
			$rsAvusCustomer = self::sqlToArray($txtSql);
			
			
			
			foreach( $rsAvusCustomer as $avusCustomer ){

				$user->first_name = $avusCustomer['first_name'];
				$user->last_name = $avusCustomer['last_name'];
				$user->company_name = null;
				$user->type = 1;
				$success = $user->update();
				if (!$success){
					Nava:sysError ("Error updating user $user->id");
					foreach ( $user->getMessages() as $message ){
						Nava::sysError ($message);
					}
				}
				else {
					Nava::sysMessage ("Updated user $user->id");
				}
			}
		}
		
		
		
		
	}
	
	public static function ispraviGreskuUser1(){
		
		$suvaUsers = \Baseapp\Suva\Models\Users::find("type = 1 and n_avus_customer_number like 'K%' ");
		foreach ($suvaUsers as $user){
			
			$txtSql = "
				select 						
					c.number as customer_number
					,cid.taxnumber as customer_taxnumber
					,a.first_name
					,a.last_name
					,a.street
					,a.postcode
					,a.city
					,a.countryabr
					,a.contactperson
					,a.email
					,a.telareacode
					,a.telno

				from 
					customers c
						join g_customerinvoicedata cid on (c.id = cid.customerid)
						left join addressee a on (c.addresseeid = a.adressee_id)
				where 1=1
					and c.number = '$user->n_avus_customer_number'
			";
			
			//error_log ($txtSql);
			$rsAvusCustomer = self::sqlToArray($txtSql);
			
			
			
			foreach( $rsAvusCustomer as $avusCustomer ){
														
				$user->first_name = 'n/a';
				$user->last_name = $avusCustomer['last_name'];
				$user->company_name = $avusCustomer['last_name'];
				$user->type = 2;
				$success = $user->update();
				if (!$success){
					Nava:sysError ("Error updating user $user->id");
					foreach ( $user->getMessages() as $message ){
						Nava::sysError ($message);
					}
				}
				else {
					Nava::sysMessage ("Updated user $user->id");
				}
			}
		}
		
		
		
		
	}
	
}

/*  SQLOVI POMOĆNI

select * from [Oglasnik d_o_o_$AVUS-Header] where HEADERID = 1706997;
select * from [Oglasnik d_o_o_$AVUS-Invoice] where HEADERID = 1706997;
select * from [Oglasnik d_o_o_$AVUS-InvoiceRow] where HEADERID = 1706997;
select * from [Oglasnik d_o_o_$AVUS-Payment] where HEADERID = 1706997;
select * from [Oglasnik d_o_o_$AVUS-Revenue] where HEADERID = 1706997;
select * from [Oglasnik d_o_o_$AVUS-Address] where HEADERID = 1706997;




		$txtSql = "select top 1 * from addressee where ADRESSEE_ID = 9074";
		$txtSql = "select top 1 * from customers where number = 'K03725'"; //id=8584
		$txtSql = "select top 100 * from customersynonyms where customerid = 8584";
		$txtSql = "select top 100 * from g_customerinvoicedata where customerid = 8584";
		
		//$txtSql = "select top 1 * from addressee where ADRESSEE_ID = 19618";
		$txtSql = "select top 1 * from customers where number = 'K14649'"; //ID 19128
		//$txtSql = "select top 100 * from customersynonyms where customerid = 19128";
		//$txtSql = "select top 100 * from g_customerinvoicedata where customerid = 8584";
		
		$txtSql = "select top 5 * from customers where number = 'P001126135'"; // pasivni privatni, state = 2
		$txtSql = "select top 5 * from customers where number = 'P001244101'"; // aktivni privatni, state = 1
		
		$txtSql = "select top 1 * from adexport where adtext like '%'";
		//$txtSql = "select top 10 orderid, count(orderid) from adexport group by orderid order by count(orderid) desc";
		
		//$txtSql = "select top 10 adtext, count(orderid) from adexport group by adtext order by count(orderid) desc";
		$txtSql = "select top 1 * from adexport";


		select 
oib, 
COUNT(*),
max(last_name), min(last_name) 
,max(first_name) , min(first_name)
,	max(address) , min(address)
from users 
group by oib 
having count(*) > 1  
and 
	(
	max(last_name) <> min(last_name)
	or 
	max(first_name) <> min(first_name)
	or
	max(address) <> min(address)
	)
order by count(*) desc


//svi koji se ne smiju syncati s avusom
update users set n_avus_sync_status = 'no_sync_multi_oib' 

where 
n_avus_sync_status = 'unsynced'
and oib in (
select * from (
select 
oib
from users 
group by oib 
having count(*) > 1  
and 
	(
	max(last_name) <> min(last_name)
	or 
	max(first_name) <> min(first_name)
	or
	max(address) <> min(address)
	)

) as p1
)

//svi s duplim oibima koji se smiju syncati s avusom
select 
oib, 
COUNT(*),
max(n_avus_sync_status), min(n_avus_sync_status),
max(last_name), min(last_name) 
,max(first_name) , min(first_name)
,	max(address) , min(address)
from users 
group by oib 
having count(*) > 1  
and 
	(
	max(last_name) = min(last_name)
	and
	max(first_name) = min(first_name)
	and
	max(address) = min(address)
	)
order by count(*) desc


select * from users where n_avus_sync_status = 'no_sync_multi_oib' and (n_avus_customer_id is not null or n_avus_customer_number is not null)

select * from users where n_avus_sync_status = 'synced' and (n_avus_customer_id is not null and n_avus_customer_number is not null)
		
		
		
update ads set description = 'n/a'
where n_source = 'avus'
and 
(length(description) < 1)

update ads set title = 'n/a'
where n_source = 'avus'
and 
(length(title) < 1)
	

		select top 50000
				*
				,ID 
			from 
				g_orders 
			where 1=1
				and lastdate > '2016-11-30'
				and lastobjectid in (2,4)	
				

				
//BRISANJE DUPLIH Usera

DELETE FROM users
WHERE id IN (
	SELECT id
	FROM(
		SELECT id
		FROM users
		WHERE 
			id NOT IN (
				SELECT MAX(id) AS id
				FROM
								users
				WHERE
								n_avus_customer_id IS NOT NULL AND n_source = 'avus'
				GROUP BY
								n_avus_customer_id
				ORDER BY COUNT(id) DESC
		) AND n_source = 'avus'
	) p1
)
		
		
//KOLIKO IMA DUPLIH Usera
			select 
				count(id) as broj
				,max(id) as id
				,n_avus_customer_id 
			from
				users
			where
				n_avus_customer_id is not null
			group by
				n_avus_customer_id
			order by
				count(id) desc

				
//BRISANJE DUPLIH OGLASA
DELETE FROM ads
WHERE id IN (
	SELECT id
	FROM(
		SELECT id
		FROM ads
		WHERE 
			id NOT IN (
				SELECT MAX(id) AS id
				FROM
								ads
				WHERE
								n_avus_order_id IS NOT NULL AND n_source = 'avus'
				GROUP BY
								n_avus_order_id
				ORDER BY COUNT(id) DESC
		) AND n_source = 'avus'
	) p1
)	
//BRISANJE INSERTIONA NA IZBRISANIM OGLASIMA
DELETE FROM n_insertions_avus
WHERE id IN (
	SELECT id
	FROM(
		SELECT ins.id
		FROM 
			n_insertions_avus ins
				left join ads a on (ins.ad_id = a.id)
		where
			a.id is null		
	) p1
)				
			


GREŠKA VIDIT
avus ima 13 a ovaj ima 2
select * 
from 
	n_insertions_avus ins
		join ads a on (ins.ad_id = a.id)
where 
	a.n_avus_order_id = 
	
	
select * from g_orders where id = 13368344

			
*/

