<?php

namespace Baseapp\Suva\Library;


use Baseapp\Suva\Library\Nava;
use Baseapp\Suva\Library\libAppSetting;
use Phalcon\Logger;
use Phalcon\Logger\Adapter\File as FileAdapter;




class libCron{
 
	// NIKOLA $logger se nalazi u funkcijama self::fMsg i self::fError i trenutno loga u public/test.log jer u app/logs nemamo pristup 
	private static $loggerPath = 'libCron_application_log.log';
	

	
	public static function updateEveryMinute(){
		$old_error_log = ini_get('error_log');
		ini_set('error_log',ROOT_PATH."/logs/updateEveryMinute.log");

		
		//UGAŠENO PRIVREMENO!
		//return;
		
		
		
		/* Što se radi svaku minutu
		
			Updateaju svi nesunhronizirani oglasi
			
			prebacuju se plaćeni oglasi sa frontenda u status plaćeno
		
			refreshaju se neaktivni oglasi
			
			updateaju se dodani pushupi na frontendu
			
			
			Automatski se namišta deadline na issuima 
			
			aktiviraju i deaktiviraju se oglasi ovisno o trenutno aktivnim order itemima
			
			NAVISION:
				slanje na fiskalizaciju plaćenih faktura koje nisu fiskalizirane (npr. koje su plaćene 
				preko frontenda ili bankovnog izvoda)
				
				dohvaćanje navision broja fakture, JIR-a i ZIK-a za račune koji ih nemaju a trebaju ih imat
		
		*/
		
		
		//$this->view->pick("obrade/index");
		
		$arrLockedTables = \Baseapp\Suva\Library\libCron::fCheckLockedTables(); 
		if ( $arrLockedTables ) {
			Nava::sysMessage("Locked tables exist: ".print_r( $arrLockedTables, true ));
		}
		else{
			Nava::sysMessage("POČETAK UPDATE EVERY MINUTE:". date("Y-m-d h:i:sa"));
			error_log("pokretanje update every minute 1");
			// sinhroniziranje oglasa koji su došli s frotenda
			$_SESSION['_context']['zadnje_vrijeme'] = date('h:i:s');
			
			
			
			//ovo više ne treba, izmijenjen je Agimov kod
			// self::updateAllUnsyncedAds();  // glavna funkcija koja odraðuje sve nesyncane (aktivne i neaktivne)
			// error_log("updateAllUnsyncedAds".date('h:i:s'));
			
			
			//2.1.2017 - funkcija koja prebacuje u status plaćeno, zahtjev 2443
			//ovo je ptorebno nakon sinhronizacije, jer se plaćanje može izvršiti naknadno, ili ukucavanje kartice može trajati preko minutu, pa treba još jednom provjeriti postoje li računi plaćeni preko frontenda
			//\Baseapp\Suva\Library\libCron::fUpdateOrderStatusForOnlinePayment();
			//error_log("fUpdateOrderStatusForOnlinePayment".date('h:i:s'));
			
			//ako je oglas u trenutku predavanja neaktivan on se preskače 
			//i orderu se dodjeljuje status ad_inactive, pa se kasnije synca
			//Nava::sysMessage("updateOrdersItemsStatusAdInactive");
			//\Baseapp\Suva\Library\libCron::updateOrdersItemsStatusAdInactive();
			//error_log("updateOrdersItemsStatusAdInactive".date('h:i:s'));
			
			Nava::sysMessage("Updating pushups from frontend");
			\Baseapp\Suva\Library\libCron::updatePushupsFromFrontend();
			error_log("updatePushupsFromFrontend".date('h:i:s'));
			//\Baseapp\Suva\Library\libCron::fMsg("KRAJ:". date("Y-m-d h:i:sa"));
			
			// $this->view->pick("obrade/index");

			//ovde idu funkcije koje se updejtaju every minute
			//error_log("pokretanje update every minute 2");
			\Baseapp\Suva\Library\libCron::setIssueDeadline();
			error_log("setIssueDeadline".date('h:i:s'));
			
			
			//postavljanje aktivnih i neaktivnih oglasa za frontend
			\Baseapp\Suva\Library\libCron::setAdsInfoForFrontend();
			error_log("setAdsInfoForFrontend".date('h:i:s'));
			
			//slanje nefiskaliziranih u NAV
			//$orders = \Baseapp\Suva\Models\Orders::find("n_status in (4,5) and n_nav_sync_status = 'unsynced'");
			//foreach( $orders as $order ){
			//	\Baseapp\Suva\Library\libNavision::insertOrderInNav( $order );
			//}
			//error_log("insertOrderInNav".date('h:i:s'));
			
			//dohvat fiskaliziranih iz NAV - za sada ne radi jer u NAV nije automatizirano
			$orders = \Baseapp\Suva\Models\Orders::find("n_status = 5 and n_nav_sync_status in ('sent', 'retry')");
			foreach( $orders as $order ){
				//ovo se ne vrti dok ne proradi NAV fiskalizacija
				//\Baseapp\Suva\Library\libNavision::testFiscalization( $order->id );
			}
			error_log("dohvat fiskaliziranih iz NAV".date('h:i:s'));

			//OVO TREBA MAKNUT KAD SE RIJEŠI PROBLEM SA GREŠKOM KOD DODAVANJA SLIKE NA AD (JER SE TAD OGLAS AKTIVIRA I OSTANE AKTIVAN)
			Nava::runSql("
				update ads set active = 0 where active = 1 and id in (
					select * from (
					select
						a.id
					from 
						ads a 
							left join orders_items oi on (oi.ad_id = a.id)
					where 
						oi.id is null
					) p1
				)			
			
			");
			error_log("kraj update every minute".date('h:i:s'));
			Nava::sysMessage("KRAJ UPDATE EVERY MINUTE:". date("Y-m-d h:i:sa"));
			
		}	
		
		ini_set('error_log',$old_error_log);
	}	
	
	// -------------------------------------UPDATE EVERY MINUTE--------------------------------------
public static function setIssueDeadline(){
	global $dbOgla;

	error_log("set issue deadline");
	//AUTOMATSKO NAMJEŠTANJE DEADLINEA
	try {
		Nava::sysMessage("UPDATING ISSUE STATUS DEADLINE.."); 

		$txtOglaWhere = " where status = 'prima' and deadline < now()";
		$sqlCount = "select count(*) as broj from n_issues $txtOglaWhere";
		$count = Nava::sqlToArray( $sqlCount )[0]['broj'];
		Nava::sysMessage($sqlCount);
		//Nava::sysMessage("count: $count");
		if ( $count > 0 ){
			error_log("count veci od nula");
			$sqlUpdate = "update n_issues set status = 'deadline' $txtOglaWhere";
			Nava::sysMessage($sqlUpdate);
			Nava::runSql ($sqlUpdate);
			Nava::sysMessage("Status of $count issues updated successfully.");
		} elseif ( intval($count) === 0 ){
			Nava::sysMessage("0 issues found to update");
		}
	}
	catch(PDOException $e){
		Nava::sysMessage("Issue status update  failed: " . $e->getMessage());
	}

}

public static function setAdsInfoForFrontend( $pAdId = null ){
	
	/*
	
	
	1. deaktivirat aktivne online order iteme
	
	2. aktivirat neaktivne online order iteme
	
	3. 
	
	
	
	*/
	
	$sqlAdId = $pAdId > 0 ? " and a.id = $pAdId " : "";
	$sqlOiAdId = $pAdId > 0 ? " and oi.ad_id = $pAdId " : "";
	

	Nava::sysMessage("BORNA Activating ads and setting dates START:". date("Y-m-d h:i:s"));
	try {
		// Nava::sysMessage("Postavljanje n_publications_id na orders_items");
		// $txtSql = "
			// update orders_items oi
				// join n_products p on ( oi.n_products_id = p.id  and p.publication_id > 0)
			// set oi.n_publications_id = p.publication_id
			// where oi.n_publications_id is null
		// ";
		// Nava::runSql ($txtSql);
		
		Nava::sysMessage("BORNA Ispravljanje greske na adovima START:". date("Y-m-d h:i:s"));	
		//ispravljanje greški na adovima
		Nava::runSql("update ads set n_active_online_order_item_id = null where active= 0 and n_active_online_order_item_id > 0");
		
		$sqlJoin = "
			join orders_items oi on (
				a.n_active_online_order_item_id = oi.id
				and oi.n_expires_at < now()
				and a.active = 1
				$sqlOiAdId
				$sqlAdId
			)
		";
		
		
		//10.6.2017
		$sqlWhere = "
			where 1=1
				and a.expires_at < UNIX_TIMESTAMP(now())
				and a.active = 1
				$sqlAdId
		";
		$sqlJoin = "";
		
		$txtSql = "
			select a.id 
			from ads a
				$sqlJoin
				$sqlWhere
		";
		error_log($txtSql);
		$rs = Nava::sqlToArray ($txtSql);
		
		$txtIds = "Deaktiviranje isteklih oglasa: ";
		Nava::sysMessage("BORNA Deaktiviranje isteklih oglasa START:". date("Y-m-d h:i:s"));	
		if ($rs){
			foreach ($rs as $row){
				$txtIds = " ".$row['id'];
			}
		}
		Nava::sysMessage ($txtIds.PHP_EOL);
		
		$txtSql = "
			update ads a 
				$sqlJoin
				
			set 
				a.active = 0
				,a.n_active_online_order_item_id = null
			$sqlWhere

		";
		Nava::runSql( $txtSql );
		
		
		Nava::sysMessage("BORNA Deaktiviranje ad-ova kojima je moderacija negativna START:". date("Y-m-d h:i:s"));	
		//Nava::sysMessage("Deaktiviranje ad-ova kojima je moderacija negativna ");
		$txtSql = "
			select a.id from 	ads a
				join categories c on (a.category_id = c.id $sqlAdId)
					join categories_settings cs on (c.id = cs.category_id)
			where
				(
					(
						a.moderation = 'nok' 
						and
						cs.moderation_type = 'post'
					)
				or
					(
						a.moderation <> 'ok' 
						and
						cs.moderation_type = 'pre'
					)
				)
				and a.active = 1
				$sqlAdId
					";
		
		$rs = Nava::sqlToArray ($txtSql);
		error_log($txtSql);
		Nava::sysMessage("BORNA Deaktiviranje ad-ova kojima je moderacija negativna START:". date("Y-m-d h:i:s"));
		$txtIds = "Deaktiviranje ad-ova kojima je moderacija negativna: ";
		if ($rs){
			foreach ($rs as $row){
				$txtIds .= " ".$row['id'];
			}
		}
		Nava::sysMessage ($txtIds.PHP_EOL);
		
		
		$txtSql = "
			/*select * from 	ads a */
			update ads a 
				join categories c on (a.category_id = c.id)
					join categories_settings cs on (c.id = cs.category_id)
			set 
				a.active = 0 
			
			where
				(
					(
						a.moderation = 'nok' 
						and
						cs.moderation_type = 'post'
					)
				or
					(
						a.moderation <> 'ok' 
						and
						cs.moderation_type = 'pre'
					)
				)
				and a.active = 1
				$sqlAdId;
		";
		Nava::runSql( $txtSql );
		error_log($txtSql);
		
		
		

Nava::sysMessage("BORNA Dodavanjesort_date, published_at, expires_at adovima iz najkasnije objavljenog order itema koji je aktivan ". date("Y-m-d h:i:s"));

		
			$txtSql = "
			select a.id , p3.id as oi_id
			from ads a
join (
	select 
		oi.id
		, oi.ad_id
		, UNIX_TIMESTAMP(oi.n_first_published_at) as published_at
		, UNIX_TIMESTAMP(oi.n_expires_at) as expires_at
		, oi.product as oi_product
		, oi.product_id as oi_product_id
		,p.is_pushup_product 
		, p.priority_online
	from 
		orders_items oi 
			join (
				select 
					max(oi.id) as oi_id
				from orders_items oi 
					join orders o on (
						oi.order_id = o.id 
						and o.n_status in (2,4,5) 
						$sqlOiAdId
						)
 
					join (
						select 
							oi.ad_id
							,max(oi.n_first_published_at) as max_fpa
						from 
							orders_items oi
								join orders o on (
									oi.order_id = o.id 
									and o.n_status in (2,4,5) 
									$sqlOiAdId
									)
								join n_products p on (oi.n_products_id = p.id and p.is_pushup_product = 0)
						where 1=1 
							and oi.n_expires_at >= NOW() 
							and oi.n_first_published_at <= now() 
							and oi.n_is_published = 1 
							and oi.n_publications_id = 1
							$sqlOiAdId
						group by
							oi.ad_id
					) p1 on ( 
						oi.ad_id = p1.ad_id 
						and oi.n_first_published_at = p1.max_fpa 
						and oi.n_expires_at >= NOW()
						and oi.n_is_published = 1
						and oi.n_publications_id = 1
						$sqlOiAdId
						)
				group by p1.ad_id
			) p2 on (oi.id = p2.oi_id)
			join n_products p on (oi.n_products_id = p.id and p.is_pushup_product = 0)
			
	) p3 on (
		a.id = p3.ad_id 
		and (
			a.n_active_online_order_item_id <> p3.id 
			or 
			a.n_active_online_order_item_id <> p3.id is null
			)
		)
		$sqlAdId
	join 
		categories c on ( a.category_id = c.id )
			join categories_settings cs on (
				
				c.id = cs.category_id 
				$sqlAdId
				
				and 	
					(
						(
							a.moderation in ( 'ok' , 'waiting' )
							and
							cs.moderation_type = 'post'
						)
					or
						(
							a.moderation = 'ok' 
							and
							cs.moderation_type = 'pre'
						)
					)
					and a.manual_deactivation = 0
			)
					";
		$rs = Nava::sqlToArray ($txtSql);
		error_log($txtSql);
		$txtIds = "DODJELA AKTIVNOG ORDER ITEMA na oglase: ";
		if ($rs){
			foreach ($rs as $row){
				$txtIds .= " ".$row['id']."-".$row['oi_id'];
			}
		}
		Nava::sysMessage ($txtIds.PHP_EOL);
		Nava::sysMessage("BORNA DODJELA AKTIVNOG ORDER ITEMA na oglase ". date("Y-m-d h:i:s"));
	
Nava::sysMessage("korak 2: ". date("Y-m-d h:i:s"));		
$txtSql = "
update 
	ads a
join (
	select 
		oi.id
		, oi.ad_id
		, UNIX_TIMESTAMP(oi.n_first_published_at) as published_at
		, UNIX_TIMESTAMP(oi.n_expires_at) as expires_at
		, oi.product as oi_product
		, oi.product_id as oi_product_id
		,p.is_pushup_product 
		, p.priority_online
	from 
		orders_items oi 
			join (
				select 
					max(oi.id) as oi_id
				from orders_items oi 
					join orders o on (
						oi.order_id = o.id 
						and o.n_status in (2,4,5) 
						)
 
					join (
						select 
							oi.ad_id
							,max(oi.n_first_published_at) as max_fpa
						from 
							orders_items oi
								join orders o on (
									oi.order_id = o.id 
									and o.n_status in (2,4,5) 
									)
								join n_products p on (oi.n_products_id = p.id and p.is_pushup_product = 0)
						where 1=1 
							and oi.n_expires_at >= NOW() 
							and oi.n_first_published_at <= now() 
							and oi.n_is_published = 1 
							and oi.n_publications_id = 1
						group by
							oi.ad_id
					) p1 on ( 
						oi.ad_id = p1.ad_id 
						and oi.n_first_published_at = p1.max_fpa 
						and oi.n_expires_at >= NOW()
						and oi.n_is_published = 1
						and oi.n_publications_id = 1
						)
				group by p1.ad_id
			) p2 on (oi.id = p2.oi_id)
			join n_products p on (oi.n_products_id = p.id and p.is_pushup_product = 0)
			
	) p3 on (
		a.id = p3.ad_id 
		and (
			a.n_active_online_order_item_id <> p3.id 
			or 
			a.n_active_online_order_item_id <> p3.id is null
			)
		)
	join 
		categories c on ( a.category_id = c.id )
			join categories_settings cs on ( 
				c.id = cs.category_id 
				and 	
					(
						(
							a.moderation in ( 'ok' , 'waiting' )
							and
							cs.moderation_type = 'post'
						)
					or
						(
							a.moderation = 'ok' 
							and
							cs.moderation_type = 'pre'
						)
					)
					and a.manual_deactivation = 0
			)
set
     a.sort_date =  UNIX_TIMESTAMP(now())
	,a.published_at = p3.published_at
	,a.product_sort = p3.priority_online
	,a.expires_at =  p3.expires_at
	,a.online_product_id = p3.oi_product_id 
	,a.online_product = p3.oi_product
	,a.n_active_online_order_item_id = p3.id
	,a.active = 1
";
Nava::runSql( $txtSql );
error_log($txtSql);


Nava::sysMessage("Dodavanje pushup proizvoda");
Nava::sysMessage("BORNA Dodavanje pushup proizvoda: ". date("Y-m-d h:i:s"));

$txtSql = "
update 
	ads a
join (
	select 
		oi.id
		, oi.ad_id
		, UNIX_TIMESTAMP(oi.n_first_published_at) as published_at
	from 
		orders_items oi 
			join (
				select 
					max(oi.id) as oi_id
				from orders_items oi 
					join n_products p on ( oi.n_products_id = p.id and p.is_pushup_product = 1 )
					join orders o on (
						oi.order_id = o.id 
						and o.n_status in (2,4,5) 
						)
					where 1=1 
						and oi.n_first_published_at <= now() 
						and oi.n_is_published = 1 
						and oi.n_publications_id = 1
						and oi.n_pushup_applied_at is null
				group by oi.id
			) p2 on (oi.id = p2.oi_id)
	where 1=1 
		$sqlOiAdId
	) p3 on (a.id = p3.ad_id and a.n_active_online_order_item_id is not null)
set
	
     a.sort_date =  p3.published_at	
	 ,a.n_last_active_pushup_order_item_id = p3.id
";
Nava::runSql( $txtSql );
error_log($txtSql);



//postavljanje oznake na pushup proizvode da su objavljeni
Nava::sysMessage("BORNA postavljanje oznake na pushup proizvode da su objavljeni: ". date("Y-m-d h:i:s"));
$txtSql = "
update
	orders_items oi 
		join ads a on (
			oi.ad_id = a.id
			and oi.id = a.n_last_active_pushup_order_item_id
			and oi.n_pushup_applied_at is null
			$sqlOiAdId
			)
	set oi.n_pushup_applied_at = now();
";
Nava::runSql ($txtSql);
error_log($txtSql);
		
		Nava::sysMessage("BORNA Activating ads and setting dates FINISH:". date("Y-m-d h:i:s"));
		
	}
	catch(PDOException $e){
		self::fMsg( "Setting Ads Info for Frontend failed: " . $e->getMessage().PHP_EOL );
	}
}


public static function getJirZik(){
	//DOHVAĆANJE JIR I ZIK IZ NAVISIONA
	try {
		self::fMsg(PHP_EOL.PHP_EOL."SYNCING FISCAL INFO FROM NAV ".PHP_EOL); 
		$dbOgla = new PDO("mysql:host=127.0.0.1;dbname=oglasnik", 'oglasnik', 'PZQBgZLMgkGH9Mz');
		// set the PDO error mode to exception
		$dbOgla->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		self::fMsg("Connected to ogladev.oglasnik successfully"); 
		
		$dbNav = new PDO('dblib:host=195.245.255.22;port=1433;dbname=OGLASNIK_PROD', 'sa', 'Tassadar1337');
		// set the PDO error mode to exception
		$dbNav->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		self::fMsg("Connected successfully to NAV"); 
		
		
	// 	foreach($dbOgla->query("select * from orders where n_status in (4,5,9) and n_invoice_zik is NULL and n_invoice_jir is NULL and n_nav_sync_status = 'unsynced'") as $row){
		
		//na fiskalizaciju idu fakture, plaćene fakture i zakašnjelo plaćene fakture
		$txtSqlOgla = "select count(*) as broj from orders where n_status in (4,5,9) and n_nav_sync_status = 'unsynced'";
		$rsOgla = $dbOgla->query($txtSqlOgla);
		foreach($rsOgla as $rowOgla)
			self::fMsg("Found ".$rowOgla['broj']." orders to sync.");
		
		$txtSqlOgla = "select * from orders where n_status in (4,5,9) and n_nav_sync_status = 'unsynced'";
		$rsOgla = $dbOgla->query($txtSqlOgla);

		foreach($rsOgla as $rowOgla){
			//echo print_r($row, true);
			$txtSqlNav = 
				" select "
				." [External Document No_]" 
				.", [Sell-to Customer No_]"
				.", [Issuer Protection Code]" 
				.", [Unique Invoice No_]"
				.", [Fisc_ Wholesale Terminal]"
				.", [Fisc_ Location Code]"
				.", [Fisc_ Doc_ No_]"
				.", [Salesperson Code]"
				.", convert(varchar(30), [Posting DateTime],120) as date_time "
				.", convert(varchar(30), [Due Date],120) as due_date "
				." from [Oglasnik d_o_o_".'$'."Sales Invoice Header] "
				." where 1=1 "
				." and [External Document No_] = '90000".$rowOgla['id']."'";
			 
			$rsNav = $dbNav->query( $txtSqlNav );
			self::fMsg("Found ".count($rsNav)." Nav Invoices for order 90000".$rowOgla['id']);
			
			foreach ( $rsNav as $rowNav ) {
				//echo print_r($rowNav, true);
				$txtSqlOgla =  
					" UPDATE orders SET " 
						." n_invoice_zik = '".$rowNav['Issuer Protection Code']."'"
						.", n_invoice_jir = '".$rowNav['Unique Invoice No_']."'"
						.", n_nav_salesperson_code = '".$rowNav['Salesperson Code']."'"
						.", n_invoice_fiscal_number = '".$rowNav['Fisc_ Doc_ No_']."-".$rowNav['Fisc_ Location Code']."-".$rowNav['Fisc_ Wholesale Terminal']."'"
						.", n_invoice_date_time_issue = '".$rowNav['date_time']."'"
						.", n_invoice_due_date = '".$rowNav['due_date']."'"
						.", n_nav_sync_status = 'synced'"
					." WHERE id = ".$rowOgla['id'];
				self::fMsg("OGLA ROW UPDATE QUERY: $txtSqlOgla");
				$dbOgla->query($txtSqlOgla);
			}
		}
	}
	catch(PDOException $e){
		self::fError("Connection to ogladev.oglasnik failed: " . $e->getMessage());
	}
}

public static function syncPaymentMethods(){
	//SINHRONIZIRANJE TABLICE PAYMENT METHODS IZ NAVISIONA
	global $dbNav;
	global $dbOgla;


	try {
		self::fMsg("SYNCING PAYMENT METHODS FROM NAV ".PHP_EOL."Connecting 127.0.0.1 ..."); 
		//$dbOgla = new PDO("mysql:host=127.0.0.1;dbname=oglasnik", 'oglasnik', 'PZQBgZLMgkGH9Mz');
		$dbOgla = new PDO("mysql:host=192.168.0.113; dbname=oglasnik", 'oglasnik', 'igor2012');
		// set the PDO error mode to exception
		$dbOgla->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		self::fMsg("Connected to ogladev.oglasnik successfully"); 
		
		
		//zakomentirano
		$sqlNav = " select * from [Oglasnik d_o_o_".'$'."Payment Method] ";
		$rsNav =  $dbNav->query($sqlNav);
		
		foreach ( $rsNav as $rowNav ) {
			
			$txtSqlOgla = "select count(*) as broj from n_payment_methods where nav_id = '".$rowNav['Code']."'"; 
			self::fMsg($txtSqlOgla);
			$rsOgla = $dbOgla->query($txtSqlOgla);
			foreach ( $rsOgla as $recOgla ) {
				self::fMsg("RECOGLA:".print_r($recOgla, true));
				if ( $recOgla['broj'] == 0 ) {
					//TODO: ovone radi IGOR
					// //ako nema payment methoda s brojem nav_id
					$txtSqlOgla = 
						" insert into n_payment_methods ( slug, name, nav_id, is_fiscalized ) values( "
						." '".$rowNav['Code']."' "
						.", '".$rowNav['Description']."' "
						.", '".$rowNav['Code']."' " 
						.", ".$rowNav['Fisc_ Subject']
						." )";
					self::fMsg("INSERTING NEW PAYMENT METHOD: $txtSqlOgla");
					$dbOgla->query( $txtSqlOgla );
				}
			}
		}
		
	} 
	catch(PDOException $e){
		self::fMsg( "Syncing payment methods failed " . $e->getMessage());
	}

}

//============================================================================================================
// SQL , MSG funkcije




public static function fConnectOgla (){
	try {

		$oglaHost = "127.0.0.1";
		$oglaDb = "test_oglasnik2_6";
		$oglaUser = "oglasnik2";
		$oglaPassword = "kah6pha6Eejahki";
		
		self::fMsg("Connecting to $oglaHost ..");
		$db = new PDO("mysql:host=$oglaHost; dbname=$oglaDb", $oglaUser, $oglaPassword);
		$db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);// set the PDO error mode to exception 
		self::fMsg("Connected to $oglaHost successfully"); 
		return $db;
		
	}
	catch(PDOException $e){ 
		self::fMsg("SQL failed: " . $e->getMessage());
	}
}

public static function fConnectNav (){
	try {
		$navHost = "195.245.255.22";
		$navPort = "1433";
		$navDb = "OGLASNIK_PROD";
		$navUser = "sa";
		$navPassword = "Tassadar1337";
		
		self::fMsg("Connecting to $navHost ..");
		$db = new PDO("dblib:host=$navHost;port=$navPort;dbname=$navDb", $navUser, $navPassword );
		$db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);// set the PDO error mode to exception 
		self::fMsg("Connected to $navHost successfully"); 
		return $db;
	}
	catch(PDOException $e){ 
		self::fMsg("SQL failed: " . $e->getMessage());
	}
	

}

public static function fCheckLockedTables(){
	$rs = Nava::sqlToArray("show open tables from oglasnik_capella where In_use > 0");
	return $rs;
}	
	
	// --------------------------------------UPDATE ORDERS-----------------------------------------------
	
	


public static function updateAllActiveUnsyncedAds (){
	global $gDebugLevel;
	//Iæi po adovima koji nisu sincani i trenutno su aktivni.
	$txtSql = "select id from ads where active = 1  and n_frontend_sync_status = 'unsynced' ";
	$ads = Nava::sqlToArray($txtSql);
	self::fMsg("Query $txtSql found ".count($ads)." records. ");
	foreach ($ads as $ad){
		$adId = $ad['id'];
		self::fUpdateAdForSuva( $adId );
	}
}

public static function updateAllUnsyncedAds (){
	//Ici po adovima koji nisu syncani i trenutno su aktivni.
	global $gDebugLevel;
	$txtSql = "select id from ads where n_frontend_sync_status = 'unsynced' ";
	$ads = Nava::sqlToArray($txtSql);
	self::fMsg("Query $txtSql found ".count($ads)." records. ");
	foreach ($ads as $ad){
		$adId = $ad['id'];
		self::fUpdateAdForSuva( $adId, true );
	}
}

public static function updateOrdersItemsStatusAdInactive(){
	/*
		ako je oglas bio neaktivan u trenutku syncanja, imao je krive datume 
		published_at i expires_at, pa se nije moglo napraviti ispravan order item.
		Zato se order item stavlja u status ad_inactive. 
		Nakon što oglas postane aktivan ponovo se prolazi kroz te oglase i upisuje se pravi 
		first_published_at i expires_at
	
	*/
	
	global $gDebugLevel;
	$txtSql  =" 
		select oi.id 
		from orders_items oi 
			join ads a on (oi.ad_id = a.id and a.active = 1 and oi.n_frontend_sync_status = 'ad_inactive')
	";
	$ois = Nava::sqlToArray($txtSql);
	self::fMsg("Query $txtSql found ".count($ois)." records. ");
	if ($ois){
		foreach ($ois as $oi){
			$orderItemId = $oi['id'];
			self::fUpdateOrderItemForSuvaStatusAdInactive( $orderItemId );
		}
	}
}

public static function updatePushupsFromFrontend(){
	/*
		Ide kroz sve order iteme koji nisu synced imaju n_source frontend i proizvod pushup
		
	*/
	
	$txtSql = "select id, order_id, product from orders_items where n_source = 'frontend' and n_frontend_sync_status in ('unsynced', 'ad_inactive') and product_id = 'pushup'";
	$ois = Nava::sqlToArray($txtSql);
	self::fMsg("Query $txtSql found ".count($ois)." records. ");
	if ($ois){
		foreach ($ois as $oi){
			$orderItemId = $oi['id'];
			$orderId = $oi['order_id'];
			$productSerialized = $oi['product'];
			self::fUpdateOrderItemForSuva( $orderItemId, $productSerialized  );
			self::fUpdateOrderForSuva ( $orderId, $pShowMessages );
		}
	}
}

public static function updateAdTestUnsync ($pAdId = null ){
	global $gDebugLevel;
	if (!$pAdId ) return;
	
	Nava::runSql ("update orders_items set n_frontend_sync_status = 'unsynced' where ad_id = $pAdId ");
	Nava::runSql ("update orders set n_frontend_sync_status = 'unsynced' where id in (select order_id from  orders_items  where ad_id = $pAdId) ");
	Nava::runSql ("update ads set n_frontend_sync_status = 'unsynced' where id = $pAdId ");
	
	self::fUpdateAdForSuva($pAdId);
}

public static function updateAdTestDeleteOrders ($pAdId = null ){
	global $gDebugLevel;
	if (!$pAdId ) return;
	$orderId = Nava::sqlToArray("select max(order_id) as order_id from order_items where ad_id = $pAdId ")[0]['order_id'];
	if ( $orderId > 0 ) {
		Nava::runSql ("delete from orders where id = $orderId ");
		Nava::runSql ("delete from orders_items where order_id = $orderId ");
	}
	Nava::runSql ("update ads set n_frontend_sync_status = 'unsynced' where id = $pAdId ");
	
	self::fUpdateAdForSuva($pAdId);
}

public static function checkMissingSuvaProducts(){
	
	global $gDebugLevel;
	$ads = Nava::sqlToArray("select * from ads limit 10000");
	
	foreach ($ads as $ad){
		if ( $ad['online_product'] ){
			//self::fMsg("online product for ad ".$ad['id']);
			$product = self::fProcessSerializedProduct ( $ad['online_product'] );
			$suva = self::fFindSuvaProduct ( $product );
			if ( $suva ) self::fMsg ("online product for ad ".$ad['id'].",  Suva product ".$suva['id']);
			else self::fMsg ("online product for ad ".$ad['id'].",  Suva product NOT FOUND");
		}

		
		if ( $ad['offline_product'] ){
			//self::fMsg("offline product for ad ".$ad['id']);
			$product = self::fProcessSerializedProduct ( $ad['offline_product'] );
			$suva = self::fFindSuvaProduct ( $product );
			if ( $suva ) self::fMsg ("offline product for ad ".$ad['id'].",  Suva product ".$suva['id']);
			else self::fMsg ("offline product for ad ".$ad['id'].",  Suva product NOT FOUND");
		}
		
		// if ( $ad['offline_product'] ){
			// self::fMsg("offline product for ad ".$ad['id']);
			// $product = self::fProcessSerializedProduct ( $ad['offline_product'] );
			// //self::fMsg(print_r($product, true));
			// self::fFindSuvaProduct ( $product );
		// }

		
	}
	
	
}




//glavne funkcije
public static function fUpdateFrontendAdForSuva ( \Baseapp\Models\Ads $ad = null , $pShowMessages = false ){
	//sinhronizira se ad iz Agimove funkcije na predajaOglasaController (oba order itema već postoje)

	global $gDebugLevel;
	if (!$ad) return self::fError("updateFroontendAdForSuva: Ad not supplied");
	
	if ($ad->n_frontend_sync_status == 'synced') Nava::greska ("Frontend ad $ad->id is already synced");
	
	
	$paidOrderId = null;
	
	//syncanje online order itema
	$ois = \Baseapp\Suva\Models\OrdersItems::find("ad_id = $ad->id and product_id = '$ad->online_product_id' and n_frontend_sync_status = 'unsynced'");
	foreach($ois as $oi){
		if (  intval($oi->price) > 0 ) $paidOrderId = $oi->order_id;
		
		$status = self::fUpdateOrderItemForSuva ( $oi->id,  $ad->online_product, $pShowMessages );
		if ( !$status ) Nava::greska("Ad $ad->id - order item $o->id  nije updatean");
		if ($status){
			Nava::runSql("update ads set n_fe_online_order_item_id = $oi->id where id = $ad->id");
		}
		else {
			Nava::greska("Ad $ad->id - nije kreiran frontend order item  za online proizvod ");
		}
	}

	//syncanje offline order itema
	$ois = \Baseapp\Suva\Models\OrdersItems::find("ad_id = $ad->id and product_id = '$ad->offline_product_id' and n_frontend_sync_status = 'unsynced'");
	foreach($ois as $oi){
		if (  intval($oi->price) > 0 ) $paidOrderId = $oi->order_id;
		
		$status = self::fUpdateOrderItemForSuva ( $oi->id,  $ad->offline_product, $pShowMessages );
		if ( !$status ) Nava::greska("Ad $ad->id - order item $o->id  nije updatean");
		if ($status){
			Nava::runSql("update ads set n_fe_offline_order_item_id = $oi->id where id = $ad->id");
		}
		else {
			Nava::greska("Ad $ad->id - nije kreiran frontend order item  za online proizvod ");
		}
	}
		
	//Ako postoji plaćeni order updateanje ordera (tj. ako ima plaćenih oglasa - ako nema sve ide u free ads pa ne treba nista updateat)
	if ($paidOrderId){
		$status = self::fUpdateOrderForSuva ( $paidOrderId, $pShowMessages );
		if ( !$status ) Nava::greska ("Ad $ad->id - order $paidOrderId  nije updatean");
	}
}


public static function fUpdateAdForSuva ( $pAdId = null , $pShowMessages = false ){
	global $gDebugLevel;
	if (!$pAdId) return self::fError("updateAdForSuva: Ad id not supplied");
	//$txtSql = "select * from ads where id =  $pAdId ";
	//2.1.2017 - ako nema ni online ni offline proizvod onda je u postupku predaje i ne synca se
	$txtSql = "select * from ads where id =  $pAdId and ( online_product_id is not null or offline_product_id is not null ) ";
	
	
	$ads = Nava::sqlToArray($txtSql);
	if (count ($ads) != 1 ) {
		return self::fError("fUpdateAdsForSuva Query $txtSql found ".count($ads)." records. instead of 1 ", "SYS_ERR");
	}
	if ($pShowMessages) self::fMsg("Query $txtSql found ".count($ads)." records. ");
	foreach ($ads as $ad){
		
		$isError = false; 
		$ad_id = $ad['id'];
		self::fMsg("Ad $ad_id ..");
		// unsync all order items and orders belonging to this ad
		//ne unsyncaj one koji imaju status no_sync
		Nava::runSql("update orders_items set n_frontend_sync_status = 'unsynced' where ad_id =  $ad_id and n_frontend_sync_status <> 'no_sync'");
		
		//unsyncaj order koji pripada adu
		Nava::runSql("update orders set n_frontend_sync_status = 'unsynced' where n_frontend_sync_status <> 'no_sync' and id in (select order_id from  orders_items  where ad_id = $ad_id)");
		Nava::runSql("update ads set n_frontend_sync_status = 'unsynced' where id = $ad_id");
		
		
		$ois = Nava::sqlToArray("select * from orders_items where ad_id =  $ad_id and n_frontend_sync_status <> 'no_sync'");
		//ako ad nema order itema , znači da su dodani besplatni proizvodi, pa treba kreirat order iteme za besplatne proizvode
		if ( count($ois) == 0 ){
			if ($pShowMessages) self::fMsg ("Ad $ad_id , user:".$ad['user_id'].", nema order itema 1");
			
			//ako user nema free_ads dokument - napravit ga
			$orderId = self::fGetFreeAdsOrderId ( $ad['user_id'] );
			if ($pShowMessages) self::fMsg ("Ad $ad_id - Free Ads Order = $orderId 2");
			
			//ako ima online proizvod
			if ( $ad['online_product_id'] ){
				if ($pShowMessages) self::fMsg ("Ad $ad_id - ima online proizvod 3");
				//napravit FrontEnd order item
				$orderItemId = self::fCreateFrontendOrderItem ( $orderId , $ad['online_product'], $ad['id'] );
				
				//ažurirat suva proizvod
				if ( $orderItemId ){
					
					if ($pShowMessages) self::fMsg ("Ad $ad_id - kreiran frontend order item $orderItemId za online proizvod 4");
					$status = self::fUpdateOrderItemForSuva ( $orderItemId,  $ad['online_product'], $pShowMessages );
					if ( !$status ) self::fError ("Ad $ad_id - order item $orderItemId  nije updatean");
					$isError = $status == true ? $isError : true;
					 
					//2.1.2017 - dodati broj syncanog order itema u ads
					if ($status){
						Nava::runSql("update ads set n_fe_online_order_item_id = $orderItemId where id = $ad_id");
					}
				}
				else {
					self::fError ("Ad $ad_id - nije kreirankreiran frontend order item  za online proizvod ");
					$isError = true;
				}
			}

			//ako ima offline proizvod, isto kao i za online
			if ( $ad['offline_product_id'] ){
				if ($pShowMessages) self::fMsg ("Ad $ad_id - ima online proizvod 5");
				
				$orderItemId = self::fCreateFrontendOrderItem ( $orderId , $ad['offline_product'], $ad['id'] );
				if ( $orderItemId ){
					if ($pShowMessages) self::fMsg ("Ad $ad_id - kreiran frontend order item $orderItemId za offline proizvod 6");
					$status = self::fUpdateOrderItemForSuva ( $orderItemId,  $ad['offline_product'], $pShowMessages );
					if ( !$status ) self::fError ("Ad $ad_id - order item $orderItemId  nije updatean");
					$isError = $status == true ? $isError : true;
					
					//2.1.2017 - dodati broj syncanog order itema u ads
					if ($status){
						Nava::runSql("update ads set n_fe_offline_order_item_id = $orderItemId where id = $ad_id");
					}
				}
				else {
					self::fError ("Ad $ad_id - nije kreirankreiran frontend order item  za offline proizvod ");
					$isError = true;
				}
			}
			
			$status = self::fUpdateOrderForSuva ( $orderId, $pShowMessages );
			if ( !$status ) self::fError ("Ad $ad_id - order $orderId  nije updatean");
			$isError = $status == true ? $isError : true;
			
		}
		else {
			/*
				za svaki order item koji postoji 
					parsirat koji proizvod je naveden na OI - processSerializedProduct()
					naæi suva proizvod - findSuvaProduct()
					updateati order item - updateOrderItemForSuva()
			*/
			if ($pShowMessages) self::fMsg ("Ad $ad_id IMA order itema 8");
			foreach ( $ois as $oi ){
				$oiId = $oi['id'];
				$orderId = $oi['order_id'];
				if ( $oi['n_frontend_sync_status'] == 'unsynced' ) {
					if ($pShowMessages) self::fMsg ("Ad $ad_id - za order item $oiId  9");
					$status = self::fUpdateOrderItemForSuva( $oiId , $oi['product'], $pShowMessages );
					$isError = $status == true ? $isError : true;
				}
				else {
					self::fError ("Order Item $oiId found to be synced while belonging to an unsynced ad $ad_id. Skipped."); 
				}
				
			$status = self::fUpdateOrderForSuva ( $orderId, $pShowMessages);
			$isError = $status == true ? $isError : true;
			}
		}
		if ( $isError === false ) {
			Nava::runSql ("update ads set n_frontend_sync_status = 'synced' where id = $ad_id ");
			self::fMsg("  .. synced");
		}
		else self::fError ("Ad $ad_id not updated due to errors.");
		if ($pShowMessages) self::fMsg ("Ad $ad_id ------------------------------");
	}
}

public static function fUpdateOrderItemForSuvaStatusAdInactive( $pOrderItemId = null){
	/*
		Ako je ad na ovom order itemu aktivan a order item je imao status ad_inactive
		onda mu se upisuju first published at i expires at iz oglasa.
		Status ad_inactive dobije ako se synca s oglasom koji je trenutno neaktivan
		
		
		
		vraĆa: true ili false
	*/
	global $gDebugLevel;
	//PROVJERE PARAMETARA
	if ( !$pOrderItemId ) {
		self::fError ("Order Item ID not supplied to public static function fUpdateOrderItemForSuvaStatusAdInactive ", "SYS_ERR");
		return null;
	}
	
 
	//DOHVAT MODELA
	$oi = Nava::sqlToArray(" select * from orders_items where id = $pOrderItemId ")[0];
	$ad = Nava::sqlToArray("select * from ads where id = ".$oi['ad_id'])[0];
	
	
	if ( $ad['active'] == 1  && $oi['n_frontend_sync_status'] == 'ad_inactive' ){
	
		//PRIPREMA SQL-A
		$n_first_published_at = date("Y-m-d H:i:s", $ad['first_published_at']);
		$n_expires_at = date("Y-m-d H:i:s", $ad['expires_at']);

		
		$txtSql = "
			update orders_items set
				n_first_published_at = '$n_first_published_at' 
				, n_expires_at = '$n_expires_at' 
				, n_frontend_sync_status = 'synced'
			where id = $pOrderItemId
		";
		
		//self::fMsg ($txtSql);
		Nava::runSql($txtSql);
		self::fMsg("self::self::fUpdateOrderItemForSuvaStatusAdInactive odradjeno");
	}

}



public static function fUpdateOrderItemForSuva( $pOrderItemId = null, $pProductSerialized = null, $pShowMessages = false ){
	/*
	funkcija updateOrderItemForSuva (order_item.id)
		processSerializedProduct(oi.product)
		findSuvaProduct()
		usporediti cijenu na order itemu i cijenu na suva proizvodu
			ako nisu iste - error (OrdersItems, cijena nije ista kao i cijena na Productu No.11 )
		updateati na order itemu
			n_price
			n_publications_id
			n_products_id
			n_total
			n_total_with_tax tota- * tax_amount
			n_category_id
			n_is_active
			n_frontend_sync_status = synced
		ako je offline proizvod
			dodati slijedeæih insertiona koliko je broj objava
				iæ po svim issues, status = prima, publikacija je OGLA
		
		vraæa: true ili false
	*/

	global $gDebugLevel;
	
	//PROVJERE PARAMETARA
	if ( !$pOrderItemId ) {
		self::fError ("Order Item ID not supplied to public static function self::fUpdateOrderItemForSuva ", "SYS_ERR");
		return null;
	}
	
	if ( !$pProductSerialized ) {
		self::fError ("Serialized product not supplied to public static function self::fUpdateOrderItemForSuva ", "SYS_ERR");
		return null;
	}
 
	/*POSEBAN SLUÈAJ tamo di je 'n/a' bi trebalo bit syncano jer je nastalo iz SUVE, 
		ali kod masovnog unsyncanja se može dogodit sa ostane unsyncan, 
		pa ga samo treba vratit da je syncan */
	if ($pProductSerialized == 'n/a'  || $pProductSerialized == 'O:47:') {
		Nava::runSql ("update orders_items set n_frontend_sync_status = 'synced' where id = $pOrderItemId ");
		return true;
	}
 
	//DOHVAT MODELA
	$oi = Nava::sqlToArray(" select * from orders_items where id = $pOrderItemId ")[0];
	$order = Nava::sqlToArray("select * from orders where id = ".$oi['order_id'])[0];
	$ad = Nava::sqlToArray("select * from ads where id = ".$oi['ad_id'])[0];
	
	if ($pShowMessages) self::fMsg ("self::fUpdateOrderItemForSuva: Processing product from  order item  $pOrderItemId ");
	$frontendProduct = self::fProcessSerializedProduct ( $pProductSerialized );
	if ($pShowMessages) self::fMsg ( "Frontend product found:".print_r ($frontendProduct, true ) );
	$suvaProduct = self::fFindSuvaProduct ( $frontendProduct );
	if (!$suvaProduct ) {
		self::fError ("Suva product for orderitem $pOrderItemId not found.");
		return false;
	}
	
	$publication = Nava::sqlToArray("select * from n_publications where id = ".$suvaProduct['publication_id'])[0];
	
	//14.10.2016 - dogovor s Patricijom: Ako oglas nije aktivan, onda ostaje u statusu unsynced, i ponovo se pokušava sinhronizirati
	//PRIPREMA SQL-A
	$n_price = (((int)$oi['price']) / 100 + 0.0) * 0.8 ;//patricija - na frintendu su cijene s PDV-om
	$n_publications_id = $publication['id'];
	$n_products_id = $suvaProduct['id'];
	$n_total = (((int)$oi['total']) / 100) * 0.8;//patricija - na frintendu su cijene s PDV-om

	// //kod oglasa koji još nisu aktivni ne postoji first published at, samo created at i expires at
	// $n_first_published_at = date("Y-m-d H:i:s", ( $ad['first_published_at'] === null ? $ad['created_at'] : $ad['first_published_at'] ) );

	//2.1.2017 - dodaje se published_at a ne first_published_at (patricija)
	//kod oglasa koji još nisu aktivni ne postoji first published at, samo created at i expires at
	$n_first_published_at = date("Y-m-d H:i:s", ( $ad['first_published_at'] === null ? $ad['created_at'] : $ad['first_published_at'] ) );

	
	$n_expires_at = date("Y-m-d H:i:s", $ad['expires_at']);
	$n_total_with_tax = ((int)$oi['total']) / 100 + ((int)$oi['tax_amount']) / 100;
	$n_category_id = $ad['category_id'];
	$n_is_active = 1;
	$n_frontend_sync_status =   $ad['active'] == 1 ?  'synced' : 'ad_inactive';

	
	$txtSql = "
		update orders_items set
			n_price = $n_price
			, n_publications_id = $n_publications_id
			, n_expires_at = '$n_expires_at' 
			, n_products_id = $n_products_id
			, n_first_published_at = '$n_first_published_at' 
			, n_total = $n_total
			, n_total_with_tax = $n_total_with_tax
			, n_category_id = $n_category_id
			, n_is_active = $n_is_active
			, n_frontend_sync_status = '$n_frontend_sync_status'
		where id = $pOrderItemId
	";
	
	if ($pShowMessages) self::fMsg ($txtSql);
	Nava::runSql($txtSql);


	//INSERTIONI - ubacivanje insertiona za offline izdanje

	if ( $suvaProduct['is_online_product'] == 1 ) return true;
	//ISSUES - dohvat onoliko koliko je definirano na Suva productu
	//PRIPREMA SQL-A
	$publications_id = $publication['id'];
	$noIssues = $suvaProduct['days_published'] > 0 ?  $suvaProduct['days_published'] : 1 ;
	if ($pShowMessages) self::fMsg ("Suva proizvod ".$suvaProduct['id']. "ima  $noIssues objava.");
	
	$rs = Nava::sqlToArray( "select * from n_issues where status = 'prima' and publications_id = $publications_id limit $noIssues " );
	if ( count( $rs) != $noIssues ){
		self::fError (" wrong number of available issues found when creating insertions for order item $pOrderItemId, product ".$suvaProduct['id'].", publication $publications_id, ( $noIssues required and ".count( $rs)." available). MAYBE PRODUCT IS WRONGLY SET TO BE OFFLINE?" );
		return null;
	}
	$maxDate = 0;
	foreach ( $rs as $issue ){
		$dateIssue = strtotime($issue['date_published']);
		// self::fMsg("DATE ISSUE $dateIssue");
		if ( $dateIssue > $maxDate ) $maxDate = $dateIssue;
		// self::fMsg("IF DATE ISSUE > MAXDATE $maxDate");
		//PRIPREMA SQL-A
		$created_at = date ("Y-m-d h:I:s");
		$product_id = $suvaProduct['id'];
		$orders_items_id = $oi['id'];
		$issues_id = $issue['id'];

		$txtSql = "
			insert into n_insertions (
				created_at 
				, product_id 
				, orders_items_id 
				, issues_id 
			)
			values (
				'$created_at'
				, $product_id
				, $orders_items_id
				, $issues_id
			);
		";
		if ($pShowMessages) self::fMsg("ISSUE: $issues_id  SQL:".$txtSql);
		Nava::runSql($txtSql);
	}
	if ($maxDate > 0){
		$dateString = date("Y-m-d h:I:s", $maxDate);
		Nava::runSql("update orders_items set n_expires_at = '$dateString' where id = $pOrderItemId ");
		self::fMsg("Expires At for OrderItem $pOrderItemId update to $dateString");
	}
	
	return true;
	
}

public static function fUpdateOrderForSuva( $pOrderId ,$pShowMessages = false ){
	/*
	funkcija self::fUpdateOrderForSuva (order_id)
		azurirat polja
			n_status
			n_total
			n_frontend_sync_status
		vraæa: true ili false
	*/
	global $gDebugLevel;
	//PROVJERE PARAMETARA
	if ( !$pOrderId ) {
		self::fError ("Order ID not supplied to public static function self::fUpdateOrderForSuva ", "SYS_ERR");
		return false;
	}
	
	//DOHVAT MODELA
	$order = Nava::sqlToArray("select * from orders where id = $pOrderId")[0];
	$fiscal_location = Nava::sqlToArray("select * from n_fiscal_locations where nav_fiscal_location_id = 'ONL1' limit 1")[0];
	$sumOrdersItems = Nava::sqlToArray("
		select 
			SUM(n_total) as n_total
			,SUM(n_total_with_tax) as n_total_with_tax
		from 
			orders_items
		where order_id = $pOrderId
	")[0];
	
	//PRIPREMA SQL-A
	
	//n_status
	/*
		const STATUS_NEW = 1; // Order record created (when "submit order" is clicked), could be payed offline
		const STATUS_COMPLETED = 2; // Order fulfilled/finalized
		const STATUS_CANCELLED = 3; // Order cancelled by the customer (or admin)
		const STATUS_EXPIRED = 4; // Will probably have to be processed via cron or something...
	*/

	
	$os = $order['status'];
	$orderTotal = (int)$order['total'];
	if ( $os == 1  and $orderTotal == 0 ) $n_status = 2;
	elseif ( $os == 1 and $orderTotal > 0 ) $n_status = 3;
	elseif ( $os == 2 and $orderTotal == 0 ) $n_status = 2;
	elseif ( $os == 2 and $orderTotal > 0 ) $n_status = 5;
	//elseif ( $os == 3 ) $n_status = 5;
	elseif ( $os == 4 ) $n_status = 9;
	else {
		self::fError (" Order $pOrderId : status not recognized: ".$order['status'], "SYS_ERR" );
		return false;
	}

	if ($n_status == 3) {
		//prebačeno u ponudu, dodaje se n_quotation_date	
		$sql_n_quotation_date = " n_quotation_date = '".date("Y-m-d")."'";
	}
	else {
		$sql_n_quotation_date = "n_quotation_date = ''";
	}
	
	$operatorId = \Baseapp\Suva\Library\libAppSetting::get('frontend_default_operator_id');
	if ($n_status == 5) {		
		//plaćeno preko frontenda, poiva se funkcija koja dodjeljuje payment method
		$n_operator_id = " n_operator_id = $operatorId";
		$n_payment_date = "n_payment_date = now()";
	}
	else {
		$n_operator_id = "n_operator_id = ''";
		$n_payment_date = "n_payment_date = null";
	}
	
	//n_invoice_date_time_issue
	$sqlInvoiceDateTimeIssue = $n_status == 5 ? ', n_invoice_date_time_issue = now() ' : '';
	
	
	$n_fiscal_location_id = \Baseapp\Suva\Library\libAppSetting::get('fin_default_wspay_fiscal_location_id');
	$n_nav_sync_status = $n_status == 5 ? 'unsynced' : 'no_sync';	
	$n_total = $sumOrdersItems['n_total'] ? $sumOrdersItems['n_total'] : 0.00;
	$n_total_with_tax = $sumOrdersItems['n_total_with_tax'] ? $sumOrdersItems['n_total_with_tax'] : 0.00;
	$n_tax_amount = $n_total_with_tax - $n_total;
	// $n_fiscal_location_id = $fiscal_location['id'];
	
	//14.2.2017
	$n_sales_rep_id = \Baseapp\Suva\Library\libAppSetting::get('frontend_default_sales_rep_id ');
	$sqlSalesRepId = $n_sales_rep_id > 0 ? ", n_sales_rep_id = $n_sales_rep_id " : "";
	$pboPrefix = \Baseapp\Suva\Library\libAppSetting::get('fin_invoice_prefix');
	$pbo = (intval($pboPrefix) + intval($pOrderId));
	
	//11.04.2017
	$sqlPaymentMethodId = '';
	$frontendPaymentMethod = strlen($order['payment_method']) > 0 ? $order['payment_method'] : 'n/a';
	$pm = \Baseapp\Suva\Models\PaymentMethods::findFirst( "frontend_payment_method = '$frontendPaymentMethod'" );
	if ($pm){
		$sqlPaymentMethodId =  ", n_payment_methods_id = $pm->id "; 
	}

	
	
	
	$txtSql = "
		update orders set
			n_status = $n_status
			, n_total = $n_total
			, n_tax_amount = $n_tax_amount 
			, $sql_n_quotation_date
			, n_total_with_tax = $n_total_with_tax 
			, n_fiscal_location_id = $n_fiscal_location_id
			, $n_operator_id
			, $n_payment_date
			, n_frontend_sync_status = 'synced'
			, n_nav_sync_status = '$n_nav_sync_status'
			, n_pbo = '$pbo'			
			$sqlSalesRepId
			$sqlPaymentMethodId
			$sqlInvoiceDateTimeIssue
		where id = $pOrderId
	";
	//error_log($txtSql);
	// error_log("libCron: fUpdateOrderForSuva : updating order $pOrderId");
	if ($pShowMessages) self::fMsg($txtSql);
	Nava::runSql($txtSql);
	
	
	//ažurirati payment method, ako je plaćeno preko onlinea
	
	
	
	
	
	
	return true;
}

public static function fProcessSerializedProduct( $pSerialized = null ){
	//TODO - sredit greške ako ne naðe nešto obavezno, u ['error'];
	/* REGEX
		'/   - poæetak stringa
		/'   - kraj stringa
		/s'  - kraj stringa, ako se hoæe matchat i linebreakovi
		(aabb) - kad se nadje aabb neka ga se doda u novi red arraya
		
		
	*/
	global $gDebugLevel;
	if (strlen ($pSerialized ) < 10) return null;
	
	$r = array (
		'is_online_product' => null,
		'name' => null,
		'old_product_id' => null,
		'old_product_option' => null,
		'old_product_extras' => null,
		'old_product_extras_options' => null,
		'is_pushup' => false,
		'cost' => null,
		'error' => null
	);
	
	
	//echo PHP_EOL."TEXT:".PHP_EOL.$pSerialized.PHP_EOL.PHP_EOL.PHP_EOL;

	$rgxOnline = '/Baseapp\\\\Library\\\\Products\\\\([^\\\\]+)\\\\([^\\\\^"]+)"/'; //matchat  Online ili Offline, i onda ime proizvoda 
	//matchat  Online ili Offline, i onda ime proizvoda 
 
	$rgxOnline = '/Baseapp\\\\Library\\\\Products\\\\([^\\\\]+)\\\\([^\\\\^"]+)".*s:5:"...id";s:[0-9]{1,2}:"([^"]+)";/'; // 
	$rgxOnline = '/Baseapp\\\\Library\\\\Products\\\\([^\\\\]+)\\\\([^\\\\^"]+)":[0-9]{1,2}:{s:5:"...id";s:[0-9]{1,2}:"([^"]+)";/'; // 
	preg_match ($rgxOnline, $pSerialized, $arrMatch);
	if( array_key_exists (1, $arrMatch) ){
		if( $arrMatch[1] == 'Online' ) $r['is_online_product'] = true;
		elseif( $arrMatch[1] == 'Offline' ) $r['is_online_product'] = false;
	}
	$r['name'] = array_key_exists( 2, $arrMatch ) ? $arrMatch[2] : null;
	$r['old_product_id'] = array_key_exists (3, $arrMatch ) ? $arrMatch[3] : null;
	//var_dump ($arrMatch);

	
	if ( $r['name'] === 'Pushup' ){
		//self::fMsg ("tu sam".PHP_EOL);
		//traženje online_product_id  i spremanje u old_product_option
		
		//ako je Pushup onda se traži s:17:"online_product_id";s:16:"istaknuti-online"; i   s:7:" * cost";s:4:"1500";
		$rgxOption = '/s:17:"online_product_id";s:[0-9]{1,2}:"([^"]+).*s:7:"...cost";s:[0-9]{1,2}:"([0-9]+)";/s';
		
		preg_match ($rgxOption, $pSerialized, $arrMatch);
		$r['is_pushup'] = true;
		$r['old_product_id'] = 'push-up';
		$r['old_product_option'] = array_key_exists( 1, $arrMatch ) ? $arrMatch[1] : null;
		$r['cost'] = array_key_exists( 2, $arrMatch ) ? ((int)$arrMatch[2])/100 : null;
		//var_dump ($arrMatch);
	}
	else{
		//za sve koji nisu Pushup
		
		//traženje opcije
		$rgxOption = '/selected";s:[0-9]{1,2}:"([^"]+)"/'; // matchaj ;s:11:" * selected";s:7:"60 dana";s:18:"
		preg_match ($rgxOption, $pSerialized, $arrMatch);
		$r['old_product_option'] = array_key_exists ( 1 , $arrMatch )  ? $arrMatch[1] : null;
		//var_dump ($arrMatch);
		
		//traženje extras 
		$rgxExtras = '/selected_extras";a:[0-9]{1,2}:.s:[0-9]{1,2}:"([^"]+)";s:[0-9]{1,2}:"([^"]+)"/';//matchati: * selected_extras";a:1:{s:20:"Objava na naslovnici";s:6:"3 dana"
		preg_match ($rgxExtras, $pSerialized, $arrMatch);
		$r['old_product_extras'] = array_key_exists( 1, $arrMatch ) ? $arrMatch[1] : null;
		$r['old_product_extras_options'] = array_key_exists( 2, $arrMatch ) ? $arrMatch[2] : null;
		//var_dump ($arrMatch);
	}
	
	
	
	$errMsg = null;
	if ( $r['is_online_product'] === null ) $errMsg .=  "is_online_product, ";
	if (! $r['name'] ) $errMsg .=  "name, ";
	if (! $r['old_product_id'] ) $errMsg .=  "old_product_id, ";
	$errMsg = $errMsg ? "Could not find ".$errMsg : $errMsg ;
	if ( $r['is_pushup']  && !( $r['old_product_option'] && $r['cost'] ) ) $errMsg .= "  Pushup product parameters not found.";
	
	if ($errMsg) {
		$r['error'] = $errMsg;
		self::fError ($errMsg." for serialized product:".PHP_EOL.$pSerialized, "SYS_ERR");
	}

	
	
	return $r;
	
}

public static function fFindSuvaProduct ( $p ){
	global $gDebugLevel;
	if ( $p['old_product_id'] == null ) {
		self::fError("parameters supplied to self::fFindSuvaProduct do not contain old_product_id :".print_r($p, true), "SYS_ERR");
		return null;
	}
	
	$txt_old_product_id = " and old_product_id = '".$p['old_product_id']."' ";
	
	$txt_old_product_option = $p['old_product_option'] === null ? " and old_product_option is null " : " and old_product_option = '".$p['old_product_option']."' ";
	
	$txt_old_product_extras = $p['old_product_extras'] === null ? " and old_product_extras is null " : " and old_product_extras = '".$p['old_product_extras']."' ";
	
	$txt_old_product_extras_options = $p['old_product_extras_options'] === null ? " and old_product_extras_options is null " : " and old_product_extras_options = '".$p['old_product_extras_options']."' ";
	
	$txtSql = "select * from n_products where 1 = 1 $txt_old_product_id $txt_old_product_option $txt_old_product_extras $txt_old_product_extras_options ";
	
	$rs = Nava::sqlToArray($txtSql );
	
	self::fMsg($txtSql);
	
	//Ako upit ne naðe toèno 1 suva proizvod, greška
	if (count( $rs ) !== 1) {
		self::fError  ("error, query: $txtSql ".PHP_EOL."Found ".count( $rs )." records!!");
		return null;
	} 
	else return $rs[0];
	
}

public static function fGetFreeAdsOrderId ( $pUserId = null ){
	/*	
		funkcija getFreeAdsOrderId(ad.user_id)
			traži order sa n_statusom = 2  po useru
			ako ga naðe vraæa id
			ako ne naðe, kreira ga
				naæ kod u Navi ili u Actions za kreiranje carta
	*/
	
	global $gDebugLevel;
	$txtSql = " select * from orders where n_status = 2 and user_id = $pUserId order by id desc";
	$rs = Nava::sqlToArray( $txtSql );

	$jePostoji = count($rs) > 0 ? true : false;

	if ($jePostoji) return $rs[0]['id'];
	else {
		//kreiranje nove liste free ads
		Nava::runSql("insert into orders (status, total, created_at, modified_at) values (1,0, now(), now() )");
		
		$newId = Nava::sqlToArray("select max(id) as id from orders ")[0]['id'];
		
		
		
		$txtSql =  " update orders set "
		  ." user_id = $pUserId "
		  .", n_status = 2 "
		  .", status = 1 "
		  .", n_source = 'frontend_sync' "
		  
		  ." where id = $newId ";
		
		//self::fMsg ($txtSql);
		Nava::runSql($txtSql);
		return $newId;
	}
	
}
	
public static function fCreateFrontendOrderItem( $pOrderId = null, $pProductSerialized = null, $pAdId = null ){
	/*
	funkcija createFrontendOrderItem(ad.id, cart_id, productSerialized)
		naæi kod u Navi
		na kraju fUpdateOrderTotals
	
		vraæa: orderItemId ili null
	*/

	global $gDebugLevel;
	global $dbOgla;
	//PROVJERE PARAMETARA
	if ( !$pOrderId ) {
		self::fError ("Order ID not supplied to public static function fCreateFrontendOrderItem ", "SYS_ERR");
		return null;
	}
	
	if ( !$pProductSerialized ) {
		self::fError ("Serialized product not supplied to public static function fCreateFrontendOrderItem ", "SYS_ERR");
		return null;
	}
	
	if ( !$pAdId) {
		self::fError ("Ad ID not supplied to public static function fCreateFrontendOrderItem ", "SYS_ERR");
		return null;
	}

	//DOHVAT MODELA
	//self::fMsg ("fCreateFrontendOrderItem: Processing product from  ad $pAdId ");
	$frontendProduct = self::fProcessSerializedProduct ( $pProductSerialized );
	if ( $frontendProduct['error'] ) {
		self::fError ( $frontendProduct['error'] );
		return null;
	}
	$suvaProduct = self::fFindSuvaProduct( $frontendProduct );
	
	$order = Nava::sqlToArray(" select * from orders where id = $pOrderId ")[0];
	$ad = Nava::sqlToArray("select * from ads where id = $pAdId ")[0];
	
	//PRIPREMA SQL-A
	$adId = $ad['id'];
	$oldProductId = $frontendProduct['old_product_id'];
	
	//ovo ne radi , treba vidit šta vraća
	$productSerializedQuoted = "'".mysql_real_escape_string ( $pProductSerialized )."'";
	//error_log ("product serialized".$pProductSerialized);
	//error_log ("productSerializedqouted".$productSerializedQuoted);
	
	//ovo je radilo na updateorders.php kako je na updateordera
	//$productSerializedQuoted = $dbOgla->quote( $pProductSerialized );  //funkcija na dbOgla - treba global dbOgla
	
	//ovo samja pokušao ovdje pa ne radi
	//$productSerializedQuoted = Nava::dbQuote( $pProductSerialized ); 
	
	/*
		test: napravit oglas preko frontenda , odabrat plaćeni proizvod, plaćanje virmanom
		 treba se pojaviti ponuda, a u ordr itemu u polju online_product treba biti json
	
	*/
	
	// echo PHP_EOL."Quoted:".$productSerializedQuoted.PHP_EOL;
	// die();
	$title = $frontendProduct['old_product_id'] . "- created by batch";
	$price = (int)$suvaProduct['unit_price'] * 100;
	$qty = 1;
	$total = $price;
	$taxRate = 0.25;
	$taxAmount = (int)($taxRate * $total);
	$nSource = 'frontend_sync';
	
	$txtSql = "
		insert into orders_items ( 
			order_id 
			,ad_id 
			,product_id 
			,product 
			,title 
			,price 
			,qty 
			,total 
			,tax_rate 
			,tax_amount
			,n_source
			)  
		values (
			$pOrderId
			,$pAdId
			,'$oldProductId'
			,$productSerializedQuoted
			,'$title'
			,'$price'
			,'$qty'
			,'$total'
			,'$taxRate'
			,'$taxAmount'
			,'$nSource'
		)
	";
	
	//echo PHP_EOL."SQL Quoted:".$txtSql.PHP_EOL;
	Nava::runSql($txtSql);
	
	
	$newId = Nava::sqlToArray("select max(id) as id from orders_items ")[0]['id'];
	return $newId;
}


	public static function fUpdateOrderStatusForOnlinePayment(){
		/*
			traži ordere koji nisu u statusu paid a pripadaju oglasima kojima je latest payment state 5
		
		*/
		global $gDebugLevel;
		$operatorId = \Baseapp\Suva\Library\libAppSetting::get('fin_default_wspay_operator_id');
		$fiscalLocationId = \Baseapp\Suva\Library\libAppSetting::get('fin_default_wspay_fiscal_location_id');
		
		$orders = \Baseapp\Suva\Models\Orders::find("status = 2 and n_status in ( 3,4 ) and n_frontend_sync_status = 'synced' and n_source = 'frontend'");

		if ( !$operatorId || !$fiscalLocationId || !$orders ){
			//Nava::greska ("")
			self::fError("nije dohvaćen operatorId, FiscalLocationId ili Orders ");
			return false;
		}
		
			
		
		$navSyncStatus = 'unsynced';
		foreach ($orders as $order){
			$frontendPaymentMethod = strlen($order->payment_method) > 0 ? $order->payment_method : 'n/a';
			$pm = \Baseapp\Suva\Models\PaymentMethods::findFirst( "frontend_payment_method = '$frontendPaymentMethod'" );
			if (!$pm){
				self::fError ("nije dohvaćen frontend paymnet method na orderu $order->id");
				return false;
			}
			
				
			$txtSql = "
				update orders set
					n_status = 5
					,n_payment_methods_id = $pm->id
					,n_operator_id = $operatorId
					,n_fiscal_location_id = $fiscalLocationId
					,n_invoice_date_time_issue = now()
					,n_payment_date = now()
					,n_nav_sync_status = '$navSyncStatus'
				where id = $order->id
			";
			// error_log($txtSql);
			self::fMsg( $txtSql );
			Nava::runSql ( $txtSql );
			
		}
}


//pomoćne funkcije
public static function fMsg ( $pMessage , $pTitle = "" ){
	
	$msg = is_array($pMessage) ? print_r($pMessage, true) : $pMessage;
	$title = $pTitle ? $pTitle.":".PHP_EOL : "";
	
	//echo PHP_EOL.$title.$msg;
	error_log($title.$msg);
	Nava::poruka($title.$msg);
	//$logger =  new FileAdapter(self::$loggerPath);
	//$logger->info($title.$msg);
}



public static function fError ( $pMessage = "", $pType = "ERROR", $pModel = "", $pModelId = null ){

/*
funkcija error( pModel = (Ads, Orders,OrdersItems), ModelId, Message)
ubaci u tablicu audit
	user_id = 120096
	username = igornazor
	modelname
		Baseapp\Models\Ads
		Baseapp\Models\Orders
		Baseapp\Models\OrdersItems
	model_pk_val - ModelId
	message - message
	ip - 127.0.0.1
	
echo na ekran
*/
	global $gDebugLevel;
	if (!$pMessage ) return false;

	$message = "FE SYNC $pType : ".$pMessage;
	$txtSql = "
	insert into audit (user_id, username, message, ip, type, created_at)
	values (
	120096
	,'igornazor'
	,'$message'
	,'127.0.0.1'
	,'S'
	,'".date("Y-m-d h:I:s")."'
	)
	";

	//ZA SADA NE U BAZU
	//Nava::runSql($txtSql);

	//echo PHP_EOL.date("Y-m-d h:I:s")." ".$message;
	error_log("libCron ERROR: $message");
	Nava::greska ( "libCron ERROR: $message" );
	//$logger =  new FileAdapter(self::$loggerPath);
	//$logger->error("libCron ERROR: $message");
	
}








	
	
}