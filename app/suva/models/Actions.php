<?php

namespace Baseapp\Suva\Models;

/**
 * Publications Model
 */
class Actions extends BaseModelBlamable
{	
	
    public function initialize()
    {
        $this->setSource("n_actions");
    }

        public function getSource()
    {
        return 'n_actions';
    }


    /**
     * Add a new Publication
     *
     * @param \Phalcon\Http\RequestInterface $request
     *
     * @return $this|\Phalcon\Mvc\Model\MessageInterface[]|\Phalcon\Validation\Message\Group|void
     */
    public function add_new($request)
    {
        $this->initialize_model_with_post($request);

            if (true === $this->create()) {
                return $this;
            } else {
                \Baseapp\Bootstrap::log($this->getMessages());
                return $this->getMessages();
            }
    }

    /**
     * Save Publication data method
     *
     * @param \Phalcon\Http\RequestInterface $request
     *
     * @return $this|\Phalcon\Mvc\Model\MessageInterface[]|\Phalcon\Validation\Message\Group|void
     */
    public function save_changes($request)
    {

        $this->initialize_model_with_post($request);

        if (count($messages)) {
            return $validation->getMessages();
        } else {
            if (true === $this->update()) {
                return $this;
            } else {
                \Baseapp\Bootstrap::log($this->getMessages());
                return $this->getMessages();
            }
        }
    }
	
	public function buttonHtml($p1 = null){
		$strLink = strlen(trim($this->html_link)) > 0 ? $this->html_link : "/suva/actions/$this->name/";
		$strHtml = "<a href ='$strLink$p1' ";
		
		if ($this->html_attributes > "") $strHtml = $strHtml.$this->html_attributes;
		else $strHtml = $strHtml." class='btn btn-default btn-xs' ";
		
		$strHtml = $strHtml." >".$this->description."</a>";
		return ($strHtml);
	} 
	
	

	public function _confirm () { 
	if (confirm("Izvršiti?") == true) 
		{ return true; }
	else { return false; }

	}
	
	
	
	public function buttonHtml_submitViaActionInputOnForm($p = Array()){
		
		$isAjax = false;
		if ( array_key_exists('is_ajax',$p) )
			$isAjax = $p['is_ajax'];
			$reqConfirmation = array_key_exists('require_confirmation', $p) ? $p['require_confirmation'] : null; //Dodano N
			//$reqConfirmation = $p['require_confirmation'];
			//error_log("ACTION NIKOLA".$reqConfirmation);
		
		if ( $isAjax && array_key_exists('action_url',$p) && array_key_exists('action_window_id',$p) && $reqConfirmation == 1 ) 
			//$izvršiti = mysql_real_escape_string($_GET['Izvršiti']); //Dodano N --> unexpected '&' 
			$strHtml = "
				<span 
					
					class = 'btn btn-danger'
					style = 'text-align:right; height:25px; margin-bottom:5px;' 
					
					onclick = "
						.'"'
						."if (confirm('Izvrsiti?')){"
						."$('#".$p['action_input_id']."').val('".$this->name."');"
						." ajaxSubmit( '".$p['action_url']."' , '".$p['action_window_id']."' , '_form_".$p['action_window_id']."' );}"
						.'"'

					.">
					$this->description
				</span>
			";
		elseif ( $reqConfirmation == 1 )
			$strHtml = "
				<button 
					type = 'submit' 
					name = 'save'
					class = 'btn btn-primary'
					style = 'text-align:right; height:25px; margin-bottom:5px;' 
					onclick = "
						.'"'
						."if (confirm('". Izvršiti.'?' ."')){"
						."$('#".$p['action_input_id']."').val('".$this->name."');"
						." ajaxSubmit( '".$p['action_url']."' , '".$p['action_window_id']."' , '_form_".$p['action_window_id']."' );}"
						.'"'
					.">
					$this->description
				</button>
			";
		if ( $isAjax && array_key_exists('action_url',$p) && array_key_exists('action_window_id',$p) && $reqConfirmation == 0 ) 
			$strHtml = "
				<span 
					class = 'btn btn-primary'
					style = 'text-align:right; height:25px; margin-bottom:5px;' 
					onclick = "
						.'"'
						
						."$('#".$p['action_input_id']."').val('".$this->name."');"
						." ajaxSubmit( '".$p['action_url']."' , '".$p['action_window_id']."' , '_form_".$p['action_window_id']."' );"
						.'"'
					.">
					$this->description
				</span>
			";
		elseif ( $reqConfirmation == 0 )
			$strHtml = "
				<button 
					type = 'submit' 
					name = 'save'
					class = 'btn btn-primary'
					style = 'text-align:right; height:25px; margin-bottom:5px;' 
					onclick = "
						.'"'
						
						."$('#".$p['action_input_id']."').val('".$this->name."');"
						." ajaxSubmit( '".$p['action_url']."' , '".$p['action_window_id']."' , '_form_".$p['action_window_id']."' );"
						.'"'
					.">
					$this->description
				</button>
			";	
	
		
		return $strHtml;
	}  
	
}