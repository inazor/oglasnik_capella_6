<?php

namespace Baseapp\Suva\Models;

// class AdsMedia extends BaseModel
class AdsMedia extends \Baseapp\Models\AdsMedia
{
	use traitBaseSuvaModel;
	
//DODANO

	//veze many2one
	public function Ad(){
		$id = $this->ad_id > 0 ? $this->ad_id : -1;
        return \Baseapp\Suva\Models\Ads::findFirst($id);
    }

	public function Media(){
		$id = $this->media_id > 0 ? $this->media_id : -1;
        return \Baseapp\Suva\Models\Media::findFirst($id);
    }

	
}
