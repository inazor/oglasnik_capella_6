<?php

namespace Baseapp\Suva\Models;

// use Baseapp\Bootstrap;
// use Phalcon\Mvc\Model\Query\Builder;
// use Baseapp\Library\Validations\CmsArticles as CmsArticlesValidations;
// use Baseapp\Traits\MediaCommonMethods;

/**
 * Cms articles (magazine) Model
 */
class CmsArticlesMagazine extends \Baseapp\Models\CmsArticlesMagazine
{
	use traitBaseSuvaModel;
    /**
     * CmsArticlesMagazine initialize
     */
    // public function initialize()
    // {
        // parent::initialize();
        // $this->setSource('cms_articles');
    // }

    // /**
     // * Get latest cms articles
     // *
     // * @param int $amount Number of "latest" cms articles to return
     // * @param $thumb_style_details
     // *
     // * @return array|null
     // */
    // public static function getLatest($amount = 4, $thumb_style_details = 'CMS-300x200')
    // {
        // $rootCategory = CmsCategories::findFirst(array(
            // 'conditions' => 'type = :magazineType:',
            // 'bind' => array(
                // 'magazineType' => CmsCategories::TYPE_MAGAZINE
            // ),
            // 'order' => 'lft ASC'
        // ));

        // // Prevents query cache from being invalidated on every single query due to seconds being used
        // // (effectively removing seconds from the equation for the duration of the calendar day)
        // $today = getdate();
        // $today_date_sql_short = $today['year'] . '-' . $today['mon'] . '-' . $today['mday'];
        // $today_date_sql = $today_date_sql_short . ' 23:59:59';

        // $builder = new Builder();

        // if ($rootCategory) {
            // $builder->columns(array(
                // 'article.*',
                // 'IF(article.featured = 1 AND article.featured_start <= "' . $today_date_sql . '" AND article.featured_end >= "' . $today_date_sql . '", 1, 0) as featured'
            // ));
            // $builder->addFrom('Baseapp\Suva\Models\CmsArticlesMagazine', 'article');
            // $builder->leftJoin('Baseapp\Suva\Models\CmsCategories', 'article.category_id = category.id', 'category');
            // $builder->where(
                // 'article.active = 1 AND article.publish_date <= :publish_date:',
                // array(
                    // 'publish_date' => $today_date_sql
                // )
            // );
            // $builder->andWhere(
                // '(category.lft >= :cms_lft: AND category.rght <= :cms_rght:) OR (article.featured = 1 AND article.featured_start <= :today_date_sql_fs: AND article.featured_end >= :today_date_sql_fe:)',
                // array(
                    // 'cms_lft'           => $rootCategory->lft,
                    // 'cms_rght'          => $rootCategory->rght,
                    // 'today_date_sql_fs' => $today_date_sql,
                    // 'today_date_sql_fe' => $today_date_sql
                // )
            // );
            // $builder->groupBy('article.id');
            // $builder->orderBy('featured DESC, article.publish_date DESC');
        // } else {
            // $builder->columns(array(
                // 'article.*',
                // '1 as featured'
            // ));
            // $builder->addFrom('Baseapp\Suva\Models\CmsArticlesMagazine', 'article');
            // $builder->where(
                // 'article.active = 1 AND article.publish_date <= :publish_date:',
                // array(
                    // 'publish_date' => $today_date_sql
                // )
            // );
            // $builder->andWhere(
                // 'article.featured = 1 AND article.featured_start <= :today_date_sql_fs: AND article.featured_end >= :today_date_sql_fe:',
                // array(
                    // 'today_date_sql_fs' => $today_date_sql,
                    // 'today_date_sql_fe' => $today_date_sql
                // )
            // );
            // $builder->orderBy('article.publish_date DESC');
        // }

        // $builder->limit($amount);
        // $results = $builder->getQuery()->execute();

        // if (count($results)) {
            // return self::getEnrichedFrontendBasicResultsArray($results, $thumb_style_details);
        // }

        // return null;
    // }

    // /**
     // * Get simmilar articles (tagged with same tags, same category)
     // *
     // * @param int $amount Number of articles to return
     // *
     // * @return array|null
     // */
    // public function getSimmilar($amount = 4)
    // {
        // $articleTags = isset($this->raw_tags) && trim($this->raw_tags) ? (strpos(trim($this->raw_tags), ',') !== false ? explode(',', $this->raw_tags) : array($this->raw_tags)) : null;
        // $categoryID  = (int) $this->category_id;

        // // Prevents query cache from being invalidated on every single query due to seconds being used
        // // (effectively removing seconds from the equation for the duration of the calendar day)
        // $today = getdate();
        // $today_date_sql_short = $today['year'] . '-' . $today['mon'] . '-' . $today['mday'];
        // $today_date_sql = $today_date_sql_short . ' 23:59:59';

        // $columns = array(
            // 'article.*'
        // );
        // $orderBy = 'article.publish_date DESC';

        // $builder = new Builder();
        // $builder->addFrom('Baseapp\Suva\Models\CmsArticlesMagazine', 'article');
        // $builder->where(
            // 'article.active = 1 AND article.publish_date <= :publish_date: AND NOT(article.id = :self_id:)',
            // array(
                // 'publish_date' => $today_date_sql,
                // 'self_id'      => $this->id
            // )
        // );
        // if ($articleTags) {
            // $tagColumn = array();
            // $tagWhere = array();
            // foreach ($articleTags as $tag) {
                // if ($tag = trim($tag)) {
                    // $tagColumn[] = "(article.raw_tags LIKE '%$tag%')";
                    // $tagWhere[] = "article.raw_tags LIKE '%$tag%'";
                // }
            // }
            // if (count($tagColumn)) {
                // $columns[] = '(' . implode(' + ', $tagColumn) . ') as hits';
                // $tagWhere[] = "article.category_id = :categoryID:";
                // $builder->andWhere(
                    // implode(' OR ', $tagWhere),
                    // array(
                        // 'categoryID' => $categoryID
                    // )
                // );
                // $orderBy = 'hits DESC, article.publish_date DESC';
            // }
        // } else {
            // $builder->andWhere(
                // 'article.category_id = :categoryID:',
                // array(
                    // 'categoryID' => $categoryID
                // )
            // );
        // }
        // $builder->orderBy($orderBy);
        // $builder->limit($amount);
        // $builder->columns($columns);

        // $results = $builder->getQuery()->execute();

        // if (count($results)) {
            // return self::getEnrichedFrontendBasicResultsArray($results);
        // }

        // return null;
    // }

}
