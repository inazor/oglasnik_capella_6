<?php

namespace Baseapp\Suva\Models;
use Baseapp\Suva\Library\Nava;
/**
 * Publications Model
 */
class Issues extends BaseModelBlamable
{
    public function initialize()
    {
        $this->setSource("n_issues");
    }

        public function getSource()
    {
        return 'n_issues';
    }

	public function n_before_save( $request = null, $p = null ){
		//Nava::poruka("n_before_save");
		//NAKON SNIMANJA ISSUEA treba ponovo refreshati statuse, ako se je promijenio deadline
		if ($request){
			$r_deadline = $request->hasPost('deadline') ? $request->getPost('deadline') : null;
		} elseif ($p) {
			$r_deadline =  array_key_exists('deadline', $p ) ? $p['deadline'] : null;
		} else {
			$r_deadline = $this->deadline;
		}
		
		
		$now = date("Y-m-d H:i:s");
		//$r_deadline = strtotime($r_deadline);
		//Nava::poruka("DEADLIN $r_deadline");
		//Nava::poruka("NOW $now");
		//Nava::poruka("status $this->status");
		if ( $r_deadline < $now  && $this->status == 'prima' ){
			//Nava::poruka("status deadline");
			$this->status = 'deadline';
		}
		if ( $r_deadline > $now  && $this->status == 'deadline' ){
			//Nava::poruka("status prima");
			$this->status = 'prima';
		}
		
	}

	
    /**
     * Add a new Publication
     *
     * @param \Phalcon\Http\RequestInterface $request
     *
     * @return $this|\Phalcon\Mvc\Model\MessageInterface[]|\Phalcon\Validation\Message\Group|void
     */
    public function add_new($request)
    {
        $this->initialize_model_with_post($request);

            if (true === $this->create()) {
                return $this;
            } else {
                \Baseapp\Bootstrap::log($this->getMessages());
                return $this->getMessages();
            }
    }

    /**
     * Save Publication data method
     *
     * @param \Phalcon\Http\RequestInterface $request
     *
     * @return $this|\Phalcon\Mvc\Model\MessageInterface[]|\Phalcon\Validation\Message\Group|void
     */
    public function save_changes($request)
    {

        $this->initialize_model_with_post($request);

        if (count($messages)) {
            return $validation->getMessages();
        } else {
            if (true === $this->update()) {
                return $this;
            } else {
                \Baseapp\Bootstrap::log($this->getMessages());
                return $this->getMessages();
            }
        }
    }
	

	public function getAdIdsByCategoryMapping( $pCategoryMappingId = null ){
		
		if (! $this->id > 0 ) return array();
		if (! $pCategoryMappingId > 0 ) return array();
		
		$txtSql = "
select 
	a_pcm.ad_id
from 
(


select 
	a.id as ad_id, pcm.id as pcm_id
from ads a 
	join ads_parameters ap on (ap.ad_id = a.id)
		join parameters p on (p.id = ap.parameter_id)
			join parameters_types pt on (pt.id = p.type_id and pt.accept_dictionary = 1)
		join n_publications_categories_mappings pcm on ( 
			pcm.mapped_category_id = a.category_id
			and pcm.category_mapping_id = $pCategoryMappingId
			and pcm.mapped_location_id is null 
			and pcm.mapped_dictionary_id is not null
			and pcm.mapped_dictionary_id = cast(ap.value as UNSIGNED)
			)
	
union

select 
	a.id as ad_id, pcm.id as pcm_id
from
	ads a
		join n_publications_categories_mappings pcm  on (
			pcm.mapped_category_id = a.category_id 
			and pcm.category_mapping_id = $pCategoryMappingId
			and pcm.mapped_dictionary_id is null
			and pcm.mapped_location_id is not null
			and pcm.mapped_location_id in (
				a.country_id, a.county_id, a.municipality_id, a.city_id)
			)

union

select 
	a.id as ad_id, pcm.id as pcm_id
from
	ads a
		join n_publications_categories_mappings pcm  on (
			pcm.mapped_category_id = a.category_id 
			and pcm.category_mapping_id = $pCategoryMappingId
			and pcm.mapped_dictionary_id is null
			and pcm.mapped_location_id is null
		)

union

select 
	a.id as ad_id, pcm.id as pcm_id
from ads a 
	join ads_parameters ap on (ap.ad_id = a.id)
		join parameters p on (p.id = ap.parameter_id)
			join parameters_types pt on (pt.id = p.type_id and pt.accept_dictionary = 1)
		join n_publications_categories_mappings pcm on ( 
			pcm.mapped_category_id = a.category_id
			and pcm.category_mapping_id = $pCategoryMappingId
			and pcm.mapped_location_id is not null 
			and pcm.mapped_dictionary_id is not null
			and pcm.mapped_dictionary_id = cast(ap.value as UNSIGNED)
			and pcm.mapped_location_id in (
				a.country_id, a.county_id, a.municipality_id, a.city_id
			)
		)
) a_pcm
	join orders_items oi on (a_pcm.ad_id = oi.ad_id)
		join n_insertions ins on (oi.id = ins.orders_items_id)
			join n_issues iss on (
				ins.issues_id = iss.id
				and iss.id = $this->id
			)
		
		";
		$arrIds = array();
		$rsIds = \Baseapp\Suva\Library\Nava::sqlToArray($txtSql);
		foreach ($rsIds as $row)
			
			//if ( !in_array($row['id'], $arrIds) )
				array_push($arrIds, $row['ad_id']);
		
		return $arrIds;		
	}
	public function getAdIdsByCategoryMappingOLD2( $pCategoryMappingId = null , $pIssuesId = null){
		//traženje svih publication category mappinga za pojedinu kategoriju od publikacije
		$txtSql = "
		select 
			pcm.mapped_category_id 
			,pcm.mapped_dictionary_id
			,pcm.mapped_location_id
		from 
			n_publications_categories_mappings pcm 
				join n_issues i 
					on (pcm.publication_id = i.publications_id) 
		where 
			i.id =  $pIssuesId 
			and pcm.category_mapping_id = $pCategoryMappingId
		";		
		$pubCatsMaps = \Baseapp\Suva\Library\Nava::sqlToArray($txtSql);
		$txtWhere = "";
		if ($pubCatsMaps)
			foreach ( $pubCatsMaps as $pubCatMap ){
				$mappedCategoryId = $pubCatMap['mapped_category_id'];
				$catSql =  $mappedCategoryId > 0 ? " category_id = $mappedCategoryId " : " category_id is null ";
				
				$mappedDictionaryId = $pubCatMap['mapped_dictionary_id'];
				$dictSql = $mappedDictionaryId > 0 ? " dictionary_id = $mappedDictionaryId " : " dictionary_id is null ";
				
				$txtWhere .= " or ( $catSql and $dictSql ) ".PHP_EOL;
			}

			
			/*
select
	id
from 
	(

	select
		a.id
		,a.category_id
		,( case when pt.accept_dictionary = 1 then d.id else null end ) as dictionary_id
		,( case when pt.accept_dictionary = 1 then d.name else ap.value end ) as value
		, i.id as insertions_id
	from 
		ads a
			left join ads_parameters ap on (a.id = ap.ad_id)
				join parameters p on (ap.parameter_id = p.id)
					join parameters_types pt on (p.type_id = pt.id )
				left join dictionaries d on ( ap.value = d.id)
			join orders_items oi on oi.ad_id = a.id 
				join n_insertions i on oi.id = i.orders_items_id and i.issues_id between 1 and 1000 
	where 1=1
	
	) p1
where 1=1
	and category_id = 4
	and dictionary_id = 1234
			*/
			
			
			
			
		//traženje svih ad-ova koji zadovoljavaju te mappinge
		$txtSql = "
select
 id
from 
	(

	select
		a.id as id
		,a.category_id as category_id
		,( case when pt.accept_dictionary = 1 then d.id else null end ) as dictionary_id
		,( case when pt.accept_dictionary = 1 then d.name else ap.value end ) as value
		, i.id as insertions_id
	from 
		ads a
			left join ads_parameters ap on (a.id = ap.ad_id)
				join parameters p on (ap.parameter_id = p.id)
					join parameters_types pt on (p.type_id = pt.id )
				left join dictionaries d on ( ap.value = d.id)
			join orders_items oi on oi.ad_id = a.id 
				join n_insertions i on oi.id = i.orders_items_id and i.issues_id between 1 and 1000 
	where 1=1
	
	) p1
where 1=0 $txtWhere

group by id
";
// group by id je samo dodano jer je raćao 40 istih ad_idjeva. TODO: vidit zašto.

	
		//error_log("Issues.php: getAdIdsByCategoryMapping 10 txtSql: $txtSql");
		$arrIds = array();
		$rsIds = \Baseapp\Suva\Library\Nava::sqlToArray($txtSql);
		foreach ($rsIds as $row)
			
			//if ( !in_array($row['id'], $arrIds) )
				array_push($arrIds, $row['id']);
		
		return $arrIds;
		
	}
	
	
	public function getAdIdsByCategoryOLD( $pCategoryId = null , $pIssuesId = null){
		$catIdList = $pCategoryId;
		$pubCats = \Baseapp\Suva\Library\Nava::sqlToArray("select pc.category_id from n_publications_categories pc join n_issues i on (pc.publication_id = i.publications_id) where i.id =  $pIssuesId and pc.forward_category_id = $pCategoryId");
		if ($pubCats)
			foreach ( $pubCats as $pubCat )
				$catIdList .= ", ".$pubCat['category_id'];
		
		//error_log("Issues:getAdIdsByCategory 2, pubCats:".print_r($pubCats, true));
		//error_log("Issues:getAdIdsByCategory 3 catIdList: $catIdList");
		
		
		//ad-ovi pod tom kategorijom i tim oglasom
		$adIds = \Baseapp\Suva\Library\Nava::sqlToArray("
			select a.id as id
			from ads a
				join orders_items oi on oi.ad_id = a.id 
					join n_insertions i on oi.id = i.orders_items_id and i.issues_id = $pIssuesId 
			where	
				a.category_id in ( $catIdList )
			order by
				a.description
		");
		
		return $adIds;
		
	}
	
//veze one2many
	// public function Insertions(){
		// if ($this->id > 0)
			// return \Baseapp\Suva\Models\Insertions::find(" issues_id = $this->id " );
		// else
			// return null;
		
	// }
	
	
	//polja one2many
	public function Insertions( $p = null ){
		if (!$this->id > 0) return null;
		$findParam = array();
		$txtWhere = '';
		if (gettype($p)=='string') $txtWhere = " and $p";
		elseif (is_array($p)){
			if (array_key_exists(0,$p)) $txtWhere = " and $p[0]";
			if (array_key_exists("order",$p)) $findParam["order"] = $p["order"];
			if (array_key_exists("limit",$p)) $findParam["limit"] = $p["limit"];
		} 
		$findParam[] = "issues_id = $this->id ".$txtWhere;
		//return \Baseapp\Suva\Models\Ads::find("user_id = $this->id");
		return \Baseapp\Suva\Models\Insertions::find($findParam);
	}
	
	public function InsertionsAvus(){
		if ($this->id > 0)
			return \Baseapp\Suva\Models\InsertionsAvus::find(" issue_id = $this->id " );
		else
			return null;
		
	}
	
//veze many2one
	public function Publication(){
		if ($this->publications_id > 0)
			return \Baseapp\Suva\Models\Publications::findFirst($this->publications_id);
		else
			return null;
	}
	
	
	
	
}
