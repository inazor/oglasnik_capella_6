<?php

namespace Baseapp\Suva\Models;

/**
 * Layouts Model
 */
class Layoutss extends BaseModelBlamable
{
    
	//se traitBaseSuvaModel;
	
	public function initialize()
    {
        $this->setSource("n_layouts");
    }

        public function getSource()
    {
        return 'n_layouts';
    }


	public function generateXml($p = array()){
		if (! $this->id > 0) {
			$this->sysMessage("Layouts.php: Layout ID not defined., p:".print_r($p,true));
			return "ERR: Layouts.php: Layout ID not defined.";
		}
		
		
		
		$arrVariables = explode( ",", $this->variables ); //variables je polje u bazi
		//provjera jesu li sve varijable iz teksta na popisu
		//provjera sadrži li popis sve varijable koje se spominju u tekstu
		//error_log(print_r($arrVariables, true));
		//provjera sadrži li p sve varijable s popisa
		foreach ($arrVariables as $var)
			if (!array_key_exists($var, $p)) {
				$txtErr = "ERR: Layouts.php: key $var not supplied, "
					.PHP_EOL."layout:".print_r($this->toArray(),true)
					.PHP_EOL."params:".print_r($p, true)
					.PHP_EOL;
				$this->sysMessage("Layout $this->id : $txtErr , p:".print_r($p, true));
				return $txtErr;
			}
		
		//zamjena
		//$xml = str_replace ('"',"_DBLQUOT_",$this->xml);
		$xml = $this->xml;
		foreach ($arrVariables as $var){
			//error_log("__".$var."__".print_r($p,true));
			
			$xml = str_replace ("__".$var."__", $p[$var], $xml);
			//error_log("STRREPLACE __".$var."__, ".$p[$var].PHP_EOL."BEFORE: $this->xml ".PHP_EOL."RESULT: $xml");
		}
		return $xml;
	}

}