<?php

namespace Baseapp\Suva\Models;

// use Phalcon\Mvc\Model\Resultset;

class Locations extends \Baseapp\Models\Locations
{
	use traitBaseSuvaModel;

	
	public function getPath(){
		
		$txt = '';
		if ( $this->id > 0 ){
			$txt = $this->name;
			if ( $this->parent_id > 0 ) {
				$parent1 = \Baseapp\Models\Locations::findFirst ( $this->parent_id );
				if ( $parent1 ){
					$txt = $parent1->name ." - ".$txt;
					
					if ( $parent1->parent_id > 0) {
						$parent2 = \Baseapp\Models\Locations::findFirst ( $parent1->parent_id );
						if ($parent2){
							$txt = $parent2->name ." - ".$txt;
							
							
							if ($parent2->parent_id > 0) {
								$parent3 = \Baseapp\Models\Locations::findFirst ( $parent2->parent_id );
								if ($parent3){
									$txt = $parent3->name ." - ".$txt;
								}
							}
							
						}
					}
				}
			}
		}
		
		return $txt;
		
	}
	
	
}
