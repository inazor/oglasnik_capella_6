<?php

namespace Baseapp\Suva\Models;
use Baseapp\Suva\Library\Nava;

/**
 * Publications Model
 */
class Model extends BaseModelBlamable
{
    use traitBaseSuvaModel;
	
	public function initialize(){}

    public function getSource(){}

    public function initialize_model_with_post($request){}

    public function save_changes($request){}
	
	
	//TRANSLATE
	public function t ( $pText=null, $p1=null , $p2=null, $p3=null, $p4=null ) {
		// error_log("translate");
		$lang = \Baseapp\Suva\Library\libAppSetting::get('sys_current_language');
		if (!$lang) {
			// error_log ("Vraća se defaultno, nije pronađen jezik u postavkama");
			return sprintf($pText,$p1,$p2,$p3,$p4);
		}
		
		elseif ( $lang == 'ORIGINAL') {
			// error_log ("Vraća se defaultno, lang = ORIGINAL");
			return sprintf($pText,$p1,$p2,$p3,$p4);
		} 
		elseif ( array_key_exists( $pText, $_SESSION['_translations'] ) ){
			//vraća se prijevod iz arraya
			return sprintf( $_SESSION['_translations'][$pText], $p1, $p2, $p3, $p4);
		}
		else {
			$txtSql = "select * from n_sys_translations where lang ='$lang' and original = '$pText'";
			error_log ("lang = $lang, traži se prijevod , sql: $txtSql");
			$rs = \Baseapp\Suva\Library\Nava::sqlToArray($txtSql);
			if ($rs){
				foreach ($rs as $row){
					$translation = $row['translation'];
					error_log ("pronadjen prijevod $translation");
					return sprintf($translation ,$p1,$p2,$p3,$p4);
				}
			}
			else {
				$txtSql= "insert into n_sys_translations (lang, original, translation, status) values ('$lang', '$pText', '$pText', 'not_translated')  ";
				error_log ("Prijevod izjave $pText nije pronadjen u $lang, upisuje se defaultno u bazu, vraća se defautlno SQL:$txtSql");
				\Baseapp\Suva\Library\Nava::runSql ($txtSql);
				return sprintf($pText,$p1,$p2,$p3,$p4);
				//\Baseapp\Suva\Library\Nava::sysMessage("Prijevod izjave $pText nije pronadjen u $lang, SQL:$txtSql");			
			}
		}
	}

		
	public static function isUserAllowed($pActionName = null, $pUserId = null){
		if (!$pActionName) return false;
		$action = \Baseapp\Suva\Models\Actions::findFirst("name = '$pActionName' and is_active = 1");
		if (!$action) return false;
		//za sada, inače treba nać logiranog usera ovdje
		if (!$pUserId) return false;
		$userId = $pUserId;
		
		
		//error_log("Model::isUserAllowed 1 permissions:".print_r($_SESSION['_permissions'], true));
		
		//traženje permissiona za userId
		if (!array_key_exists('_permissions',$_SESSION))
			$_SESSION['_permissions'] = array();
		
		//dodavanje liste permissiona
		// if (!array_key_exists($userId, $_SESSION['_permissions']))
		$_SESSION['_permissions'][$userId] = \Baseapp\Suva\Library\Nava::getPermissionsArray($userId);
		
		if ($_SESSION['_permissions'][$userId][$pActionName] === true) 
			return true;
		else 
			return false;
		
		//error_log ("Model::isUserAllowed  1 txtSql: $txtSql");
	}

	public function n_validate($value, $type){
		//koristi se u cashout.volt
			switch ($type){
				case 'id':
					if ((int)$value > 0) return true;
					break;
				case 'date':
					$dmy = explode(".", trim($value));
					if (count($dmy) != 3) return false;
					// 25.02.2015
					elseif ((int)$dmy[0] > 0 && (int)$dmy[0] < 32 && (int)$dmy[1] > 0 && (int)$dmy[1] < 13 && (int)$dmy[2] > 1969  && (int)$dmy[2] < 9999) 
						return true;
					// 2015.02.25
					elseif ((int)$dmy[2] > 0 && (int)$dmy[2] < 32 && (int)$dmy[1] > 0 && (int)$dmy[1] < 13 && (int)$dmy[0] > 1969  && (int)$dmy[0] < 9999) 
						return true;
					else return false;
					break;
			}
			return false;
				
	}
	
	public function txtPad ( $pValue, $pLength, $pType = 'text', $pAlign = null, $pDecimals = null ){
		//error_log("MOdel:txtPad 1");
		if ($pType == 'text'){
			//error_log("MOdel:txtPad 2");
			if (!$pAlign) $pAlign = 'left';
			$txt = $pValue;
		}
		
		elseif ($pType == 'currency' || $pType == 'numeric' ){
			//error_log("MOdel:txtPad 5");
			if (!$pAlign ) $pAlign = 'right';
			if ( $pDecimals === null ) $pDecimals = 2;
			$txt = number_format( $pValue, $pDecimals, ',','.');
			if (mb_strlen($txt) > $pLength) $txt = str_pad("",$pLength, "*");
		}
		
		elseif ($pType == 'date'){
			//error_log("MOdel:txtPad 5");
			if (!$pAlign ) $pAlign = 'right';
			$txt = $this->n_dateTimeUs2DateHr( $pValue );
			if (mb_strlen($txt) > $pLength) $txt = str_pad("",$pLength, "*");
		}
		elseif ($pType == 'datetime'){
			//error_log("MOdel:txtPad 5");
			if (!$pAlign ) $pAlign = 'right';
			$txt = $this->n_dateTimeUs2DateTimeHr( $pValue );
			if (mb_strlen($txt) > $pLength) $txt = str_pad("",$pLength, "*");
		}
		$len = mb_strlen($txt);
		//error_log (PHP_EOL.PHP_EOL."Model:txtPad: 4.1 len: $len, length: $pLength, align: $pAlign, txt: '$txt'");

		
		if ($pAlign == 'left' && $len < $pLength) {
			//error_log ("Model:txtPad: 1");
			for ( $x = $len ; $x < $pLength; $x++ ) 
				$txt = $txt."&nbsp;";
		}
		elseif ($pAlign == 'right' && $len < $pLength) {
			//error_log ("Model:txtPad: 2");
			for ( $x = $len ; $x < $pLength; $x++ ) 
				$txt = "&nbsp;".$txt;
		}
		elseif ($pAlign == 'left' && $len > $pLength) {
			//error_log ("Model:txtPad: 3");
			$txt = mb_substr($txt, 0, $pLength) ;
		}
		elseif ($pAlign == 'right' && $len > $pLength) {
			
			//error_log ("Model:txtPad: 4 mb_substr('$txt', $len - $pLength, $pLength)");
			
			$txt = mb_substr($txt, $len - $pLength);
		}

		//error_log ("Model:txtPad: 5.1 len: $len, length: $pLength, align: $pAlign, txt: '$txt'");
		$txt = str_replace(" ","&nbsp;",$txt);
		$result = $txt;
		$result = str_replace(" ","&nbsp;",$result);
		
		return $result;
		//{{ str_replace("_","&nbsp;",str_pad( substr(order.id, 0,6),6,"_")) }}
	}
	
	public function getAllowedActionIds( $pUserId = null, $pOrderStatusId = Null , $pContext = Null) {
	/*	
		vraća listu id-jeva dopuštenih akcija
			context je mjesto na kojem se akcija provodi
			item 
			orders
			login
			main_menu

			KORIŠTENJE:
	*/
		if (!$pUserId) return Array();
		
		$txtOrderStatusId = "";
		if ($pOrderStatusId) $txtOrderStatusId = " and rp.order_status_id = ".$pOrderStatusId;
		if (!$pContext) return Array();
		$str = 
		"select "
		."	a.id "
		."from users u "
		." join roles_users ru on u.id = ru.user_id "
		." join n_roles_permissions rp on ru.role_id = rp.role_id "
		." join n_actions a on rp.action_id = a.id "
		." where "
		." u.id = ".$pUserId
		." and a.is_active = 1 "
		." and rp.is_active = 1 "
		.$txtOrderStatusId
		." and a.context = '".$pContext."'" 
		." group by a.id";

		//error_log($str);
		
		$ids = \Baseapp\Suva\Library\Nava::sqlToArray($str);
		
		$result = Array();
		foreach((array)$ids as $id)
			array_push($result, $id['id']);
		return $result;
	}
	
	
	
	public function getOne($pModelName = null, $p = null){
		
		if (!$pModelName || !$p) return null;
		//if (!is_numeric($p)) return null;
		// error_log("P = ".$p."  $pModelName  ".gettype($p));
		
		$id = intval($p);
		//$strWhere = " id = ".$p;
		$strWhere = is_numeric($p) ? " id = ".intval($p) : null;
		$strWhere = !$strWhere ? $p : $strWhere;
		if (!$strWhere) return null;
		$class = '\\Baseapp\\Suva\\Models\\'.$pModelName;
		$model = new $class();
		
		// $e = new \Exception();
		// error_log($e->getTraceAsString() );
		
		//error_log("Model:getModelByNameAndId 1.54 id:$strWhere");
		// error_log("Model:getModelByNameAndId 1.5  model:".get_class($model));
		$object = $model->findFirst($strWhere);
		//error_log("Model:getModelByNameAndId 2  classname:".get_class($object));

		if ($object === false) return null;
		else return $object;
	}
	

//POJEDINAČNI OBJEKTI (findfirst)	
	public function Action($p = null){	return $this->getOne("Actions", $p); }
	public function Ad($p = null){	return $this->getOne("Ads", $p); }
	public function AdMedia($p = null){	return $this->getOne("AdsMedia", $p); }
	public function AdParameter($p = null){	return $this->getOne("AdsParameters", $p); }
	
	public function Category($pId = null){	return $this->getOne("Categories", $pId); }
	public function CategoryMapping($pId = null){	return $this->getOne("CategoriesMappings", $pId); }
	public function Currency($pId = null){	return $this->getOne("Currency", $pId); }
	public function Dictionary($p = null){ return $this->getOne("Dictionaries", $p); }
	public function Discount($p = null){ return $this->getOne("Discounts", $p); }
	public function DiscountPublication($p = null){ return $this->getOne("DiscountsPublications", $p); }
	public function FiscalLocation($pId = null){	return $this->getOne("FiscalLocations", $pId); }
	public function Issue($pId = null){	return $this->getOne("Issues", $pId); }
	public function Insertion($pId = null){	return $this->getOne("Insertions", $pId); }
	public function InsertionAvus($pId = null){	return $this->getOne("InsertionsAvus", $pId); }
	public function Layout( $p = null ) { return $this->getMany('Layouts', $p, " is_active = 1 ");}
	public function Location($pId = null){	return $this->getOne("Locations", $pId); }
	public function Order($pId = null){	return $this->getOne("Orders", $pId); }
	public function OrderItem($pId = null){	return $this->getOne("OrdersItems", $pId); }
	public function OrderStatus($pId = null){	return $this->getOne("OrdersStatuses", $pId); }
	public function OrdersStatus($pId = null){	return $this->getOne("OrdersStatuses", $pId); }
	public function PaymentMethod($p = null){	return $this->getOne("PaymentMethods", $p); }
	public function PaymentType($p = null){	return $this->getOne("PaymentTypes", $p); }
	public function Product($p = null){	return $this->getOne("Products", $p); }
	public function PublicationCategory($p = null){	return $this->getOne("PublicationsCategories", $p); }
	public function Publication($p = null){	return $this->getOne("Publications", $p); }
	public function User($pId = null){	return $this->getOne("Users", $pId); }
	public function UserContact($pId = null){	return $this->getOne("UsersContacts", $pId); }
	public function Country($pId = null){	return $this->getOne("Locations", $pId); }
	public function DtpGroup( $p = null ) { return $this->getOne('DtpGroups', $p, " is_active = 1 ");}
	
	
	//LISTE OBJEKATA
	
	public function getMany($pModelName = null, $p = null, $pDefaultCondition = null){
		if (!$pModelName) return null;
//		error_log("NIKOLA MODEL MODELS.PHP 1 DEFAULT CONDITIONS".$pDefaultCondition);
//		error_log("NIKOLA MODEL MODELS.PHP 1 PARAMETER".$p);
		$class = '\\Baseapp\\Suva\\Models\\'.$pModelName;
		$model = new $class();
		
		$findParam = array();
		$findParam[0] = " 1 = 1 ";
		
		if (gettype($p)=='string') $findParam[0] .= " and $p";
		elseif (is_array($p)){
			if ( array_key_exists( 0, $p) ) $findParam[0] .= " and $p[0]";
			if ($p["order"]) $findParam["order"] = $p["order"];
			if (array_key_exists('limit',$p)) $findParam["limit"] = $p["limit"];
		}
		
		if ( gettype($pDefaultCondition)=='string') 
			$findParam[0] .= " and $pDefaultCondition ";
		// error_log("NIKOLA MODEL MODELS.PHP 2".print_r($findParam,true));
		$object = $model->find($findParam);
		return $object;
	}
	
	
	public function Actions( $p = null ) { return $this->getMany('Actions', $p, " is_active = 1 " );}
	public function Ads( $p = null ) { return $this->getMany('Ads', $p );}
	public function AdsMedia( $p = null ) { return $this->getMany('AdsMedia', $p);}
	public function AdsParameters( $p = null ) { return $this->getMany('AdsParameters', $p);}
	public function AppSettings( $p = null ) { return $this->getMany('AppSettings', $p, " is_active = 1 ");}
	public function Categories( $p = null ) { return $this->getMany('Categories', $p);}
	public function CategoriesMappings( $p = null ) { return $this->getMany('CategoriesMappings', $p);}
	public function Counties( $p = null ) { return $this->getMany('Counties', $p);}
	public function Countries( $p = null ) { return $this->getMany('Countries', $p," level = 1 ");}
	public function Currencies( $p = null ) { return $this->getMany('Currency', $p, " active = 1 ");}
	public function Dictionaries( $p = null ) { return $this->getMany('Dictionaries', $p);}
	public function Discounts( $p = null ) { return $this->getMany('Discounts', $p, " is_active = 1 ");}
	public function DiscountsPublications( $p = null ) { return $this->getMany('DiscountsPublications', $p, " is_active = 1 ");}
	public function DtpGroups( $p = null ) { return $this->getMany('DtpGroups', $p, " is_active = 1 ");}
	public function FiscalLocations( $p = null ) { return $this->getMany('FiscalLocations', $p, " is_active = 1 ");}
	public function Insertions( $p = null ) { return $this->getMany('Insertions', $p, " is_active = 1 ");}
	public function InsertionsAvus( $p = null ) { return $this->getMany('InsertionsAvus', $p);}

	public function Issues( $p = null ) { return $this->getMany('Issues', $p, " is_active = 1 ");}
	public function Layouts( $p = null ) { return $this->getMany('Layouts', $p, " is_active = 1 ");}
	public function Locations( $p = null ) { return $this->getMany('Locations', $p);}
	public function ModerationReasons( $p = null ) { return $this->getMany('ModerationReasons', $p, " is_active = 1 ");}
	public function Orders( $p = null ) { return $this->getMany('Orders', $p);}
	public function OrdersItems( $p = null ) { return $this->getMany('OrdersItems', $p);}
	public function OrdersStatuses( $p = null ) { return $this->getMany('OrdersStatuses', $p, " is_active = 1 ");}
	public function PaymentMethods( $p = null ) { return $this->getMany('PaymentMethods', $p, " is_active = 1 ");}
	public function PaymentTypes( $p = null ) { return $this->getMany('PaymentTypes', $p, " is_active = 1 ");}
	public function Products( $p = null ) { return $this->getMany('Products', $p);}
	public function Publications( $p = null ) { return $this->getMany('Publications', $p, " is_active = 1 ");}
	public function PublicationsCategories( $p = null ) { return $this->getMany('PublicationsCategories', $p, " is_active = 1 ");}
	public function Roles( $p = null ) { return $this->getMany('Roles', $p);}
	public function RolesPermissions( $p = null ) { return $this->getMany('RolesPermissions', $p, " is_active = 1 ");}
	public function TransactionTypes( $p = null ) { return $this->getMany('TransactionTypes', $p);}
	public function Users( $p = null ) { return $this->getMany('Users', $p);}
	public function ProductsCategoriesPrices( $p = null ) { return $this->getMany('ProductsCategoriesPrices', $p);}
	public function PublicationsCategoriesMappings( $p = null ) { return $this->getMany('PublicationsCategoriesMappings', $p);}
	
	
	
	 

}
