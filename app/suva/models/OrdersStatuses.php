<?php
namespace Baseapp\Suva\Models;



use Baseapp\Console;
use Baseapp\Library\Utils;
use Baseapp\Library\RawDB;
use Baseapp\Library\Parameters\Parametrizator;

class OrdersStatuses extends BaseModelBlamable
{
	public $id;
	public $name;
	public $desc;

    public function initialize()
    {
        $this->setSource("n_order_statuses");
    }

		public function getSource()
    {
        return 'n_order_statuses';
    }


	
}

