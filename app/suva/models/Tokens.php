<?php

namespace Baseapp\Suva\Models;

/**
 * Token Model
 */
class Tokens extends \Baseapp\Models\Tokens
{
	use traitBaseSuvaModel;
    // /**
     // * Sets the table name
     // */
    // public function getSource()
    // {
        // return 'user_tokens';
    // }

    // /**
     // * Token initialize
     // */
    // public function initialize()
    // {
        // parent::initialize();

        // $this->belongsTo('user_id', __NAMESPACE__ . '\Users', 'id', array(
            // 'alias' => 'User',
            // 'foreignKey' => true
        // ));

        // // Do garbage collection
        // if (mt_rand(1, 100) === 1) {
            // $this->delete_expired();
        // }

        // // This object has expired
        // if (property_exists($this, 'expires') && $this->expires < time()) {
            // $this->delete();
        // }
    // }

    // /**
     // * Deletes all expired tokens
     // */
    // public function delete_expired()
    // {
        // $this->getDI()->getShared('db')->execute('DELETE FROM `' . $this->getSource() . '` WHERE `expires` < :time', array(':time' => time()));
    // }
}
