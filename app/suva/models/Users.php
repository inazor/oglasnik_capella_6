<?php

namespace Baseapp\Suva\Models;



use Baseapp\Extension\Validator\BirthDate;
use Baseapp\Library\Email;
use Baseapp\Library\Utils;
use Baseapp\Library\Validations\UsersBackend as UsersValidationsBackend;
use Baseapp\Library\Validations\UsersBackendRelaxed as UsersValidationsBackendRelaxed;
use Baseapp\Bootstrap;
use libphonenumber\NumberParseException;
use libphonenumber\PhoneNumberFormat;
use libphonenumber\PhoneNumberUtil;
use Phalcon\Db\Column;
use Phalcon\Di;
use Phalcon\Escaper;
use Phalcon\Http\RequestInterface;
use Phalcon\Validation as PhValidation;
use Baseapp\Traits\InfractionReportsHelpers;

use Baseapp\Suva\Library\Nava;

class Users extends \Baseapp\Models\Users {

	use traitBaseSuvaModel;

	
	function generateRandomString($length = 10) {
		$characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
		$charactersLength = strlen($characters);
		$randomString = '';
		for ($i = 0; $i < $length; $i++) {
			$randomString .= $characters[rand(0, $charactersLength - 1)];
		}
		return $randomString;
	}
	public function provjeraPolja(){
	
		$jeGreska = false;
		
		if ( strlen($this->username) > 0 ){
			$users = \Baseapp\Suva\Models\Users::find( "username = '$this->username' " );
			if ($this->id > 0){
				if ( count($users) > 1) 
					$jeGreska = $this->greskaSnimanje("U sustavu već postoji ".(count($users))." korisnika s traženim Username-om", 'username');
			} else {
				if ( count($users) > 0) $jeGreska = $this->greskaSnimanje("U sustavu već postoji ".(count($users))." korisnika s traženim Username-om", 'username');
			}
		}
		
		if ( strlen($this->email) > 0 ){
			$users = \Baseapp\Suva\Models\Users::find( "email = '$this->email' " );
			if ($this->id > 0){
				if ( count($users) > 1) 
					$jeGreska = $this->greskaSnimanje("U sustavu već postoji ".(count($users))." korisnika s traženim Username-om", 'username');
			}
			else {
				if ( count($users) > 1) 
					$jeGreska = $this->greskaSnimanje("U sustavu već postoji ".(count($users))." korisnika s traženim Username-om", 'username');
			}
		}
		
		//oib:
		$this->oib = trim($this->oib);
		//error_log("NIKOLA users beforeSave OIB : $this->oib");
		$userIstiOib = $this->type > 0 && strlen($this->oib) > 0 ? \Baseapp\Suva\Models\Users::find( "type = $this->type and oib = '$this->oib'" ) : 0;
		//error_log("models/Users.php $userIstiOib");
		
		//ako ima više od jednog oiba za isti tip korisnika
		if (count($userIstiOib) > 1) 
			$this->greskaSnimanje("U sustavu već postoji ".(count($userIstiOib)-1)." korisnika s traženim OIB-om", 'oib');

		//ako nije Hrvatska a unesen je oib javlja grešku
		elseif ( !($this->country_id == 1) && strlen($this->oib) > 0  ) 
			$jeGreska = $this->greskaSnimanje("In order to enter OIB country Hrvatska must be chosen", 'oib');

		//ako je Hrvatska i unesen je oib ali nije 11 znakova
		elseif ( $this->country_id == 1 && strlen($this->oib) > 0 && strlen(trim($this->oib)) != 11) 
			$jeGreska = $this->greskaSnimanje("OIB must be exactly 11 characters long", 'oib');

		//ako je Hrvatska i unesen je oib ali sadrži ne-numeričke znakove
		elseif ( $this->country_id == 1 && !ctype_digit($this->oib)) 
			$jeGreska = $this->greskaSnimanje("OIB must contain only numeric characters", 'oib');
	
		/*
		// n_vat_number 
		$this->n_vat_number = trim($this->n_vat_number);
		$userIstiVatNumber = ( $this->type > 0 && $this->n_vat_number > '' ) ? \Baseapp\Suva\Models\Users::find( "type = $this->type and n_vat_number = '$this->n_vat_number'" ) : 0;

		//ako ima više od jednog oiba za isti tip korisnika //ovo samo po sebi nije greska, trebalo bi biti upozorenje
		if (count($userIstiVatNumber) > 1) 
			$this->greskaSnimanje("U sustavu već postoji ".(count($userIstiVatNumber)-1)." korisnika s traženim VAT No.", 'n_vat_number');
		*/
		
		//ako nije unesena država a unesen je n_vat_number javlja grešku
		if ( !$this->country_id && strlen($this->n_vat_number) > 0  ) 
			$jeGreska = $this->greskaSnimanje("In order to enter VAT No. country must be chosen", 'n_vat_number');

		if ( !( $this->country_id >= 1 ) ) 
			$jeGreska = $this->greskaSnimanje("Please select a Country", 'country_id');
		
		if ( !($this->county_id > 1) && $this->country_id == 1 ) 
			$jeGreska = $this->greskaSnimanje("Za Hrvatsku je obavezna županija", 'county_id');
		
		if ( strlen($this->address) < 2 ) 
			$jeGreska = $this->greskaSnimanje("Address must be longer than 2 characters", 'address');
		if ( strlen($this->city) < 2 ) 
			$jeGreska = $this->greskaSnimanje("City must be longer than 2 characters", 'city');
		if ( strlen($this->zip_code) < 2 ) 
			$jeGreska = $this->greskaSnimanje("ZIP must be longer than 2 characters", 'zip_code');

		if ($this->type != 1 && $this->type != 2) 
			$jeGreska = $this->greskaSnimanje("Incorrect User Type", 'type');

		if (strlen(trim($this->first_name)) < 2 && $this->type == 1) 
			$jeGreska = $this->greskaSnimanje("First must be longer than 2 characters", 'first_name');
		
		if (strlen($this->last_name) < 2 && $this->type == 1) 
			$jeGreska = $this->greskaSnimanje("Last must be longer than 2 characters", 'last_name');

		if (strlen($this->company_name) < 2 && $this->type == 2) 
			$jeGreska = $this->greskaSnimanje("Company Name must be longer than 2 characters", 'company_name');
		
		
		// if (empty($_SESSION['_context']['selected_roles_users']) || !array_key_exists('selected_roles_users', $_SESSION['_context']))  $jeGreska = $this->greskaSnimanje("Potrebno je odabrati ROLU korisniku");
		
		
		
		if ($jeGreska) return false; 
		else return true;
	}	
		
	 public static function buildInvoiceDetailsArray($source)
    {
        $details = array();

        if ($source instanceof Users) {
            $source = $source->toArray();
        }
		
		
		$details['TAXNUMBER']  = array_key_exists('oib', $source) ? $source['oib'] : null;//Dodano N
        $details['EMAIL']      = array_key_exists('email', $source) ? $source['email'] : null;//Dodano N
        $details['FIRST_NAME'] = array_key_exists('first_name', $source) ? $source['first_name'] : null;//Dodano N
        $details['LAST_NAME']  = array_key_exists('last_name', $source) ? $source['last_name'] : null;//Dodano N
        $details['STREET']     = array_key_exists('address', $source) ? $source['address'] : null;//Dodano N
        $details['ZIPCODE']    = array_key_exists('zip_code', $source) ? $source['zip_code'] : null;//Dodano N
        $details['CITY']       = array_key_exists('city', $source) ? $source['city'] : null;//Dodano N
		
        if (array_key_exists('type',$source) && self::TYPE_PERSONAL == $source['type']) {
            $parts = array();
            if (!empty($source['first_name'])) {
                $parts[] = $source['first_name'];
            }
            if (!empty($source['last_name'])) {
                $parts[] = $source['last_name'];
            }
            $details['CUSTOMERNAME'] = implode(' ', $parts);
            $details['COMPANY']      = 0;
        } elseif(array_key_exists('company_name',$source)) {
            $details['CUSTOMERNAME'] = $source['company_name'];
            $details['COMPANY']      = 1;
        }

       

        // Setting 'TAXCOUNTRYCODE' based on user's country_id if available
		$country_id = array_key_exists('country_id', $source) ? $source['country_id'] : null;  
		
        if ($country_id) {
            $location = Locations::findFirst('id = ' . (int) $country_id);
            if ($location) {
                $country_iso_code = $location->iso_code;
                if (!empty($country_iso_code)) {
                    $details['TAXCOUNTRYCODE'] = $country_iso_code;
                }
            }
        }

        $phone_no   = !empty($source['phone1']) ? $source['phone1'] : (!empty($source['phone2']) ? $source['phone2'] : null);
        $phone_util = PhoneNumberUtil::getInstance();
        try {
            $phone_data = $phone_util->parse($phone_no, 'HR');

            /**
             * This is so that Oglasnik can dump shitty data into a table in Navision,
             * which seems to be handling phone numbers like its 1980 or something...
             * Area code means nothing, but they must have it, so we produce something, but...
             * Read more here if you care: https://github.com/googlei18n/libphonenumber/issues/46
             */
            $nat_dest_code = null;
            $national_significant_number = $phone_util->getNationalSignificantNumber($phone_data);
            $nat_dest_code_len = $phone_util->getLengthOfNationalDestinationCode($phone_data);
            if ($nat_dest_code_len > 0) {
                $nat_dest_code = substr($national_significant_number, 0, $nat_dest_code_len);
                /**
                 * Zero-padding left side up to 2 or 3 digits (based on gotten length) so that:
                 * - +3851... -> 01
                 * - +38521123456 -> 021
                 * - +38598462602 -> 098
                 * ... etc. It will break eventually.
                 */
                $len = strlen($nat_dest_code);
                if ($len < 2) {
                    $nat_dest_code = str_pad($nat_dest_code, 2, '0', STR_PAD_LEFT);
                } else if ($len <= 3) {
                    $nat_dest_code = str_pad($nat_dest_code, 3, '0', STR_PAD_LEFT);
                }
            }

            $phone_formatted = $phone_util->format($phone_data, PhoneNumberFormat::E164);

            $details['TELAREA']        = $nat_dest_code;
            $details['TELPHONENUMBER'] = $phone_formatted;
        } catch (NumberParseException $e) {
            $details['TELAREA']        = null;
            $details['TELPHONENUMBER'] = $phone_no;
        }

        $details['USERNAME'] = array_key_exists('username', $details) ? $details['username'] : null;//Dodano N

        return $details;
    }	

	
	public function _beforeSave( $pCallingObject = null ){
		
	
		$jeGreska = false;
		
		if ( strlen($this->username) > 0 ){
			$users = \Baseapp\Suva\Models\Users::find( "username = '$this->username' " );
			if ($this->id > 0){
				if ( count($users) > 1) 
					$jeGreska = $this->greskaSnimanje("U sustavu već postoji ".(count($users))." korisnika s traženim Username-om", 'username');
			} else {
				if ( count($users) > 0) $jeGreska = $this->greskaSnimanje("U sustavu već postoji ".(count($users))." korisnika s traženim Username-om", 'username');
			}
		}
		
		if ( strlen($this->email) > 0 ){
			$users = \Baseapp\Suva\Models\Users::find( "email = '$this->email' " );
			if ($this->id > 0){
				if ( count($users) > 1) 
					$jeGreska = $this->greskaSnimanje("U sustavu već postoji ".(count($users))." korisnika s traženim Username-om", 'username');
			}
			else {
				if ( count($users) > 1) 
					$jeGreska = $this->greskaSnimanje("U sustavu već postoji ".(count($users))." korisnika s traženim Username-om", 'username');
			}
		}
		
		//oib:
		$this->oib = trim($this->oib);
		//error_log("NIKOLA users beforeSave OIB : $this->oib");
		$userIstiOib = $this->type > 0 && strlen($this->oib) > 0 ? \Baseapp\Suva\Models\Users::find( "type = $this->type and oib = '$this->oib'" ) : 0;
		//error_log("models/Users.php $userIstiOib");
		
		//ako ima više od jednog oiba za isti tip korisnika
		if (count($userIstiOib) > 1) 
			$this->greskaSnimanje("U sustavu već postoji ".(count($userIstiOib)-1)." korisnika s traženim OIB-om", 'oib');

		//ako nije Hrvatska a unesen je oib javlja grešku
		elseif ( !($this->country_id == 1) && strlen($this->oib) > 0  ) 
			$jeGreska = $this->greskaSnimanje("In order to enter OIB country Hrvatska must be chosen", 'oib');

		//ako je Hrvatska i unesen je oib ali nije 11 znakova
		elseif ( $this->country_id == 1 && strlen($this->oib) > 0 && strlen(trim($this->oib)) != 11) 
			$jeGreska = $this->greskaSnimanje("OIB must be exactly 11 characters long", 'oib');

		//ako je Hrvatska i unesen je oib ali sadrži ne-numeričke znakove
		elseif ( $this->country_id == 1 && !ctype_digit($this->oib)) 
			$jeGreska = $this->greskaSnimanje("OIB must contain only numeric characters", 'oib');
	
		/*
		// n_vat_number 
		$this->n_vat_number = trim($this->n_vat_number);
		$userIstiVatNumber = ( $this->type > 0 && $this->n_vat_number > '' ) ? \Baseapp\Suva\Models\Users::find( "type = $this->type and n_vat_number = '$this->n_vat_number'" ) : 0;

		//ako ima više od jednog oiba za isti tip korisnika //ovo samo po sebi nije greska, trebalo bi biti upozorenje
		if (count($userIstiVatNumber) > 1) 
			$this->greskaSnimanje("U sustavu već postoji ".(count($userIstiVatNumber)-1)." korisnika s traženim VAT No.", 'n_vat_number');
		*/
		
		//ako nije unesena država a unesen je n_vat_number javlja grešku
		if ( !$this->country_id && strlen($this->n_vat_number) > 0  ) 
			$jeGreska = $this->greskaSnimanje("In order to enter VAT No. country must be chosen", 'n_vat_number');

		if ( !( $this->country_id >= 1 ) ) 
			$jeGreska = $this->greskaSnimanje("Please select a Country", 'country_id');
		
		if ( !($this->county_id > 1) && $this->country_id == 1 ) 
			$jeGreska = $this->greskaSnimanje("Za Hrvatsku je obavezna županija", 'county_id');
		
		if ( strlen($this->address) < 2 ) 
			$jeGreska = $this->greskaSnimanje("Address must be longer than 2 characters", 'address');
		if ( strlen($this->city) < 2 ) 
			$jeGreska = $this->greskaSnimanje("City must be longer than 2 characters", 'city');
		if ( strlen($this->zip_code) < 2 ) 
			$jeGreska = $this->greskaSnimanje("ZIP must be longer than 2 characters", 'zip_code');

		if ($this->type != 1 && $this->type != 2) 
			$jeGreska = $this->greskaSnimanje("Incorrect User Type", 'type');

		if (strlen(trim($this->first_name)) < 2 && $this->type == 1) 
			$jeGreska = $this->greskaSnimanje("First must be longer than 2 characters", 'first_name');
		
		if (strlen($this->last_name) < 2 && $this->type == 1) 
			$jeGreska = $this->greskaSnimanje("Last must be longer than 2 characters", 'last_name');

		if (strlen($this->company_name) < 2 && $this->type == 2) 
			$jeGreska = $this->greskaSnimanje("Company Name must be longer than 2 characters", 'company_name');
		
		
		// if (empty($_SESSION['_context']['selected_roles_users']) || !array_key_exists('selected_roles_users', $_SESSION['_context']))  $jeGreska = $this->greskaSnimanje("Potrebno je odabrati ROLU korisniku");
		
		//error_log("NIKOLA users beforeSave1 jeGreska $jeGreska");
		
		if ($jeGreska) return false; 
		else return true;
	}
	
	public function afterSave( $pCallingObject = null ) {
		
// 		//ako se ne zove iz kontrolera da ne pucaju poruke i sl.
// 		if ($pCallingObject === null)
// 			$caller = new \Phalcon\Mvc\Controller\IndexController();
// 		else
// 			$caller = $pCallingObject;
		
		//ne može odavde jer je protected metoda
		//$caller->saveAudit("User $this->id updated.");
	}
	
	public function n_before_save($request){ 
		
		//dodano
	
		//dodano
		
		
		//error_log("NIKOLA users n_before_save");
		$user = $this->id >0 ? \Baseapp\Suva\Models\Users::findFirst($this->id) : null;
		
		//ako ne postoji id (ako se snima prvi put) i ako nije odabrana rola "login", dodat je na popis rola
		if (!$user){
			$roleLogin  = \Baseapp\Suva\Models\Roles::findFirst("name = 'login'");
			//error_log("sers:n_before_save 2 rolelogin->id: $roleLogin->id");
			if (!$roleLogin) return ("Nije pronađena rola 'login' - defaultna rola za svakog novog korisnika.");
			
			//ako već postoji 
			if (!array_key_exists('_context', $_SESSION)) $_SESSION['_context'] = Array();
			if (!array_key_exists('selected_roles_users', $_SESSION['_context'])) $_SESSION['_context']['selected_roles_users'] = Array();
			if (!in_array($roleLogin->id, $_SESSION['_context']['selected_roles_users']))
				array_push($_SESSION['_context']['selected_roles_users'],$roleLogin->id);
		}// else //error_log("Users:n_before_save 2 : već postoji korisnik");
		
		// snimanje passworda samo ako se mijenja password u formi. Po deafultu vraća virjednost __NOT_WRITTEN__ i preskače to polje
		// kad se snima password onda ga se hashira, svako hashiranje daje drugi rezultat, pa se ne može više puta ista šifra hashirat
		if ($request->hasPost('password')) {
            $pwd = $request->getPost('password');
            if ( $pwd != '__NOT_WRITTEN__' ) {
// 				//error_log("snima se pasword");
                $this->password = $this->getDI()->getShared('auth')->hash_password($pwd);
            } else {
// 				error_log ("preskocen pasword");
                // Avoids non-null default validation in case no (or empty) password was submitted
                $this->skipAttributesOnUpdate(array('password'));
            }
        }
		
		
		$jeGreska = false;
		
		if ( strlen($this->username) > 0 ){
			$users = \Baseapp\Suva\Models\Users::find( "username = '$this->username' " );
			if ($this->id > 0){
				if ( count($users) > 1) 
					$jeGreska = $this->greskaSnimanje("U sustavu već postoji ".(count($users))." korisnika s traženim Username-om", 'username');
			} else {
				if ( count($users) > 0) 
					$jeGreska = $this->greskaSnimanje("U sustavu već postoji ".(count($users))." korisnika s traženim Username-om", 'username');
			}
		}
		
		if ( strlen($this->email) > 0 ){
			$users = \Baseapp\Suva\Models\Users::find( "email = '$this->email' " );
			if ($this->id > 0){
				if ( count($users) > 1) 
					$jeGreska = $this->greskaSnimanje("U sustavu već postoji ".(count($users))." korisnika s traženim E-mailom", 'email');
			}
			else {
				if ( count($users) > 0) 
					$jeGreska = $this->greskaSnimanje("U sustavu već postoji ".(count($users))." korisnika s traženim E-mailom", 'email');
			}
		}
		
		//oib:
		if ( strlen($this->oib) > 0 ){
			$users = \Baseapp\Suva\Models\Users::find( "oib = '$this->oib' " );
			if ($this->id > 0){
				if ( count($users) > 1) 
					$jeGreska = $this->greskaSnimanje("U sustavu već postoji ".(count($users))." korisnika s istim OIB-om", 'oib');
			}
			else {
				if ( count($users) > 0) 
					$jeGreska = $this->greskaSnimanje("U sustavu već postoji ".(count($users))." korisnika s istim OIB-om", 'oib');
			}
		}
		// $this->oib = trim($this->oib);
		// //error_log("NIKOLA users beforeSave OIB : $this->oib");
		// $userIstiOib = $this->type > 0 && strlen($this->oib) > 0 ? \Baseapp\Suva\Models\Users::find( "type = $this->type and oib = '$this->oib'" ) : 0;
		// //error_log("models/Users.php $userIstiOib");
		
		// //ako ima više od jednog oiba za isti tip korisnika
		// if (count($userIstiOib) > 1) 
			// $jeGreska = $this->greskaSnimanje("U sustavu već postoji ".(count($userIstiOib)-1)." korisnika s traženim OIB-om", 'oib');

		//ako nije Hrvatska a unesen je oib javlja grešku
		elseif ( !($this->country_id == 1) && strlen($this->oib) > 0  ) 
			$jeGreska = $this->greskaSnimanje("In order to enter OIB country Hrvatska must be chosen", 'oib');

		//ako je Hrvatska i unesen je oib ali nije 11 znakova
		elseif ( $this->country_id == 1 && strlen($this->oib) > 0 && strlen(trim($this->oib)) != 11) 
			$jeGreska = $this->greskaSnimanje("OIB must be exactly 11 characters long", 'oib');

		//ako je Hrvatska i unesen je oib ali sadrži ne-numeričke znakove
		elseif ( $this->country_id == 1 && !ctype_digit($this->oib)) 
			$jeGreska = $this->greskaSnimanje("OIB must contain only numeric characters", 'oib');
	
		/*
		// n_vat_number 
		$this->n_vat_number = trim($this->n_vat_number);
		$userIstiVatNumber = ( $this->type > 0 && $this->n_vat_number > '' ) ? \Baseapp\Suva\Models\Users::find( "type = $this->type and n_vat_number = '$this->n_vat_number'" ) : 0;

		//ako ima više od jednog oiba za isti tip korisnika //ovo samo po sebi nije greska, trebalo bi biti upozorenje
		if (count($userIstiVatNumber) > 1) 
			$this->greskaSnimanje("U sustavu već postoji ".(count($userIstiVatNumber)-1)." korisnika s traženim VAT No.", 'n_vat_number');
		*/
		
		//ako nije unesena država a unesen je n_vat_number javlja grešku
		if ( !$this->country_id && strlen($this->n_vat_number) > 0  ) 
			$jeGreska = $this->greskaSnimanje("In order to enter VAT No. country must be chosen", 'n_vat_number');

		if ( !( $this->country_id >= 1 ) ) 
			$jeGreska = $this->greskaSnimanje("Please select a Country", 'country_id');
		
		if ( !($this->county_id > 1) && $this->country_id == 1 ) 
			$jeGreska = $this->greskaSnimanje("Za Hrvatsku je obavezna županija", 'county_id');
		
		if ( strlen($this->address) < 2 ) 
			$jeGreska = $this->greskaSnimanje("Address must be longer than 2 characters", 'address');
		if ( strlen($this->city) < 2 ) 
			$jeGreska = $this->greskaSnimanje("City must be longer than 2 characters", 'city');
		if ( strlen($this->zip_code) < 2 ) 
			$jeGreska = $this->greskaSnimanje("ZIP must be longer than 2 characters", 'zip_code');

		if ($this->type != 1 && $this->type != 2) 
			$jeGreska = $this->greskaSnimanje("Incorrect User Type", 'type');

		if (strlen(trim($this->first_name)) < 2 && $this->type == 1) 
			$jeGreska = $this->greskaSnimanje("First must be longer than 2 characters", 'first_name');
		
		if (strlen($this->last_name) < 2 && $this->type == 1) 
			$jeGreska = $this->greskaSnimanje("Last must be longer than 2 characters", 'last_name');

		if (strlen($this->company_name) < 2 && $this->type == 2) 
			$jeGreska = $this->greskaSnimanje("Company Name must be longer than 2 characters", 'company_name');
		
		
		// if (empty($_SESSION['_context']['selected_roles_users']) || !array_key_exists('selected_roles_users', $_SESSION['_context']))  $jeGreska = $this->greskaSnimanje("Potrebno je odabrati ROLU korisniku");
		
		if ($jeGreska) return "ERRORS:"; else return null;
		//error_log("NIKOLA users beforeSave1 jeGreska $jeGreska");
		
		
	}
	 
	public function n_after_save($request = null, $pCallingObject = null){
		
		$user = $this->id >0 ? \Baseapp\Suva\Models\Users::findFirst($this->id) : null;
		
		//ubacivanje korisnikovih rola ako su poslane u requestu
		if ($user && array_key_exists('_context', $_SESSION))
			if (array_key_exists('selected_roles_users', $_SESSION['_context'])){
				//error_log("asdasd");
				//error_log($this->password);
				//error_log("aaaa");
				//error_log($this->getDI()->getShared('auth')->hash_password('igor2012'));
				//error_log($this->getDI()->getShared('auth')->hash_password('igor2012'));
				\Baseapp\Suva\Library\Nava::runSql("delete from roles_users where user_id = $this->id");
				foreach ( $_SESSION['_context']['selected_roles_users'] as $ru )
					\Baseapp\Suva\Library\Nava::runSql("insert into roles_users (role_id, user_id) values( $ru, $this->id)");
					
					//error_log($this->getDI()->getShared('auth')->hash_password('igor2012'));
			}
		
		if ($this->id > 0){


			//Updateanje userscontacts prema podacima iz usera - ovo služi da bi moglo postojat brzo pretraživanje
			$firstName = strlen(trim($_REQUEST['first_name'])) > 0 ? trim($_REQUEST['first_name']) : ''; //mora '' radi kombinacije first + last
			$lastName = strlen(trim($_REQUEST['last_name'])) > 0 ? trim($_REQUEST['last_name']) : '';
			if($firstName || $lastName) $this->insertUsersContact('first_lastname', '1', "$firstName $lastName", 1);

			
			$companyName = strlen(trim($_REQUEST['company_name'])) > 0 ? trim($_REQUEST['company_name']) : null;
			if($companyName) $this->insertUsersContact('companyname', '1', $companyName, 1);

			$phone1 = strlen(trim($_REQUEST['phone1'])) > 0 ? trim($_REQUEST['phone1']) : null;
			if($phone1) $this->insertUsersContact('phone', '1', $phone1, 0);

			$phone2 = strlen(trim($_REQUEST['phone2'])) > 0 ? trim($_REQUEST['phone2']) : null;
			if($phone2) $this->insertUsersContact('phone', '2', $phone2, 0);

			$email = strlen(trim($_REQUEST['email'])) > 0 ? trim($_REQUEST['email']) : null;
			if($email) $this->insertUsersContact('email', '1', $email, 0);

			$username = strlen(trim($_REQUEST['username'])) > 0 ? trim($_REQUEST['username']) : null;
			if($username) $this->insertUsersContact('username', '1', $username, 1);
			
			$vat = 	strlen(trim($_REQUEST['oib'])) > 0 ? trim($_REQUEST['oib']) : null;
			if($vat) $this->insertUsersContact('vat', '1', $vat, 1);
		}
		
		// trenutno zakomentirano radi return-a
// 		$oib = (int)$_REQUEST['oib'];
// 		if($oib > 0){
// 			$oib_list = Nava::sqlToArray("select oib from users where oib = $oib");

// 			if($oib_list[1] > 0){
// 				return ("oib vec postoji u bazi");
// 			}
// 		}
	}
		
	public function n_generateUsernamePassword(){
		$rs = \Baseapp\Suva\Library\Nava::sqlToArray("select max(id) as id from users");
		$maxid = $rs[0]['id'] + 1;
		$this->username = "user_$maxid";
		$pwd = $this->generateRandomString(10);
		$this->password = $this->getDI()->getShared('auth')->hash_password($pwd);
		return;
	}
	
	public function insertUsersContact(  $pType, $pSubType, $pName, $pHidden ){
		//error_log ("insertUsersContact 1");
		if (!$this->id > 0 ) return;
		
		$strWhere = " user_id = $this->id and type = '$pType' and sub_type = '$pSubType'";
		//error_log ("insertUsersContact 2 strWhere: $strWhere");
		$uc = \Baseapp\Suva\Models\UsersContacts::findFirst( $strWhere );
		
		if ($uc){
			//error_log ("insertUsersContact 3 EXISTS");
			$uc->name = $pName;
			$uc->search_string = $uc->makeSearchString();
			$uc->update();
			//error_log ("insertUsersContact 3 UPDATE: success: $success");
		}
		else {
			//error_log ("insertUsersContact 4 DOES NOT EXIST");
			$uc = new \Baseapp\Suva\Models\UsersContacts();
			$uc->user_id = $this->id;
			$uc->type = $pType;
			$uc->sub_type = $pSubType;
			$uc->name = $pName;
			$uc->is_hidden = $pHidden;
			$uc->search_string = $uc->makeSearchString();
			$success = $uc->create();
			$greske = $uc->getMessages();
			foreach ($greske as $greska)  
				$err_msg .= $greska.", ";
			//error_log ("insertUsersContact 3 CREATE: success: $success, greske: $err_msg");
		}
			
	}
	
	public function getFiscalLocationName( $pUserId = null ){
		
		$userId = $pUserId ? $pUserId : $this->id;
		
 		$fiscalLocationName = Nava::sqlToArray("select name from n_fiscal_locations fl join users u on (fl.id = u.n_fiscal_location_id) where u.id = $userId ")[0]['name'];
		return $fiscalLocationName ? $fiscalLocationName : 'N/A';
	}

	public function getCart(){
		$this->poruka("getCart User.php 471");
		if (! $this->id > 0) return null;
		$order = new \Baseapp\Suva\Models\Orders();
		return $order->getCartForUser($this->id);
	}
	
	public function isAllowed($pActionName = null){
		if (!$pActionName) return false;
		if (!$this->id > 0) return false;
		
		$action = \Baseapp\Suva\Models\Actions::findFirst("name = '$pActionName' and is_active = 1");
		if (!$action) return false;
		$userId = $this->id;
		
		//traženje permissiona za userId
		if (!array_key_exists('_permissions',$_SESSION))
			$_SESSION['_permissions'] = array();
		
		//dodavanje liste permissiona
		// if (!array_key_exists($userId, $_SESSION['_permissions']))
			$_SESSION['_permissions'][$userId] = \Baseapp\Suva\Library\Nava::getPermissionsArray($userId);
		
		if ($_SESSION['_permissions'][$userId][$pActionName] === true) 
			return true;
		else 
			return false;
		
		//error_log ("User::isAllowed  1 txtSql: $txtSql");
	}	
	//polja many2one
	public function Country(){
		return \Baseapp\Suva\Models\Locations::findFirst($this->country_id);
	}
		
	public function County(){
		return \Baseapp\Suva\Models\Locations::findFirst($this->county_id);
	}
	
	public function Agency(){
		return \Baseapp\Suva\Models\Users::findFirst($this->n_agency_id);
	}

	
	public function PaymentType(){
		$id = $this->n_payment_type_id > 0 ? $this->n_payment_type_id : -1;
		return \Baseapp\Suva\Models\PaymentTypes::findFirst($id);
	}
	
	//polja one2many

	public function Ads ($p = null) { return $this->getMany ("Ads", $p, " user_id =  ".($this->id > 0 ? $this->id : -1)); }
	public function Orders ($p = null) { return $this->getMany ("Orders", $p, " user_id =  ".($this->id > 0 ? $this->id : -1)); }
	public function RolesUsers ($p = null) { return $this->getMany ("RolesUsers", $p, " user_id =  ".($this->id > 0 ? $this->id : -1)); }
	public function UsersContacts ($p = null ) { return $this->getMany ("UsersContacts", $p, " user_id =  ".($this->id > 0 ? $this->id : -1)); }
	

	
	//veze many2Many
	public function Roles(){
		$rus = $this->RolesUsers();
		$txtIn = "-1";
		foreach($rus as $ru)
			$txtIn = $txtIn.", ".$ru->role_id;
		$result = \Baseapp\Suva\Models\Roles::find(" id in ( $txtIn ) ");
		return $result;
	} 	 
	
}
