<br/>

<div class="row">
	<div class="col-lg-12 col-md-12">
	<b>AD FROM AVUS</b> [ ORDER ID:<b>{{ad.n_avus_order_id}}</b> ] [ CLASS ID: <b>{{ad.n_avus_class_id}}</b> ] [ CUSTOMER ID: <b>{{ ad.n_avus_customer_number }}</b> ] [ ADLAYOUT ID: <b>{{ ad.n_avus_adlayout_id }}</b> ]
	</div>

</div>
<div class="row">
	<div class="col-lg-2 col-md-2 text-right">
		SUVA PRODUCT:
	</div>
	<div class="col-lg-10 col-md-10">
		{% set _layoutId = ( ad.n_avus_adlayout_id > 0 ? ad.n_avus_adlayout_id : -1 ) %}
		{% set _product = _model.Product(" avus_adlayout_id = " ~ _layoutId ) %}
		{% set name = ( _product ? _product.id ~ ' - ' ~ _product.name : 'N/A' ) %}
		{{ name }}
	</div>
</div>
<div class="row">
	<div class="col-lg-2 col-md-2 text-right">
		SUVA USER:
	</div>
	<div class="col-lg-10 col-md-10">
		{% set _customerNumber = ( ad.n_avus_customer_number > 0 ? ad.n_avus_customer_number : -1 ) %}
		{% set _user = _model.User(" n_avus_customer_id = " ~ _customerNumber ) %}
		{% set name = ( _user ? _user.id ~ ' - ' ~ _user.n_avus_customer_number ~ ' ' ~ _user.first_name ~ ' ' ~ _user.last_name ~ ' ' ~ _user.company_name: 'N/A' ) %}
		{{ name }}
	</div>
</div>

<div class="row">
	<div class="col-lg-2 col-md-2 text-right">
		SUVA CATEGORY:
	</div>
	<div class="col-lg-10 col-md-10">
		{% set _classId = ( ad.n_avus_class_id > 0 ? ad.n_avus_class_id : -1 ) %}
		{% set _catMap = _model.CategoryMapping(" avus_class_id = " ~ _classId ) %}
		{% set name = ( _catMap ? _catMap.id ~ ' - ' ~ _catMap.n_path : 'N/A' ) %}
		{{ name }}
	</div>
</div>
<div class="row">
	<div class="col-lg-2 col-md-2 text-right">
		INSERTIONS:
	</div>
	<div class="col-lg-10 col-md-10">
		{% for _ins in _model.InsertionsAvus ("ad_id = " ~ ad.id) %}
			{% set _issue = _model.Issue( _ins.issue_id ) %}
			{% set name = ( _issue ? "Issue " ~ _issue.id ~ ' - ' ~ date("d.m.Y", strtotime(_issue.date_published)) : 'N/A' ) %}
			{{ name }}
			<br/>
		{% endfor %}
	</div>
</div>
<br/>