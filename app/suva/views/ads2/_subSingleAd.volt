<!-- subSingleAd.volt -->

{#

	PARAMETRI:
		ad

#}

{# AJAX BLOK - NA SVAKU FORMU #}
{% if _form_id is not defined %}{% set _form_id = '1' %}{% endif %}
{% if _is_ajax_submit is not defined %}{% set _is_ajax_submit = false %}{% endif %}
{% set _subform_id = 0 %}

{% if user is defined %}
	

	<!-- GORNJI DIO SA USER DETAILS, CONTACT INFO, INSERTIONS -->
	<div class="row">
		<!-- USER DETAILS -->
		<div class="col-md-2 col-lg-2">
			<div class="panel panel-default" style="overflow-x:scroll; height:160px; white-space:nowrap;">

				<div class="panel-heading">
					<h3 class="panel-title">
						User {{ linkTo(['suva/korisnici/crud/' ~ ad.user_id, ad.User().username, 'target': '_blank', 'class':'username_redirect ']) }}
						&nbsp;[ID:<b>{{ ad.user_id }}</b>]
					</h3>				
				</div>
				<div class="panel-body">
					<div class="tab-content">
						<div class="row">
							<div class="col-md-6 col-lg-6">
								<span>{{ad.User().company_name}}</span>
							</div>
						</div>
						<div class="row">
							<div class="col-md-6 col-lg-6">
								<span>{{ad.User().first_name}} {{ad.User().last_name}}</span>
							</div>
						</div>
						<div class="row">
							<div class="col-md-6 col-lg-6">
								<span>{{ad.User().address}}</span>
							</div>
						</div>
						<div class="row">
							<div class="col-md-6 col-lg-6">
								<span>{{ad.User().zip_code}} {{ad.User().city}}</span>
							</div>
						</div>
						<div class="row">
							<div class="col-md-6 col-lg-6">
								<span>OIB:{{ad.User().oib}}</span>
							</div>
						</div>							
					</div>
				</div>
			</div>
		</div>
		
		<!-- CONTACT INFO -->
		<div class="col-md-3 col-lg-3">
			{{ partial("partials/wraSubFormAjaxV2",[
				'_window_id' : 'subUsersContactsSingle'
				
			] ) }}
		</div>
		
		<!-- INSERTIONS -->
		<div class="col-md-7 col-lg-7">
			{% set _subform_id += 1 %}
			{{ partial("partials/wraSubFormAjaxV2",[
				'_window_id' : 'subInsertionsSingle'
			] ) }}
		</div>
	</div>		

	<!-- DONJI DIO S EDITOROM AD-a, PUBLICATIONS-ISSUES, MODERATION, AD INFO -->
	<div class="row">
	
		<!--LIJEVA POLOVINA EKRANA  - EDITOR AD-a-->
		<div class="col-lg-8 col-md-8">
			{{ partial("partials/wraSubFormAjaxV2",[
				'_window_id' : 'crudSingleAd'
			] ) }}
		</div>

		<!-- DESNA POLOVINA EKRANA -->
		
		<div class="col-md-4 col-lg-4">
			{{ partial("partials/wraSubFormAjaxV2",[
				'_window_id' : 'subPublicationsAndIssuesSingle'
			] ) }}	
			
subModeration
			{{ partial("partials/wraSubFormAjaxV2",[
				'_window_id' : 'subModerationSingle'
			] ) }}	
			
		</div> 
	</div>

{% else %}
	<h1>ERROR: subSingleAd.volt  USER MUST BE DEFINED TO INVOKE THIS FORM</h1>
{% endif %}

