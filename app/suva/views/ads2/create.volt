
{%  if array_key_exists( 'selected_user_id', _context) %}
	{% set _selected_user = _model.User ( _context['selected_user_id'] ) %}

<style>
 
 .btn-group {
	visibility:hidden;
 
 }

</style>


	{# Admin Ad Edit/Create View #}
	<div class="row">
		<div class="col-lg-12 col-md-12">
			<h1 class="page-header"><!-- Ads --> <small>{{ form_title_long }}</small></h1>
			{{ flashSession.output() }}

			<div class="row">
			{{ form(NULL, 'id':'frm_ads', 'method':'post', 'autocomplete':'off', 'data-type':'ads') }}
				{{ hiddenField('next') }}
				{{ hiddenField('_csrftoken') }}
				{{ hiddenField(['category_id', 'value': category.id]) }}
				{{ hiddenField(['user_id', 'value': _selected_user.id]) }}
				
				<div class="col-md-9 col-lg-9">
					<div>
						{% if ad is defined and ad.id is defined %}
						<div class="pull-right">
							<a target="_blank" href="{{ ad.get_frontend_view_link() }}" class="btn btn-xs btn-default"><i class="fa fa-fw fa-eye"></i>View ad</a>
						</div>
						{% endif %}
						<h4>Ad details</h4> <button class="btn btn-primary btn-xs" type="submit" name="save">Check mandatory fields</button>
					</div>
					
					<!-- CONTACT INFO -->
					<div>
						{{ partial("partials/wraSubFormAjaxV2",[
							'_window_id' : 'subUsersContactsSingle'
							,'_url' : '/suva/users-contacts/index/' ~ _selected_user.id
							
						] ) }}
					</div>
					
					
					

					<hr />

					{% if ad is defined and ad.id is defined  %}

					{% include('chunks/infraction-reports') %}

					{% if can_be_moderated %}
					<div class="panel panel-default">
						<div class="panel-heading">
							<h3 class="panel-title">Ad moderation</h3>
						</div>
						<div class="panel-body">
							<div class="row">
								<div class="col-lg-3 col-md-4">
									<div class="form-group">
										<div class="icon_dropdown">
											<label class="control-label" for="moderation">Status</label>
											{{ moderation_dropdown }}
										</div>
									</div>
								</div>
								{% if moderation_reasons and moderation_reasons is iterable %}
								<div id="moderation_reason_box" class="col-lg-9 col-md-8">
									{% for reason, reason_dropdown in moderation_reasons %}
										{% set field = 'moderation_reason_' ~ reason %}
										<div id="{{ field }}_box" class="form-group moderation-reason-group{{ errors is defined and errors.filter(field) ? ' has-error' : '' }}"{{ (ad.moderation != 'ok' ? ' style="display:none"' : '') }}>
											<label class="control-label" for="{{ field }}">{{ reason|upper }} Reason</label>
											{{ reason_dropdown }}
											{%- if errors is defined and errors.filter(field) -%}
											<p class="help-block">{{- current(errors.filter(field)).getMessage() -}}</p>
											{%- endif -%}
										</div>
									{% endfor %}
								</div>
								{% endif %}
							</div>
							<div class="row">
								<div class="col-lg-12 col-md-12">
									<button class="btn btn-primary pull-right" type="submit" name="savemoderation">Save moderation status</button>
									{% if moderation_reasons and moderation_reasons is iterable %}
									<div id="reason_email_box" class="checkbox checkbox-primary pull-right" style="margin-right:20px;">
										<input name="send_reason_email" id="send_reason_email" type="checkbox" checked="checked">
										<label class="control-label" for="send_reason_email"> Send reason email</label>
									</div>
									{% endif %}
								</div>
							</div>
						</div>
					</div>
					{% else %}
					<div class="panel panel-danger">
						<div class="panel-heading">
							<h3 class="panel-title"><span class="fa fa-exclamation-triangle fa-fw"></span> Warning! Ad cannot be moderated!</h3>
						</div>
						<div class="panel-body">
							<div class="text-danger">{{ moderation_warning }}</div>
						</div>
					</div>
					{% endif %}
					{% endif %}

					<div class="row">
						<div class="col-md-8 col-lg-8">
							<div class="form-group">
								<label class="control-label">Ad Category</label>
								{% if ad is defined and ad.id is defined and auth.logged_in(['admin','supersupport','support','sales','moderator']) %}
								<div class="input-group">
									<input class="form-control" type="text" value="{{ category.getPath() }}" disabled="disabled" />
									<span class="input-group-btn">
										<button class="btn btn-default" id="changeCategoryBtn" type="button">
											<span class="fa fa-folder fa-fw"></span> Change
										</button>
									</span>
								</div>
								{% else %}
								<input class="form-control" type="text" value="{{ category.getPath() }}" disabled="disabled" />
								{% endif %}
							</div>
						</div>

					</div>
					{% if ad is defined and ad.id is defined %}
					<div class="row">
						<div class="col-md-12 col-lg-12">
							<label>ID:</label> {{ ad.id }}
							{% if ad.import_id %}
								<label>ImportID:</label> {{ ad.import_id }}
							{% endif %}
						</div>
					</div>
					{% endif %}
					<div id="ads_category_parameters">{{ rendered_parameters }}</div>
					<div class="panel panel-default panel-heading-has-buttons">
						<div class="panel-heading">
							<div class="pull-right"><span id="refreshPhoneNumbers" class="btn btn-primary btn-xs" title="Prefill with user's public phones"><span class="fa fa-phone"></span></span></div>
							<h3 class="panel-title">Dodatni kontakt podaci</h3>
						</div>
						<div class="panel-body">
							<div class="row">
								{% set ad_phone1 = '' %}
								{% set ad_phone2 = '' %}
								
								{% if ad is defined %}
									{% if _selected_user.phone1_public AND _selected_user.phone1_public == 1 %}
										{% set ad_phone1 = ( ad.phone1 ? ad.phone1 : _selected_user.phone1 ) %}
									{% endif %} 
									{% if _selected_user.phone2_public AND _selected_user.phone2_public == 1 %}
										{% set ad_phone2 = ( ad.phone2 ? ad.phone2 : _selected_user.phone2 ) %}
									
									{% endif %}
								{% endif %}
								<div class="col-lg-3 col-md-3">
									<div class="form-group{{ errors is defined and errors.filter('phone1') ? ' has-error' : '' }}">
										<label class="control-label" for="phone1">Telefon #1</label>
										<input class="form-control" name="phone1" id="phone1" value="{{ ad_phone1 }}" type="text">
										{%- if errors is defined and errors.filter('phone1') -%}
										<p class="help-block">{{- current(errors.filter('phone1')).getMessage() -}}</p>
										{%- endif -%}
									</div>
								</div>
								<div class="col-lg-3 col-md-3">
									<div class="form-group">
										<label class="control-label" for="phone2">Telefon #2</label>
										<input class="form-control" name="phone2" id="phone2" value="{{ ad_phone2 }}" type="text">
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>

				<div class="col-md-3 col-lg-3">
					{% if ad is defined and ad.id is defined %}
					<table class="table table-condensed table-striped table-bordered">
						<tr{{ ad.latest_payment_state == constant('Baseapp\Models\Ads::PAYMENT_STATE_CANCELED_ORDER') ? ' class="danger"' : '' }}>
							<td>Last payment state</td>
							<td><span class="fa fa-money"></span> {{ Models_Ads__getPaymentStateString(ad.latest_payment_state) }}</td>
						</tr>
						<tr>
							<td>Created</td>
							<td><span class="fa fa-clock-o"></span> {{ date('Y-m-d H:i:s', ad.created_at) }}</td>
						</tr>
						<tr>
							<td>Last modified</td>
							<td><span class="fa fa-clock-o"></span> {{ date('Y-m-d H:i:s', ad.modified_at) }}</td>
						</tr>
						<tr>
							<td>Virtual publish date (sort)</td>
							<td><span class="fa fa-clock-o"></span> {{ date('Y-m-d H:i:s', ad.sort_date) }}</td>
						</tr>
						<tr>
							<td>First published at</td>
							<td><span class="fa fa-clock-o"></span> {{ date('Y-m-d H:i:s', ad.first_published_at) }}</td>
						</tr>
						<tr>
							<td>Last published at</td>
							<td><span class="fa fa-clock-o"></span> {{ date('Y-m-d H:i:s', ad.published_at) }}</td>
						</tr>
						{% set classname = ad.isExpired() ? 'danger' : 'success' %}
						<tr class="{{ classname }}">
							<td>Expire{{ ad.isExpired() ? 'd' : 's' }}</td>
							<td><span class="fa fa-clock-o"></span> {{ date('Y-m-d H:i:s', ad.expires_at) }}</td>
						</tr>
						{% set classname = ad.active ? 'success' : 'danger' %}
						<tr class="{{ classname }}">
							<td><b>Currently active</b></td>
							<td><span class="fa fa-{{ ad.active ? 'check' : 'close' }}"></span> {{ ad.active ? 'YES' : 'NO' ~ (ad.sold ? ' (marked as SOLD)' : '') }}</td>
						</tr>
					</table>
					<div class="panel panel-default">
						<div class="panel-heading">Ad's remark</div>
						<div class="panel-body">
							<div class="form-group">
								<textarea class="form-control" id="ads_remark" rows="5" cols="40" data-ad-id="{{ ad.id }}">{{ ads_remark }}</textarea>
							</div>
						</div>
						<div class="panel-footer text-right">
							<span class="btn btn-primary btn-sm" id="save_remark">Save remark</span>
						</div>
					</div>
					{% endif %}
					{% if products_chooser_markup is defined %}
					<section class="product-section">
						<h4 id="product-section-title" data-offline-exportable="{{ is_offline_exportable|default(false) ? 'true' : 'false' }}">Product</h4>
						<hr>
						{% if show_pushup_button is defined and show_pushup_button %}
							<button type="submit" class="btn btn-primary" name="pushup">Push up</button>
							<hr>
						{% endif %}
						{{ products_chooser_markup }}
						{% if not(is_offline_exportable|default(false)) %}
						<div id="offline-product-warning" class="alert alert-warning">
							<small>
								Currently presented offline product is <strong>not scheduled for export to AVUS</strong> 
								(it is something that user ordered in the past) and, <strong>unless changed</strong> to 
								something else, <strong>will not be exported to AVUS</strong>!
							</small>
						</div>
						{% endif %}
					</section>
					{% endif %}
				</div>

				{%- if (not(ads_related_orders is empty)) -%}
				<div class="col-lg-12 col-md-12">
					<br>
					<div class="panel panel-default panel-heading-has-buttons">
						<div class="panel-heading">
							{% if auth.logged_in(['admin','supersupport']) %}
								<div class="pull-right">
									<a class="btn btn-xs btn-primary" href="{{ url('admin/orders', ['field': 'ad_id', 'mode': 'exact', 'q': ad.id]) }}" target="_blank">Find all <span class="fa fa-fw fa-external-link"></span></a>
								</div>
							{% endif %}
							<h3 class="panel-title"><span class="fa fa-credit-card fa-fw"></span> Ad's latest orders</h3>
						</div>
						<div class="panel-body">
							<div class="list-group">
							{%- for order in ads_related_orders -%}
							{% if auth.logged_in(['admin','supersupport']) %}
							<a href="{{ url('admin/orders?field=id&mode=exact&q=' ~ order.id) }}" class="list-group-item list-group-item-info">
								<span class="fa fa-credit-card fa-fw"></span>
								ID: {{ order.id }} <small class="text-muted">PBO: {{ order.getPbo() }}</small> <small>{{ order.payment_method }}</small>

								{{ order.getLabeledStatusText() }}
							</a>
							{% else %}
							<span class="list-group-item list-group-item-info">
								<span class="fa fa-credit-card fa-fw"></span>
								ID: {{ order.id }} <small class="text-muted">PBO: {{ order.getPbo() }}</small> <small>{{ order.payment_method }}</small>

								{{ order.getLabeledStatusText() }}
							</span>
							{% endif %}
							{%- endfor -%}
							</div>
						</div>
					</div>
				</div>
				{%- endif -%}

				<div class="col-lg-12 col-md-12">
					{% if show_save_buttons %}
					<button class="btn btn-primary" type="submit" name="save">Save</button>
					<!-- <button class="btn btn-primary" type="submit" name="saveexit">Save and exit</button> -->
					{% endif %}
					<!-- <button class="btn btn-default" type="submit" name="cancel">Cancel</button> -->
				</div>
			{{ endForm() }}
			</div>
		</div>
	</div>
	{% if ad is defined and ad.id is defined and auth.logged_in(['admin','supersupport','support','sales','moderator']) %}
	<!-- Change Ad's Category Modal -->
	<div class="modal fade" id="changeAdsCategoryModal" tabindex="-1" role="dialog" aria-labelledby="Change Ad's Category" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<h4 class="modal-title">Change Ad's Category</h4>
				</div>
				<div class="modal-body">
					<div class="row">
						<div class="col-lg-12 col-md-12">
							{{ form('suva/ads2/create', 'id' : 'frm_createNewAd', 'method': 'get') }}
								<div class="form-group">
									<label class="control-label" for="new_category_id">New category</label>
									{{ new_category_id_dropdown }}
								</div>
							{{ endForm() }}
						</div>
					</div>
					<div class="row hidden">
						<div class="col-lg-12 col-md-12">
							<div>
								<strong>Parameters that can be copied</strong><br />
								<div id="matching_parameters" class="text-danger"></div>
							</div>
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-primary disabled" data-dismiss="modal" data-target-category="" id="changeAdsCategoryModalMoveBtn">Move!</button>
					<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
				</div>
			</div>
		</div>
	</div>
	{% endif %}

{% else %}
	<H1>ERROR: User must be chosen to invoke this form </H1>
	{% set _selected_user = ad.User %}

{% endif %}



