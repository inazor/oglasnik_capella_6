{% if _is_ajax_submit is not defined %}
	{% set _is_ajax_submit = '' %}
{% endif %}

{%  set _form_id = 'frmCrudDisplay' %}
{%  set _window_id = 'crudDisplay' %}
{%  set _url = '/suva/ads2/crudDisplay/' ~ ( entity.id > 0 ? entity.id : 0) ~ '/' ~ ( entity.user_id > 0 ? entity.user_id : '' )%}
{%  set _function_to_execute_after_load = 'ctlFileUploadAjax' %}

{% if entity.user_id > 0 %}
	{% set _additional_js_onclick_save = 
		"ajaxSubmit ( '/suva/users-contacts/index/" ~ entity.user_id ~ "', 'subUsersContactsDisplay' );
		ajaxSubmit( '/suva/ads2/subInsertions/" ~ entity.id ~ "' , 'subInsertionsDisplay'  );
		" %}
{% else %}
	{% set _additional_js_onclick_save = ''%}
{% endif %}
crudDisplay.volt

{#{ var_dump(_context) }#}

<div class="row">
	<div class="col-lg-12 col-md-12">
	{% if  _is_ajax_submit == false %}
		{{ partial("partials/blkFlashSession") }}
	{% endif %}
	{#{ partial("partials/blkFlashSession") }#}
	
		<div>
			<h1 class="page-header">
				{% if entity.id is not defined %}
					New Display Ad
				{% else %}
					Display Ad {{ entity.id }}
				{% endif %}
			</h1>
		</div>

		{% if entity is defined %}
			{#% set _context['ajax_edit'] = true %#}
		
			{% set ad = entity %}
			{% if ad.n_source == 'avus' %}
				{{ partial("ads2/blkAvusAdInfo") }}
			{% endif %}
		
			{{ form(NULL, 'id' : _form_id, 'method' : 'post', 'autocomplete' : 'off') }}
				{{ hiddenField('_csrftoken') }}
				{{ hiddenField('next') }}
				<input type="hidden" name="id" value="{{entity.id}}"></input>
				<input type="hidden" name="category_id" value="{{entity.id}}"></input>
				<input type="hidden" name="created_at" value="{{entity.created_at}}"></input>
				<input type="hidden" name="description" value="{{entity.description}}"></input>
				<input type="hidden" name="product_sort" value="{{entity.product_sort}}"></input>
				<input type="hidden" name="category_id" value="919"></input>
				<div class="row">
					<div class="col-lg-12 col-md-12">
						<div class="panel panel-default">
							<div class="panel-heading">
								{% if entity.id is defined %}
									<div class="pull-right">[ID: <b>{{ entity.id }}</b> ]</div>
								{% endif %}
								<h3 class="panel-title">Display Ad details</h3>
							</div>
							<div class="panel-body">
								<div class="row">
									<div class="col-lg-1 col-md-1">
										{{  (entity.category_id > 0 ? _model.Category(entity.category_id).name  : '' ) }}
									</div>
									{{ partial("partials/ctlTextTypeV3",[
										'title' : "User"
										, 'field' : 'user_id'
										, 'value' : entity.user_id
										, 'width' : 5
										, '_context' : _context
										, 'value_show' :  (entity.user_id > 0 ? _model.User( entity.user_id ).username : '')
										, 'model_name' : 'Users' 
										, 'expr_search' : 'username'
										, 'expr_value' : 'id' 
										, 'expr_show_input' : 'username'
										, 'expr_show_list' : 'username'
										, 'readonly' : true
										, 'additional_where' : ' order by username '
									]) }}
								</div>
								<div class="row">
								
									{{ partial("partials/ctlTextTypeV3",[
										'title' : "Publication"
										, 'field' : 'n_publication_id'
										, 'value' : entity.n_publication_id
										, 'width' : 3
										, '_context' : _context
										, 'value_show' :  _model.Publication( entity.n_publication_id ).name
										, 'model_name' : 'Publications' 
										, 'expr_search' : 'name'
										, 'expr_value' : 'id' 
										, 'expr_show_input' : 'name'
										, 'expr_show_list' : 'name'
										, 'additional_where' : ' and is_active = 1 order by name '
									]) }}
							
									{{ partial("partials/ctlTextTypeV3",[
										'title' : "Publication Category"
										, 'field' : 'n_category_mapping_id'
										, 'value' : entity.n_category_mapping_id
										, 'width' : 9
										, '_context' : _context
										, 'value_show' :  _model.CategoryMapping( entity.n_category_mapping_id ).n_path
										, 'model_name' : 'CategoriesMappings' 
										, 'expr_search' : 'n_path'
										, 'expr_value' : 'id' 
										, 'expr_show_input' : 'n_path'
										, 'expr_show_list' : 'n_path'
										, 'additional_where' : ' and active = 1 and n_publications_id = __P1__  order by n_path '
										, 'input_P1' : 'n_publication_id'
									]) }}
								</div>
								<div class="row">
									
									{{ partial("partials/ctlText",[
										'value':entity.title
										, 'title':'Title'
										, 'width': 12
										, 'field':'title'
										, '_context': _context
									] )}}
									
									{{ partial("partials/ctlText",[
										'value':entity.n_remark
										, 'title':'Remark'
										, 'width': 12
										, 'field':'n_remark'
										, '_context': _context
									] )}}
									
									{{ partial("partials/ctlText",[
										'value':entity.n_positioning_desc
										, 'title':'Positioning Description'
										, 'width': 12
										, 'field':'n_positioning_desc'
										, '_context': _context
									] )}}
																															
									{{ partial("partials/ctlFileUpload",[
										'value':entity.title
										, 'title':'File Upload'
										, 'width': 12
										, 'field':'title'
										, 'ad' : ( entity.id > 0 ? entity : null ) 
										, '_context': _context
									] )}}
								</div>																						
							</div>
						</div>
					</div>
				</div>

				{% set _is_saveexit = false %}
				{% set _is_js_debug = false %}
				{{ partial ("partials/btnSaveCancel") }}

			{{ endForm() }}
		{% else %}
			ENTITY NIJE DEFINIRAN
		{% endif %}
	</div>
	
	
		
		
	
</div>


