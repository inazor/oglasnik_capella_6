{# Admin Ads Listing/View #}
{# CONTEXT<pre>{{ print_r(_context, true ) }}</pre>#} 

	{# Admin User Listing/View #}
	
	<h1>
		{% if form_title is defined %}
			{{ form_title }}
		{% else %}
			Ads
		{% endif %}
	</h1> 
	
	<div class="row">
		<div class="col-lg-12 col-md-12">
			<br/>
			{{ partial("partials/blkFlashSession") }}
			<table class="table table-striped table-hover table-condensed">
			{{ partial("partials/blkTableSort",[
			'_context' : _context,
			'fields':[ 
				['title':'Options'											,'width':200	,'show_navigation' : true ]
				
				,['title':'ID'				,'name':'id'				,'width':100		,'search_ctl' : 'ctlNumeric'	]
				,['title':'Username'		,'name':'username'			,'width':200	] 
				,['title':'Mode ration'		,'name':'moderation'		,'width':100		,'search_ctl' : 'ctlDropdown'	,'search_ctl_params': [ 'list': ['','ok','nok','waiting','reported'], 'no_ids':1 ] ] 
				,['title':'Title'			,'name':'title'				,'width':200	,'search_ctl' : 'ctlText'	] 
				,['title':'Description'		,'name':'description'		,'width':400	,'search_ctl' : 'ctlText'	] 
				,['title':'Category'		,'name':'category_id'		,'width':200	,'search_ctl' : 'ctlDropdown'	,'search_ctl_params': [ 'list': _model.Categories(), 'option_fields':['n_path'] ]] 
				,['title':'Moderation'		,'name':'moderation'		,'width':200	,'search_ctl' : 'ctlDropdown'	,'search_ctl_params': [ 'list': [ 'waiting', 'ok', 'nok', 'reported' ], 'no_ids' : true ]]
				,['title':'Source'			,'name':'n_source'			,'width':150	,'search_ctl' : 'ctlDropdown'	,'search_ctl_params': [ 'list': [ 'frontend','cc','avus','n/a','backend' ], 'no_ids' : true ]]
				,['title':'Active'			,'name':'active'			,'width':100		,'search_ctl' : 'ctlNumeric'	]

				,['title':'Soft delete'		,'name':'soft_delete'		,'width':100	,'search_ctl' : 'ctlNumeric'	] 
				,['title':'Avus AdLayout Id','name':'n_avus_adlayout_id','width':100	,'search_ctl' : 'ctlText'	]
				,['title':'Avus Class Id'	,'name':'n_avus_class_id'	,'width':100	,'search_ctl' : 'ctlText'	]
				,['title':'Avus Order Id'	,'name':'n_avus_order_id'	,'width':100	,'search_ctl' : 'ctlText'	]
				,['title':'Avus Customer Number'	,'name':'n_avus_customer_number'	,'width':100	,'search_ctl' : 'ctlText'	]
				,['title':'Dont Sync with Avus','name':'n_dont_sync_with_avus','width':100	,'search_ctl' : 'ctlText'	]
				
				] ]) }}
				{#,['title':'Related Info'						,'width': 150	] #}
				
				
				{% for id in ids %}
					{% set entity = _model.getOne('Ads', id) %}
					
					
					{% set _context['ajax_edit'] = true %}
					{% set _context['current_entity_id'] = entity.id %}
					
					
					
					{# prikazi sve ads osim onih koji su SOFTDELETED ! soft delete je nepovratna funkcija i nema ih smisla prikazivati #}
					
						<tr {% if entity.soft_delete == 1 %} style="background-color:#D5D5D5"; {% endif %}>
							<td>
								{% if entity.soft_delete != 1 %}
									{{ linkTo( 'suva/ads2/softDelete/'~ entity.id, '<span class="fa fa-times">Soft Delete</span> ') }}
								{% endif %}
								{{ linkTo('suva/ads2/edit/'~ entity.id~ '/' ~ entity.user_id, '<span class="btn btn-info">Edit</span> ') }}
							</td>
							<td>{{ entity.id }}</td>
							<td><a href="/suva/ads2/edit/0/{{entity.user_id}}">{{ entity.User().username }}</a></td>
							<td>{{ entity.moderation }}</td>
							<td>{{ entity.title }}</td>
							<td>{{ entity.description }}{% if entity.n_is_display_ad == 1 %} <span style="margin-left:80px; color:#FF4646;">display-ad</span> {% endif %}</td>
							<td>{{ _model.Category(entity.category_id).n_path }} </td>						
							<td>{{entity.moderation}}</td>
							<td>{{entity.n_source}}</td>
							<td>{{ partial("partials/ctlCheckbox",['value':entity.active, 'readonly':1 ] ) }}</td>
							<td>{{ partial("partials/ctlCheckbox",['value':entity.soft_delete, 'readonly':1 ] ) }}</td>
							<td style="text-align:center;">
								{{ partial("partials/ctlText",[
									'value':entity.n_avus_adlayout_id, 
									'field':'n_avus_adlayout_id', 
									
									'_context': _context
								] ) }}	
							</td>
							<td style="text-align:center;">
								{{ partial("partials/ctlText",[
									'value':entity.n_avus_class_id, 
									'field':'n_avus_class_id', 
									
									'_context': _context
								] ) }}	
							</td>
							<td style="text-align:center;">
								{{ partial("partials/ctlText",[
									'value':entity.n_avus_order_id, 
									'field':'n_avus_order_id', 
									
									'_context': _context
								] ) }}	
							</td>
							<td style="text-align:center;">
								{{ partial("partials/ctlText",[
									'value':entity.n_avus_customer_number, 
									'field':'n_avus_customer_number', 
									
									'_context': _context
								] ) }}	
							</td>
							
							
							<td style="text-align:center;">
								{{ partial("partials/ctlCheckbox", [
									'value' : entity.n_dont_sync_with_avus, 
									'field' : 'n_dont_sync_with_avus',
									'_context' : _context
								] ) }} 
															
							</td>
							
							
							{#
							<td>
								{{ linkTo( 'suva/categories-mappings/index/0/' ~ entity.id , '<span class="btn btn-warning btn-xs">Categories</span> ') }}
								{{ linkTo( 'suva/publications-categories-mappings/index/0/' ~ entity.id , '<span class="btn btn-info btn-xs">Categories<br/>Mappings</span> ') }} 
							
							</td>
							#}
							
						</tr>
					
				{% else %}
					<tr><td colspan="7">Currently, there are no ads here!</td></tr>
				{% endfor %}
			</table>
		</div>
	</div>

<!-- privremeno za dupli flash-session radi dispatchera iz soft-deleta-->
<script>
	var flash_sessions = $('.status-success').length
	
	if(flash_sessions > 1){
		if($(".status-success").eq(0).text() == $(".status-success").eq(1).text())
			$(".status-success").eq(1).remove()
	}
	
</script>