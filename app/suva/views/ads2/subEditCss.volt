<!-- CSS -->
<style type="text/css">
.panel-title{
    font-size:15px;
}

span, label{
    font-size:smaller;
}

td{
    font-size:11px;
}

.panel-heading{
    padding:0;
    margin:0;
}

.panel-body{
    padding:0;
    margin:0;
}

.insertion-list{
    margin:0;
    padding:0;
}

.tab-content{
    margin:0;
    padding:0;
}

.panel-footer{
    padding:0;
    margin:0;
}

.panel-default{
    margin-bottom:1;
    padding:0;
}

.control-label{
    font-size:x-small;
    font-weight:500;
}

.kontakt-podaci{
    font-size:x-small;
    font-weight:bolder;
    margin-left:10px;
    padding-left:30px;
}

.dodatni-kontakt-podaci{
    margin-right:0px;
    margin-left:50px;
    padding-bottom:3px;
}

.form-control{
    height:25px;
}

.btn-xs{
    height:19px;
    margin-top:-5px;
}

.form-group{
    margin:0;
}

.pull-right{
    margin-right:30px;
}

#category_id-input{
    margin-left: 30px;
    width: 600px;
}

div.btn-group.bootstrap-select.show-tick{
	display:none;
}

.form-control{
	padding:0;
}
	
</style>