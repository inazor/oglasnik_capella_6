<script>
	$('#cbx_hidden_n_is_display_ad').val(1);
	$('#block_upload_image').show();
	$('#block_upload_image2').show();
	$('#ads_category_parameters').hide();
	$('label.product_label:not(:contains(display))').hide(); 
	$('label.product_label:contains(display)').show();
</script>

<br/>
<br/>
<input type="hidden" id="hidden_picture_name" name="_context[picture_name]" value></input>
<input type="hidden" id="hidden_tmp_name" name="hidden_tmp_name" value></input>
<div class="row" id="block_upload_image" style="{% if ad.n_is_display_ad == 0 %}  display:none; {% endif %}">
	<div class="col-lg-12 col-md-12"> 
		<div class="col-lg-6 col-md-6"> 
			<input name="imageUpload" value id="imageUpload" onchange="loadFile(event)" type="file"></input>
		</div>	

				<!-- <input type="button" value="zip" onclick="create_zip()"></input> -->
		{% if ad.id is defined %}
			<div class="col-lg-6 col-md-6"> 	
				<input 
					class="submit_picture"
					value="submit picture"
					type="button"
					onclick="if($('#uploaded_preview').attr('src') > ''){
								alert('Please remove the picture before uploading another one');
								return;
							 } 
							 if($('#uploaded_preview_append').find('#current_preview').attr('src') > ''){
								alert('Please remove the picture before uploading another one');
								return;
							 }

							if($('#imageUpload').prop('files')[0] > ''){
								var file_data = $('#imageUpload').prop('files')[0];
								var form_data = new FormData();
								form_data.append('file', file_data); 
								form_data.append('ad_id', {{ ad.id }}); 
								$.ajax({
									type:'POST',
									url: 'suva/ajax/pictureUpload',
									data:form_data,
									cache:false,
									contentType: false,
									processData: false,
									success:function(data){
										$('#uploaded_preview').remove();															 			
										$('#uploaded_preview_append').append($('#current_preview').clone());
										console.log($('#current_preview').attr('src'));
										document.getElementById('delete_uploaded_photo').style.display = 'inline';
										alert('image uploaded');
									}						
								});
							}
							 else{
								alert('Odaberite sliku')
							}"
				></input>	
			</div>
		{% else %}
		    <span
					class="btn btn-primary submit_image_upload image_submit_before_ad_id"
					onclick="if($('#uploaded_preview').attr('src') > ''){
								alert('Please remove the picture before uploading another one');
								return;
							 } 

							if($('#imageUpload').prop('files')[0] > ''){
								var file_data = $('#imageUpload').prop('files')[0];
								var form_data = new FormData();
								form_data.append('file', file_data); 
								$.ajax({
									type:'POST',
									url: '/ajax/displayPictureUpload',
									data:form_data,
									cache:false,
									contentType: false,
									processData: false,
									success:function(data){
	
							 			
									}						
								});
							}
							 else{
								alert('Odaberite sliku')
							 	return
							}
							
							 "
							 
							 
					>Upload
		
			</span>
		<script>$('.image_submit_before_ad_id').click(function() { if($('#imageUpload').prop('files')[0] > '') setTimeout(function() { $('.submit_image_upload').click()}, 500); });</script>
			<button 
				  style="visibility:hidden;"
				  class="btn btn-primary submit_image_upload"  
				  type="submit" 
				  name="save"
				  onclick="$('#hidden_picture_name').val('123')"
				  >Upload
			</button>
		{% endif %}
	</div>
	
</div>

<div class="row">
	<div class="col-md-3 col-lg-2">
		<span>current preview</span>
	</div>
	<div class="col-md-3 col-lg-2">
		<span>uploaded preview</span>
	</div>
	<div class="col-md-3 col-lg-2">
		<span>final preview</span>
	</div>
	<div class="col-md-3 col-lg-6">
		&nbsp;
	</div>
</div>
										
<div class="row" id="block_upload_image2" style=" {% if ad.n_is_display_ad == 0 %} display:none; {% endif %}">
	<div class="col-md-3 col-lg-2">										
		<span
			id="remove_display_photo"
			onclick="$('#imageUpload').replaceWith($('#imageUpload').val('').clone(true))
					 $('#current_preview').attr('src','')
					 $(this).css('display', 'none') "
			style="color:red; display:none; cursor:pointer;">X
		</span>
	</div>

	<div class="col-md-3 col-lg-2" id="remove_uploaded_photo">
		<span
			onclick="
					var form_data = new FormData();
					form_data.append('ad_id', {{ ad.id }}); 
					if(confirm('Are you sure you want to delete this picture from the database?')){
						$.ajax({
							type:'POST',
							url: '/ajax/removeUploadedPhoto',
							data:form_data,
							cache:false,
							contentType: false,
							processData: false,
							success:function(data){
							$('.remove_uploaded_photo').css('display','none')
							$('#uploaded_preview').remove()
							$('#uploaded_preview_append').find('#current_preview').remove();

							}						
						})
					}"
			class="remove_uploaded_photo" 
			id="delete_uploaded_photo"
			style="color:red; display:none;">X
		</span>
	</div>

	<div class="col-md-3 col-lg-2">
		<span style="color:red; display:none;">X</span>
	</div>

	<div class="col-md-3 col-lg-6">
		&nbsp;
	</div>

</div>


<div class="row">
	<div class="col-md-3 col-lg-2">
		<img id="current_preview" style="width:100%;"/>
	</div>	
	<div class="col-md-3 col-lg-2" id="uploaded_preview_append">
		{% if ad.n_picture_path != '' %}
			<script>document.getElementById("delete_uploaded_photo").style.display = "inline";</script>
			<img src="/{{ad.n_picture_path}}" id="uploaded_preview" style="width:100%;"/>
		{% endif %}
	</div>

	<div class="col-md-3 col-lg-2">
		{% if ad.n_picture_path != '' %}
			<img id="final_preview" src="/repository/dtp/processed/{{ad.id}}.jpg" style="width:100%;"/>
		{% endif %}
	</div>	

	<div class="col-md-3 col-lg-6">
		&nbsp;
	</div>

</div>

<script>
	   var loadFile = function(event) {
		 var current_preview = document.getElementById('current_preview');
		 current_preview.style.display = "inline"
		 current_preview.src = URL.createObjectURL(event.target.files[0]);			
		 var remove_display_photo = document.getElementById('remove_display_photo')
		 remove_display_photo.style.display = "inline"
	   }

	   function create_zip() {
		 var zip = new JSZip();
		 zip.add("hello1.txt", "Hello First World\n");
		 zip.add("hello2.txt", "Hello Second World\n");
		 content = zip.generate();
		 location.href="data:application/zip;base64," + content;
	   }		

</script>