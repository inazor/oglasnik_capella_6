{% if _context['_window_id'] is defined %}{% set _window_id = _context['_window_id'] %}{% endif %}
{% if _window_id is not defined %}{% set _window_id = '1' %}{% endif %}

{% if _is_ajax_submit is not defined %}{% set _is_ajax_submit = false %}{% endif %}
{% set _subform_id = 0 %}
{% set _url = '/suva/ads2/subInsertions/' ~ ad.id %}
{% if _form_id is not defined %} {% set _form_id = 'frm_subInsertions' %}{% endif %}

{{ form(NULL, 'id': _form_id , 'enctype':"multipart/form-data", 'method': 'post', 'autocomplete': 'off') }}
	{{ hiddenField('next') }}
	{{ hiddenField('_csrftoken') }}
	<input type="hidden" name = "_context[_window_id]" value ={{_window_id}}></input>
	
	<input type="hidden" name="_context[selected_user_id]" value = "{{ad.user_id}}" ></input>
	<input type="hidden" name="_context[clicked_action]" id="_clicked_action_subInsertions" value = "" ></input>

	<input type="hidden" name="n_first_uploaded_photo_id" value="{{ad.n_first_uploaded_photo_id}}"/> 
	<input type="hidden" name="moderation" value="ok"/> 
	<input type="hidden" id="_context_active_tab_subInsertions" name = "_context[_active_tab]" value="{{ ( array_key_exists('_active_tab', _context) ? _context['_active_tab'] : '') }}"></input>

	<input type = "hidden" id = "_clicked_users_insertion" value = ""></input>
	{% if ad.id > 0 %}
	<div role="tabpanel" style="padding-top:0" class="tab-pane" id="{{ _window_id }}">
		<div class="row">
			<div class="col-lg-12 col-md-12">
				<div class="panel panel-default" >
				
					<!-- NOVA VERZIJA SA STAVKAMA -->
					<div class="panel-heading" style="overflow:visible;">
						{% if _is_ajax_submit %}
							<span 
								class="btn btn-primary btn-xs" 
								style="text-align:right; height:25px; margin-bottom:5px;" 
								onclick = "
									$('#_clicked_action').val('lock_insertions');
									$('#_clicked_action_subInsertions').val('lock_insertions');
									ajaxSubmit( '{{ _url }}' , '{{ _window_id }}', '{{ _form_id }}', '{{ _url }}' ); 	
								"
								>
								Lock 
							</span>
							<span 
								class="btn btn-primary btn-xs" 
								style="text-align:right; height:25px; margin-bottom:5px;" 
								onclick = "
									$('#_clicked_action').val('unlock_insertions');
									$('#_clicked_action_subInsertions').val('unlock_insertions');
									ajaxSubmit( '{{ _url }}' , '{{ _window_id }}', '{{ _form_id }}', '{{ _url }}' );
								"
								>
								Unlock 
							</span>
							{% else %}
							<button 
								class="btn btn-primary btn-xs" 
								style="text-align:right; height:25px; margin-bottom:5px;" 

								type="submit" 
								name="save"
								onclick="$('#_clicked_action').val('lock_insertions');
										 $('#_clicked_action_subInsertions').val('lock_insertions');
								
								"
								>
								Lock 
							</button>
							<button 
								class="btn btn-primary btn-xs" 
								style="text-align:right; height:25px; margin-bottom:5px;" 
								 

								name="save"
								onclick="$('#_clicked_action').val('unlock_insertions');
										 $('#_clicked_action_subInsertions').val('unlock_insertions');
								"
								>
								Unlock 
							</button>
						{% endif %}
						<span 
							class="btn btn-primary btn-xs" 
							style="text-align:right; height:25px; margin-bottom:5px;" 
							onclick="var options = document.getElementById('_select_users_insertions').options, count = 0, total = 0;	
									 for (var i=0; i < options.length; i++) {
										if (options[i].selected){ count++;
											option_price = parseFloat(options[i].getAttribute('ins_price'))	
											total += option_price;
									 }
										
									 }						 
									 total_pdv= total*1.25;
									 $('.insertion_total').html(total.toFixed(2));
									 $('.insertion_total_pdv').html(total_pdv.toFixed(2));
									 $('.selected_insertions').html(count);"
							>
							Calculate
						</span>					
					</div>
				</div>

				<!-- INVOICE - NASLOVNI RED -->
				<div class="panel panel-default" style = "overflow-x:scroll; min-height:200px; max-height:200px; " >
					<div class="panel-heading" style="background-color:#CDCDCD; ">
						<h3 class="panel-title" style="font-family:Courier; font-size:12px;" >
						{{ str_replace("+","&nbsp","INS_ID+ORDER+ITEM+++STATUS+ISSUE++LOCK+ONLINE+OBJ.+DATE++++++++++++++++EXPIRES+AT+CREATED+++++++++PRICE++PRODUCT++++++++++++++++++++++++AdTaker")}}
						</h3> 
					</div>
					<div class="panel-body" >
						<!-- <select style="display:block !important; height:360px;" name="_orders_items_ids[]" multiple> -->
						<select style="display:block !important; overflow-y:scroll; height:160px;" name="_users_insertions[]" id = "_select_users_insertions" selected_order_item = "" multiple>
							{% for orderitem in ad.OrdersItems(['order': 'id desc'])  %}								
								{% if orderitem.Publication()%}
									{% if orderitem.Publication().is_online == 0 %}
										{#  ako je offline onda ima insertions #}
										{% for insertion in _model.Insertions(['orders_items_id = ' ~ orderitem.id,'order':'issue_date_published DESC, orders_items_id DESC', 'limit':100])  %}
											{% set oi = insertion.OrderItem() %}
											{% set _days = oi.Product().days_published %}
											{% set _price = ( _days > 0 ? oi.n_total / _days : oi.n_total) %}
										
											<option  
												ins_price = "{{ oi.n_price }}"
												pr ="{{oi.n_price }}"
												days = "{{ _days }}"
												id = "{{oi.id}}"
												{#onmouseover = "this.style.backgroundColor='#ffa6a6'"
												onmouseout = "this.style.backgroundColor=''"#}
												style="
												border-top:1px solid #DCDCDC;
												font-family:Courier;
												font-size:12px;
												{% if oi.Order().OrderStatus().description == 'Cart' %} background-color: LightGrey; {% endif %}
												{% if oi.Order().OrderStatus().description == 'Offer' %} background-color: yellow; {% endif %}
												{% if oi.Order().OrderStatus().description == 'Paid Invoice' %} background-color:#B9D8A8; color: black; {% endif %}
												{% if oi.Order().OrderStatus().description == 'Paid Invoice' and insertion.published_status == 'ok'  %} background-color:#B9D8A8; color:black; {% endif %}
												{% if oi.Order().OrderStatus().description == 'Paid Invoice' and insertion.published_status == 'lock'  %} background-color:#B9D8A8; color:red; {% endif %}
											
											
												
											
												
												"
												value="{{ insertion.id }}"
												order = "{{oi.order_id}}"
												ondblclick = "
														//alert('alo');
														window.open ('/suva/orders-items/edit/{{ oi.id }}','_blank');
													"
											>
													{{ _model.txtPad( insertion.id , 6 ) }}
													{{ _model.txtPad( oi.order_id , 5 ) }}
													{{ str_replace("_","&nbsp",str_pad(substr(oi.id, 0,6),6,"_")) }}
													{{ str_replace("_","&nbsp",str_pad(substr(oi.Order().OrderStatus().description, 0,6),6,"_")) }}
													{{ str_replace("_","&nbsp",str_pad(substr(insertion.Issue().status, 0,6),6,"_")) }}
													{{ _model.txtPad( insertion.published_status , 5 ) }}
													{{ str_replace("_","&nbsp",str_pad(substr('', 0,5),5,"_")) }}
													{{ str_replace("_","&nbsp",str_pad(substr(oi.Publication().slug, 0,4),4,"_")) }}
													{{ str_replace("_","&nbsp",str_pad(substr(_model.n_dateUs2DateHr(insertion.Issue().date_published), 0,19),19,"_")) }}
													{{ _model.txtPad( oi.n_expires_at , 10, 'date' ) }}
													
													{{ str_replace("_","&nbsp",str_pad(substr(_model.n_dateUs2DateHr(insertion.created_at), 0,10),10,"_")) }}
													{{ _model.txtPad( oi.n_price , 10, 'currency' ) }}
													{{ str_replace("_","&nbsp",str_pad(substr(oi.Product().name, 0,30),30,"_")) }}
													{{ _model.txtPad( ( orderitem.n_ad_taker_id > 0 ? _model.User( orderitem.n_ad_taker_id ).username : 'n/a') , 20 ) }}
											</option>
										{% endfor %}
										
									{% else %}
										{# ako je online onda se čita direktno s order itema #}
										{% set oi = orderitem %}
										{% set _ad = oi.Ad() %}
										
										{% set adOnline = 'NO' %}
										{% if _ad %}
											{% if _ad.active == 1 %}
												{% set adOnline = 'YES' %}
											{% endif %}
										{% endif %}
										<option 
											ins_price = "{{oi.n_total}}"
											id = "{{oi.id}}"
											{#onmouseover = "this.style.backgroundColor='#ffa6a6'"
											onmouseout = "this.style.backgroundColor=''"#}
											style = "
												border-top:1px solid #DCDCDC; font-family:Courier; font-size:12px;
												{% if oi.Order().n_status == 1 %} background-color: LightGrey; 
												{% elseif oi.Order().n_status == 3 %} background-color: yellow; 
												{% elseif oi.Order().n_status == 5 %} background-color:#B9D8A8; color: black; 
												{%endif%}
											"
											value="{{oi.id}}"
											ondblclick = "
												window.open ('/suva/orders-items/edit/{{ oi.id }}','_blank');
											" >
											{{ _model.txtPad( "" , 6 ) }}
											{{ _model.txtPad( oi.order_id , 5 ) }}
											{{ str_replace("_","&nbsp",str_pad( substr(oi.id, 0,6),6,"_") ) }}
											{{ str_replace("_","&nbsp",str_pad( substr(oi.Order().OrderStatus().description, 0,6),6,"_")) }}
											{{ str_replace("_","&nbsp",str_pad( substr('', 0,12),12,"_")) }}
											{{ _model.txtPad( adOnline , 5 ) }}
											{{ str_replace("_","&nbsp",str_pad( substr(oi.Publication().slug, 0,4),4,"_")) }}
											{{ str_replace("_","&nbsp",str_pad( substr(_model.n_dateTimeUs2DateTimeHr(oi.n_first_published_at), 0,19),19,"_")) }}	
											{{ _model.txtPad( oi.n_expires_at , 10, 'date' ) }}
											{{ str_replace("_","&nbsp",str_pad( substr(_model.n_dateUs2DateHr(oi.n_created_at), 0,10),10,"_")) }}													
											{{ _model.txtPad( oi.n_total , 10, 'currency' ) }}										
											{{ str_replace("_","&nbsp",str_pad( substr(oi.Product().name, 0,30),30,"_")) }}
											{#{ str_replace("_","&nbsp",str_pad( substr(oi.Category().path, 0,100),100,"_")) }#}
											{#{ str_replace("_","&nbsp",str_pad( substr(oi.n_is_published, 0,1),1,"_")) }#}
											{{ _model.txtPad( ( orderitem.n_ad_taker_id > 0 ? _model.User( orderitem.n_ad_taker_id ).username : 'n/a') , 20 ) }}
										</option>
									{% endif %}
								{% endif %}
							{% endfor %}
						</select>
					</div>
				</div>
			</div>
		</div>
	</div>
		<div class="row">
			<div class="col-lg-1 col-md-1">
				<span>Selected: </span>
			</div>
			<div class="col-lg-1 col-md-1">
				<span class="selected_insertions"></span>
			</div>
			<div class="col-lg-1 col-md-1">
				<span>&nbsp;</span>
			</div>
			<div class="col-lg-1 col-md-1">
				<span>Total: </span>
			</div>
			<div class="col-lg-1 col-md-1">
				<span class="insertion_total"></span>
			</div>
			<div class="col-lg-1 col-md-1">
				<span>Total(PDV): </span>
			</div>
			<div class="col-lg-1 col-md-1">
				<span class="insertion_total_pdv"></span>
			</div>
		</div>
	{% endif %}
{{ endForm() }}	