<!-- subPublicationsAndIssues.volt -->

{% if ad is not defined %}
ERROR:AD is not defined
{% endif %}




<div class="col-md-12 col-lg-12">
	<div class="panel panel-default">
		<div class="panel-heading">
			<div 
				class="panel-title"
				title = "{{ ( ad is defined ? 'DISPLAY:' ~ ad.n_is_display_ad ~ ', n_publication_id:' ~ ad.n_publication_id ~ ', n_category_mapping_id:' ~ ad.n_category_mapping_id : 'ad not defined' ) }}"
				>
				<strong>Publications & Issues</strong>
				<div 
					class="pull-right glyphicon glyphicon-resize-small"
					onclick = "$('#_form_publications_panel').toggle();"
				></div>
			</div>
		</div>
		<!-- Publikacije, izdanja, proizvodi i popusti -->
		<div class="panel-body" id = "_form_publications_panel">
			<div class="tab-content">
				<br/>
				{% if _is_ajax_submit %}
					<div 
						class = "btn btn-primary btn-md"
						onclick = "
							$('#_clicked_action').val('add_insertions');
							$('#_clicked_action_subPublicationsAndIssues').val('add_insertions');
							$('#_clicked_action_subPublicationsAndIssuesAjax').val('add_insertions');
							
							
							ajaxSubmitP ( { 
								url: '{{_url}}'
								, windowId: '{{ _window_id }}'
								, formId: '{{ _form_id }}'
								, callerUrl : {{ ( _caller_url is defined ? "'" ~ _url ~ "'" : 'null' ) }} 
								, functionToExecuteAfterLoad: {{ ( _function_to_execute_after_load is defined ? "'" ~ _function_to_execute_after_load ~ "'" : 'null' ) }}
								, functionsToExecuteAfterSuccess : 
									[ 
										{ 
											name: 'ads2RefreshAd'
											, p: { 
												adId: {{ad.id}}
												, isRefreshInsertions : true
												, isRefreshAd : false
												, isRefreshUsersContacts : false
												, isRefreshModeration : false
												, isRefreshPublicationsAndIssues : false
											}
										}
										,{ 
											name: 'resetPublicationsAndIssuesForm'
											, p: { } 
										}
									]
								, isFillHtmlElementWithResponse : false
							});
						"
					>
						Add <b>I</b>nsertions..
					</div>
				
				{% else %}
					<button 
						class="btn btn-primary btn-md " 
						accesskey="i"
						type="submit" 
						name="save"
						onclick="$('#_clicked_action').val('add_insertions');
								 $('#_clicked_action_subPublicationsAndIssues').val('add_insertions');
								 "
						>
						Add <b>I</b>nsertions
					</button>
				{% endif %}
				<br></br>
				<div class="row">
					<!-- Publikacije -->
					{#% set publications = _model.Publications() %#}
					{#% for publication in _model.Publications() %#}
					{% if ad.n_publication_id > 0 and ad.n_is_display_ad == 1 %}
						{% set pubList =  _model.Publications(" is_active = 1 and id = " ~ ad.n_publication_id ) %}
					{% else %}		
						{% set pubList = ad.getPublications() %}
					{% endif %}
					
					{% for publication in pubList %}
						{#% set _adCatMapPubId = ( ad.n_category_mapping_id ? ad.n_category_mapping_id : ad.getCategoryMappingForPublication(publication.id) ) %#}

						{#  TRAŽENJE category mapping #}
						{% set _adCatMapPubId = null %}
						{% set _adCatMapPub = null %}
						{% if ( ad.n_is_display_ad == 1 and ad.n_category_mapping_id > 0 ) %}
							{% set _adCatMapPubId = ad.n_category_mapping_id %}
							{% set _adCatMapPub = _model.CategoryMapping( _adCatMapPubId ) %}
						{% else %}
							{%  set _tmpCatMapId = ad.getCategoryMappingForPublication(publication.id) %}
							{% if _tmpCatMapId > 0 %}
								{% set _adCatMapPubId = _tmpCatMapId %}
								{% set _adCatMapPub = _model.CategoryMapping( _adCatMapPubId ) %}
							{% endif %}
						{% endif %}
									
						
						
						<div class="col-md-12 col-lg-12" >
							<div  id = "pub_{{publication.id}}_issues_list" style="overflow-y:hidden; padding-left:13px; width:100%; min-height:0px; max-height:80px; overflow-x: hidden; {% if publication.is_online == 1 %} background-color:#B9D8A8; {% else %} background-color:#A0D2D0; {% endif %}">
							{#<p style="color:#018be3; "  id = "pub_{{publication.id}}_issues_list">#}
								{% if publication.is_online == 0 %}
								<div 
									id = "issues_list_toggle"
									class="pull-right fa fa-arrows-v"
									onclick = "var clicks = $(this).data('clicks');
											  if (clicks) {
												$('#pub_{{publication.id}}_issues_list').css({'min-height':0,'max-height':300,'height':''})
											  } else {
												$('#pub_{{publication.id}}_issues_list').css({'min-height':'','max-height':'','height':''})
											  }
											  $(this).data('clicks', !clicks);"

								></div>
								{% endif %}

								<label 
									style="
										vertical-align:top; 
										{% if  _adCatMapPub === null %}
											color:red;
										{% endif %}
									"
									
									title = "CATMAP: {% if _adCatMapPub === null %} ERROR - CATEGORY MAPPING NOR FOUND FOR PUBLICAION {{  publication.id }} AND CATEGORY {{ ad.Category().n_path }} {%  else %}	{{ _adCatMapPubId }}-{{ _adCatMapPub.n_path }} {% endif %} "
									>
									{{ publication.slug }}
								</label>
								
								<!-- Izdanja na pojedinoj publikaciji -->
								{% if publication.is_online == 0 %}
									{#% for issue in publication.Issues(["status='prima'", 'limit':8]) %#}
									{% for issue in publication.Issues(["status='prima'"]) %}		
									<label style="font-weight:normal; margin-left:5px; text-align:center;">
										<input type="checkbox" 
										name="_checked_publications[{{publication.id}}][]" 
										id="{{ issue.id }}" 
										value="{{ issue.id }}"
										></input>
										<br/>
										{{ date("d.m", strtotime(issue.date_published))}}
									</label>
									{% endfor %}
								{% else %}
									<label style="vertical-align:top;">
										<span style="vertical-align:top; float:left;">
											{{ partial("partials/ctlDate",['value': date('d.m.Y', time() ) ,   'field':'_checked_dates[' ~ publication.id ~ ']', 'width': 3] ) }}
										</span>
										<span style="vertical-align:top; font-size:15px;" class="to_date"></span>
									</label>
								{% endif %}
								
							{#</p>#}
							</div>


							<div class="panel panel-default">
							
								<!-- selektor proizvoda, da se skrati lista -->
								<input 
									size="10"
									placeholder="Type to search..."
									id="pub_{{publication.id}}_selector" 
									></input> 
									<div 
										id = "product_list_toggle"
										class="pull-right fa fa-arrows-v"
										onclick = "var clicks = $(this).data('clicks');
												  if (clicks) {
													$('.product_box_{{publication.id}}').css({'min-height':0,'max-height':100,'height':''})
												  } else {
													$('.product_box_{{publication.id}}').css({'min-height':'','max-height':'','height':''})
												  }
												  $(this).data('clicks', !clicks);"

									></div>

								<div 
									class="product_box_{{publication.id}}" 
									style="
										overflow:scroll 
										padding-left:13px; 
										width:100%; 
										min-height:0px; 
										max-height:100px; 
										overflow-x: hidden;
										font-family:Courier; font-size:14px;
									">
									{#% for product in publication.Products(['order':'sort_order, name ']) %#}


									{% if ad.n_publication_id > 0 and ad.n_is_display_ad == 1 %}
										{% set productList = publication.Products([ ' is_active = 1 ', 'order':'sort_order, name ']) %}
									{% else %}
										{#% set productList = publication.getProductsForAd( ad.id ) %#}
										{% set productList = publication.getProductsForAdAndCategoryMapping( ad.id, _adCatMapPubId ) %} 
									{% endif %}

									
									{% for product in productList %}
										{% if product.is_active %}
										
											<!-- ODREĐIVANJE CIJENE OVISNO O KATEGORIJI -->
											
											<label 
												{% if product.is_online_product == 1 %}
													dana = {{product.days_published}}
												{% endif %} 
												
												class="product_label" 
												id="prod_{{publication.id}}_{{product.id}}" 
												vrij="{{product.name}} {{product.unit_price}}" 
												style="font-weight:normal; width:100%; margin-bottom:0px;"
												>
												<input
													{# check box - točka #}			
													days_published = '{{product.days_published}}'
													type="radio" 
													{% if product.is_online_product == 1 %}
														dana = {{product.days_published}}
													{% endif %}
													class="product_radio"
													checked1 = "" 
													title = "PRODUCT ID:{{product.id}}, PCP Filter: {{' categories_mappings_id = ' ~ _adCatMapPubId ~ ' and products_id = ' ~ product.id }} &#10; Relevant PCPs :&#10; {% for pcp in _model.getMany('ProductsCategoriesPrices', ' categories_mappings_id = ' ~ _adCatMapPubId ~ ' and products_id = ' ~ product.id  ) %}{#{ _model.getOne('CategoriesMappings', pcp.categories_mappings_id ).name }#} PCP ID:{{pcp.id}}, CATEGORY ID:{{ pcp.categories_mappings_id }}: PRICE:{{pcp.unit_price}}:{{ ( pcp.is_displayed ? 'displayed':'not displayed' ) }}:{{ ( pcp.is_active ? 'active':'not active' ) }}&#10; {% endfor %}" 
													name="_checked_products[{{publication.id}}]" 
													value="{{product.id}}"
													onclick = "var next_objava_list = parseInt($(this).attr('days_published'));
														   
														   var if_checked_product = $(this).attr('checked1')
														   if(next_objava_list >= 0){
															  var objava = 0;	
															  $( '#pub_{{publication.id}}_issues_list label input' ).each(function( index, element ) {
																objava ++;
																
																if(if_checked_product == 22 || if_checked_product == ''){
																	
																	element.checked = true;
																	if(objava > (next_objava_list) ){
																		element.checked = false;
																		return;
																	}
																	
																}else{
																	element.checked = false;
																}
																
															  })

														   }
														var this_input=$(this);
														if(this_input.attr('checked1')=='11')
														{
															this_input.attr('checked1','11')
														}
														else
														{
															this_input.attr('checked1','22')
														}
														$('.radio-button').prop('checked', false);
														if(this_input.attr('checked1')=='11')
														{
															this_input.prop('checked', false);
															this_input.attr('checked1','22')
														   
														}
														else{
															this_input.prop('checked', true);
															this_input.attr('checked1','11')
														}
														  "
												>
												</input>
												{{ str_replace("_","&nbsp",str_pad(substr(product.name, 0,20),20,"_")) }}
												
												
												{#{ str_replace("_","&nbsp",str_pad(substr(product.unit_price, 0,7),7,"_")) }#}
												
												<!-- ODREĐIVANJE CIJENE OVISNO O KATEGORIJI getPrice(category_id)-->
												
												{{ str_replace("_","&nbsp",str_pad(substr(product.getPrice( _adCatMapPubId ), 0,7),7,"_")) }}
												kn
												{% if product.days_published > 0 %}
													{{ str_replace("_","&nbsp",str_pad(substr(product.days_published , 0,3),3,"_")) }}
												{% else %}
													{{ str_replace("_","&nbsp",str_pad(substr(' ', 0,3),3,"_")) }}
												{% endif %}
												
												{% if product.is_online_product == 1 and product.days_published > 0 %}
													{{ str_replace("_","&nbsp",str_pad(substr('dana', 0,7),7,"_")) }}
												{% elseif product.is_online_product == 0 and product.days_published > 0 %}
													{{ str_replace("_","&nbsp",str_pad(substr('objava', 0,7),7,"_")) }}
												{% else %}
													{{ str_replace("_","&nbsp",str_pad(substr(' ', 0,7),7,"_")) }}
												{% endif %}
												
												{% if product.is_display_ad > 0 %}
													{{ str_replace("_","&nbsp",str_pad(substr('Display AD', 0,11),11,"_")) }}
												{% elseif product.is_picture_required > 0 %}
													{{ str_replace("_","&nbsp",str_pad(substr('Picture', 0,11),11,"_")) }}
												{% else %}
													{{ str_replace("_","&nbsp",str_pad(substr(' ', 0,11),11,"_")) }}
												{% endif %}

											</label>
										{% endif %}
									
							
									{% endfor %}
									
								</div>
								<script>
								$.extend($.expr[":"], {
									"containsIN": function(elem, i, match, array) {
										return (elem.textContent || elem.innerText || "").toLowerCase().indexOf((match[3] || "").toLowerCase()) >= 0;
									}										
								});
									
								$.extend($.expr[":"], {
									"doesentContain": function(elem, i, match, array) {
										return (elem.textContent || elem.innerText || "").toLowerCase().indexOf((match[3] || "").toLowerCase()) <= 0;
									}										
								});

									
								$('#pub_{{publication.id}}_selector').keyup(function(){									
									$('.product_box_{{publication.id}} label').css("background-color",'');
									$('.product_box_{{publication.id}} label').show()
									{%  if ad.n_is_display_ad == 0 %}$('label.product_label:contains(Display)').hide();{% endif %}
									$('.product_box_{{publication.id}}').css({'min-height':0,'max-height':100,'height':''})
									
									var value= $(this).val();				
									$('.product_box_{{publication.id}} label:containsIN("'+value+'")').css("background-color",'yellow'); 
									$('.product_box_{{publication.id}} label:doesentContain("'+value+'")').hide()
									
									$('.product_box_{{publication.id}}').css({'min-height':'','max-height':'','height':'100%'})
									if(value == ''){
										$('.product_box_{{publication.id}} label:doesentContain("'+value+'")').show()
										$('.product_box_{{publication.id}} label').css("background-color",'');
										$('.product_box_{{publication.id}}').css({'min-height':0,'max-height':100,'height':''})
										{%  if ad.n_is_display_ad == 0 %}$('label.product_label:contains(Display)').hide();{% endif %}
									}
								 });
									
							</script>
							</div>
							
							<!-- Popusti na publikaciji -->
							<div class="panel panel-default">
								<div class='pull-left popusti_tab'><span><b>Popusti</b></span></div>
								<div 
									id = "discount_list_toggle"
									class="pull-right fa fa-arrows-v"
									onclick = "var clicks = $(this).data('clicks');
											  if (clicks) {
												$('.discount_box_{{product.id}}').css({'min-height':0,'max-height':100,'height':''})
											  } else {
												$('.discount_box_{{product.id}}').css({'min-height':'','max-height':'','height':''})
											  }
											  $(this).data('clicks', !clicks);"

								></div>
								<div class="popusti discount_box_{{product.id}}" style="overflow:scroll; padding-left:13px; width:100%; min-height:0px; max-height:100px; overflow-x: hidden">
									{% for discount in publication.Discounts() %}
									
											<label class="popust_checkbox_value" style="font-weight:normal;">
												<input type="checkbox" name="_checked_discounts[{{publication.id}}][]" value="{{discount.id}}"></input>
													{{discount.name}}
											</label>
											<br/>
										
									{% endfor %}
								</div>
							</div>
						</div>
					{% endfor %}
					<script>

						$( ".product_label" ).click(function() {
						if($(this).attr('dana')){
						  var date_val = $('input[name="_checked_dates[1]"]').val();

							var res = date_val.split(".");
							res0 = res[0];
							res1 = res[1];
							res2 = res[2];
							tt = res1+'.'+res0+'.'+res2
							
							var date = new Date(tt);

							var newdate = new Date(date);

							newdate.setDate(newdate.getDate() +parseInt($(this).attr('dana')));

							var dd = newdate.getDate();
							var mm = newdate.getMonth() + 1;
							var y = newdate.getFullYear();

							var someFormattedDate = dd + '.' + mm + '.' + y;
						
							$('.to_date').text('do '+someFormattedDate);
							
						}
						
						});

						$(".product_radio" ).click(function() {
						if($(this).attr('dana')){
							 if($(this).attr('checked1') == 22){
								$('.to_date').hide();
							}
							 if($(this).attr('checked1') == 11){
								$('.to_date').show();
							}
						}
						});

						$("input[name='_checked_dates[1]']").change(function() {
							var clicked_product = $("input[checked1='11']").attr('dana')
							
							if(clicked_product > 0){
						
								var res = $(this).val().split(".");
								res0 = res[0];
								res1 = res[1];
								res2 = res[2];
								tt = res1+'.'+res0+'.'+res2

								var date = new Date(tt);

								var newdate = new Date(date);

								newdate.setDate(newdate.getDate() +parseInt(clicked_product));

								var dd = newdate.getDate();
								var mm = newdate.getMonth() + 1;
								var y = newdate.getFullYear();

								var someFormattedDate = dd + '.' + mm + '.' + y;

								$('.to_date').text('do '+someFormattedDate);
									
							}
						
						});
						
					</script>
					{%  if ad.n_is_display_ad == 1 %} <script>$('label.product_label:not(:contains(Display))').hide();</script> {% endif %}
					{%  if ad.n_is_display_ad == 0 %} <script>$('label.product_label:contains(Display)').hide();</script> {% endif %}
				</div>
			</div>
		</div>
	</div>
</div>