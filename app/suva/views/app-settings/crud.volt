{% set _form_id = (_context['_form_id'] is defined ) ? ( _context['_form_id'] ) : '1' %}

{% set _subform_id = 0 %}
{% if errors is not defined %}{% if _context['errors'] is defined %}{% set errors = _context['errors'] %}{% endif %}{% endif %}
{% set _is_ajax_submit = (_context['_is_ajax_submit'] === true ) ? true : false %}

{% if entity is not defined and p is defined %}{% if p['entity'] is defined %}{% set entity = p['entity'] %}{% endif %}{% endif %}

{% set _context['_form_url'] = '/admin/app-settings/crud' ~ ( entity.id > 0 ? ('/' ~ entity.id ) : '' ) %}
{% if _is_ajax_submit is not defined %}
	{% set _is_ajax_submit = '' %}
{% endif %}


{#

form URL:{{_form_url}}<br/>
form URL PARAMS:{{ ( entity.id > 0 ? ('/' ~ entity.id ) : 'nije' ) }}<br/>
#}

{#
	<div id ="12345">
		pocetak
	</div>
	<script>
		$('#12345').html("kraj");
	</script>

	{% do assets.addJs('/assets/js/trgovina.js') %}
#}

<div class="row">
	<div class="col-lg-12 col-md-12">
		<div><h1 class="page-header">{% if entity.id is not defined %}New {% endif %}App Settings</h1></div>
		<br/>
		{{ partial("partials/blkFlashSession") }}
		{% if entity is defined %}
			{{ form(NULL, 'id' :  '_form_' ~ _form_id , 'method' : 'post', 'autocomplete' : 'off') }}
				{{ hiddenField('_csrftoken') }}
				{{ hiddenField('next') }}
				<input type = "hidden" name="_context[_form_id]" value = "{{ _form_id }}"></input>
				<input type="hidden" name="id" value="{{entity.id}}"></input>
				
				<div class="row">
					<div class="col-lg-12 col-md-12">
						<div class="panel panel-default">
							<div class="panel-heading">
								{% if entity.id is defined %}
									<div class="pull-right">[ID: <b>{{ entity.id }}</b> ]</div>
								{% endif %}
								<h3 class="panel-title">App settings details</h3>
							</div>
							<div class="panel-body">
								<div class="row">
									
								 
									{{ partial("partials/ctlText",['value':entity.name, 'title':'Name', 'width':3, 'field':'name', '_context':_context] ) }}


									{{ partial("partials/ctlDropdown",[
										'value':entity.type, 
										'title':'Type', 
										'list':['boolean','integer', 'decimal','text'], 
										'width':3, 
										'field':'type', 
										'no_ids':1,
										'_context': _context
									] ) }}	
									
									{{ partial("partials/ctlNumeric",[
										'value': entity.lastAvusId, 
										'title': 'last Avus id', 
										'width': 3, 
										'field': 'lastAvusId', 
										'decimals': 0, 
										'_context': _context
									] ) }}
									
							
								</div>
								

								
								<div class="row">
									{{ partial("partials/ctlNumeric",[
										'value': entity.value_decimal, 
										'title': 'value decimal', 
										'width': 3, 
										'field': 'value_decimal', 
										'decimals': 0, 
										'_context': _context
									] ) }}
									
									{{ partial("partials/ctlText", [
										'value': entity.value_text, 
										'title': 'value text', 
										'width': 3, 
										'field': 'value_text'
									] ) }}
									
									
								</div>
							</div>
						</div>
					</div>
				</div>
				{% include "partials/btnSaveCancel.volt" %}
			{{ endForm() }}
		{% else %}
			ENTITY NIJE DEFINIRAN
		{% endif %}
	</div>
</div>