{# CONTEXT<pre>{{ print_r( _context, true ) }}</pre> #} 

	

{% if _context['default_filter']['n_publications_id'] is defined %}
	{% set publication = _model.Publication( _context['default_filter']['n_publications_id'] ) %}
	{% set pubFilter = "n_publications_id = " ~ publication.id %}
{% else %}
	{% set pubFilter = "1=1" %}
{% endif %}
{{pubFilter}}


<div class="row">
    <div class="col-lg-12 col-md-12">
        <div>
			{{ partial("partials/btnAddNew", ['controller':'categories-mappings'] ) }}

		
            <h1 class="page-header">
				Categories 
				{% if publication is defined %}
					for <i>{{ publication.name }}</i>
				{% else %}
					for Publications
				{% endif %}
			</h1>
        </div>
        <br>
        {{ partial("partials/blkFlashSession") }}
        <table style="width:1500 !important;" class="table table-striped table-hover table-condensed">

		{{ partial("partials/blkTableSort", [
			'_context' : _context,
			'fields':[ 
				 ['title':'Options'											,'width':150	,'show_navigation' : true ] 
				,['title':'ID'					,'name':'id'				,'width':100	,'search_ctl':'ctlText'	] 
				,['title':'Category Path'		,'name':'id'				,'width':500	,'search_ctl':'ctlDropdown'	,'search_ctl_params': [ 'list':_model.CategoriesMappings( [pubFilter , 'order':'name' ]), 'option_fields':['n_path','id'] ]]  
				,['title':'Parent Category'		,'name':'parent_id'			,'width':500	,'search_ctl':'ctlDropdown'	,'search_ctl_params': [ 'list':_model.CategoriesMappings( [ pubFilter , 'order':'name' ]), 'option_fields':['n_path','id'] ]]  

				,['title':'Publication'			,'name':'n_publications_id'	,'width':300	,'search_ctl':'ctlDropdown'	,'search_ctl_params': [ 'list':_model.Publications( ), 'option_fields':['id', 'name'] ]]  
				,['title':'EPS File'			,'name':'eps_file_path'		,'width':150	,'search_ctl':'ctlText'	] 
				,['title':'Sort Order'			,'name':'sort_order'		,'width':100	,'search_ctl':'ctlNumeric'] 
				,['title':'DTP Group'			,'name':'dtp_group_id'		,'width':200	,'search_ctl':'ctlDropdown'	,'search_ctl_params': [ 'list': _model.DtpGroups(), 'option_fields':['name'] ]]  
				,['title':'Is Active'			,'name':'active'			,'width':100	,'search_ctl':'ctlText'	] 	
				
			] ]) }}
			
			{% for id in ids %}
				{% set entity = _model.getOne('CategoriesMappings',id) %}
				
				<!-- editiranje preko ajaxa -->
				{% set _context['current_entity_table_name'] = 'n_categories_mappings' %}
				{% set _context['current_entity_id'] = entity.id %}
				{% set _context['ajax_edit'] = true %}
				
				<tr>
					<td>{{ partial("partials/btnEditDelete", ['value':entity.id , 'controller':'categories-mappings'] ) }}</td>
					<td>{{ entity.id }}</td>
					<td>{{ entity.n_path }}</td>
					<td>
					
						{% set _parentName = '' %}
						{% if entity.parent_id > 0 %}
							{% set _parentName =  _model.CategoryMapping( entity.parent_id ).n_path %}
						{% endif %}
						
						{{ partial("partials/ctlTextTypeV3",[
							'field' : 'parent_id'
							, 'value' : entity.parent_id
							, '_context' : _context 
							, 'value_show' :  _parentName
							, 'model_name' : 'CategoriesMappings' 
							, 'expr_search' : 'n_path'
							, 'expr_value' : 'id' 
							, 'expr_show_input' : 'n_path'
							, 'expr_show_list' : 'n_path'
							, 'additional_where' : ' and ' ~ pubFilter  ~ ' and active = 1 order by n_path '
						]) }}
					</td>				
					<td>{{ entity.Publication().name }}</td>
					<td>
						{{ partial("partials/ctlText",[
							'value' : entity.eps_file_path, 
							'field' : 'eps_file_path',
							'_context' : _context
						] )}} 
					</td>
					<td>
						{{ partial("partials/ctlNumeric",[
							'value':entity.sort_order, 
							'field':'sort_order', 
							'decimals':0, 
							'_context': _context
						] ) }}
					</td>
					<td>
						{{ partial("partials/ctlDropdown",[
							'value': entity.dtp_group_id,
							'list': _model.DtpGroups(), 
							'field': 'dtp_group_id', 
							'option_fields':['name'],
							'_context': _context
						] ) }}	
					</td>
					<td>
						{{ partial("partials/ctlCheckbox",[
							'field' : 'active',
							'value' : entity.active, 
							'_context' : _context
						] ) }}
					</td>					
				</tr>
            {% else %}
				<tr><td colspan="6">Currently, there are no Categories for publication here!</td></tr>
            {% endfor %}
        </table>

    </div>
</div>



	{#
	<div class = "col-lg-12 col-md-12">
		<!-- POSTAVKE KATEGORIJA -->
	
		<input type="hidden" name="id" value="{{entity.id}}"></input>
		<div class="row">
			<div class="col-lg-12 col-md-12">
				<div class="panel panel-default">
					<div class="panel-heading">
						{% if entity.id is defined %}
							<div class="pull-right">[ID: <b>{{ entity.id }}</b> ]</div>
						{% endif %}
						<h3 class="panel-title"><b>Publications Categories settings</b></h3>
					</div>
					<div class="panel-body">
						  <!-- lista kategorija mora biti sortirana po n_path zato ide obratna logika  -->
						{% set catlist = _model.Categories([' level > 1  and transaction_type_id is not null ','order':'n_path']) %}
						{%  for cat in catlist %}
							{% if entity.id is defined %}
								{% set pubcat = _model.PublicationCategory("publication_id = " ~ entity.id ~ " and category_id = " ~ cat.id ) %}
								
								
								 <!-- postavke za brzo editiranje postavit ručno jer nije klasična  lista  -->
								{% set _context['current_entity_table_name'] = 'n_publications_categories' %}
								{% set _context['current_entity_id'] = entity.id %}
								{% set _context['ajax_edit'] = true %}

								
								<div class="row">
									<!-- CATEGORY_ID -->
									{{ partial("partials/ctlDropdown",[
										'value':pubcat.layouts_id, 
										'list':_model.Layouts(), 
										'field':'layouts_id', 
										'width':1, 
										'_context':_context
									] ) }}
									<div class="col-lg-4 col-md-4" >
										{{ pubcat.id }} {{ pubcat.Category().n_path }} {{ pubcat.category_id }}
									</div>
									
									<!-- IS_ACTIVE -->
									{{ partial("partials/ctlCheckbox",[
										'value': pubcat.is_active, 
										'width':1, 
										'field':'is_active',
										'_context':_context
									] ) }}							
									
									{{ partial("partials/ctlDropdown",[
										'value': pubcat.dtp_groups_id,
										'list': _model.DtpGroups(), 
										'field':'dtp_groups_id', 
										'width': 1, 
										'option_fields':['name'],
										'_context': _context
									] ) }}	
									
									{{ partial("partials/ctlDropdown",[
										'value': pubcat.forward_category_id,
										'list': catlist, 
										'field':'forward_category_id', 
										'width': 5, 
										'option_fields': ['n_path','id'],
										'_context': _context
									] ) }}										
								</div>	
							{% endif %}	
						{% endfor %}

					</div>

				</div>
			</div>
		</div>
	</div>
	#}
