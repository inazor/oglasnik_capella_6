{% if errors is not defined %}
	{% set errors = null %}
{% endif %}

{# Admin User Edit View #}
<div class="row">
   <div class="col-lg-12 col-md-12">
        <div>
            <h1 class="page-header">{{entity_name}} <small>{{ form_title_long }}</small></h1>
        </div>

        {{ partial("partials/blkFlashSession") }}

        {% if auth.logged_in(['admin', 'support']) %}
        {{ form(NULL, 'id' : 'frm_register', 'method' : 'post', 'autocomplete' : 'off') }}
            {{ hiddenField('_csrftoken') }}
            {{ hiddenField('next') }}
            <input type="hidden" name="action" value="{{form_action}}">
        {% endif %}
	
        <div class="row">
            <div class="col-lg-12 col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        {% if entity.id is defined %}
                        <div class="pull-right">[ID: <b>{{ entity.id }}</b> ]</div>
                        {% endif %}
                        <h3 class="panel-title">{{ entity_name }} details</h3>
                    </div>
                    <div class="panel-body">
						<div class="row">
								
							{{ partial("partials/ctlDropdown",['value':entity.discounts_id, 'title':'Discount', 'list':discounts, 'width':4, 'field':'discounts_id', 'errors': errors] ) }}							
							{{ partial("partials/ctlDropdown",['value':entity.publications_id, 'title':'Publication', 'list':publications, 'width':5, 'field':'publications_id', 'errors': errors] ) }}						
							{{ partial("partials/ctlCheckbox",['value':entity.is_active, 'title':'is active', 'width':3, 'field':'is_active', 'errors': errors] ) }}

						</div>
                   
                </div>
            </div>
        </div>
        {% if auth.logged_in(['admin', 'support']) %}
        	{% include "partials/btnSaveCancel.volt" %}
        {{ endForm() }}
        {% else %}
        <div>
            <button class="btn btn-default" onclick="history.go(-1);">Back</button>
        </div>
        {% endif %}
    </div>
</div>