{% set _form_id = (_context['_form_id'] is defined ) ? ( _context['_form_id'] ) : '1' %}

{% set _subform_id = 0 %}
{% if errors is not defined %}{% if _context['errors'] is defined %}{% set errors = _context['errors'] %}{% endif %}{% endif %}
{% set _is_ajax_submit = (_context['_is_ajax_submit'] === true ) ? true : false %}

{% if entity is not defined and p is defined %}{% if p['entity'] is defined %}{% set entity = p['entity'] %}{% endif %}{% endif %}

{% set _context['_form_url'] = '/admin/discounts/crud' ~ ( entity.id > 0 ? ('/' ~ entity.id ) : '' ) %}
{% if _is_ajax_submit is not defined %}
	{% set _is_ajax_submit = '' %}
{% endif %}


{#

form URL:{{_form_url}}<br/>
form URL PARAMS:{{ ( entity.id > 0 ? ('/' ~ entity.id ) : 'nije' ) }}<br/>
#}

{#
	<div id ="12345">
		pocetak
	</div>
	<script>
		$('#12345').html("kraj");
	</script>

	{% do assets.addJs('/assets/js/trgovina.js') %}
#}

<div class="row">
	<div class="col-lg-12 col-md-12">
		<div><h1 class="page-header">{% if entity.id is not defined %}New {% endif %}Discount</h1></div>
		<br/>
		{{ partial("partials/blkFlashSession") }}
		{% if entity is defined %}
			{{ form(NULL, 'id' :  '_form_' ~ _form_id , 'method' : 'post', 'autocomplete' : 'off') }}
				{{ hiddenField('_csrftoken') }}
				{{ hiddenField('next') }}
				<input type = "hidden" name="_context[_form_id]" value = "{{ _form_id }}"></input>
				<input type="hidden" name="id" value="{{entity.id}}"></input>
				
				<div class="row">
					<div class="col-lg-12 col-md-12">
						<div class="panel panel-default">
							<div class="panel-heading">
								{% if entity.id is defined %}
									<div class="pull-right">[ID: <b>{{ entity.id }}</b> ]</div>
								{% endif %}
								<h3 class="panel-title">Discount details</h3>
							</div>
							<div class="panel-body">
								<div class="row">
									
								 
									{{ partial("partials/ctlText",['value':entity.name, 'title':'Name', 'width':3, 'field':'name', '_context':_context] ) }} 
									{{ partial("partials/ctlCheckbox",['value':entity.is_active, 'title':'Active', 'width':1, 'field':'is_active'] ) }}
									{#{ partial("partials/ctlDropdown",[
										'title':'Category',
										'value':entity.category_id, 
										'list':_model.Categories(), 
										'width':6,
										'field':'category_id', 
										'option_fields':['n_path'],
										'_context':_context
									] ) }#}
									
									
									{#{ partial("partials/ctlDropdown",[
										'title':'Publication',
										'value':entity.publication_id, 
										'list':_model.Publications(), 
										'width':6,
										'field':'publication_id', 
										'option_fields':['n_path'],
										'_context':_context
									] ) }#}
								</div>
								
								<div class="row">
									{% if entity.Category() %}
										{% set category_path  = entity.Category().n_path %}
									{% else %}
										{% set category_path  = '' %}
									{% endif %}
									
									{#{ category_path }#}
									{#{ print_r(entity.toArray(),true) }#}
									
									{{ partial("partials/ctlTextTypeV3",[
										'title' : "Kategorija"
										, 'field' : 'category_id'
										, 'value' : entity.category_id
										, 'width' : 12
										, '_context' : _context
										, 'value_show' : category_path
										, 'model_name' : 'Categories' 
										, 'expr_search' : 'name'
										, 'expr_value' : 'id' 
										, 'expr_show_input' : 'n_path'
										, 'expr_show_list' : 'n_path'
										, 'additional_where' : ''
									]) }}
								</div> 
								
								<div class="row">
									{{ partial("partials/ctlNumeric",[
										'value': entity.amount, 
										'title': 'Amount', 
										'width': 3, 
										'field': 'amount', 
										'decimals': 0, 
										'_context': _context
									] ) }}
									{{ partial("partials/ctlText", [
										'value': entity.tag, 
										'title': 'Tag', 
										'width': 3, 
										'field': 'tag'
									] ) }}
								</div>
							</div>
						</div>
					</div>
				</div>
				{% include "partials/btnSaveCancel.volt" %}
			{{ endForm() }}
		{% else %}
			ENTITY NIJE DEFINIRAN
		{% endif %}
	</div>
</div>