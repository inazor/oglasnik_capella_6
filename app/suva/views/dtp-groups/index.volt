<div class="row">
	
	<div class="col-lg-12 col-md-12">
		<div>
			
			<h1 class="page-header">Dtp Groups</h1>
		</div>
		<br/>
		{{ partial("partials/btnAddNew", ['controller':'dtp-groups'] ) }}
			{{ partial("partials/blkFlashSession") }}
		<table class="table table-striped table-hover table-condensed">
			{{ partial("partials/blkTableSort",['fields':[ 
				['title':'Options'								,'width':150	,'show_navigation' : true ]
				,['title':'ID'			,'name':'id'			,'width':100		,'search_ctl':'ctlNumeric'	]
				,['title':'Name'		,'name':'name'			,'width':300	,'search_ctl':'ctlText'	] 
				,['title':'Active'		,'name':'is_active'		,'width':100		,'search_ctl' : 'ctlNumeric'	]
			] ]) }}

		{% for id in ids %}
			{% set entity = _model.getOne('DtpGroups',id) %}
            <tr>
				<td class="text-right">{{ partial("partials/btnEditDelete", ['value':entity.id , 'controller':'dtp-groups'] ) }}</td>
                <td>{{ entity.id }}</td>
				<td>{{ entity.name }}</td>
				<td>{{ partial("partials/ctlCheckbox",['value':entity.is_active, 'readonly':1 ] ) }}</td>
            </tr>
            {% else %}
            <tr><td colspan="6">Currently, there are no app settings here!</td></tr>
		{% endfor %}
        </table>

    </div>
</div>