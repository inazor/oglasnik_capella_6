{# Admin User Edit View #}
{% if _context is defined %}
	
	{% if _context['_errors'] is defined %}
		{% set _errors = _context['errors'] %}
	{% else %}
		{% set _errors = errors %}
	{% endif %}

{% endif %}
<div class="row">
	

    <div class="col-lg-12 col-md-12">
        <div>
			<a href='#' onclick="window.history.back();" class="btn btn-primary">&lt;&lt; Back</a>
			<a>{{ linkTo(moduleRootDir ~'/actions/actOrderStatusPlaceno/'~ entity.id, '<span class="btn btn-info">Manual Pay</span> ') }}</a>
            <h1 class="page-header">{{entity_name}} <small>{{ form_title_long }}</small></h1>
			
						 
        </div>

        {{ partial("partials/blkFlashSession") }}


		{{ form(NULL, 'id' : 'frm_register', 'method' : 'post', 'autocomplete' : 'off') }}
		{{ hiddenField('_csrftoken') }}
		{{ hiddenField('next') }}
		<input type="hidden" name="action" value="{{form_action}}">
        <div class="row">
            <div class="col-lg-12 col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        {% if entity.id is defined %}
                        <div class="pull-right">[ID: <b>{{ entity.id }}</b> ]</div>
                        {% endif %}
                        <h3 class="panel-title">Order Header edit</h3>
                    </div>
                    <div class="panel-body">
						
			
							

					<br/>

					<div class="row">
						<div class="col-md-2 col-lg-2 text-right">User ID</div><div class="col-md-2 col-lg-2">{{ entity.user_id }}&nbsp;</div>
						<div class="col-md-2 col-lg-2 text-right">User Name</div><div class="col-md-2 col-lg-2">{{ user_name }}&nbsp;</div>
						<div class="col-md-2 col-lg-2 text-right">Status</div><div class="col-md-2 col-lg-2">{{ entity.OrderStatus().name }}&nbsp;</div>
					</div>
					<div class="row">
						<div class="col-md-2 col-lg-2 text-right">Total</div><div class="col-md-2 col-lg-2">{{number_format((entity.total), 2, ',', '.')}}&nbsp;</div>
						<div class="col-md-2 col-lg-2 text-right">Agency Commission</div><div class="col-md-2 col-lg-2">{{number_format((entity.n_agency_commission/100), 2, ',', '.')}}&nbsp;</div>
						
					</div>	
						
						



						
						
					<div class="row">
						<div class="col-md-2 col-lg-2 text-right">Payment method:  {#{entity.PaymentMethod().name}#} {{entity.payment_method}}  </div><div class="col-md-2 col-lg-2"></div>
						<div class="col-md-2 col-lg-2 text-right">Created at</div><div class="col-md-2 col-lg-2">{{ entity.created_at }}&nbsp;</div>
					</div>
					<div class="row">
						<div class="col-md-2 col-lg-2 text-right">Modified at</div>
						<div class="col-md-2 col-lg-2">
							{% set field = 'modified_at' %} 
							{% set title = 'Modified at' %} 
							{% set value = entity.modified_at %} 
							<div class="form-group" data-company-label="Deadline" data-personal-label="Deadline">
								<input placeholder="Choose date" 
										 class="form-control" 
										 name='{{field}}' 
										 id='{{field}}'
										 value="{{ (value ? date('d.m.Y', strtotime(value)) : '') }}" 
										 maxlength="10">
								{% if _errors is defined and_errors.filter(field) %}
									<p class="help-block">{{ current(_errors.filter(field)).getMessage() }}</p>
								{% endif %}
							</div>
						&nbsp;</div>
							
						<div class="col-md-2 col-lg-2 text-right">Valid until</div>
						<div class="col-md-2 col-lg-2">
							{% set field = 'valid_until' %} 
							{% set title = 'valid_until' %} 
							{% set value = entity.valid_until %} 
							<div class="form-group" data-company-label="Valid until" data-personal-label="Valid until">
								<input placeholder="Choose date" 
										 class="form-control" 
										 name='{{field}}' 
										 id='{{field}}'
										 value="{{ (value ? date('d.m.Y', strtotime(value)) : '') }}" 
										 maxlength="10">
								{% if _errors is defined and _errors.filter(field) %}
									<p class="help-block">{{ current(_errors.filter(field)).getMessage() }}</p>
								{% endif %}
							</div>
						&nbsp;</div>
							
							
								<div class="col-md-2 col-lg-2 text-right">Payment date</div>
						<div class="col-md-2 col-lg-2">
							{% set field = 'n_payment_date' %} 
							{% set title = 'n_payment_date' %} 
							{% set value = entity.n_payment_date %} 
							<div class="form-group" data-company-label="Payment Date" data-personal-label="Payment Date">
								<input placeholder="Choose date" 
										 class="form-control" 
										 name='{{field}}' 
										 id='{{field}}'
										 value="{{ (value ? date('d.m.Y', strtotime(value)) : '') }}" 
										 maxlength="10">
								{% if _errors is defined and _errors.filter(field) %}
									<p class="help-block">{{ current(_errors.filter(field)).getMessage() }}</p>
								{% endif %}
							</div>
						&nbsp;</div>
						
					</div>
					<div class="row">
					
						<div class="col-md-2 col-lg-2 text-right">Ip</div><div class="col-md-2 col-lg-2">{{ entity.ip }}&nbsp;</div>
						<div class="col-md-2 col-lg-2 text-right">Applied document id</div><div class="col-md-2 col-lg-2">{{ entity.n_applied_document_id }}&nbsp;</div>
						<div class="col-md-2 col-lg-2 text-right">Payment reference</div><div class="col-md-2 col-lg-2">{{ entity.n_payment_reference }}&nbsp;</div>
					</div>
					<div class="row">
					
					
						
						
						
						
						
						<div class="col-md-2 col-lg-2 text-right">Cart</div><div class="col-md-2 col-lg-2">{{ entity.cart }}&nbsp;</div>
						<div class="col-md-2 col-lg-2 text-right">Status</div><div class="col-md-2 col-lg-2">{{ entity.n_status }}&nbsp;</div>
						<div class="col-md-2 col-lg-2 text-right">Fiscal number</div><div class="col-md-2 col-lg-2">{{ entity.n_invoice_fiscal_number }}&nbsp;</div>
					</div>
					<div class="row">
					
						
						
						<div class="col-md-2 col-lg-2 text-right">Pbo</div><div class="col-md-2 col-lg-2">{{ entity.n_pbo }}&nbsp;</div>
						<div class="col-md-2 col-lg-2 text-right">Invoice zik</div><div class="col-md-2 col-lg-2">{{ entity.n_invoice_zik }}&nbsp;</div>
						<div class="col-md-2 col-lg-2 text-right">Invoice jir</div><div class="col-md-2 col-lg-2">{{ entity.n_invoice_jir }}&nbsp;</div>
											</div>
					<div class="row">
						<div class="col-md-2 col-lg-2 text-right">Fiscal location</div><div class="col-md-2 col-lg-2">{{ fiscal_location_name }}&nbsp;</div>
						<div class="col-md-2 col-lg-2 text-right">Status text</div><div class="col-md-2 col-lg-2">{{ entity.n_status_txt }}&nbsp;</div>
						<div class="col-md-2 col-lg-2 text-right">Orderscol</div><div class="col-md-2 col-lg-2">{{ entity.orderscol }}&nbsp;</div>
											</div>
					<div class="row">
						<div class="col-md-2 col-lg-2 text-right">Invoice due date</div>
						<div class="col-md-2 col-lg-2">
							{% set field = 'n_invoice_due_date' %} 
							{% set title = 'n_invoice_due_date' %} 
							{% set value = entity.n_invoice_due_date %} 
							<div class="form-group" data-company-label="Invoice Due Date" data-personal-label="Invoice Due Date">
								<input placeholder="Choose date" 
										 class="form-control" 
										 name='{{field}}' 
										 id='{{field}}'
										 value="{{ (value ? date('d.m.Y', strtotime(value)) : '') }}" 
										 maxlength="10">
								{% if _errors is defined and _errors.filter(field) %}
									<p class="help-block">{{ current(errors.filter(field)).getMessage() }}</p>
								{% endif %}
							</div>
						&nbsp;</div>
						
						<div class="col-md-2 col-lg-2 text-right">Quotation date</div>
							
								<div class="col-md-2 col-lg-2">
							{% set field = 'n_quotation_date' %} 
							{% set title = 'n_quotation_date' %} 
							{% set value = entity.n_quotation_date %} 
							<div class="form-group" data-company-label="Quotation Date" data-personal-label="Quotation Date">
								<input placeholder="Choose date" 
										 class="form-control" 
										 name='{{field}}' 
										 id='{{field}}'
										 value="{{ (value ? date('d.m.Y', strtotime(value)) : '') }}" 
										 maxlength="10">
								{% if _errors is defined and _errors.filter(field) %}
									<p class="help-block">{{ _current(_errors.filter(field)).getMessage() }}</p>
								{% endif %}
							</div>
						&nbsp;</div>
							
							
							<div class="col-md-2 col-lg-2 text-right">Date time issue</div>
						
						<div class="col-md-2 col-lg-2">
							{% set field = 'n_invoice_date_time_issue' %} 
							{% set title = 'n_invoice_date_time_issue' %} 
							{% set value = entity.n_invoice_date_time_issue %} 
							<div class="form-group" data-company-label="Invoice Date Time Issue" data-personal-label="Invoice Date Time Issue">
								<input placeholder="Choose date" 
										 class="form-control" 
										 name='{{field}}' 
										 id='{{field}}'
										 value="{{ (value ? date('d.m.Y', strtotime(value)) : '') }}" 
										 maxlength="10">
								{% if _errors is defined and _errors.filter(field) %}
									<p class="help-block">{{ current(_errors.filter(field)).getMessage() }}</p>
								{% endif %}
							</div>
						&nbsp;</div>
						
											</div>
					<div class="row">
						<div class="col-md-2 col-lg-2 text-right">Sales rep ID</div><div class="col-md-2 col-lg-2">{{ entity.n_sales_rep_id }}&nbsp;</div>
						<div class="col-md-2 col-lg-2 text-right">Sub customer ID</div><div class="col-md-2 col-lg-2">{{ entity.n_sub_customer }}&nbsp;</div>

					</div>
		
					<br/>

					<div class="row">
					
	
					<div class="col-md-6 col-lg-6">
				
						
						{% set field = 'n_sales_rep_id' %}
						{% set user = sales_rep %}
						<div class="form-group{{ _errors is defined and _errors.filter(field) ? ' has-error' : '' }}">
							<label class="control-label" for="{{ field }}_input">Sales rep</label>

								{# Showing the user selector only for new ads #}
								{%- if (not(user is empty)) -%}
										<input type="hidden" name="{{ field }}" id="{{ field }}" value="{{ user.id }}" data-username="{{ user.username }}" />
							
								{%- else -%}
										<input type="hidden" name="{{ field }}" id="{{ field }}" value="" data-username="" />
							
								{%- endif -%}

								<input type="text" class="form-control" id="{{ field }}_input" value="" />
							{%- if _errors is defined and _errors.filter(field) -%}
								<p class="help-block">{{- current(_errors.filter(field)).getMessage() -}}</p>
							{%- endif -%}
						</div>
					</div>

					<div class="col-md-6 col-lg-6">
						{% set field = 'n_sub_customer' %}
						{% set user = sub_customer %}
						<div class="form-group{{ _errors is defined and _errors.filter(field) ? ' has-error' : '' }}">
							<label class="control-label" for="{{ field }}_input">Sub customer</label>

								{# Showing the user selector only for new ads #}
								{%- if (not(user is empty)) -%}
										<input type="hidden" name="{{ field }}" id="{{ field }}" value="{{ user.id }}" data-username="{{ user.username }}" />
								{%- else -%}
										<input type="hidden" name="{{ field }}" id="{{ field }}" value="" data-username="" />
								{%- endif -%}
								<input type="text" class="form-control" id="{{ field }}_input" value="" />

							{%- if _errors is defined and _errors.filter(field) -%}
								<p class="help-block">{{- current(_errors.filter(field)).getMessage() -}}</p>
							{%- endif -%}
						</div>
					</div>
					
					{{ partial("partials/ctlFindUser",['value':entity.n_sales_rep_id, 'title':'Sales Rep', 'width':3, 'field':'n_sales_rep_id',   '_context': _context ] )  }}
					
						
						
						
				</div>


                    </div>
                   
                </div>
            </div>
        </div>
        {% if auth.logged_in(['admin', 'support']) %}
        <div>
            <button class="btn btn-primary" type="submit" name="save">Save</button>
            <button class="btn btn-primary" type="submit" name="saveexit">Save and exit</button>
            <button class="btn btn-default" type="submit" name="cancel">Cancel</button>
			
        </div>
        {{ endForm() }}
        {% else %}
        <div>
            <button class="btn btn-default" onclick="history.go(-1);">Back</button>
        </div>
        {% endif %}
    </div>
							
				
</div>
							
							
							
							
							
							
							
							
							
							
							
							


