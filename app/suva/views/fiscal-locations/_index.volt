{% if pagination_links is not defined%}
	{% set pagination_links = null %}
{% endif %}
<div class="row">
	<div class="col-lg-12 col-md-12">
		<div>
			
			<h1 class="page-header">Fiscal locations</h1>
		</div>
		<br/>
		{{ partial("partials/btnAddNew", ['controller':'fiscal-locations'] ) }}
			{{ partial("partials/blkFlashSession") }}
			<table class="table table-striped table-hover table-condensed">
			{{ partial("partials/blkTableSort",['fields':[ 
				['title':'Options'																,'width':150	,'show_navigation' : true ] 
				,['title':'ID'								,'name':'id'						,'width':100		,'search_ctl':'ctlNumeric']
				,['title':'Name'							,'name':'name'						,'width':300	,'search_ctl':'ctlText'	  ] 
				,['title':'Nav fiscal location ID'			,'name':'nav_fiscal_location_id'	,'width':100	,'search_ctl':'ctlText'	  ] 				
			] ]) }}
			
		{% for id in ids %}
			{% set entity = _model.getOne('FiscalLocations',id) %}

            <tr>
				<td class="text-right">{{ partial("partials/btnEditDelete", ['value':entity.id , 'controller':'fiscal-locations'] ) }}</td>
                <td>{{ entity.id }}</td>
                <td>{{ entity.name }}</td>
                <td>{{ entity.nav_fiscal_location_id }}</td>
            </tr>
            {% else %}
            <tr><td colspan="6">Currently, there are no fiscal locations here!</td></tr>
            {% endfor %}
        </table>
        {% if pagination_links %}{{ pagination_links }}{% endif %}
    </div>
</div>
