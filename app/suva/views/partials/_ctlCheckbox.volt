{#
	ctlCheckbox parametri:  
	field - 'ime_polja'  - ako je postavljen dodaje klasu inputa (postaje form control)
	value - vrijednost polja
	width - ako je postavljen dodaje div class="col-mg-width-..width".....
	title - ako je naveden dodaje Label
	readonly - ako je postavljen (1) dodaje READONLY na input
	value - polje koje ima vrijednost - entity.is_active

	primjer; ctlText(['value':entity.name, 'title':'Name', 'width':3, '_field':'name', 'readonly':1 ] ) 
	
									{{ partial("partials/ctlCheckbox", [
										'value' : entity.active, 
										'title' : 'Active',
										'width' : 2,
										'field' : 'active',
										'errors' : errors
									] ) }} 
#}
	
	
	<!-- TODO: errors će se koristiti samo preko context -->
	{#{% if errors is defined %}
		{% set _errors = errors %}
	{% endif %}#}
	{% if field is defined %}
		{% set _field = field %}
	{% else %}
		{% set _field = null %}
	{% endif%}
 
	{% set is_ajax = false %}
	{% if _context is defined %}
		{% if _context['ajax_edit'] is defined and _context['current_entity_id'] is defined and _context['current_entity_table_name'] is defined and field is defined %}
			{% if _context['ajax_edit'] === true %}
				{% set is_ajax = true %}
				{% set ajax_table = _context['current_entity_table_name'] %}
				{% set ajax_id = _context['current_entity_id'] %}
			{% endif %}
		{% endif %}
		
		{% if _context['_errors'] is defined %}
			{% set _errors = _context['errors'] %}
		{% endif %}
		
	{% endif %}
	

	
	{% if is_ajax %}
		{% set id_string = 'id__' ~ ajax_id ~ '__field__' ~ _field %}
	{% else %}
		{% set id_string = _field %}
	{% endif %}
	
	
	
	{% if width is defined %}<div class="col-lg-{{width}} col-md-{{width}}" id = "border_div_{{ id_string }}">{% endif %} 
		{% if _field is defined %}<div class="{% if is_ajax %} ajax_edit {% endif %}   form-group{{ _errors is defined and _errors.filter(_field) ? ' has-error' : '' }}">{% endif %}
			{% if title is defined %}<label {% if _field is defined %}class="control-label" for="{{ _field }}" {% endif %} >{{ title }}</label><br/>{% endif %}
			
			{% if is_ajax %}<div id = "border_div_{{ id_string }}" >{% endif %}
				<input
						
					type="checkbox" 
					{% if readonly is defined %} disabled = "disabled" {% endif %} 
					{% if _field is defined %} 
						name = "{{ _field }}_cbx" 
						style = "{% if is_ajax %} margin: 0px 10px 0px 10px; {% endif %}"
						
						class = "form-control"
						
						onclick = "					
							if($(this).is(':checked')){
								$('#cbx_hidden_{{ id_string }}').val(1)
							}else{
								$('#cbx_hidden_{{ id_string }}').val(0)
							}
							//alert($('#cbx_hidden_{{ id_string }}').val());
						"
					{% endif %} 
					value="1" 
					{% if value == "1"  %} checked {% endif %} 
					maxlength="0" 
					id = "{{ id_string }}"
					style = " " 
					
					{% if is_ajax %}
						onchange = " ctlCheckboxUpdateField( 'cbx_hidden_{{ id_string }}', '{{ ajax_table }}', '{{ ajax_id }}', '{{ field }}', 'border_div_{{ id_string }}' );"
					{% endif %}
						
					/>
			{% if is_ajax %}</div>{% endif %}
			{% if field is defined %}
				<input type="hidden" name="{{ _field }}" id="cbx_hidden_{{id_string}}" value="{{value}}"></input>
				{% if _errors is defined and _errors.filter( field ) %}
					<p class="help-block">{{ current(_errors.filter( _field )).getMessage() }}</p>
				{% endif %}
			{% endif %}
			
		{% if _field is defined %}</div>{% endif %}
	{% if width is defined %}</div>{% endif %}