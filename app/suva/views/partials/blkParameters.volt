{#
potrebne varijable:

	- parameter - iz veze entity.Category().CategoriesFieldsets().CategoriesFieldsetsParameters().Parameter()
	- properties  - izvuče se iz json strukture u tablici categories_fieldsets_parameters
	
		
properties.	
object(stdClass)[3339]
  public 'label_text' => string 'Snaga motora' (length=12)
  public 'placeholder_text' => null
  public 'default_value' => null
  public 'other_text' => null
  public 'other_label_text' => null
  public 'other_placeholder_text' => null
  public 'help_text' => null
  public 'prefix' => null
  public 'suffix' => string 'kW' (length=2)
  public 'number_decimals' => null
  public 'currency_id' => null
  public 'is_searchable' => int 1
  public 'is_searchable_options' => 
    object(stdClass)[3327]
      public 'label_text' => string 'Snaga motora' (length=12)
      public 'type' => string 'INTERVAL_SLIDER' (length=15)
  public 'is_required' => int 0
  public 'is_hidden' => int 0



#}

{#{ var_dump(properties)}#}

<!-- Izvlači iz json stringa koji se nalazi u tablici categories_fieldsets_parameters 
polje data koji je parsan iz properties varijable i stavlja se u varijable -->
{% set _is_required = '' %}{% if properties.is_required %}{% set _is_required = '<span style= "color:red; font-size:150%;">*</span>' %}{% endif %}
{% set _title = '' %}{% if properties.label_text is defined %}{% set _title = properties.label_text ~ ' ' ~ _is_required %}{% endif %}
{% set _suffix = '' %}{% if properties.suffix is defined %}{% set _suffix = '<span class="input-group-addon">' ~ properties.suffix ~ '</span>' %}{% endif %}

<!-- _value, _field, _field_id -->
{% if parameter.slug == 'ad_title' %}{% set _value = entity.title %}{% set _field = 'title' %}{% set _field_id = 'title' %}
{% elseif parameter.slug == 'ad_price' %}{% set _value = entity.price %}{% set _field = 'price' %}{% set _field_id = 'price' %}
{% elseif parameter.slug == 'ad_description' %}
	{% set _value1 = entity.description %}{% set _field1 = 'description' %}{% set _field_id1 = 'parameter_description' %}
	{% set _value2 = entity.description_offline %}{% set _field2 = 'description_offline' %}{% set _field_id2 = 'parameter_description_offline' %}
	
{% elseif parameter.slug == 'ad_location' %}
	{#{% set _value1 = entity.country_id %}{% set _field1 = '_context[ad_params][' ~ parameter.slug ~ '_1]' %}
	{% set _value2 = entity.county_id %}{% set _field2 = '_context[ad_params][' ~ parameter.slug ~ '_2]' %}
	{% set _value3 = entity.city_id %}{% set _field3 = '_context[ad_params][' ~ parameter.slug ~ '_3]' %}
	{% set _value4 = entity.municipality_id %}{% set _field4 = '_context[ad_params][' ~ parameter.slug ~ '_4]' %}#}

	{% set _value1 = entity.country_id %}{% set _field1 = 'country_id' %}
	{% set _value2 = entity.county_id %}{% set _field2 = 'county_id' %}
	{% set _value3 = entity.city_id %}{% set _field3 = 'city_id' %}
	{% set _value4 = entity.municipality_id %}{% set _field4 = 'municipality_id' %}


{% else %}
	{% set _field = '_context[ad_params][' ~ parameter.slug ~ ']' %}
	{% set _field = '_context[ad_params][' ~ parameter.id ~ ']' %}
	
	{% set _field_id = '_field_id_' ~ parameter.slug %}
	{% set _field_id = '_field_id_' ~ parameter.id %}

	{% set ap = _model.AdParameter(" ad_id =  " ~ entity.id ~ " and parameter_id = " ~ parameter.id ) %}
	{% if ap %}
		{% set _value = ap.value %}
		{% if parameter.ParameterType().accept_dictionary %}
			{% set _value = _model.Dictionary ( ap.value ).name %}
		{% endif %}
	{% else  %}
		{# TODO: ako ne postoji, prazni value (ili defultni, to nać kasnije) #}
		{#% set _value = "GREŠKA, AdParameter za ad_id " ~ entity.id ~ " i parameter_id " ~  parameter.id ~ " ne postoji" %#}
		{% set _value = '' %}
	{%  endif %}
{% endif %}
{#
FIELD:{{_field}}:{{_value}}.<br/>
#}
<!-- {{ _value  }} -->
{% set pType = parameter.type_id %}
<!-- PTYPE= {{pType}} -->

{# za svaki parametar je li required ili nije #}
{% if properties.is_required is defined and properties.is_required == 1 %}
	{% set _required = 1 %}
	<input type="hidden" name="_context[ad_params_required][{{ parameter.id }}]" value="1"/>
{% else %}
	{% set _required = 0 %}
{% endif %}

{#{var_dump(pType)}#}
{% if pType == 'TITLE' %}
	<div class="col-lg-12">
		<div class="form-group">
			<label class="control-label" for="{{_field}}">{{_title}}</label>
			<input type="text" class="form-control" name="{{_field}}" id ="{{_field_id}}"  value="{{_value}}" maxlength="80"/>
		  <p class="help-block">{{properties.help_text}}</p>
		</div>
	</div>
{% elseif pType == 'DESCRIPTION' %}
{#{var_dump(pType)}#}
	<div class="col-lg-6 col-md-6">
		<div class="form-group">
			<label class="control-label" for="{{_field1}}">{{_title1}}</label>
			<textarea class="form-control" name="{{_field1}}" id ="{{_field_id1}}" rows="10" cols="40">{{_value1}}</textarea>
		</div>
	</div>
	<div class="col-lg-6 col-md-6">
		<div class="form-group">
			<label class="control-label" for="{{_field2}}">
				Opis za tisak 
				<small class="text-muted" id="parameter_description_offline_avus_id"></small>
			</label>
			<textarea class="form-control" name="{{_field2}}" id="{{_field_id2}}" rows="10" cols="40" maxlength="256">{{_value2}}</textarea>
			<p class="help-block">
				<span id="parameter_description_offline_maxlength_label" class="pull-left">
					<div class="maxlength">160 characters left</div>
				</span>
				<span id="refresh_ad_description_offline_btn" class="btn btn-primary btn-xs pull-right" title="Refresh">
					<span class="fa fa-refresh"></span>
				</span>
			</p>
		</div>
	</div>
{% elseif pType == 'LOCATION' %}
	<div class="row">
		{#{var_dump(parameter)}#}
		<input type="hidden" name="country_id" id="parameter_location_level_1" value="1">
		{{ partial("partials/ctlTextTypeV3",[
			'title' : "Country"
			, 'field' : _field1
			, 'value' : _value1
			, 'width' : 3
			, '_context' : _context
			, 'value_show' :  _model.Location( _value1 ).name
			, 'model_name' : 'Locations' 
			, 'expr_search' : 'name'
			, 'expr_value' : 'id' 
			, 'expr_show_input' : 'name'
			, 'expr_show_list' : 'name'
			, 'additional_where' : ' and level = 1 order by name '
		]) }}

		{{ partial("partials/ctlTextTypeV3",[
			'title' : "County"
			, 'field' : _field2
			, 'value' : _value2
			, 'width' : 3
			, '_context' : _context
			, 'value_show' :  _model.Location( _value2 ).name
			, 'model_name' : 'Locations' 
			, 'expr_search' : 'name'
			, 'expr_value' : 'id' 
			, 'expr_show_input' : 'name'
			, 'expr_show_list' : 'name'
			, 'additional_where' : ' and parent_id = __P1__ and level = 2 order by name '
			, 'input_P1' : _field1
			, 'debug' : false

		]) }}
		{{ partial("partials/ctlTextTypeV3",[
			'title' : "City"
			, 'field' : _field3
			, 'value' : _value3
			, 'width' : 3
			, '_context' : _context
			, 'value_show' :  _model.Location( _value3 ).name
			, 'model_name' : 'Locations' 
			, 'expr_search' : 'name'
			, 'expr_value' : 'id' 
			, 'expr_show_input' : 'name'
			, 'expr_show_list' : 'name'
			, 'additional_where' : ' and parent_id = __P1__ and level = 3 order by name '
			, 'input_P1' : _field2

		]) }}
		{{ partial("partials/ctlTextTypeV3",[
			'title' : "Municipality"
			, 'field' : _field4
			, 'value' : _value4
			, 'width' : 3
			, '_context' : _context
			, 'value_show' :  _model.Location( _value4 ).name
			, 'model_name' : 'Locations' 
			, 'expr_search' : 'name'
			, 'expr_value' : 'id' 
			, 'expr_show_input' : 'name'
			, 'expr_show_list' : 'name'
			, 'additional_where' : ' and parent_id = __P1__ and level = 4 order by name '
			, 'input_P1' : _field3
		]) }} 




	</div>
{% elseif pType == 'UPLOADABLE' %}
	<div class="col-lg-12 uploadable-holder">
		<div class="form-group">
			<label class="control-label" for="parameter_uploadable">Fotografije</label>
			<div>            
				<span class="btn btn-success fileinput-button">
				<span class="glyphicon glyphicon-plus"></span>
				<span>Dodaj fotografije</span>
				<input id="parameter_uploadable" type="file" name="{{_field}}" multiple="" accept=".jpeg,.jpg,.png,image/jpeg,image/png"/>
				</span>
				<br>
				<br>
				<div id="parameter_uploadable_progress" class="progress hidden">
					<div class="progress-bar progress-bar-success"></div>
				</div>
				<ul id="parameter_uploadable_gallery" class="uploadable-sorter row ui-sortable" data-form="ad_media_gallery">
					<!-- <li class="col-md-3 col-xs-6" style="display: list-item;">
						<div class="uploadable-sorter-box" data-url="/repository/images/_var/2/4/248e4571c0a661c9df3bbdba6b83ae93f51da5b111-GridView.jpg">
							<div class="uploadable-sorter-box-item text-center">
								<img class="img-rounded uploadable-dragger ui-sortable-handle" src="/repository/images/_var/2/4/248e4571c0a661c9df3bbdba6b83ae93f51da5b111-GridView.jpg"/>
									<input type="hidden" name="ad_media_gallery[]" value="15905305"/>
							</div>
							<div class="uploadable-sorter-options btn-group btn-group-justified">
								<div class="btn-group btn-group-sm">
									<span class="btn btn-default btn-sm set-as-main disabled" title="Postavi kao glavnu">
										<span class="fa fa-thumb-tack"></span>
									</span>
								</div>
								<div class="btn-group btn-group-sm">
									<span class="btn btn-default btn-sm move-prev disabled" title="Pomakni lijevo">
										<span class="fa fa-caret-left"></span>
									</span>
								</div>
								<div class="btn-group btn-group-sm">
									<span class="btn btn-default btn-sm move-next disabled" title="Pomakni desno">
										<span class="fa fa-caret-right"></span>
									</span>
								</div>
								<div class="btn-group btn-group-sm">
									<span class="btn btn-danger btn-sm delete" title="Obriši">
										<span class="fa fa-trash-o text-danger"></span>
									</span>
								</div>
							</div>
						</div>
					</li> -->
				</ul>
			</div>
		</div>
	</div>
{% elseif pType == 'YEAR' %}
	<div class="col-lg-3 col-md-3">
		<div class="form-group">        
			<label class="control-label" for="{{_field}}">{{_title}}</label>
			<div class="icon_dropdown">
				<select class="form-control show-tick bs-select-hidden" id ="{{_field_id}}" name="{{_field}}">
					<option value=""></option>
					{% for yr in 2016..1960 %}
						<option  {% if _value == yr %}selected{% endif %} value="{{ yr }}">{{ yr }}</option>
					{% endfor %}
				</select>
			</div>
		</div>
	</div>
{% elseif pType == 'MONTHYEAR' %}
	
	<div class="col-lg-3 col-md-3">
		<div class="form-group">        
			<label class="control-label" for="{{_field}}">{{_title}}</label>
			<div class="icon_dropdown">
				<select class="form-control show-tick bs-select-hidden" id ="{{_field_id}}" name="{{_field}}">
					<option value=""></option>
						{% for i in 0..11 %}
							
							  
							{#% set dateOption = strtotime('"+' ~ 2 ~ 'months"',time()) %#}
							{% set dateOption = mktime(0, 0, 0, date('n', time())+i, 1, date('y', time())) %}
							
							{% set valueFromDB = strtotime(_value)%}
							
							{% set valueMMYYYY = date("m/Y", dateOption )%}
							{% set valueYYYYMM = date("Y-m", dateOption ) %}
							<option  {% if _value == valueYYYYMM %}selected{% endif %} value="{{ valueYYYYMM }}">{{ valueMMYYYY }} </option>
						{% endfor %}
				</select>
			</div>
		</div>
	</div> 
{% elseif pType == 'NUMBER' %}
	<div class="col-lg-3 col-md-3">

		<div class="form-group">
			
			<label class="control-label" for="{{_field_id}}">{{_title}}</label>
			<input type="text" class="form-control text-right" name="{{_field}}" id = "{{_field_id}}" value="{{_value}}"></input>{{ _suffix }}
			{# <input type="text" class="form-control text-right" id="{{_field_id}}_input" value="{{_value}}"/>{{ _suffix }} #}
		</div>
	</div>
{% elseif pType == 'RADIO' %}
	<div class="col-lg-3 col-md-3">
		<label>{{_title}}</label>
		<div class="main_radio_container">
			{% for dict in _model.Dictionaries("parent_id = " ~ parameter.dictionary_id)  %}
				<div  style = "margin-top:0">
					<input 
						{% if _value == dict.name %} checked="checked" {% endif %} 
						type = "radio" 
						name = "{{ _field }}" 
						id = "{{ _field_id }}_{{ dict_id }}" 
						value = "{{ dict.id }}" 
					>
					</input>
					<label class="control-label">{{ dict.name }}</label>
				</div>
			{% endfor %}
		</div>
	</div>
{% elseif pType == 'DROPDOWN' %}
	<!-- dict_id: {{parameter.dictionary_id}}, _value_ {{_value}}-->
	<div class="col-lg-3 col-md-3">
		<div class="form-group">        
			<label class="control-label" for="{{_field}}">{{_title}}</label>
			<div class="icon_dropdown">
				<select class="form-control show-tick bs-select-hidden" id ="{{_field_id}}" name="{{_field}}">
					<option value=""></option>
					{% for dict in _model.Dictionaries("active = 1 and parent_id = " ~ parameter.dictionary_id)  %}
						<option  {% if _value == dict.name %}selected{% endif %} value="{{ dict.id }}">{{ dict.name }}</option>
					{% endfor %}
				</select>
			</div>
		</div>
	</div>
{% elseif pType == 'CHECKBOX' %}
	<div class="col-lg-3 col-md-3 solo-checkbox">
		<label class="spacer">&nbsp;</label>    <div class="checkbox checkbox-primary">
			<!-- <input type="checkbox" name="{{_field}}" value = "{{_value}}" id="{{_field_id}}"/> -->
			<input 
				type="checkbox" 
				onclick = "					
					if($(this).is(':checked')){
						$('.{{ _field_id }}').val(1)
					}else{
						$('.{{ _field_id }}').val('')
					}
				"				 
				{% if _value == "1"  %} checked {% endif %}
				id="{{_field_id}}"
			/>
			<label class="control-label" for="{{ _field_id }}">{{ _title }}</label>
			<input type="hidden" name="{{ _field }}" class="{{_field_id}}" value='1'></input>
		</div>
	</div>
{% elseif pType == 'CURRENCY' %}
	{% set _value_kn = _value / 100 %}
	<div class="col-lg-3 col-md-3">
		<div class="form-group">
			<label class="control-label" for="{{_field_id}}">{{_title}}</label>
			<div class = "input-group">
				<input type="number" step='0.01' placeholder='0.00' class="form-control text-right" name="{{_field}}" id="{{_field_id}}" value="{{_value_kn}}"/>
{#
				<input type="hidden"  name="{{_field}}" id = "{{_field_id}}" value="{{_value}}"/>
				<input type="hidden" name="{{_field}}_currency_id" value="{{ properties.currency_id }}"/>
#}
				<span class="input-group-addon">{{ _model.Currency( properties.currency_id ).short_name }}</span>
			</div>
		</div>
	</div>
{% elseif pType == 'URL' %}
	<div class="col-lg-6 col-md-6">
		<div class="form-group">
			<label class="control-label" for="{{ _field_id }}">{{ _title }}</label>
			<div class="input-group">
				<input type="text" class="form-control" name="{{_field}}" id="{{ _field_id }}" value="{{_value}}"/>
				<span class="input-group-btn">
					<button class="btn btn-primary" type="button" id="{{_field_id}}_preview_btn">
						<span class="fa fa-globe"></span>
						&nbsp;&nbsp; Pogledaj
					</button>
				</span>        
			</div>    
		</div>
	</div>
{% elseif pType == 'TEXT' %}
	<div class="col-lg-3 col-md-3">
	
		<div class="form-group">
		<label class="control-label" for="{{ _field_id }}">{{ _title }}</label>
		<input type="text" class="form-control" name="{{ _field }}" id="{{ _field_id }}" value="{{ _value }}"/>
		</div>
	</div>
{% elseif pType == 'SPINIDGW' %}
	<div class="col-lg-4">
		<div class="form-group">
			<label class="control-label" for="{{ _field_id }}">{{ _title }}</label>
			<input type="text" class="form-control" name="{{ _field }}" id="{{ _field_id }}" value="{{ _value }}" placeholder="Unesite SPIN ID"/>
		</div>
	</div>
	
{% elseif pType == 'SPINID' %}
	<div class="col-lg-4">
		<div class="form-group">
			<label class="control-label" for="{{ _field_id }}">{{ _title }}</label>
			<input type="text" class="form-control" name="{{ _field }}" id="{{ _field_id }}" value="{{ _value }}" placeholder="Unesite SPIN ID"/>
		</div>
	</div>
	
{% elseif pType == 'CHECKBOXGROUP' %}
	<div class="col-lg-3 col-md-3">
		<label class="control-label">{{_title}}</label>
			{% for dict in _model.Dictionaries("parent_id = " ~ parameter.dictionary_id)  %}
			
			{% set adparam = _model.AdParameter(" ad_id =  " ~ entity.id ~ " and value = " ~ dict.id ) %}
			{% if adparam %}
				{% set _value = dict.name %}
			{% else %}
				{% set _value = '' %}
			{% endif %}
				<!-- <div class="checkbox checkbox-primary" style="margin-top:0">
					<input 
						{% if _value == dict.name %} checked {% endif %} 
						type = "checkbox" 
						name = "{{ _field }}[]" 
						id = "{{ _field_id }}_{{ dict_id }}" 
						value = "{{ dict.id }}" 
					>
					</input>
					<label class="control-label" for="{{ _field_id }}_{{ dict_id }}">{{ dict.name }}</label>
				</div> -->
				
				 <div class="checkbox-primary">
					<label>
						<input 
							{% if _value == dict.name %} checked {% endif %}
							type="checkbox" 
							name="{{ _field }}[]" 
							id = "{{ _field_id }}_{{ dict_id }}" 
							value="{{ dict.id }}" 
						/> {{ dict.name }}
					</label>
				</div>
			{% endfor %}
		</div>
	</div>


	
	
{% else %}
	<div class="col-lg-12 col-md-12">
		label_text:{{ properties.label_text  }}<br/>
		type_id:{{ parameter.type_id }}<br/>
		properties:{{ var_dump(properties) }}<br/>	
	</div>
{% endif %}







