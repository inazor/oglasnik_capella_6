{#
	blkTableSort parametri:  
	name - field name, ako nije definirano, nema botuna za sortiranje
	title - naslov na vrhu tablice
	class - ako je definirana, klasa od elementa <th>
	
	limit_options - 
	
	'search_ctl_params': ['field': 'q', 'placeholder': 'Enter search'] - parametri za search kontrolu (ono što bi se inače unijelo kad bi se htjelo pozvati tu kontrolu)
		Ako se ne unese ništa, stavlja defaultne vrijednosti fields i placeholder(field mora biti q)

	primjer; {{ partial("partials/blkTableSort",['fields':field_options, 'mode_options':mode_options, 'order_by_options':order_by_options, 'dir_options':dir_options, 'limit_options':limit_options ] ) }}

			{{ partial("partials/blkTableSort",[
			'_context' : _context,
			'fields':[ 
				 ['title':'ID'				,'name':'id'				,'width':60		,'search_ctl':'ctlText'		,'search_ctl_params': ['field': 'q', 'placeholder': 'Enter search']] 
				,['title':'Name'			,'name':'name'				,'width':400	,'search_ctl':'ctlText'		,'search_ctl_params': ['field': 'q', 'placeholder': 'Enter search']] 
				,['title':'Unit price'		,'name':'unit_price'		,'width':100	,'search_ctl':'ctlNumeric'	,'search_ctl_params': ['field': 'q', 'placeholder': 'Enter search', 'decimals':2 ]]  
				,['title':'Publication'		,'name':'publication_id'	,'width':250	] 
				,['title':'Category'		,'name':'category_id'		,'width':150	] 
				,['title':'ID Resursa Nav'	,'name':'id_resursa_nav'	,'width':70 	] 
				,['title':'Old product ID'	,'name':'old_product_id'	,'width':200	,'search_ctl':'ctlText'		,'search_ctl_params': ['field': 'q', 'placeholder': 'Enter search']]  
				,['title':'Is Online Product','name':'is_online_product','width':50 	,'search_ctl':'ctlNumeric'	,'search_ctl_params': ['field': 'q', 'placeholder': 'Enter search']]  
				,['title':'Days Published'	,'name':'days_published'	,'width':50 	,'search_ctl':'ctlNumeric'	,'search_ctl_params': ['field': 'q', 'placeholder': 'Enter search', 'decimals':0 ]]
				,['title':'Is Display Ad'	,'name':'is_display_ad'		,'width':50		] 
				,['title':'Is Pushup Product','name':'is_pushup_product','width':50		] 
				,['title':'Is Active'		,'name':'is_active'			,'width':50		] 
				,['title':'Options'										,'width':200	] 
			] ]) }}	
			
		{{ partial("partials/blkTableSort",['fields':[ 
			['title':'ID'			,'name':'id'			,'width':60		,'search_ctl':'ctlNumeric'	]
			,['title':'Name'		,'name':'name'			,'width':200	,'search_ctl':'ctlText'	] 
			,['title':'Amount'		,'name':'amount'		,'width':100	,'search_ctl':'ctlNumeric'	,'search_ctl_params': [ 'decimals':2 ] ] 
			,['title':'Tag'			,'name':'tag'			,'width':200	,'search_ctl':'ctlText'	] 
			,['title':'Category'	,'name':'category_id'	,'width':300	,'search_ctl':'ctlDropdown'	,'search_ctl_params': [ 'list':categories, 'option_fields':['path','id'] ]] 
			,['title':'Is active'	,'name':'is_active'		,'width':60		,'search_ctl':'ctlNumeric'	]
			,['title':'Options'								,'width':150	]
		] ]) }}
		
		
		<!-- S NAVIGACIJOM -->
		{{ partial("partials/blkTableSort",[
			'_context' : _context,
			'fields':[ 
				 ['title':'Options'											,'width':150	,'show_navigation' : true ] 
				,['title':'ID'					,'name':'id'				,'width':100	,'search_ctl':'ctlText'		] 
				,['title':'Name'				,'name':'name'				,'width':250	,'search_ctl':'ctlText'		] 
				,['title':'Sort Order'			,'name':'sort_order'		,'width':100	,'search_ctl':'ctlNumeric'		] 
				,['title':'Unit price'			,'name':'unit_price'		,'width':100	,'search_ctl':'ctlNumeric'	,'search_ctl_params': [ 'decimals':2 ] ]  
				,['title':'Publication'			,'name':'publication_id'	,'width':200	,'search_ctl':'ctlDropdown'	,'search_ctl_params': [ 'list':_model.Publications() ]]  
				,['title':'Category'			,'name':'category_id'		,'width':200	,'search_ctl':'ctlDropdown'	,'search_ctl_params': [ 'list':_model.Categories(), 'option_fields':['n_path','id'] ]]  
				,['title':'ID Re-sursa Nav'		,'name':'id_resursa_nav'	,'width':100	,'search_ctl':'ctlText'		]
				,['title':'Old product ID'		,'name':'old_product_id'	,'width':200	,'search_ctl':'ctlText'	]  
				,['title':'Online Pri-ority' 	,'name':'online_priority'	,'width':100 	,'search_ctl':'ctlNumeric'	] 
				,['title':'Is On-line Pro-duct'	,'name':'is_online_product'	,'width':100 	,'search_ctl':'ctlNumeric'	]  
				,['title':'Days Publi shed'		,'name':'days_published'	,'width':100 	,'search_ctl':'ctlNumeric'	]
				,['title':'Layout'				,'name':'layouts_id'		,'width':150 	,'search_ctl':'ctlDropdown'	,'search_ctl_params': [ 'list':_model.Layouts(), 'option_fields':['id','name'] ]]  
				,['title':'Is Dis play Ad'		,'name':'is_display_ad'		,'width':100		] 
				,['title':'Is Pushup Pro-duct'	,'name':'is_pushup_product'	,'width':100		] 
				,['title':'Is Pic-ture Rqd'		,'name':'is_picture_required','width':100 	,'search_ctl':'ctlNumeric'	]  
				,['title':'Is Active'			,'name':'is_active'			,'width':100		] 
				
			] ]) }}
	
#}	


<style>
/* nasljeđivanje suva.css */

.table-condensed>tbody {
	font-size: 14px;
}

.table-condensed>thead {
	font-size: 14px;
}


.form-control {
	border-top: 50px;
	padding-top: 0px;
	padding-bottom: 0px;
	height:inherit;
}

.table-condensed,
.table-condensed>tbody>tr {
    max-height: 20px !important;
	padding-top: 5px;
	padding-bottom: 5px;
	 
}

.table-condensed>tbody>tr>td  {
    padding-top: 5px;
    padding-right: 12px;
    padding-bottom: 5px;
    padding-left: 12px;
	word-break: break-all;
}

.table-striped>tbody>tr:nth-of-type(odd) {
    background-color: lightGrey; 
	/*background-color: Wheat;*/
	background-color: PowderBlue; 
}

.table-striped .ajax-edit {
    /* background-color: lightGrey;*/
	background-color: Wheat;
}

table>thead>tr>th{
	vertical-align:top;
}

.form-group {
    margin-bottom: 0px; 
}

</style>


<thead>
	<!-- PRIKAZ FILTERA -->
	<tr>
		<th colspan = "{{  count(fields) }}">
		{# var_dump(_context['default_filter']) #} {#  default_filter je NULL #}
		{% if _context['default_filter'] is not empty %}
			{%  for key, value in _context['default_filter'] %}
				<span style = "border: solid 1px red; margin:2px; padding:4px; background-color:darkSalmon; border-radius:3px;">
					{{ key }}  
					{% if is_array(value) %}
						: 
						{% for val in  value %}
							{{ val }},
						{% endfor %}
					{% else %}	
						= {{ value }}
					{% endif %}
				</span>
			{% endfor %}
			{% endif %}
			{# var_dump(_context['filter']) #} {# filter je NULL#}
			{% if _context['filter'] is not empty %}
			{%  for key, value in _context['filter'] %}
				<span style = "border: solid 1px black; margin:2px; padding:4px; background-color:lightGray; border-radius:3px;">
					{{ key }}  
					{% if is_array(value) %}
						: 
						{% for val in  value %}
							{{ val }},
						{% endfor %}
					{% else %}	
						= {{ value }}
					{% endif %}
				</span>
			{% endfor %}
			{% endif %}
		</th>
	</tr>

	<!-- SORT GORE-GOLJE UZ NASLOV-->
	<tr>
		{%- for field in  fields -%}
			<th 
				{% if  field['class'] is defined %} class = "{{field['class']}}" {% endif %}
				{% if  field['width'] is defined %} style = "max-width:{{field['width']}}px !important;  min-width:{{field['width']}}px !important;" {% endif %}
			>
				<!-- <div style="float:left;"> -->
				{{ field['title'] }}&nbsp;
				<!-- </div> -->
				{% if field['name'] is defined %}	
					<!-- VARIJABLE ZA PRIKAZ STRELIZA ZA SORTIRANJE -->
					{% set _style_up = 'display:none;' %}
					{% set _style_down = 'display:none;' %}
					{% set _style_both = 'display:none;' %}

					{% if _context['order_by'] is defined %}
						{% if _context['order_by'][field['name']] is defined %}
							{% if _context['order_by'][field['name']] == 'asc' %}
								{% set _style_up = 'color:green;' %}
							{% endif %}
							{% if _context['order_by'][field['name']] == 'desc' %}
								{% set _style_down = 'color:green;' %}
							{% endif %}
							{% if _context['order_by'][field['name']] == ''  %}
								{% set _style_both = 'color:green;' %}
							{% endif %}
						{% else %}
							{% set _style_both = 'color:green;' %}
						{% endif %}
					{% else %}
						{% set _style_both = 'color:green;' %}
					{% endif %}
					
					
					{% set _default_order_by = null %}
					{% if _context['default_order_by'] is defined %}
						{% if _context['default_order_by'][field['name']] is defined %}
							{% set _default_order_by = _context['default_order_by'][field['name']] %}						
						{% endif %}
					{% endif %}
					
					{% if _default_order_by %}
						{% if _default_order_by == 'asc' %}
							<div  
								class="glyphicon glyphicon-arrow-up btn-xs" 
								style = " padding-right:1px; padding-left:1px; color:red; "
							></div>
						{% endif %}
						{% if _default_order_by == 'desc' %}
							<div  
									class="glyphicon glyphicon-arrow-down btn-xs" 
									style = " padding-right:1px; padding-left:1px; color:red; "
							></div>
						{% endif %}
						{% if _default_order_by == '' %}
							<div  
									class="glyphicon glyphicon-sort btn-xs" 
									style = " padding-right:1px; padding-left:1px; color:red; "
							></div>
						{% endif %}

					{% else %}
						<div 
							style = "float:right;" 
							id="div_orderby_{{ field['name'] }}_up" 
							onclick="
								///$(this).hide(); 
								//$('#div_orderby_{{ field['name'] }}_down').show();"
						>
							<!-- UP, KAD SE KLIKNE IDE DOWN -->
							<form onclick="
								$('#div_orderby_{{ field['name'] }}_up').hide();
								$('#div_orderby_{{ field['name'] }}_down').show();
								this.submit();
								" >
								<input type="hidden" name="order_by" id="order_by" value="{{ field['name'] }}"/>
								<input type="hidden" name="_context[order_by][{{ field['name'] }}]" id="order_by" value="desc"/>
								<input type="hidden" name="dir" id="dir" value="desc"/>
								<input type="hidden" name="_context[keep_default_filter]" value="1"/>
								<input type="hidden" name="_context[keep_list_params]" value="1"/>
								<input type="hidden" name="_context[reset_paginator]" value="1"/>
								<input type="hidden" name="_gfId" value="{{_gfId}}"/>
								<div  
									class = "glyphicon glyphicon-arrow-up btn-xs" 
									style = "
										padding-right:1px; padding-left:1px;
										{{ _style_up }}
										"
								></div>
							</form>
						</div>
						<div 
							style = "float:right;" 
							id="div_orderby_{{ field['name'] }}_down" 
							onclick="
								//$(this).hide(); 
								//$('#div_orderby_{{ field['name'] }}_cancel').show();"
						>
							<!-- DOWN, KAD SE KLIKNE IDE CANCEL -->
							<form  onclick="
								$('#div_orderby_{{ field['name'] }}_down').hide();
								$('#div_orderby_{{ field['name'] }}_cancel').show();
								this.submit();">
								<input type="hidden" name="order_by" id="order_by" value="{{ field['name'] }}"/>
								<input type="hidden" name="_context[order_by][{{ field['name'] }}]" id="order_by" value=""/>
								<input type="hidden" name="dir" id="dir" value=""/>
								<input type="hidden" name="_context[keep_default_filter]" value="1"/>
								<input type="hidden" name="_context[keep_list_params]" value="1"/>
								<input type="hidden" name="_context[reset_paginator]" value="1"/>
								<input type="hidden" name="_gfId" value="{{_gfId}}"/>
								<div 
									class="glyphicon glyphicon-arrow-down btn-xs" 
									style = "
										padding-right:1px; padding-left:1px;
										{{ _style_down }}
									"
									
								></div>
							</form>
						</div>
						<div 
							style = "float:right;" 
							id="div_orderby_{{ field['name'] }}_cancel" 
							onclick="
								//$(this).hide(); 
								//$('#div_orderby_{{ field['name'] }}_up').show();"
						>
							<!-- CANCEL, KAD SE KLIKNE IDE UP -->
							<form  onclick="
									$('#div_orderby_{{ field['name'] }}_cancel').hide();
									$('#div_orderby_{{ field['name'] }}_up').show();
									this.submit();">
								<input type="hidden" name="order_by" id="order_by" value="{{ field['name'] }}"/>
								<input type="hidden" name="_context[order_by][{{ field['name'] }}]" id="order_by" value="asc"/>
								<input type="hidden" name="dir" id="dir" value="asc"/>
								<input type="hidden" name="_context[keep_default_filter]" value="1"/>
								<input type="hidden" name="_context[keep_list_params]" value="1"/>
								<input type="hidden" name="_context[reset_paginator]" value="1"/>
								<input type="hidden" name="_gfId" value="{{_gfId}}"/>
								<div  
									class="glyphicon glyphicon-sort btn-xs" 
									style = "
										padding-right:1px; padding-left:1px;
										{{ _style_both }}
									"
								></div>
							</form>
						</div>
					{% endif %}
				{% else %}
					<!-- nn -->
				{% endif %}

			</th>	
		
		{%- endfor -%}
	</tr>	

	<!-- PRETRAGA -->
	<tr>
		{%- for field in  fields -%}
			<th>
				{% if field['search_ctl'] is defined  %}
					{% if field['search_ctl_params'] is defined %}
						{% set params = field['search_ctl_params'] %}
					{% else %}
						{% set params = [] %}
					{% endif %}	
					{#% if !(params['field'] is defined) %} {% set params = params + ['field':'q'] %} {% endif %#}

					{% if !(params['field'] is defined) %} {% set params = params + ['field':'_context[filter][' ~ field['name'] ~ ']'] %} {% endif %}
					
					{#% if array_key_exists( 'filter' , _context )  %}
						{% if !( params['value'] is defined and _context['filter'][field['name']] > '' ) %}
							{% set params = params + ['value':_context['filter'][field['name']]] %} 
						{% endif %}
					{% endif %#}
					
					{#% if _context['filter'] is defined %}
						{% if !(params['placeholder'] is defined) %} 
							{% set params = params + ['placeholder':'Enter Search'] %} 
						{% endif %}	
					{% endif %#}	
					
					{% if array_key_exists( 'filter' , _context )  %}
						{% if !( array_key_exists( 'value' , params )) %}
							{% if ( array_key_exists( field['name'] , _context['filter'] ) and _context['filter'][field['name']] > '' ) %}
								{% set params = params + ['value':_context['filter'][field['name']]] %} 
							{% endif %}
						{% endif %}
						
						{% if !(array_key_exists ( 'placeholder', params ) ) %} 
							{% set params = params + ['placeholder':'Enter Search'] %} 
						{% endif %}	
					{% endif %}
					

					
					{% set _allow_remove_filter = true %}
					
					{# je li vrijednost ovog polja ograničena defaultnim filterom ? #}
					{% set _default_value = null %}
					{% if _context['default_filter'] is defined %}
						{% if _context['default_filter'][field['name']] is defined %}
							{% set _default_value = _context['default_filter'][field['name']] %}
						{% endif %}
					{% endif %}
					
					{% if _default_value %}	
						{# ako postoji defaultna vrijednost, postavlja se, readonly je i ne može se brisati filter #}
						
						{#{ _default_value  }#}
						
						{#  zato jer se u polju za filter ne može prikazati array, pa neka ništa ne prikazuje, filter je ionako u _default_filter  #}
						{% if is_array (_default_value) %}
							{% set _default_value = '' %}
						{% endif %}	
						
						{% if params['value'] is defined  %}
							{% set params['value'] = _default_value %}
						{% else %}
							{% set params = params + ['value': _default_value] %}
						{% endif %}
		
						{% if params['readonly'] is defined %}
							{% set params['readonly'] = true %}
						{% else %}
							{% set params = params + [ 'readonly': true ] %}
						{% endif %}
						
						
						{% set _allow_remove_filter = false %} <!-- DEFAULTNI FILTER SE NE MOŽE X-ATI -->
						
					{% endif %}
						
					
						

					
					{#{print_r(params,true)}#}
					
					<!-- PARTIAL ZA FILTRIRANJE -->
					<form 
						onchange = "
							//var txt = $(this).serialize();
							//var txt = $('#_context_filter__categories_mappings_id_').val();
							//alert (txt);
							this.submit();
						" 
						style="float:left; width: calc(100% - 25px); ">
						<input type="hidden" name="field" id="field" value="{{ field['name'] }}"/>
						<input type="hidden" name="mode" id="mode" value="fuzzy"/>
						<input type="hidden" name="_context[keep_default_filter]" value="1"/>
						<input type="hidden" name="_context[keep_list_params]" value="1"/>
						<input type="hidden" name="_context[reset_paginator]" value="1"/>
						<input type="hidden" name="" id="mode" value="fuzzy"/>
						<input type="hidden" name="_gfId" value="{{_gfId}}"/>
						
						 
						{{ partial("partials/" ~ field['search_ctl'],['p': params] ) }}
					</form>
					
					<!-- X BUTTON -->
					{% if _allow_remove_filter == true %}
						<form onchange="this.submit();" style="float:left; width:25px; " id="_form_clear_selection_{{field['name']}}">
							<input type="hidden" name="_context[filter][{{field['name']}}]" value=""/>
							<input type="hidden" name="_context[keep_default_filter]" value="1"/>
							<input type="hidden" name="_context[keep_list_params]" value="1"/>

							<input type="hidden" name="_context[reset_paginator]" value="1"/>
							<input type="hidden" name="_gfId" value="{{_gfId}}"/>
							<div 
								onclick = "$('#_form_clear_selection_{{field['name']}}').submit();"
								class="glyphicon glyphicon-remove btn-xs" 
								style="padding-right:0px; padding-left:px; float:left;"></div>
						</form>
					{% endif %}
					
				{% endif %}
				{# Navigacija #}
				<!-- NAVIGACIJA -->
				{#CONTEXT:<pre>{{ print_r(_context,true) }}</pre>#}
				{% if field['show_navigation'] is defined %}
					{% if field['show_navigation'] === true %}
						<!-- field['show_navigation'] === true -->
						{% if _context is defined %}
							<!-- _context is defined --> 
							{#{var_dump(_context)}#}
							{#{ _context['from_rec']}} {{_context['to_rec']}} {{_context['count_rec']}} {{_context['step_rec']}#}
							{% if _context['from_rec'] is defined and _context['to_rec'] is defined and _context['count_rec'] is defined and _context['step_rec'] is defined %}
								<!-- _context['from_rec'] is defined and _context['to_rec'] is defined and _context['count_rec'] is defined and _context['step_rec'] is defined -->
								{% set _from = _context['from_rec'] %}
								{% set _to = _context['to_rec'] %}
								{% set _step = _context['step_rec'] %}
								{% set _count = _context['count_rec'] %}
								<span> 

		
									<form style="float:left;">
										<input type = "hidden" name = "_context[from_rec]" value = "{% if _from - _step >= 1%}{{ _from - _step }}{% else %}1{% endif %}"></input>
										<input type = "hidden" name = "_context[to_rec]" value = "{% if _from - _step >= 1%}{{ _from - 1 }}{% else %}{{ _step }}{% endif %}"></input>
										<input type = "hidden" name="_context[keep_default_filter]" value="1"/>
										<input type="hidden" name="_context[keep_list_params]" value="1"/>
										<input type="hidden" name="_gfId" value="{{_gfId}}"/>

										<button 
											type = "submit" 
											class = "glyphicon glyphicon-chevron-left"
											{% if _from <= 1 %}disabled {% endif %}
											>
										</button>
									</form>
									
									<span style="float:left;">
										{{ _from }}
										 - 
										{% if _to <= _count %}
											{{ _to }}
										{% else %}
											{{ _count }}
										{% endif %}
									</span>
									
									<form >
										<input type = "hidden" name = "_context[from_rec]" value = "{{ _to + 1 }}"></input>
										<input type = "hidden" name = "_context[to_rec]" value = "{% if _to + _step  <= _count %}{{ _to + _step }}{% else %}{{ _count }}{% endif %}"></input>
										<input type = "hidden" name="_context[keep_default_filter]" value="1"/>
										<input type="hidden" name="_context[keep_list_params]" value="1"/>
										<input type="hidden" name="_gfId" value="{{_gfId}}"/>

										<button 
											{% if _to >= _count %}disabled{% endif %} 
											type = "submit" 
											class = "glyphicon glyphicon-chevron-right"
											>
										</button>
									</form>
									<span>of {{ _count }}</span>
								</span>			

							{% endif %}
						{% endif %}
					{% endif %}
				{% endif %}			
			</th>	
		{%- endfor -%}
	</tr>
</thead>		
