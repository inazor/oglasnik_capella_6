{#
	btnAddNew - na vrhu liste (index.volt) se pojavljuje button Add New
	controller - OBAVEZNO - ime foldera na koji se šalje
	params - ako je definiran, dodatak na link

	primjer {{ partial("partials/btnAddNew", ['controller':'discounts'] ) }}
#}

{% set _params = '' %}
{% if params is defined %}
	{% set _params = '?' ~ params %}
{% endif %}


{% if controller is defined %}
	<div class="pull-left">
			{#{ linkTo(['suva/' ~ controller ~ '/create', '<span class="fa fa-plus fa-fw"></span> Add new - staro', 'class': 'btn btn-info']) }#}
			{{ linkTo(['suva/' ~ controller ~ '/crud' ~ _params , '<span class="fa fa-plus fa-fw"></span> Add new', 'class': 'btn btn-info']) }}
	</div>
{% endif %}