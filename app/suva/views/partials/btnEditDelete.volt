{#
	btnEditDelete - na listi u zadnjem stupcu tablice pokazuje linkove Edit i Delete:  
	controller - OBAVEZNO - ime foldera na koji se šalje
	value - vrijednost koja se šalje kontroler (id retka)
	model - naziv modela - treba njega koristiti za deletanje

	primjer {{ partial("partials/btnEditDelete", ['value':entity.id , 'controller':'discounts'] ) }}
#}

{% if controller is defined and value is defined %}
	{#{ linkTo(['suva/' ~ controller ~ '/edit/' ~ value, '<span class="fa fa-edit fa-fw"></span>Edit staro']) }#}
	{{ linkTo(['suva/' ~ controller ~ '/crud/' ~ value, '<span class="fa fa-edit fa-fw"></span>Edit']) }}
	
	{%  if model is defined %}
		{{ linkTo(['suva/' ~ controller ~ '/delete/' ~ model ~ '/' ~ value, '<span class="fa fa-edit fa-fw"></span>Delete']) }}
	{% else %}
		{{ linkTo(['suva/' ~ controller ~ '/delete/' ~ value, '<span class="fa fa-edit fa-fw"></span>Delete']) }}
	{% endif %}
{% endif %}