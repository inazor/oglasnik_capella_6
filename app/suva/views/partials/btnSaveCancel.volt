{#
Upotreba:
	{% include "partials/btnSaveCancel.volt" %}
	
	{% set _is_saveexit = false %}
	{% set _is_exit = false %}
	{{ partial ("partials/btnSaveCancel") }}

Rarametri:	
	_is_js_debug

#}

{% set _kill_tab = false %}
{%if _context['_kill_tab_on_exit'] is defined %} 
	{%if _context['_kill_tab_on_exit'] == 1 %}
		{% set _kill_tab = true %}
	{% endif %}
{% endif %} 

{% set _ajax = false %}
{% if _is_ajax_submit is not defined %}
		{% set _is_ajax_submit = '' %}
	{% endif %}
{% if _is_ajax_submit === true %}{% set _ajax = true %}{% endif %}


{% if _additional_js_onclick_save is not defined  %}
	{% set _additional_js_onclick_save = '' %}
{% endif %}

{% if _is_js_debug is not defined  %}
	{% set _is_js_debug = false %}
{% endif %}


{% if _is_saveexit is not defined  %}
	{% set _is_saveexit = true %}
{% endif %}
{% if _is_exit is not defined  %}
	{% set _is_exit = true %}
{% endif %}

{% if _execute_default_js_onclick_save is not defined  %}
	{% set _execute_default_js_onclick_save = true %}
{% endif %}


<div>
		{% if _ajax %}
		
			<div 
				class = "btn btn-primary"
				onclick = " 
					{% if _execute_default_js_onclick_save %}
						ajaxSubmit ( 
						'{{_url}}'
						, '{{ _window_id }}'
						, '{{ _form_id }}'
						, {{ ( _caller_url is defined ? "'" ~ _url ~ "'" : 'null' ) }} 
						, {{ ( _is_js_debug ? 'true' : 'false'  ) }}
						, true
						, {{ ( _function_to_execute_after_load is defined ? "'" ~ _function_to_execute_after_load ~ "'" : 'null' ) }}
						);
					{%  endif %}
					{{ _additional_js_onclick_save }}
					"
			>
				Save..
			</div>
			
			{% if _caller_url is defined %}
				<div 
					class = "btn btn-warning"
					onclick = "ajaxSubmit ( 
						'{{ _caller_url }}'
						, '{{ _window_id }}' 
						, '{{ _form_id }}'
						);"
				>
					Back..
				</div>
			{% endif %}
 
			
		{% else %}
			{%  set _kill_tab = false %}
			{% if _kill_tab == true %}
				<button class="btn btn-warning" onclick="close_window();"  type="button">Close Window</button>
			{% else %}
				<button class="btn btn-primary" type="submit" accesskey="s" name="save">Save</button>
				{#% if _is_saveexit %#}
					<!-- <button class="btn btn-primary" type="submit" name="saveexit">Save & Exit</button>  -->
				{#% endif %#}
				<!-- <button class="btn btn-info" type="submit" name="savenew">Save & New</button> -->
				{% if _is_saveexit %}
					<button class="btn btn-warning" type="submit" name="cancel">Exit</button>
				{% endif %} 
					
			{% endif %}
		{% endif %}
		<script>
		function close_window() {
			if (confirm("Close Window?")) {
				window.close();
			  }
			}
		
		</script>
	

</div>