{#
	ctlFileUpload parametri:  
	p - array sa svim parametrima, radi rekurzivnog pozivanja (ako je definirann parametris e uzimaju iz njega)
	field - 'ime_polja'  - ako je postavljen dodaje klasu inputa (postaje form control)
	value - vrijednost polja
	width - ako je postavljen dodaje div class="col-mg-width-..width".....
	title - ako je naveden dodaje Label
	readonly - ako je postavljen (1) dodaje READONLY na input
	password - ako je postavljen onda se ne prikazuju znakovi
	placeholder - defaultni tekst (( Unesite vrijednost.. ))
	value - vrijednost polja
	js_onclick_value - js koji se doda na onclick event od liste rezultata
	ajax_edit - ako je definiran onda se izmjene odmah šalju ajaxom. Potrebne su još varijable ajax_id, ajax_table
	ajax_id - vidi ajax_edit
	ajax_table - vidi ajax_edit
	errors - errors  - ne treba više, ide preko konteksta

	primjer:
		{{ partial("partials/ctlFileUpload",[
			'value':ad.phone2, 
			'title':'Phone 2', 
			'width':3, 
			'field':'phone2',
			'readonly':1,
			'errors': errors	
		] )}} 
		
		{{ partial("partials/ctlFileUpload",[
			'value':entity.name, 
			'title':'Name', 
			'width':3, 
			'field':'name',
			'readonly':1,
			'ajax_edit':1,
			'ajax_id':entity.id,
			'ajax_table':'n_discounts',
			'js_onclick_value' : "alert('alo');"
			'errors': errors	
		] )}} 
		
		{{ partial("partials/ctlFileUpload",[
			'value':entity.name, 
			'title':'Name', 
			'width':3, 
			'field':'name',
			'readonly':1,
			'_context': _context
		] )}} 


#}

	{% if p is defined %}
		{% if p['field'] is defined %}{% set field = p['field'] %}{% endif %}
		{% if p['value'] is defined %}{% set value = p['value'] %}{% endif %}
		{% if p['width'] is defined %}{% set width = p['width'] %}{% endif %}
		{% if p['title'] is defined %}{% set title = p['title'] %}{% endif %}
		{% if p['readonly'] is defined %}{% set readonly = p['readonly'] %}{% endif %}
		{% if p['password'] is defined %}{% set password = p['password'] %}{% endif %}
		{% if p['placeholder'] is defined %}{% set placeholder = p['placeholder'] %}{% endif %}
		{% if p['_context'] is defined %}{% set _context = p['_context'] %}{% endif %}
		{% if p['errors'] is defined %}{% set _errors = p['errors'] %}{% endif %}
	{% endif %}
	
	{% set is_ajax = false %}
	
	{#{% if errors is defined %}
		{% set _errors = errors %}
	{% endif %}#}
	{% if _errors is not defined and _context is defined %}
		{% if _context['errors'] is defined %}
			{% set _errors = _context['errors'] %}
		{% endif %}
	{% endif %}
 
 {#{ print_r(_context,true)}#}
	
	{% if _context is defined %}
		{% if _context['ajax_edit'] is defined and _context['current_entity_id'] is defined and _context['current_entity_table_name'] is defined and field is defined %}
			{% if _context['ajax_edit'] === true %}
				{% set is_ajax = true %}
				{% set ajax_table = _context['current_entity_table_name'] %}
				{% set ajax_id = _context['current_entity_id'] %}
			{% endif %}
		{% endif %}
		
		{% if _context['_errors'] is defined %}
			{% set _errors = _context['errors'] %}
			
		{% endif %}
		
	{% endif %}
	 
	
{#	AJAX_EDIT{{ ajax_edit }} #}

{#
	{% if  width is defined %}<div class="col-lg-{{ width}} col-md-{{ width }}" >{% endif %} 
		{% if  field is defined and _errors is defined %}<div class="form-group{{ _errors is defined and _errors.filter( field ) ? ' has-error' : '' }}">{% endif %}
			{% if  title  is defined %}<label {% if  field  is defined %}class="control-label" for="{{  field  }}" {% endif %} >{{  title  }}</label>{% endif %}
			
			
			<input 
				 style="box-sizing : border-box;"
				{% if password is defined %} type = "password" {% endif %}
				{% if placeholder is defined %} placeholder = "{{ placeholder}}" {% endif %}
				{% if  readonly  is defined %} readonly {% endif %} 
				{% if  field  is defined %} name="{{  field  }}"  {% endif %} 
				
				class = "
					{% if  field  is defined %} form-control {% endif %}
					{% if is_ajax %} ajax_edit {% endif %}
				"
					
				
				
				{% if is_ajax %}
					{% set id_string = 'id__' ~ ajax_id ~ '__field__' ~ field %}
					id = "{{ id_string }}"
					onchange = " ctlFileUploadUpdateField( '{{ id_string }}', '{{ ajax_table }}', '{{ ajax_id }}', '{{ field }}' ); "
					{% else %}
						id="{{  field  }}"
					{% endif %}
				
				value="{{  value  | default('') }}" maxlength="999" 
			></input>
			{% if _errors is defined %}
				{% if _errors.filter(field) %}
					<p class="help-block">{{ current(_errors.filter(field)).getMessage() }}</p>
				{% endif %}
			{% endif %}
		{% if  field is defined and _errors is defined %}</div>{% endif %}
	{% if  width  is defined %}</div>{% endif %}
#}
								
					

<link rel="stylesheet" type="text/css" href="/assets/css/uploadable-sorter.css" />
<link rel="stylesheet" type="text/css" href="/assets/css/pictureSorter.css" />
<link rel="stylesheet" type="text/css" href="/assets/vendor/jquery-file-upload/jquery.fileupload.css" />
<!-- <script type="text/javascript" src="/assets/suva/js/suva.js"></script> -->
<script type="text/javascript" src="/assets/vendor/jquery-file-upload/jquery.iframe-transport.js"></script>
<script type="text/javascript" src="/assets/vendor/jquery-file-upload/jquery.fileupload.js"></script>

<div class="col-lg-12 uploadable-holder">
	<div class="form-group">
		<label class="control-label" for="parameter_uploadable">
			Fotografije
		</label>
		<div>
			<span class="btn btn-success fileinput-button">
				<span class="glyphicon glyphicon-plus"></span>
				<span>Dodaj fotografije</span>
				<input 
					id="parameter_uploadable" 
					type="file" 
					name="ad_media[]" 
					multiple="" 
					accept=".jpeg,.jpg,.png,image/jpeg,image/png"
					>
				</input>
			</span>
			<br/>
			<br/>
			<div id="parameter_uploadable_progress" class="progress hidden">
				<div class="progress-bar progress-bar-success" style="width: 100%;"></div>
			</div>
			<ul 
				id="parameter_uploadable_gallery" 
				class="uploadable-sorter row ui-sortable" 
				data-form="ad_media_gallery"
				>
				{% if ad %}
					{% for am in ad.AdsMedia() %}
						{% set media = am.Media() %}
						{% if media %}
							{% set url = media.get_url() %}
							{% if url %}
								
								<li class="col-md-3 col-xs-6" style="display: list-item;">
									<!-- <div class="uploadable-sorter-box" data-url="/repository/images/_var/a/f/afd0d2daad2da37a259e177d25879188920068126-GridView.jpg"> -->
									<div class="uploadable-sorter-box" data-url="{{ url }}">
										<div class="uploadable-sorter-box-item text-center">
											<img class="img-rounded uploadable-dragger ui-sortable-handle" src="{{url}}"/>
											<input 
												type="hidden" 
												name="ad_media_gallery[]" value="{{ media.id }}"
												/>
										
										</div>
										<div class="uploadable-sorter-options btn-group btn-group-justified">
											<div class="btn-group btn-group-sm">
												<span class="btn btn-default btn-sm set-as-main disabled" title="Postavi kao glavnu">
													<span class="fa fa-thumb-tack"><!--IE--></span>
												</span>
											</div>
											<div class="btn-group btn-group-sm">
												<span class="btn btn-default btn-sm move-prev disabled" title="Pomakni lijevo">
													<span class="fa fa-caret-left"><!--IE--></span>
												</span>
											</div>
											<div class="btn-group btn-group-sm">
												<span class="btn btn-default btn-sm move-next disabled" title="Pomakni desno">
													<span class="fa fa-caret-right"><!--IE--></span>
												</span>
											</div>
											<div class="btn-group btn-group-sm">
												<span class="btn btn-danger btn-sm delete" title="Obriši">
													<span class="fa fa-trash-o text-danger"><!--IE--></span>
												</span>
											</div>
										</div>
									</div>
								</li>
							{% endif %}
						{% endif %}
					{% endfor %}
				{% endif %}
			</ul>
		</div>
	</div>
</div>

{% if is_ajax == false %}
	{# AKO JE AJAX, ONDA JE RUČNO TREBA POZVATI KAO PARAMETAR KOD AJAX POZIVA #}
	<script type="text/javascript" id="category_specific_js">
		$(document).ready(function(){
			$('#parameter_description_offline').maxlength({
				counterContainer: $('#parameter_description_offline_maxlength_label')
			});
			$('#refresh_ad_description_offline_btn').click(function(){
				$.post(
					'/admin/ads/refreshOfflineDescription',
					$('#parameter_description_offline').closest('form').serializeArray(),
					function(json) {
						if (json.status) {
							$('#parameter_description_offline_avus_id').html(json.data.avus);
							$('#parameter_description_offline').val(json.data.content).trigger('change');
						} else if ('undefined' !== typeof json.msg) {
							alert(json.msg);
						}
					},
					'json'
				);
			});
			function uploadableParam_append(data, selector) {
				var html_builder = function(str) {
					var $html = $('<div/>').append(str);
					return $html.children();
				};

				$.each(data.media, function(idx, media){
					var $remover = html_builder('<div class="btn-group btn-group-sm"><span class="btn btn-danger btn-sm delete" title="Obriši"><span class="fa fa-trash-o text-danger"><!--IE--></span></span></div>');
					$remover.bind('click', function(){
						uploadableParam_delete($(this));
					});

					var $next = html_builder('<div class="btn-group btn-group-sm"><span class="btn btn-default btn-sm move-next" title="Pomakni desno"><span class="fa fa-caret-right"><!--IE--></span></span></div>');
					$next.bind('click', function(){
						uploadableParam_moveNext($(this));
					});

					var $prev = html_builder('<div class="btn-group btn-group-sm"><span class="btn btn-default btn-sm move-prev" title="Pomakni lijevo"><span class="fa fa-caret-left"><!--IE--></span></span></div>');
					$prev.bind('click', function(){
						uploadableParam_movePrev($(this));
					});

					var $main = html_builder('<div class="btn-group btn-group-sm"><span class="btn btn-default btn-sm set-as-main" title="Postavi kao glavnu"><span class="fa fa-thumb-tack"><!--IE--></span></span></div>');
					$main.bind('click', function(){
						uploadableParam_setAsMain($(this));
					});

					

					var $buttons = html_builder('<div class="uploadable-sorter-options btn-group btn-group-justified"></div>');
					$buttons.append($main).append($prev).append($next).append($remover);

					$('<li/>')
						.addClass('col-md-3 col-xs-6')
						.append(
							$('<div/>')
								.addClass('uploadable-sorter-box')
								.attr('data-url', media.preview.src).data('url', media.preview.src)
								.append(
									$('<div/>')
										.addClass('uploadable-sorter-box-item text-center')
										.append(
											$('<img/>')
												.addClass('img-rounded uploadable-dragger ui-sortable-handle')
												.attr('src', media.preview.src)
										)
										.append(
											$('<input/>')
												.attr('type', 'hidden')
												.attr('name', $(selector).data('form') + '[]')
												.attr('value', media.id)
										)
								)
								.append($buttons)
								
						)
						.fadeIn('fast', function(){
							$(this).appendTo(selector);
							uploadableParam_modifyFirstLast();
						});
				});
			}
			function uploadableParam_setAsMain($sender) {
				$sender.closest('li').prependTo($sender.closest('ul'));
				uploadableParam_modifyFirstLast();
			}
			function uploadableParam_movePrev($sender) {
				var item = $sender.closest('li');
				if (item.length > 0) {
					var prev = item.prev();
					if (0 === prev.length) {
						return;
					}
					item.insertBefore(prev);
				}
				uploadableParam_modifyFirstLast();
			}
			function uploadableParam_moveNext($sender) {
				var item = $sender.closest('li');
				if (item.length > 0) {
					var next = item.next();
					if (0 === next.length) {
						return;
					}
					item.insertAfter(next);
				}
				uploadableParam_modifyFirstLast();
			}
			function uploadableParam_delete($sender) {
				$sender.closest('li').fadeOut('fast', function(){
					$container = $sender.closest('ul');
					$(this).remove();
					uploadableParam_modifyFirstLast();
				});
			}
			function uploadableParam_modifyFirstLast() {
				// disable the first's .set-as-main and .move-left buttons
				$('.uploadable-sorter li:first').find('.set-as-main, .move-prev').addClass('disabled');
				// disable .move-next too if we only have one sortable element
				var cnt = $('.uploadable-sorter li').length;
				if (cnt <= 1) {
					$('.uploadable-sorter li:first').find('.move-next').addClass('disabled');
				} else {
					// more than one available, enable those buttons now even on the first one
					$('.uploadable-sorter li:first').find('.move-next').removeClass('disabled');
				}
				// enable all those potentially disabled buttons on all others except the first one
				$('.uploadable-sorter li').not(':eq(0)').find('.set-as-main, .move-prev, .move-next').removeClass('disabled');

				// disable next button on last one
				$('.uploadable-sorter li:last').find('.move-next').addClass('disabled');
			}
			$('#parameter_uploadable').fileupload({
				url: '/ajax/upload',
				dataType: 'json',
				dropZone: null,
				pasteZone: null,
				limitMultiFileUploads: 5,
				// acceptFileTypes: /(\.|\/)(gif|jpe?g|png)$/i,
				acceptFileTypes: /(\.|\/)(jpe?g|png)$/i,
				done: function (e, data) {
					if ('undefined' !== data.result) {
						if (data.result.ok) {
							uploadableParam_append(data.result, '#parameter_uploadable_gallery');
							//$('<p/>').text(data.result.picture.filename).appendTo('#parameter_uploadable_gallery');
						} else {
							// TODO: handle failure in some way...
						}
					}
				},
				progressall: function (e, data) {
					var progress = parseInt(data.loaded / data.total * 100, 10);
					$('#parameter_uploadable_progress .progress-bar').css(
						'width',
						progress + '%'
					);
				},
				start: function(e) {
					$('#parameter_uploadable_progress').removeClass('hidden');
				},
				stop: function(e) {
					$('#parameter_uploadable_progress').addClass('hidden');
				}
			}).prop('disabled', !$.support.fileInput)
			  .parent().addClass($.support.fileInput ? undefined : 'disabled');

			$('#parameter_uploadable_gallery').sortable({
				forcePlaceholderSize: true,
				handle: '.uploadable-dragger',
				helper: 'clone',
				listType: 'ul',
				items: 'li',
				opacity: .6,
				placeholder: 'col-md-3 col-lg-3 placeholder',
				revert: 250,
				tabSize: 25,
				create: function(event, ui) {
					// initialize set-as-main buttons
					$('.uploadable-sorter .uploadable-sorter-options .btn.set-as-main').click(function(){
						uploadableParam_setAsMain($(this));
					});
					// initialize move-prev buttons
					$('.uploadable-sorter .uploadable-sorter-options .btn.move-prev').click(function(){
						uploadableParam_movePrev($(this));
					});
					// initialize move-next buttons
					$('.uploadable-sorter .uploadable-sorter-options .btn.move-next').click(function(){
						uploadableParam_moveNext($(this));
					});
					// initialize delete buttons
					$('.uploadable-sorter .uploadable-sorter-options .btn.delete').click(function(){
						uploadableParam_delete($(this));
					});
				},
				stop: function(event, ui) {
					uploadableParam_modifyFirstLast();
				}
			});    promise_parameter_location = [];

			function parameter_location_handle_needed_levels(last_needed_level) {
				var max_location_level = 4;
				for (var i = 1; i <= max_location_level; i++) {
					var $current_level_select = $('#parameter_location_level_' + i);
					if ('undefined' !== typeof $current_level_select && 1 === $current_level_select.length) {
						var current_level_type = $current_level_select.data('type');
						var $current_level_form_group = $current_level_select.closest('.form-group');
						if (i <= last_needed_level) {
							$current_level_form_group.show();
							$current_level_select.prop('disabled', false)
							if (current_level_type !== 'point') {
								$current_level_select.selectpicker('refresh');
							}
						} else {
							$current_level_form_group.hide();
							if (current_level_type !== 'point') {
								$current_level_select.prop('disabled', true).selectpicker('refresh');
							}
						}
					}
				}
			}

			function parameter_location_onChange($source, $target) {
				if ('undefined' != typeof $source.val()) {
					var last_location_needed_level = parseInt($source.attr('id').replace('parameter_location_level_', ''));
					if ('' != $.trim($source.val())) {
						var $selected_option = $source.find(':selected');
						if ($target.data('type') !== 'point') {
							promise_parameter_location[$source.attr('id')] = $.ajax({
								url: '/ajax/location/' + $source.val() + '/values',
								dataType: 'json',
								success: function(data) {
									$target.find('option').remove();
									if (data.length) {
										last_location_needed_level++;
										$target.append(
											$('<option/>').attr('value', '').text('')
										);
										$.each(data, function(idx, data){
											$target.append(
												$('<option/>').attr('value', data.id).text(data.name).data('lat', data.lat).data('lng', data.lng).data('zoom', data.zoom)
											);
										});
										$target.selectpicker('refresh');
									}
									parameter_location_handle_needed_levels(last_location_needed_level);
								}
							});
						} else {
							last_location_needed_level++;
							parameter_location_handle_needed_levels(last_location_needed_level);
							var $google_map_container = $('#' + $target.attr('id') + '_map');
							if ('undefined' !== typeof $google_map_container) {
								$google_map_container.attr('data-lat', $selected_option.data('lat')).data('lat', $selected_option.data('lat'));
								$google_map_container.attr('data-lng', $selected_option.data('lng')).data('lng', $selected_option.data('lng'));
								$google_map_container.attr('data-zoom', $selected_option.data('zoom')).data('zoom', $selected_option.data('zoom'));
								$target.closest('.form-group').show('fast', function(){
									parameter_location_initMap();
								});
							}
						}
						if ('undefined' !== typeof parameter_location_map && parameter_location_map) {
							parameter_location_setCoords($selected_option.data('lat'), $selected_option.data('lng'));
							parameter_location_map.changeView(
								$selected_option.data('lat'),
								$selected_option.data('lng'),
								$selected_option.data('zoom')
							);
						}
					} else {
						$target.selectpicker('refresh');
						parameter_location_handle_needed_levels(last_location_needed_level);
					}
				} else {
					$target.selectpicker('refresh');
				}
			}
			$('#parameter_location_level_1').change(function(){
				parameter_location_onChange($(this), $('#parameter_location_level_2'));
			});
			$('#parameter_location_level_2').change(function(){
				parameter_location_onChange($(this), $('#parameter_location_level_3'));
			});
			$('#parameter_location_level_3').change(function(){
				parameter_location_onChange($(this), $('#parameter_location_level_4'));
			});
			$('#parameter_location_level_2, #parameter_location_level_3, #parameter_location_level_4').selectpicker({
				style: 'btn btn-default',
				size: 8,
				noneSelectedText: '',
				liveSearch: true,
				noneResultsText: '',
				mobile: OGL.is_mobile_browser,
				iconBase: 'fa',
				tickIcon: 'fa-check text-primary'
			});
			$('#parameter_price_input').autoNumeric({aSep:'.', aDec:',', mDec:'0', lZero:'deny', vMax: '99999999999999.99'});
			$('#parameter_price_input').change(function(){
				$('#parameter_price').val($('#parameter_price_input').autoNumeric('get'));
			});
			$('#parameter_price').val($('#parameter_price_input').autoNumeric('get'));
			$('#parameter_6_preview_btn').click(function(){
				var preview_url = $.trim($('#parameter_6').val());
				if (preview_url) {
					$.post(
						'/ajax/check-video-url', {
							'url': preview_url
						},
						function (json) {
							if (json.status) {
								preview_url = json.url;
							}
							$.fancybox({
								type: 'iframe',
								href: preview_url
							});
						},
						'json'
					);
				}
			});
		});	

	</script>
{% endif %}