{#
	ctlMany2Many parametri:
		width - ako je postavljen dodaje div class="col-mg-width-..width".....
		title - ako je naveden dodaje Label
		list - kada bude napravljeno editiranje pomoću ovoga onda če tu biti lista
		value - array koji sadrži listu vrijednosti
		option_fields - ako je postavljeno, od ovih se polja slaže tekst u dropdownu (npr: ['id', 'path' ). Ako nije ništa navedeno default je 'name'

		field - polje u koje se vraća lista checkiranih vrijednosti
		
		value2 - lista vrijednosti
		readonly - po defaultu true
		
		each_option_in_new_line = false
		show_entire_list - ako je postavljeno, onda je prikazana cijela lista, a tamo di je postoji i value je sivo, inače je bijelo
		
		show_active_insertions - specifičan slučaj za insertione na order itemu, ne pokazuje istekle osim onih koji su označeni
		height - ako je postavljen, ograničena visina kontrole
		primjer: 
			{{ partial("partials/ctlMany2Many",[
				'title':'Users roles', 
				'value':entity.Roles(), 
				'width':4, 
				'option_fields':['id','name'],
			] )}}	



#}

	{% if p is defined %}
		{% if p['width'] is defined %}{% set width = p['width'] %}{% endif %}
		{% if p['title'] is defined %}{% set title = p['title'] %}{% endif %}
		{% if p['value'] is defined %}{% set list = p['list'] %}{% endif %}
		{% if p['option_fields'] is defined %}{% set option_fields = p['option_fields'] %}{% endif %}
	{% endif %}
	
	{% set _list = null %}
	{% if list is defined %}
		{% if is_array(list)  %} 
			{% set _list = list %}
		{% else %}
			{% set _list = list.toArray() %}
		{% endif %}
	{% endif %}
	
	{% set _value = [] %}
	{% if value is defined %}
		{% if is_array( value ) %}
			{% set _value = value %}
		{% else %}
			{# ako nije array onda je recordset, pa treba napraviti listu id-jeva #}
			{% for item in value %}
				{% set a = array_push( _value, item.id ) %}
			{% endfor %}
		{% endif %}
	{% endif %}
	
	{% set _show_entire_list = false %}
	{% if show_entire_list is defined %}
		{% if show_entire_list == true or show_entire_list == 1 %}
			{% set _show_entire_list = true %}
		{% endif %}
	{% endif %}

	{% set _readonly = true %}
	{% if readonly is defined %}
		{% if readonly == false or readonly == 0 %}
			{% set _readonly = false %}
		{% endif %}
	{% endif %}	

	
	
	{% set _each_option_in_new_line = true %}
	{% if each_option_in_new_line is defined %}
		{% if each_option_in_new_line == false or each_option_in_new_line == 0 %}
			{% set _each_option_in_new_line = false %}
		{% endif %}
	{% endif %}		

	{% set _first_checked_option_id = null %}
{% if  width is defined %}<div class="col-lg-{{ width }} col-md-{{ width }}">{% endif %} 
	{% if  title  is defined %}<label {% if  field  is defined %}class="control-label" for="{{  field }}" {% endif %} >{{  title  }}</label>{% endif %}
	<div 
		style = "
			width:100%;
			{% if height is defined %}
				height: {{height}}px;
				overflow-y:scroll;
			{% endif %}
		" >
		{%  if _show_entire_list %}
			{% if _list %}
				{% for item in _list %}
					{% set _single_value = item['id'] %}
					{% set _option_id = field ~ '_checkbox_' ~ item['id'] %}
					
					{% if in_array( _single_value, _value ) %}
						{% set _txt_checked = 'checked' %}
						{% if _first_checked_option_id === null %}
							{% set _first_checked_option_id = _option_id %}
						{% endif %}
					{% else %}
						{% set _txt_checked = '' %}
					{% endif %}
					
					<div style="
						{% if ! _each_option_in_new_line %}	float:left; {% endif %}
						{% if in_array( _single_value, _value ) %}
							background-color:rgba(0,0,0,.3); 
						{% else %}
							background-color:rgba(0,0,0,.1); 
						{% endif %}

						border-radius: .4rem;  padding-left:0.05em; padding-right:0.05em; padding-top:0.01em; padding-bottom:0.01em; margin-bottom: 0.2em; margin-left: 0.2em;"> 
						{% if ! _readonly %}
							<input 
								id = "{{ _option_id }}"
								type = "checkbox" 
								name = "{{ field }}[]"
								value ="{{ item['id'] }}" 
								{{ _txt_checked }}
								>
							</input>
						{% endif %}
						{% if  option_fields is defined %}
							{% for option_field in  option_fields %}{{ item[option_field] }}&nbsp;-&nbsp;{% endfor %}
						{% else %}
							{{ item['id'] }}&nbsp;{{ item['name'] }}
						{% endif %}
					</div>
					{%  if _first_checked_option_id > '' %}
						<script>
							$( "#{{ _first_checked_option_id }}" ).focus();
						</script>
					{% endif %}
				{% endfor %}
			{% endif %}
		{% else %}
			{% if _list %}
				{% for item in _list %}
					{% set _single_value = item['id'] %}
					{% if in_array( _single_value, _value ) %}
						{% if _each_option_in_new_line and loop.index > 1 %}<br/>{% endif %}
						<div style="
							{% if ! _each_option_in_new_line %}	float:left; {% endif %}
							
							background-color:rgba(0,0,0,.2); border-radius: .4rem;  padding-left:0.05em; padding-right:0.05em; padding-top:0.01em; padding-bottom:0.01em; margin-bottom: 0.2em; margin-left: 0.2em;"> 
							{% if  option_fields is defined %}
								{% for option_field in  option_fields %}{{ item[option_field] }}&nbsp;-&nbsp;{% endfor %}
							{% else %}
								{{ item['id'] }}&nbsp;{{ item['name'] }}
							{% endif %}
						</div>
						
					{% endif %}
					
				{% endfor %}
			{% endif %}
		{% endif %}
	</div>
{% if  width  is defined %}</div>{% endif %}

