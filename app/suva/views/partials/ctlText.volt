{#
	ctlText parametri:  
	p - array sa svim parametrima, radi rekurzivnog pozivanja (ako je definirann parametris e uzimaju iz njega)
	field - 'ime_polja'  - ako je postavljen dodaje klasu inputa (postaje form control)
	value - vrijednost polja
	width - ako je postavljen dodaje div class="col-mg-width-..width".....
	title - ako je naveden dodaje Label
	readonly - ako je postavljen (1) dodaje READONLY na input
	password - ako je postavljen onda se ne prikazuju znakovi
	placeholder - defaultni tekst (( Unesite vrijednost.. ))
	value - vrijednost polja
	js_onclick_value - js koji se doda na onclick event od liste rezultata
	ajax_edit - ako je definiran onda se izmjene odmah šalju ajaxom. Potrebne su još varijable ajax_id, ajax_table
	ajax_id - vidi ajax_edit
	ajax_table - vidi ajax_edit
	errors - errors  - ne treba više, ide preko konteksta

	primjer:
		{{ partial("partials/ctlText",[
			'value':ad.phone2, 
			'title':'Phone 2', 
			'width':3, 
			'field':'phone2',
			'readonly':1,
			'errors': errors	
		] )}} 
		
		{{ partial("partials/ctlText",[
			'value':entity.name, 
			'title':'Name', 
			'width':3, 
			'field':'name',
			'readonly':1,
			'ajax_edit':1,
			'ajax_id':entity.id,
			'ajax_table':'n_discounts',
			'js_onclick_value' : "alert('alo');"
			'errors': errors	
		] )}} 
		
		{{ partial("partials/ctlText",[
			'value':entity.name, 
			'title':'Name', 
			'width':3, 
			'field':'name',
			'readonly':1,
			'_context': _context
		] )}} 


#}

	{% if p is defined %}
		{% if p['field'] is defined %}{% set field = p['field'] %}{% endif %}
		{% if p['value'] is defined %}{% set value = p['value'] %}{% endif %}
		{% if p['width'] is defined %}{% set width = p['width'] %}{% endif %}
		{% if p['title'] is defined %}{% set title = p['title'] %}{% endif %}
		{% if p['readonly'] is defined %}{% set readonly = p['readonly'] %}{% endif %}
		{% if p['password'] is defined %}{% set password = p['password'] %}{% endif %}
		{% if p['placeholder'] is defined %}{% set placeholder = p['placeholder'] %}{% endif %}
		{% if p['_context'] is defined %}{% set _context = p['_context'] %}{% endif %}
		{% if p['errors'] is defined %}{% set _errors = p['errors'] %}{% endif %}
	{% endif %}
	
	{% set is_ajax = false %}
	
	{#{% if errors is defined %}
		{% set _errors = errors %}
	{% endif %}#}
	{% if _errors is not defined and _context is defined %}
		{% if _context['errors'] is defined %}
			{% set _errors = _context['errors'] %}
		{% endif %}
	{% endif %}
 
 {#{ print_r(_context,true)}#}
	
	{% if _context is defined %}
		{% if _context['ajax_edit'] is defined and _context['current_entity_id'] is defined and _context['current_entity_table_name'] is defined and field is defined %}
			{% if _context['ajax_edit'] === true %}
				{% set is_ajax = true %}
				{% set ajax_table = _context['current_entity_table_name'] %}
				{% set ajax_id = _context['current_entity_id'] %}
			{% endif %}
		{% endif %}
		
		{% if _context['_errors'] is defined %}
			{% set _errors = _context['errors'] %}
			
		{% endif %}
		
	{% endif %}
	 
	
{#	AJAX_EDIT{{ ajax_edit }} #}


	{% if  width is defined %}<div  class="col-lg-{{ width}} col-md-{{ width }}" >{% endif %} 
		{% if  field is defined and _errors is defined %}<div class="form-group{{ _errors is defined and _errors.filter( field ) ? ' has-error' : '' }}">{% endif %}
			{% if  title  is defined %}<label {% if  field  is defined %}class="control-label" for="{{  field  }}" {% endif %} >{{  title  }}</label>{% endif %}
			
			
			<input 
				 style="box-sizing : border-box;"
				{% if password is defined %} type = "password" {% endif %}
				{% if placeholder is defined %} placeholder = "{{ placeholder}}" {% endif %}
				{% if  readonly  is defined and readonly == 1 %} readonly {% endif %} 
				{% if  field  is defined %} name="{{  field  }}"  {% endif %} 
				
				class = "
					{% if  field  is defined %} form-control {% endif %}
					{% if is_ajax %} ajax_edit {% endif %}
				"
					
				
				
				{% if is_ajax %}
					{% set id_string = 'id__' ~ ajax_id ~ '__field__' ~ field %}
					id = "{{ id_string }}"
					onchange = " ctlTextUpdateField( '{{ id_string }}', '{{ ajax_table }}', '{{ ajax_id }}', '{{ field }}' ); "
					{% else %}
						id="{{  field  }}"
					{% endif %}
				
				value="{{  value  | default('') }}"  {%if _maxlength is defined %} maxlength="{{ _maxlength }}" {% endif %}
			></input>
			{% if _errors is defined %}
				{% if _errors.filter(field) %}
					<p class="help-block">{{ current(_errors.filter(field)).getMessage() }}</p>
				{% endif %}
			{% endif %}
		{% if  field is defined and _errors is defined %}</div>{% endif %}
	{% if  width  is defined %}</div>{% endif %}