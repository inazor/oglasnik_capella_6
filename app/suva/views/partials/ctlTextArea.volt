{#
<div class="col-lg-{{ width}} col-md-{{ width }}">

<div class="form-group" data-company-label="Company registration date" data-personal-label="Layout">
<label for="{{field}}" class="control-label">{{title}}</label>
	<textarea class="form-control" name="{{ field }}" id="{{ field }}" rows="4" cols="40">{{ value }}</textarea>
</div>
</div> 
#}

{#
	ctlTextArea parametri:  
	field - 'ime_polja'  - ako je postavljen dodaje klasu inputa (postaje form control)
	value - vrijednost polja
	width - ako je postavljen dodaje div class="col-mg-width-..width".....
	title - ako je naveden dodaje Label

	readonly - ako je postavljen (1) dodaje READONLY na input
	placeholder - defaultni tekst (( Unesite vrijednost.. ))
	rows - broj redova, ako nije naveden, 4
	cols - broj stupaca, ako nije naveden, 40
	errors - errors

	primjer; ctlTextArea (['value':entity.name, 'title':'Name', 'width':3, 'rows':4, 'cols':40, 'field':'name', 'readonly':1 ] ) 
#}

	{% set is_ajax = false %}
	<!-- TODO: izbrisati ovo greske idu preko contexta -->
	{#{% if errors is defined %}
		{% set _errors = errors %}
	{% endif %}#}

	{% if _context is defined %}
		{% if _context['ajax_edit'] is defined and _context['current_entity_id'] is defined and _context['current_entity_table_name'] is defined and field is defined %}
			{% if _context['ajax_edit'] === true %}
				{% set is_ajax = true %}
				{% set ajax_table = _context['current_entity_table_name'] %}
				{% set ajax_id = _context['current_entity_id'] %}
			{% endif %}
		{% endif %}
		
		{% if _context['_errors'] is defined %}
			{% set _errors = _context['errors'] %}
		{% endif %}
		
	{% endif %}
	
	{% set _readonly = false %}
	{% if readonly is defined %}
		{% if readonly == true or readonly == 1 %}
			{% set _readonly = true %}
		{% endif %}
	{% endif %}


	{% if  width is defined %}<div class="col-lg-{{ width}} col-md-{{ width }}">{% endif %} 
		{% if  field  is defined %}<div class="form-group{{ _errors is defined and _errors.filter( field ) ? ' has-error' : '' }}">{% endif %}
			{% if  title  is defined %}<label {% if  field  is defined %}class="control-label" for="{{  field  }}" {% endif %} >{{  title  }}</label>{% endif %}
			<textarea 
				rows="{% if rows is defined %}{{ rows }}{% else %}4{% endif %}"
				cols="{% if cols is defined %}{{ cols }}{% else %}40{% endif %}"
				{% if placeholder is defined %} placeholder = "{{ placeholder}}" {% endif %}
				{% if  _readonly %} readonly {% endif %} 
				{% if  field  is defined %} class="form-control" name="{{  field  }}" {% endif %} 
				
				{% if is_ajax %}
					{% set id_string = 'id__' ~ ajax_id ~ '__field__' ~ field %}
					id = "{{ id_string }}"
					onchange = "
						//console.log('onchange'); 
						//alert('onchange');
						var value = $( '#{{ id_string }}' ).val();
						urlEncodedValue = encodeURIComponent(value);
						
						var txtUrl = '/ajax/updateField' 
							 + '/{{ ajax_table }}'
							 + '/{{ ajax_id }}'
							 + '/{{ field }}' 
							 + '/' + urlEncodedValue;
						console.log( txtUrl );
						
						$.ajax({
							async: false,
							url: txtUrl,
							cache: false,
							dataType: 'json'
						}).done(function(json_data) {
							//alert('received');
							console.log('received');
							if (json_data){
								console.log(json_data.length);

								for(var i = 0; i < json_data.length; i++) {
									var obj = json_data[i];
									//alert(obj);
									if (obj == 'OK')
										$( '#{{ id_string }}' ).css('border', '3px solid green');
									else
										$( '#{{ id_string }}' ).css('border', '3px solid red');
									setTimeout(function(){
										$( '#{{ id_string }}' ).css('border', '1px solid gray');
										}, 1000);
								}; 
							}  
							else{
								$( '#{{ id_string }}' ).css('border', '3px solid red');
								setTimeout(function(){
									$( '#{{ id_string }}' ).css('border', '1px solid gray');
									}, 1000);
								alert('nothing received');
								console.log('received null');
							}
						});
						"
				{% endif %}
			>{{  value  | default('') }}</textarea> 
		{% if _errors is defined and _errors.filter(field) %}
			<p class="help-block">{{ current(_errors.filter(field)).getMessage() }}</p>
		{% endif %}
		{% if  field  is defined %}</div>{% endif %}
	{% if  width  is defined %}</div>{% endif %}