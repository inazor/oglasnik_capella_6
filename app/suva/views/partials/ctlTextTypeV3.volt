<style>
input, select, textarea{
 
    
    -ms-box-sizing:content-box;
    -moz-box-sizing:content-box;
    box-sizing:content-box;
    -webkit-box-sizing:content-box; 
}



</style>

{#
	ctlTextTypeV3 parametri:  
	
	
	field - 'ime_polja'  - ako je postavljen dodaje klasu inputa (postaje form control)
	value - vrijednost parametra 
	title - naslov
	errors - greške
	value_show - vrijednost koja će se pokazati kad se učita prozor (TODO: automatski)
	model_name - ime modela koji se pretražuje u bazi
	expr_search - sql izraz ili ime polja koji se pretražuje (npr. name) 
	expr_value - sql izraz ili ime polja koji vraća vrijednost (defaultno id) 
	expr_show_input - sql izraz ili ime polja koji će se pokazati u input boxu nakon sto se odabere list item
	expr_show_list - sql izraz ili ime polja koji će se pokazati na listi
	additional_where - dodatni kriteriji u tablici, na kraju where clause, moraju sadržavati .. and kriterij.. 
	keep_typed_text  - akos e odabere utipkani tekst ostaje u kontroli iako ga nema na listi
	js_PAR1 - javascript izraz za vrijednost parametra 1 koji će se koristiti u slanju upita:
			npr. "$('input[name=country_id]')"
	
	readonly - readonly - JOŠ NIJE DEFINIRAN
	placeholder - nije definiran
	debug - šalju se debug poruke
	
	allow_add_new : true
	add_new_url : "/suva/locations/addNewCity/__P1__/Berlin"
	'input_P1' : 'country_id'
	
	
	
	SQL U BAZI
		select 
			$exprValue as _value
			, $exprShowInput as _show_input 
			, $exprShowList as _show_list 
		from 
			$tableName 
		where 
			where cast( ( $exprSearch )  as char) like '%$pValueSearch%'  
			$pAdditionalWhere
		limit 15
	
	primjer; {{ partial("partials/ctlTextType",['field':'n_category_id', 'search_field':'name', 'return_field':'name', 'table_name':'category', 'value_show':category.n_path, 'value_id':category.id, 'width':3] ) }}
	
					{{ partial("partials/ctlTextTypeV3",[
						'title' : "Kategorija"
						, 'field' : 'category_id'
						, 'value' : category.id
						, 'width' : 3
						, 'errors' : errors
						, 'value_show' : category.n_path
						, 'model_name' : 'Categories'
						, 'expr_search' : 'name'
						, 'expr_value' : 'id' 
						, 'expr_show_input' : 'n_path'
						, 'expr_show_list' : 'n_path'
						, 'additional_where' : ' and transaction_type_id is not null '
						, 'nr_returned_rows' : 15
					]) }}
									
	
	
	WARNING:
		- u additional_where se ne smiju koristiti navodnici nego samo '
		
		
	
	
	TODO:: Šta ako se ukuca nešto bezveze i po  tome se ništa ne pronađe? Onda to ostane u inputu i treba nekako javit da to nije valjan unos
#}
	<!-- ctlTextType_V3 -->
	
	{% if p is defined %}
		{% if p['field'] is defined %}{% set field = p['field'] %}{% endif %}
		{% if p['value'] is defined %}{% set value = p['value'] %}{% endif %}
		{% if p['width'] is defined %}{% set width = p['width'] %}{% endif %}
		{% if p['title'] is defined %}{% set title = p['title'] %}{% endif %}
		{% if p['list'] is defined %}{% set list = p['list'] %}{% endif %}
		{% if p['value_show'] is defined %}{% set value_show = p['value_show'] %}{% endif %}
		{% if p['model_name'] is defined %}{% set model_name = p['model_name'] %}{% endif %}
		{% if p['expr_search'] is defined %}{% set expr_search = p['expr_search'] %}{% endif %}
		{% if p['expr_value'] is defined %}{% set expr_value = p['expr_value'] %}{% endif %}
		{% if p['expr_show_input'] is defined %}{% set expr_show_input = p['expr_show_input'] %}{% endif %}
		{% if p['expr_show_list'] is defined %}{% set expr_show_list = p['expr_show_list'] %}{% endif %}
		{% if p['additional_where'] is defined %}{% set additional_where = p['additional_where'] %}{% endif %}
		{% if p['nr_returned_rows'] is defined %}{% set nr_returned_rows = p['nr_returned_rows'] %}{% endif %}
		{% if p['readonly'] is defined %}{% set readonly = p['readonly'] %}{% endif %}
		{% if p['keep_typed_text'] is defined %}{% set keep_typed_text = p['keep_typed_text'] %}{% endif %}
		{% if p['placeholder'] is defined %}{% set placeholder = p['placeholder'] %}{% endif %}
		{% if p['debug'] is defined %}{% set debug = p['debug'] %}{% endif %}
		{% if p['_context'] is defined %}{% set _context = p['_context'] %}{% endif %}
	
	{% endif %}
	
	{#{ var_dump (p) }#}
	
	{% set _js_onclick_value = ( js_onclick_value is defined ) ? js_onclick_value : '' %}
	{% set _js_beforecreate_value = ( js_beforecreate_value is defined ) ? js_beforecreate_value : '' %}
	
 
	
	{#{% if errors is defined %}
		{% set _errors = errors %}
	{% endif %}#}
	{% if _errors is not defined and _context is defined %}
		{% if _context['errors'] is defined %}
			{% set _errors = _context['errors'] %}
		{% endif %}
	{% endif %}
	
	
	
	

 
	{% set _readonly = false %}
	{% if readonly is defined %}
		{% if readonly === true %}
			{% set _readonly = true %}
		{% endif %}
	{% endif %}
	
	{% if field is defined %}
		{% set escField = str_replace(']','_',str_replace('[','_',field)) %}
	{% else %}
		{% set escField = 'rnd_' ~ rand(1,10000) %}
	{% endif %}
	
	{% if additional_where is not defined %}
		{% set additional_where = '' %}
	{% endif %}
	
	{% if keep_typed_text is not defined %}
		{% set _keep_typed_text = 'false' %}
	{% else %}
		{%  if keep_typed_text %}
			{%  set _keep_typed_text = 'true' %}
		{% else %}
			{%  set _keep_typed_text = 'false' %}
		{% endif %}
	{% endif %}
	

	{% if value is defined %}
		{% set _value = value %}
	{% else %}
		{% set _value = '' %}
	{% endif %}
	
	{% if input_P1 is defined %}
		{% set _input_P1 = input_P1 %}
	{% else %}
		{% set _input_P1 = '' %}
	{% endif %}
	
	{% if value_show is defined %}
		{% set _value_show = value_show %}
	{% else %}
		{% set _value_show = '' %}
	{% endif %}

	{% if nr_returned_rows is defined %}
		{% set _nr_returned_rows = nr_returned_rows %}
	{% else %}
		{% set _nr_returned_rows = 15 %}
	{% endif %}
	
	{% if debug is defined %}
		{% set _debug = debug %}
	{% else %}
		{% set _debug = false %}
	{% endif %}
	{% set _js_debug = ( _debug == true ? 'true' : 'false' ) %}
	
	
<script>
//testCtlPartial();
//alert ( eval("{#{ js_P1 }#}") );
</script>
{#{input_P1}#}
	{% set is_ajax = false %}
	{% if _context is defined %}
		{% if _context['ajax_edit'] is defined and _context['current_entity_id'] is defined and _context['current_entity_table_name'] is defined and field is defined %}
			{% if _context['ajax_edit'] === true %}
				{% set is_ajax = true %}
				{% set ajax_table = _context['current_entity_table_name'] %}
				{% set ajax_id = _context['current_entity_id'] %}
				
				{% set escField = escField ~ '_rec_' ~ ajax_id ~ '_' %}
			{% endif %}
		{% endif %}
		
		{% if _context['_errors'] is defined %}
			{% set _errors = _context['errors'] %}
		{% endif %}
		
		
		
	{% endif %}
  
		{% if width is defined %}<div class="col-lg-{{width}} col-md-{{width}}"> {% endif %}
			{% if  field is defined and _errors is defined %}<div class="form-group{{ _errors is defined and _errors.filter( field ) ? ' has-error' : '' }}">{% endif %}
				{% if  title  is defined %}
					<label {% if  field  is defined %}class="control-label" for="{{  field  }}" {% endif %} >
						{{  title  }}
					</label>
				{% endif %}
				
				{% if field is defined %} 
					<input type="hidden" id="{{escField}}" name ="{{field}}" value="{{_value}}">
					</input> 
				{% endif %}
				<div style="position:relative; margin-left: 5px;">
					<input 
							 type = "text" 
							 class = "form-control
							 {% if is_ajax %} ajax_edit {% endif %}
							 " 
							 id = "{{ escField }}-input" 
							 value = "{{_value_show}}"

							{% if _readonly === false %}
						   		{#
								onchange = "$( '#_context_cashout_report_user_id__value_list' ).children().eq( 2 ).click();"					   
								#}
								
								{% if is_ajax %}
									
									
									onchange = "
										console.log('input.onchange.ajax'); 
										ctlNumericUpdateField( 
											'{{ escField }}', 
											'{{ ajax_table }}', 
											'{{ ajax_id }}', 
											'{{ field }}', 
											'{{ escField }}-input' 
											); 
										"
									
									{#
									oninput = "console.log('input.onInput.ajax.2');"
									#}
									
									oninput = "
										console.log('input.onInput.ajax');
										ctlTextTypeV3_1({
											modelName : '{{model_name}}'
											, exprSearch : '{{expr_search}}'
											, exprValue : '{{expr_value}}'
											, exprShowList : '{{expr_show_list}}'
											, exprShowInput : '{{expr_show_input}}'
											, additionalWhere : '{{additional_where}}'
											, inputIdPrefix : '{{escField}}'
											, input_P1 : '{{_input_P1}}'
											, keepTypedText : {{_keep_typed_text}}
											, nrReturnedRows : '{{ _nr_returned_rows }}'
											, debug : {{ _js_debug }}
											, pOnClick2 : {  
												escField : '{{ escField }}',
												ajax_table : '{{ ajax_table }}',
												ajax_id : '{{ ajax_id }}',
												field : '{{ field }}',
												escFieldInput : '{{ escField }}-input'
												}
										});"									
									
									
								{% else %}									
									oninput = "console.log('input.onInput.noAjax');
										ctlTextTypeV3_1({
											modelName : '{{model_name}}'
											, exprSearch : '{{expr_search}}'
											, exprValue : '{{expr_value}}'
											, exprShowList : '{{expr_show_list}}'
											, exprShowInput : '{{expr_show_input}}'
											, additionalWhere : '{{additional_where}}'
											, inputIdPrefix : '{{escField}}'
											, input_P1 : '{{_input_P1}}'
											, keepTypedText : {{_keep_typed_text}}
											, nrReturnedRows : '{{ _nr_returned_rows }}'
											, debug : {{ _js_debug }}

											});
											"									
								
								
								
								{% endif %}
								
								
								
								
								
								{#
								oninput = "
									ctlTextTypeV3(
										'{{model_name}}'
										, '{{expr_search}}'
										, '{{expr_value}}'
										, '{{expr_show_list}}'
										, '{{expr_show_input}}'
										, '{{additional_where}}'
										, '{{escField}}'
									);"
								#}	
								
								
							{% else %}
								readonly
							{% endif %}
					></input> 
					<div 
						style = "position:absolute; overflow-y: scroll; cursor:pointer; background-color:#EEEEEE; z-index:1000; max-height: 250px; width:100%;" 
						id = "{{escField}}_value_list"
						hidden
						>
					</div>
						
					{% if _errors is defined %}
						{% if _errors.filter(field) %}
							<p class="help-block">{{ current(_errors.filter(field)).getMessage() }}</p>
						{% endif %}
					{% endif %}
				</div>
			{% if  field is defined and _errors is defined %}</div>{% endif %}	
		{% if width is defined %}</div> {% endif %}