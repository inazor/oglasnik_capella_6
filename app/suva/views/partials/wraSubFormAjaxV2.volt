
{#
Korištenje:

		{{ partial("partials/wraSubFormAjax",[
			'_window_id' : 'usersContacts',
			'_url': 'users-contacts/index/' ~ ad.user_id, 
			'_context': _context, 
		] ) }} 
		
		VAŽNO: ako se ne definira _url onda se samo radi wrapper
#} 


{% set _wrapper_id = '_wrapper_' ~ _window_id %}

{% if _is_js_debug is not defined  %}
	{% set _is_js_debug = false %}
{% endif %}

<div class="row">
	<div class="col-lg-12 col-md-12" id="{{ _wrapper_id }}">
		{% if _url is defined %}
			<script>
				//alert("wrapper prije {{ _url }}   wrapper: {{ _wrapper_id }}");
				ajaxSubmit ( 
					"{{ _url }}"
					, "{{ _window_id }}"
					, null
					, null
					, {{ ( _is_js_debug ? 'true' : 'false'  ) }}  
				);
				//alert("wrapper poslije");
			</script>
		{% endif %}
	</div>
</div>
