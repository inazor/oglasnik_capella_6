{% if errors is not defined %}
	{% set errors = null %}
{% endif %}

{% if _is_ajax_submit is not defined %}
	{% set _is_ajax_submit = '' %}
{% endif %}
<div class="row">
	<div class="col-lg-12 col-md-12">
		<div><h1 class="page-header">{% if entity.id is not defined %}New {% endif %}Payment Methods</h1></div>
		{{ partial("partials/blkFlashSession") }}
		{% if entity is defined %}
			{{ form(NULL, 'id' : 'frm_register', 'method' : 'post', 'autocomplete' : 'off') }}
				{{ hiddenField('_csrftoken') }}
				{{ hiddenField('next') }}
				<input type="hidden" name="id" value="{{entity.id}}"></input>
				<div class="row">
					<div class="col-lg-12 col-md-12">
						<div class="panel panel-default">
							<div class="panel-heading">
								{% if entity.id is defined %}
									<div class="pull-right">[ID: <b>{{ entity.id }}</b> ]</div>
								{% endif %}
								<h3 class="panel-title">Payment Methods</h3>
							</div>
							<div class="panel-body">
								<div class="row">
									{{ partial("partials/ctlText",['value':entity.name, 'title':'Name', 'width':3, 'field':'name', 'errors': errors] ) }}
									{{ partial("partials/ctlText",['value':entity.slug, 'title':'Slug', 'width':3, 'field':'slug', 'errors': errors] ) }}
									
									{{ partial("partials/ctlCheckbox",['value':entity.is_fiscalized, 'title':'is_fiscalized', 'width':2, 'field':'is_fiscalized', 'errors': errors] ) }}
									{{ partial("partials/ctlCheckbox",['value':entity.is_active, 'title':'is_active', 'width':1, 'field':'is_active', 'errors': errors] ) }}
								</div>
								<div class = "row">
									{{ partial("partials/ctlText",['value':entity.nav_id, 'title':'Nav ID', 'width':3, 'field':'nav_id', 'errors': errors] ) }}
									{{ partial("partials/ctlNumeric",['value':entity.nav_payment_type_id, 'title':'Navision Payment type ID', 'width':4, 'field':'nav_payment_type_id', 'decimals':0, '_context': _context] ) }}
									{{ partial("partials/ctlNumeric",['value':entity.nav_credit_card_id, 'title':'Navision Credit Card ID', 'width':4, 'field':'nav_credit_card_id', 'decimals':0, '_context': _context] ) }}
								</div>
							</div>
						</div>
					</div>
				</div>
				{% include "partials/btnSaveCancel.volt" %}
			{{ endForm() }}
		{% else %}
			ENTITY NIJE DEFINIRAN
		{% endif %}
	</div>
</div>