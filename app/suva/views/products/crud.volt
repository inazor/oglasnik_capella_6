{% if errors is not defined %}
	{% set errors = null %}
{% endif %}
 
{% if _is_ajax_submit is not defined %}
	{% set _is_ajax_submit = '' %}
{% endif %}
{#
<pre>
{{ print_r(entity.toArray(), true)  }}
</pre>
#}

<div class="row">
	<div class="col-lg-12 col-md-12">
		<div><h1 class="page-header">{% if entity.id is not defined %}New {% endif %}Products</h1></div>
		{{ partial("partials/blkFlashSession") }}
		{% if entity is defined %}
			{{ form(NULL, 'id' : 'frm_register', 'method' : 'post', 'autocomplete' : 'off') }}
				{{ hiddenField('_csrftoken') }}
				{{ hiddenField('next') }}
				{% if entity.id is defined %}
					<input type="hidden" name="id" value="{{entity.id}}"></input>
				{% endif %}
				<div class="row">
					<div class="col-lg-12 col-md-12">
						<div class="panel panel-default">
							<div class="panel-heading">
								{% if entity.id is defined %}
									<div class="pull-right">[ID: <b>{{ entity.id }}</b> ]</div>
								{% endif %}
								<h3 class="panel-title">Products details</h3>
							</div>
							<div class="panel-body">
								<div class="row">								
									{{ partial("partials/ctlNumeric",['value':entity.sort_order, 'title':'Sort Order', 'width':2, 'field':'sort_order', 'decimals':0, '_context': _context] ) }}
									
									{{ partial("partials/ctlText",['value':entity.name, 'title':'Name', 'width':5, 'field':'name', '_context': _context] ) }}                     
									
									{{ partial("partials/ctlNumeric",['value':entity.unit_price, 'title':'Unit price', 'width':2, 'field':'unit_price', 'decimals':2, '_context': _context] ) }}
								</div>
								<div class="row">
									{{ partial("partials/ctlDropdown",[
										'value':entity.category_id, 
										'title':'Category', 
										'list':_model.Categories(), 
										'field':'category_id', 
										'width':7, 
										'option_fields':['n_path','id'],
										'errors': errors
									] ) }}
									{{ partial("partials/ctlDropdown",['value':entity.publication_id, 'title':'Publication', 'list':_model.Publications(), 'width':4, 'field':'publication_id', '_context': _context] ) }}
								</div>
								<div class="row">
									{{ partial("partials/ctlCheckbox",['value':entity.is_online_product, 'title':'is online product', 'width':3, 'field':'is_online_product', '_context': _context] ) }}		
									{{ partial("partials/ctlText",['value':entity.days_published, 'title':'Days published', 'width':3, 'field':'days_published', '_context': _context] )}}
									{{ partial("partials/ctlText",['value':entity.id_resursa_nav, 'title':'Id Resursa nav', 'width':3, 'field':'id_resursa_nav', '_context': _context] )}}
								</div>
								<div class="row">		
									{{ partial("partials/ctlCheckbox",['value':entity.is_display_ad, 'title':'is display ad', 'width':3, 'field':'is_display_ad', '_context': _context] ) }}							
									{{ partial("partials/ctlText",['value':entity.display_ad_width, 'title':'Ad display width', 'width':3, 'field':'display_ad_width', '_context': _context] )}}							
									{{ partial("partials/ctlText",['value':entity.display_ad_height, 'title':'Ad display height', 'width':3, 'field':'display_ad_height', '_context': _context] )}}							
									
								</div>
								<div class="row">		
									{{ partial("partials/ctlCheckbox",['value':entity.is_naslovnica, 'title':'Ovaj proizvod JE naslovnica', 'width':3, 'field':'is_naslovnica', '_context': _context] ) }}							
									{{ partial("partials/ctlCheckbox",['value':entity.is_naslovnica_allowed, 'title':'Na ovom proizvodu se MOŽE DODATI Naslovnica', 'width':3, 'field':'is_naslovnica_allowed', '_context': _context] )}}							
									
								</div>
								
								<div class="row">
									{{ partial("partials/ctlNumeric",['value':entity.priority_online, 'title':'Online priority', 'width':1, 'field':'priority_online', '_context': _context] ) }}
									{{ partial("partials/ctlCheckbox",['value':entity.is_pushup_product, 'title':'is pushup product', 'width':3, 'field':'is_pushup_product',  '_context': _context] ) }}
									{{ partial("partials/ctlCheckbox",['value':entity.is_published, 'title':'is published', 'width':3, 'field':'is_published',  '_context': _context] ) }}									
									{{ partial("partials/ctlCheckbox",['value':entity.is_active, 'title':'is active', 'width': 3, 'field':'is_active',  '_context': _context] ) }}													
									{{ partial("partials/ctlCheckbox",['value':entity.is_default_product, 'title':'is default product', 'width':3, 'field':'is_default_product',  '_context': _context] ) }}
									{{ partial("partials/ctlCheckbox",['value':entity.is_picture_required, 'title':'is picture required', 'width':2, 'field':'is_picture_required',  '_context': _context] ) }}
								</div>
								<div class="row">
									{{ partial("partials/ctlText",['value':entity.old_product_id, 'title':'Old product ID', 'width':3, 'field':'old_product_id', '_context': _context] ) }}
									{{ partial("partials/ctlText",['value':entity.old_product_option, 'title':'Old product option', 'width':3, 'field':'old_product_option', '_context': _context] ) }}
									{{ partial("partials/ctlText",['value':entity.old_product_extras, 'title':'Old product extras', 'width':3, 'field':'old_product_extras', '_context': _context] ) }}
									{{ partial("partials/ctlText",['value':entity.old_product_extras_options, 'title':'Old product extras options', 'width':3, 'field':'old_product_extras_options', '_context': _context] ) }}
								</div>
								<div class="row">
									{{ partial("partials/ctlDropdown",[
										'value':entity.layouts_id, 
										'title':'Layout', 
										'list':_model.Layouts(), 
										'field':'layouts_id', 
										'width':3, 
										'_context': _context
									] ) }}
									{#{ partial("partials/ctlTextArea",['value':entity.xml_layout, 'title':'XML Layout', 'width':12, 'rows':4, 'cols':40, 'field':'xml_layout', '_context': _context] ) }#}
									{{ partial("partials/ctlCheckbox",['value':entity.dtp_hashtag_before_contact_info, 'title':'Hashtag before contact info', 'width':2, 'field':'dtp_hashtag_before_contact_info',  '_context': _context] ) }}

								</div>
								
							</div>
						</div>
					</div>
				</div>
				{% include "partials/btnSaveCancel.volt" %}
			{{ endForm() }}
		{% else %}
			ENTITY NIJE DEFINIRAN
		{% endif %}
	</div>
</div>