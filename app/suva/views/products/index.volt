{# CONTEXT<pre>{{ print_r( _context, true ) }}</pre>#}

	

{% if _context['default_filter']['publication_id'] is defined %}
	{% set publication = _model.Publication( _context['default_filter']['publication_id'] ) %}
	{% set pubFilter = "publication_id = " ~ publication.id %}
{% else %}
	{% set pubFilter = "1=1" %}
{% endif %}



<div class="row">
	
	<div class="col-lg-12 col-md-12">
		<div>
			
			<h1 class="page-header">
				Products
				{% if publication is defined %}
					for <i>{{ publication.name }}</i>
				{% endif %}
			</h1>
		</div>
		<br/>
		{{ partial("partials/btnAddNew", ['controller':'products'] ) }}
			{{ partial("partials/blkFlashSession") }}
			<table  class="table table-striped table-hover table-condensed">
			{{ partial("partials/blkTableSort",[
			'_context' : _context,
			'fields':[ 
				 ['title':'Options'											,'width':150	,'show_navigation' : true ] 
				,['title':'ID'					,'name':'id'				,'width':100	,'search_ctl':'ctlText'		] 
				,['title':'Name'				,'name':'name'				,'width':250	,'search_ctl':'ctlText'		] 
				,['title':'Avus_adlayout_id'	,'name':'avus_adlayout_id'	,'width':250	,'search_ctl':'ctlText'		] 
				,['title':'Sort Order'			,'name':'sort_order'		,'width':100	,'search_ctl':'ctlNumeric'		] 
				,['title':'Unit price'			,'name':'unit_price'		,'width':100	,'search_ctl':'ctlNumeric'	,'search_ctl_params': [ 'decimals':2 ] ]  
				,['title':'Publication'			,'name':'publication_id'	,'width':250	,'search_ctl':'ctlDropdown'	,'search_ctl_params': [ 'list':_model.Publications( [ 'order' : 'name' ] ) ]]  
				,['title':'Category'			,'name':'category_id'		,'width':400	,'search_ctl':'ctlDropdown'	,'search_ctl_params': [ 'list':_model.Categories( [ 'order' : 'n_path' ] ), 'option_fields':['n_path','id'] ]]  
				,['title':'ID Resursa Nav'		,'name':'id_resursa_nav'	,'width':150	,'search_ctl':'ctlText'		]
				,['title':'Old product ID'		,'name':'old_product_id'	,'width':250	,'search_ctl':'ctlText'	]  
				,['title':'Old product option'		,'name':'old_product_option'	,'width':250	,'search_ctl':'ctlText'	] 
				,['title':'Old product extras'		,'name':'old_product_extras'	,'width':250	,'search_ctl':'ctlText'	] 
				,['title':'Old product extras options'		,'name':'old_product_extras_options'	,'width':250	,'search_ctl':'ctlText'	] 
				,['title':'Online Priority' 	,'name':'online_priority'	,'width':100 	,'search_ctl':'ctlNumeric'	] 
				,['title':'Is Online Product'	,'name':'is_online_product'	,'width':100 	,'search_ctl':'ctlNumeric'	]  
				,['title':'Days Published'		,'name':'days_published'	,'width':100 	,'search_ctl':'ctlNumeric'	]
				,['title':'Layout'				,'name':'layouts_id'		,'width':200 	,'search_ctl':'ctlDropdown'	,'search_ctl_params': [ 'list':_model.Layouts(), 'option_fields':['id','name'] ]]  
				,['title':'Is Display Ad'		,'name':'is_display_ad'		,'width':100		] 
				,['title':'Is Pushup Product'	,'name':'is_pushup_product'	,'width':100		] 
				,['title':'Is Picture Rqd'		,'name':'is_picture_required','width':100 	,'search_ctl':'ctlNumeric'	]  
				,['title':'Is Active'			,'name':'is_active'			,'width':100		] 
				
			] ]) }}
		
		{% for id in ids %}
			{% set entity = _model.getOne('Products',id) %}
			{% set _context['current_entity_id'] = entity.id %}
			
			
			{% if _user.isAllowed('frmProductsList_actEdit') %}
				{% set _context['ajax_edit'] = true %}
			{% else %}
				{% set _context['ajax_edit'] = false %}
			{% endif%}
			
            <tr>
				<td class="text-right">{{ partial("partials/btnEditDelete", ['value':entity.id , 'controller':'products', 'model': 'Products'] ) }}</td>
                <td>{{ entity.id }}</td>
                <td>{#{ entity.name }#}
					{{ partial("partials/ctlText",[
						'value':entity.name, 
						'field':'name',
						'_context': _context
					] )}} 
 
				</td>
				<td>
					{{ partial("partials/ctlText",[
						'value':entity.avus_adlayout_id, 
						'field':'avus_adlayout_id',
						'_context': _context
					] )}} 
 
				</td>
				
				<td style="text-align:center;">
					{{ partial("partials/ctlNumeric",[
						'value':entity.sort_order, 
						'field':'sort_order', 
						'decimals':0, 
						'_context': _context
					] ) }}	
				</td>
				
                <td style="text-align:right;">
					{#{ number_format(entity.unit_price, 2, ',', '.') }#}
					{{ partial("partials/ctlNumeric",[
						'value':entity.unit_price, 
						'field':'unit_price', 
						'decimals':2, 
						'_context': _context
					] ) }}				
				
				</td>

				<td style="text-align:center;">
					{#{ _model.getOne('Publications', entity.publication_id).name }} - {{entity.publication_id}#}
					{{ partial("partials/ctlDropdown",[
						'value':entity.publication_id, 
						'list':_model.Publications(), 
						'field':'publication_id', 
						'_context':_context
					] ) }}	

				</td>

				<td style="text-align:center;">
					{#{ _model.getOne('Categories', entity.category_id).path }} - {{entity.category_id}#}
					
					{#{ partial("partials/ctlDropdown",[
						'value':entity.category_id, 
						'list':_model.Categories(), 
						'field':'category_id', 
						'option_fields':['n_path'],
						'_context':_context
					] ) }#}	

					{% set _category = entity.Category() %}
					{{ partial("partials/ctlTextTypeV3",[
						'field' : 'category_id'
						, 'value' : entity.category_id
						, '_context' : _context 
						, 'value_show' :  ( _category ? _category.id ~ ' - ' ~ _category.n_path : '')
						, 'model_name' : 'Categories' 
						, 'expr_search' : 'n_path'
						, 'expr_value' : 'id' 
						, 'expr_show_input' : 'n_path'
						, 'expr_show_list' : 'n_path'
						, 'additional_where' : ' and active = 1 and transaction_type_id is not null order by n_path '
						, 'nr_returned_rows' : 100
					]) }}
					 
				</td>
				<td>{#{ entity.id_resursa_nav }#}
					{{ partial("partials/ctlText",[
						'value':entity.id_resursa_nav, 
						'field':'id_resursa_nav',
						'_context': _context
					] )}} 
				</td>
				<td>{{ entity.old_product_id }}</td>
				<td>{{ entity.old_product_option }}</td>
				<td>{{ entity.old_product_extras }}</td>
				<td>{{ entity.old_product_extras_options }}</td>
				<td style="text-align:center;">
					{{ partial("partials/ctlNumeric",[
						'value':entity.priority_online, 
						'field':'priority_online', 
						'_context': _context
					] ) }}	
				</td>
				
				{#<td>{{ entity.priority_online }}</td>#}
				
				
				<td style="text-align:center;"><input disabled="disabled" type="checkbox" {% if entity.is_online_product == 1  %} checked{% endif %} maxlength="2" /></td>
				<td style="text-align:center;">
					{{ partial("partials/ctlNumeric",[
						'value':entity.days_published, 
						'field':'days_published', 
						'decimals':0, 
						'_context': _context
					] ) }}	
					{#{ entity.days_published }#}
				</td>
				<td >
					
					{{ partial("partials/ctlDropdown",[
						'value':entity.layouts_id, 
						'list':_model.Layouts(), 
						'field':'layouts_id', 
						'option_fields':['id','name'],
						'_context':_context
					] ) }}	
					 
				</td>
				
				<td style="text-align:center;">
					{{ partial("partials/ctlCheckbox", [
						'value' : entity.is_display_ad, 
						'field' : 'is_display_ad',
						'_context' : _context
					] ) }} 
					{#<input disabled="disabled" type="checkbox" {% if entity.is_display_ad == 1  %} checked {% endif %} maxlength="2" />#}
				</td>
				<td style="text-align:center;">
					{{ partial("partials/ctlCheckbox", [
						'value' : entity.is_pushup_product, 
						'field' : 'is_pushup_product',
						'_context' : _context
					] ) }} 
				
					{#<input disabled="disabled" type="checkbox" {% if entity.is_pushup_product == 1  %} checked {% endif %} maxlength="2" />#}
				</td>
				<td style="text-align:center;">
					{{ partial("partials/ctlCheckbox", [
						'value' : entity.is_picture_required, 
						'field' : 'is_picture_required',
						'_context' : _context
					] ) }} 
					{#<input disabled="disabled" type="checkbox" {% if entity.is_picture_required == 1  %} checked {% endif %} maxlength="2" />#}
				</td>
				<td style="text-align:center;">
					{{ partial("partials/ctlCheckbox", [
						'value' : entity.is_active, 
						'field' : 'is_active',
						'_context' : _context
					] ) }} 
					
					{#<input disabled="disabled" type="checkbox" {% if entity.is_active == 1  %} checked {% endif %} maxlength="2" />#}
				</td>
                
            </tr>
            {% else %}
            <tr><td colspan="6">Currently, there are no products here!</td></tr>
            {% endfor %}
        </table>
    </div>
</div>
