
<div class="row">
    <div class="col-lg-12 col-md-12">
        <div>
			
            <h1 class="page-header">Categories for Publication</h1>
        </div>
        <br>
		{{ partial("partials/btnAddNew", ['controller':'publications-categories'] ) }}
        {{ partial("partials/blkFlashSession") }}
        <table class="table table-striped table-hover table-condensed">

			
			{{ partial("partials/blkTableSort",['fields':[ 
				 ['title':'ID'				,'name':'id'				,'width':60		,'search_ctl':'ctlText'		] 
				,['title':'Category'		,'name':'name'				,'width':400	,'search_ctl':'ctlText'		] 
				,['title':'Is Active'		,'name':'is_active'			,'width':50		] 
				,['title':'Options'										,'width':200	] 
			] ]) }}
			
			{% for id in ids %}
				{% set entity = _model.getOne('PublicationsCategories',id) %}
				<tr>
					<td>{{ entity.id }}</td>
					<td>{{ entity.Category().n_path }}</td>
					<td>{{ partial("partials/ctlCheckbox",['value':entity.is_active, 'readonly':1 ] ) }}</td>
					<td class="text-right">{{ partial("partials/btnEditDelete", ['value':entity.id , 'controller':'publications-categories'] ) }}</td>
				</tr>
            {% else %}
				<tr><td colspan="6">Currently, there are no Categories for publication here!</td></tr>
            {% endfor %}
        </table>

    </div>
	
	{#
	<div class = "col-lg-12 col-md-12">
		<!-- POSTAVKE KATEGORIJA -->
	
		<input type="hidden" name="id" value="{{entity.id}}"></input>
		<div class="row">
			<div class="col-lg-12 col-md-12">
				<div class="panel panel-default">
					<div class="panel-heading">
						{% if entity.id is defined %}
							<div class="pull-right">[ID: <b>{{ entity.id }}</b> ]</div>
						{% endif %}
						<h3 class="panel-title"><b>Publications Categories settings</b></h3>
					</div>
					<div class="panel-body">
						  <!-- lista kategorija mora biti sortirana po n_path zato ide obratna logika  -->
						{% set catlist = _model.Categories([' level > 1  and transaction_type_id is not null ','order':'n_path']) %}
						{%  for cat in catlist %}
							{% if entity.id is defined %}
								{% set pubcat = _model.PublicationCategory("publication_id = " ~ entity.id ~ " and category_id = " ~ cat.id ) %}
								
								
								 <!-- postavke za brzo editiranje postavit ručno jer nije klasična  lista  -->
								{% set _context['current_entity_table_name'] = 'n_publications_categories' %}
								{% set _context['current_entity_id'] = pubcat.id %}
								{% set _context['ajax_edit'] = true %}

								
								<div class="row">
									<!-- CATEGORY_ID -->
									{{ partial("partials/ctlDropdown",[
										'value':pubcat.layouts_id, 
										'list':_model.Layouts(), 
										'field':'layouts_id', 
										'width':1, 
										'_context':_context
									] ) }}
									<div class="col-lg-4 col-md-4" >
										{{ pubcat.id }} {{ pubcat.Category().n_path }} {{ pubcat.category_id }}
									</div>
									
									<!-- IS_ACTIVE -->
									{{ partial("partials/ctlCheckbox",[
										'value': pubcat.is_active, 
										'width':1, 
										'field':'is_active',
										'_context':_context
									] ) }}							
									
									{{ partial("partials/ctlDropdown",[
										'value': pubcat.dtp_groups_id,
										'list': _model.DtpGroups(), 
										'field':'dtp_groups_id', 
										'width': 1, 
										'option_fields':['name'],
										'_context': _context
									] ) }}	
									
									{{ partial("partials/ctlDropdown",[
										'value': pubcat.forward_category_id,
										'list': catlist, 
										'field':'forward_category_id', 
										'width': 5, 
										'option_fields': ['n_path','id'],
										'_context': _context
									] ) }}										
								</div>	
							{% endif %}	
						{% endfor %}

					</div>

				</div>
			</div>
		</div>
	</div>
	#}
</div>
