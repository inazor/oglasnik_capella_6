<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous"/>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap-theme.min.css" integrity="sha384-fLW2N01lMqjakBkx3l/M9EahuwpSfeNvV63J5ezn3uZzapT0u7EYsXMjQV+0En5r" crossorigin="anonymous"/>

<!-- Latest compiled and minified JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"/>


<h1> DTP MISSING PICTURES REPORT</h1>

{% set issue = _model.Issue(_context['xml_report_issue_id']) %}
<h4>Issue: {{ issue.id }}</h4>
<h4>Date: {{ issue.date_published }}</h4>


<table>
	<tr>
		<th width=70 >ID</th>
		<th width=40 >CAT .ID</th>
		<th width=40 >FWD CAT. ID</th>
		<th width=400 >Description</th>
	</tr>
	
	{% for insertion in issue.Insertions() %}
		{% set order_item = insertion.OrderItem() %}
		{% if order_item %}
			{% set ad = order_item.Ad() %}
			{% set product = order_item.Product() %}
			
			{% if ad %}
				{% if product %}
					{% set _needs_photo = false %}
					{% if ad.is_display_ad %}{% set _needs_photo = true %}{% endif %}
					{% if product.is_picture_required %}{% set _needs_photo = true %}{% endif %}
					
					{% if _needs_photo %}
						{% set is_processed = ad.has_attached_picture('repository/dtp/processed') %}
						
						{% if not is_processed %}
							<tr style="border:1px solid black;">
								<td>{{ ad.id }}</td>
								<td>{{ ad.category_id }}</td>
								<td>{{ forward_id }}</td>
								<td>{{ ad.description }}</td>
							</tr>
						{% endif %}
					{% endif %}
				{% else %}
					<tr style="border:1px solid black;">
						<td colspan = "4">ERROR: Order Item No {{ order_item.id }} does not have Product</td>
					</tr>
				{% endif %}
			{% else %}
				<tr style="border:1px solid black;">
					<td colspan = "4">ERROR: Order Item No {{ order_item.id }} does not have Ad</td>
				</tr>
			{% endif %}
		{% else %}
			<tr style="border:1px solid black;">
				<td colspan = "4">ERROR: Insertion No {{ insertion.id }} does not have order item</td>
			</tr>
		{% endif %}
	{% endfor %}
</table>