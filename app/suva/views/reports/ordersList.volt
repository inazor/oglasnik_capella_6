
<div class="page" style="font:12px arial, sans-serif;  margin:0;" >

{#CONTEXT<pre>{{ print_r( _context, true ) }}</pre> #}

<!-- OVDE REPORT -->

	{% set _payment_type = null %}
	{% set _order_status = null %}
	{% set _from_date = null %}
	{% set _to_date = null %}
{#{var_dump(_context)}#}
	{% if _model.n_validate(_context['orders_list_payment_type'], 'id')  %}
		{% set _payment_type = _model.PaymentType(_context['orders_list_payment_type']) %}
	{% endif %}
	
	{% if _model.n_validate(_context['orders_list_status'], 'id')  %}
		{% set _order_status = _model.OrderStatus(_context['orders_list_status']) %}
	{% endif %}
	
	{% if _model.n_validate(_context['orders_list_from_date'], 'date')  %}
		{% set _from_date = _model.n_interpret_value_from_request(_context['orders_list_from_date'], 'date') %}
	{% endif %}
	
	{% if _model.n_validate(_context['orders_list_to_date'], 'date')  %}
		{% set _to_date = _model.n_interpret_value_from_request(_context['orders_list_to_date'], 'date') %}
	{% endif %}
	
	{% if strlen(_context['orders_list_nav_sync_status']) > 1  %}
		{% set _nav_sync_status = _context['orders_list_nav_sync_status'] %}
	{% endif %}

	{# parametri za upit #}
	{% set txtOrder = " 1 = 1 " %}
	{% if _order_status %}{%  set txtOrder = txtOrder ~ ' and n_status = ' ~ _order_status.id %}{%  endif %}
	{#% if _payment_type %}{%  set txtOrder = txtOrder ~ ' and user_id in ( select \Baseapp\Suva\Models\Users.id from \Baseapp\Suva\Models\Users where n_payment_type_id = ' ~ _payment_type.id  ~ ' ) ' %}{%  endif %#}
	{% if _payment_type %}
			
		{%  set txtOrder = txtOrder ~ ' and user_id in 
			( 
			select \Baseapp\Suva\Models\Users.id 
			from \Baseapp\Suva\Models\Users 
			where \Baseapp\Suva\Models\Users.n_payment_type_id = ' ~ _payment_type.id  ~ ' ) ' 
		%}
	{%  endif %}
	{% if _from_date %}{%  set txtOrder = txtOrder ~ " and created_at >= '" ~ _from_date ~ "'" %}{%  endif %}
	{% if _to_date %}{%  set txtOrder = txtOrder ~ " and created_at <= '" ~ _to_date ~ "'" %}{%  endif %}
	{% if _nav_sync_status %}{%  set txtOrder = txtOrder ~ " and n_nav_sync_status = '" ~ _nav_sync_status ~ "'" %}{%  endif %}
	
	
	{{ txtOrder }}
	{%  set orders = _model.Orders( txtOrder ) %}
	count:{{ orders.count() }}
		<div style="text-align:center;" class="row">
			<h2>
				Pregled financijskih dokumenata 
				{% if _order_status %}
					u statusu {{ _model.OrderStatus( _order_status ) }}
				{% endif %}
			</h2>		
		</div>
		<br/>
		<br/>
		<br/>
	<table style="width:100%;">
		<tr>
			<th style="width:30%; font-weight: bold; text-align:right;"></th>
			<th style="width:30%; font-weight: normal; "></th>
			<th style="width:40%; "></th>
		</tr>

		{% if _from_date %}
			<tr>
				<td>From:</td>
				<td>{{ date("d.m.Y", strtotime(_from_date)) }}</td>
			</tr>
		{% endif %}
		{% if _to_date %}
			<tr>
				<td>To:</td>
				<td>{{ date("d.m.Y", strtotime(_to_date)) }}</td>
			</tr>
		{% endif %}
		
		{% if _order_status %}
			<tr>
				<td>Status:</td>
				<td>{{ _order_status.name }}</td>
			</tr>
		{% endif %}

		{% if _payment_type %}
			<tr>
				<td>Customer Payment Type:</td>
				<td>{{ _payment_type.name }}</td>
			</tr>
		{% endif %}

		{% if _nav_sync_status %}
			<tr>
				<td>Navision sync:</td>
				<td>{{ _nav_sync_status }}</td>
			</tr>
		{% endif %}
	</table>
	
	<br/>
	<hr/>
	 
	<table style="width:100%;">
			<tr style="border:1px solid black; background-color:#F2F1F1; text-align:right;">
				<th style="width:20%" >Status</th>
				
				<th style="width:10%" >ID</th>
				<th style="width:15%">Date</th>
				<th style="width:15%">SalesRep</th>
				<th style="width:10%">Customer ID</th>
				<th style="width:30%">Customer Name</th>
				<th style="width:15%">Payment</th>
				<th style="width:20%; text-align:right; margin-left:10px;">Amount</th> 
			</tr>

			{% set _suma = 0 %}
			{%  for order in orders %}
				{%  set _customer = order.User() %}
				{%  set _sales_rep_username = (order.n_sales_rep_id > 0 ? order.SalesRep().username : 'n/a') %}
				{%  set _suma = _suma + order.n_total_with_tax %}
			{#% if _customer.PaymentType().name != "Advance" %#}
			<tr>
				<th>
					{{order.OrderStatus().name}}
				</th>
				<th>
					{{order.id}}
				</th>
				<th>
					{{  date("d.m.Y", strtotime(order.created_at)) }}
				</th>
				<th>
					{{ _sales_rep_username }}
				</th>
				<th>
					{{ _customer.id }}
				</th>
				<th>
					{{ _customer.username }}
				</th>
				<th>
					{{ _customer.PaymentType().name }}
				</th>
				<th style = "text-align:right; margin-left:10px;">
					{{ number_format( order.n_total_with_tax, 2, ',', '.') }}
				</th>
			</tr>
			{#% endif %#}
			{% endFor %}
			<tr>	
				<td colspan = "7"><B>UKUPNO ZA IZVJEŠTAJ</B></td>
				<td style="text-align:right; margin-left:10px;"><B>{{ number_format( _suma, 2, ',', '.') }}</B></td>
			</tr>
	</table>
	

</div>

<div class="row" style="position:absolute; bottom:0; height:40px; margin-top:40px; font:8.5px arial, sans-serif; width:85%">
	<hr/>
			Poduzeće je registrirano u Trgovačkom sudu u Zagrebu, Reg.broj 080109869; Temeljni kapital: 2.320.000&nbsp;HRK u potpunosti uplaćen; 
			Članovi uprave: Željko Hudoletnjak 
	<br/>
			Poslovna banka: RBA 11; HRK Žiro račun: 2484008-1100153885; Devizni žiro račun: 2484008-1100153885; IBAN: HR1624840081100153885; SWIFT: RZBHHR2X
</div>
