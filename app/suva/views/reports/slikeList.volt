<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous"/>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap-theme.min.css" integrity="sha384-fLW2N01lMqjakBkx3l/M9EahuwpSfeNvV63J5ezn3uZzapT0u7EYsXMjQV+0En5r" crossorigin="anonymous"/>

<!-- Latest compiled and minified JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"/>



<div class="page" style="font:12px arial, sans-serif;  margin:0;" >

{#CONTEXT<pre>{{ print_r( _context, true ) }}</pre> #}

<h1> DisplayAd List Report</h1>

{% set issue = _model.Issue(_context['xml_report_issue_id']) %}
<h4>Issue: {{ issue.id }}</h4>
<h4>Date: {{ issue.date_published }}</h4>


	Publications;Issue;Layouts Name;ID;Ad Taker;File Name;File Format;Image Width(px);Height(px);Color Mode;
	<br/>
	
{% for insertion in issue.Insertions() %}

		{% set order_item = insertion.OrderItem() %}
		{% if order_item %}
			{% set ad_taker = order_item.AdTaker() %}
			{% set ad = order_item.Ad() %}
			{% set product = order_item.Product() %}
			{% set publication = product.Publication() %}
			{% set category = ad.Category() %}
			{% set layout = product.Layout()%}
			{% set order = _model.Order(order_item.order_id)%}
			{% set sales_rep = _model.User(order.n_sales_rep_id)%}
			
			{% if ad.is_picture_required %}
				
					{{ publication.name }};
					{{ issue.id }};
					{{ category.n_path }};
					{{ layout.name }};
					{{ ad.id }};
					{{ sales_rep.username }};
					{{ ad.user_id }}.{{ ad.id }};
					{{ product.display_ad_width }};
					{{ product.display_ad_height }};
					{{ ad.AdsMedia().Media().mimetype}}
			{% endif %}
		{% endif %}	
		<br/>
{% endfor %}





	


</div>
