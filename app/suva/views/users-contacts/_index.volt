{% if _window_id is not defined %}{% set _window_id = '1' %}{% endif %}
{% if _is_ajax_submit is not defined %}{% set _is_ajax_submit = false %}{% endif %}
{% set _subform_id = 0 %}
{% set _url = '/suva/users-contacts/index/' ~ user_id %}


<!-- Contact Info -->
<div class="panel panel-default" style="overflow:scroll; height:160px; white-space:nowrap;">
	<div class="panel-heading" style="display:block; white-space: nowrap; width:100%;">
		<h3 class="panel-title">Contact Info</b>
		{% if _is_ajax_submit %}
			<span 
				class="fa fa-fw fa-plus"
				onclick = " ajaxSubmit( '/suva/users-contacts/crud/{{ user_id }}' , '{{ _window_id }}', null, '{{ _url }}' ); "
			/>
		{% endif %}
		</h3>
		
	</div>
	<div class="panel-body">
		<div class="tab-content">
			<div class="row">
				<div class="col-md-12 col-lg-12">
					{% for id in ids %}
						{% set contact = _model.UserContact(id) %} 
						<div style="display:block; margin-top:3px; width:100% !important; font-size:12px;">
							<div 
								id="contact_value_{{contact.id}}" 
								style="float:left !important; width:75% !important; border:none !important;" 
								onmouseout = "$(this).prop('readonly', true);
											 $(this).css('background-color','white');
											 "
								onmouseover = "$(this).css('background-color','lightgreen');"
								onclick="	var startVrsta = '';//20.12.2016 - prije ide zarez space
											var endVrsta = '';
											var gradele = '';//20.12.2016 - gradele u issues
											var descOffline = $('#parameter_description_offline').val();
											var vrsta = $(this).attr('vrsta');
											var vrijednost = $(this).attr('vrijednost');
											
											if ( vrsta == 'phone'){ 
												startVrsta = ', <Tel>';
												endVrsta = '</Tel>';
											} else if ( vrsta == 'email'){ 
												startVrsta = ' <Email>';
												endVrsta = '</Email>';
											}
											var indexof = descOffline.indexOf('###');
											//alert ('indexof ' + indexof );
											if ( indexof >= 0 ){
												gradele = '';
											}
											
											var novaVrijednost = descOffline + gradele + ' ' + startVrsta + vrijednost + endVrsta;
											
											//alert ( ' vrsta ' + vrsta + ' gradele ' + gradele + ' startVrsta ' + startVrsta + ' endVrsta ' + endVrsta );
											
											var chars = $('.maxlength').text().split(' ');
											var chars_left = parseInt(chars[0]) -	parseInt( $(this).attr('vrijednost').length );
											if(chars_left > 0){
												$('.maxlength').text(chars_left + ' characters left'); 
												
												$('#parameter_description_offline').val( novaVrijednost );
											}
											else if(chars_left < 0){
												alert('characters exceeded');
												return false;
											}
											" 
								vrijednost = "{{contact.name}}"
								vrsta = "{{contact.type}}"
								>
								{{contact.name}}
							</div>
							<div 
								class="fa fa-fw fa-pencil-square-o"
								style="float:right !important; margin-top:3px;"
								onclick="ajaxSubmit ( '/suva/users-contacts/crud/0/{{contact.id}}', '{{ _window_id }}', null, '{{ _url }}' );"
								>
							</div>
							<div 
								class="fa fa-fw fa-times" 
								style="float:right !important; margin-left:0px; margin-right:3px;"
								onclick="
									if (confirm('Are you sure to delete contact ' + $('#contact_value_{{contact.id}}').attr('vrijednost') + '?')) {
										ajaxSubmit ( '/suva/users-contacts/index/{{ user_id }}/delete/{{contact.id}}', '{{ _window_id }}' );
									 }
									" 
								style="height:17px;"
								>
							</div>
							<div class="fa fa-phone"
								 aria-hidden="true"
								 onclick="$('#phone1').val($('#contact_value_{{contact.id}}').attr('vrijednost'))"
								 >
							</div>
							<div class="fa fa-phone"
								 aria-hidden="true"
								 onclick="$('#phone2').val($('#contact_value_{{contact.id}}').attr('vrijednost'))"
								 >
							</div>
						</div>
					{% endfor %}
				</div>	
			</div>
		</div>
	</div>
</div>
