/**
 * This is basic helper items sorter/reposition file.
 *
 * With every change of the order within the list, this helper writes current order of the items. It checks to see if
 * there are any changes withing the list and if so it can even show/hide specific $element (Save changes button, etc.).
 *
 * If you want to get currently sorted list, you need the 'reorder_final_ids' variable. You can easily convert it to
 * JSON with 'JSON.stringify(reorder_final_ids)' and use it with POST or AJAX...
 *
 * If you would like this helper to handle $element automatic showing/hiding then, you should include your page specific
 * javascript file AFTER this helper and in onDocumentReady set the 'reorder_controllable_button' variable with
 * selector of your element that you want to control.
 */

var reorder_initial_ids = [];
var reorder_final_ids = [];
var reorder_initial_serialization = '';
var reorder_last_serialization = '';
var reorder_has_changes = false;
var reorder_controllable_button = '';

function reorder_check_changes() {
    if (reorder_last_serialization != reorder_initial_serialization) {
        reorder_recalculate_positions();
        reorder_has_changes = true;
    } else {
        reorder_final_ids = reorder_initial_ids;
        reorder_has_changes = false;
    }
    reorder_controllable_button_handle();
}

function reorder_controllable_button_handle() {
    if ($.trim(reorder_controllable_button)) {
        $button = $(reorder_controllable_button);
        if ($button.length) {
            if (reorder_has_changes && !$button.is(':visible')) {
                $button.fadeIn('fast');
            } else if (!reorder_has_changes && $button.is(':visible')) {
                $button.fadeOut('fast');
            }
        }
    }
}

function reorder_recalculate_positions() {
    reorder_final_ids = [];
    reorder_final_ids = $('.sortable').sortable("toArray", {'attribute': 'data-id'});
}

$(document).ready(function(){
    $('.sortable').sortable({
        forcePlaceholderSize: true,
        handle: '.drag-me',
        helper: 'clone',
        listType: 'ul',
        items: 'li',
        opacity: .6,
        placeholder: 'placeholder',
        revert: 250,
        tabSize: 25,
        create: function(event, ui) {
            reorder_recalculate_positions();
            reorder_initial_serialization = $('.sortable').sortable('serialize');
            reorder_initial_ids = reorder_final_ids;
        },
        stop: function(event, ui) {
            reorder_last_serialization = $('.sortable').sortable('serialize');
            reorder_check_changes();
        }
    });
});
