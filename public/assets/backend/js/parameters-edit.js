function load_dictionary_values(dictionary_id) {
    if ($.trim(dictionary_id) != '' && parseInt(dictionary_id) != 0) {
        $.get(
            '/ajax/dictionary/' + dictionary_id + '/values',
            function(data) {
                $('#current_dictionary_values').find('li').remove();
                if (data.length) {
                    $.each(data, function(idx, data){
                        $('#current_dictionary_values').append(
                            $('<li/>').text(data.name)
                        );
                    });
                } else {
                    $('#current_dictionary_values').append(
                        $('<li/>').addClass('text-danger').text('No values found in this dictionary...')
                    );
                }
            },
            'json'
        );
    } else {
        $('#current_dictionary_values').find('li').remove();
    }
}

$(document).ready(function(){
    $('#type_id').selectpicker({
        style: 'btn btn-default',
        title: 'Select parameter type',
        mobile: OGL.is_mobile_browser
    });

    $('#dictionary_id').selectpicker({
        style: 'btn btn-default',
        size: 8,
        liveSearch: true,
        noneResultsText: 'No dictionaries match',
        mobile: OGL.is_mobile_browser
    });

    // handle change of type_id
    $('#type_id').change(function(){
        var $current_option = $('#type_id option[value="' + $(this).val() + '"]');
        if (parseInt($("#type_id option:selected").data('accept-dictionary')) == 0) {
            $('#dictionary_container').addClass('hidden');
            $('#dictionary_values_container').addClass('hidden');
        } else {
            var $visible_options = null;
            // see if we're working with a dependable type of parameter
            if (parseInt($current_option.data('is-dependable'))) {
                // hide all
                $('#dictionary_id option').hide();
                // get all items that shoud be visible
                $visible_options = $('#dictionary_id option').filter(function() {
                    return $(this).data('max-level') >= 2;
                });
            } else {
                // hide all
                $('#dictionary_id option').hide();
                // get all items that shoud be visible
                $visible_options = $('#dictionary_id option').filter(function() {
                    return $(this).data('max-level') < 2;
                });
            }
            // show all items that should be visible
            $visible_options.show();
            // refresh dictionary dropdown
            $('#dictionary_id').selectpicker('refresh');
            $('#dictionary_id').selectpicker('val', $($visible_options[0]).val());
            $('#dictionary_container').removeClass('hidden');
            $('#dictionary_values_container').removeClass('hidden');
        }
    });

    // handle change of dictionary_id
    $('#dictionary_id').change(function(){
        load_dictionary_values($(this).val());
    });
});
