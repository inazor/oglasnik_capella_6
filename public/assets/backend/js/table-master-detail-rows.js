$(document).ready(function() {
    // Clickable master/detail table rows and other goodies
    var $table = $('.master-detail');
    if ($table.length > 0) {

        var hide_all = function () {
            $table.find('tr.detail-row').hide();
        };
        var show_all = function () {
            $table.find('tr.detail-row').show();
        };
        var toggle_all = function () {
            $table.find('tr.master-row').trigger('click');
        };

        hide_all();

        $table.find('tr.master-row').on('click', function () {
            var id = $(this).attr('id');
            $table.find('tr[data-master="' + id + '"]').toggle();
        });

        // TODO: we could store the last triggered button in a cookie and remember/restore
        // the state for sub-sequent page loads...
        var buttons = {
            // selector : function to bind on click
            '#hide-all': hide_all,
            '#show-all': show_all,
            '#toggle-all': toggle_all
        };
        for (var b in buttons) {
            if (buttons.hasOwnProperty(b)) {
                var $b = $(b);
                var func = buttons[b];
                if ($b.length > 0) {
                    (function (trigger) {
                        $b.on('click', function (e) {
                            e.preventDefault();
                            trigger();
                        });
                    })(func);
                }
            }
        }

    }
});
