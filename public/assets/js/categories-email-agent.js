function email_agent_valid(callback) {
    var email_agent_name = $.trim($('#email_agent_name').val());

    if (email_agent_name) {
        $('#email_agent_name').closest('.form-group').removeClass('has-error');
        $('#email_agent_name').closest('.form-group').find('.help-block').html('');
        if ('function' == typeof callback) {
            callback();
        }
        return true;
        /*
        $.post(
            '/ajax/get-user-email-agent', {
                'name'    : email_agent_name
            },
            function (json) {
                if (json.status) {
                    $('#email_agent_name').closest('.form-group').removeClass('has-error');
                    $('#email_agent_name').closest('.form-group').find('.help-block').html('');
                    if ('function' == typeof callback) {
                        callback();
                    }
                    return true;
                } else {
                    if ('undefined' !== typeof json.msg) {
                        if ('undefined' !== typeof json.data && 'undefined' !== typeof json.data.logged_in) {
                            if (!json.data.logged_in) {
                                alert("Ooops!\nMolimo vas da se prijavite u vaš račun kako bi mogli spremiti ovaj oglas u svoj račun...");
                            }
                        } else {
                            $('#email_agent_name').closest('.form-group').addClass('has-error');
                            $('#email_agent_name').closest('.form-group').find('.help-block').html(json.msg);
                        }
                    }
                    return false;
                }
            },
            'json'
        );
        */
    } else {
        $('#email_agent_name').closest('.form-group').addClass('has-error');
        $('#email_agent_name').closest('.form-group').find('.help-block').html('Unesite naziv Email agenta');
        return false;
    }
}

function save_email_agent() {
    var email_agent_settings = get_email_agent_settings();
    if (email_agent_settings) {
        email_agent_valid(function(){
            var email_agent_name = $.trim($('#email_agent_name').val());
            $.post(
                '/ajax/saveUserEmailAgent', {
                    'name'    : email_agent_name,
                    'settings': email_agent_settings
                },
                function (json) {
                    if (json.status) {
                        $('#emailAgentModal').modal('hide');
                        $('#btnSaveAsEmailAgent').fadeOut('fast', function(){
                            $('#email_agent_name').val('');
                        });
                    } else {
                        if ('undefined' !== typeof json.msg) {
                            if ('undefined' !== typeof json.data.logged_in) {
                                if (!json.data.logged_in) {
                                    alert("Ooops!\nMolimo vas da se prijavite u vaš račun kako bi mogli spremiti ovaj oglas u svoj račun...");
                                }
                            } else {
                                alert("Ooops!\n" + json.msg);
                            }
                        }
                    }
                },
                'json'
            );
        });
    }
}

function get_email_agent_settings() {
    var json_result = null;

    var $filter_parameters = $('#category_filters');
    var full_queryString = $filter_parameters.serialize();
    var cleaned_queryString = full_queryString.replace(/[^&]+=\.?(?:&|$)/g, '');

    // Removing default filter values in order to reduce url length
    var queryString_array = cleaned_queryString.split('&');

    var final_json = {
        category_id: parseInt($('#category_id').val())
    };
    var final_json_parameters = 0;
    for (var i = 0, il = queryString_array.length; i < il; i++) {
        var parts = queryString_array[i].split('=');
        var filter_name = parts[0];
        var filter_value = parts[1];

        if (filter_name !== 'sort') {
            var $f_parameters = $filter_parameters.find('[name="' + filter_name + '"]');
            if ($f_parameters.length > 0) {
                var filter_default = $f_parameters.data('default');
                if (('' !== filter_value) && (filter_value != filter_default)) {
                    if (!isNaN(filter_value)) {
                        final_json[filter_name] = parseInt(filter_value);
                    } else {
                        final_json[filter_name] = filter_value;
                    }
                    final_json_parameters++;
                }
            }
        }
    }

    if (final_json_parameters) {
        json_result = JSON.stringify(final_json);
    }

    return json_result;
}

jQuery(document).ready(function() {
    $('#btnSaveAsEmailAgent').click(function(){
        if (get_email_agent_settings()) {
            $('#emailAgentModal').modal({
                backdrop: 'static',
                keyboard: true
            });
        } else {
            alert('Morate odabrati parametre pretrage da bi spremili vašeg Email agenta');
        }
    });
    $('#emailAgentModalYesBtn').click(function(){
        save_email_agent();
    });
});
