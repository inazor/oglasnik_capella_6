function getFilteredBulkIDs(bulkAction, ids) {
    var rawIDs = $.trim(ids) ? ($.trim(ids).indexOf(',') > -1 ? ids.split(',') : [$.trim(ids)]) : [];
    var applicableIDs = [];
    var filteredIDs = [];

    $('.ad-box.selected .actions button[data-type="' + bulkAction + '"]').each(function(i, classified){
        applicableIDs.push(parseInt($(classified).closest('.ad-box.selected').data('classified-id')).toString());
    });

    filteredIDs = rawIDs.filter(function(classifiedID){
        return applicableIDs.indexOf(classifiedID) > -1 ? classifiedID : false;
    });

    return filteredIDs.length ? filteredIDs.join(',') : null;
}

$(document).ready(function(){
    $('.payment-details-btn').on('click', function(e){
        e.preventDefault();
        var $this = jQuery(this);
        var $target = $this.closest('.ad-box');
        var $details = $target.find('.classified-payment-details');
        var $icon_elem = $this.find('span.fa');
        $details.stop().slideToggle(300, function(){
            $icon_elem.toggleClass('fa-search-minus');
        });
    });

    $('#deleteModal').on('show.bs.modal', function (event) {
        var $sender = $(event.relatedTarget);
        var form_url = $sender.data('formurl');
        var $modal = $(this);
        $modal.find('#frm_ads_delete').attr('action', form_url);
    });

    $('#orderCancellationModal').on('show.bs.modal', function (event) {
        var $sender = $(event.relatedTarget);
        var form_url = $sender.data('formurl');
        var $modal = $(this);
        $modal.find('#frm_order_cancellation').attr('action', form_url);
    });

    $('.actions button[data-href]').click(function(e){
        e.preventDefault();
        window.location.href = $(this).data('href');
    });

    $('.ad-box-wide input.checkbox').change(function(){
        if ($(this).prop('checked')) {
            $(this).closest('.ad-box-wide').addClass('selected');
        } else {
            $(this).closest('.ad-box-wide').removeClass('selected');
        }

        var selected_classified_ids = [];
        $('.ad-box-wide.selected').each(function(i, elem){
            selected_classified_ids.push($(elem).data('classified-id'));
        });
        selected_classified_ids = selected_classified_ids.join(',');

        $('.bulk-actions button').prop('disabled', (selected_classified_ids == ''));
        $('.bulk-actions').attr('data-classified-ids', selected_classified_ids).data('classified-ids', selected_classified_ids);
    });

    if ($('.bulk-actions input.select-all').length) {
        $('.bulk-actions input.select-all').change(function(){
            $('.ad-box-wide input.checkbox').prop('checked', $(this).prop('checked')).trigger('change');
        });

        $('#bulkActionModal').on('show.bs.modal', function(event) {
            var $sender     = $(event.relatedTarget);
            var form_url    = $sender.data('formurl');
            var bulkAction  = $sender.data('bulk-action');
            var selectedIDs = getFilteredBulkIDs(bulkAction, $('.bulk-actions').data('classified-ids'));
            var $modal      = $(this);

            if (selectedIDs) {
                var warningMessage = 'Želite li nastaviti s izvršavanjem ove akcije na <b>svim odabranim oglasima</b>?<br/>Ova akcija je nepovratna!';

                switch(bulkAction) {
                    case 'sold':
                        warningMessage = 'Odabrali ste <span class="text-danger">označavanje više oglasa (svi trenutno označeni) kao <b>prodanima</b></span>. <br/><br/>Jeste li sigurni da to doista želite?';
                        break;

                    case 'activate':
                        warningMessage = 'Odabrali ste <span class="text-danger"><b>aktiviranje</b> više oglasa (svi trenutno označeni)</span>. <br/><br/>Jeste li sigurni da to doista želite?';
                        break;

                    case 'deactivate':
                        warningMessage = 'Odabrali ste <span class="text-danger"><b>deaktiviranje</b> više oglasa (svi trenutno označeni)</span>. <br/><br/>Jeste li sigurni da to doista želite?';
                        break;

                    case 'republish':
                        warningMessage = 'Odabrali ste <span class="text-danger"><b>ponavljanje</b> više oglasa (svi trenutno označeni)</span>. <br/>Jeste li sigurni da to doista želite?<br/><br/><small>Ponoviti će se samo oni oglasi koji imaju mogućnost besplatnog ponavljanja!</small>';
                        break;

                    case 'delete':
                        warningMessage = 'Odabrali ste <span class="text-danger"><b>brisanje</b> više oglasa (svi trenutno označeni)</span>. Ta akcija je nepovratna i trajno će izbrisati sve onačene oglase! <br/><br/>Jeste li sigurni da to doista želite?';
                        break;
                }
                $modal.find('.modal-body p').html(warningMessage);
                $modal.find('#frm_bulk_action').attr('action', form_url  + selectedIDs);
            } else {
                event.stopPropagation();
                $modal.data('bs.modal').isShown = true;
                setTimeout(function(){
                    $modal.data('bs.modal').isShown = false;
                }, 1);
            }
        });
    }

    // prevent browser from caching checked form elements...
    if ($('.ad-box-wide .checkbox-wrapper .checkbox:checked').length) {
        $('.ad-box-wide .checkbox-wrapper .checkbox:checked').prop('checked', false);
        $('.bulk-actions input.select-all').prop('checked', false);
    }

    $('#my_classifieds').filterizeForm();
});
