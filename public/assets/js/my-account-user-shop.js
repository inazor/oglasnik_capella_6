$(document).ready(function(){
    $('select').selectpicker({
        style: 'btn btn-default',
        noneSelectedText: '',
        liveSearch: false,
        noneResultsText: '',
        mobile: OGL.is_mobile_browser,
        dropupAuto: false
    });

    $('textarea#about').ckeditor({
        customConfig: '/assets/js/ckeditor.basic.config.js'
    });

    // Select the tab specified via anchor
    var hash = location.hash;
    if (hash) {
        $('a[href="' + hash + '"').tab('show');
    }

    $('#delete-shopping-window-confirm').on('show.bs.modal', function(event) {
        var $sender = $(event.relatedTarget);
        var form_url = $sender.data('formurl');
        $(this).find('#confirm-delete-link').attr('href', form_url);
    });

    $('.actions button[data-href], .actions button[data-formurl]').click(function(e){
        e.preventDefault();
        if ('undefined' !== typeof $(this).data('href')) {
            window.location.href = $(this).data('href');
        }
    });

    $('#media_fileupload').fileupload({
        url: '/ajax/upload',
        dataType: 'json',
        dropZone: null,
        pasteZone: null,
        limitMultiFileUploads: 1,
        acceptFileTypes: /(\.|\/)(gif|jpe?g|png)$/i,
        done: function (e, data) {
            if ('undefined' !== data.result) {
                if (data.result.ok) {
                    $('#media_id').val(data.result.media[0].id);
                    $('#media_preview').html(data.result.media[0].preview.tag);
                } else {
                    // TODO: handle failure in some way...
                }
            }
        }
    }).prop('disabled', !$.support.fileInput)
      .parent().addClass($.support.fileInput ? undefined : 'disabled');

});
