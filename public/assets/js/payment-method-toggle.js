jQuery(function(){
    var $container = jQuery('#payment-method-chooser');
    var $radios = $container.find('input[type=radio]');
    var $details = jQuery('.payment-method-details,.hub-3a-details');
    var $spans = $container.find('label span');

    $radios.on('change.oglasnik', function(){
        var $el = jQuery(this);
        var $holder = $el.parents('.radio');
        var $span = $holder.find('label span');

        $details.hide();
        $spans.removeClass('text-muted text-primary');

        if ($el.prop('checked')) {
            localStorage.setItem('local_payment_method', $el.val());

            // Highlight the current choice
            $span.addClass('text-primary');

            // Show matching details
            var $target = $el.data('target');
            if ($target) {
                var $target_el = jQuery($target);
                if ($target_el.length > 0) {
                    $target_el.show();
                }
            }
        }
    });

    // Bind click action on radio containers
    $container.find('.radio').on('click', function(){
        var $el = jQuery(this);
        var $el_radio = $el.find('input[type=radio]');
        var checked = $el.prop('checked');
        if (!checked) {
            $el_radio.prop('checked', true).attr('checked', 'checked').trigger('change.oglasnik');
        }
    });

    // Selecting a payment method if nothing is selected initially
    var $checked_radios = $radios.find(':checked');
    if (0 === $checked_radios.length) {
        var local = localStorage.getItem('local_payment_method');
        var $radio;
        if (null !== local) {
            // Try selecting what's stored earlier if we have something
            $radio = $radios.filter('[value="' + local + '"]');
        } else {
            // If localStorage has no data, check/select the first listed method
            $radio = $radios.first();
        }
        if ($radio.length > 0) {
            $radio.prop('checked', true).attr('checked', 'checked').trigger('change.oglasnik');
        }
    }
});
