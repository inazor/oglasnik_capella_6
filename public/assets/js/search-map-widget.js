/**
 * TODO:
 *     - see what we can do about mobile...
 */

(function($) {
    // searchMap
    $.widget('ogl.searchMap', {
        options: {
            debug: false,
            ajax: {
                triggerDelay: 500, // milliseconds
            },
            scrollToTopOfListingAfterAjax: false,
            scrollToMoreFiltersOnMobile: true,
            historyApi: false,
            defaultCurrency: 'EUR',
            showInfoWindowOnMarkerHover: true,
            instanceName: 'searchMap',
            moreFiltersBtnText: 'Više filtera &nbsp; <span class="fa fa-angle-down"></span>',
            moreFiltersBtnActiveText: 'Manje filtera &nbsp; <span class="fa fa-angle-up"></span>',
            defaultDelayedTriggerTime: 500, // ms
            defaultMobileView: 'filters',
            selectors : {
                listingSection: '.listing-section',
                mapSection: '.map-section',
                searchOnMapContent: '#searchOnMapContent',
                listingContainer: '#listingContainer',
                googleMap: '#googleMapContainer',
                categorySelect: '#parent_category_id',
                transactionTypeSelect: '#category_id',
                locationTextInput: '#location_text',
                locationBoundsInput: '#location_bounds',
                locationZoomInput: '#location_zoom',
                locationCenterInput: '#location_center',
                moreFiltersInput: '#more-filters-input',
                moreFiltersBtn: '.more-filters-btn',
                moreFiltersDiv: '#more-filters-div',
                categoryFiltersDiv: '#categoryFiltersDiv',
                submitBtn: '#submitBtn',
                ajaxLoading: '.ajaxLoading',
                mobile: {
                    footer: '#searchOnMapMobileFooter',
                    mapAutosuggest: '.map-section .autoSuggest',
                    listingAutosuggest: '.listing-section .location-text-filter',
                    switchViewButtons: '#searchOnMapMobileFooter .view-button',
                    submitBtn: '#mobileSubmitBtn'
                }
            },
            mapDetails: {
                spiderify: {
                    nearbyDistance: 40, 
                    keepSpiderfied: true, 
                    markersWontMove: true, 
                    markersWontHide: true
                },
                clustering: {
                    maxZoom: 16,
                    minimumClusterSize: 2,
                    averageCenter: true,
                    imagePath: '/assets/img/markers/m'
                },
                center: {
                    lat: 45.8, 
                    lng: 16
                    //lat: 44.5, 
                    //lng: 16.3
                },
                zoom: 10,
                minZoom: 10,
                optimalZoom: 17
            }
        },

        _console: function(method, data) {
            if (this.options.debug && 'undefined' !== typeof window.console && 'function' === typeof window.console[method]) {
                window.console[method](data);
            }
        },

        _create: function() {
            var _self = this;

            // use selectpicker for select controls if it's loaded
            if ($().selectpicker) {
                this.element.find('select').not(this.options.selectors.moreFiltersDiv + ' select').selectpicker({
                    style: 'btn btn-default',
                    mobile: is_mobile_browser(),
                    dropupAuto: false
                });
            }

            this.initialized = false;
            this.timers = {};
            this.$submitBtn = $(this.element).find(this.options.selectors.submitBtn);
            this.$searchOnMapContent = $(this.options.selectors.searchOnMapContent);
            this._setupMobileBehavior();
            this._initDependableTransactionTypes();
            this._initLocationBasedParametersChangeTrigger();
            this._initGoogleMap();
            this._initPriceFilters();

            // TODO:FIX:
            // load initial markers with dealy of 1s as in firefox we're getting projection undefined from the map
            var initialClassifiedsMarkers = this._getInitialClassifiedsMarkersFromListing();
            if (initialClassifiedsMarkers.length) {
                setTimeout(function(){
                    _self._addHoverInfoForFoundItems($(_self.options.selectors.listingContainer));
                    _self.renderClassifiedsMarkers(initialClassifiedsMarkers);
//                    _self._centerBasedOnMarkersBoundsCenter();
                }, 1000);
            }
    
            this.element.attr('data-' + this.options.instanceName, true).data(this.options.instanceName, true);
            this.initialized = true;

            if (this.options.ajax) {
                if (!this._isMobileScreen()) {
                    this._showSearchBtn(false);
                }
                $(this.element).submit(function(event){
                    event.preventDefault();
                });
                this._ajaxifyPagination();

                if (this.options.historyApi && 'undefined' !== typeof window.history) {
                    var initialMD5                = $(this.element).data('md5');
                    this.onpophistory             = {};
                    this.onpophistory[initialMD5] = $(this.options.selectors.listingContainer).html();
                    window.history.pushState({'md5': initialMD5, 'map':{'bounds':this._getMapBoundsOnInit().toJSON(), 'center':this.googleMap.getCenter().toJSON(), 'zoom':this.googleMap.getZoom()}}, window.document.title, window.location.href);
                    window.onpopstate = function(e){
                        if (e.state) {
                            if ('undefined' !== typeof e.state.md5 && 'undefined' !== typeof _self.onpophistory[e.state.md5]) {
                                google.maps.event.clearListeners(_self.googleMap, 'idle');
                                if ('undefined' !== typeof e.state.map) {
                                    if ('undefined' !== typeof e.state.map.bounds) {
                                        _self.googleMap.fitBounds(e.state.map.bounds);
                                    } else if ('undefined' !== typeof e.state.map.center) {
                                        _self.googleMap.panTo(e.state.map.center);
                                    }
                                    if ('undefined' !== typeof e.state.map.zoom) {
                                        _self.googleMap.setZoom(e.state.map.zoom);
                                    }
                                }
                                //_self.clearRenderedMarkers();
                                var $newContent = $(_self.onpophistory[e.state.md5]);
                                _self._addHoverInfoForFoundItems($newContent);
                                $(_self.options.selectors.listingContainer).html('');
                                $(_self.options.selectors.listingContainer).append($newContent.fadeIn('fast', function() {
                                    var initialClassifiedsMarkers = _self._getInitialClassifiedsMarkersFromListing();
                                    if (initialClassifiedsMarkers.length) {
                                        _self.renderClassifiedsMarkers(initialClassifiedsMarkers);
                                    }
                                    _self._ajaxifyPagination();
                                }));
                                setTimeout(function(){
                                    _self.googleMap.addListener('idle', function(){ _self._updateCurrentMapInfo('noAjax'); });
                                }, 1000);
                            }
                        }
                    };
                }
                this._setAjaxTriggerForAllOtherFields();
            }

            $(window).resize(function(){
                _self.resizeSearchWrapper();
            });

            return this;
        },

        _delayedTrigger: function(callback, timer, time) {
            if ('function' === typeof callback) {
                if ('undefined' === typeof timer) {
                    callback();
                } else {
                    if ('undefined' === typeof time) {
                        time = this.options.defaultDelayedTriggerTime;
                    }
                    // delay execution of callback() for [time] ms
                    if ('undefined' !== typeof this.timers[timer]) {
                        clearTimeout(this.timers[timer]);
                    }
                    this.timers[timer] = setTimeout(callback(), time);
                }
            } else {
                this._console('error', 'Callback is not a function!');
            }
        },

        _showSearchBtn: function(show) {
            if (false !== show && true !== show) {
                show = true;
            }

            if (show) {
                this.$submitBtn.show();
            } else {
                this.$submitBtn.hide();
            }
        },

        /**
         * TODO: Mobile mapSearch type switcher
         * 
         * Try to implement some kind of view switching on mobile devices (switch between filters and map view)
         * Also, we should change the way how bounds are used. Currently when the map is initialized, it sets
         * the bounds in the hidden inputs so we can use them in filtering. On mobile screens, we should track
         * what user is searching for and not event trigger the search if we don't know where are we actually 
         * searching...
         */
        _isMobileScreen: function() {
            return window.innerWidth < 768;
        },
        _setupMobileBehavior: function() {
            this.currentMobileView  = null;
            this.$searchOnMapMobileFooter = $(this.options.selectors.mobile.footer);
            this.$mobileViewButtons = $(this.options.selectors.mobile.switchViewButtons);
            this.$mobileSearchButton = $(this.options.selectors.mobile.submitBtn);
            if (this.$mobileSearchButton && 1 === this.$mobileSearchButton.length) {
                var _self = this;
                this.$mobileSearchButton.on('click.' + this.options.instanceName, function(){
                    var $locationInputParent = _self.$locationTextInput.parent();

                    if (_self._testIfMapBoundsAreSet()) {
                        $locationInputParent.removeClass('has-error').find('p.help-block').remove();
                        _self._console('info', 'mobile filters search ajax');
                        _self._mobileShowMapViewButton();
                        _self._triggerAjaxSearch(1, true);
                    } else {
                        _self._console('info', 'map bounds are not set');
                        $locationInputParent.addClass('has-error');
                        if (!$locationInputParent.find('p.help-block').length) {
                            $locationInputParent.append($('<p class="help-block text-small">Molimo upišite lokaciju koju pretražujete</p>'));
                        }
                        $(_self.element).find('.scroll-y.search-by-location-filter').animate({
                            scrollTop: 0
                        }, 500);
                    }
                });
            }
            if (this.$mobileViewButtons.length) {
                // see if we have a defaultMobileView in options
                $defaultMobileViewButton = null;
                if ('undefined' !== typeof this.options.defaultMobileView) {
                    $defaultMobileViewButton = this.$mobileViewButtons.filter('[data-view-type="' + this.options.defaultMobileView + '"]');
                    if (1 === $defaultMobileViewButton.length) {
                        this.currentMobileView = this.options.defaultMobileView;
                    }
                }

                var _self = this;
                this.$mobileViewButtons.on('click.' + _self.options.instanceName, function(){
                    if ('filters' === $(this).data('view-type')) {
                        _self._mobileShowFiltersViewButton();
                    } else {
                        _self._mobileShowMapViewButton();
                    }
                });

                if ($defaultMobileViewButton && 1 === $defaultMobileViewButton.length && this._isMobileScreen()) {
                    $defaultMobileViewButton.trigger('click.' + this.options.instanceName);
                }

                delete $defaultMobileViewButton;
            }
            if (this.currentMobileView) {
                $(this.element).addClass(this.currentMobileView + 'View');
            }
        },
        _mobileShowFiltersViewButton: function() {
            if ('undefined' !== typeof this.$locationTextInput) {
                if (this.$locationTextInput.closest('.map-section')) {
                    var $locationTextInput = this.$locationTextInput.detach();
                    $(this.options.selectors.mobile.listingAutosuggest).append($locationTextInput);
                }
            }
            $(this.element).removeClass('mapView').addClass('filtersView');
            this.currentMobileView = 'filters';
            this.$searchOnMapMobileFooter.addClass('filters').removeClass('map');
            $(this.element).find(this.options.selectors.mapSection).hide();
            $(this.element).find(this.options.selectors.listingSection).show();
        },
        _mobileShowMapViewButton: function() {
            if ('undefined' !== typeof this.$locationTextInput) {
                if (this.$locationTextInput.closest('.listing-section')) {
                    var $locationTextInput = this.$locationTextInput.detach();
                    $(this.options.selectors.mobile.mapAutosuggest).append($locationTextInput);
                }
            }
            $(this.element).removeClass('filtersView').addClass('mapView');
            this.googleMapFirstIdle = true;
            this.currentMobileView = 'map';
            this.$searchOnMapMobileFooter.addClass('map').removeClass('filters');
            $(this.element).find(this.options.selectors.listingSection).hide();
            $(this.element).find(this.options.selectors.mapSection).show();
            var _self = this;
            if (this.googleMap) {
                google.maps.event.trigger(this.googleMap, 'resize');
                this._setLastKnownBounds();

                //this._setMapZoomAndCenterBasedOnMarkersBounds();
            }
        },
        _showCurrentMobileView: function() {
            $defaultMobileViewButton = this.$mobileViewButtons.filter('[data-view-type="' + this.currentMobileView + '"]');
            if (1 === $defaultMobileViewButton.length) {
                $defaultMobileViewButton.trigger('click.' + this.options.instanceName);
            }
        },

        _initLocationBasedParametersChangeTrigger: function() {
            var locationBaseParameters = [];
            var _self = this;

            this.$moreFiltersDiv.find('select[data-gps-location="true"]').each(function(i, elem){
                $(elem).on('change.' + _self.options.instanceName, function(event){
                    // detect that human fired this event
                    if (event.originalEvent !== undefined) {
                        var $selectedOption = $(elem).find('option:selected');
                        var gpsBounds = 'undefined' !== typeof $selectedOption.data('gps-bounds') ? $selectedOption.data('gps-bounds') : null;
                        if (gpsBounds && $.trim(gpsBounds)) {
                            _self._updateMapBounds(gpsBounds);
                        }
                    }
                });
            });
        },

        _initPriceFilters: function() {
            var _self = this;

            var $priceFilter = $(this.element).find('.price-filter');
            var $currencySwitchItems = $priceFilter.find('.currency-switch .currency-item');

            var initialCurrency = $.trim($(this.element).find('#ad_price_currency').val());
            if (!initialCurrency) {
                initialCurrency = this.options.defaultCurrency;
                $(this.element).find('#ad_price_currency').val(initialCurrency);
            }

            $currencySwitchItems.click(function(){
                _self._setSelectedCurrency($(this));
            });

            $currencySwitchItems.filter('[data-currency="' + initialCurrency + '"]').click();
        },
        _setSelectedCurrency: function($senderItem) {
            var currency = $senderItem.data('currency');
            $(this.element).find('.price-filter .currency-switch .currency-item').removeClass('active');
            $senderItem.addClass('active');
            $(this.element).find('#ad_price_currency').val(currency);
            $(this.element).find('.price-currency-label').text(currency);
        },

        /**
         * Helper methods to get map's center, bounds and zoom level on map init. All this details should be in hidden
         * input fields upon pageload, but some of them (center and zoom level) can fallback to some default values
         */
        _getMapCenterOnInit: function(returnFallBackCenter) {
            if ('undefined' === typeof returnFallBackCenter) {
                returnFallBackCenter = false;
            }

            var mapCenter      = null;
            var fallBackCenter = new google.maps.LatLng(this.options.mapDetails.center);

            if ($.trim(this.$locationCenterInput.val())) {
                var location_center = $.trim(this.$locationCenterInput.val());
                if (-1 !== location_center.indexOf(',')) {
                    location_center = location_center.split(',');
                    if (2 == location_center.length) {
                        mapCenter = new google.maps.LatLng({
                            lat: parseFloat(location_center[0]),
                            lng: parseFloat(location_center[1])
                        });
                    }
                }
            }

            // if we have map's center, return it
            if (mapCenter) {
                return mapCenter;
            }

            // if we have map's bounds, return center from them
            var mapBounds = this._getMapBoundsOnInit();
            if (mapBounds) {
                return mapBounds.getCenter();
            }

            if (returnFallBackCenter) {
                mapCenter = fallBackCenter;
            }

            return mapCenter;
        },
        _getMapBoundsOnInit: function() {
            var mapBounds = null;

            if ($.trim(this.$locationBoundsInput.val())) {
                var location_bounds = $.trim(this.$locationBoundsInput.val());
                if (-1 !== location_bounds.indexOf(',')) {
                    location_bounds = location_bounds.split(',');
                    if (4 == location_bounds.length) {
                        var mapSW = new google.maps.LatLng({
                            lat: parseFloat(location_bounds[0]), // south
                            lng: parseFloat(location_bounds[1])  // west
                        });
                        var mapNE = new google.maps.LatLng({
                            lat: parseFloat(location_bounds[2]), // north
                            lng: parseFloat(location_bounds[3])  // east
                        });

                        mapBounds = new google.maps.LatLngBounds(mapSW, mapNE);
                    }
                }
            }

            return mapBounds;
        },
        _getMapZoomOnInit: function(returnFallBackZoom) {
            if ('undefined' === typeof returnFallBackZoom) {
                returnFallBackZoom = false;
            }

            var mapZoom      = null;
            var fallBackZoom = this.options.mapDetails.zoom;

            if ($.trim(this.$locationZoomInput.val())) {
                var location_zoom = parseInt($.trim(this.$locationZoomInput.val()));
                if (location_zoom >= this.options.mapDetails.minZoom && location_zoom <= 21) {
                    mapZoom = location_zoom;
                }
            }

            // if we have map's zoom, return it
            if (mapZoom) {
                return mapZoom;
            }

            if (returnFallBackZoom) {
                mapZoom = fallBackZoom;
            }

            return mapZoom;
        },
        _centerBasedOnMarkersBoundsCenter: function() {
            if (this.googleMap && !this.mapMarkersBounds.isEmpty()) {
                var mapMarkersBoundsCenter = this.mapMarkersBounds.getCenter();
                if ('undefined' !== typeof mapMarkersBoundsCenter) {
                    this.googleMap.panTo(mapMarkersBoundsCenter);
                }
            }
        },
        _setMapZoomAndCenterBasedOnMarkersBounds: function() {
            if (this.googleMap && !this.mapMarkersBounds.isEmpty()) {
                this.googleMap.fitBounds(this.mapMarkersBounds);
                var mapCenter = this.mapMarkersBounds.getCenter();
                if ('undefined' !== typeof mapCenter) {
                    var zoomLevel = this._getBoundsZoomLevel(this.mapMarkersBounds);
                    this._console('info', 'zoom level: ' + zoomLevel);
                    this._console('info', 'map center: ' + mapCenter.toUrlValue());
                    this.googleMap.setZoom(zoomLevel);
                    this.googleMap.panTo(mapCenter);
                } else {
                    this._console('error', 'no map center found in bounds');
                }
            } else {
                this._console('error', 'no map bounds found');
            }
        },

        /**
         * Initialize dependable behavior for parent_category_id and 'real' category_id (transaction type in our case)
         */
        _initDependableTransactionTypes: function() {
            var _self = this;
            this.$categorySelect        = $(this.element).find(this.options.selectors.categorySelect);
            this.$transactionTypeSelect = $(this.element).find(this.options.selectors.transactionTypeSelect);
            this.$categoryFiltersDiv    = $(this.element).find(this.options.selectors.categoryFiltersDiv);
            this.$moreFiltersInput      = $(this.element).find(this.options.selectors.moreFiltersInput);
            this.$moreFiltersBtn        = $(this.element).find(this.options.selectors.moreFiltersBtn);
            this.$mobileMoreFiltersBtn  = $(this.element).find(this.options.selectors.mobile.moreFiltersBtn);
            this.$moreFiltersDiv        = $(this.element).find(this.options.selectors.moreFiltersDiv);
            this.$moreFiltersBtn.click(function(){
                _self._showHideMoreFilters();
            });

            this.$categorySelect.on('change', function(){
                var $selectedOption = $(this).find('option:selected');
                if ('undefined' !== typeof $selectedOption && 1 == $selectedOption.length) {
                    var selectedParentID = parseInt($selectedOption.val());
                    if (selectedParentID) {
                        _self._setAvailableTransactionTypes(selectedParentID);
                    }
                }
            });
            this.$transactionTypeSelect.on('change', function(){
                var $selectedOption = $(this).find('option:selected');
                if ('undefined' !== typeof $selectedOption && 1 == $selectedOption.length) {
                    var selectedCategoryID = parseInt($selectedOption.val());
                    if (selectedCategoryID) {
                        _self._setTransactionType(selectedCategoryID);
                    }
                }
            });
            this.$categorySelect.trigger('change');
        },
        _setAvailableTransactionTypes: function(parentCategoryID) {
            this.$transactionTypeSelect.find('option[data-parent-id!="' + parentCategoryID + '"]').prop('disabled', true).hide();
            this.$transactionTypeSelect.find('option[data-parent-id="' + parentCategoryID + '"]').prop('disabled', false).show();

            // if we have a selected option that is currently visible, then we leave it as it is, otherwise, we select 
            // first possible option
            if (!this.$transactionTypeSelect.find('option[data-parent-id="' + parentCategoryID + '"]:selected').length) {
                // select first enabled option
                this.$transactionTypeSelect.find('option[data-parent-id="' + parentCategoryID + '"]:eq(0)').prop('selected', true);
            }

            // refresh selectpicker
            if ($().selectpicker) {
                this.$transactionTypeSelect.selectpicker('refresh');
            }

            this.$transactionTypeSelect.trigger('change');
        },
        _setTransactionType: function(categoryID) {
            // see if we have any filters related to this categoryID
            var $categoryAdditionalFilters = this.$moreFiltersDiv.find('.category-specific-filters[data-category-id="' + categoryID + '"]');
            if ('undefined' !== typeof $categoryAdditionalFilters && 1 === $categoryAdditionalFilters.length) {
                this.$moreFiltersDiv.find('.category-specific-filters[data-category-id!="' + categoryID + '"]').hide();
                this.$moreFiltersDiv.find('.category-specific-filters[data-category-id!="' + categoryID + '"] :input').prop('disabled', true);
                $categoryAdditionalFilters.find(':input').prop('disabled', false);
                $categoryAdditionalFilters.show();
                this.$moreFiltersBtn.data('category-id', categoryID);
                this.$categoryFiltersDiv.addClass('has-filters');
                this.$moreFiltersBtn.show();
                this.$moreFiltersDiv.show();
                if (this.$moreFiltersBtn.hasClass('active')) {
                    this.$categoryFiltersDiv.addClass('open');
                    this.$moreFiltersDiv.show();
                } else {
                    this.$categoryFiltersDiv.removeClass('open');
                    this.$moreFiltersDiv.hide();
                }
            } else {
                this.$categoryFiltersDiv.removeClass('has-filters open');
                this.$moreFiltersBtn.hide();
                this.$moreFiltersBtn.data('category-id', null);
                this.$moreFiltersDiv.find('.category-specific-filters').hide();
                this.$moreFiltersDiv.find('.category-specific-filters :input').prop('disabled', true);
            }
            this._triggerAjaxSearch();
        },
        _setAjaxTriggerForAllOtherFields: function() {
            // all other fields here refer to any other field that was not initialized at the _create part of the widget.
            // (all except: 'trazi u blizini', 'parent_category_id', 'category_id')
            var _self = this;
            $('#parameter_price_from, #parameter_price_to, #sort, #parameter_uploadable').on('change', function(){
                _self._triggerAjaxSearch();
            });
            this.$moreFiltersDiv.find(':input').on('change', function(){
                _self._triggerAjaxSearch();
            });
        },
        _showHideMoreFilters: function() {
            mobileScroolPosition = 0;
            if (this.$moreFiltersBtn.is(':visible')) {
                this.$categoryFiltersDiv.addClass('has-filters');
                var categoryID = this.$moreFiltersBtn.data('category-id');
                if (categoryID) {
                    var $visibleFilters = this.$moreFiltersDiv.find('.category-specific-filters[data-category-id="' + categoryID + '"]');
                    if (this.$moreFiltersDiv.is(':visible')) {
                        this.$categoryFiltersDiv.removeClass('open');
                        this.$moreFiltersDiv.hide();
                        $visibleFilters.find(':input').prop('disabled', true);
                        this.$moreFiltersBtn.removeClass('active').html(this.options.moreFiltersBtnText);
                        this.$moreFiltersInput.val('0');
                    } else {
                        $visibleFilters.find(':input').prop('disabled', false);
                        this.$categoryFiltersDiv.addClass('open');
                        this.$moreFiltersDiv.show();
                        this.$moreFiltersBtn.addClass('active').html(this.options.moreFiltersBtnActiveText);
                        this.$moreFiltersInput.val('1');
                        mobileScroolPosition = this.$categoryFiltersDiv.find('div.title').offset().top - 60;
                    }
                }
            } else {
                this.$categoryFiltersDiv.removeClass('has-filters open');
                $(this).find('.category-specific-filters :input').prop('disabled', true);
            }

            if (this.options.scrollToMoreFiltersOnMobile && this._isMobileScreen()) {
                if (mobileScroolPosition < 0) {
                    mobileScroolPosition = 0;
                }
                $(this.element).find('.scroll-y.search-by-location-filter').animate({
                    scrollTop: mobileScroolPosition
                }, 500);
            }
        },

        /**
         * Methods related to google map object
         */
        resizeSearchWrapper: function() {
            // TODO/FIX:
            // Currently there is an issue with android phones where, whenever 
            // user clicks on the autosuggest field, the screen is resized and 
            // the input looses focus and ... autosuggest doesn't actually work ;)
            if (this._isMobileScreen() && this.$locationTextInput.is(':focus')) {
                return;
            }

            mobileFooterHeight = 0;
            if (this.$searchOnMapMobileFooter.is(':visible')) {
                mobileFooterHeight = this.$searchOnMapMobileFooter.height();
                ///////////////////////////////
                var _self = this;
                this._delayedTrigger(
                    function() { _self._showCurrentMobileView(); },
                    'showCurrentMobileView'
                );
                /////////////////////////////
            } else {
                this.googleMapFirstIdle = true;
                if ('undefined' !== typeof this.$locationTextInput) {
                    if (this.$locationTextInput.closest('.map-section')) {
                        var $locationTextInput = this.$locationTextInput.detach();
                        $(this.options.selectors.mobile.listingAutosuggest).append($locationTextInput);
                    }
                }
                $(this.element).find(this.options.selectors.mapSection).show();
                $(this.element).find(this.options.selectors.listingSection).show();
/*
                this._delayedTrigger(
                    function() { _self.googleMapFirstIdle = false; },
                    'showCurrentMobileView'
                );
*/
            }

            this.$searchOnMapContent.css({'height': (window.innerHeight - this.$searchOnMapContent.offset().top - mobileFooterHeight) + 'px'})

            this.resizeFiltersDiv();
            this.resizeGoogleMapDiv();
        },
        resizeGoogleMapDiv: function() {
            this.$googleMapDiv.css({'height': this.$searchOnMapContent.height() + 'px'});
            this._triggerAjaxSearch();
        },
        resizeFiltersDiv: function() {
            mobileFooterHeight = 0;
            if (this.$searchOnMapMobileFooter.is(':visible')) {
                mobileFooterHeight = this.$searchOnMapMobileFooter.height();
            }
            $(this.element).css({
                'height': this.$searchOnMapContent.height() + 'px',
                'overflow':'hidden'
            });
            this.$filtersDiv.css({
                'height': $(this.element).height() + 'px'
            });
            if (this.options.ajax) {
                var _self = this;
                this._delayedTrigger(
                    function() {
                        _self._showSearchBtn(_self._isMobileScreen());
                    },
                    'searchBtn'
                );
            }
        },
        _updateMapBounds: function(gpsBounds) {
            var mapBounds = null;
            if ('object' === typeof gpsBounds) {
                if ('function' === typeof gpsBounds.getNorthEast && 'function' === typeof gpsBounds.getSouthWest) {
                    mapBounds = gpsBounds;
                }
            } else if ('string' === typeof gpsBounds) {
                var gpsBoundsArray = gpsBounds.split(',');
                if (4 == gpsBoundsArray.length) {
                    mapBounds = new google.maps.LatLngBounds(
                        new google.maps.LatLng({ // map's south-west point
                            lat: parseFloat(gpsBoundsArray[0]),
                            lng: parseFloat(gpsBoundsArray[1])
                        }),
                        new google.maps.LatLng({ // map's north-east point
                            lat: parseFloat(gpsBoundsArray[2]),
                            lng: parseFloat(gpsBoundsArray[3])
                        })
                    );
                }
            }

            if (mapBounds) {
                var mapZoomLevel = this._getBoundsZoomLevel(mapBounds);

                this.googleMap.fitBounds(mapBounds);
                this.googleMap.setZoom(mapZoomLevel);
                this._updateCurrentMapInfo();
            }
        },
        _getInitialClassifiedsMarkersFromListing: function() {
            var initialMarkers = [];

            $(this.element).find('.results .ad-box[data-marker-json]').each(function(i, adBox){
                var markerJson = $(adBox).data('marker-json');
                initialMarkers.push(markerJson);
            });

            return initialMarkers;
        },
        // initialize autosuggest for places
        _initLocationsAutosuggest: function() {
            if ('function' !== typeof google.maps.places.Autocomplete) {
                this._console('error', 'Google Maps Places API is not loaded!');
                this.$locationTextInput       = null;
                this.locationAutocomplete     = null;
                return;
            }

            this.$locationTextInput       = $(this.element).find(this.options.selectors.locationTextInput);
            if (this.$locationTextInput.length !== 1) {
                this._console('error', 'Found LocationText fields: ' + this.$locationTextInput.length);
                this.$locationTextInput   = null;
                this.locationAutocomplete = null;
            }

            if (this.$locationTextInput) {
                this.$locationBoundsInput = $(this.element).find(this.options.selectors.locationBoundsInput);
                this.$locationZoomInput   = $(this.element).find(this.options.selectors.locationZoomInput);
                this.$locationCenterInput = $(this.element).find(this.options.selectors.locationCenterInput);

                this.locationAutocomplete = new google.maps.places.Autocomplete(this.$locationTextInput.get(0), {
                    types: ['geocode'],
                    componentRestrictions: {country: 'hr'}
                });

                var _self = this;
                this.locationAutocomplete.addListener('place_changed', function(){ _self._onAutocompletePlaceChanged(); });
                this._selectFirstAutosuggestedPlace(this.$locationTextInput.get(0));

                return this.locationText;
            }
        },
        _selectFirstAutosuggestedPlace: function(inputField) {
            // store the original event binding function
            var _addEventListener = (inputField.addEventListener) ? inputField.addEventListener : inputField.attachEvent;

            function addEventListenerWrapper(eventType, eventListener) {
                // Simulate a 'down arrow' keypress on hitting 'return' when no pac suggestion is selected,
                // and then trigger the original eventListener.
                if ('keydown' === eventType) {
                    var origEventListener = eventListener;
                    eventListener = function(event) {
                        var suggestionSelected = $(".pac-item-selected").length > 0;
                        if (event.which == 13 && !suggestionSelected) {
                            var simulatedDownArrow = $.Event('keydown', {keyCode:40, which:40});
                            origEventListener.apply(inputField, [simulatedDownArrow]);
                        }

                        origEventListener.apply(inputField, [event]);
                    };
                }

                // add the modified eventListener
                _addEventListener.apply(inputField, [eventType, eventListener]);
            }

            if (inputField.addEventListener) {
                inputField.addEventListener = addEventListenerWrapper;
            } else if (inputField.attachEvent) {
                inputField.attachEvent = addEventListenerWrapper;
            }
        },
        // When the user selects a city, get the place details zoom the map to that location/place
        _onAutocompletePlaceChanged: function() {
            var place = this.locationAutocomplete.getPlace();
            if (this._isMobileScreen()) {
                this.$locationTextInput.blur();
            }
            if (place.geometry) {
                if (this._isMobileScreen()) {
                    this.$locationTextInput.parent()
                        .removeClass('has-error')
                        .find('p.help-block').remove();
                }

                var placeBounds = null;
                var placeCenter = null;
                var zoomLevel   = null;

                if ('undefined' !== typeof place.geometry.viewport) {
                    placeBounds = new google.maps.LatLngBounds(
                        place.geometry.viewport.getSouthWest(), 
                        place.geometry.viewport.getNorthEast()
                    );
                    placeCenter = placeBounds.getCenter();
                    zoomLevel   = this._getBoundsZoomLevel(placeBounds);
                } else {
                    placeBounds = null;
                    placeCenter = place.geometry.location;
                    zoomLevel   = this.options.mapDetails.optimalZoom;

                    if (this._isMobileScreen()) {
                        placeCircle = new google.maps.Circle({
                            center: placeCenter,
                            radius: 500,
                        });
                        placeBounds = placeCircle.getBounds();
                        //zoomLevel   = this._getBoundsZoomLevel(placeBounds);
                    }
                }

                if (placeBounds) {
                    this.$locationBoundsInput.val(placeBounds.toUrlValue());
                    this.googleMapBounds = placeBounds;
                    this.googleMap.fitBounds(placeBounds);
                }
                this.$locationZoomInput.val(zoomLevel);
                this.googleMapZoom = zoomLevel;
                this.googleMap.setZoom(zoomLevel);
                this.$locationCenterInput.val(placeCenter.toUrlValue());
                this.googleMapCenter = placeCenter;
                this.googleMap.panTo(placeCenter);

                this._console('log', [placeBounds, zoomLevel, placeCenter.toUrlValue()]);

                if (this._isMobileScreen()) {
                    google.maps.event.trigger(this.googleMap, 'resize');
                    this._setLastKnownBounds();
                }
            }
        },
        /**
         * Calculate optimal map's zoom level based on given bounds and map's dimensions
         * @author John S (http://stackoverflow.com/a/13274361)
         * 
         * @param  google.maps.LatLngBounds  bounds  Bounds we'll be working with
         * @return int                               Zoom level that will fit requested bounds
         */
        _getBoundsZoomLevel: function(bounds) {
            var mapDim    = { height: this.$googleMapDiv.height(), width: this.$googleMapDiv.width() };
            var WORLD_DIM = { height: 256, width: 256 };
            var ZOOM_MAX  = 21;

            function latRad(lat) {
                var sin = Math.sin(lat * Math.PI / 180);
                var radX2 = Math.log((1 + sin) / (1 - sin)) / 2;
                return Math.max(Math.min(radX2, Math.PI), -Math.PI) / 2;
            }

            function zoom(mapPx, worldPx, fraction) {
                return Math.floor(Math.log(mapPx / worldPx / fraction) / Math.LN2);
            }

            var ne = bounds.getNorthEast();
            var sw = bounds.getSouthWest();

            var latFraction = (latRad(ne.lat()) - latRad(sw.lat())) / Math.PI;

            var lngDiff = ne.lng() - sw.lng();
            var lngFraction = ((lngDiff < 0) ? (lngDiff + 360) : lngDiff) / 360;

            var latZoom = zoom(mapDim.height, WORLD_DIM.height, latFraction);
            var lngZoom = zoom(mapDim.width, WORLD_DIM.width, lngFraction);

            var zoomLevel = Math.min(latZoom, lngZoom, ZOOM_MAX);
            if (zoomLevel <= this.options.mapDetails.minZoom) {
                zoomLevel = this.options.mapDetails.optimalZoom;
            }

            return zoomLevel;
        },
        _setLastKnownBounds: function() {
            if ('undefined' !== typeof this.googleMapBounds) {
                this.googleMap.fitBounds(this.googleMapBounds);
            }
            if ('undefined' !== typeof this.googleMapZoom) {
                this.googleMap.setZoom(this.googleMapZoom);
            }
            if ('undefined' !== typeof this.googleMapCenter) {
                this.googleMap.panTo(this.googleMapCenter);
            }
        },
        // update map details (write them to hidden inputs in case the form will be submitted regularly - without ajax!)
        _updateCurrentMapInfo: function(infoToUpdate) {
            if (!this._isMobileScreen() || (this._isMobileScreen() && this.currentMobileView == 'map')) {
                if ('undefined' === typeof infoToUpdate) {
                    infoToUpdate = 'all';
                } else {
                    infoToUpdate = $.trim(infoToUpdate.toLowerCase());
                }

                switch(infoToUpdate) {
                    case 'bounds':
                        this.$locationBoundsInput.val(this.googleMap.getBounds().toUrlValue());
                        this.googleMapBounds = this.googleMap.getBounds();
                        break;
                    case 'zoom':
                        this.$locationZoomInput.val(this.googleMap.getZoom());
                        this.googleMapZoom = this.googleMap.getZoom();
                        break;
                    case 'center':
                        this.$locationCenterInput.val(this.googleMap.getCenter().toUrlValue());
                        this.googleMapCenter = this.googleMap.getCenter();
                        break;
                    default:
                        this.$locationBoundsInput.val(this.googleMap.getBounds().toUrlValue());
                        this.$locationZoomInput.val(this.googleMap.getZoom());
                        this.$locationCenterInput.val(this.googleMap.getCenter().toUrlValue());

                        this.googleMapBounds = this.googleMap.getBounds();
                        this.googleMapZoom   = this.googleMap.getZoom();
                        this.googleMapCenter = this.googleMap.getCenter();
                }



                if (this.googleMapFirstIdle) {
                    this._console('info', 'first idle - no ajax call');
                    this.googleMapFirstIdle = false;
                } else {
                    this._triggerAjaxSearch();
                }
            }
        },
        _testIfMapBoundsAreSet: function() {
            var boundsAreSet = false;

            if (
                (
                    'undefined' !== typeof this.$locationBoundsInput && 
                    'undefined' !== typeof this.$locationZoomInput &&
                    'undefined' !== typeof this.$locationCenterInput
                )
                &&
                (
                    $.trim(this.$locationBoundsInput.val()) &&
                    $.trim(this.$locationZoomInput.val()) &&
                    $.trim(this.$locationCenterInput.val())
                )
            ) {
                boundsAreSet = true;
            }

            return boundsAreSet;
        },
        _showMapInfo: function() {
            this._console('log', {
                'bounds': this.googleMap.getBounds().toUrlValue(),
                'zoom'  : this.googleMap.getZoom(),
                'center': this.googleMap.getCenter().toUrlValue()
            });
        },
        _initMapSpiderification: function() {
            this.oms = null;
            if (this.options.mapDetails.spiderify && 'object' === typeof this.options.mapDetails.spiderify && 'undefined' !== typeof OverlappingMarkerSpiderfier && this.googleMap) {
                this.oms = new OverlappingMarkerSpiderfier(
                    this.googleMap, 
                    this.options.mapDetails.spiderify
                );
                var _self = this;

                this.oms.addListener('click', function(marker, event) {
                    _self.showMarkersInfo(marker);
                });
            }
        },
        _addMarkerToOMS: function(marker) {
            if (this.oms) {
                this.oms.addMarker(marker);
            }
        },
        _removeMarkerFromOMS: function(marker) {
            if (this.oms) {
                this.oms.removeMarker(marker);
            }
        },
        _initMapClustering: function() {
            this.markerClusterer = null;
            if (this.options.mapDetails.clustering && 'object' === typeof this.options.mapDetails.clustering && 'undefined' !== typeof MarkerClusterer && this.googleMap) {
                this.markerClusterer = new MarkerClusterer(
                    this.googleMap, 
                    this.clusteredMarkers,
                    this.options.mapDetails.clustering
                );
            }
        },
        _addMarkerToCluster: function(marker) {
            if (this.markerClusterer) {
                this.markerClusterer.addMarker(marker);
            }
        },
        _removeMarkerFromCluster: function(marker) {
            if (this.markerClusterer) {
                this.markerClusterer.removeMarker(marker);
            }
        },
        // initialize the actual map on page
        _initGoogleMap: function() {
            this.googleMap        = null;
            this.markerInfoWindow = null;
            this.displayedInfoWindowMarker = null;

            if ('undefined' !== typeof window.google) {
                if ('undefined' !== typeof window.google.maps) {
                    if ('function' !== typeof google.maps.Map) {
                        this._console('error', 'Google Maps API is not loaded!');
                        this.$filtersDiv    = null;
                        this.$googleMapDiv  = null;
                        this.googleMap      = null;
                        return;
                    }

                    var googleMapObject = $(this.element).find(this.options.selectors.googleMap);
                    if (googleMapObject.length !== 1) {
                        this._console('error', 'Found GoogleMaps: ' + googleMapObject.length);
                        googleMapObject = null;
                    }

                    if (googleMapObject) {
                        // initialize autosuggest
                        this._initLocationsAutosuggest();

                        this.$filtersDiv   = $(this.element).find('div[data-view-type="filters"]');
                        this.$googleMapDiv = googleMapObject;
                        this.resizeSearchWrapper();

                        var mapCenter = this._getMapCenterOnInit(true);
                        var mapZoom   = this._getMapZoomOnInit(true);
                        this.googleMap = new google.maps.Map(this.$googleMapDiv.get(0), {
                            center: mapCenter,
                            zoom: mapZoom,
                            minZoom: this.options.mapDetails.minZoom,
                            mapTypeControl: false,
                            panControl: false,
                            //zoomControl: false,
                            streetViewControl: false,
                            clickableIcons: false
                        });
                        this.markerInfoWindow = new google.maps.InfoWindow({
                            disableAutoPan: true
                        });
                        /*
                         * The google.maps.event.addListener() event waits for
                         * the creation of the infowindow HTML structure 'domready'
                         * and before the opening of the infowindow defined styles
                         * are applied.
                         */
                        google.maps.event.addListener(this.markerInfoWindow, 'domready', function() {

                            // Reference to the DIV which receives the contents of the infowindow using jQuery
                            var iwOuter = $('.gm-style-iw');

                            /* The DIV we want to change is above the .gm-style-iw DIV.
                             * So, we use jQuery and create a iwBackground variable,
                             * and took advantage of the existing reference to .gm-style-iw for the previous DIV with .prev().
                             */
                            var iwBackground = iwOuter.prev();

                            // Remove the background shadow DIV
                            iwBackground.children(':nth-child(2)').css({'display' : 'none'});

                            // Remove the white background DIV
                            iwBackground.children(':nth-child(4)').css({'display' : 'none'});

                            // Changes the desired color for the tail outline.
                            // The outline of the tail is composed of two descendants of div which contains the tail.
                            // The .find('div').children() method refers to all the div which are direct descendants of the previous div. 
                            iwBackground.children(':nth-child(3)').find('div').children().css({'box-shadow': 'rgba(0, 0, 0, 0.35) 0px 1px 6px', 'z-index':5});

                            // Taking advantage of the already established reference to
                            // div .gm-style-iw with iwOuter variable.
                            // You must set a new variable iwCloseBtn.
                            // Using the .next() method of JQuery you reference the following div to .gm-style-iw.
                            // Is this div that groups the close button elements.
                            var iwCloseBtn = iwOuter.next();

                            // Apply the desired effect to the close button
                            iwCloseBtn.css({
                                'border-radius': '10px',
                                'box-shadow': 'rgba(0, 0, 0, 0.35) 0px 1px 6px'
                            });
                        });
                        google.maps.event.addListener(this.markerInfoWindow, 'closeclick', function(){ _self.markerInfoWindowDisplayed = false; });

                        this.mapMarkersBounds   = new google.maps.LatLngBounds();
                        this.mapMarkers         = {};
                        this.loadedMarkers      = {};
                        this.googleMapFirstIdle = true;
                        this._initMapSpiderification();
                        this._initMapClustering();

                        var _self = this;
                        //this.googleMap.addListener('resize', function(){ _self._setLastKnownBounds(); });
                        this.googleMap.addListener('idle', function(){ _self._updateCurrentMapInfo(); });

                        return this.googleMap;
                    }
                } else {
                    this._console('error', 'Google Maps API is not loaded!');
                }
            } else {
                this._console('error', 'Google API is not loaded!');
            }
        },

        clearRenderedMarkers: function() {
            if (this.googleMap) {
                this.mapMarkersBounds = new google.maps.LatLngBounds();
                var _self = this;
                var mapMarkers = Object.keys(_self.mapMarkers).map(function(k) { return {'id': k, 'marker':_self.mapMarkers[k]} });
                if (mapMarkers.length) {
                    $(mapMarkers).each(function(i, item){
                        var markerInListing = $(_self.options.selectors.listingContainer).find('.ad-box[data-id="' + item.id + '"]').length > 0;
                        if (!markerInListing) {
                            item.marker.setMap(null);
                            _self.mapMarkers[item.id] = null;
                            delete _self.mapMarkers[item.id];
                            _self.loadedMarkers[item.id] = null;
                            delete _self.loadedMarkers[item.id];

                            _self._removeMarkerFromOMS(item.marker);
                            _self._removeMarkerFromCluster(item.marker);
                        } else {
                            _self.mapMarkersBounds.extend(item.marker.getPosition());
                        }
                    });
                }
            }
        },
        renderClassifiedsMarkers: function(markersArray) {
            if (this.googleMap) {
                var _self = this;
                $(markersArray).each(function(i, classified){
                    // Skip already loaded markers
                    if (_self.loadedMarkers[classified.id]) {
                        return;
                    }

                    var mapMarkerPoint = new google.maps.LatLng(classified.lat, classified.lng);
                    var mapMarker      = new google.maps.Marker({
                        animation: 'DROP',
                        position: mapMarkerPoint,
                        map: _self.googleMap,
                        icon: '/assets/img/icn-location-dark-stroke.png',
                        title: classified.title,
                        desc: _self.buildInfoWindowMarkup(classified)
                    });

                    _self.mapMarkers[classified.id]    = mapMarker;
                    _self.loadedMarkers[classified.id] = true;
                    _self.mapMarkersBounds.extend(mapMarkerPoint);

                    if (_self.oms) {
                        _self._addMarkerToOMS(mapMarker);
                    } else {
                        mapMarker.addListener('click', function(){ _self.showMarkersInfo(this) });
                    }
                    _self._addMarkerToCluster(mapMarker);
                });
            }
        },
        showMarkersInfo: function(marker) {
            if (this.markerInfoWindow) {
                if (this.displayedInfoWindowMarker !== marker) {
                    this.markerInfoWindow.setContent(marker.desc);
                    if (this.markerClusterer) {
                        markersCluster = this.markerClusterer.getMarkersCluster(marker);
                        markerPosition = marker.getPosition();
                        if ('object' === typeof markersCluster && markersCluster.getSize() >= this.options.mapDetails.clustering.minimumClusterSize) {
                            markerPosition = markersCluster.getCenter();
                        }
                        this.markerInfoWindow.setPosition(markerPosition);
                        this.markerInfoWindow.open(this.googleMap);
                    } else {
                        this.markerInfoWindow.open(this.googleMap, marker);
                    }
                    this.displayedInfoWindowMarker = marker;
                } else {
                    this.closeMarkersInfo();
                }
            }
        },
        closeMarkersInfo: function() {
            if (this.markerInfoWindow) {
                this.markerInfoWindow.close();
                this.displayedInfoWindowMarker = null;
            }
        },
        buildInfoWindowMarkup: function(classified) {
            var markup       = '<div data-classified-id="' + classified.id + '" class="ad-box ad-box-default mapPreview">';

            var pinLeftInfo  = '<div class="pinLeftInfo"><span>' + classified.published_at + '</span></div>';
            var pinRightInfo = [];

            if ('undefined' !== typeof classified.info) {
                if ('undefined' !== typeof classified.info.right && classified.info.right.length) {
                    $(classified.info.right).each(function(i, rightInfo){
                        pinRightInfo.push('<span>' + rightInfo + '</span>');
                    });
                }
            }

            if (pinRightInfo.length) {
                pinRightInfo = '<div class="pinRightInfo">' + pinRightInfo.join('') + '</div>';
            } else {
                pinRightInfo = '';
            }

            markup += '<div class="image-wrapper"><a href="' + classified.frontend_url + '" class="classified-img" target="_blank">' + classified.thumb + '</a>' + pinLeftInfo + pinRightInfo + '</div>';
            markup += '<h3><a href="' + classified.frontend_url + '" target="_blank">' + classified.title + '</a></h3>';
            if (classified.price) {
                markup += '<div class="classified-footer">';
                markup += '<span class="price-kn">' + (classified.price.main ? classified.price.main : '') + '</span>';
                markup += '<span class="price-euro">' + (classified.price.other ? classified.price.other : '') + '</span>';
                markup += '</div>';
            }
            markup += '</div>';

            return markup;
        },
        _ajaxifyPagination: function() {
            if (this.initialized && this.options.ajax) {
                var $paginationLinks = $(this.options.selectors.listingContainer).find('ul.pagination li[data-page] a');
                if ($paginationLinks.length) {
                    var _self = this;
                    $($paginationLinks).each(function(i, li_a){
                        $(li_a).off('click.' + _self.options.instanceName);
                        $(li_a).on('click.' + _self.options.instanceName, function(event){
                            event.preventDefault();
                            _self._openPageViaAjax($(li_a));
                        });
                    });
                }
            }
        },
        _openPageViaAjax: function($senderLink) {
            var page = parseInt($senderLink.parent().data('page'));
            if (page) {
                this._console('info', 'pagination ajax');
                this._triggerAjaxSearch(page, true);
            } else {
                // fallback to actual link in case we couldn't get valid page number
                window.location.href = $senderLink.attr('href');
            }
        },
        _triggerAjaxSearch: function(page, forceAjax) {
            if ('undefined' === typeof forceAjax) {
                forceAjax = false;
            }
            if (forceAjax || !this._isMobileScreen() || (this._isMobileScreen() && 'map' == this.currentMobileView)) {
                if ('undefined' == typeof page) {
                    page = null;
                }
                if (this.initialized && this.options.ajax && !this.$locationTextInput.is(':focus')) {
                    var _self = this;
                    this._delayedTrigger(
                        function(){
                            _self._ajaxSearch(page);
                        },
                        'ajaxSearch',
                        this.options.ajax.triggerDelay
                    );
                }
            }
        },
        _getAjaxifiedQueryString: function(page) {
            if ('undefined' == typeof page) {
                page = null;
            }
            var fullQueryString    = $.trim($(this.element).serialize());
            var cleanedQueryString = $.trim(fullQueryString.replace(/[^&]+=\.?(?:&|$)/g, ''));

            // Removing default filter values in order to reduce url length
            var queryStringArray     = cleanedQueryString.split('&');
            var finalQueryStringJson = {};
            for (var i = 0, il = queryStringArray.length; i < il; i++) {
                var queryStringSegment = $.trim(queryStringArray[i]);
                if (queryStringSegment) {
                    var filterParts = queryStringSegment.split('=');
                    var filterName  = $.trim(filterParts[0]);
                    if ('parent_category_id' !== filterName) {
                        var filterValue = $.trim(filterParts[1]);
                        if (OGL.isInt(filterValue)) {
                            filterValue = parseInt(filterValue);
                        } else if (OGL.isFloat(filterValue)) {
                            filterValue = parseFloat(filterValue);
                        }

                        var $formFilter = $(this.element).find('[name="' + filterName + '"]');
                        if ($formFilter.length) {
                            if (filterValue) {
                                var pushFilter             = true;
                                var formFilterDefaultValue = $formFilter.data('default');

                                if ('undefined' !== typeof formFilterDefaultValue && $.trim(formFilterDefaultValue) == filterValue) {
                                    pushFilter = false;
                                }

                                if (pushFilter) {
                                    finalQueryStringJson[filterName] = filterValue;
                                }
                            }
                        }
                    }
                }
            }
/*
            TODO: maybe we'll have to distinguish when we're running ajax from mobile/desktop so we can get different number of
            results per page...

            if (this._isMobileScreen()) {
                finalQueryStringJson['itemsPerPage'] = 100;
            }
*/
            if (!$.isEmptyObject(finalQueryStringJson)) {
                if (page) {
                    finalQueryStringJson['page'] = page;
                }
                return Object.keys(finalQueryStringJson).map(function(k) { return k + '=' + finalQueryStringJson[k] }).join('&');
            }

            return null;
        },
        _ajaxSearch: function(page) {
            if ('undefined' == typeof page) {
                page = null;
            }

            this._console('info', 'Triggering ajax call..');
            var queryString = this._getAjaxifiedQueryString(page);
            if (queryString) {
                $(this.element).find(this.options.selectors.ajaxLoading).show();
                $(this.options.selectors.listingContainer).css({'opacity':.5});
                var ajaxURL = $(this.element).attr('action') + '?' + queryString;
                var _self = this;
                $.get(
                    ajaxURL, 
                    function(json) {
                        _self._handleAjaxDraw(ajaxURL, json);
                    },
                    'json'
                );
            }
        },
        _handleAjaxDraw: function(ajaxURL, json) {
            this.closeMarkersInfo();
            var _self = this;
            if (this.options.historyApi && json.md5 && 'undefined' !== typeof window.history && 'function' === typeof window.history.pushState) {
                _self.onpophistory[json.md5] = json.data;
                window.history.pushState({'md5': json.md5, 'map':{'bounds':this.googleMap.getBounds().toJSON(), 'center':this.googleMap.getCenter().toJSON(), 'zoom':this.googleMap.getZoom()}}, window.document.title, ajaxURL);
            }

            if (json.status) {
                if ('undefined' !== typeof json.data) {
                    var $newContent = $(json.data);
                    _self._addHoverInfoForFoundItems($newContent);
                    $(_self.options.selectors.listingContainer).html('');
                    $(_self.options.selectors.listingContainer).append($newContent.fadeIn('fast', function() {
                        if (_self.options.scrollToTopOfListingAfterAjax && !_self._isMobileScreen()) {
                            var scrollPosition = 0;
                            $(_self.element).find('.scroll-y.search-by-location-filter > div').not(_self.options.selectors.listingContainer).each(function(i, elem){
                                scrollPosition += $(elem).outerHeight(true);
                            });
                            if (scrollPosition < 0) {
                                scrollPosition = 0;
                            }
                            $(_self.element).find('.scroll-y.search-by-location-filter').animate({
                                scrollTop: scrollPosition
                            }, 500);
                        }

                        _self.clearRenderedMarkers();
                        var initialClassifiedsMarkers = _self._getInitialClassifiedsMarkersFromListing();
                        if (initialClassifiedsMarkers.length) {
                            _self.renderClassifiedsMarkers(initialClassifiedsMarkers);
                        }
                        _self._ajaxifyPagination();
                    }));
                }
            } else {

            }
            $(this.options.selectors.listingContainer).css({'opacity':1});
            $(this.element).find(this.options.selectors.ajaxLoading).hide();
        },
        _addHoverInfoForFoundItems: function($content) {
            if (this.options.showInfoWindowOnMarkerHover) {
                var _self = this;
                $content.find('.ad-box').on({
                    'mouseenter': function(){
                        var mapMarker = ('undefined' !== typeof _self.mapMarkers[$(this).data('marker-json').id]) ? _self.mapMarkers[$(this).data('marker-json').id] : null;
                        if (mapMarker) {
                            _self.showMarkersInfo(mapMarker);
                        }
                    },
                    'mouseleave': function(){
                        _self.closeMarkersInfo();
                    }
                });
            }
        }
    });
}(jQuery));
