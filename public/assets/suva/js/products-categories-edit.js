$(document).ready(function(){
    var $disabler_checkboxes = $('.product-disabler-checkbox');
    $disabler_checkboxes.on('change', function(){
        var $el = $(this);
        var $panel = $el.parents('.panel');
        if ($el.prop('checked')) {
            $panel.removeClass('panel-info').addClass('panel-danger');
        } else {
            $panel.removeClass('panel-danger').addClass('panel-info');
        }
    });
    $disabler_checkboxes.trigger('change');

    $('#types-tabs').on('click', 'a', function(e) {
        e.preventDefault();
        $(this).tab('show');
    });

    $('input.price').autoNumeric('init', {aSep: '.', aDec: ',', lZero: 'deny'});

    // Setup a generic form watcher and enable/disable the save button if
    // any changes are detected
    var $window = $(window);
    var $form = $('#products-config');
    var $submit = $form.find('button[type="submit"]');
    var $inputs = $form.find(':input');

    // Hook up form watcher
    $form.changeWatcher();

    // Alert on navigating away if there are any unsaved changes (unless a submit happens)
    $window.on('beforeunload', function(e){
        if ($form.hasChanges()) {
            return 'Unsaved changes detected, continue anyway?';
        }
    });
    // Turn off beforeunload handler if form submit is happening
    $form.on('submit', function(e){
        $window.off('beforeunload');
    });

    // Trim text/price values on blur
    $('.panel-body input[type="text"]').on('blur', function(){
        $(this).val($.trim($(this).val()));
    });

    // Enable submit button on any form change
    $inputs.on('change', function(){
        if ($form.hasChanges()) {
            $submit.prop('disabled', false);
        } else {
            $submit.prop('disabled', true);
        }
    });
    $submit.prop('disabled', true);
});
