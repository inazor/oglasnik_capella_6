$(document).ready(function(){
    var $birth_date = $('#birth_date').datepicker({
        format: 'dd.mm.yyyy',
        weekStart: 1,
        language: 'hr',
        autoclose: true
    });

	//radi ovoga pokazuje dupla polja
	
//     $('select').selectpicker({
//         style: 'btn btn-default',
//         size: 8,
//         liveSearch: true,
//         noneResultsText: 'No items match',
//         mobile: OGL.is_mobile_browser,
//         showSubtext: true
//     });

    // bind country dropdown change
    $('#country_id').change(function(){
        $('#county_id').find('option').remove();
        var county_div = $('#county_div');
        if ($(this).val() !== '') {
            $.get(
                '/ajax/counties/' + $(this).val(),
                function (data) {
                    if (data.length) {
                        $('#county_id').append($('<option/>').text('Odaberite županiju...'));
                        $.each(data, function(idx, data){
                            $('#county_id').append($('<option/>').attr('value', data.id).text(data.name));
                        });
                        if (!county_div.is(':visible')) {
                            county_div.fadeIn();
                        }
                    } else {
                        if (county_div.is(':visible')) {
                            county_div.fadeOut();
                        }
                    }
                },
                'json'
            );
        } else {
            county_div.fadeOut();
        }
    });

    $('input[name=type]').change(function(){
        var $user_type_details = $('#userTypeDetails');
        if ($(this).val() == '1') {
            if ($user_type_details.hasClass('companyData')) {
                $user_type_details.removeClass('companyData');
            }
            $('.form-group[data-personal-label]').each(function(i, item){
                $(item).find('label').text($(item).data('personal-label'));
            });
            $birth_date.datepicker('setEndDate', '-13y');
        } else {
            if(!$user_type_details.hasClass('companyData')) {
                $user_type_details.addClass('companyData');
            }
            $('.form-group[data-company-label]').each(function(i, item){
                $(item).find('label').text($(item).data('company-label'));
            });
            $birth_date.datepicker('setEndDate', '0');
        }
    });

    // TODO: this could be made more reusable probably
    $('.target-toggler').on('change', function(){
        var $el = $(this);
        var checked = $el.prop('checked');
        var $toggle_target = $el.data('toggle-target');
        if ($toggle_target) {
            $($toggle_target).toggle(checked);
        }
    }).trigger('change');

    $('input[name=type]:checked').trigger('change');

    if(jQuery().mask) {
        $('input#oib').mask('00000000000');
    }
		
		var $cash_out_report_date = $('.cash_out_report_date').datepicker({
        format: 'yyyy-mm-dd',
        weekStart: 1,
        language: 'hr',
        autoclose: true
    });
		
    $(".cash-out").click(function () {
       
       
        
			var cash_out_report_date = ($(".cash_out_report_date").val())
		    var fiscal_location = ($(".cash_out_fiscal_location_id").val())
			var user_id = ($(".cash_out_user_id").val())
			
			
		    document.location.href = 'http://www.oglasnik.hr/admin/cash-out/createPdf/'+user_id+'/'+fiscal_location+'/'+cash_out_report_date
			
        
			
	});
	

		

		
});

