/**
 * Handle category_id select change
 */
function handle_category_id_select_change() {
    var selected_category_id = $.trim($('#category_id').selectpicker('val'));
    if ('' !== selected_category_id && parseInt(selected_category_id)) {
        $('#createNewAdModalCreateBtn').removeClass('disabled');
    } else {
        $('#createNewAdModalCreateBtn').addClass('disabled');
    }
}

/**
 * Submits the form with selected category_id
 */
function createNewAdModalCreateBtn_Click() {
    // bootstrap handles this automatically, but let it be here just in case ;)
    if (!$('#createNewAdModalCreateBtn').hasClass('disabled')) {
        $('#frm_createNewAd').submit();
    }
}

$(document).ready(function(){
    
    var $cat_id_dropdown = $('#category_id');
    $cat_id_dropdown.selectpicker({
        style: 'btn btn-default',
        size: 8,
        liveSearch: true,
        title: 'Choose category',
        noneSelectedText: 'Choose category',
        noneResultsText: 'No categories match',
        mobile: OGL.is_mobile_browser,
        showSubtext: true
    });

    $cat_id_dropdown.on('change', handle_category_id_select_change);

    $('#createNewAdModalCreateBtn').on('click', createNewAdModalCreateBtn_Click);

    $('#createNewAd').on('click', function(){
        handle_category_id_select_change();
        $('#createNewAdModal').modal({
            backdrop: 'static',
            keyboard: true
        }).on('shown.bs.modal', function(e){
            // trigger a click on the dropdown toggle to show the category selector/search when modal opens
            $('button.dropdown-toggle').trigger('click');
        });
    });
});
