function get_matching_parameters() {
    var $matching_parameters = $('#matching_parameters');
    var $matching_parameters_container = $matching_parameters.closest('div.row');
    var $moveBtn = $('#changeAdsCategoryModalMoveBtn');
    var selected_category_id = $.trim($('#new_category_id').selectpicker('val'));

    $matching_parameters_container.addClass('hidden');
    $moveBtn.addClass('disabled').data('target-category-id', '');

    if ('' !== selected_category_id && parseInt(selected_category_id)) {
        $.get(
            '/admin/category-parameterization/compare/' + $('#category_id').val() + '/' + selected_category_id,
            function(json){
                if (json.status) {
                    $moveBtn.removeClass('disabled').data('target-category-id', selected_category_id);
                    if (json.count > 0) {
                        var compare_results = [];
                        $.each(json.data, function(i, name){
                            compare_results.push(name);
                        });
                        $matching_parameters.removeClass('text-danger').html('These parameters can be transferred to selected category<br/><span class="text-success">' + compare_results.join(', ') + '.</span><br /><span class="text-danger">All other parameters (if they exist) will be lost!</span>');
                    } else {
                        $matching_parameters.addClass('text-danger').html('There are no parameters in common. If you choose to move this ad to this category you will loose all the data!');
                    }
                } else {
                    var error_msg = 'Something went wrong... We were unable to compare categories';
                    if ('undefined' !== typeof json.msg && $.trim(json.msg)) {
                        error_msg = $.trim(json.msg);
                    }
                    $matching_parameters.addClass('text-danger').html(error_msg);
                }
                $matching_parameters_container.removeClass('hidden');
            },
            'json'
        );
    }
}

function get_users_public_phones() {
    var username = $.trim($('#user_id').data('username'));
    if (username) {
        $.get(
            '/ajax/userInfo/' + username,
            function(json){
                if (json.status) {
                    $('#phone1').val(json.data.phone1);
                    $('#phone2').val(json.data.phone2);
                } else {
                    if ('undefined' !== typeof json.msg && $.trim(json.msg)) {
                        alert(json.msg);
                    }
                }
            },
            'json'
        );
    }
}

function save_remark() {
    $.post(
        '/admin/ads/saveRemark', {
            '_csrftoken': $('#_csrftoken').val(),
            'ad_id'     : $('#ads_remark').data('ad-id'),
            'remark'    : $('#ads_remark').val()
        },
        function (json) {
            if (!json.status) {
                if ('undefined' !== typeof json.msg) {
                    if ('undefined' !== typeof json.msg && $.trim(json.msg)) {
                        alert("Ooops!\n" + json.msg);
                    }
                }
            }
        },
        'json'
    );
}

function add_cart() {

    var issues_values = new Array();
        $.each($("input[name='offline-issues[]']:checked"), function() {
                issues_values.push($(this).val());
        });

    $.post(
        '/admin/fakture/insertToCart/' + $('#ad_id').val() , {
            '_csrftoken'        : $('#_csrftoken').val(),
            'user_id'             : $('#user_id').val(),
            'offline-issues'    : issues_values,
            'offline-products'  : $('#offline-products-option').val(),
            'offline-discounts' : $('#offline-discounts').val(),
            'offline-publications' : $('#offline-publications').val()
        },
        function (json) {
            if (!json.status) {
                if ('undefined' !== typeof json.msg) {
                    if ('undefined' !== typeof json.msg && $.trim(json.msg)) {
                        alert("Ooops!\n" + json.msg);
                    }
                }
            }else{
                alert("YES: " + json.msg);
                console.log(json.msg);
            }
        },
        'json'
    );
}

$(document).ready(function(){

    $('#save_remark').click(function(){save_remark();});
    $('#add_cart').click(function(){add_cart();});

    $('#offline-publications').change(function(){

        var selItems = "<div id='select-products' class='products-group-offline'><div class='icon_dropdown pull_right'>";
            
            selItems += "<select id='offline-products-option' name='offline-products-option' class='select-product-options n_sel'>";

        $('#sel-spot-products').empty();

        $.post(
                    '/admin/ads/publications/' + $(this).val(), {
                        '_csrftoken': $('#_csrftoken').val()
                    },
                    function (data) {

                        if(data['products'].length)
                        {
                            for (var i = 0; i < data['products'].length; i++) {
                                selItems +='<option value="' + data['products'][i].id + '">' + data['products'][i].name + '</option>';
                            };
                            selItems += "</select></div></div>";
                            
                            $('#sel-spot-products').append(selItems);

                            $('#sel-spot-issues').empty();

                            if(data['issues'].length){

                                selIssItems = "<div id='select-issues'>";
                                for (var j = 0; j < data['issues'].length; j++) {
                                    selIssItems +='<div><input type="checkbox" name="offline-issues[]" value="' + data['issues'][j].id + '"><label for="offline-issues"><span class="text-primary">' + data['issues'][j].date_published + '</span></label></div>';
                                };
                                selIssItems += "</div>";
                                $('#sel-spot-issues').append(selIssItems);
                            }else{
                                $('#sel-spot-issues').append("<p>Nema dostupnih datuma.</p>");
                            }
                        }else{
                            $('#sel-spot-products').append('<p>Nema dostupnih proizvoda.</p>');   
                        }
                        
                    },
                    'json'
            );
    });



    
    
    //IIGGOORR
function getUserName( field_name ) {    
    var $n_sales_rep_id = $('#'+ field_name);
    var $n_sales_rep_id_input = $('#' + field_name + '_input');
    if ('undefined' === typeof $n_sales_rep_id_input || $n_sales_rep_id_input.length == 0) {
        $n_sales_rep_id_input = null;
    }
    if ($n_sales_rep_id_input) {
        console.log($n_sales_rep_id_input)
        var user_filter = $n_sales_rep_id_input.magicSuggest({
            allowFreeEntries: false,
            autoSelect: true,
            highlight: false,
            selectFirst: true,
            hideTrigger: true,
            maxSelection: 1,
            minChars: 1,
            displayField: 'username',
            noSuggestionText: 'No users found matching your search',
            placeholder: 'Choose a user',
            useTabKey: true,
            required: true,
            resultAsString: true,
            data: function(q) {
                var usersData = [];
                if ($.trim(q)) {
                    $.ajax({
                        async: false,
                        url: '/ajax/users/' + $.trim(q),
                        cache: false,
                        dataType: 'json'
                        
                    }).done(function(json_data) {
                        usersData = json_data;
                    });
                }
//                 console.log(data)
                return usersData;
                
            },
            renderer: function(data){
                console.log(data)
                var userRemark = '';
                if (data.remark) {
                    userRemark = '<br/><span class="fa fa-fw"></span><small><span class="fa fa-warning text-danger"></span> ' + data.remark + '</small>';
                }
                var userIcon = '<span class="fa fa-' + data.icon + ' fa-fw"></span>';
                var userRow = '<div>' + userIcon + ' <span class="username">' + data.username + '</span>' + (data.full_name ? '<span class="full_name">[' + data.full_name + ']</span>' : '') + '<br><span class="fa fa-fw"></span><small>' + data.email + '</small>' + userRemark + '</div>';
                return userRow;
            },
            selectionRenderer: function(data){
                var userRemark = '';
                if (data.remark) {
                    userRemark = '<span class="fa fa-warning fa-fw text-danger" data-toggle="tooltip" data-type="html" data-placement="right" title="' + data.remark + '"></span>';
                }
                var userIcon = '<span class="fa fa-' + data.icon + ' fa-fw"></span>';
                var userRow = userRemark + userIcon + ' ' + data.username;
                return userRow;
            }
        });

        var $uid = $n_sales_rep_id.val();
        if ($.trim($uid)) {
            var $username = $n_sales_rep_id.data('username');
            if ('undefined' !== typeof $username && $.trim($username)) {
                user_filter.setSelection([{id: parseInt($uid), username: $.trim($username)}]);
            }
        }

        $(user_filter).on('selectionchange', function(e, m){
            var selectedItem = this.getSelection();
            if (selectedItem.length == 1) {
                selectedItem = selectedItem[0];
            } else {
                selectedItem = {id: '', username: ''};
            }
            $n_sales_rep_id.val(selectedItem.id).attr('data-username', selectedItem.username).data('username', selectedItem.username);
        });
//       console.log(user_filter)
      
    }
    
    
    
    
}
    
    
function getCategory( field_name ) {    
    var $n_category_id = $('#'+ field_name);
    var $n_category_id_input = $('#' + field_name + '_input');
    if ('undefined' === typeof $n_category_id_input || $n_category_id_input.length == 0) {
        $n_category_id_input = null;
    }
    if ($n_category_id_input) {
        console.log($n_category_id_input)
        var category_filter = $n_category_id_input.magicSuggest({
            allowFreeEntries: false,
            autoSelect: true,
            highlight: false,
            selectFirst: true,
            hideTrigger: true,
            maxSelection: 1,
            minChars: 1,
            displayField: 'name',
            noSuggestionText: 'No categories found matching your search',
            placeholder: 'Choose a category',
            useTabKey: true,
            required: true,
            resultAsString: true,
            data: function(q) {
                var categoriesData = [];
                if ($.trim(q)) {
                    $.ajax({
                        async: false,
                        url: '/ajax/categories/' + $.trim(q),
                        cache: false,
                        dataType: 'json'
                        
                    }).done(function(json_data) {
                        categoriesData = json_data;
                    });
                }
//                   if ($.trim(q)) {
//                     $.ajax({
//                         async: false,
//                         url: '/admin/ads/ajaxGetCategory/' + $.trim(q),
//                         cache: false,
//                         dataType: 'json'
                        
//                     }).done(function(json_data) {
//                         categoriesData = json_data;
//                     });
//                 }
//                 console.log(data)
                return categoriesData;
                
            },
            renderer: function(data){
//                 console.log(data)
                var categoryRemark = '';

                var categoryIcon = '<span class="fa fa-' + data.icon + ' fa-fw"></span>';
                var categoryRow = '<div>' + userIcon + ' <span class="name">' + data.name + '</span>' + (data.name ? '<span class="full_name">[' + data.name + ']</span>' : '') + '<br><span class="fa fa-fw"></span><small>' + data.name + '</small>' + '</div>';
                return categoryRow;
            },
            selectionRenderer: function(data){

                var categoryIcon = '<span class="fa fa-' + data.icon + ' fa-fw"></span>';
                var categoryRow =  + categoryIcon + ' ' + data.name;
                return categoryRow;
            }
        });

        var $uid = $n_category_id.val();
        if ($.trim($uid)) {
            var $categoryName = $n_category_id.data('name');
            if ('undefined' !== typeof $categoryName && $.trim($categoryName)) {
                category_filter.setSelection([{id: parseInt($uid), categoryName: $.trim($categoryName)}]);
            }
        }

        $(category_filter).on('selectionchange', function(e, m){
            var selectedItem = this.getSelection();
            if (selectedItem.length == 1) {
                selectedItem = selectedItem[0];
            } else {
                selectedItem = {id: '', name: ''};
            }
            $n_category_id.val(selectedItem.id).attr('data-name', selectedItem.name).data('name', selectedItem.name);
        });
//       console.log(user_filter)
      
    }
    
    
    
    
}
    
    
    
getUserName('n_sales_rep_id');
getUserName('n_sub_customer');  
    
getCategory('n_category_id'); 
    
    
    
    
    
    
  
//     var $n_sales_rep_id = $('#n_sales_rep_id');
//     var $n_sales_rep_id_input = $('#n_sales_rep_id_input');
//     if ('undefined' === typeof $n_sales_rep_id_input || $n_sales_rep_id_input.length == 0) {
//         $n_sales_rep_id_input = null;
//     }
//     if ($n_sales_rep_id_input) {
//         var user_filter = $n_sales_rep_id_input.magicSuggest({
//             allowFreeEntries: false,
//             autoSelect: true,
//             highlight: false,
//             selectFirst: true,
//             hideTrigger: true,
//             maxSelection: 1,
//             minChars: 1,
//             displayField: 'username',
//             noSuggestionText: 'No users found matching your search',
//             placeholder: 'Choose a user',
//             useTabKey: true,
//             required: true,
//             resultAsString: true,
//             data: function(q) {
//                 var usersData = [];
//                 if ($.trim(q)) {
//                     $.ajax({
//                         async: false,
//                         url: '/ajax/users/' + $.trim(q),
//                         cache: false,
//                         dataType: 'json'
//                     }).done(function(json_data) {
//                         usersData = json_data;
//                     });
//                 }
//                 return usersData;
//             },
//             renderer: function(data){
//                 var userRemark = '';
//                 if (data.remark) {
//                     userRemark = '<br/><span class="fa fa-fw"></span><small><span class="fa fa-warning text-danger"></span> ' + data.remark + '</small>';
//                 }
//                 var userIcon = '<span class="fa fa-' + data.icon + ' fa-fw"></span>';
//                 var userRow = '<div>' + userIcon + ' <span class="username">' + data.username + '</span>' + (data.full_name ? '<span class="full_name">[' + data.full_name + ']</span>' : '') + '<br><span class="fa fa-fw"></span><small>' + data.email + '</small>' + userRemark + '</div>';
//                 return userRow;
//             },
//             selectionRenderer: function(data){
//                 var userRemark = '';
//                 if (data.remark) {
//                     userRemark = '<span class="fa fa-warning fa-fw text-danger" data-toggle="tooltip" data-type="html" data-placement="right" title="' + data.remark + '"></span>';
//                 }
//                 var userIcon = '<span class="fa fa-' + data.icon + ' fa-fw"></span>';
//                 var userRow = userRemark + userIcon + ' ' + data.username;
//                 return userRow;
//             }
//         });

//         var $uid = $n_sales_rep_id.val();
//         if ($.trim($uid)) {
//             var $username = $n_sales_rep_id.data('username');
//             if ('undefined' !== typeof $username && $.trim($username)) {
//                 user_filter.setSelection([{id: parseInt($uid), username: $.trim($username)}]);
//             }
//         }

//         $(user_filter).on('selectionchange', function(e, m){
//             var selectedItem = this.getSelection();
//             if (selectedItem.length == 1) {
//                 selectedItem = selectedItem[0];
//             } else {
//                 selectedItem = {id: '', username: ''};
//             }
//             $n_sales_rep_id.val(selectedItem.id).attr('data-username', selectedItem.username).data('username', selectedItem.username);
//         });
//       console.log(user_filter)
      
//     }
  
  
//   var $n_sub_customer = $('#n_sub_customer');
//     var $n_sub_customer_input = $('#n_sub_customer_input');
//     if ('undefined' === typeof $n_sub_customer_input || $n_sub_customer_input.length == 0) {
//         $n_sub_customer_input = null;
//     }
//     if ($n_sub_customer_input) {
//         var user_filter = $n_sub_customer_input.magicSuggest({
//             allowFreeEntries: false,
//             autoSelect: true,
//             highlight: false,
//             selectFirst: true,
//             hideTrigger: true,
//             maxSelection: 1,
//             minChars:3,
//             displayField: 'username',
//             noSuggestionText: 'No users found matching your search',
//             placeholder: 'Choose a user',
//             useTabKey: true,
//             required: true,
//             resultAsString: true,
//             data: function(q) {
//                 var usersData = [];
//                 if ($.trim(q)) {
//                     $.ajax({
//                         async: false,
//                         url: '/ajax/users/' + $.trim(q),
//                         cache: false,
//                         dataType: 'json'
//                     }).done(function(json_data) {
//                         usersData = json_data;
//                     });
//                 }
//                 return usersData;
//             },
//             renderer: function(data){
//                 var userRemark = '';
//                 if (data.remark) {
//                     userRemark = '<br/><span class="fa fa-fw"></span><small><span class="fa fa-warning text-danger"></span> ' + data.remark + '</small>';
//                 }
//                 var userIcon = '<span class="fa fa-' + data.icon + ' fa-fw"></span>';
//                 var userRow = '<div>' + userIcon + ' <span class="username">' + data.username + '</span>' + (data.full_name ? '<span class="full_name">[' + data.full_name + ']</span>' : '') + '<br><span class="fa fa-fw"></span><small>' + data.email + '</small>' + userRemark + '</div>';
//                 return userRow;
//             },
//             selectionRenderer: function(data){
//                 var userRemark = '';
//                 if (data.remark) {
//                     userRemark = '<span class="fa fa-warning fa-fw text-danger" data-toggle="tooltip" data-type="html" data-placement="right" title="' + data.remark + '"></span>';
//                 }
//                 var userIcon = '<span class="fa fa-' + data.icon + ' fa-fw"></span>';
//                 var userRow = userRemark + userIcon + ' ' + data.username;
//                 return userRow;
//             }
//         });

//         var $uid = $n_sub_customer.val();
//         if ($.trim($uid)) {
//             var $username = $n_sub_customer.data('username');
//             if ('undefined' !== typeof $username && $.trim($username)) {
//                 user_filter.setSelection([{id: parseInt($uid), username: $.trim($username)}]);
//             }
//         }

//         $(user_filter).on('selectionchange', function(e, m){
//             var selectedItem = this.getSelection();
//             if (selectedItem.length == 1) {
//                 selectedItem = selectedItem[0];
//             } else {
//                 selectedItem = {id: '', username: ''};
//             }
//             $n_sub_customer.val(selectedItem.id).attr('data-username', selectedItem.username).data('username', selectedItem.username);
//             get_users_public_phones();
//         });
//     }
  
  
  
  
  
    $('.moreless_items').moreless();

    $('select.moderation-reasons').selectpicker({
        style: 'btn btn-default btn-sm',
        size: 8,
        liveSearch: true,
        title: 'Choose reason',
        noneSelectedText: 'Choose reason',
        noneResultsText: 'No reasons match',
        mobile: OGL.is_mobile_browser,
        showSubtext: false
    });

    $('#moderation').change(function(e){
        $('#moderation_reason_box, .moderation-reason-group, #reason_email_box').hide();
        if (parseInt($(this).find(':selected').data('has-reasons'))) {
            $('#moderation_reason_box, #moderation_reason_' + $(this).val() + '_box, #reason_email_box').show();
        }
    });

    $('#moderation').trigger('change');

    $('#refreshPhoneNumbers').click(function(){
        get_users_public_phones();
    });

    // change ad's category
    $new_category_dropdown = $('#new_category_id');
    $new_category_dropdown.selectpicker({
        style: 'btn btn-default',
        size: 8,
        liveSearch: true,
        title: 'Choose category',
        noneSelectedText: 'Choose category',
        noneResultsText: 'No categories match',
        mobile: OGL.is_mobile_browser,
        showSubtext: true
    });
    $new_category_dropdown.on('change', get_matching_parameters);
    $('#changeCategoryBtn').click(function(){
        $('#changeAdsCategoryModal').modal({
            backdrop: 'static',
            keyboard: true
        }).on('shown.bs.modal', function(e){
            $('button.dropdown-toggle').trigger('click');
        });
    });
    $('#changeAdsCategoryModalMoveBtn').click(function(){
        if ('undefined' !== typeof $(this).data('target-category-id') && parseInt($.trim($(this).data('target-category-id')))) {
            $('#category_id').val(parseInt($.trim($(this).data('target-category-id'))));
            // submit the form immediately.
            $('button[name="save"]').click();
        }
    });
    
    
    if($(".popusti").val('')){
        $(this).hide()
    }
    
    
    

    
});
