CKEDITOR.plugins.add('imagebrowser', {
	init: function(editor) {

		if (typeof(editor.config.imageBrowser_listUrl) === 'undefined' || editor.config.imageBrowser_listUrl === null) {
			return;
		}

		var custom_source_media;

		var generate_browse_url = function() {
			var url = editor.plugins.imagebrowser.path + 'browser/browser.html?listUrl=' + encodeURIComponent(editor.config.imageBrowser_listUrl);
			if (editor.config.imageBrowser_custom_source) {
				var custom_source = editor.config.imageBrowser_custom_source;
				if ('undefined' !== typeof custom_source.name && 'undefined' !== typeof custom_source.field) {
					var $form = editor.element.$.form;
					var name = custom_source.field;
					var value;
					if (name.substr(-2) == '[]') {
						var values = [];
						$($form).find('[name="' + name + '"]').each(function() {
							values.push($(this).val());
						});
						value = values.join(',');
						name = name.substr(0, name.length -2);
					} else {
						value = $form.elements[name].value;
					}

					custom_source_media = {
						name: custom_source.name,
						ids: value
					};

					url += '&custom_source=' + encodeURIComponent(JSON.stringify(custom_source_media));
				}
			}
			if (editor.config.baseHref) {
				url += '&baseHref=' + encodeURIComponent(editor.config.baseHref);
			}

			return url;
		};

		editor.config.filebrowserImageBrowseUrl = generate_browse_url();

		CKEDITOR.on('dialogDefinition', function(ev) {
			var dialogName = ev.data.name;
			var dialogDefinition = ev.data.definition;
			var currDialog = dialogDefinition.dialog;

			currDialog.on('show', function(d){
				var browse_url = generate_browse_url();

				var info_browse_btn = currDialog.getContentElement('info', 'browse');
				info_browse_btn.filebrowser.url = browse_url;

				var link_browse_btn = currDialog.getContentElement('Link', 'browse');
				link_browse_btn.filebrowser.url = browse_url;
			});
		});
	}
});
