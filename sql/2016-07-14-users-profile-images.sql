CREATE TABLE `users_profile_images` (
	`id` BIGINT(20) UNSIGNED NOT NULL AUTO_INCREMENT,
	`user_id` INT(11) UNSIGNED NOT NULL,
	`avatar` VARCHAR(254) NULL DEFAULT NULL COLLATE 'utf8_croatian_ci',
	`avatar_details` TEXT NULL DEFAULT NULL,
	`profile_bg` VARCHAR(254) NULL DEFAULT NULL COLLATE 'utf8_croatian_ci',
	`profile_bg_details` TEXT NULL DEFAULT NULL,
	`created_at` DATETIME NULL DEFAULT NULL,
	`modified_at` DATETIME NULL DEFAULT NULL,
	PRIMARY KEY (`id`),
	INDEX `user_id` (`user_id`),
	INDEX `avatar` (`avatar`),
	INDEX `profile_bg` (`profile_bg`),
	CONSTRAINT `users_profile_images_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON UPDATE CASCADE ON DELETE CASCADE
)
COLLATE='utf8_croatian_ci'
ENGINE=InnoDB
;
