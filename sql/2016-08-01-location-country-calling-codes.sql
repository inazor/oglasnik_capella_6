ALTER TABLE `location` ADD `calling_code` INT UNSIGNED NULL DEFAULT NULL AFTER `iso_code`;
-- Hrvatska
UPDATE `location` SET `calling_code` = 385 WHERE id = 1;
-- Afganistan
UPDATE `location` SET `calling_code` = 93 WHERE id = 8;
-- Albanija
UPDATE `location` SET `calling_code` = 355 WHERE id = 9;
-- Alžir
UPDATE `location` SET `calling_code` = 213 WHERE id = 10;
-- Američka Samoa
UPDATE `location` SET `calling_code` = 1 WHERE id = 12;
-- Andora
UPDATE `location` SET `calling_code` = 376 WHERE id = 13;
-- Angila
UPDATE `location` SET `calling_code` = 1 WHERE id = 14;
-- Angola
UPDATE `location` SET `calling_code` = 244 WHERE id = 15;
-- Antarktika
UPDATE `location` SET `calling_code` = 672 WHERE id = 22;
-- Antigva i Barbuda
UPDATE `location` SET `calling_code` = 1 WHERE id = 16;
-- Argentina
UPDATE `location` SET `calling_code` = 54 WHERE id = 17;
-- Armenija
UPDATE `location` SET `calling_code` = 374 WHERE id = 18;
-- Aruba
UPDATE `location` SET `calling_code` = 297 WHERE id = 19;
-- Australija
UPDATE `location` SET `calling_code` = 61 WHERE id = 21;
-- Austrija
UPDATE `location` SET `calling_code` = 43 WHERE id = 23;
-- Azerbajdžan
UPDATE `location` SET `calling_code` = 994 WHERE id = 24;
-- Bahama
UPDATE `location` SET `calling_code` = 1 WHERE id = 26;
-- Bahrein
UPDATE `location` SET `calling_code` = 973 WHERE id = 27;
-- Bangladeš
UPDATE `location` SET `calling_code` = 880 WHERE id = 28;
-- Barbados
UPDATE `location` SET `calling_code` = 1 WHERE id = 29;
-- Belgija
UPDATE `location` SET `calling_code` = 32 WHERE id = 30;
-- Belize
UPDATE `location` SET `calling_code` = 501 WHERE id = 31;
-- Benin
UPDATE `location` SET `calling_code` = 229 WHERE id = 32;
-- Bermuda
UPDATE `location` SET `calling_code` = 1 WHERE id = 33;
-- Bjelorusija
UPDATE `location` SET `calling_code` = 375 WHERE id = 34;
-- Bocvana
UPDATE `location` SET `calling_code` = 267 WHERE id = 35;
-- Bolivija
UPDATE `location` SET `calling_code` = 591 WHERE id = 36;
-- Bosna i Hercegovina
UPDATE `location` SET `calling_code` = 387 WHERE id = 2;
-- Brazil
UPDATE `location` SET `calling_code` = 55 WHERE id = 37;
-- Brunej
UPDATE `location` SET `calling_code` = 673 WHERE id = 38;
-- Bugarska
UPDATE `location` SET `calling_code` = 359 WHERE id = 39;
-- Burkina Faso
UPDATE `location` SET `calling_code` = 226 WHERE id = 40;
-- Burundi
UPDATE `location` SET `calling_code` = 257 WHERE id = 41;
-- Butan
UPDATE `location` SET `calling_code` = 975 WHERE id = 42;
-- Caymanski otoci
UPDATE `location` SET `calling_code` = 1 WHERE id = 43;
-- Cipar
UPDATE `location` SET `calling_code` = 357 WHERE id = 44;
-- Cookovo otocje
UPDATE `location` SET `calling_code` = 682 WHERE id = 45;
-- Crna Gora
UPDATE `location` SET `calling_code` = 382 WHERE id = 3;
-- Čad
UPDATE `location` SET `calling_code` = 235 WHERE id = 46;
-- Češka Republika
UPDATE `location` SET `calling_code` = 420 WHERE id = 47;
-- Čile
UPDATE `location` SET `calling_code` = 56 WHERE id = 11;
-- Danska
UPDATE `location` SET `calling_code` = 45 WHERE id = 48;
-- Djevičansko otočje
UPDATE `location` SET `calling_code` = 1 WHERE id = 50;
-- Dominika
UPDATE `location` SET `calling_code` = 1 WHERE id = 51;
-- Dominikanska Republika
UPDATE `location` SET `calling_code` = 1 WHERE id = 52;
-- Džibuti
UPDATE `location` SET `calling_code` = 253 WHERE id = 53;
-- Egipat
UPDATE `location` SET `calling_code` = 20 WHERE id = 54;
-- Ekvador
UPDATE `location` SET `calling_code` = 593 WHERE id = 55;
-- Ekvatorska Gvineja
UPDATE `location` SET `calling_code` = 240 WHERE id = 56;
-- El Salvador
UPDATE `location` SET `calling_code` = 503 WHERE id = 57;
-- Eritreja
UPDATE `location` SET `calling_code` = 291 WHERE id = 58;
-- Estonija
UPDATE `location` SET `calling_code` = 372 WHERE id = 59;
-- Etiopija
UPDATE `location` SET `calling_code` = 251 WHERE id = 60;
-- Falklandsko otočje
UPDATE `location` SET `calling_code` = 500 WHERE id = 61;
-- Farski otoci
UPDATE `location` SET `calling_code` = 298 WHERE id = 62;
-- Fidži
UPDATE `location` SET `calling_code` = 679 WHERE id = 63;
-- Filipini
UPDATE `location` SET `calling_code` = 63 WHERE id = 64;
-- Finska
UPDATE `location` SET `calling_code` = 358 WHERE id = 65;
-- Francuska
UPDATE `location` SET `calling_code` = 33 WHERE id = 66;
-- Francuska Gijana
UPDATE `location` SET `calling_code` = 594 WHERE id = 67;
-- Francuska Polinezija
UPDATE `location` SET `calling_code` = 689 WHERE id = 68;
-- Gabon
UPDATE `location` SET `calling_code` = 241 WHERE id = 69;
-- Gambija
UPDATE `location` SET `calling_code` = 220 WHERE id = 70;
-- Gana
UPDATE `location` SET `calling_code` = 233 WHERE id = 71;
-- Gibraltar
UPDATE `location` SET `calling_code` = 350 WHERE id = 72;
-- Grčka
UPDATE `location` SET `calling_code` = 30 WHERE id = 73;
-- Grenada
UPDATE `location` SET `calling_code` = 1 WHERE id = 74;
-- Grenland
UPDATE `location` SET `calling_code` = 299 WHERE id = 75;
-- Gruzija
UPDATE `location` SET `calling_code` = 995 WHERE id = 76;
-- Guam
UPDATE `location` SET `calling_code` = 1 WHERE id = 77;
-- Gvadalupa
UPDATE `location` SET `calling_code` = 590 WHERE id = 78;
-- Gvajana
UPDATE `location` SET `calling_code` = 592 WHERE id = 79;
-- Gvatemala
UPDATE `location` SET `calling_code` = 502 WHERE id = 80;
-- Gvineja
UPDATE `location` SET `calling_code` = 224 WHERE id = 81;
-- Gvineja Bisau
UPDATE `location` SET `calling_code` = 245 WHERE id = 82;
-- Haiti
UPDATE `location` SET `calling_code` = 509 WHERE id = 83;
-- Honduras
UPDATE `location` SET `calling_code` = 504 WHERE id = 85;
-- HongKong
UPDATE `location` SET `calling_code` = 852 WHERE id = 86;
-- Indija
UPDATE `location` SET `calling_code` = 91 WHERE id = 87;
-- Indonezija
UPDATE `location` SET `calling_code` = 62 WHERE id = 88;
-- Irak
UPDATE `location` SET `calling_code` = 964 WHERE id = 89;
-- Iran
UPDATE `location` SET `calling_code` = 98 WHERE id = 90;
-- Irska
UPDATE `location` SET `calling_code` = 353 WHERE id = 91;
-- Island
UPDATE `location` SET `calling_code` = 354 WHERE id = 92;
-- Istočni Timor
UPDATE `location` SET `calling_code` = 670 WHERE id = 49;
-- Italija
UPDATE `location` SET `calling_code` = 39 WHERE id = 93;
-- Izrael
UPDATE `location` SET `calling_code` = 972 WHERE id = 94;
-- Jamajka
UPDATE `location` SET `calling_code` = 1 WHERE id = 95;
-- Japan
UPDATE `location` SET `calling_code` = 81 WHERE id = 96;
-- Jemen
UPDATE `location` SET `calling_code` = 967 WHERE id = 97;
-- Jordan
UPDATE `location` SET `calling_code` = 962 WHERE id = 98;
-- Južnoafricka Republika
UPDATE `location` SET `calling_code` = 27 WHERE id = 99;
-- Kambodža
UPDATE `location` SET `calling_code` = 855 WHERE id = 100;
-- Kamerun
UPDATE `location` SET `calling_code` = 237 WHERE id = 101;
-- Kanada
UPDATE `location` SET `calling_code` = 1 WHERE id = 102;
-- Katar
UPDATE `location` SET `calling_code` = 974 WHERE id = 103;
-- Kazahstan
UPDATE `location` SET `calling_code` = 7 WHERE id = 104;
-- Kenija
UPDATE `location` SET `calling_code` = 254 WHERE id = 105;
-- Kina
UPDATE `location` SET `calling_code` = 86 WHERE id = 106;
-- Kirgistan
UPDATE `location` SET `calling_code` = 996 WHERE id = 107;
-- Kiribati
UPDATE `location` SET `calling_code` = 686 WHERE id = 108;
-- Kolumbija
UPDATE `location` SET `calling_code` = 57 WHERE id = 109;
-- Komori
UPDATE `location` SET `calling_code` = 269 WHERE id = 110;
-- Kongo
UPDATE `location` SET `calling_code` = 242 WHERE id = 111;
-- Kongo Demokratska Republika
UPDATE `location` SET `calling_code` = 243 WHERE id = 112;
-- Koreja, Južna
UPDATE `location` SET `calling_code` = 850 WHERE id = 113;
-- Koreja, Sjeverna
UPDATE `location` SET `calling_code` = 82 WHERE id = 114;
-- Kosovo
UPDATE `location` SET `calling_code` = 381 WHERE id = 4;
-- Kostarika
UPDATE `location` SET `calling_code` = 506 WHERE id = 115;
-- Kuba
UPDATE `location` SET `calling_code` = 53 WHERE id = 116;
-- Kuvajt
UPDATE `location` SET `calling_code` = 965 WHERE id = 117;
-- Laos
UPDATE `location` SET `calling_code` = 856 WHERE id = 118;
-- Lesoto
UPDATE `location` SET `calling_code` = 266 WHERE id = 119;
-- Letonija-Latvija
UPDATE `location` SET `calling_code` = 371 WHERE id = 120;
-- Libanon
UPDATE `location` SET `calling_code` = 961 WHERE id = 121;
-- Liberija
UPDATE `location` SET `calling_code` = 231 WHERE id = 122;
-- Libija
UPDATE `location` SET `calling_code` = 218 WHERE id = 123;
-- Lihtenštajn
UPDATE `location` SET `calling_code` = 423 WHERE id = 124;
-- Litva
UPDATE `location` SET `calling_code` = 370 WHERE id = 125;
-- Luksemburg
UPDATE `location` SET `calling_code` = 352 WHERE id = 126;
-- Madagaskar
UPDATE `location` SET `calling_code` = 261 WHERE id = 127;
-- Mađarska
UPDATE `location` SET `calling_code` = 36 WHERE id = 128;
-- Makao
UPDATE `location` SET `calling_code` = 853 WHERE id = 129;
-- Makedonija
UPDATE `location` SET `calling_code` = 389 WHERE id = 5;
-- Malavi
UPDATE `location` SET `calling_code` = 265 WHERE id = 130;
-- Maldivi
UPDATE `location` SET `calling_code` = 960 WHERE id = 131;
-- Malezija
UPDATE `location` SET `calling_code` = 60 WHERE id = 132;
-- Mali
UPDATE `location` SET `calling_code` = 223 WHERE id = 133;
-- Malta
UPDATE `location` SET `calling_code` = 356 WHERE id = 134;
-- Marijansko otočje
UPDATE `location` SET `calling_code` = 1 WHERE id = 135;
-- Maroko
UPDATE `location` SET `calling_code` = 212 WHERE id = 136;
-- Marshallovi otoci
UPDATE `location` SET `calling_code` = 692 WHERE id = 137;
-- Martinik
UPDATE `location` SET `calling_code` = 596 WHERE id = 138;
-- Mauretanija
UPDATE `location` SET `calling_code` = 222 WHERE id = 139;
-- Mauricius
UPDATE `location` SET `calling_code` = 230 WHERE id = 140;
-- Mayotte
UPDATE `location` SET `calling_code` = 262 WHERE id = 141;
-- Meksiko
UPDATE `location` SET `calling_code` = 52 WHERE id = 142;
-- Mianma-Myanma
UPDATE `location` SET `calling_code` = 95 WHERE id = 143;
-- Mikronezija
UPDATE `location` SET `calling_code` = 691 WHERE id = 144;
-- Moldavija
UPDATE `location` SET `calling_code` = 373 WHERE id = 145;
-- Monako
UPDATE `location` SET `calling_code` = 377 WHERE id = 146;
-- Mongolija
UPDATE `location` SET `calling_code` = 976 WHERE id = 147;
-- Montserrat
UPDATE `location` SET `calling_code` = 1 WHERE id = 148;
-- Mozambik
UPDATE `location` SET `calling_code` = 258 WHERE id = 149;
-- Namibija
UPDATE `location` SET `calling_code` = 264 WHERE id = 150;
-- Nauru
UPDATE `location` SET `calling_code` = 674 WHERE id = 151;
-- Nepal
UPDATE `location` SET `calling_code` = 977 WHERE id = 152;
-- Niger
UPDATE `location` SET `calling_code` = 227 WHERE id = 153;
-- Nigerija
UPDATE `location` SET `calling_code` = 234 WHERE id = 154;
-- Nikaragva
UPDATE `location` SET `calling_code` = 505 WHERE id = 155;
-- Niue
UPDATE `location` SET `calling_code` = 683 WHERE id = 156;
-- Nizozemska
UPDATE `location` SET `calling_code` = 31 WHERE id = 157;
-- Nizozemski Antili
UPDATE `location` SET `calling_code` = 599 WHERE id = 158;
-- Norfolk
UPDATE `location` SET `calling_code` = 672 WHERE id = 159;
-- Norveška
UPDATE `location` SET `calling_code` = 47 WHERE id = 160;
-- Nova Kaledonija
UPDATE `location` SET `calling_code` = 687 WHERE id = 161;
-- Novi Zeland
UPDATE `location` SET `calling_code` = 64 WHERE id = 162;
-- Njemacka
UPDATE `location` SET `calling_code` = 49 WHERE id = 163;
-- Obala Bjelokosti
UPDATE `location` SET `calling_code` = 225 WHERE id = 164;
-- Oman
UPDATE `location` SET `calling_code` = 968 WHERE id = 165;
-- Pakistan
UPDATE `location` SET `calling_code` = 92 WHERE id = 166;
-- Palau
UPDATE `location` SET `calling_code` = 680 WHERE id = 167;
-- Palestina
UPDATE `location` SET `calling_code` = 970 WHERE id = 20;
-- Panama
UPDATE `location` SET `calling_code` = 507 WHERE id = 168;
-- Papua Nova Gvineja
UPDATE `location` SET `calling_code` = 675 WHERE id = 169;
-- Paragvaj
UPDATE `location` SET `calling_code` = 595 WHERE id = 170;
-- Peru
UPDATE `location` SET `calling_code` = 51 WHERE id = 171;
-- Pitcairnovo Otočje
UPDATE `location` SET `calling_code` = 64 WHERE id = 25;
-- Poljska
UPDATE `location` SET `calling_code` = 48 WHERE id = 172;
-- Portoriko
UPDATE `location` SET `calling_code` = 1 WHERE id = 173;
-- Portugal
UPDATE `location` SET `calling_code` = 351 WHERE id = 174;
-- Réunion
UPDATE `location` SET `calling_code` = 262 WHERE id = 175;
-- Ruanda
UPDATE `location` SET `calling_code` = 250 WHERE id = 176;
-- Rumunjska
UPDATE `location` SET `calling_code` = 40 WHERE id = 177;
-- Ruska Federacija
UPDATE `location` SET `calling_code` = 7 WHERE id = 178;
-- Salamunski otoci
UPDATE `location` SET `calling_code` = 677 WHERE id = 179;
-- San Marino
UPDATE `location` SET `calling_code` = 378 WHERE id = 180;
-- San Tome i Prinsipe
UPDATE `location` SET `calling_code` = 239 WHERE id = 181;
-- Saudijska Arabija
UPDATE `location` SET `calling_code` = 966 WHERE id = 182;
-- Sejšeli
UPDATE `location` SET `calling_code` = 248 WHERE id = 183;
-- Senegal
UPDATE `location` SET `calling_code` = 221 WHERE id = 184;
-- Sijera Leone
UPDATE `location` SET `calling_code` = 232 WHERE id = 185;
-- Singapur
UPDATE `location` SET `calling_code` = 65 WHERE id = 186;
-- Sirija
UPDATE `location` SET `calling_code` = 963 WHERE id = 187;
-- Sjedinjene Američke Države
UPDATE `location` SET `calling_code` = 1 WHERE id = 188;
-- Slovačka
UPDATE `location` SET `calling_code` = 421 WHERE id = 189;
-- Slovenija
UPDATE `location` SET `calling_code` = 386 WHERE id = 6;
-- Somalija
UPDATE `location` SET `calling_code` = 252 WHERE id = 190;
-- Srbija
UPDATE `location` SET `calling_code` = 381 WHERE id = 7;
-- Srednjoafricka Rep.
UPDATE `location` SET `calling_code` = 236 WHERE id = 191;
-- Sudan
UPDATE `location` SET `calling_code` = 249 WHERE id = 192;
-- Surinam
UPDATE `location` SET `calling_code` = 597 WHERE id = 193;
-- Svazi
UPDATE `location` SET `calling_code` = 268 WHERE id = 194;
-- Sveta Helena
UPDATE `location` SET `calling_code` = 290 WHERE id = 195;
-- Sveta Lucija
UPDATE `location` SET `calling_code` = 1 WHERE id = 196;
-- Sveti Kristofer i Nevis
UPDATE `location` SET `calling_code` = 1 WHERE id = 197;
-- Sveti Pierre i Miquelon
UPDATE `location` SET `calling_code` = 508 WHERE id = 198;
-- Sveti Vincent i Grenadini
UPDATE `location` SET `calling_code` = 1 WHERE id = 199;
-- Španjolska
UPDATE `location` SET `calling_code` = 34 WHERE id = 200;
-- Šrilanka
UPDATE `location` SET `calling_code` = 94 WHERE id = 201;
-- Švedska
UPDATE `location` SET `calling_code` = 46 WHERE id = 202;
-- Švicarska
UPDATE `location` SET `calling_code` = 41 WHERE id = 203;
-- Tadžikistan
UPDATE `location` SET `calling_code` = 992 WHERE id = 204;
-- Tajland
UPDATE `location` SET `calling_code` = 66 WHERE id = 205;
-- Tajvan
UPDATE `location` SET `calling_code` = 886 WHERE id = 206;
-- Tanzanija
UPDATE `location` SET `calling_code` = 255 WHERE id = 207;
-- Togo
UPDATE `location` SET `calling_code` = 228 WHERE id = 208;
-- Tokelausko otocje
UPDATE `location` SET `calling_code` = 690 WHERE id = 209;
-- Tonga
UPDATE `location` SET `calling_code` = 676 WHERE id = 210;
-- Trinidad i Tobago
UPDATE `location` SET `calling_code` = 1 WHERE id = 211;
-- Tunis
UPDATE `location` SET `calling_code` = 216 WHERE id = 212;
-- Turkmenistan
UPDATE `location` SET `calling_code` = 993 WHERE id = 213;
-- Turksi Caicos
UPDATE `location` SET `calling_code` = 1 WHERE id = 214;
-- Turska
UPDATE `location` SET `calling_code` = 90 WHERE id = 215;
-- Tuvalu
UPDATE `location` SET `calling_code` = 688 WHERE id = 216;
-- UAE
UPDATE `location` SET `calling_code` = 971 WHERE id = 217;
-- Uganda
UPDATE `location` SET `calling_code` = 256 WHERE id = 218;
-- Ukrajina
UPDATE `location` SET `calling_code` = 380 WHERE id = 219;
-- Urugvaj
UPDATE `location` SET `calling_code` = 598 WHERE id = 220;
-- Uzbekistan
UPDATE `location` SET `calling_code` = 998 WHERE id = 221;
-- Vanuatu
UPDATE `location` SET `calling_code` = 678 WHERE id = 222;
-- Vatikan
UPDATE `location` SET `calling_code` = 39 WHERE id = 223;
-- Velika Britanija
UPDATE `location` SET `calling_code` = 44 WHERE id = 224;
-- Venecuela
UPDATE `location` SET `calling_code` = 58 WHERE id = 225;
-- Vijetnam
UPDATE `location` SET `calling_code` = 84 WHERE id = 226;
-- Wallis i Futuna
UPDATE `location` SET `calling_code` = 681 WHERE id = 227;
-- Zambija
UPDATE `location` SET `calling_code` = 260 WHERE id = 228;
-- Zapadna Sahara
UPDATE `location` SET `calling_code` = 212 WHERE id = 84;
-- Zapadna Samoa
UPDATE `location` SET `calling_code` = 685 WHERE id = 229;
-- Zelenortski otoci
UPDATE `location` SET `calling_code` = 238 WHERE id = 230;
-- Zimbabve
UPDATE `location` SET `calling_code` = 263 WHERE id = 231;
