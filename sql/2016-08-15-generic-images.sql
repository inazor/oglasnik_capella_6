-- phpMyAdmin SQL Dump
-- version 4.4.13.1deb1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Aug 15, 2016 at 11:45 AM
-- Server version: 10.1.16-MariaDB-1~wily
-- PHP Version: 5.6.11-1ubuntu3.4

SET FOREIGN_KEY_CHECKS=0;
SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";

--
-- Database: `oglasnik_facelift`
--

-- --------------------------------------------------------

--
-- Table structure for table `generic_images`
--

CREATE TABLE IF NOT EXISTS `generic_images` (
  `id` bigint(20) unsigned NOT NULL,
  `model_name` varchar(64) COLLATE utf8_croatian_ci NOT NULL,
  `model_pk_val` bigint(20) unsigned NOT NULL,
  `model_field` varchar(64) COLLATE utf8_croatian_ci DEFAULT NULL,
  `image` varchar(254) COLLATE utf8_croatian_ci DEFAULT NULL,
  `image_details` text COLLATE utf8_croatian_ci,
  `created_at` datetime DEFAULT NULL,
  `modified_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_croatian_ci;

--
-- RELATIONS FOR TABLE `generic_images`:
--

--
-- Indexes for dumped tables
--

--
-- Indexes for table `generic_images`
--
ALTER TABLE `generic_images`
  ADD PRIMARY KEY (`id`),
  ADD KEY `model_pk_field` (`model_name`,`model_pk_val`,`model_field`) USING BTREE;

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `generic_images`
--
ALTER TABLE `generic_images`
  MODIFY `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT;SET FOREIGN_KEY_CHECKS=1;
COMMIT;
