<?php

$local_file = __DIR__ . '/config.db.local.php';

if (file_exists($local_file)) {
    require $local_file;
} else {
    // default if no local file is provided
    $config_mysql = array(
        'host' => 'localhost',
        'username' => 'root',
        'password' => '',
        'dbname' => 'oglasnik_tests',
        'options' => array(
            \PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8',
            \PDO::ATTR_EMULATE_PREPARES => false,
            \PDO::ATTR_STRINGIFY_FETCHES => false,
        )
    );
}
